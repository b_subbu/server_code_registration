<?php
/**
 * Ajax.php Ajax program to handle Ajax requests
 * For session Management 
 * Should be called like
 * http://ec2-54-213-174-132.us-west-2.compute.amazonaws.com/Ajax.php?functionName=sessionStart&userRole=admin
 * Or it can be post    
 *  
 */
header("Cache-Control: no-cache,no-store,must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header("Pragma: no-cache"); //to avoid caching of previous instructions
session_cache_limiter ('nocache');


//var_dump($_REQUEST);

if(isset($_REQUEST['functionName']))  {
    $funcName = html_entity_decode($_REQUEST['functionName']);
    unset($_REQUEST['func']);
    switch($funcName) {
    case 'sessionStart':
         start_session();
      break;
    case 'sessionCheck':
         session_check();
     break;
    case 'sessionEnd':
         session_end();
         break;
    default:
        echo 'System Error';
        break;
    }
}

function start_session() {
       $sess_id = substr(md5(rand()), 0, 8);
       @session_name($sess_id); //use named session for destorying individual sessions
       @session_start();
       $usr_role = $_REQUEST['userRole'];
       
       //$sess_id = session_id();
       $_SESSION['user_role'] = $usr_role; 
       $_SESSION['session_id']= $sess_id;
       echo $sess_id;
       exit;
 
}

function session_check() {

   $y = $_REQUEST['sessionId'];
   $user_Role = $_REQUEST['userRole'];

   @session_name($y);
   @session_start();


   if(isset($_SESSION['session_id'])) {
     if(session_id() == "") {
       echo 'System Error'; //server not supporting sessions
       exit;
       }

   $x= $_SESSION['session_id'];

   
   if ($x != $y) {
       echo 'Expired Session';
      exit;
     }

   $usr_role = $_SESSION['user_role'];

   if($usr_role != $user_Role) {
      echo 'Invalid User';
      exit;
    }
   else { 
     echo 'success';
     exit;
    } 
  }else {
      echo 'Invalid Session';
      exit;
  }
}


function session_end(){
  $y = $_REQUEST['sessionId'];

  @session_name($y);
  @session_start();
  @session_destroy();
  //cant pass variable to session_destory
  //if this does not work then let us try for session_regenerate to create new id
  echo 'success';
  exit;
}
?>
