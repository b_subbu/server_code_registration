var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
var config = require('./config');

var postAPI = 'removeFromCart';
var profile = {        };
var profile1 = {customerId: '',businessId: '563ef083be12ca4705c9dd77',itemCode: '',itemDetailCode:''};
var profile2 = {customerId: 'tempo000002',businessId: '563ef083be12ca4705c9dd77',itemCode: '',itemDetailCode:''};
var profile3 = {customerId: 'tempo000001',businessId: '563ef083be12ca4705c9dd77',itemCode: '',itemDetailCode:''};
var profile4 = {customerId: 'tempo000001',businessId: '563ef083be12ca4705c9dd77',itemCode: 'RA-001',itemDetailCode:'RA-001-0'};








describe('Routing', function() {
  before(function(done) {
    done();
  });
  describe('removeFromCart', function() {
    it('should return error System error', function(done) {
   request(url)
	.post(postAPI)
	.send(profile)
  .end(function(err, res) {
          if (err) {
            throw err;
          }
          res.body.responseCode.should.equal(6);
          done();
        });
    });
    it('should return Customer not found', function(done){
  	request(url)
		.post(postAPI)
		.send(profile1)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
  it('should return Customer not Active', function(done){
	   request(url)
		.post(postAPI)
		.send(profile2)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
		  res.body.responseCode.should.equal(3);
  		done();
		});
	});
 it('should return Item Code not Found', function(done){
	request(url)
		.post(postAPI)
		.send(profile3)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(4);
  			done();
		});
	});
  
it('should return Success', function(done){
	request(url)
		.post(postAPI)
		.send(profile4)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});


