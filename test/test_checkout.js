var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
var config = require('./config');

var postAPI = 'checkout';
var profile = {        };
var profile1 = {customerId: '',businessId: '55a8d9c3076c1f8c721c8888',itemCode: ''};
var profile2 = {customerId: 'JusC0001000052',businessId: '559e13c4b1e1046a3c5afd14'};
var profile3 = {customerId: 'tempo000002',businessId: '563ef083be12ca4705c9dd77',amount:600,
                billingDetails:{firstName:'abc',lastName:'xyz',address:'123',city:'456',state:'aaa',country:'bbb',zipCode:'xxx',phone:'1234'}};








describe('Routing', function() {
  before(function(done) {
    done();
  });
  describe('checkout', function() {
    it('should return error System error', function(done) {
   request(url)
	.post(postAPI)
	.send(profile)
  .end(function(err, res) {
          if (err) {
            throw err;
          }
          res.body.responseCode.should.equal(6);
          done();
        });
    });
    it('should return Customer not found', function(done){
  	request(url)
		.post(postAPI)
		.send(profile1)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
  it('should return Customer not Active', function(done){
	   request(url)
		.post(postAPI)
		.send(profile2)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
		  res.body.responseCode.should.equal(3);
  		done();
		});
	});
it('should return Success', function(done){
	request(url)
		.post(postAPI)
		.send(profile3)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});


