var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
//var config = require('./config-debug');
var config = require('./config');
var postAPI = 'business/web/editMenu';

describe('Routing', function() {
  before(function(done) {
    // In our tests we use the test db
    //mongoose.connect(config.db.mongodb);							
    done();
  });
  // use describe to give a title to your test suite, the tile 
  describe(postAPI, function() {
    it('should return System error', function(done) {
      var profile = {
        };
   request(url)
	.post(postAPI)
	.send(profile)
	.end(function(err, res) {
          if (err) {
            throw err;
          }
          // this is should.js syntax, very clear
          res.body.responseCode.should.equal(6);
          done();
        });
    });
	var profile = {
     userId: '559e13c4b1e1046a3c5afd14',
      sessionToken:'MyKXeUHD',
      "menuData": {
        "menuName": "Temp123",
        "menuDescription": "Temp Description from Edit",
        "menuLinkage": [
            {
                "id": "1",
                "parent": "#",
                "text": "Temp",
                "state": {
                    "opened": true
                },
                "__uiNodeId": 1,
                "hasChildNodes": true,
                "isLeaf":false
            },
            {
                "id": "2",
                "parent": "1",
                "text": "Test",
                "state": {
                    "opened": true
                },
                "__uiNodeId": 2,
                "isLinked": true,
                "isLeaf":false,
                "linkedNodeId": "Item_4"
            },
            {
                "id": "Item_4",
                "parent": "2",
                "text": "Fourth Item (In Use)",
                "icon": false,
                "state": {
                    "opened": true
                },
                "isEndNode": true,
                "isLeaf":true,
                "nodeIs":"item",
                "linkId": "RA-16",
                "__uiNodeId": 3
            },
             {
                "id": "Item_4",
                "parent": "2",
                "text": "Fourth Item (In Use)",
                "icon": false,
                "state": {
                    "opened": true
                },
                "isEndNode": true,
                "isLeaf":true,
                "nodeIs":"group",
                "linkId": "BusC000300003",
                "__uiNodeId": 3
            }
        ]
    }
 	};
	request(url)
		.post(postAPI)
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });




