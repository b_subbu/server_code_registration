var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
var config = require('./config');

var postAPI = 'business/web/editGroup';
var profile = {        };
var profile1 = {userId: '559e13c4b1e1046a3c5afd14',};
var profile2 = {userId: '559e13c4b1e1046a3c5afd14',invitationCode:''};
var profile3 = {userId: '559e13c4b1e1046a3c5afd14',groupCode:'BusC000300003',groupName:'XYZ',groupStatus:'84',groupDescription:'TEST-1',itemsData:[{itemCode:'RA-001'},{itemCode:'RA-002'}],sessionToken:'MyKXeUHD',invitationData:{}};








describe('Routing', function() {
  before(function(done) {
    done();
  });
  describe(postAPI, function() {
    it('should return error System error', function(done) {
   request(url)
	.post(postAPI)
	.send(profile)
  .end(function(err, res) {
          if (err) {
            throw err;
          }
          res.body.responseCode.should.equal(6);
          done();
        });
    });
    it('should return Business not found', function(done){
  	request(url)
		.post(postAPI)
		.send(profile1)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
 it('should return Success', function(done){
	request(url)
		.post(postAPI)
		.send(profile3)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});


