var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
var config = require('./config');

var postAPI = 'getInvoices';
var profile = {        };
var profile1 = {businessId: '55a8d9c3076c1f8c721c8888',};
var profile2 = {businessId: '563ef083be12ca4705c9dd77',customerId:''};
var profile3 = {businessId: '563ef083be12ca4705c9dd77',customerId:'tempo000001',waybillId:'WWW123',trackingId:'TTT123'};








describe('Routing', function() {
  before(function(done) {
    done();
  });
  describe(postAPI, function() {
    it('should return error System error', function(done) {
   request(url)
	.post(postAPI)
	.send(profile)
  .end(function(err, res) {
          if (err) {
            throw err;
          }
          res.body.responseCode.should.equal(6);
          done();
        });
    });
    it('should return Business not found', function(done){
  	request(url)
		.post(postAPI)
		.send(profile1)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
 it('should return Success', function(done){
	request(url)
		.post(postAPI)
		.send(profile3)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});





