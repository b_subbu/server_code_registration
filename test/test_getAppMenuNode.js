var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
//var config = require('./config-debug');
var config = require('./config');

describe('Routing', function() {
  before(function(done) {
    done();
  });
  // use describe to give a title to your test suite, the tile 
  describe('getAppMenuNode', function() {
    it('should return System error', function(done) {
      var profile = {
        };
   request(url)
	.post('getAppMenuNode')
	.send(profile)
	.end(function(err, res) {
          if (err) {
            throw err;
          }
          res.body.responseCode.should.equal(6);
          done();
        });
    });
  it('should return System error', function(done) {
      var profile = {
        customerId:''
        };
   request(url)
	.post('getAppMenuNode')
	.send(profile)
	.end(function(err, res) {
          if (err) {
            throw err;
          }
          res.body.responseCode.should.equal(6);
          done();
        });
    });

    it('should return Customer not found', function(done){
	var profile = {
    customerId: '',
    menuCode: 5, 
    businessId: '55a8d9c3076c1f8c721c8888' 
	};
	request(url)
		.post('getAppMenuNode')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
  it('should return Customer not Active', function(done){
	var profile = {
    customerId: 'JusC0001000052',
    businessId: '55a8d9c3076c1f8c721c8888',
    menuCode: 5 
	};
	request(url)
		.post('getAppMenuNode')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(3);
  			done();
		});
	});
 it('should return No Menus found', function(done){
	var profile = {
    customerId: 'JusC0001000058',
    businessId: '55a8d9c3076c1f8c721c8888',
    menuCode: 5 
	};
	request(url)
		.post('getAppMenuNode')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(4);
  			done();
		});
	});
  
it('should return Success', function(done){
	var profile = {
    customerId: 'JusC0001000058',
    businessId: '559e13c4b1e1046a3c5afd14',
    menuCode: 3,
    nodeId: 4 
	};
	request(url)
		.post('getAppMenuNode')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});


