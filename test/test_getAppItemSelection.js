var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
//var config = require('./config-debug');
var config = require('./config');

describe('Routing', function() {
  before(function(done) {
    // In our tests we use the test db
    //mongoose.connect(config.db.mongodb);							
    done();
  });
  // use describe to give a title to your test suite, the tile 
  describe('getAppItemSelection', function() {
    it('should return System error', function(done) {
      var profile = {
        };
   request(url)
	.post('getAppItemSelection')
	.send(profile)
	.end(function(err, res) {
          if (err) {
            throw err;
          }
          // this is should.js syntax, very clear
          res.body.responseCode.should.equal(6);
          done();
        });
    });
  it('should return System error', function(done) {
      var profile = {
        customerId:''
        };
   request(url)
	.post('getAppItemSelection')
	.send(profile)
	.end(function(err, res) {
          if (err) {
            throw err;
          }
          // this is should.js syntax, very clear
          res.body.responseCode.should.equal(6);
          done();
        });
    });
 it('should return System Error', function(done){
	var profile = {
    customerId: '',
    itemCode: 5, 
    businessId: '55a8d9c3076c1f8c721c8888' 
	};
	request(url)
		.post('getAppItemSelection')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(6);
  			done();
		});
	});

    it('should return Customer not found', function(done){
	var profile = {
    customerId: '',
    itemCode: 5,
    searchCriteria:{}, 
    businessId: '55a8d9c3076c1f8c721c8888' 
	};
	request(url)
		.post('getAppItemSelection')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
  it('should return Customer not Active', function(done){
	var profile = {
    customerId: 'JusC0001000052',
    businessId: '55a8d9c3076c1f8c721c8888',
    searchCriteria: {},
    itemCode: 5 
	};
	request(url)
		.post('getAppItemSelection')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(3);
  			done();
		});
	});
 it('should return No Item found', function(done){
	var profile = {
    customerId: 'JusC0001000058',
    businessId: '55a8d9c3076c1f8c721c8888',
    searchCriteria: {},
    itemCode: 5 
	};
	request(url)
		.post('getAppItemSelection')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(4);
  			done();
		});
	});
  
it('should return Success', function(done){
	var profile = {
    customerId: 'JusC0001000058',
    businessId: '559e13c4b1e1046a3c5afd14',
    searchCriteria: {color: 'Green'},
    itemCode: 'RA-16' 
	};
	request(url)
		.post('getAppItemSelection')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});



