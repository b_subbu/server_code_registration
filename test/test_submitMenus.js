var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
//var config = require('./config-debug');
var config = require('./config');

describe('Routing', function() {
  before(function(done) {
    // In our tests we use the test db
    //mongoose.connect(config.db.mongodb);							
    done();
  });
  // use describe to give a title to your test suite, the tile 
  describe('submitMenus', function() {
    it('should return System error', function(done) {
      var profile = {
        };
   request(url)
	.post('submitMenus')
	.send(profile)
	.end(function(err, res) {
          if (err) {
            throw err;
          }
          // this is should.js syntax, very clear
          res.body.responseCode.should.equal(6);
          done();
        });
    });
 it('should return No Menus found', function(done){
	var profile = {
    userId: '55a8d9c3076c1f8c721c8888',
    menuCodes: ["5"] 
	};
	request(url)
		.post('submitMenus')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(3);
  			done();
		});
	});
  
it('should return Success', function(done){
	var profile = {
    userId: '55a8d9c3076c1f8c721c8888',
    menuCodes: ["1"] 
	};
	request(url)
		.post('submitMenus')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});

