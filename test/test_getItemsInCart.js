var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
var config = require('./config');

var postAPI = 'customer/app/getItemsInCart';
var profile = {        };
var profile1 = {customerId: '',businessId: '55a8d9c3076c1f8c721c8888',itemCode: ''};
var profile2 = {customerId: 'JusC0001000052',businessId: '559e13c4b1e1046a3c5afd14'};
var profile3 = {customerId: 'JusC0001000058',businessId: '559e13c4b1e1046a3c5afd14'};








describe('Routing', function() {
  before(function(done) {
    done();
  });
  describe('changeItemQtyInCart', function() {
    it('should return error System error', function(done) {
   request(url)
	.post(postAPI)
	.send(profile)
  .end(function(err, res) {
          if (err) {
            throw err;
          }
          res.body.responseCode.should.equal(6);
          done();
        });
    });
    it('should return Customer not found', function(done){
  	request(url)
		.post(postAPI)
		.send(profile1)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
  it('should return Customer not Active', function(done){
	   request(url)
		.post(postAPI)
		.send(profile2)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
		  res.body.responseCode.should.equal(3);
  		done();
		});
	});
it('should return Success', function(done){
	request(url)
		.post(postAPI)
		.send(profile3)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});

