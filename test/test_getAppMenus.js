var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  
//var config = require('./config-debug');
var config = require('./config');

describe('Routing', function() {
  //var url = 'http://ec2-54-149-103-9.us-west-2.compute.amazonaws.com:3000/'; //DEV
  //var url = 'http://localhost:3000/'
  // within before() you can run all the operations that are needed to setup your tests. In this case
  // I want to create a connection with the database, and when I'm done, I call done().
  before(function(done) {
    // In our tests we use the test db
    //mongoose.connect(config.db.mongodb);							
    done();
  });
  // use describe to give a title to your test suite, the tile 
  // and then specify a function in which we are going to declare all the tests
  // we want to run. Each test starts with the function it() and as a first argument 
  // we have to provide a meaningful title for it, whereas as the second argument we
  // specify a function that takes a single parameter, "done", that we will use 
  // to specify when our test is completed, and that's what makes easy
  // to perform async test!
  describe('getAppMenus', function() {
    it('should return error System error', function(done) {
      var profile = {
        };
    // once we have specified the info we want to send to the server via POST verb,
    // we need to actually perform the action on the resource, in this case we want to 
    // We do this using the request object, requiring supertest!
  request(url)
	.post('getAppMenus')
	.send(profile)
    // end handles the response
	.end(function(err, res) {
   //console.log(res.status);
   //console.log(err);
          if (err) {
            throw err;
          }
          // this is should.js syntax, very clear
          res.body.responseCode.should.equal(6);
          done();
        });
    });
    it('should return Customer not found', function(done){
	var profile = {
    customerId: '',
    businessId: '55a8d9c3076c1f8c721c8888' 
	};
	request(url)
		.post('getAppMenus')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(2);
  			done();
		});
	});
  it('should return Customer not Active', function(done){
	var profile = {
    customerId: 'JusC0001000052',
    businessId: '55a8d9c3076c1f8c721c8888' 
	};
	request(url)
		.post('getAppMenus')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(3);
  			done();
		});
	});
 it('should return No Menus found', function(done){
	var profile = {
    customerId: 'JusC0001000058',
    businessId: '55a8d9c3076c1f8c721c8888' 
	};
	request(url)
		.post('getAppMenus')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(4);
  			done();
		});
	});
  
it('should return Success', function(done){
	var profile = {
    customerId: 'JusC0001000058',
    businessId: '559e13c4b1e1046a3c5afd14' 
	};
	request(url)
		.post('getAppMenus')
		.send(profile)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err,res) {
    	if (err) {
				throw err;
			}
			// Should.js fluent syntax applied
			  res.body.responseCode.should.equal(9);
  			done();
		});
	});
  
  });
});
