<<<<<<< HEAD
/**
 *File: app.js
 *Author: Subu
 *Date: 18-May-2015
 *Desc: Sets project wide dependcies and configurations
 */
     
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var router = express.Router();
var cors = require('cors');
var favicon = require('serve-favicon'); 
var debug = require('debug'); 
var monk = require('monk');

var mongo = require('mongoskin');
db = require('./dbconn');

  var routes = require('./routes/index');
 
  var cluster = require('cluster');
 
   if(cluster.isMaster){
      // Count the machine's CPUs  restrict to just 1 workers else this consumes too much memory
      var cpuCount = require('os').cpus().length;
      for (var i = 0; i < 1; i += 1) {
         cluster.fork();
      }
    
    cluster.on('exit', function (worker) {
   
    // Replace the dead worker,
    console.log('Worker ' + worker.id + ' died :(');
    cluster.fork();
    });
   }
 else { 
 
  app = express();

  app.set('port', process.env.PORT || 3000);
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.set('controller',path.join(__dirname,'controller/'));
  app.set('model',path.join(__dirname,'model/'));
  app.set('views', __dirname + '/views');
  app.set('fileuploadfolder',__dirname+'/public/upload_files/');
  app.set('publicfolder',__dirname+'/public/');
 
  app.set('env','development');
  
  app.use(logger('dev'));
  app.use(bodyParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded());

  app.use(cookieParser('scarab_123'));
  app.use(express.static(path.join(__dirname, 'public')));
  app.options('*',cors());
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
   });
   

  app.use(favicon(__dirname + '/public/favicon.ico'));  
  app.get('/', routes.initial);
  app.post('/business/web/checkUser',routes.checkuser);
  app.post('/business/web/generateRegistration',routes.generateregistration);
  app.post('/business/web/verifyRegistration',routes.verifyregistration);
  app.post('/business/web/subscribeUser',routes.subscribeuser);
  app.post('/business/web/checkSubscription',routes.checksubscript);
  app.post('/business/web/loginUser',routes.loginuser);
  app.post('/business/web/resetPassword',routes.resetpassword);
  app.post('/business/web/forgotPassword',routes.forgotpassword);
  app.post('/business/web/clearPassword',routes.clearpassword);
  app.post('/business/web/saveSubscription',routes.savesubscription);
  app.post('/business/web/logoutUser',routes.logoutuser);
 
  app.post('/business/web/getBusinessProfile',routes.businesswebcontroller);
  app.post('/business/web/saveBusinessProfile',routes.businesswebcontroller);
  
  
  app.post('/business/web/getPaymentGateway',routes.businesswebcontroller);
  app.post('/business/web/createPaymentGateway',routes.businesswebcontroller);
  app.post('/business/web/editPaymentGateway',routes.businesswebcontroller);
  app.post('/business/web/authorisePaymentGateways',routes.businesswebcontroller);
  app.post('/business/web/rejectPaymentGateways',routes.businesswebcontroller);
  app.post('/business/web/releasePaymentGateway',routes.businesswebcontroller);
  app.post('/business/web/recallPaymentGateway',routes.businesswebcontroller);
  app.post('/business/web/deletePaymentGateways',routes.businesswebcontroller);
  app.post('/business/web/submitPaymentGateways',routes.businesswebcontroller);
  
  app.post('/business/web/getDeliveryOption',routes.businesswebcontroller);
  app.post('/business/web/createDeliveryOption',routes.businesswebcontroller);
  app.post('/business/web/editDeliveryOption',routes.businesswebcontroller);
  app.post('/business/web/authoriseDeliveryOptions',routes.businesswebcontroller);
  app.post('/business/web/rejectDeliveryOptions',routes.businesswebcontroller);
  app.post('/business/web/submitDeliveryOptions',routes.businesswebcontroller);
  app.post('/business/web/deleteDeliveryOptions',routes.businesswebcontroller);
   
  app.post('/business/web/getSeller',routes.businesswebcontroller);
  app.post('/business/web/createSeller',routes.businesswebcontroller);
  app.post('/business/web/editSeller',routes.businesswebcontroller);
  app.post('/business/web/authoriseSellers',routes.businesswebcontroller);
  app.post('/business/web/rejectSellers',routes.businesswebcontroller);
  app.post('/business/web/submitSellers',routes.businesswebcontroller);
  app.post('/business/web/deleteSellers',routes.businesswebcontroller);

  app.post('/business/web/getInvoiceConfig',routes.businesswebcontroller);
  app.post('/business/web/createInvoiceConfig',routes.businesswebcontroller);
  app.post('/business/web/editInvoiceConfig',routes.businesswebcontroller);
  app.post('/business/web/authoriseInvoiceConfigs',routes.businesswebcontroller);
  app.post('/business/web/rejectInvoiceConfigs',routes.businesswebcontroller);
  app.post('/business/web/submitInvoiceConfigs',routes.businesswebcontroller);
  app.post('/business/web/deleteInvoiceConfigs',routes.businesswebcontroller);


  app.post('/business/web/getTaxType',routes.businesswebcontroller);
  app.post('/business/web/createTaxType',routes.businesswebcontroller);
  app.post('/business/web/editTaxType',routes.businesswebcontroller);
  app.post('/business/web/authoriseTaxTypes',routes.businesswebcontroller);
  app.post('/business/web/rejectTaxTypes',routes.businesswebcontroller);
  app.post('/business/web/submitTaxTypes',routes.businesswebcontroller);
  app.post('/business/web/deleteTaxTypes',routes.businesswebcontroller);

  app.post('/business/web/getCharge',routes.businesswebcontroller);
  app.post('/business/web/createCharge',routes.businesswebcontroller);
  app.post('/business/web/editCharge',routes.businesswebcontroller);
  app.post('/business/web/authoriseCharges',routes.businesswebcontroller);
  app.post('/business/web/rejectCharges',routes.businesswebcontroller);
  app.post('/business/web/submitCharges',routes.businesswebcontroller);
  app.post('/business/web/deleteCharges',routes.businesswebcontroller);
  
  app.post('/business/web/getRole',routes.businesswebcontroller);
  app.post('/business/web/createRole',routes.businesswebcontroller);
  app.post('/business/web/editRole',routes.businesswebcontroller);
  app.post('/business/web/authoriseRoles',routes.businesswebcontroller);
  app.post('/business/web/deleteRoles',routes.businesswebcontroller);
  app.post('/business/web/rejectRoles',routes.businesswebcontroller);
  app.post('/business/web/submitRoles',routes.businesswebcontroller);


  app.post('/business/web/getUserModules',routes.businesswebcontroller);
  app.post('/business/web/getProductDetails',routes.businesswebcontroller);
  app.post('/business/web/getProductData',routes.businesswebcontroller);
  app.post('/business/web/getProductHeader',routes.businesswebcontroller);
  
  app.post('/business/web/getAlertMasterData',routes.businesswebcontroller);
  app.post('/business/web/saveAlert',routes.businesswebcontroller);
  app.post('/business/web/editAlert',routes.businesswebcontroller);
  app.post('/business/web/submitAlert',routes.businesswebcontroller);
  app.post('/business/web/authoriseAlert',routes.businesswebcontroller);
  app.post('/business/web/rejectAlert',routes.businesswebcontroller);
  app.post('/business/web/deleteAlert',routes.businesswebcontroller);
  app.post('/business/web/releaseAlert',routes.businesswebcontroller);
  app.post('/business/web/recallAlert',routes.businesswebcontroller);
  app.post('/business/web/saveLearnAlertImage',routes.businesswebcontroller);
  
  app.post('/business/web/getMenuItem',routes.businesswebcontroller);
  app.post('/business/web/createMenuItemDetails',routes.businesswebcontroller);
  app.post('/business/web/editMenuItemDetails',routes.businesswebcontroller);
  app.post('/business/web/deleteMenuItems',routes.businesswebcontroller);
  app.post('/business/web/createMenuItemVisuals',routes.businesswebcontroller);
  app.post('/business/web/editMenuItemVisuals',routes.businesswebcontroller);
  app.post('/business/web/approveMenuItems',routes.businesswebcontroller);
  app.post('/business/web/rejectMenuItems',routes.businesswebcontroller);
  app.post('/business/web/getItemMetaData',routes.businesswebcontroller);
  app.post('/business/web/getMenuItemsDetails',routes.businesswebcontroller);

  app.post('/business/web/getMenu',routes.businesswebcontroller); 
  app.post('/business/web/createMenu',routes.businesswebcontroller); 
  app.post('/business/web/editMenu',routes.businesswebcontroller); 
  app.post('/business/web/authoriseMenus',routes.businesswebcontroller);
  app.post('/business/web/rejectMenus',routes.businesswebcontroller);
  app.post('/business/web/releaseMenus',routes.businesswebcontroller);
  app.post('/business/web/recallMenus',routes.businesswebcontroller);
  app.post('/business/web/deleteMenus',routes.businesswebcontroller);
  app.post('/business/web/submitMenus',routes.businesswebcontroller);

  app.post('/business/web/getOrder',routes.businesswebcontroller);
  app.post('/business/web/checkOut',routes.businesswebcontroller);
  app.post('/business/web/checkIn',routes.businesswebcontroller);
  app.post('/business/web/ship',routes.businesswebcontroller);
  app.post('/business/web/deliver',routes.businesswebcontroller)
 
  app.post('/business/web/getInvitation',routes.businesswebcontroller);
  app.post('/business/web/createInvitation',routes.businesswebcontroller);
  app.post('/business/web/editInvitation',routes.businesswebcontroller);
  app.post('/business/web/submitInvitations',routes.businesswebcontroller);
  app.post('/business/web/authoriseInvitations',routes.businesswebcontroller);
  app.post('/business/web/rejectInvitations',routes.businesswebcontroller);
  app.post('/business/web/deleteInvitations',routes.businesswebcontroller);
  app.post('/business/web/releaseInvitations',routes.businesswebcontroller);

  app.post('/business/web/getGroup',routes.businesswebcontroller);
  app.post('/business/web/createGroup',routes.businesswebcontroller);
  app.post('/business/web/editGroup',routes.businesswebcontroller);
  app.post('/business/web/deleteGroups',routes.businesswebcontroller);
  app.post('/business/web/approveGroups',routes.businesswebcontroller);
  app.post('/business/web/rejectGroups',routes.businesswebcontroller);
  
  app.post('/business/web/getUser',routes.businesswebcontroller);
  app.post('/business/web/createUser',routes.businesswebcontroller);
  app.post('/business/web/editUser',routes.businesswebcontroller);
  app.post('/business/web/authoriseUsers',routes.businesswebcontroller);
  app.post('/business/web/rejectUsers',routes.businesswebcontroller);
  app.post('/business/web/submitUsers',routes.businesswebcontroller);
  app.post('/business/web/deleteUsers',routes.businesswebcontroller);
  app.post('/business/web/inviteUsers',routes.businesswebcontroller);
  app.post('/business/web/uninviteUsers',routes.businesswebcontroller);
  app.post('/business/web/deactivateUsers',routes.businesswebcontroller);
  

  app.post('/business/web/getAuditTrail',routes.businesswebcontroller);
  app.post('/business/web/getWorkQueueItems',routes.businesswebcontroller);
   
  //customerApp services from below---->
  
  app.post('/customer/app/registerCustomer',routes.customerappcontroller);
  app.post('/customer/app/loginCustomer',routes.customerappcontroller);
  app.post('/customer/app/getBusinessTypes',routes.customerappcontroller);
  app.post('/customer/app/getBusinessesForType',routes.customerappcontroller);
  app.post('/customer/app/followBusiness',routes.customerappcontroller);
  app.post('/customer/app/unfollowBusiness',routes.customerappcontroller);
  app.post('/customer/app/getAlerts',routes.customerappcontroller);
  app.post('/customer/app/getAlertsForBusiness',routes.customerappcontroller);
  app.post('/customer/app/getAlertLinkContent',routes.customerappcontroller);
  
  app.post('/customer/app/getMenus',routes.customerappcontroller);
  app.post('/customer/app/getMenu',routes.customerappcontroller);
  app.post('/customer/app/getMenuItem',routes.customerappcontroller);
  app.post('/customer/app/getMenuItemSelection',routes.customerappcontroller);
  app.post('/customer/app/getMenuLinkageForNode',routes.customerappcontroller);
  
  app.post('/customer/app/addToCart',routes.customerappcontroller);
  app.post('/customer/app/removeFromCart',routes.customerappcontroller);
  app.post('/customer/app/changeItemQtyInCart',routes.customerappcontroller);
  app.post('/customer/app/getItemsInCart',routes.customerappcontroller);
  app.post('/customer/app/getCartItemsCount',routes.customerappcontroller);
  app.post('/customer/app/getOrderSummary',routes.customerappcontroller);
  
  app.post('/customer/app/checkout',routes.checkout);
  app.post('/sucessPayUPayment',routes.sucesspayupayment);
  app.post('/failurePayUPayment',routes.failurepayupayment);
  app.post('/cancelPayUPayment',routes.cancelpayupayment);
   
  app.get('/customer/app/downloadAPK',routes.customerappcontroller);
  
  app.post('/customer/app/getGroup',routes.customerappcontroller);
  
 
  //-->These are only for test can be removed later 
  app.post('/testPaymentGateway',routes.testpaymentgateway);
  app.post('/testsucessPayUPayment',routes.testsucesspayupayment);
  app.post('/testfailurePayUPayment',routes.testfailurepayupayment);
  app.post('/testcancelPayUPayment',routes.testcancelpayupayment);
  
  app.post('/testNotification',routes.testnotification);
  app.post('/testSMSGateway',routes.testsmsgateway);
  
  app.post('/createTestData',routes.testdata);
  app.post('/createReleaseTestData',routes.releasetestdata);
  app.post('/dumpDB',routes.dumpdb);
  
  app.post('/testPDF',routes.testPDF);
  


/// catch 404 and forward to error handler
   app.use(function(req, res, next) {
     var err = new Error('Not Found');
     err.status = 404;
     next(err);
    });

/// error handlers

// development error handler
// will print stacktrace
   if (app.get('env') === 'development') {
      app.use(function(err, req, res, next) {
          console.log(err);
          res.status(err.status || 500).jsonp({status:err.status,error:err.message,stack:err.stack});
       });
}

// production error handler
// no stacktraces leaked to user
    app.use(function(err, req, res, next) {
       res.status(err.status || 500).jsonp({error:"System Error Please try later"});
     });



  module.exports = app;

  app.listen(app.get('port'), function(){
      console.log("Express server listening on port " + app.get('port')+" on Worker:"+cluster.worker.id);
  });
 

 //-->For implementing notification services
  var schedule = require('node-schedule');
   
   var j = schedule.scheduleJob('*/20 * * * *',function(){
     debuglog("Starting Notification Job");
     var model = app.get('model');
     var x = require(model+'Notification/sendNotificationCron');
     x.execute();
     debuglog("Notification Job ended");
  });   
  
=======
/**
 *File: app.js
 *Author: Subu
 *Date: 18-May-2015
 *Desc: Sets project wide dependcies and configurations
 */
     
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var router = express.Router();
var cors = require('cors');
var favicon = require('serve-favicon'); 
var debug = require('debug'); 

var mongo = require('mongoskin');
   //db = mongo.db('mongodb://scarabAdmin:scarab123@localhost:27017/scarabDB',{safe: true});
   //use monk for better results
   //now no driver seems to be available for mongodb 3.0 use with noauth
   //until we get new driver
   
   if(process.env.OS == 'Windows_NT') {   //local machine
          db = require('monk')('localhost:27017/scarabDB', {
                  user:'scarabAdmin',
                  pwd:'scarab123',
                  authenticationDatabase:'scarabDB'
                  });
          DEBUG = new debug();        
    }
    else {
        db = require('monk') ('ec2-52-25-195-79.us-west-2.compute.amazonaws.com:27017/scarabDB', {
                  user:'scarabAdmin',
                  pwd:'scarab123',
                  authenticationDatabase:'scarabDB'
                  });
   }
    
  var routes = require('./routes/index');

  var cluster = require('cluster');
 
   if(cluster.isMaster){
      // Count the machine's CPUs
      var cpuCount = require('os').cpus().length;
      for (var i = 0; i < cpuCount; i += 1) {
         cluster.fork();
      }
    
    cluster.on('exit', function (worker) {
   
    // Replace the dead worker,
    console.log('Worker ' + worker.id + ' died :(');
    cluster.fork();
    });
   }
 else { 
 
  app = express();

  app.set('port', process.env.PORT || 3000);
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.set('controller',path.join(__dirname,'controller/'));
  app.set('model',path.join(__dirname,'model/'));
  app.set('views', __dirname + '/views');
  app.set('fileuploadfolder',__dirname+'/public/upload_files/')
 
  app.set('env','development');
  
  app.use(logger('dev'));
  app.use(bodyParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded());

  app.use(cookieParser('scarab_123'));
  //app.use(express.session());
  app.use(express.static(path.join(__dirname, 'public')));
  app.options('*',cors());
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
   });

  app.use(favicon(__dirname + '/public/favicon.ico'));  
  app.get('/', routes.initial);
  app.post('/checkUser',routes.checkuser);
  app.post('/generateRegistration',routes.generateregistration);
  app.post('/verifyRegistration',routes.verifyregistration);
  app.post('/subscribeUser',routes.subscribeuser);
  app.post('/checkSubscription',routes.checksubscript);
  app.post('/loginUser',routes.loginuser);
  app.post('/resetPassword',routes.resetpassword);
  app.post('/forgotPassword',routes.forgotpassword);
  app.post('/clearPassword',routes.clearpassword);
  app.post('/userProfile',routes.userprofile);
  app.post('/saveSubscription',routes.savesubscription);
  
  app.post('/getUserModules',routes.getusermodules);
  app.post('/getProductDetails',routes.getproductdetails);
  app.post('/getProductData',routes.getproductdata);
  
  app.post('/getAlertMasterData',routes.createalert);
  app.post('/saveAlert',routes.savealert);
  app.post('/editAlert',routes.editalert);
  app.post('/submitAlert',routes.submitalert);
  app.post('/authoriseAlert',routes.authorisealert);
  app.post('/rejectAlert',routes.rejectalert);
  app.post('/deleteAlert',routes.deletealert);
  app.post('/releaseAlert',routes.releasealert);
  app.post('/recallAlert',routes.recallalert);
  
  app.post('/deleteUser',routes.deleteuser);
  
  app.post('/searchData',routes.searchdata);
  
  app.post('/createTestData',routes.testdata);

/// catch 404 and forward to error handler
   app.use(function(req, res, next) {
     var err = new Error('Not Found');
     err.status = 404;
     next(err);
    });

/// error handlers

// development error handler
// will print stacktrace
   if (app.get('env') === 'development') {
      app.use(function(err, req, res, next) {
          console.log(err);
          res.status(err.status || 500).jsonp({status:err.status,error:err.message,stack:err.stack});
       });
}

// production error handler
// no stacktraces leaked to user
    app.use(function(err, req, res, next) {
       res.status(err.status || 500).jsonp({error:"System Error Please try later"});
     });



  module.exports = app;

  app.listen(app.get('port'), function(){
      console.log("Express server listening on port " + app.get('port')+" on Worker:"+cluster.worker.id);
  });
    
>>>>>>> DEV/master
}