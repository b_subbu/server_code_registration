conn = new Mongo();
db = conn.getDB('scarabDB');

db.user_detail.drop();


db.createCollection("user_detail");

db.user_detail.update(
  {},
  { $set: {user_id: String},
    $set: {bus_name: String},
    $set: {bus_addr: String},
    $set: {bus_email: String},
    $set: {bus_mobile: String},
    $set: {bus_type: Number},
    $set: {bus_branch: String},
    $set: {bus_url: String},
    $set: {bus_logo: String},
    $set: {bus_descr: String},
    $set: {org_id: String},
    $set: {pass_word: String},
    $set: {regist_date: Date},
    $set: {activ_date: Date},
    $set: {new_pass: Boolean},
    $set: {country_id: Array},
    $set: {state_id: Array},
    $set: {district_id: Array},
    $set: {city_id: String} 
    
  },
  { multi: true}

);


db.user_detail.createIndex({ user_id:1},{unique: true});



db.user_detail.update(
  {},
  { $set: {user_status: Number},
        
  },
  { multi: true}

);

