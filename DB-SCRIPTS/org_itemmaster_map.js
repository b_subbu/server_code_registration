conn = new Mongo();
db = conn.getDB('scarabDB');



db.org_itemmaster_map.drop();

db.createCollection("org_itemmaster_map");

db.org_itemmaster_map.update(
  {},
  { 
    $set: {org_id: String},
    $set: {bus_type: String},
    $set: {table_name_master: String},
    $set: {field_name: String},
    $set: {field_label: String},
    $set: {field_order: Number},
    $set: {mandatory: Boolean}
    },
  { multi: true}

);


db.org_itemmaster_map.createIndex({ org_id:1,bus_type:1});
