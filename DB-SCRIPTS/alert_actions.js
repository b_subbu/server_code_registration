//use db available only from RoboMonog
//Integer is not available in Mongo shell only Number
conn = new Mongo();
db = conn.getDB('scarabDB');

db.alert_actions.drop();

db.createCollection("alert_actions");

db.alert_actions.update(
  {},
  { $set: {alert_id: String},
    $set: {action_type_id: Number},
    $set: {create_date: Date}
  },
  { multi: true}

);


db.alert_actions.createIndex({ alert_id:1});
