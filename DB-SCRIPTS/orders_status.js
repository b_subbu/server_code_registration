conn = new Mongo();
db = conn.getDB('scarabDB');



db.orders_status.drop();

db.createCollection("orders_status");

db.orders_status.update(
  {},
  { 
    $set:{org_id: String},
    $set: {business_type:String},
    $set:{order_id: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: new ISODate()}
    },
  { multi: true}

);
