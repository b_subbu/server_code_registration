conn = new Mongo();
db = conn.getDB('scarabDB');

db.groups_status.drop();

db.createCollection("groups_status");

db.groups_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {groupCode: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);

