conn = new Mongo();
db = conn.getDB('scarabDB');

db.cust_ship_details.drop();


db.createCollection("cust_ship_details");

db.cust_ship_details.update(
  {},
  { $set: {customer_id: String},
    $set: {firstName: String},
    $set: {lastName: String},
    $set: {address: String},
    $set: {city: String},
    $set: {state: String},
    $set: {country: String},
    $set: {zipCode: String},
    $set: {phone: String},
    $set: {create_date: Date}
    },
  { multi: true}

);


//db.cust_ship_details.createIndex({ customer_id:1},{unique:true});


