conn = new Mongo();
db = conn.getDB('scarabDB');

db.cust_master.drop();



db.createCollection("cust_master");

db.cust_master.update(
  {},
  { 
    $set: {cust_id: String},
    $set: {cust_name: String},
    $set: {cust_email: String},
    $set: {pass_word: String},
    $set: {invitation_code: String},
    $set: {register_date: String},
    $set: {os_version: String},
    $set: {os_api_level: String},
    $set: {cust_device: String},
    $set: {cust_model: String},
    $set: {cust_product: String},
    $set: {device_manufacturer: String},
    $set: {cust_active: Boolean}
  },
  { multi: true}

);


db.cust_master.createIndex({ cust_id:1});


