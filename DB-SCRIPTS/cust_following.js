conn = new Mongo();
db = conn.getDB('scarabDB');

db.cust_following.drop();

db.createCollection("cust_following");

db.cust_following.update(
  {},
  { 
    $set: {cust_id: String},
    $set: {org_id: String},
    $set: {bus_type: String},
    $set: {biz_id: String},
    $set: {follow_date: Date}
  },
  { multi: true}

);

//Will add more data to following later like Geo location, prefernces etc..

db.cust_following.createIndex({ cust_id:1});

