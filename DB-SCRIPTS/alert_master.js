//use db available only from RoboMonog
//Integer is not available in Mongo shell only Number
conn = new Mongo();
db = conn.getDB('scarabDB');

db.alert_master.drop();

db.createCollection("alert_master");

db.alert_master.update(
  {},
  { 
    $set: {alert_id: String},
    $set: {user_id: String},
    $set: {org_id: String},
    $set: {title: String},
    $set: {descr: String},
    $set: {content_type_id: Number},
    $set: {alert_status_id: Number},
    $set: {content_target: Number},
    $set: {alert_life: Number},
    $set: {alert_type_id: Number},
    $set: {template_id: String},
    $set: {create_date: Date},
    $set: {update_date: Date}
  },
  { multi: true}

);


db.alert_master.createIndex({ org_id:1});
