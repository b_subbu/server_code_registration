conn = new Mongo();
db = conn.getDB('scarabDB');

db.invite_master.drop();


db.createCollection("invite_master");

db.invite_master.update(
  {},
  { 
    $set: {invitation_code: String},
    $set: {org_id: String},
    $set: {biz_id: String},
    $set: {bus_type: String},
    $set: {invite_date: Date}
  },
  { multi: true}
);


//Other validations like target, expiry may be added later
//invitation_code here must match in the url of invitation


db.invite_master.createIndex({ invitation_code:1});


