conn = new Mongo();
db = conn.getDB('scarabDB');


//NOT creating DB files from now on because it is same like below

db.menuitem_status.drop();

db.createCollection("menuitem_status");

db.menuitem_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {itemCode: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);


db.menuitem_status.createIndex({ org_id:1,bus_type:1, item_code:1});



