conn = new Mongo();
db = conn.getDB('scarabDB');

db.user_status.drop();

db.createCollection("user_status");

db.user_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {userId: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);


