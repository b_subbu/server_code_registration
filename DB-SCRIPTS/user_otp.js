conn = new Mongo();
db = conn.getDB('scarabDB');

db.user_otp.drop();


db.createCollection("user_otp");

db.user_otp.update(
  {},
  { $set: {user_id: String},
    $set: {email_otp: String},
    $set: {mobile_otp: String},
    $set: {num_attempts: Number},
    $set: {created_time: Date},
    $set: {url_code: String}
  },
  { multi: true}

);


db.user_otp.createIndex({ user_id:1},{unique: true});
db.user_otp.createIndex({ url_code:1},{unique: true});

