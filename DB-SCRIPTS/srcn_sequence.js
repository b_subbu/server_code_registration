conn = new Mongo();
db = conn.getDB('scarabDB');

db.srcn_sequence.drop();


db.createCollection("srcn_sequence");

db.srcn_sequence.update(
  {},
  { $set: {running_date: Date},
    $set: {running_sequence: String}
  },
  { multi: true}

);


db.srcn_sequence.createIndex({ running_date:1 },{unique: true});

