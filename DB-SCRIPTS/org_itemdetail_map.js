conn = new Mongo();
db = conn.getDB('scarabDB');



db.org_itemdetail_map.drop();

db.createCollection("org_itemdetail_map");

db.org_itemdetail_map.update(
  {},
  { 
    $set: {org_id: String},
    $set: {bus_type: String},
    $set: {table_name_detail: String},
    $set: {itemCode: String},
    $set: {field_name: String},
    $set: {field_label: String},
    $set: {field_order: Number},
    $set: {field_type: Number},
    $set: {field_additional_type: Number},
    $set: {field_visibility: Boolean},
    $set: {field_width: Number},
    $set: {field_purpose: String},
    $set: {mandatory: Boolean}
   },
  { multi: true}

);



//field_label may not be used just creating for future requirement


