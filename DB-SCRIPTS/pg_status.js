conn = new Mongo();
db = conn.getDB('scarabDB');

db.pg_status.drop();

db.createCollection("pg_status");

db.pg_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {pgId: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);
