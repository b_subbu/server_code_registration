conn = new Mongo();
db = conn.getDB('scarabDB');

db.charge_type_status.drop();

db.createCollection("charge_type_status");

db.charge_type_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {chargeId: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);
