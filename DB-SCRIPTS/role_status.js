conn = new Mongo();
db = conn.getDB('scarabDB');

db.role_status.drop();

db.createCollection("role_status");

db.role_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {roleId: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);

