conn = new Mongo();
db = conn.getDB('scarabDB');

db.alert_images.drop();

db.createCollection("alert_images");

db.alert_images.update(
  {},
  { $set: {alert_id: String},
    $set: {image_url: String},
    $set: {image_seq: Number},
    $set: {image_text: String},
    $set: {video_url: String}
  },
  { multi: true}

);


db.alert_images.createIndex({ alert_id:1});

