conn = new Mongo();
db = conn.getDB('scarabDB');

db.tax_type_status.drop();

db.createCollection("seller_type_status");

db.tax_type_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {sellerId: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);



