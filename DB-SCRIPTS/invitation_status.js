conn = new Mongo();
db = conn.getDB('scarabDB');



db.invitation_status.drop();

db.createCollection("invitation_status");

db.invitation_status.update(
  {},
  { 
    $set:{org_id: String},
    $set: {business_type:String},
    $set:{invitation_code: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: new ISODate()}
    },
  { multi: true}

);

