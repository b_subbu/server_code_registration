conn = new Mongo();
db = conn.getDB('scarabDB');

db.alert_release_arc.drop();

db.createCollection("alert_release_arc");

db.alert_release_arc.update(
  {},
  { 
    $set: {alert_id: String},
    $set: {bus_type: String},
    $set: {org_id: String},
    $set: {content_title: String},
    $set: {content_type_id: Number},
    $set: {alert_type_id: Number},
    $set: {image_url: String},
    $set: {video_url: String},
    $set: {image_text: String},
    $set: {release_date: String},
    $set: {recall_date: Date},
    $set: {recall_reason: String}
  },
  { multi: true}

);

db.alert_release_arc.update(
  {},
  { 
   $set: {descr: String},
  },
  { multi: true}

);

db.alert_release_arc.createIndex({ alert_id:1});


