conn = new Mongo();
db = conn.getDB('scarabDB');

db.delivery_option_status.drop();

db.createCollection("delivery_option_status");

db.delivery_option_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {deliveryId: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);

