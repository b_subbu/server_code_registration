conn = new Mongo();
db = conn.getDB('scarabDB');

db.shopping_cart.drop();


db.createCollection("shopping_cart");

db.shopping_cart.update(
  {},
  { $set: {business_id: String},
    $set: {org_id: String},
    $set: {customer_id: String},
    $set: {shopping_time: Date},
    $set: {itemCode: String},
    $set: {itemDetailCode: String},
    $set: {qty: Number},
    },
  { multi: true}

);


//db.shopping_cart.createIndex({ business_id:1});
//db.shopping_cart.createIndex({ customer_id:1});

//If need to create unique index then it should be on
//business_id+customer_id+itemCode+itemDetailCode
