conn = new Mongo();
db = conn.getDB('scarabDB');

db.user_role_link.drop();



db.createCollection("user_role_link");

db.user_role_link.update(
  {},
  { $set: {user_id: String},   //user_master user_id
    $set: {role_id: String}, //role_master role_id
    $set: {role_seq: Number},//sequence of role of given user
    $set: {created_date: Date}
  },
  { multi: true}

);


db.user_role_link.createIndex({ user_id:1, role_id: 1},{unique: true});


