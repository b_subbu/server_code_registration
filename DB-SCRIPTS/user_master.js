conn = new Mongo();
db = conn.getDB('scarabDB');

db.user_master.drop();


db.createCollection("user_master");

db.user_master.update(
  {},
  { $set: {email: String},
    $set: {mobile: Number},
    $set: {user_status: Number},
    $set: {logged_in: Boolean}
  },
  { multi: true}

);


db.user_master.createIndex({ email:1},{unique: true});

