conn = new Mongo();
db = conn.getDB('scarabDB');

db.invoice_config_status.drop();

db.createCollection("invoice_config_status");

db.invoice_config_status.update(
  {},
  { 
    $set: {org_id: String},
    $set: {business_type: String},
    $set: {invoiceConfigId: String},
    $set: {status_id: Number},
    $set: {reason: String},
    $set: {create_date: Date}
    },
  { multi: true}

);




