function handleRequest(req,res) {
   var url= require('url');
   var apis = require('./customerApiDefinitions');
   
   var urlpath = req.path;
     
   var apiname = urlpath.substring(14);
   
   debuglog("The input api request is");
   debuglog(urlpath);
   debuglog("And the api name is:");
   debuglog(apiname);
   
   debuglog("Now get the params for this api name such as");
   debuglog("Model folder, model name, sessiontoken check required");
   
   debuglog("Note the status messages here are hard-coded and not got from status_master table");
   debuglog("This is to slightly speed up response");
   debuglog("TODO remove status messages for invalid inactive business etc., later");
   
   apis.apiParam(apiname,function(err,result){
	 debuglog(err);
     debuglog("Result of getApiParam is:")	 ;
	 debuglog(result);
	 if(err){
	   debuglog("This api does not seem to have definition setup");
	   debuglog("So just send system error");
	   sendBackResponse(request,response,6,'System Error' );
     }
	 else{
		req.modelFolder = result.modelFolder;
		req.modelFile   = result.modelFile;
		req.sessionCheck = result.sessionCheck;
		
		if(req.sessionCheck == true){
               debuglog("This api requires orgChcek so call orgCheck and customercheck");
			   checkOrgActive(req,res);
			  }
	    else{
	         debuglog("This api does not require sessionchecking either it is multiple part") ;
			 debuglog("form-data or registration flow etc.,");
			 debuglog("So just call model");
			 var data = {};
			 callModel(req,res,data);
		}
	  }
    });
   
}  
   
   
function checkOrgActive(req,res){

  var totalRows = 0; 
  var data = {};
  
  if(typeof req.body.orgId == 'undefined'){
   debuglog("orgId must be present in all /business/web requests");
   sendBackResponse(req,res,1,'Invalid or Business ID not found in request' );
  }
 else {
	  var orgId = req.body.orgId;
	 debuglog("OrgId is present so check if that is active");
	 debuglog("i.e., the system user of the given orgId must be active to consider orgId is active");
	  var modelPath=app.get('model');
      var roleUtils = require(modelPath+'Roles/roleutils.js');
   
	 roleUtils.orgStatus(orgId,function(err,result){
		debuglog(err);
     debuglog("Result of org Check is:")	 ;
	 debuglog(result);
	 if(err){
	   debuglog("This api does not seem to have definition setup");
	   debuglog("So just send system error");
	   sendBackResponse(request,response,6,'System Error' );
     }
    else{
		if(result.errorCode != 0){
		   sendBackResponse(req,res,2,'Business not found or Inactive please contact Koi Admin' );
         }
		 else{
		     debuglog("orgId is fine so let us check userId");
		   data.orgId = orgId;
		   data.businessType = result.bizType;
		   checkCustomerActive(req,res,data);
    	 }
	 }
	});
 }
  
}


function checkCustomerActive(req,res,data){

  var totalRows = 0;
  
  if(typeof req.body.customerId == 'undefined'){
    debuglog("customerId must be present in all /business/web requests");
    sendBackResponse(req,res,3,'Invalid or Customer ID not found in request' );
   }
 else {
	 debuglog("CustomerId is present so check if that is active");
	 debuglog("We are calling roleutils");
	 debuglog("TODO change this to masterutils later");
	 debuglog("Also maynot need orgId returned from there anymore");
    var cid = req.body.customerId;
	var oid = req.body.orgId;
	var async = require('async');
	var modelPath = app.get('model');
	
    var roleutils = require(modelPath+'Render/utils');
    var isError = 0;
    data.customerId = cid;
	 
     async.parallel({
         usrStat: function(callback){roleutils.checkCustomerId(cid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
             sendBackResponse(req,res,4,'Either Customer Id is not found or is inactive. Please contact Admin' );
              isError = 1;
             }
          if(isError == 0) {
			  data.customerEmail = results.usrStat.customerEmail;
              callModel(req,res,data);
			 }
          }); 
        }
}
 
function callModel(req,res,data) {
    var url= require('url');
    var modelPath = app.get('model');
	var modelFile = req.modelFolder+'/'+req.modelFile;
    var model = require(modelPath+modelFile);
	debuglog("Before calling Model");
	debuglog(data);
   
    model.getData(req,res,data);
} 

function handleStatus(request,response,stat,data) { 
    var modelPath=app.get('model');
    var model = require(modelPath+'getStatus');
    model.getData(request,response,request.modelFolder,request.modelFile,stat,data);
}  

function handleResponse(request,response,status,message,data) {
   //response.type('json');
   response.send({responseCode:status,message:message,data:data});
}

function sendBackResponse(request,response,status,message) {
   //response.type('json');
   response.send({responseCode:status,message:message});
}

exports.execute = handleRequest;
exports.process_status = handleStatus;
exports.process_return = handleResponse;




