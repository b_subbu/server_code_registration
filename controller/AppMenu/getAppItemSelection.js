function handleRequest(request,response) {
    var url= require('url');
    var modelpath = app.get('model');
    var model = require(modelpath+'AppMenu/getAppItemSelection');
   
    model.getData(request,response);
} 
    
function handleStatus(request,response,stat,data) { 
    var modelpath=app.get('model');
    var model = require(modelpath+'getStatus');
    model.getData(request,response,'AppMenu','getAppItemSelection',stat,data);}  

function handleResponse(request,response,status,message,data) {
   //response.type('json');

   response.send({responseCode:status,message:message,data:data});
}

exports.execute = handleRequest;
exports.process_status = handleStatus;
exports.process_return = handleResponse;
