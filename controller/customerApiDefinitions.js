function getApiParam(apiName,callback){
debuglog("This lists all the apiNames of customer app");
debuglog("The params are Model folder, model Name");

var isError = 0;
var data = {};
switch(apiName){
	
	case 'registerCustomer':
	case 'loginCustomer':
	case 'getBusinessTypes':
	case 'getBusinessesForType':
	 data={modelFolder:'Render',modelFile:apiName,sessionCheck:false};
	 break;
	 
	case 'followBusiness':
	case 'unfollowBusiness':
	case 'getAlertsForBusiness':
	case 'getAlertLinkContent':
     data={modelFolder:'Render',modelFile:apiName,sessionCheck:true};
	 break;
	 
	case 'getAlerts':
     data={modelFolder:'Render',modelFile:apiName,sessionCheck:false};
	 break;
	
	case 'getMenus':
	 data={modelFolder:'AppMenu',modelFile:'getAppMenus',sessionCheck:true};
	 break;
   
   case 'getMenu':
	 data={modelFolder:'AppMenu',modelFile:'getAppMenu',sessionCheck:true};
	 break;
	 
	case 'getMenuItem':
	 data={modelFolder:'AppMenu',modelFile:'getAppItem',sessionCheck:true};
	 break; 
	 
	 case 'getMenuItemSelection':
	 data={modelFolder:'AppMenu',modelFile:'getAppItemSelection',sessionCheck:true};
	 break;
	 
	 case 'getMenuLinkageForNode':
	 data={modelFolder:'AppMenu',modelFile:'getAppMenuNode',sessionCheck:true};
	 break;
	 
	 case 'addToCart':
	 case 'removeFromCart':
	 case 'changeItemQtyInCart':
	 case 'getItemsInCart':
	 case 'getCartItemsCount':
	 case 'getOrderSummary':
	  data={modelFolder:'Cart',modelFile:apiName,sessionCheck:true};
	  break;
	  
	 case 'downloadAPK/':
	   data={modelFolder:'Invitation',modelFile:'downloadAPK',sessionCheck:false};
	   break;
	  
	case 'getGroup':
        data={modelFolder:'Groups',modelFile:'getAppGroup',sessionCheck:true};
		break;
	 
	default:
     isError = 1;
     break;	 

}

if(isError == 1){
	
	debuglog("no definition exists for");
	debuglog(apiName);
	debuglog("so return error");
	callback(6,null);
}
else{
	debuglog("definition exists for api");
	debuglog(apiName);
	debuglog(data);
	callback(null,data);
}

}

exports.apiParam = getApiParam;