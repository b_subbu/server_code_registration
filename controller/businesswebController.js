function handleRequest(req,res) {
   var url= require('url');
   var apis = require('./businessApiDefinitions');
   
   var re = /\/business\/web\//;
  
   var urlpath = req.path;
     
   var apiname = urlpath.substring(14);
   
   debuglog("The input api request is");
   debuglog(urlpath);
   debuglog("And the api name is:");
   debuglog(apiname);
   
   debuglog("Now get the params for this api name such as");
   debuglog("Model folder, model name, sessiontoken check required");
   
   debuglog("Note the status messages here are hard-coded and not got from status_master table");
   debuglog("This is to slightly speed up response");
   debuglog("TODO remove status messages for invalid inactive business etc., later");
   
   apis.apiParam(apiname,function(err,result){
	 debuglog(err);
     debuglog("Result of getApiParam is:")	 ;
	 debuglog(result);
	 if(err){
	   debuglog("This api does not seem to have definition setup");
	   debuglog("So just send system error");
	   sendBackResponse(request,response,6,'System Error' );
     }
	 else{
		req.modelFolder = result.modelFolder;
		req.modelFile   = result.modelFile;
		req.sessionCheck = result.sessionCheck;
		
		if(req.sessionCheck == true){
               debuglog("This api requires sessionchecking so call sessioncheck");
			   debuglog("And all user check etc.,")
			   checkOrgActive(req,res);
			  }
	    else{
	         debuglog("This api does not require sessionchecking either it is multiple part") ;
			 debuglog("form-data or registration flow so there is no userid/orgid in request.body");
			 debuglog("So just call model and check userid there");
			 var data = {};
			 callModel(req,res,data);
		}
	  }
   });
   
}  
   
   
function checkOrgActive(req,res){

  var totalRows = 0; 
  
  if(typeof req.body.orgId == 'undefined'){
   debuglog("orgId must be present in all /business/web requests");
   sendBackResponse(req,res,1,'Invalid or Business ID not found in request' );
  }
 else {
	 debuglog("OrgId is present so check if that is active");
	 debuglog("i.e., the system user of the given orgId must be active to consider orgId is active");
     var orgId = req.body.orgId;	 
     var promise = db.get('user_master').find({$and:[{org_id:orgId},{system_user:true}]});
     promise.each(function(doc){
       totalRows++;
       orgActive = doc.user_status;
     });
     promise.on('complete',function(err,doc){
       if(totalRows == 0 || orgActive != 9) {
         sendBackResponse(req,res,2,'Business not found or Inactive please contact Koi Admin' );
        }
      else {
         debuglog("orgId is fine so let us check userId");
		 checkUserActive(req,res);
        }
     });
     promise.on('error',function(err){
      debuglog(err);
      sendBackResponse(req,res,6,'System Error' );
    });
  }
}


function checkUserActive(req,res){

  var totalRows = 0;
  var data = {}; 
  
  if(typeof req.body.userId == 'undefined'){
    debuglog("userId must be present in all /business/web requests");
    sendBackResponse(req,res,3,'Invalid or User ID not found in request' );
   }
 else {
	 debuglog("UserId is present so check if that is active");
	 debuglog("We are calling roleutils");
	 debuglog("TODO change this to masterutils later");
	 debuglog("Also maynot need orgId returned from there anymore");
    var uid = req.body.userId;
	var oid = req.body.orgId;
    var async = require('async');
	var modelPath = app.get('model');
	
    var roleutils = require(modelPath+'Roles/roleutils');
    var isError = 0;
    data.userId = uid;
	data.orgId = oid;
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
             sendBackResponse(req,res,4,'Either UserId is not found or is inactive. Please contact Admin' );
              isError = 1;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              sendBackResponse(req,res,4,'Either UserId is not found or is inactive. Please contact Admin');
			  isError = 1;
             }
          if(isError == 0) {
              data.businessType = results.usrStat.bizType;
			  checkSessionToken(req,res,data);
			 }
          }); 
        }
}

  
function checkSessionToken(req,res,data){
 if(typeof req.body.sessionToken == 'undefined'){
     sendBackResponse(req,res,16,'Invalid or Expired session' );
  }
 else {
   sessionToken = req.body.sessionToken;
   userId = req.body.userId;
  

  var totalRows = 0; 
  var sessToken = "";
  var sessTimestamp = "";
  var nowTime = new Date();
  
  var promise = db.get('user_master').find({_id:userId},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      sessToken = docuser.sessionToken;
      sessTimestamp = docuser.sessionTimestamp;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0 || sessionToken != sessToken) {
        debuglog("check table user_master for userId+sessionToken");
        debuglog("If no row then invalid session message");
        sendBackResponse(req,res,16,'Invalid or Expired session' );
        }
      else {
           var sessTime = new Date(sessTimestamp);
           var diffTime = (nowTime-sessTime)/(1000*60*20);
      
           if(diffTime > 0){
             debuglog(diffTime);
             debuglog("Now is greater than session expiry timestamp");
             debuglog("so user has to relogin and create new token");
             sendBackResponse(req,res,16,'Invalid or Expired session' );
           }
           else{
              debuglog(diffTime);
              debuglog("update session token to now+20minutes");
              debuglog("so that it will not expire for another 20 minutes");
              debuglog("this will be repeated every time a request comes");
              updateSessionToken(req,res,userId,sessionToken,data)
           }
      }
    });
    promise.on('error',function(err){
      debuglog(err);
      sendBackResponse(req,res,16,'Invalid or Expired session' );
     });
  }
}


function updateSessionToken(request,response,userId,sessionToken,data){
    var sessionTimestamp= new Date();
        sessionTimestamp.setMinutes(sessionTimestamp.getMinutes()+20);
  
    var promise = db.get('user_master').update(
                         {_id:userId},
                         {$set:{sessionTimestamp: sessionTimestamp}},
                         {upsert:false});
      promise.on('success',function(err,doc){
        debuglog("updated user_master for userId+sessionToken");
        debuglog("set session timestamp = now + 20 minutes");
        callModel(request,response,data);
	 });
      promise.on('error',function(err){
        debuglog("If error send as type 6 error")
        sendBackResponse(request,response,6,'System Error' );
   });
   
}    

function callModel(req,res,data) {
    var url= require('url');
    var modelPath = app.get('model');
	var modelFile = req.modelFolder+'/'+req.modelFile;
    var model = require(modelPath+modelFile);
   
    model.getData(req,res,data);
} 

function handleStatus(request,response,stat,data) { 
    var modelPath=app.get('model');
    var model = require(modelPath+'getStatus');
    model.getData(request,response,request.modelFolder,request.modelFile,stat,data);
}  

function handleResponse(request,response,status,message,data) {
   //response.type('json');
   response.send({responseCode:status,message:message,data:data});
}

function sendBackResponse(request,response,status,message) {
   //response.type('json');
   response.send({responseCode:status,message:message});
}

exports.execute = handleRequest;
exports.process_status = handleStatus;
exports.process_return = handleResponse;




