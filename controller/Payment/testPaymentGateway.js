function handleRequest(request,response) {
   
   //Need to use model to get key, salt etc., for the given
   //business ID.
   //Need to encrypt based on Business Id
    var url= require('url');
    var http= require('http');
    
    var hash = require('node_hash/lib/hash');
    //var key='C0Dr8m'; //This is generic test key
    var key ='gtKFFx'; //This is test server key need to change on prod deployment
		var SALT='eCwWELxi';
    
    var txnid =request.body.txnid; 
		var amount=request.body.amount;
		var productinfo=request.body.productinfo;
		var firstname=request.body.firstname;
		var email=request.body.email;
		
    //should be in exact same format as per pdf otherwise it will error out
    var hash_string = key+'|'+txnid+'|'+amount+'|'+productinfo+'|'+firstname+'|'+email+'|'+'BusC00002'+'||||||||||'+SALT;
	                                                                                                       
	  
	  var payment_gateway_hash = hash.sha512(hash_string.toString());
    var nodeURL = global.nodeURL;
	  
    //This will be returned to APP to redirect for url
    //surl->success, furl->failure, curl->cancel
    //All must be prefixed with corresponding payment gateway
    //because the response will be different encryption
    //so cant have common responses
    //the ip should change between DEV,STAGING AND PROD
    
    var options = {
          firstname:firstname,
          lastname:request.body.lastname,
          phone:request.body.phone,
          key:key, 
          hash:payment_gateway_hash,
          surl: nodeURL+'/testsucessPayUPayment',
          curl:nodeURL+'/testcancelPayUPayment',
          furl:nodeURL+'/testfailurePayUPayment',
          txnid:txnid,
          productinfo:productinfo,
          amount:amount,
          email:email,
          udf1:'BusC00002'
        };
 
  console.log(options);
  
 //http.request(options); //not required since not directly redirecting from server
 response.header("Content-Type", "application/json");
 response.send({url:'https://test.payu.in/_payment',params:options});
 //response.redirect(307,'https://test.payu.in/_payment'); //not working for additional params
} 
    

exports.execute = handleRequest;



