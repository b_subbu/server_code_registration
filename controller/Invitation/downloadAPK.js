function handleRequest(request,response) {
    var url= require('url');
    var modelpath = app.get('model');
    var model = require(modelpath+'Invitation/downloadAPK');
   
    model.getData(request,response);
} 
    
function handleStatus(request,response,stat,data) { 
    var modelpath=app.get('model');
    var model = require(modelpath+'getStatus');
    model.getData(request,response,'Invitation','downloadAPK',stat,data);}  

function handleResponse(request,response,status,message,data) {
   //response.type('json');
    response.header("Content-Type", "text/html");
   response.write("<script>alert('"+message+"');</script>");
   response.write("<script>self.close();</script>");
   response.end();
   //response.send({responseCode:status,message:message,data:data});
}

exports.execute = handleRequest;
exports.process_status = handleStatus;
exports.process_return = handleResponse;


