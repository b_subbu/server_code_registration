function handleRequest(request,response) {
    var url= require('url');
    var modelpath = app.get('model');
    var model = require(modelpath+'Checkout/cancelPayUPayment');
   
    model.getData(request,response);
} 
    
//NOTE: response code is for checkout not cancelpayu 

function handleStatus(request,response,stat,data) { 
    var modelpath=app.get('model');
    var model = require(modelpath+'getStatus');
    model.getDataCheckout(request,response,'Checkout','cancelPayUPayment',stat,data);
}  

function handleResponse(request,response,status,message,data) {
   //response.type('json');
    //response.send({responseCode:status,message:message,data:data});
    
   var options =  {responseCode:status,message:message,data:data};
   //console.log(options);
   var prettyoptions = JSON.stringify(options);
   //console.log(prettyoptions);
   
   //console.log("Script out now");
   response.header("Content-Type", "text/html");
   response.write("<script>Payment.cancel('"+prettyoptions+"');</script>");

}

exports.execute = handleRequest;
exports.process_status = handleStatus;
exports.process_return = handleResponse;



