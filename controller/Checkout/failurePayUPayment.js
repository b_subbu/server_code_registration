function handleRequest(request,response) {
    var url= require('url');
    var modelpath = app.get('model');
    var model = require(modelpath+'Checkout/failurePayUPayment');
   
    model.getData(request,response);
} 
    
//NOTE: response code is for checkout not failurepayu 

function handleStatus(request,response,stat,data) { 
    var modelpath=app.get('model');
    var model = require(modelpath+'getStatus');
    model.getDataCheckout(request,response,'Checkout','failurePayUPayment',stat,data);}  

function handleResponse(request,response,status,message,data) {
   //response.type('json');

   //response.send({responseCode:status,message:message,data:data});
     var options =  {responseCode:status,message:message,data:data};
   
   var prettyoptions = JSON.stringify(options);
   response.header("Content-Type", "text/html");
   response.write("<script>Payment.error('"+prettyoptions+"');</script>");

}

exports.execute = handleRequest;
exports.process_status = handleStatus;
exports.process_return = handleResponse;



