function handleRequest(request,response) {
    var url= require('url');
    var modelpath = app.get('model');
    var model = require(modelpath+'Checkout/successCAshPayment');
   
    model.getData(request,response);
} 
    

function handleStatus(request,response,stat,data) { 
    var modelpath=app.get('model');
    var model = require(modelpath+'getStatus');
     model.getData(request,response,'Checkout','successCashPayment',stat,data);
}  

function handleResponse(request,response,status,message,data) {
   //response.type('json');
    response.send({responseCode:status,message:message,data:data});
   /*var options =  {responseCode:status,message:message,data:data};
   
   var prettyoptions = JSON.stringify(options);
   response.header("Content-Type", "text/html");
   response.write("<script>Payment.success('"+prettyoptions+"');</script>");
   */
   
}

exports.execute = handleRequest;
exports.process_status = handleStatus;
exports.process_return = handleResponse;

