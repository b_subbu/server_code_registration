function getApiParam(apiName,callback){
debuglog("This lists all the apiNames of business web");
debuglog("The params are Model folder, model Name");
debuglog("and sessionCheck which can be either true or false");

var isError = 0;
var data = {};
switch(apiName){
	
	case 'getProductData':
	case 'getUserModules':
	case 'getProductDetails':
	case 'getProductHeader':
     data={modelFolder:'Roles',modelFile:apiName,sessionCheck:true};
     break;
	 
   case 'createRole':
   case 'editRole':
   case 'getRole':
   case 'authoriseRoles':
   case 'rejectRoles':
   case 'submitRoles':
   case 'deleteRoles':
     data={modelFolder:'Users',modelFile:apiName,sessionCheck:true};
	 break;
	
    case 'getPaymentGateway':
    case 'createPaymentGateway':
    case 'editPaymentGateway':
    case 'authorisePaymentGateways':
    case 'rejectPaymentGateways':
    case 'releasePaymentGateway':
    case 'recallPaymentGateway':
    case 'deletePaymentGateways':
    case 'submitPaymentGateways':
       data={modelFolder:'Admin',modelFile:apiName,sessionCheck:true};
	   break;
	   
	case 'getDeliveryOption':
	case 'createDeliveryOption':
	case 'editDeliveryOption':
	case 'authoriseDeliveryOptions':
	case 'rejectDeliveryOptions':
	case 'submitDeliveryOptions':
	case 'deleteDeliveryOptions':
	    data={modelFolder:'Admin',modelFile:apiName,sessionCheck:true};
	    break;
		
	case 'getSeller':
	case 'createSeller':
	case 'editSeller':
	case 'authoriseSellers':
	case 'rejectSellers':
	case 'submitSellers':
	case 'deleteSellers':
	     data={modelFolder:'Sellers',modelFile:apiName,sessionCheck:true};
	    break;
	
	case 'getInvoiceConfig':
	case 'createInvoiceConfig':
	case 'editInvoiceConfig':
	case 'authoriseInvoiceConfigs':
	case 'rejectInvoiceConfigs':
	case 'submitInvoiceConfigs':
	case 'deleteInvoiceConfigs':
	    data={modelFolder:'InvoiceConfig',modelFile:apiName,sessionCheck:true};
	    break;
		
	case 'getTaxType':
	case 'createTaxType':
	case 'editTaxType':
	case 'authoriseTaxTypes':
	case 'rejectTaxTypes':
	case 'submitTaxTypes':
	case 'deleteTaxTypes':
	    data={modelFolder:'Admin',modelFile:apiName,sessionCheck:true};
	    break;
		
	case 'getCharge':
	case 'createCharge':
	case 'editCharge':
	case 'authoriseCharges':
	case 'rejectCharges':
	case 'submitCharges':
	case 'deleteCharges':
	   data={modelFolder:'Charges',modelFile:apiName,sessionCheck:true};
	    break;
	
	case 'getAlertMasterData':
	    data={modelFolder:'Boarding',modelFile:'createAlert',sessionCheck:true};
	    break;
    case 'saveAlert':
	 data={modelFolder:'Boarding',modelFile:apiName,sessionCheck:false};
	    break;
	
	case 'saveLearnAlertImage':
	 data={modelFolder:'Boarding',modelFile:apiName,sessionCheck:false};
	    break;
	
	case 'editAlert':
	case 'submitAlert':
	case 'authoriseAlert':
	case 'rejectAlert':
	case 'deleteAlert':
	case 'releaseAlert':
	case 'recallAlert':
	  data={modelFolder:'Boarding',modelFile:apiName,sessionCheck:true};
	    break;
	
	case 'getMenuItem':
	case 'createMenuItemDetails':
	case 'editMenuItemDetails':
	case 'deleteMenuItems':
	case 'approveMenuItems':
	case 'rejectMenuItems':
	case 'getMenuItemsDetails':
	    data={modelFolder:'Menu',modelFile:apiName,sessionCheck:true};
	    break;
	
	case 'createMenuItemVisuals':
	case 'editMenuItemVisuals':
	    data={modelFolder:'Menu',modelFile:apiName,sessionCheck:false};
	    break;
		
	case 'getItemMetaData':
	    data={modelFolder:'Menu',modelFile:'getSimilarItem',sessionCheck:true};
	    break;
	
	case 'getMenu':
	case 'createMenu':
	case 'editMenu':
	case 'authoriseMenus':
	case 'rejectMenus':
	case 'releaseMenus':
	case 'recallMenus':
	case 'deleteMenus':
	case 'submitMenus':
	    data={modelFolder:'Menu',modelFile:apiName,sessionCheck:true};
	    break;
	
	case 'getOrder':
	case 'checkOut':
	case 'checkIn':
	case 'ship':
	case 'deliver':
	  data={modelFolder:'Orders',modelFile:apiName,sessionCheck:true};
	    break;
	
	case 'getGroup':
	case 'createGroup':
	case 'editGroup':
	case 'deleteGroups':
	case 'approveGroups':
	case 'rejectGroups':
	    data={modelFolder:'Groups',modelFile:apiName,sessionCheck:true};
	    break;
		
	case 'getAuditTrail':
	    data={modelFolder:'Audit',modelFile:apiName,sessionCheck:true};
	    break;
		
	case 'getWorkQueueItems':
	   data={modelFolder:'Orders',modelFile:apiName,sessionCheck:true};
	    break;
		
	case 'getBusinessProfile':
	    data={modelFolder:'Admin',modelFile:apiName,sessionCheck:true};
	    break;
		
	case 'saveBusinessProfile':
	    data={modelFolder:'Admin',modelFile:apiName,sessionCheck:false};
	    break;
		
	case 'getInvitation':
	case 'createInvitation':
	case 'editInvitation':
	case 'submitInvitations':
	case 'authoriseInvitations':
	case 'rejectInvitations':
	case 'deleteInvitations':
	case 'releaseInvitations':
	    data={modelFolder:'Invitation',modelFile:apiName,sessionCheck:true};
	    break;
	
	case 'getUser':
	case 'createUser':
	case 'editUser':
	case 'authoriseUsers':
	case 'rejectUsers':
	case 'submitUsers':
	case 'deleteUsers':
	case 'inviteUsers':
	case 'uninviteUsers':
	case 'deactivateUsers':
	   data={modelFolder:'Users',modelFile:apiName,sessionCheck:true};
	   break;
	
	default:
     isError = 1;
     break;	 

}

if(isError == 1){
	
	debuglog("no definition exists for");
	debuglog(apiName);
	debuglog("so return error");
	callback(6,null);
}
else{
	debuglog("definition exists for api");
	debuglog(apiName);
	debuglog(data);
	callback(null,data);
}

}

exports.apiParam = getApiParam;