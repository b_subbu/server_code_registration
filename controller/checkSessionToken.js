function handleRequest(request,response,next) {
   var url= require('url');
   
   var re = /\/business\/web\//;
   
   //These are APIs before login no need to check session Token for them
   //logout user will have a session token but no need to validate it
   //anyhow the user is going to log out
   //saveAlert and *visuals are multi-part form data so can't directly check here
   var noSessionCheckBusinessWebArray = ['checkUser',
                                         'generateRegistration',
                                         'verifyRegistration',
                                         'subscribeUser',
                                         'checkSubscription',
                                         'loginUser',
                                         'resetPassword',
                                         'forgotPassword',
                                         'clearPassword',
                                         'saveSubscription',
                                         'logoutUser',
                                         'saveAlert',
                                         'createMenuItemVisuals',
                                         'editMenuItemVisuals',
                                         'saveLearnAlertImage',
                                         'deleteUser',
                                         'saveBusinessProfile']
   
   var urlpath = request.path;
   
   //If the url path consists of /business/web then do checking
   if(urlpath.search(re) != -1){
     //console.log("business web requests");
    //var apiname = urlpath.match(re); //regex is too slow here use string functions
    // /business/web/ is 14 characters
    var apiname = urlpath.substring(14);
    if(noSessionCheckBusinessWebArray.indexOf(apiname) >= 0){
      //console.log(" but no session check required for these APIs just proceed");
    next();
    
    }
    else{
    //console.log("session check required");
    getSessionToken(request,response,next);
    }
   
   }
   else{
   //console.log(" not business web requests");
   //console.log("can be either customer app or payment gateway requests");
   //console.log("just proceed no check required");
   next();
   }
   
} 

function getSessionToken(request,response,next){
  var userId = "";
  var sessionToken = "";
 if(typeof request.body.sessionToken == 'undefined'){
   //console.log("Session token must be present in /business/web requests");
   //console.log("Not taking from status master table to speed up response")
   sendBackResponse(request,response,16,'Invalid or Expired session' );
  }
 else {
 
  sessionToken = request.body.sessionToken;
  //console.log("Taking user id from request body which can either be");
  //console.log("with name businessId or userId ")
   if(typeof request.body.businessId == 'undefined')
      userId = request.body.userId;
   else
      userId = request.body.businessId;
 
  checkSessionToken(request,response,userId,sessionToken,next)
 }

}    

function checkSessionToken(request,response,userId,sessionToken,next){

  var totalRows = 0; 
  var sessToken = "";
  var sessTimestamp = "";
  var nowTime = new Date();
  
  var promise = db.get('user_master').find({_id:userId},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      sessToken = docuser.sessionToken;
      sessTimestamp = docuser.sessionTimestamp;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0 || sessionToken != sessToken) {
        //console.log("check table user_master for userId+sessionToken");
        //console.log("If no row then invalid session message");
        sendBackResponse(request,response,16,'Invalid or Expired session' );
        }
      else {
           var sessTime = new Date(sessTimestamp);
           var diffTime = (nowTime-sessTime)/(1000*60*20);
      
           if(diffTime > 0){
             //console.log(diffTime);
             //console.log("Now is greater than session expiry timestamp");
             //console.log("so user has to relogin and create new token");
             sendBackResponse(request,response,16,'Invalid or Expired session' );
           }
           else{
              //console.log(diffTime);
              //console.log("update session token to now+20minutes");
              //console.log("so that it will not expire for another 20 minutes");
              //console.log("this will be repeated every time a request comes");
              updateSessionToken(request,response,userId,sessionToken,next)
           }
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }


function updateSessionToken(request,response,userId,sessionToken,next){

    var sessionTimestamp= new Date();
        sessionTimestamp.setMinutes(sessionTimestamp.getMinutes()+20);
  
    var promise = db.get('user_master').update(
                         {_id:userId},
                         {$set:{
                                sessionTimestamp: sessionTimestamp}
                         },
                         {upsert:false});
      promise.on('success',function(err,doc){
        //console.log("update user_master for userId+sessionToken");
        //console.log("set session timestamp = now + 20 minutes");
        next();
      });
      promise.on('error',function(err){
        //console.log("If error send as type 6 error")
        sendBackResponse(request,response,6,'System Error' );
   });
   
}    

function sendBackResponse(request,response,status,message) {
   //response.type('json');
   response.send({responseCode:status,message:message});
}

exports.execute = handleRequest;



