<<<<<<< HEAD
function processRequest(req,res) {
  var totalRows = 0;
  var user_stat = "";
  var data = {userId:""};

  if( typeof req.body.userId === 'undefined')
    processStatus(req,res,6,data); //system error
  else {
    var usr_id = req.body.userId;

    var promise = db.get('user_master').find({_id:usr_id},{stream: true});
    promise.each(function(doc){
      totalRows++;
      data = {userId:doc._id,emailId:doc.email,mobileNumber:doc.mobile};
      usr_stat = doc.user_status;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,1,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        if(usr_stat != 3)
          processStatus(req,res,8,data);
        else
          createUserDetail(req,res,usr_id,data);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}

function createUserDetail(req,res,uid,data) {

    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
  
     if(req.body.subscription.businessDetails)
       var cid =  req.body.subscription.businessDetails;
     else
       var cid = {};
     
    
     async.parallel({
         usrStat: function(callback){datautils.createUsrDet(uid,cid,callback); },
         },
         function(err,results) {
            if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
          else
             processStatus(req,res,9,data);
           });
 
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/saveSubscription');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
=======
function processRequest(req,res,userdata,files) {  
   var totalRows = 0;
   var user_stat = "";
   var data = {userId:""};
   
    if( typeof req.body.userId === 'undefined')
       processStatus(req,res,6,data); //system error
   else {
      var usr_id = req.body.userId;
    
      var promise = db.get('user_master').find({_id:usr_id},{stream: true});
      promise.each(function(doc){
          totalRows++;
          data = {userId:doc._id,emailId:doc.email,mobileNumber:doc.mobile};
          usr_stat = doc.user_status;
        }); 
      promise.on('complete',function(err,doc){
        if(totalRows == 0)
          processStatus(req,res,1,data);
        else if(totalRows > 1)  
          processStatus(req,res,6,data); //system error should be 1 row only
        else { 
             if(usr_stat != 3) 
               processStatus(req,res,8,data);
             else 
               createUserDetail(req,res,usr_id,data);
          } 
      });
     promise.on('error',function(err){
        console.log(err);
       processStatus(req,res,6,data);//system error 
      });
    } 
}

function createUserDetail(req,res,uid,data) {
    
    var promise = db.get('user_detail').update({ 
                                               _id:uid},
                                              { 
                                               user_id: uid,
                                               bus_name: req.body.businessName,
                                               bus_addr: req.body.businessAddress,
                                               bus_email: req.body.businessEmail, 
                                               bus_mobile: req.body.businessMobile,
                                               bus_type: req.body.businessType,
                                               bus_branch: req.body.numBranches,
                                               bus_url: req.body.businessUrl,
                                               bus_logo: '',
                                               bus_descr: req.body.businessDescription, 
                                               regist_date: new Date(),
                                               org_id: '',
                                               pass_word: '',
                                               activ_date: '',
                                               new_pass: false,
                                               country_id: userdata.businessCountry,
                                               state_id: userdata.businessState,
                                               district_id: userdata.businessRegion,
                                               city_id: userdata.businessCity 
                                               },
                                               {upsert: true}
                                               );
     promise.on('success',function(doc){
          processStatus(req,res,9,data)
     });
     promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
     });
}
 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/saveSubscription');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;

>>>>>>> DEV/master
