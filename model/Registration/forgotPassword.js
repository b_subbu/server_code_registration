<<<<<<< HEAD
function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {userId:""};
  var user_email = "";

  if( typeof req.body.emailId === 'undefined')
    processStatus(req,res,6,data); //system error
  else {
    var user_email = req.body.emailId;

    var promise = db.get('user_master').find({email:user_email},{stream: true});
      promise.each(function(docuser){
      totalRows++;
      data = {userId:docuser._id};
      user_stat = docuser.user_status;
      user_email = docuser.email;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,6,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        if (user_stat == 9)
          sendNotification(req,res,user_stat,user_email,data);
        else
          processStatus(req,res,1,data);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}

function sendNotification(req,res,user_stat,user_email,data){

  var async = require('async');
  var utils = require('./chanceUtils');
  var isError = 0;

//call all functions async in parallel
//but proceed only after completion and checks
//no error checking done now
//do at time of integration to admin dashboard


  async.parallel({
    eOTP: function(callback){utils.getEOTP(callback); },
    mOTP: function(callback){utils.getMOTP(callback); },
    urlcode: function(callback){utils.getURLCODE(callback); }
  },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    else {
      data.emailOtp = results.eOTP;
      data.mobileOtp = results.mOTP;
      data.urlCode = results.urlcode;
      emailOTP = results.eOTP;
      mobileOTP = results.mOTP;

      var eurlCode = encodeURIComponent(results.urlcode);
      var base_url = global.baseURL;


//Below 2 are async calls so insert into db will not
//wait until email/sms is sent if email/sms fails
//need to do manual
//sms is disabled for now until mydime confirmation
//here OTP is plain but while saving in db it will be encrypted

      var email_text = "Dear Customer,<br/><br/> Password Reset Instructions";
      email_text += "please click <a href="+base_url+"subscription/#/forgot-password/"+eurlCode+">link</a> to reset your password.<br/>";
      email_text += "<br/>Email OTP: "+emailOTP;
      email_text += "<br/>Thanks";

      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendmail(user_email,'Forgot Password Reset mail',email_text);
//model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);

      var hash = require('node_hash/lib/hash');
      var salt = 'mydime_##123##';
      var eotp_enc = hash.sha256(emailOTP,salt);

      var salt2 = 'mydime_##456##';
      var eotp_useless = hash.sha256(emailOTP,salt2);


      db.get('user_otp').remove({user_id: data.userId});
//for now just remove earlier row used for verification etc
//this has to be acutally logged in analytics db later and
//removed after successful authentication

      var promise = db.get('user_otp').insert({ user_id: data.userId,
      email_otp:eotp_enc,
      mobile_otp:mobileOTP,
      num_attempts: 0,
      created_time: new Date(),
      url_code: results.urlcode});
      promise.on('success',function(doc){
        data.urlCode = results.urlcode;
        updateUser(req,res,eotp_useless,data);
        processStatus(req,res,9,data);
      });
      promise.on('error',function(err){
        console.log(err);
        processStatus(req,res,6,data);//system error
      });

    }
  });
}

function updateUser(req,res,eonc, data){

  var uid = data.userId;

  db.get('user_master').update({ _id:uid},
  { $set: { user_status: 11} });


  db.get('user_detail').update({_id:uid},
  { $set: {pass_word: eonc } });


}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/forgotPassword');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;


=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {userId:""};
   var user_email = "";
   
   if( typeof req.body.emailId === 'undefined')
       processStatus(req,res,6,data); //system error
   else {
    var user_email = req.body.emailId;
   
      var promise = db.get('user_master').find({email:user_email},{stream: true});
      promise.each(function(docuser){
         totalRows++;
         data = {userId:docuser._id};
         user_stat = docuser.user_status;
         user_email = docuser.email;
        }); 
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,6,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else { 
           if (user_stat == 9)
            sendNotification(req,res,user_stat,user_email,data);
          else
            processStatus(req,res,1,data);
         } 
      });
    promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
      });
  }
}

function sendNotification(req,res,user_stat,user_email,data){

    var async = require('async');
    var utils = require('./chanceUtils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    //no error checking done now
    //do at time of integration to admin dashboard
   
   
   async.parallel({
         eOTP: function(callback){utils.getEOTP(callback); },
         mOTP: function(callback){utils.getMOTP(callback); },
         urlcode: function(callback){utils.getURLCODE(callback); }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
        else {
               data.emailOtp = results.eOTP;
               data.mobileOtp = results.mOTP;
               data.urlCode = results.urlcode;
               emailOTP = results.eOTP;
               mobileOTP = results.mOTP;
               
              var eurlCode = encodeURIComponent(results.urlcode);
     
             //Below 2 are async calls so insert into db will not
             //wait until email/sms is sent if email/sms fails
             //need to do manual
             //sms is disabled for now until mydime confirmation
             //here OTP is plain but while saving in db it will be encrypted
       
            var email_text = "Dear Customer,<br/><br/> Password Reset Instructions";
                email_text += "please click <a href='http://ec2-54-213-174-132.us-west-2.compute.amazonaws.com/subscription/#/forgot-password/"+eurlCode+"'>link</a> to reset your password.<br/>";
                email_text += "<br/>Email OTP: "+emailOTP;
                email_text += "<br/>Thanks";
     
            var modelpath=app.get('model');
            var model = require(modelpath+'email');
                model.sendmail(user_email,'Forgot Password Reset mail',email_text);
               //model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);
               
               var hash = require('node_hash/lib/hash');
               var salt = 'mydime_##123##';
               var eotp_enc = hash.sha256(emailOTP,salt);
               
               var salt2 = 'mydime_##456##';
               var eotp_useless = hash.sha256(emailOTP,salt2);
     
       
             db.get('user_otp').remove({user_id: data.userId});
            //for now just remove earlier row used for verification etc
           //this has to be acutally logged in analytics db later and
          //removed after successful authentication
     
            var promise = db.get('user_otp').insert({ user_id: data.userId,
                                                      email_otp:eotp_enc, 
                                                      mobile_otp:mobileOTP,
                                                      num_attempts: 0,
                                                      created_time: new Date(),
                                                      url_code: results.urlcode});
            promise.on('success',function(doc){
                 data.urlCode = results.urlcode;
                 updateUser(req,res,eotp_useless,data);
                 processStatus(req,res,9,data);
            });
           promise.on('error',function(err){
               console.log(err);
               processStatus(req,res,6,data);//system error 
          });
          
    }
   });
}

function updateUser(req,res,eonc, data){

  var uid = data.userId;
  
  db.get('user_master').update({ _id:uid},
                                { $set: { user_status: 11} });
  
  
  db.get('user_detail').update({_id:uid},
                                 { $set: {pass_word: eonc } });


}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/forgotPassword');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



>>>>>>> DEV/master
