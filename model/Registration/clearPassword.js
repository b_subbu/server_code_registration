<<<<<<< HEAD
function processRequest(req,res){
  var totalRows = 0;
  var eotp="";
  var email_otp = "";
  var num_attempt="";
  var create_time="";
  var moment = require('moment');
  var curr_time = moment();
  var data ={userId:""};
  var uid = "";

  if( typeof req.body.urlCode === 'undefined' || typeof req.body.emailOtp === 'undefined' || typeof req.body.newPassword == 'undefined' )
    processStatus(req,res,6,data); //system error
  else {
    var url_code = req.body.urlCode;
    var eotp = req.body.emailOtp;
    var npass = req.body.newPassword;

     //console.log(npass);
    var hash = require('node_hash/lib/hash');
    var salt = 'mydime_##123##';
    var npass_enc = hash.sha256(npass,salt);
    var eotp_enc = hash.sha256(eotp,salt);

    //console.log(npass_enc);

    var promise = db.get('user_otp').find({url_code:url_code},{stream: true});
      promise.each(function(doc){
      totalRows++;
      data = {userId:doc.user_id,urlCode:doc.url_code};
      email_otp = doc.email_otp;
      num_attempt = doc.num_attempts;
      create_time = moment(doc.created_time);
      uid = doc.user_id;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,1,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        secondsDiff = curr_time.diff(create_time, 'seconds');
//note: both node and mongodb server must have same time and zone
        if(secondsDiff > 60*60) {
          regenerateOTP(req,res,3,data);
          return;
        }
        if (num_attempt > 5) {
          regenerateOTP(req,res,4,data);
          return;
        }
        if (eotp_enc != email_otp) {
          updateNumAttempts(req,res,2,data,num_attempt);
          return;
        }
        getSessionDetail(req,res,9,data,npass_enc);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}


function updateNumAttempts(req,res,stat,data,nattmpt) {
  var urlCode = data.urlCode;
  nattmpt++;

  var promise = db.get('user_otp').update({ url_code:urlCode},
  { $set: { num_attempts: nattmpt }});
  promise.on('success',function(doc){
    processStatus(req,res,stat,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });


}

function regenerateOTP(req,res,stat,data){
  var uid = data.userId;
  var totalRows = 0;
  var user_email = "";
  var promise = db.get('user_master').find({_id:uid});
  promise.each(function(doc){
    totalRows++;
    user_email = doc.email;
    data.emailId = doc.email;
    data.mobileNumber = doc.mobile;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else
      processgenerateOTP(req,res,user_email,stat,data);

  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });

}

function processgenerateOTP(req,res,user_email,stat,data){


  var async = require('async');
  var utils = require('./chanceUtils');
  var isError = 0;

//call all functions async in parallel
//but proceed only after completion and checks
//no error checking done now
//do at time of integration to admin dashboard


  async.parallel({
  eOTP: function(callback){utils.getEOTP(callback); },
  mOTP: function(callback){utils.getMOTP(callback); } },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    else {
      var emailOTP = results.eOTP;
      var mobileOTP = results.mOTP;
      var urlCode = encodeURIComponent(data.urlCode);
//Below 2 are async calls so insert into db will not
//wait until email/sms is sent if email/sms fails
//need to do manual
//sms is disabled for now until mydime confirmation
//here OTP is plain but while saving in db it will be encrypted

      var hash = require('node_hash/lib/hash');
      var salt = 'mydime_##123##';
      var eotp_enc = hash.sha256(emailOTP,salt);
      var base_url = global.baseURL;

      var email_text = "Dear Customer,<br/><br/> ";
      email_text += "Your password reset attempt has failed. So please try again with below credentials";
      email_text += "<br/>please click <a href="+base_url+"subscription/#/forgot-password/"+urlCode+">link</a> to reset your password.<br/>";
      email_text += "<br/>Email OTP: "+emailOTP;
//email_text += "<br/>Mobile OTP: "+mobileOTP;
      email_text += "<br/>Thanks";

      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendmail(user_email,'OTP Regenerated mail',email_text);
//model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);

      var promise = db.get('user_otp').update({ url_code:urlCode},
      { 
        $set: { email_otp:eotp_enc,
        mobile_otp:mobileOTP,
        num_attempts: 0,
        created_time: new Date() }
      });
      promise.on('success',function(doc){
        data= {emailId:data.emailId,mobileNumber:data.mobileNumber,emailOtp:emailOTP,userId:data.userId,urlCode:urlCode};
        processStatus(req,res,stat,data);
      });
      promise.on('error',function(err){
        console.log(err);
        processStatus(req,res,6,data);//system error
      });
    }
  });
}

function getSessionDetail(req,res,stat,data,npass){
  var uid = data.userId;
  var totalRows = 0;
  var user_email = "";
  var promise = db.get('user_master').find({_id:uid});
  promise.each(function(docuser){
    totalRows++;
      userEmail = docuser.email;
      userLoggedin = docuser.logged_in;
      sessionToken = docuser.sessionToken;
      sessionTimestamp = docuser.sessionTimestamp;
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else
      processLogin(req,res,9,data,userLoggedin,sessionToken,sessionTimestamp,userEmail,npass);

  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });

}

function processLogin(req,res,stat,data,userLoggedIn,sessionToken,sessionTimeStamp,userEmail,npass){
     var moment = require('moment');
     var chance = require('chance').Chance();
     var sessionToken = chance.string({length:8, pool: 'abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ023456789' });
 
     var sessionTimestamp= new Date();
     sessionTimestamp.setMinutes(sessionTimestamp.getMinutes()+20);
      
     if(userLoggedIn == true){
      //check if now > sessionTimeStamp+20 minutes
      //if yes then generate new session token
      //set sessiontimestamp = now+20 minutes
      //set data.sessionToken = token value
      
     var nowTime = new Date();
     var sessTime = new Date(sessionTimeStamp);
      //date in monogdb is ISO timestamp
      
      var diffTime = (nowTime-sessTime)/(1000*60*20);
      
      if(diffTime <= 0)
           processStatus(req,res,8,data); //already logged in message
    
      else {
        data.sessionToken = sessionToken;
        var promise = db.get('user_master').update(
             {email:userEmail},
            {$set:{
                   user_status: 9,
                   logged_in: true,
                   sessionToken: sessionToken,
                   sessionTimestamp: sessionTimestamp}
                   },
              {upsert:false});
      
      promise.on('success',function(err,doc){
        completePasswordChange(req,res,9,data,userEmail,npass);
      });
      promise.on('error',function(err){
        processStatus(req,res,6,data);
      });
    }
   }
   else {
     //generate sessionToken
     //set sessionTimestamp = now+20 minutes
     //set data.sessionToken = token value
     data.sessionToken = sessionToken;
     var promise = db.get('user_master').update(
                   {email:userEmail},
                   {$set:{
                        user_status: 9,
                        logged_in: true,
                        sessionToken: sessionToken,
                        sessionTimestamp: sessionTimestamp}
                   },
                        {upsert:false});
      promise.on('success',function(err,doc){
           completePasswordChange(req,res,9,data,userEmail,npass);
     });
      promise.on('error',function(err){
           processStatus(req,res,6,data);
      });
  }
}

function completePasswordChange(req,res,stat,data,userEmail,npass){

//-->TODO: delete from user_otp table for this entry
//or archive to a audit table or log in json as required
  var uid = data.userId;
   db.get('cust_master').update(
     {cust_email:userEmail},
     {$set: {pass_word: npass} },
     {upsert: false}
  );
 
  
  var promise = db.get('user_detail').update({_id:uid}, {$set: {pass_word : npass} });

  promise.on('success',function(doc){
    populateUserDetails(req,res,9,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });

}

function populateUserDetails(req,res,stat,data) {

  var totalRows = 0;
  var org_id = "";
  var uid = data.userId;
  var promise = db.get('user_detail').find({_id: uid},{stream: true});
  promise.each(function(doc){
    totalRows++;
    data.emailId = doc.bus_email;
    data.mobileNumber = doc.bus_mobile;
    data.orgId = doc.org_id;
    data.displayName = doc.bus_name;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else
      processStatus(req,res,9,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });

}
function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/clearPassword');

  controller.process_status(req,res,stat,data);
}




exports.getData = processRequest;
=======
function processRequest(req,res){
   var totalRows = 0;
   var eotp="";
   var email_otp = "";
   var num_attempt="";
   var create_time="";
   var moment = require('moment');
   var curr_time = moment();
   var data ={userId:""};
   var uid = "";
   
   if( typeof req.body.urlCode === 'undefined' || typeof req.body.emailOtp === 'undefined' || typeof req.body.newPassword == 'undefined' )
      processStatus(req,res,6,data); //system error
   else {
    var url_code = req.body.urlCode;
    var eotp = req.body.emailOtp;
    var npass = req.body.newPassword;
    
    var hash = require('node_hash/lib/hash');
    var salt = 'mydime_##123##';
    var npass_enc = hash.sha256(npass,salt);
    var eotp_enc = hash.sha256(eotp,salt);
     
   
   var promise = db.get('user_otp').find({url_code:url_code},{stream: true});
       promise.each(function(doc){
          totalRows++;
          data = {userId:doc.user_id,urlCode:doc.url_code};
          email_otp = doc.email_otp;
          num_attempt = doc.num_attempts;
          create_time = moment(doc.created_time);
          uid = doc.user_id;
         }); 
      promise.on('complete',function(err,doc){
        if(totalRows == 0)
           processStatus(req,res,1,data);
        else if(totalRows > 1)  
           processStatus(req,res,6,data); //system error should be 1 row only
        else { 
              secondsDiff = curr_time.diff(create_time, 'seconds');
             //note: both node and mongodb server must have same time and zone
              if(secondsDiff > 60*60) {
                  regenerateOTP(req,res,3,data);
                  return;
                }
             if (num_attempt > 5) { 
                regenerateOTP(req,res,4,data);
                return;
                }
             if (eotp_enc != email_otp) {
                 updateNumAttempts(req,res,2,data,num_attempt);
                 return;
                }
             completePasswordChange(req,res,uid,npass_enc,data);
            } 
      });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
  });
 } 
}


function updateNumAttempts(req,res,stat,data,nattmpt) {
   var urlCode = data.urlCode;
   nattmpt++;
 
   var promise = db.get('user_otp').update({ url_code:urlCode},
                                             { $set: { num_attempts: nattmpt }
                                              });
    promise.on('success',function(doc){
         processStatus(req,res,stat,data);
    });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
  });


}

function regenerateOTP(req,res,stat,data){
   var uid = data.userId;
   var totalRows = 0;
   var user_email = "";
   var promise = db.get('user_master').find({_id:uid});
   promise.each(function(doc){
        totalRows++;
        user_email = doc.email;
        data.emailId = doc.email;
        data.mobileNumber = doc.mobile;
        }); 
   promise.on('complete',function(err,doc){
       if(totalRows == 0)
         processStatus(req,res,6,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else 
         processgenerateOTP(req,res,user_email,stat,data);
         
   });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
   });
    
}

function processgenerateOTP(req,res,user_email,stat,data){


    var async = require('async');
    var utils = require('./chanceUtils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    //no error checking done now
    //do at time of integration to admin dashboard
   
   
   async.parallel({
         eOTP: function(callback){utils.getEOTP(callback); },
         mOTP: function(callback){utils.getMOTP(callback); } },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
        else {
              var emailOTP = results.eOTP;
              var mobileOTP = results.mOTP;
              var urlCode = encodeURIComponent(data.urlCode); 
       //Below 2 are async calls so insert into db will not
       //wait until email/sms is sent if email/sms fails
       //need to do manual
       //sms is disabled for now until mydime confirmation
       //here OTP is plain but while saving in db it will be encrypted
      
         var hash = require('node_hash/lib/hash');
         var salt = 'mydime_##123##';
         var eotp_enc = hash.sha256(emailOTP,salt);
      
           var email_text = "Dear Customer,<br/><br/> ";
             email_text += "Your password reset attempt has failed. So please try again with below credentials";
             email_text += "<br/>please click <a href='http://ec2-54-213-174-132.us-west-2.compute.amazonaws.com/subscription/#/forgot-password/"+urlCode+"'>link</a> to reset your password.<br/>";
             email_text += "<br/>Email OTP: "+emailOTP;
            //email_text += "<br/>Mobile OTP: "+mobileOTP;
             email_text += "<br/>Thanks";
     
            var modelpath=app.get('model');
            var model = require(modelpath+'email');
                model.sendmail(user_email,'OTP Regenerated mail',email_text);
               //model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);
      
            var promise = db.get('user_otp').update({ url_code:urlCode},
                                                   { $set: { email_otp:eotp_enc, 
                                                     mobile_otp:mobileOTP,
                                                     num_attempts: 0,
                                                     created_time: new Date() }
                                                    });
            promise.on('success',function(doc){
                data= {emailId:data.emailId,mobileNumber:data.mobileNumber,emailOtp:emailOTP,userId:data.userId,urlCode:urlCode};
                processStatus(req,res,stat,data);
           });
           promise.on('error',function(err){
               console.log(err);
               processStatus(req,res,6,data);//system error 
           });
        }
     });
}

function completePasswordChange(req,res,uid,npass,data){

      //-->TODO: delete from user_otp table for this entry
      //or archive to a audit table or log in json as required
   
    
     var totalRows = 0;
     
     db.get('user_master').update({ _id:uid},{ $set: { user_status: 9} });
  
     var promise = db.get('user_detail').update({_id:uid}, {$set: {pass_word : npass} });
 
    promise.on('success',function(doc){
           populateUserDetails(req,res,9,data);
         });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
       });

 }

 function populateUserDetails(req,res,stat,data) {
 
    var totalRows = 0;
    var org_id = "";
    var uid = data.userId;
    var promise = db.get('user_detail').find({_id: uid},{stream: true});
    promise.each(function(doc){
        totalRows++;
        data.emailId = doc.bus_email;
        data.mobileNumber = doc.bus_mobile;
        data.orgId = doc.org_id;
        //data.businessLogo = doc.bus_logo;
        data.businessName = doc.bus_name;
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,6,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else
         processStatus(req,res,9,data);
    });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
  });
 
 }
 function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/clearPassword');
  
    controller.process_status(req,res,stat,data);
}




exports.getData = processRequest;
>>>>>>> DEV/master
