<<<<<<< HEAD
function processRequest(req,res,folder,servicename,stat,data){
  var totalRows = 0;
  var user_stat = "";
  if( typeof req.body.emailId === 'undefined' )
    processStatus(req,res,6,data); //system error
  else
    checkUserOTP(req,res,data);

}


function checkUserOTP(req,res,data) {

  var totalRows = 0;
  var user_stat = "";
  var eotp="";
  var motp = "";
  var num_attempt="";
  var create_time="";
  var uid =data.userId;
  var moment = require('moment');
  var curr_time = moment();

  var promise = db.get('user_otp').find({user_id:uid},{stream: true});
  promise.each(function(doc){
    totalRows++;
//data = {userId:doc.user_id,urlCode:doc.url_code};
    data.urlCode = doc.url_code;
    create_time = moment(doc.created_time);
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      createUserOTP(req,res,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else {
//regenerate OTP only if more than 3 minutes
//else reuse same
      secondsDiff = curr_time.diff(create_time, 'seconds');
//note: both node and mongodb server must have same time and zone
      if(secondsDiff > 60*3) {
        createUserOTP(req,res,data);
      }
      else
        processStatus(req,res,7,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function createUserOTP(req,res,data){

  var async = require('async');
  var utils = require('./chanceUtils');
  var isError = 0;

//call all functions async in parallel
//but proceed only after completion and checks
//no error checking done now
//do at time of integration to admin dashboard


  async.parallel({
    eOTP: function(callback){utils.getEOTP(callback); },
    mOTP: function(callback){utils.getMOTP(callback); },
    urlcode: function(callback){utils.getURLCODE(callback); }
  },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    else {
      data.emailOtp = results.eOTP;
      data.mobileOtp = results.mOTP;
      data.urlCode = results.urlcode;
      emailOTP = results.eOTP;
      mobileOTP = results.mOTP;
      var urlCode = results.urlcode;

      var eurlCode = encodeURIComponent(urlCode);
      var hash = require('node_hash/lib/hash');
      var salt = 'mydime_##123##';
      var eotp_enc = hash.sha256(emailOTP,salt);
      var base_url = global.baseURL;


//Below 2 are async calls so insert into db will not
//wait until email/sms is sent if email/sms fails
//need to do manual
//sms is disabled for now until mydime confirmation
//here OTP is plain but while saving in db it will be encrypted

      var email_text = "Dear Customer,<br/><br/> ";
      email_text += "please click <a href="+base_url+"subscription/#/verify/"+eurlCode+">link</a> to re-verify your credentials and continue to Subscribe.<br/>";
      email_text += "<br/>Email OTP: "+emailOTP;
//email_text += "<br/>Mobile OTP: "+mobileOTP;
      email_text += "<br/>Thanks";

      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendmail(data.emailId,'Registration confirmation mail',email_text);
//model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);

      db.get('user_otp').remove({user_id: data.userId});
//for now just remove earlier row used for verification etc
//this has to be acutally logged in analytics db later and
//removed after successful authentication

      var promise = db.get('user_otp').insert({ user_id: data.userId,
      email_otp:eotp_enc,
      mobile_otp:mobileOTP,
      num_attempts: 0,
      created_time: new Date(),
      url_code: urlCode});
      promise.on('success',function(doc){
        data= {emailId:data.emailId,mobileNumber:data.mobileNumber,emailOtp:emailOTP,mobileOtp:mobileOTP,userId:data.userId,urlCode:urlCode};
        processStatus(req,res,9,data);
      });
      promise.on('error',function(err){
        console.log(err);
        processStatus(req,res,6,data);//system error
      });
    }
  });
}



function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/checkUser');

  controller.process_reverify(req,res,stat,data);
}

exports.getData = processRequest;

=======
function processRequest(req,res,folder,servicename,stat,data){
   var totalRows = 0;
   var user_stat = "";
   if( typeof req.body.emailId === 'undefined' )
      processStatus(req,res,6,data); //system error
   else
     checkUserOTP(req,res,data);

}


function checkUserOTP(req,res,data) {

   var totalRows = 0;
   var user_stat = "";
   var eotp="";
   var motp = "";
   var num_attempt="";
   var create_time="";
   var uid =data.userId;
   var moment = require('moment');
   var curr_time = moment();

  var promise = db.get('user_otp').find({user_id:uid},{stream: true});
   promise.each(function(doc){
        totalRows++;
        //data = {userId:doc.user_id,urlCode:doc.url_code};
        data.urlCode = doc.url_code;
        create_time = moment(doc.created_time);
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
         createUserOTP(req,res,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else { 
            //regenerate OTP only if more than 3 minutes
            //else reuse same
            secondsDiff = curr_time.diff(create_time, 'seconds');
             //note: both node and mongodb server must have same time and zone
             if(secondsDiff > 60*3) {
                 createUserOTP(req,res,data);
                }
             else
               processStatus(req,res,7,data);
           } 
      });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
  });
 }

function createUserOTP(req,res,data){

    var async = require('async');
    var utils = require('./chanceUtils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    //no error checking done now
    //do at time of integration to admin dashboard
   
   
   async.parallel({
         eOTP: function(callback){utils.getEOTP(callback); },
         mOTP: function(callback){utils.getMOTP(callback); },
         urlcode: function(callback){utils.getURLCODE(callback); }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
        else {
               data.emailOtp = results.eOTP;
               data.mobileOtp = results.mOTP;
               data.urlCode = results.urlcode;
               emailOTP = results.eOTP;
               mobileOTP = results.mOTP;
               var urlCode = results.urlcode;
               
              var eurlCode = encodeURIComponent(urlCode);
              var hash = require('node_hash/lib/hash');
              var salt = 'mydime_##123##';
              var eotp_enc = hash.sha256(emailOTP,salt);
     
               //Below 2 are async calls so insert into db will not
               //wait until email/sms is sent if email/sms fails
               //need to do manual
               //sms is disabled for now until mydime confirmation
               //here OTP is plain but while saving in db it will be encrypted
               
               var email_text = "Dear Customer,<br/><br/> ";
                   email_text += "please click <a href='http://ec2-54-213-174-132.us-west-2.compute.amazonaws.com/subscription/#/verify/"+eurlCode+"'>link</a> to re-verify your credentials and continue to Subscribe.<br/>";
                   email_text += "<br/>Email OTP: "+emailOTP;
                   //email_text += "<br/>Mobile OTP: "+mobileOTP;
                   email_text += "<br/>Thanks";
             
              var modelpath=app.get('model');
              var model = require(modelpath+'email');
                  model.sendmail(data.emailId,'Registration confirmation mail',email_text);
                 //model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);
        
                 db.get('user_otp').remove({user_id: data.userId});
               //for now just remove earlier row used for verification etc
               //this has to be acutally logged in analytics db later and
               //removed after successful authentication
           
             var promise = db.get('user_otp').insert({ user_id: data.userId,
                                                       email_otp:eotp_enc, 
                                                       mobile_otp:mobileOTP,
                                                       num_attempts: 0,
                                                       created_time: new Date(),
                                                       url_code: urlCode});
            promise.on('success',function(doc){
                 data= {emailId:data.emailId,mobileNumber:data.mobileNumber,emailOtp:emailOTP,mobileOtp:mobileOTP,userId:data.userId,urlCode:urlCode};
                 processStatus(req,res,9,data);
            });
           promise.on('error',function(err){
               console.log(err);
               processStatus(req,res,6,data);//system error 
          });
      }
   });
}


   
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/checkUser');
  
    controller.process_reverify(req,res,stat,data);
}
   
exports.getData = processRequest;


>>>>>>> DEV/master
