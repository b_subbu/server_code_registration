function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {};
  if( typeof req.body.userId === 'undefined' )
    processStatus(req,res,6,data); //system error
  else
    checkMasterStatus(req,res,9,data);
    
}
function checkMasterStatus(req,res,stat,data){ 
    var user_id = req.body.userId;
    var user_stat = 0;
    var user_loggedin = false;
    var totalRows = 0;
    
    var promise = db.get('user_master').find({_id:user_id},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      data = {emailId:docuser.email,mobileNumber:docuser.mobile,userId:docuser._id};
      user_stat = docuser.user_status;
      user_loggedin = docuser.logged_in;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,1,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
       if (user_stat == 9)
          updateIsLoggedIn(req,res,user_stat,data);
        else
          processStatus(req,res,8,data);
        }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }

function updateIsLoggedIn(req,res,stat,data){ 
 var user_id = req.body.userId;
 var sessionTimestamp= new Date();
     sessionTimestamp.setMinutes(sessionTimestamp.getMinutes()+0);

      var promise = db.get('user_master').update(
      {_id:user_id},
       {$set:
         {logged_in: false,
          sessionToken: '',
          sessionTimestamp:sessionTimestamp}
      },
      {multi: false, upsert: false});
    promise.on('success',function(err,doc){
          processStatus(req,res,9,data);
      });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/logoutUser');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

