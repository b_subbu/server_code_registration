<<<<<<< HEAD
function processRequest(req, res, data, srcn) {
    var totalRows = 0;
    var orgId = data.busName.substring(0, 3);
    var bizType = data.businessType;

    var mastutils = require('./masterUtils');

    mastutils.masterNames('business_type_master', bizType, function (err, result) {
        if (err) callback(6, null);
        else {
            bizName = result.typeName.substring(0, 1); //Take the first letter of business type name
            data.orgId = orgId;
            data.bizName = bizName;
            generateOrgId(req, res, data, srcn);
        }
    });

}


function generateOrgId(req, res, data, srcn) {
    var totalRows = 0;
    var orgId = data.orgId;
    var bizName = data.bizName;
    var currseqnum;
    var next_seq_str;


//use same srcn_sequence table but instead of current date use business type
//This is a stop gap arrangement before moving to Admin workflow
//TODO move to admin workflow once screens are ready
    var promise = db.get('srcn_sequence').find({running_date: bizName}, {stream: true});
    promise.each(function (doc) {
        totalRows++;
        currseqnum = doc.running_sequence;
    });
    promise.on('complete', function (err, doc) {
        if (totalRows == 0) {
            db.get('srcn_sequence').insert({
                running_date: bizName,
                running_sequence: '00001'
            });
            orgId += bizName + '00001';
            getSystemRoles(req, res, data, orgId, srcn);
        }
        else {
            var next_sequence = parseInt(currseqnum);
            next_sequence++;
            next_seq_str = String("0000000" + next_sequence).slice(-5);
            var promise = db.get('srcn_sequence').update({running_date: bizName},
                {$set: {running_sequence: next_seq_str}});
            orgId += bizName + next_seq_str;
            getSystemRoles(req, res, data, orgId, srcn);
        }
    });
    promise.on('error', function (err) {
        console.log(err);
        processStatus(req, res, 6, data);//system error
    });

}

function getSystemRoles(req, res, data, orgId, srcn) {
    debuglog("Get the system roles as defined for the given");
    debuglog("Business type");
    debuglog("And then get the products as defined for the given role");
    debuglog("Then these will be created in orgId_roles table as system role");
    debuglog("With authorised status");
    
   var datautils = require('./dataUtils');
   
   datautils.systemRoles(data.businessType,function(err,result){
     if(err){
      isError = 1;
      processStatus(req,res,6,data);
     }
     else{
       if(result.errorCode == 1){
       debuglog('No products defined for this business type');
       debuglog('So error dont create user further');
       isError =1;
       processStatus(req,res,6,data);
       }
       else{
       var systemRoles = result.rolesArray;
       var products    = result.productArray;
       createSystemRoles(req,res,data,orgId,srcn,systemRoles,products);
       debuglog("Start creating system roles");
       }
     }
     });
}


function createSystemRoles(req, res, data, orgId, srcn,systemRoles,products) {
    var tblName = orgId + '_roles';
    var async   = require('async');
    var dataUtils = require('./dataUtils');
    var isError = 0;
    
    debuglog("Create one row in orgId roles table for each row in systemRoles");
    debuglog("Note the productArray format is assumed to be fixed and");
    debuglog("Roles are hard-coded for business type");
   
    async.forEachOf(systemRoles,function(key1,value1,callback1){
       debuglog("Start creating for :");
       debuglog(key1);
       debuglog(value1);
       dataUtils.createSystemRoles(tblName,key1,products,function(err,result) {
        if(err){
         isError = 1;
         callback1();
        }
        else {
                debuglog("generated for Role:");
                debuglog(key1);
                callback1();
              }
           }); 
        },
     function(err){
       if(err || isError == 1){
              processStatus (req,res,6,data);
              debuglog("Errored out");        
            }
      else {
            createServiceStatus(req,res,data,orgId,srcn);
            debuglog("No error proceed further");
            }
      });
   }


function createServiceStatus(req, res, data, orgId, srcn) {
    debuglog("Org Id", orgId);
    var uid = data.userId;
    data.orgId = orgId;
    var totalRows = 0;
    db.get('service_status').insert({
        user_id: data.userId,
        srvice_req: srcn,
        srvice_desc: 'Subscription Request',
        status: 'Approved',
        created_time: new Date()
    });

    var async = require('async');
    var utils = require('./chanceUtils');
    var custUtils = require('../Render/utils');
    var isError = 0;

//call all functions async in parallel
//no error checking done now do at time of integration to admin dashboard


    async.parallel({
            passWord: function (callback) {utils.getPASSWORD(callback);},
            customer:function (callback) {custUtils.checkCustomerEmail(data.emailId,callback);}
        },
        function (err, results) {
            if (err) {
                console.log(err);
                processStatus(req, res, 6, data);//system error
                isError = 1;
                return;
            }
            else {
                db.get('service_status').insert({
                    user_id: data.userId,
                    srvice_req: srcn,
                    srvice_desc: 'Business Credentials',
                    status: 'Sent',
                    created_time: new Date()
                });
                db.get('user_master').update({_id: uid},
                    {$set: {
                            user_status: 9,
                            org_id: orgId,
                            system_user: true,
                            activ_date: new Date()
                           }
                    });

               debuglog(results);
               debuglog("If no customer record existing then update password as below");
               debuglog("If customer record exists but without password then updated business web");
               debuglog("And customer app with below password and username");
               debuglog("If customer record exists with password then update");
               debuglog("with that of below but here customername and username can be different");
               debuglog("Also set newpass as true and email as different since cant");
               debuglog("send decrypted password");
               debuglog("Note cust record is created with active as true and registration date in invitation module ");
               debuglog("In case if that was deactived active will become so isCustomer also false");
               debuglog("Password can be still changed but not an issue since anyhow customer is inactive");
               
               
               if(results.customer.isCustomer == false ||
                  results.customer.passWord   == null  ||
                  typeof results.customer.passWord   == 'undefined'){
                debuglog("Either customer is not found or inactive");
                debuglog("Or has downloaded APK but not yet registered");
                
                var pass_word = results.passWord;

                var email_text = "Dear Customer,<br/><br/> Thank you for subscription";
                email_text += "<br/>please find below details for login to your account.<br/>";
                email_text += "<br/>Organization ID: " + orgId;
                email_text += "<br/>Password: " + pass_word;
                email_text += "<br/>Thanks";

                var modelpath = app.get('model');
                var model = require(modelpath + 'email');
                model.sendmail(data.emailId, 'Account Activation mail', email_text);

                var hash = require('node_hash/lib/hash');
                var salt = 'mydime_##123##';
                var pass_word_enc = hash.sha256(pass_word, salt);

                db.get('user_detail').update({_id: uid},
                    {$set: {
                            org_id: orgId,
                            pass_word: pass_word_enc,
							              role_id: 18,
							              user_name: data.userName, 
                            activ_date: new Date(),
                            new_pass: true
                           }
                        },
                      {upsert:false}
                    );
                    
                debuglog("Update cust_record with name and password");
                debuglog("If no cust record then update fails and nothing happends");
                debuglog("TODO move this to standard functions since this is a quickfix");
               
                db.get('cust_master').update(
                    {cust_email: data.emailId},
                    {$set: {
                            cust_name: data.userName,
                            pass_word: pass_word_enc,
                           }
                         },
                         {upsert:false}
                    );
         
                console.log("After ", orgId);
                getUserDetail(req, res, data);
              }
              if(results.customer.isCustomer == true && results.customer.passWord   != null){
                debuglog("This is a registered customer so update password with app customer password");
                debuglog("email will be different and change password on first login will not be enforced");
                debuglog("Note this is a copy of encrypted password");
                
                var email_text = "Dear Customer,<br/><br/> Thank you for subscription";
                email_text += "<br/>please find below details for login to your account.<br/>";
                email_text += "<br/>Organization ID: " + orgId;
                email_text += "<br/>Password: 'use same as your customer app'";
                email_text += "<br/>Thanks";

                var modelpath = app.get('model');
                var model = require(modelpath + 'email');
                model.sendmail(data.emailId, 'Account Activation mail', email_text);

                db.get('user_detail').update({_id: uid},
                    {$set: {
                            org_id: orgId,
                            pass_word: results.customer.passWord,
							              role_id: 18,
							              user_name: data.userName, 
                            activ_date: new Date(),
                            new_pass: false }
                        },
                       {upsert:false}  
                    );
                    
                 debuglog("After ", orgId);
                getUserDetail(req, res, data);
              } 
            }
        });


}

function getUserDetail(req, res, data) {
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    data.subscription = {};
    var uid = data.userId;

    async.parallel({
            usrStat: function (callback) {
                datautils.usrDets(uid, callback);
            }
        },
        function (err, results) {
            if (err) {
                console.log(err);
                isError = 1;
                return;
            }
            if (isError == 0) {
                var userDets = results.usrStat.subscription;
                debuglog("Result as obtained from user details");
                debuglog(userDets);
                createMainSeller(req, res, data, userDets);
            }
        });
}


function createMainSeller(req, res, data, userDets) {
    var async = require('async');
    var datautils = require('./dataUtils');
    var sellerutils = require('../Admin/dataUtils');

    var isError = 0;
    var uid = data.userId;

    var userDetails = userDets;
    userDetails.businessLogo = '';
    userDetails.sellerId = 1;
    userDetails.sellerType = {id: 11, name: 'Main'};
    userDetails.status_id = 82;

    var sellerTblName = data.orgId + '_sellers';
    async.parallel({
            sellerStat: function (callback) {
                sellerutils.saveSellerDetails(uid, userDets, sellerTblName, callback);
            }
        },
        function (err, results) {
            if (err) {
                console.log(err);
                isError = 1;
                return;
            }
            if (isError == 0) {
          		getSystemTaxes(req,res,data,userDets);
            }
        });
}

function getSystemTaxes(req, res, data, userDets) {
    debuglog("Get the taxesas defined for the given");
    debuglog("Business type+country+state in tax_type_master");
    debuglog("Then these will be created in orgId_tax_type table as taxes");
    debuglog("With authorised status");
    
   var datautils = require('./dataUtils');
   
   datautils.systemTaxes(data.businessType,userDets.country,userDets.state,function(err,result){
     if(err){
      isError = 1;
      processStatus(req,res,6,data);
     }
     else{
       if(result.errorCode == 1){
       debuglog('No taxes defined for this business type');
       debuglog('So dont create taxes but just proceed further since user is already created');
       isError =1;
       createMainUser(req,res,data,userDets);
       }
       else{
       var systemTaxes = result.taxesArr;
       createSystemTaxes(req,res,data,systemTaxes,userDets);
       debuglog("Start creating system taxes");
       }
     }
     });
}


function createSystemTaxes(req, res, data, systemTaxes,userDets) {
    var tblName = data.orgId + '_tax_type';
    var async   = require('async');
    var taxUtils = require('../Admin/dataUtils');
    var isError = 0;
    
    debuglog("Start creating row by row");
    
    async.forEachOf(systemTaxes,function(key1,value1,callback1){
       debuglog("Start creating for :");
       debuglog(key1);
       debuglog(value1);
       taxUtils.TaxUpdate(tblName,key1.tax.taxId,key1,function(err,result) {
        if(err){
         isError = 1;
         callback1();
        }
        else {
                debuglog("generated for:");
                debuglog(key1);
                callback1();
              }
           }); 
        },
     function(err){
       if(err || isError == 1){
              processStatus (req,res,6,data);
              debuglog("Errored out");        
            }
      else {
            createMainUser(req,res,data,userDets);
            debuglog("No error proceed further");
            }
      });
   }


function createMainUser(req, res, data, userDets) {
    var async = require('async');
    var datautils = require('./dataUtils');
    var userutils = require('../Users/dataUtils');

    var isError = 0;
    var UserData = {};
	 
   UserData.users = {};
   
   UserData.users.userCode = 1; 
   UserData.users.userName = data.userName;
   UserData.users.userEmail = data.emailId;
   UserData.users.userPhone = data.mobileNumber;
   UserData.users.userType = 1;
   UserData.users.userRole = {roleId:18,roleName:'Super User',roleType:'System',status:82};
 
     UserData.status_id = 84;
   debuglog("userdata data");
   debuglog("system user created and set status as active");
  
  
    var tblName = data.orgId+'_users'; //create will always be in main table
   
   
      userutils.UserUpdate(tblName,UserData.users.userCode,UserData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(UserData);
                updateMongoId(tblName,UserData,data);
                }
            });
}

function updateMongoId(tblName,userData,data){

debuglog("We are updating the entry with MongoId of user");
debuglog("This will be useful in uninviting because user could change email in edit");
debuglog("and submit again for authorised status");  
var promise = db.get(tblName).update(
  {userId:userData.users.userCode},
  {$set: {mongoId: data.userId,
          update_time: new Date}},
        {upsert:false}
        );  
  promise.on('success',function(err,doc){
       return;
       });
   promise.on('error',function(err){
       console.log(err);
       return;
     });
}

function processStatus(req, res, stat, data) {
    var controllerPath = app.get('controller');
    var controller = require(controllerPath + 'Registration/adminApprove');

    controller.process_status(req, res, stat, data);
}


exports.processData = processRequest;
=======
function processRequest(req,res,data,srcn){
       var totalRows = 0;
       var orgId = data.busName.substring(0,3);
       var currseqnum;
       var next_seq_str;
      
       //use same srcn_sequence table but instead of current date use
       //business type
       //for now business type is hard-coded to 'C'
       //when new businesses are added take from business type master
       //also not using promise for other db queries
       //since this is a stop gap arrangement when moving to Admin workflow
       //do everything correctly
      var promise = db.get('srcn_sequence').find({running_date:'C'},{stream: true});
         promise.each(function(doc){
           totalRows++;
           currseqnum = doc.running_sequence;
           }); 
         promise.on('complete',function(err,doc){
           if(totalRows == 0) {
              db.get('srcn_sequence').insert({ running_date: 'C',
                                               running_sequence: '00001'
                                            });
              orgId += 'C'+'0001';                              
              createServiceStatus(req,res,data,orgId,srcn);                              
            }
            else {
            var next_sequence = parseInt(currseqnum);
                next_sequence++;
                next_seq_str = String("0000000"+next_sequence).slice(-5); 
              var promise = db.get('srcn_sequence').update({ running_date:'C'},
                                             { $set: { running_sequence: next_seq_str }
                                              });
                 orgId += 'C'+next_seq_str;                              
                 createServiceStatus(req,res,data,orgId,srcn);  
              } 
      });
     promise.on('error',function(err){
        console.log(err);
        processStatus(req,res,6,data);//system error 
      });
  
}

function createServiceStatus(req,res,data,orgId,srcn){
        var uid = data.userId;
        var totalRows = 0;
        db.get('service_status').insert({ user_id: data.userId,
                                                   srvice_req: srcn,
                                                   srvice_desc:'Subscription Request',
                                                   status: 'Approved', 
                                                   created_time: new Date()
                                               });
     
     var async = require('async');
     var utils = require('./chanceUtils');
     var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    //no error checking done now
    //do at time of integration to admin dashboard
   
   
   async.parallel({
         passWord: function(callback){utils.getPASSWORD(callback); } },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
        else {
     
         var pass_word = results.passWord;
     
         var email_text = "Dear Customer,<br/><br/> Thank you for subscription";
             email_text += "<br/>please find below details for login to your account.<br/>";
             email_text += "<br/>Organization ID: "+orgId;
             email_text += "<br/>Password: "+pass_word;
             email_text += "<br/>Thanks";
     
         var modelpath=app.get('model');
         var model = require(modelpath+'email');
             model.sendmail(data.emailId,'Account Activation mail',email_text);
     
         var hash = require('node_hash/lib/hash');
         var salt = 'mydime_##123##';
         var pass_word_enc = hash.sha256(pass_word,salt);
     
         db.get('service_status').insert({ user_id: data.userId,
                                                   srvice_req: srcn,
                                                   srvice_desc:'Business Credentials',
                                                   status: 'Sent', 
                                                   created_time: new Date()
                                               });
         //Temporary arrangement make all newly created user 
         //as super user until admin dashboard is ready 
         var userid_str = data.userId+''; //to convert objectid to string
         db.get('user_role_link').insert({user_id: userid_str,
                                          role_id: 18,
                                          role_seq: 1,
                                          created_date: new Date()
                                            });                                               
                                               
         db.get('user_master').update({ _id:uid},
                                       { $set: { user_status: 9}
                                     });
         db.get('user_detail').update({ _id:uid},
                                       { $set: { org_id: orgId,
                                                pass_word: pass_word_enc,
                                                activ_date: new Date(),
                                                new_pass: true,
                                                 }
                                      });
        }
    });
                                     
 return;

}
   
exports.processData = processRequest;

>>>>>>> DEV/master
