function getMasterTypes(tablName,callback) {

 var typeDetails = [];
 var data = {};

  var totalRows = 0;
  var promise = db.get(tablName).find({});

  promise.each(function(doc){
    totalRows++;
    var tmparray = {id:doc.type_id,name:doc.type_name};
    typeDetails.push(tmparray);
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0){
      data.errorCode = 1;
      data.types = [];
      callback(null,data)
      }
      else{
      data.errorCode = 0;
      data.types = typeDetails;
      callback(null,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}

//TODO change to getMasterStatusNames
function getMasterNames(tablName,typeid,callback) {

 var typeName = "";
 var data = {};

  var totalRows = 0;
  var promise = db.get(tablName).find({type_id:typeid});

  promise.each(function(doc){
    totalRows++;
    typeName = doc.type_name;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0){
      data.errorCode = 1;
      data.typeName = "";
      callback(null,data)
      }
      else{
      data.errorCode = 0;
      data.typeName = typeName;
      callback(null,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}

function getMasterStatus(tablName,callback) {

 var typeDetails = [];
 var data = {};

  var totalRows = 0;
  var promise = db.get(tablName).find({});

  promise.each(function(doc){
    totalRows++;
    var tmparray = {id:doc.status_id,name:doc.status_name};
    statusDetails.push(tmparray);
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0){
      data.errorCode = 1;
      data.status = [];
      callback(null,data)
      }
      else{
      data.errorCode = 0;
      data.status= statusDetails;
      callback(null,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}


function getMasterStatusNames(tablName,statusid,callback) {

 var statusName = "";
 var data = {};

  var totalRows = 0;
  var promise = db.get(tablName).find({status_id:statusid});

  promise.each(function(doc){
    totalRows++;
    statusName = doc.status_name;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0){
      data.errorCode = 1;
      data.statusName = "";
      callback(null,data)
      }
      else{
      data.errorCode = 0;
      data.statusName = statusName;
      callback(null,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}

exports.masterTypes = getMasterTypes;
exports.masterNames = getMasterNames;
exports.masterStatus = getMasterStatus;
exports.masterStatusNames = getMasterStatusNames;