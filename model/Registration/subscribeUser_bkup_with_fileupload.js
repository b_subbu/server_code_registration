function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {userId:""};
  var userdata = "";
  var files = "";
   var formidable = require('formidable');
  var form = new formidable.IncomingForm();
  form.uploadDir = app.get('fileuploadfolder')+'/tmp';
  form.parse(req, function (err, fields) {
    userdata = fields;
  });
  form.on('file',function(name,file) {
    files = file;
  });
  form.on('end',function() {
    processInput(req,res,userdata,files);
  });
}

function processInput(req,res,userdata,files) {
  var totalRows = 0;
  var user_stat = "";
  var data = {userId:""};

  if( typeof userdata.userId === 'undefined')
    processStatus(req,res,6,data); //system error
  else {
    var usr_id = userdata.userId;

    var promise = db.get('user_master').find({_id:usr_id},{stream: true});
    promise.each(function(doc){
      totalRows++;
      data = {userId:doc._id,emailId:doc.email,mobileNumber:doc.mobile};
      usr_stat = doc.user_status;
    });
    promise.on('complete',function(err,doc){ 
      if(totalRows == 0)
        processStatus(req,res,1,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        if(usr_stat != 3)
          processStatus(req,res,8,data);
        else {
        //gives async call since file can be saved even later
        //so after db insert no need to wait
          createUserDetail(req,res,data,userdata,files);
          if(files)
            saveFile(req,res,data,userdata,files);
        }
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}


function saveFile(req,res,data,userdata,files) {
  var fs = require('fs');
  var mkdirp = require('mkdirp');
  var oldfile = files.path;
  var newfolder = app.get('fileuploadfolder')+userdata.userId+'/';
  var newfile = newfolder+files.name;
  mkdirp(newfolder,function(err,made){

    if(err) {
      console.log(err);
      fs.unlink(oldfile,function(err){
        if(err) {
          console.log("remove file failed");
          console.log(err);
        }
        return;
      });
    }

    else if(files.size == 0) {
      fs.unlink(oldfile,function(err){
        if(err) {
          console.log("remove empty file failed");
          console.log(err);
        }
        return;
      });
    }
    else {
        fs.rename(oldfile,newfile,function(err) {
        if(err) {
          console.log("Moving file failed");
          console.log(err);
        }
        return;
      });
    }
  });
}


function createUserDetail(req,res,data,userdata,files) {

  var filename = files.name;
  var business_logo = '/upload_files/'+userdata.userId+'/'+encodeURIComponent(filename);
  data.busName = userdata.displayName; //required for generating Org ID
  data.mobileNumber = userdata.businessMobile;
  data.businessType = parseInt(userdata.businessType);

    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var uid = userdata.userId;
  
    //for both payments and user details we send same params
    //because for subscribeuser it will have both in flat structure
     if(userdata){
       var cid =  userdata;
           cid.businessLogo = business_logo;
        }
     else
       var cid = {};
     
       
    
     async.parallel({
         usrStat: function(callback){datautils.createUsrDet(uid,cid,callback); },
         payStat: function(callback){datautils.createPayDet(uid,cid,callback); } 
         },
         function(err,results) {
            if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
          else{
               createSRCN(req,res,data);
            }
          });
 
}

function createSRCN(req,res,data) {
  var moment = require('moment');
  var totalRows = 0;
  var curr_time = moment().format('DDMMYYYY');
  var srcn = "SSC";
  var currseqnum;
  var next_seq_str;

  var promise = db.get('srcn_sequence').find({running_date:curr_time},{stream: true});
  promise.each(function(doc){
    totalRows++;
    currseqnum = doc.running_sequence;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
      db.get('srcn_sequence').insert({ 
        running_date: curr_time,
        running_sequence: '0001'
      });
      srcn += curr_time+'0001';
      createServiceStatus(req,res,data,srcn);
    }
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else {
      var next_sequence = parseInt(currseqnum);
      next_sequence++;
      next_seq_str = String("000000"+next_sequence).slice(-4);
      var promise = db.get('srcn_sequence').update({ running_date:curr_time},
      { $set: { running_sequence: next_seq_str }});
      srcn += curr_time+next_seq_str;
      createServiceStatus(req,res,data,srcn);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function createServiceStatus(req,res,data,srcn){
  var promise = db.get('service_status').insert({ 
    user_id: data.userId,
    srvice_req: srcn,
    srvice_desc:'Subscription Request',
    status: 'Submited',
    created_time: new Date()
  });
  promise.on('success',function(doc){
    finishProcess(req,res,data,srcn);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });

}


function finishProcess(req,res,data,srcn){
  var promise = db.get('service_status').insert({ 
    user_id: data.userId,
    srvice_req: srcn,
    srvice_desc:'Subscription Request',
    status: 'In Progress',
    created_time: new Date()
  });
  promise.on('success',function(doc){
    notifyAdmin(req,res,data,srcn);
    notifyUser(req,res,data,srcn);
    data.srcn = srcn;
    processStatus(req,res,9,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function notifyAdmin(req,res,data,srcn) {
  var uid = data.userId;
  var email_text = "Dear Admin,<br/><br/> New User registered";
  email_text += "please check";
  email_text += "<br/>User Id: "+data.userId;
  email_text += "<br/>User Email: "+data.emailId;
  email_text += "<br/>Service Request:"+srcn;
  email_text += "<br/>Thanks";

  var modelpath=app.get('model');
  var model = require(modelpath+'email');
  model.sendmail('mydime_admin@mydimesystems.com','New User Registration alert',email_text);

  db.get('user_master').update({ _id:uid},
  { 
    $set: { user_status: 4},
    $set: { mobile: data.mobileNumber}
  });

//Note: This is a stopgap arragnement
//The below will move to Admin flow later
  var modelpath = app.get('model');
  var model = require(modelpath+'Registration/adminApprove');

  model.processData(req,res,data,srcn);


}


function notifyUser(req,res,data,srcn) {
  var uid = data.userId;
  var email_text = "Dear User,<br/><br/> Thank you for Subscribing to MyDime ";
  email_text += "<br/>You can check your service status request with below detail anytime:";
  email_text += "<br/>Your Service Request Number:  "+srcn;
  email_text += "<br/>Thanks";

  var modelpath=app.get('model');
  var model = require(modelpath+'email');
  model.sendmail(data.emailId,'Your Subscription Confirmation Mail',email_text);
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/subscribeUser');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
