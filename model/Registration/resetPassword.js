<<<<<<< HEAD
function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {emailId:"",mobileNumber:"",userId:"",newPass:""}
  if( typeof req.body.userId === 'undefined' || typeof req.body.oldPassword === 'undefined' || typeof req.body.newPassword === 'undefined' )
  processStatus(req,res,6,data); //system error
  else {
    var user_id = req.body.userId;
    var old_pass = req.body.oldPassword;
    var new_pass = req.body.newPassword;

    var promise = db.get('user_master').find({_id: user_id},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      data = {emailId:docuser.email,mobileNumber:docuser.mobile,userId:docuser._id};
      user_stat = docuser.user_status;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,6,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        if (user_stat == 9)
          checkPassword(req,res,user_stat,old_pass,new_pass,data);
        else
          processStatus(req,res,1,data);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}

function  checkPassword(req,res,user_stat,opass,npass,data){
  var totalRows = 0;
  var pass_word = "";
  var uid = data.userId;
  var hash = require('node_hash/lib/hash');
  var salt = 'mydime_##123##';
  var opass_enc = hash.sha256(opass,salt);
  var npass_enc = hash.sha256(npass,salt);

  var promise = db.get('user_detail').find({_id: uid},{stream: true});
  promise.each(function(doc){
    totalRows++;
    pass_word = doc.pass_word;
    data.orgId = doc.org_id;
    data.displayName = doc.bus_name;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else  {
      if( pass_word != opass_enc)
        processStatus(req,res,2,data);
      else
        updatePassword(req,res,user_stat,opass_enc,npass_enc,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function  updatePassword(req,res,user_stat,opass,npass,data){
  var totalRows = 0;
  var uid = data.userId;
  debuglog("also set cust master password whenever business user resets password");
  debuglog("If no custmaster found then no issue it will be just ignored");
  debuglog("TODO add check and update if no performance issue  is there and add promise");
  db.get('cust_master').update(
     {cust_email:data.emailId},
     {$set: {pass_word: npass} },
     {upsert: false}
  );
  var promise = db.get('user_detail').update({ _id:uid},
  { 
    $set: { 
      pass_word: npass,
      new_pass: false,
    }
  });
  promise.on('success',function(err,doc){
    updateUserStatus(req,res,user_stat,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function  updateUserStatus(req,res,stat,data){
  var totalRows = 0;
  var uid = data.userId;
  
  var tblName = data.orgId+'_users';
  var promise = db.get(tblName).update(
  { userEmail:data.emailId},
  { $set: { status_id:84}}
  );
  promise.on('success',function(err,doc){
    processStatus(req,res,stat,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/resetPassword');

    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {emailId:"",mobileNumber:"",userId:"",newPass:""}
   if( typeof req.body.userId === 'undefined' || typeof req.body.oldPassword === 'undefined' || typeof req.body.newPassword === 'undefined' )
       processStatus(req,res,6,data); //system error
   else {
    var user_id = req.body.userId;
    var old_pass = req.body.oldPassword;
    var new_pass = req.body.newPassword;
   
    var promise = db.get('user_master').find({_id: user_id},{stream: true});
    promise.each(function(docuser){
         totalRows++;
         data = {emailId:docuser.email,mobileNumber:docuser.mobile,userId:docuser._id};
         user_stat = docuser.user_status;
        }); 
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,6,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else { 
           if (user_stat == 9)
            checkPassword(req,res,user_stat,old_pass,new_pass,data);
          else
            processStatus(req,res,1,data);
         } 
      });
    promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
      });
  }
}

function  checkPassword(req,res,user_stat,opass,npass,data){
    var totalRows = 0;
    var pass_word = "";
    var uid = data.userId;
    var hash = require('node_hash/lib/hash');
    var salt = 'mydime_##123##';
    var opass_enc = hash.sha256(opass,salt);
    var npass_enc = hash.sha256(npass,salt);
     
    var promise = db.get('user_detail').find({_id: uid},{stream: true});
    promise.each(function(doc){
        totalRows++;
        pass_word = doc.pass_word;
        data.orgId = doc.org_id;
        //data.businessLogo = doc.bus_logo;
        data.businessName = doc.bus_name;
      }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,6,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else  {
         if( pass_word != opass_enc)
             processStatus(req,res,2,data);
         else
             updatePassword(req,res,user_stat,opass_enc,npass_enc,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
     });
}

function  updatePassword(req,res,user_stat,opass,npass,data){
    var totalRows = 0;
    var uid = data.userId;
    var promise = db.get('user_detail').update({ _id:uid},
                                       { $set: { pass_word: npass,
                                                 new_pass: false,
                                                 }
                                   });
     promise.on('success',function(err,doc){
               processStatus(req,res,user_stat,data);
        });
    promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
       });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/resetPassword');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;


>>>>>>> DEV/master
