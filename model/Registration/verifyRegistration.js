<<<<<<< HEAD
function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {userId:""};
  var eotp="";
  var motp = "";
  var num_attempt="";
  var create_time="";
  var uid ="";
  var moment = require('moment');
  var curr_time = moment();

  if( typeof req.body.urlCode === 'undefined' || typeof req.body.emailOtp === 'undefined' || typeof req.body.mobileOtp == 'undefined' )
    processStatus(req,res,6,data); //system error
  else {
    var url_code = req.body.urlCode;
    var email_otp = req.body.emailOtp;
    var mob_otp = req.body.mobileOtp;

    var promise = db.get('user_otp').find({url_code:url_code},{stream: true});
    promise.each(function(doc){
      totalRows++;
      data = {userId:doc.user_id,urlCode:doc.url_code};
      eotp = doc.email_otp;
      motp = doc.mobile_otp;
      num_attempt = doc.num_attempts;
      create_time = moment(doc.created_time);
      uid = doc.user_id;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,1,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
//for all cases we update user_master status to 2 (authentication rejected)
//before processing each status
//on successful authentication it will be updated to status 3
//not using promise here since it is update by id and not expected
//to fail and no need to wait for status before return

        var hash = require('node_hash/lib/hash');
        var salt = 'mydime_##123##';
        var email_otp_enc = hash.sha256(email_otp,salt);

        db.get('user_master').update({ _id:uid},
        { $set: { user_status: 2}});
        secondsDiff = curr_time.diff(create_time, 'seconds');
       //note: both node and mongodb server must have same time and zone
        if(secondsDiff > 60*60) {
          regenerateOTP(req,res,2,data);
          return;
        }
        if (num_attempt > 5) {
          regenerateOTP(req,res,3,data);
          return;
        }
        if (eotp != email_otp_enc) {
          updateNumAttempts(req,res,7,data,num_attempt);
          return;
        }
        if (motp  != mob_otp) {
          updateNumAttempts(req,res,8,data,num_attempt);
          return;
        }
        completeAuthentication(req,res,data);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}


function updateNumAttempts(req,res,stat,data,nattmpt) {
  var urlCode = data.urlCode;
  nattmpt++;

  var promise = db.get('user_otp').update(
     { url_code:urlCode},
     { $set: { num_attempts: nattmpt }});
  
    promise.on('success',function(doc){
       processStatus(req,res,stat,data);
    });
   promise.on('error',function(err){
     console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function regenerateOTP(req,res,stat,data){
  var uid = data.userId;
  var totalRows = 0;
  var promise = db.get('user_master').find({_id:uid});
  promise.each(function(doc){
    totalRows++;
    data.emailId = doc.email;
    data.mobileNumber = doc.mobile;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else
      processgenerateOTP(req,res,stat,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function processgenerateOTP(req,res,stat,data){

  var async = require('async');
  var utils = require('./chanceUtils');
  var isError = 0;

//call all functions async in parallel
//but proceed only after completion and checks
//no error checking done now
//do at time of integration to admin dashboard


  async.parallel({
    eOTP: function(callback){utils.getEOTP(callback); },
    mOTP: function(callback){utils.getMOTP(callback); } 
  },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    else {
      emailOTP = results.eOTP;
      mobileOTP = results.mOTP;
      var url_code = data.urlCode; //needed for update of OTP on expiry

      var urlCode = encodeURIComponent(data.urlCode);
      var base_url = global.baseURL;

//Below 2 are async calls so insert into db will not
//wait until email/sms is sent if email/sms fails
//need to do manual
//sms is disabled for now until mydime confirmation
//here OTP is plain but while saving in db it will be encrypted

      var email_text = "Dear Customer,<br/><br/> Thank you for registration";
      email_text += "Your verfication attempt has failed. So please try again with below credentials";
      email_text += "please click <a href="+base_url+"subscription/#/verify/"+urlCode+">link</a> to confirm your account.<br/>";
      email_text += "<br/>Email OTP: "+emailOTP;
//email_text += "<br/>Mobile OTP: "+mobileOTP;
      email_text += "<br/>Thanks";

      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendmail(data.emailId,'OTP Regenerated mail',email_text);
//model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);

      var hash = require('node_hash/lib/hash');
      var salt = 'mydime_##123##';
      var email_otp_enc = hash.sha256(emailOTP,salt);

      var promise = db.get('user_otp').update(
      { url_code:url_code},
      {$set: { email_otp:email_otp_enc,
        mobile_otp:mobileOTP,
        num_attempts: 0,
        created_time: new Date() }
      });
      promise.on('success',function(doc){
        data= {emailId:data.emailId,mobileNumber:data.mobileNumber,emailOtp:emailOTP,mobileOtp:mobileOTP,userId:data.userId,urlCode:urlCode};
        processStatus(req,res,stat,data);
      });
      promise.on('error',function(err){
        console.log(err);
        processStatus(req,res,6,data);//system error
      });
    }
  });
}

function completeAuthentication(req,res,data){

//-->TODO: delete from user_otp table for this entry
//or archive to a audit table or log in json as required
  var uid = data.userId;

  var totalRows = 0;
  var promise = db.get('user_master').find({_id:uid});

  promise.each(function(doc){
    totalRows++;
    data.emailId = doc.email;
    data.mobileNumber = doc.mobile;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else{
      db.get('user_master').update
      ({ _id:uid},
      {$set: { user_status: 3}});
      getUserDetail(req,res,uid,3,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function  getUserDetail(req,res,uid,user_stat,data){
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    data.subscription = {};
    var usr_id_str = uid+'';
    
     async.parallel({
         usrStat: function(callback){datautils.usrDets(uid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.usrStat.errorCode != 0 && results.usrStat.errorCode !=1)) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
         if(isError == 0) {
              data.subscription.businessDetails = results.usrStat.subscription;
              getMasterTypes(req,res,9,data);
              }
          }); 
}
function getMasterTypes(req,res,stat,data){
    
    var async = require('async');
    var mastutils = require('./masterUtils');
    var isError = 0;
    
     async.parallel({
         currStat: function(callback){mastutils.masterTypes('currency_type_master',callback); },
         bizStat: function(callback){mastutils.masterTypes('business_type_master',callback);} 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.currStat.errorCode != 0 && results.currStat.errorCode !=1)) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && (results.bizStat.errorCode != 0 && results.bizStat.errorCode !=1)) {
             processStatus (req,res,results.bizStat.errorCode,data);
             isError = 1;
             return;
            }
          if(isError == 0) {
              data.businessTypes = results.bizStat.types;
              data.currencies = results.currStat.types;
              processStatus(req,res,9,data);
            }
          }); 
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/verifyRegistration');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {userId:""};
   var eotp="";
   var motp = "";
   var num_attempt="";
   var create_time="";
   var uid ="";
   var moment = require('moment');
   var curr_time = moment();
   
   if( typeof req.body.urlCode === 'undefined' || typeof req.body.emailOtp === 'undefined' || typeof req.body.mobileOtp == 'undefined' )
      processStatus(req,res,6,data); //system error
   else {
    var url_code = req.body.urlCode;
    var email_otp = req.body.emailOtp;
    var mob_otp = req.body.mobileOtp;
   
   var promise = db.get('user_otp').find({url_code:url_code},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data = {userId:doc.user_id,urlCode:doc.url_code};
         eotp = doc.email_otp;
         motp = doc.mobile_otp;
         num_attempt = doc.num_attempts;
         create_time = moment(doc.created_time);
         uid = doc.user_id;
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,1,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else { 
            //for all cases we update user_master status to 2 (authentication rejected)
            //before processing each status
            //on successful authentication it will be updated to status 3 
            //not using promise here since it is update by id and not expected
            //to fail and no need to wait for status before return
            
         var hash = require('node_hash/lib/hash');
         var salt = 'mydime_##123##';
         var email_otp_enc = hash.sha256(email_otp,salt);
        
         db.get('user_master').update({ _id:uid},
                                             { $set: { user_status: 2}
                                             }); 
             secondsDiff = curr_time.diff(create_time, 'seconds');
             //note: both node and mongodb server must have same time and zone
             if(secondsDiff > 60*60) {
                regenerateOTP(req,res,2,data);
                return;
                }
             if (num_attempt > 5) { 
                regenerateOTP(req,res,3,data);
                return;
                }
             if (eotp != email_otp_enc) {
                 updateNumAttempts(req,res,7,data,num_attempt);
                 return;
                }
             if (motp  != mob_otp) { 
                updateNumAttempts(req,res,8,data,num_attempt);
                return;
               }
           completeAuthentication(req,res,data);
         } 
      });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
  });
 } 
}


function updateNumAttempts(req,res,stat,data,nattmpt) {
   var urlCode = data.urlCode;
   nattmpt++;
 
   var promise = db.get('user_otp').update({ url_code:urlCode},
                                             { $set: { num_attempts: nattmpt }
                                              });
    promise.on('success',function(doc){
         processStatus(req,res,stat,data);
    });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
  });


}

function regenerateOTP(req,res,stat,data){
   var uid = data.userId;
   var totalRows = 0;
   var promise = db.get('user_master').find({_id:uid});
   promise.each(function(doc){
        totalRows++;
        data.emailId = doc.email;
        data.mobileNumber = doc.mobile;
   }); 
   promise.on('complete',function(err,doc){
       if(totalRows == 0)
         processStatus(req,res,6,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else 
         processgenerateOTP(req,res,stat,data);
         
   });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
   });
    
}

function processgenerateOTP(req,res,stat,data){


    var async = require('async');
    var utils = require('./chanceUtils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    //no error checking done now
    //do at time of integration to admin dashboard
   
   
   async.parallel({
         eOTP: function(callback){utils.getEOTP(callback); },
         mOTP: function(callback){utils.getMOTP(callback); } },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
        else {
               emailOTP = results.eOTP;
               mobileOTP = results.mOTP;
               var url_code = data.urlCode; //needed for update of OTP on expiry
              
              var urlCode = encodeURIComponent(data.urlCode); 
               //Below 2 are async calls so insert into db will not
               //wait until email/sms is sent if email/sms fails
               //need to do manual
               //sms is disabled for now until mydime confirmation
               //here OTP is plain but while saving in db it will be encrypted
               
              var email_text = "Dear Customer,<br/><br/> Thank you for registration";
                   email_text += "Your verfication attempt has failed. So please try again with below credentials";
                   email_text += "please click <a href='http://ec2-54-213-174-132.us-west-2.compute.amazonaws.com/subscription/#/verify/"+urlCode+"'>link</a> to confirm your account.<br/>";
                   email_text += "<br/>Email OTP: "+emailOTP;
                   //email_text += "<br/>Mobile OTP: "+mobileOTP;
                   email_text += "<br/>Thanks";
             
              var modelpath=app.get('model');
              var model = require(modelpath+'email');
                  model.sendmail(data.emailId,'OTP Regenerated mail',email_text);
                 //model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);
              
              var hash = require('node_hash/lib/hash');
              var salt = 'mydime_##123##';
              var email_otp_enc = hash.sha256(emailOTP,salt);
             
              var promise = db.get('user_otp').update({ url_code:url_code},
                                                     { $set: { email_otp:email_otp_enc, 
                                                       mobile_otp:mobileOTP,
                                                       num_attempts: 0,
                                                       created_time: new Date() }
                                                      });
              promise.on('success',function(doc){
                   data= {emailId:data.emailId,mobileNumber:data.mobileNumber,emailOtp:emailOTP,mobileOtp:mobileOTP,userId:data.userId,urlCode:urlCode};
                  processStatus(req,res,stat,data);
             });
             promise.on('error',function(err){
                 console.log(err);
                 processStatus(req,res,6,data);//system error 
             });
        }
  });
}

function completeAuthentication(req,res,data){

      //-->TODO: delete from user_otp table for this entry
      //or archive to a audit table or log in json as required
     var uid = data.userId;
   
    
     var totalRows = 0;
     var promise = db.get('user_master').find({_id:uid});
 
    promise.each(function(doc){
        totalRows++;
        data.emailId = doc.email;
        data.mobileNumber = doc.mobile;
    }); 
   promise.on('complete',function(err,doc){
       if(totalRows == 0)
         processStatus(req,res,6,data); 
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else 
         {
           db.get('user_master').update({ _id:uid},
                                       { $set: { user_status: 3}
                                 });
  
          getUserDetail(req,res,uid,3,data);
         }
   });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
       });

 }
 
 
 function  getUserDetail(req,res,uid,user_stat,data){
    var totalRows = 0;
    var promise = db.get('user_detail').find({_id: uid},{stream: true});
    promise.each(function(doc){
        totalRows++;
        data.businessName = doc.bus_name;
        data.businessAddress = doc.bus_addr;
        data.businessEmail = doc.bus_email;
        data.businessMobile = doc.bus_mobile;
        data.businessType = doc.bus_type;
        data.businessBranches = doc.bus_branch;
        data.businessUrl = doc.bus_url;
        data.buinsessLogoUrl = doc.bus_logo;
        data.businessDescription = doc.bus_descr;
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
         getBusinessTypes(req,res,9,data);//no partial save case
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else
         getBusinessType(req,res,user_stat,data);
      });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
     });
}

function  getBusinessType(req,res,user_stat,data){
    var totalRows = 0;
    var bus_type = parseInt(data.businessType);
    
       
    var promise = db.get('business_type_master').find({business_id: bus_type},{stream: true});
    promise.each(function(doc){
         totalRows++;
         data.businessType = doc.business_type;
         }); 
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
         getBusinessTypes(req,res,9,data); //no business types found is not error user had not yet saved type
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //business type error
      else 
         getBusinessTypes(req,res,9,data);
      });
    promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
      });
}

function getBusinessTypes(req,res,stat,data) {      
     var businessTypes = [];
   
    
     var totalRows = 0;
     var promise = db.get('business_type_master').find({});
 
    promise.each(function(doc){
        totalRows++;
        var temparray = {id:doc.business_id,name:doc.business_type};
        businessTypes.push(temparray);
    }); 
    promise.on('complete',function(err,doc){
       if(totalRows == 0)
         processStatus(req,res,6,data);
      else 
         {
         data.businessTypes = businessTypes;
         processStatus(req,res,9,data);
         }
   });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
       });
    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/verifyRegistration');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;


>>>>>>> DEV/master
