<<<<<<< HEAD
function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {emailId:"",mobileNumber:"",userId:"",newPass:""}
  if( typeof req.body.emailId === 'undefined' || typeof req.body.orgId === 'undefined' || typeof req.body.passWord === 'undefined' )
    processStatus(req,res,6,data); //system error
  else
    checkMasterStatus(req,res,9,data);
    
}
 
 //NOTE not using generic satus check utils
 //because password and logged-in is only for login user API
function checkMasterStatus(req,res,stat,data){ 
    var user_email = req.body.emailId;
    var user_stat = 0;
    var user_loggedin = false;
    var totalRows = 0;
    
    var promise = db.get('user_master').find({email:user_email},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      data = {emailId:docuser.email,mobileNumber:docuser.mobile,userId:docuser._id};
      userStatus = docuser.user_status;
      userLoggedin = docuser.logged_in;
      sessionToken = docuser.sessionToken;
      sessionTimestamp = docuser.sessionTimestamp;
      
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,1,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
       if(userStatus == 9)
         processLogin(req,res,9,data,userLoggedin,sessionToken,sessionTimestamp);
       else
         processStatus(req,res,8,data);
       }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }

function processLogin(req,res,stat,data,userLoggedIn,sessionToken,sessionTimeStamp){
     var user_email = req.body.emailId;
     var moment = require('moment');
     var chance = require('chance').Chance();
     var sessionToken = chance.string({length:8, pool: 'abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ023456789' });
 
     var sessionTimestamp= new Date();
     sessionTimestamp.setMinutes(sessionTimestamp.getMinutes()+20);
      
     if(userLoggedIn == true){
      //check if now > sessionTimeStamp+20 minutes
      //if yes then generate new session token
      //set sessiontimestamp = now+20 minutes
      //set data.sessionToken = token value
      
      var nowTime = new Date();
      var sessTime = new Date(sessionTimeStamp);
      //date in monogdb is ISO timestamp
      
      var diffTime = (nowTime-sessTime)/(1000*60*20);
      
      if(diffTime <= 0)
           processStatus(req,res,4,data); //already logged in message
    
      else {
         data.sessionToken = sessionToken;
        var promise = db.get('user_master').update(
                         {email:user_email},
                         {$set:{
                                sessionToken: sessionToken,
                                sessionTimestamp: sessionTimestamp}
                        },
                        {upsert:false});
      promise.on('success',function(err,doc){
         getUserDetail(req,res,9,data);
      });
      promise.on('error',function(err){
         processStatus(req,res,6,data);
      });
    }
  }
  else {
     //generate sessionToken
     //set sessionTimestamp = now+20 minutes
     //set data.sessionToken = token value
     data.sessionToken = sessionToken;
     var promise = db.get('user_master').update(
                         {email:user_email},
                         {$set:{
                               sessionToken: sessionToken,
                               sessionTimestamp: sessionTimestamp}
                          },
                          {upsert:false});
      promise.on('success',function(err,doc){
           getUserDetail(req,res,9,data);
      });
      promise.on('error',function(err){
           processStatus(req,res,6,data);
      });
   }
}

function  getUserDetail(req,res,user_stat,data){
  var totalRows = 0;
  var org_id = "";
  var pass_word = "";
  var uid = data.userId;
  var promise = db.get('user_detail').find({_id: uid},{stream: true});
  promise.each(function(doc){
    totalRows++;
    org_id = doc.org_id;
    pass_word = doc.pass_word;
    data.newPass = doc.new_pass;
    data.orgId = org_id;
    data.displayName = doc.bus_name;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else  {

      var password = req.body.passWord;
      var hash = require('node_hash/lib/hash');
      var salt = 'mydime_##123##';
      var pass_word_enc = hash.sha256(password,salt);
   
      if( org_id != req.body.orgId)
        processStatus(req,res,2,data);
      else if(pass_word != pass_word_enc)
        processStatus(req,res,3,data);
      else
         updateIsLoggedIn(req,res,9,data);
        //processStatus(req,res,9,data);
    }

  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function updateIsLoggedIn(req,res,stat,data){ 
    var user_email = req.body.emailId;
   var promise = db.get('user_master').update(
                   {email:user_email},
                   {$set:{logged_in: true}},
                    {multi: false, upsert: false});
    promise.on('success',function(err,doc){
         updateUserStatus(req,res,9,data);
      });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }

function  updateUserStatus(req,res,stat,data){
  var totalRows = 0;
  var uid = data.userId;
  debuglog("This is to handle invitation based users who does not");
  debuglog("Have firstlogin reset password");
  
  var tblName = data.orgId+'_users';
  var promise = db.get(tblName).update(
  { userEmail:data.emailId},
  { $set: { status_id:84}}
  );
  promise.on('success',function(err,doc){
    processStatus(req,res,stat,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/loginUser');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {emailId:"",mobileNumber:"",userId:"",newPass:""}
   if( typeof req.body.emailId === 'undefined' || typeof req.body.orgId === 'undefined' || typeof req.body.passWord === 'undefined' )
      processStatus(req,res,6,data); //system error
   else {
    var user_email = req.body.emailId;
    var org_id = req.body.orgId;
    var pass_word = req.body.passWord;
   
    var promise = db.get('user_master').find({email:user_email},{stream: true});
    promise.each(function(docuser){
        totalRows++;
        data = {emailId:docuser.email,mobileNumber:docuser.mobile,userId:docuser._id};
        user_stat = docuser.user_status;
         }); 
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,1,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else { 
           if (user_stat == 9)
            getUserDetail(req,res,user_stat,data);
          else
            processStatus(req,res,8,data);
         } 
      });
    promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data,folder,controller);//system error 
      });
  }
}

function  getUserDetail(req,res,user_stat,data){
    var totalRows = 0;
    var org_id = "";
    var pass_word = "";
    var uid = data.userId;
    var promise = db.get('user_detail').find({_id: uid},{stream: true});
    promise.each(function(doc){
        totalRows++;
        org_id = doc.org_id;
        pass_word = doc.pass_word;
        data.newPass = doc.new_pass;
        data.orgId = org_id;
        //data.businessLogo = doc.bus_logo;
        data.businessName = doc.bus_name;
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,6,data);
      else if(totalRows > 1)  
         processStatus(req,res,6,data); //system error should be 1 row only
      else  {
      
         var password = req.body.passWord
         var hash = require('node_hash/lib/hash');
         var salt = 'mydime_##123##';
         var pass_word_enc = hash.sha256(password,salt);
  
         if( org_id != req.body.orgId)
             processStatus(req,res,2,data);
         else if(pass_word != pass_word_enc)
            processStatus(req,res,3,data);
         else
            processStatus(req,res,9,data);
       }
         
      });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
  });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/loginUser');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;

>>>>>>> DEV/master
