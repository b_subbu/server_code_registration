<<<<<<< HEAD
/**
*@function processRequest
*@param user_email required
*checks user_master.email
*No rows, Greater than 1 row is returned as system error
*If Status is 1,2,3,11 (REf: Registration_flow_SDS_3.2)
*function Get urlCode {@link:processGetUrlCode} is called
*Else flow returns to controller with the user status
*/
function processRequest(req,res,folder,controller){
  var totalRows = 0;
  var user_stat = "";
  var data = {emailId:"",mobileNumber:"",userId:"",urlCode:""};
  if( typeof req.body.emailId === 'undefined')
  processStatus(req,res,6,data,folder,controller); //system error
  else {
    var user_email = req.body.emailId;

    var promise = db.get('user_master').find({$or: [{email:user_email}]},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      data = {emailId:docuser.email,mobileNumber:docuser.mobile,userId:docuser._id};
      user_stat = docuser.user_status;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,0,data,folder,controller);
      else if(totalRows > 1)
        processStatus(req,res,6,data,folder,controller); //system error should be 1 row only
      else {
        if (user_stat == 1 || user_stat == 2 || user_stat == 3 || user_stat == 11)
          processGetUrlCode(req,res,user_stat,data,folder,controller);
        else
          processStatus(req,res,user_stat,data,folder,controller);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data,folder,controller);//system error
    });
  }
}
/**
*@function processGetUrlCode
*checks user_otp for the given userId
*No rows OR Greater than 1 row is returned as system error
*If user status is 3 (REf: Registration_flow_SDS_3.2)
*and OTP generation time is less than 3 minutes response code 7
*is returned
*Else flow returns to controller with the user status
*/


function processGetUrlCode(req,res,user_stat,data,folder,controller){
  var totalRows = 0;
  var moment = require('moment');
  var curr_time = moment();

  var promise = db.get('user_otp').find({user_id: data.userId});
  promise.each(function(doc){
    totalRows++;
    data.urlCode = doc.url_code;
    create_time = moment(doc.created_time);
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data,folder,controller);
    else if(totalRows > 1)
      processStatus(req,res,6,data,folder,controller); //system error should be 1 row only
    else {
      if(user_stat == 3) {
        secondsDiff = curr_time.diff(create_time, 'seconds');
        if(secondsDiff > 60*3)
          processStatus(req,res,user_stat,data,folder,controller);
        else
          processStatus(req,res,7,data,folder,controller);
      }
      else
        processStatus(req,res,user_stat,data,folder,controller);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data,folder,controller);//system error
  });
}


function processStatus(req,res,stat,data,folder,controller) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+folder+'/'+controller);

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
=======
function processRequest(req,res,folder,controller){
   var totalRows = 0;
   var user_stat = "";
   var data = {emailId:"",mobileNumber:"",userId:"",urlCode:""};
   if( typeof req.body.emailId === 'undefined')
      processStatus(req,res,6,data,folder,controller); //system error
   else { 
   var user_email = req.body.emailId;
   
   var promise = db.get('user_master').find({$or: [{email:user_email}]},{stream: true});
   promise.each(function(docuser){
        totalRows++;
        //hardcode urlcode until otp generation program is ready
        data = {emailId:docuser.email,mobileNumber:docuser.mobile,userId:docuser._id};
        user_stat = docuser.user_status;
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
         processStatus(req,res,0,data,folder,controller);
      else if(totalRows > 1)  
         processStatus(req,res,6,data,folder,controller); //system error should be 1 row only
      else { 
           if (user_stat == 1 || user_stat == 2 || user_stat == 3 || user_stat == 11)
            processGetUrlCode(req,res,user_stat,data,folder,controller);
          else
            processStatus(req,res,user_stat,data,folder,controller);
         } 
      });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data,folder,controller);//system error 
  });
 } 
}


function processGetUrlCode(req,res,user_stat,data,folder,controller){
   var totalRows = 0;
   var moment = require('moment');
   var curr_time = moment();

   var promise = db.get('user_otp').find({user_id: data.userId});
   promise.each(function(doc){
        totalRows++;
        data.urlCode = doc.url_code;
        create_time = moment(doc.created_time);
       }); 
   promise.on('complete',function(err,doc){
       if(totalRows == 0)
         processStatus(req,res,6,data,folder,controller);
      else if(totalRows > 1)  
         processStatus(req,res,6,data,folder,controller); //system error should be 1 row only
      else { 
              if(user_stat == 3) {
                secondsDiff = curr_time.diff(create_time, 'seconds');
                if(secondsDiff > 60*3) 
                   processStatus(req,res,user_stat,data,folder,controller);
                 else
                   processStatus(req,res,7,data,folder,controller);
               }
            else
             processStatus(req,res,user_stat,data,folder,controller);
         }
       });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data,folder,controller);//system error 
       });
 } 

    
function processStatus(req,res,stat,data,folder,controller) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+folder+'/'+controller);
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
 
>>>>>>> DEV/master
