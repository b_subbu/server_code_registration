<<<<<<< HEAD
function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {}
  if( typeof req.body.emailId === 'undefined' )
      processStatus(req,res,6,data); //system error
  else {
    var user_email = req.body.emailId;
//var user_mobile = req.body.mobileNumber;   //mobile number hidden in registration now
    var user_mobile = 0;

    var promise = db.get('user_master').insert({ email:user_email,
    mobile: user_mobile,
    user_status:1});
    promise.on('success',function(doc){
      data= {emailId:user_email,userId:doc._id};
      createUserOTP(req,res,data);
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}

function createUserOTP(req,res,data){

  var async = require('async');
  var utils = require('./chanceUtils');
  var isError = 0;

//call all functions async in parallel
//but proceed only after completion and checks
//no error checking done now
//do at time of integration to admin dashboard


  async.parallel({
    eOTP: function(callback){utils.getEOTP(callback); },
    mOTP: function(callback){utils.getMOTP(callback); },
    urlcode: function(callback){utils.getURLCODE(callback); }
  },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    else {
      //data.emailOtp = results.eOTP;
      //data.mobileOtp = results.mOTP;
      data.urlCode = results.urlcode;
      emailOTP = results.eOTP;
      mobileOTP = results.mOTP;

      var eurlCode = encodeURIComponent(results.urlcode);
      var base_url = global.baseURL;


//Below 2 are async calls so insert into db will not
//wait until email/sms is sent if email/sms fails
//need to do manual
//sms is disabled for now until mydime confirmation
//here OTP is plain but while saving in db it will be encrypted

      var email_text = "Dear Customer,<br/><br/> Thank you for registration";
      email_text += "please click <a href="+base_url+"subscription/#/verify/"+eurlCode+">link</a> to confirm your account.<br/>";
      email_text += "<br/>Email OTP: "+emailOTP;
      email_text += "<br/>Thanks";

      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendmail(data.emailId,'Registration confirmation mail',email_text);
      //model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);
      //Note: ecnryption is not centralized to prevent attacks
      var hash = require('node_hash/lib/hash');
      var salt = 'mydime_##123##';
      var emailOTPenc = hash.sha256(emailOTP,salt);

      var promise = db.get('user_otp').insert({ user_id: data.userId,
      email_otp:emailOTPenc,
      mobile_otp:mobileOTP,
      num_attempts: 0,
      created_time: new Date(),
      url_code: results.urlcode});
      promise.on('success',function(doc){
        processStatus(req,res,9,data);
      });
      promise.on('error',function(err){
        console.log(err);
        processStatus(req,res,6,data);//system error
      });
    }
  });
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/generateRegistration');

  controller.process_create(req,res,stat,data);
}

exports.createUser = processRequest;
=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {emailId:"",mobileNumber:"",userId:"",urlCode:""}
   if( typeof req.body.emailId === 'undefined' )
      processStatus(req,res,6,data); //system error
   else {
    var user_email = req.body.emailId;
    //var user_mobile = req.body.mobileNumber;   //mobile number hidden in registration now
    var user_mobile = 0;
   
    var promise = db.get('user_master').insert({ email:user_email, 
                                                 mobile: user_mobile,
                                                 user_status:1});
    promise.on('success',function(doc){
         data= {emailId:user_email,userId:doc._id};
         createUserOTP(req,res,data);
    });
    promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);//system error 
    });
  }
}

function createUserOTP(req,res,data){

    var async = require('async');
    var utils = require('./chanceUtils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    //no error checking done now
    //do at time of integration to admin dashboard
   
   
   async.parallel({
         eOTP: function(callback){utils.getEOTP(callback); },
         mOTP: function(callback){utils.getMOTP(callback); },
         urlcode: function(callback){utils.getURLCODE(callback); }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
        else {
               data.emailOtp = results.eOTP;
               data.mobileOtp = results.mOTP;
               data.urlCode = results.urlcode;
               emailOTP = results.eOTP;
               mobileOTP = results.mOTP;
               
              var eurlCode = encodeURIComponent(results.urlcode);
 
              //Below 2 are async calls so insert into db will not
             //wait until email/sms is sent if email/sms fails
             //need to do manual
             //sms is disabled for now until mydime confirmation
             //here OTP is plain but while saving in db it will be encrypted
             
             var email_text = "Dear Customer,<br/><br/> Thank you for registration";
                 email_text += "please click <a href='http://ec2-54-213-174-132.us-west-2.compute.amazonaws.com/subscription/#/verify/"+eurlCode+"'>link</a> to confirm your account.<br/>";
                 email_text += "<br/>Email OTP: "+emailOTP;
                 //email_text += "<br/>Mobile OTP: "+mobileOTP;
                 email_text += "<br/>Thanks";
           
            var modelpath=app.get('model');
            var model = require(modelpath+'email');
                model.sendmail(data.emailId,'Registration confirmation mail',email_text);
               //model.sendSMS(data.mobileNumber,emailOTP,mobileOTP,email_txt);
               var hash = require('node_hash/lib/hash');
               var salt = 'mydime_##123##';
               var emailOTPenc = hash.sha256(emailOTP,salt);
               
               var promise = db.get('user_otp').insert({ user_id: data.userId,
                                                     email_otp:emailOTPenc, 
                                                     mobile_otp:mobileOTP,
                                                     num_attempts: 0,
                                                     created_time: new Date(),
                                                     url_code: results.urlcode});
          promise.on('success',function(doc){
               data= {emailId:data.emailId,mobileNumber:data.mobileNumber,emailOtp:emailOTP,mobileOtp:mobileOTP,userId:data.userId,urlCode:results.urlcode};
               processStatus(req,res,9,data);
          });
         promise.on('error',function(err){
             console.log(err);
             processStatus(req,res,6,data);//system error 
         });
     }
 });
}


   
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Registration/generateRegistration');
  
    controller.process_create(req,res,stat,data);
}
   
exports.createUser = processRequest;

>>>>>>> DEV/master
