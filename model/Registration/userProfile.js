<<<<<<< HEAD
function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {userId:""}
  if( typeof req.body.userId === 'undefined')
    processStatus(req,res,6,data); //system error
  else {
    var user_id = req.body.userId;

    var promise = db.get('user_master').find({_id: user_id},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      data = {userId:docuser._id};
      user_stat = docuser.user_status;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,6,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        if (user_stat == 9)
          getUserDetail(req,res,user_id,user_stat,data);
        else
          processStatus(req,res,1,data);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}

function  getUserDetail(req,res,uid,user_stat,data){

    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    data.subscription = {};
    
     async.parallel({
         usrStat: function(callback){datautils.usrDets(uid,callback); },
         payStat: function(callback){datautils.payGates(uid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.usrStat.errorCode != 0 && results.usrStat.errorCode !=1)) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
          if(isError == 0 && (results.payStat.errorCode != 0 && results.payStat.errorCode !=1)) {
              processStatus (req,res,results.payStat.errorCode,data);
              isError = 1;
              return;
             }
      
            if(isError == 0) {
              data.subscription.businessDetails = results.usrStat.subscription;
              data.subscription.paymentGateway  = results.payStat.gateways;
              getMasterNames(req,res,9,data);
              }
          }); 
  }

function getMasterNames(req,res,stat,data){
    
    var async = require('async');
    var mastutils = require('./masterUtils');
    var isError = 0;
  
  
    if(data.subscription.businessDetails.businessType)
       var bizid =  parseInt(data.subscription.businessDeails.businessType);
     else
       var bizid = 0;
  
     if(data.subscription.businessDetails.baseCurrency)
       var cid =  data.subscription.businessDeails.baseCurrency;
     else
       var cid = '';
     
     async.parallel({
         bizStat:  function(callback){mastutils.masterNames('business_type_master',bizid,callback); },
         currStat: function(callback){mastutils.masterNames('currency_type_master',cid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.currStat.errorCode != 0 && results.currStat.errorCode !=1)) {
              processStatus (req,res,8,data);
              isError = 1;
              return;
             }
         if(isError == 0 && (results.bizStat.errorCode != 0 && results.bizStat.errorCode !=1)) {
              processStatus (req,res,8,data);
              isError = 1;
              return;
             }
        
            if(isError == 0) {
              data.subscription.businessDetails.baseCurrency = results.currStat.typeName;
              processStatus(req,res,9,data);
            }
          }); 
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/userProfile');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;


=======
function processRequest(req,res){
  var totalRows = 0;
  var user_stat = "";
  var data = {userId:""}
  if( typeof req.body.userId === 'undefined')
    processStatus(req,res,6,data); //system error
  else {
    var user_id = req.body.userId;

    var promise = db.get('user_master').find({_id: user_id},{stream: true});
    promise.each(function(docuser){
      totalRows++;
      data = {userId:docuser._id};
      user_stat = docuser.user_status;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,6,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        if (user_stat == 9)
          getUserDetail(req,res,user_id,user_stat,data);
        else
          processStatus(req,res,1,data);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
  }
}

function  getUserDetail(req,res,uid,user_stat,data){
  var totalRows = 0;
  var promise = db.get('user_detail').find({_id: uid},{stream: true});
  promise.each(function(doc){
    totalRows++;
    data.businessName = doc.bus_name;
    data.businessAddress = doc.bus_addr;
    data.businessEmail = doc.bus_email;
    data.businessMobile = doc.bus_mobile;
    data.businessType = doc.bus_type;
    data.businessBranches = doc.bus_branch;
    data.businessUrl = doc.bus_url;
    data.buinsessLogoUrl = doc.bus_logo;
    data.businessDescription = doc.bus_descr;
    data.registrationDate = doc.regist_date;
    data.organizationId = doc.org_id;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,6,data);
    else if(totalRows > 1)
      processStatus(req,res,6,data); //system error should be 1 row only
    else
      getBusinessType(req,res,user_stat,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function  getBusinessType(req,res,user_stat,data){
  var totalRows = 0;
  var bus_type = parseInt(data.businessType);

  var moment = require('moment');
  var regist_time = moment(data.registrartionDate).format('Do MMMM YYYY');
  data.registrationDate = regist_time;

  var promise = db.get('business_type_master').find({business_id: bus_type},{stream: true});
  promise.each(function(doc){
    totalRows++;
    data.businessType = doc.business_type;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)
      processStatus(req,res,8,data); //no business type found is an error
    else if(totalRows > 1)
      processStatus(req,res,8,data); //business type error
    else
      processStatus(req,res,user_stat,data);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Registration/userProfile');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;


>>>>>>> DEV/master
