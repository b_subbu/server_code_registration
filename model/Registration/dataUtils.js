function getUserDetails(uid,callback) {

  var totalRows = 0;
  var data = {};
  var result = {};
  var promise = db.get('user_detail').find({_id: uid},{stream: true});
  promise.each(function(doc){
    totalRows++;
    doc.bus_name ? data.displayName = doc.bus_name : '';
    doc.reg_name ? data.registeredName = doc.reg_name : '';
    doc.bus_addr ? data.businessAddress = doc.bus_addr : ''; //if value is null dont assing property
    doc.bus_email ? data.businessEmail = doc.bus_email : '';
    doc.bus_mobile ? data.businessMobile = doc.bus_mobile : '';
    doc.bus_type ? data.businessType = doc.bus_type : '' ;
    doc.bus_url ? data.website = doc.bus_url : '';
    doc.bus_logo ? data.businessLogo = global.nodeURL+doc.bus_logo : '';
    doc.bus_descr ? data.description = doc.bus_descr : '';
    doc.country_id ? data.country = doc.country_id : '';
    doc.state_id ? data.state = doc.state_id : '';
    doc.city_id ? data.city = doc.city_id : '';
    doc.street ? data.street = doc.street : '';
    doc.building ? data.building = doc.building : '';
    doc.zip ? data.zipCode = doc.zip : '' ;
    doc.lat ? data.latitude = doc.lat : '' ;
    doc.longi ? data.longitude = doc.longi : '' ;
    doc.currency_id ? data.baseCurrency = doc.currency_id : '' ;
    doc.cin_number ? data.cinNumber = doc.cin_number: '';
    doc.vat_number ? data.vatNumber = doc.vat_number: '';
    doc.tin_number ? data.tinNumber = doc.tin_number: '';
    doc.cst_number ? data.cstNumber = doc.cst_number: '';
  	doc.org_id ? data.orgId = doc.org_id: '';
    doc.user_name ? data.userName = doc.user_name:'';
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)  {
      result.errorCode = 1; //no partial save case
      result.subscription = {};
      callback(null,result);
    }
    else if(totalRows > 1)
      callback(6,null); //system error should be 1 row only
    else {
      result.errorCode = 0;
      result.subscription = data;
      callback(null,result);
      }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(null,result);//system error
  });
}


function updateUserDetails(uid,userDets,callback){

 var promise = db.get('user_detail').update({_id:uid},
  { $set:{
    user_id: uid,
    bus_name: userDets.displayName,
    reg_name: userDets.registeredName,
    bus_addr: userDets.businessAddress,
    bus_email: userDets.businessEmail,
    bus_mobile: userDets.businessMobile,
    bus_type: parseInt(userDets.businessType),
    bus_branch: parseInt(userDets.totalBranches),
    bus_url: userDets.website,
    bus_logo: userDets.businessLogo,
    bus_descr: userDets.description,
    regist_date: new Date(),
    org_id: '',
    pass_word: '',
    activ_date: '',
    new_pass: false,
    country_id: userDets.country,
    state_id: userDets.state,
    city_id: userDets.city,
    street: userDets.street,
    building: userDets.building,
    zip: userDets.zipCode,
    lat: parseFloat(userDets.latitude),
    longi: parseFloat(userDets.longitude),
    currency_id: userDets.baseCurrency,
    cin_number: userDets.cinNumber,
    vat_number: userDets.vatNumber,
    tin_number: userDets.tinNumber,
    cst_number: userDets.cstNumber,
    user_name: userDets.userName
    }},
  {upsert: true}
);
  promise.on('success',function(doc){
   callback(null,null)
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function getSystemRoles(bizType,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var tblName = 'product_master';
   var prodUtils = require('../Users/dataUtils');
   
   prodUtils.products(tblName,bizType,function(err,result){
      if(err){
        debuglog("Could not get products for given biztype");
        data.errorCode = 1;
        callback(null,data);
      }
      else{
           createRoles(bizType,result[0].products,callback);
           debuglog("Product data for the given business is:");
           debuglog(result);
      }
   });
}

function createRoles(bizType,productArray,callback){
  var data = {};
  var rolesArray = [];
  var isError = 0;
  
     
    debuglog("For now roles are hard-coded for business type");
    debuglog("This may become table driven later when mydime dashboard is done");
    
    switch(bizType){
    
    case 1001:
      var tmpArr = {roleId:18,roleName:'Super User'};
      rolesArray.push(tmpArr);
      var tmpArr = {roleId:17,roleName:'Operator'};
      rolesArray.push(tmpArr);
      var tmpArr = {roleId:16,roleName:'Administrator'};
      rolesArray.push(tmpArr);
	  var tmpArr = {roleId:19,roleName:'Queue Operator'};
	  rolesArray.push(tmpArr);
      break;
      
     default:
      isError = 1;
       break; 
      }
    
  if(isError == 0){
  debuglog("Definition for given business type exists");
  debuglog(rolesArray);
  debuglog(productArray);
  data.rolesArray = rolesArray;
  data.productArray = productArray;
  data.errorCode = 0;
  callback(null,data);
  }
  else{
  debuglog("No definition for given business type exists");
  data.errorCode = 1;
  callback(null,data);
  }
}


function createSystemRoles(tblName,roles,products,callback){
debuglog("The given role is:");
debuglog(roles);

var roleId = roles.roleId;
var roleName = roles.roleName;
var roleType = 1;
var status = 82;
var productList = [];

var roleUtils = require('../Users/dataUtils');

var isError = 0;

var opProducts = products.filter(function(products){ return products.id == 12 });

debuglog("operation Products are:");
debuglog(opProducts);

var opSubModules = opProducts[0].subModules;

var wqTmpArr = {};

wqTmpArr.id = opProducts[0].id;
wqTmpArr.sequence = opProducts[0].sequence;
wqTmpArr.name     = opProducts[0].name;
wqTmpArr.enabled  = true;
wqTmpArr.subModules = [];



debuglog("submodules of operation are:");
debuglog(opSubModules);

debuglog("Details of main module of work Queue is:");
debuglog(wqTmpArr);

var wqProducts = opSubModules.filter(function(opSubModules){return opSubModules.id == 104 });

var wqTmpArr2 = {};

wqTmpArr2.id = wqProducts[0].id;
wqTmpArr2.sequence = wqProducts[0].sequence;
wqTmpArr2.name     = wqProducts[0].name;
wqTmpArr2.enabled  = true;
wqTmpArr2.subModules = [];

debuglog("work queue products are:");
debuglog("Note product/tab is defined in product tab header");
debuglog("If that would change then change here as well");
debuglog(wqProducts);

debuglog("The work queue is populated with operation and work queue top level submodule");
debuglog("For proper display in UI");
debuglog(wqTmpArr2);

var wqSubProducts = wqProducts[0].subModules;


debuglog("And in detail work queues for the business types are");
debuglog(wqSubProducts);

switch(roles.roleId){

   case 18:
       var productList = products;
       break;
   
   case 17:
       var productList = opProducts;
       break;
    
   case 16:
       var productList = products.filter(function(products){ return products.id == 11 });
	   break;
	   
   case 19:
           debuglog("tmpList is:");
		   debuglog(wqProducts);
	       wqTmpArr.subModules.push(wqProducts[0]);
		   debuglog(wqTmpArr);
		   productList.push(wqTmpArr);
		   debuglog(productList);
		   break;
       
   case 21:
       var tmpList = wqSubProducts.filter(function(wqSubProducts){return wqSubProducts.id == 1});
	       debuglog("tmpList is:");
		   debuglog(tmpList[0]);
	       wqTmpArr2.subModules.push(tmpList[0]);
		   debuglog(wqTmpArr2);
		   wqTmpArr.subModules.push(wqTmpArr2);
		   debuglog(wqTmpArr);
		   productList.push(wqTmpArr);
		   debuglog(productList);
       break;            

  case 22:
       var tmpList = wqSubProducts.filter(function(wqSubProducts){return wqSubProducts.id == 2});
	       wqTmpArr2.subModules.push(tmpList[0]);
		   wqTmpArr.subModules.push(wqTmpArr2);
		   productList.push(wqTmpArr);
       break;            

  case 23:
       var tmpList = wqSubProducts.filter(function(wqSubProducts){return wqSubProducts.id == 3});
	       wqTmpArr2.subModules.push(tmpList[0]);
		   wqTmpArr.subModules.push(wqTmpArr2);
		   productList.push(wqTmpArr);
       break;            
  
  case 24:
       var tmpList = wqSubProducts.filter(function(wqSubProducts){return wqSubProducts.id == 4});
	       wqTmpArr2.subModules.push(tmpList[0]);
		   wqTmpArr.subModules.push(wqTmpArr2);
		   productList.push(wqTmpArr);
       break; 
       
  default:
   isError = 1;
   break;                  
}

 if(isError == 1){
 debuglog("Some error since no definition for the given role");
 callback(6,null);
 }
 else{
 debuglog("No error so create the row now");
 var RoleData = {};
 RoleData.roles = {};
 RoleData.roles.roleName = roleName;
 RoleData.roles.roleType = roleType;
 RoleData.roles.productList = productList;
 RoleData.status_id = 82;
 
 debuglog(RoleData);
 roleUtils.RoleUpdate(tblName,roleId,RoleData,function(err,result) {
         if(err) {
               debuglog("Some error");
               callback(6,null);
              }
         else {
                debuglog("saved data");
                debuglog(RoleData);
                callback(null,null);
              }
       });
  }
}

function getSystemTaxes(bizType,country,state,callback){
   var totalRows =0;
   var resultArr = [] ;
   var data = {};
   var tblName = 'tax_types_master';
   debuglog("Create tax row corresponding to tax creation in datautils of taxes");
   debuglog("These rows will be created with authorised status");
   
var promise = db.get(tblName).find({$and:[{business_type:bizType},{country_id:country.geonameId},{state_id:state.geonameId}]});

promise.each(function(doc){
    totalRows++;
	var tmpArr = {};
        tmpArr.tax = {};
	tmpArr.tax.taxId =doc.tax_id;
	tmpArr.tax.taxName = doc.tax_name;
	tmpArr.tax.label = doc.tax_label;
	tmpArr.tax.enabled = true;
	tmpArr.status_id = 82;
	resultArr.push(tmpArr);
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0){
      data.errorCode = 1;
      callback(null,data)
      }
      else{
      data.errorCode = 0;
      data.taxesArr = resultArr;
      callback(null,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
   
  }


exports.usrDets = getUserDetails;
exports.createUsrDet = updateUserDetails;
exports.systemRoles  = getSystemRoles;
exports.createSystemRoles = createSystemRoles;
exports.systemTaxes = getSystemTaxes;