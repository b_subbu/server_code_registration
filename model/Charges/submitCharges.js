function processRequest(req,res,data){
    if(typeof req.body.chargeIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkChargeStatus(req,res,9,data);
}


function checkChargeStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var ChargeArr = req.body.chargeIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.chargeIds = ChargeArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_charge_type'; //TODO: move to a common definition file

    async.forEachOf(ChargeArr,function(key,value,callback1){
        var ChargeData= {};
            ChargeData.charge = {};
            ChargeData.charge.chargeId = key;
        datautils.checkChargeCode(tabl_name,ChargeData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });       }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToRevision(req,res,9,data);
          }
      });
}

function moveToRevision(req,res,stat,data){
    
    var ChargeArr = data.chargeIds;
    
    debuglog("Now start moving the entries to Revision Table");
    debuglog(ChargeArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_charge_type'; //TODO: move to a common definition file
    var mainTable    = orgId+'_charge_type';
   
    async.forEachOf(ChargeArr,function(key,value,callback1){
      chargeId = key;
     async.parallel({
          relStat: function (callback){datautils.moveChargeHeader(mainTable,revisionTable,chargeId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeChargeStatus(req,res,9,data,ChargeArr,mainTable,revisionTable); 
         }
     });
}

function copyToRevision(req,res,stat,data,ChargeData){
    
    
    debuglog("Even though this called Copy we are actually creating a new entry");
    debuglog(ChargeData);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_charge_type'; //We will create the new entry in Revision Table
   
      if(   ChargeData.charge.rateType == null
       || ChargeData.charge.rateType == 'undefined'
       || ChargeData.charge.rate == null
       || ChargeData.charge.rate == 'undefined'
      )
       processStatus(req,res,4,data);
    else{
         datautils.ChargeUpdate(revisionTable,data.chargeId,ChargeData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(ChargeData);
                var ChargeArr = [];
                ChargeArr.push(ChargeData.charge.chargeId);
                changeChargeStatus(req,res,9,data,ChargeArr,'',revisionTable);
                }
        });
      }
}


function changeChargeStatus(req,res,stat,data,ChargeArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(ChargeArr,function(key,value,callback1){
    chargeId = key;
    var reason = '';
       statutils.changeChargeStatus(revTable,data.orgId,data.businessType,data.userId,chargeId,81,'Charge Type Submitted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.chargeIds = ChargeArr;
                 getChargeDetails(req,res,9,data,revTable);
             }
          });
     
}


function getChargeDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.ChargesData(revTable,data.chargeIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getChargeStatusName(req,res,9,data,result); 
         }
     });
    
}

function getChargeStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('charge_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.chargeData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
exports.moveToRevision=moveToRevision; 
exports.copyToRevision=copyToRevision;



