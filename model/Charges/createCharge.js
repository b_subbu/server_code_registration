function processRequest(req,res,data){
   if( typeof req.body.userId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getChargeCode(req,res,9,data);
}


function getChargeCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var ChargeData = extend({},data);
       ChargeData.charge = req.body.chargeDetails;
       ChargeData.baseCurrency = req.body.baseCurrency;
       ChargeData.status_id = 80;
      
     var tblName = data.orgId+'_charge_type'; //create will always be in main table
      datautils.generateChargeCode(ChargeData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                debuglog("Generated new charge ID");
                 debuglog(result);
                 data.chargeId = result;
                 ChargeData.charge.chargeId = result;
                 checkChargeCode(req,res,9,data,ChargeData,tblName);
             }
        });
} 


function checkChargeCode(req,res,stat,data,ChargeData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     debuglog("Check if the  seller Id+ charge type exists or not");
     debuglog(ChargeData);
     debuglog(req.body);
      datautils.checkSellerCharge(ChargeData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 if(result.errorCode == 1)
                   processStatus(req,res,5,data);
                 else
                   saveChargeData(req,res,9,data,ChargeData,tblName);
             }
        });
} 


function saveChargeData(req,res,stat,data,ChargeData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
     debuglog("This is after getting data from request");
     debuglog(ChargeData);
     debuglog(data);
     
    var submitop = req.body.submitOperation;
  
   
    if(   ChargeData.charge.rateType == null
       || ChargeData.charge.rateType == 'undefined'
       || ChargeData.charge.rate == null
       || ChargeData.charge.rate == 'undefined'
      )
        processStatus(req,res,4,data);
    else{
    
     datautils.ChargeUpdate(tblName,data.chargeId,ChargeData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(ChargeData);
             if(submitop == 1){
             var chargeIds = [];
             chargeIds.push(data.chargeId);
             data.chargeIds = chargeIds;
               var submitCharge = require('./submitCharges');
               submitCharge.moveToRevision(req,res,9,data);
             }
             else{
                changeChargeStatus(req,res,9,data,ChargeData,tblName);
                }
            }
         });
        }
} 


function changeChargeStatus(req,res,stat,data,ChargeData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeChargeStatus(tblName,data.orgId,data.businessType,data.userId,data.chargeId,80,'Charge Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getChargeData(req,res,9,data,ChargeData,tblName);
               }
        });
} 

function getChargeData(req,res,stat,data,ChargeData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
    
    var chargeArr = [];
    chargeArr.push(data.chargeId) 
   datautils.ChargesData(tblName,chargeArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getChargeStatusName(req,res,9,data,result); 
         }
     });
    
}

function getChargeStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('charge_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.chargeDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;











