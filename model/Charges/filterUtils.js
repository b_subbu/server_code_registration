function getFilters(key_name,statList,pid,tid,oid,callback) {
  
  switch(key_name){
     case 'status':
     var tblName = 'charge_type_status_master';
       getStatusFilter(statList,tblName,callback);  
        break;
     case 'chargeType':
       getChargeTypeFilter(callback);
       break;
     case 'chargeName':
       getChargeNameFilter(callback);
       break;
     case 'enabled':
       getEnableFilter(callback);
       break;
     case 'seller':
       if(tid == 501)
        var tblName = oid+'_sellers';
       else
        var tblName = oid+'_Revision_sellers';
       getSellerFilter(tblName,callback);
       break;   
    default:
       callback(null,null);
       break;
  }
  
}

//for status no need to again query db since statList is already passed
//and it varies from tab to tab for other filters we can just query master
//and return

function getStatusFilter(mastData,tblName,callback) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var data = {};
     
      async.forEachOf(mastData,function(key1,value1,callback1){
        var statID=key1;
        getItemStatName(statID,tblName,function(err,result) {
           if(err){ isError =1; callback1();}
         else {
                 var tmpArray = {id:statID, name:result};
                 itemMasters.push(tmpArray);
                  callback1(); 
              }
           }); 
          },
         function(err){
       if(err){
            callback(6,null);      }
      else {
            callback(null,itemMasters);
	         }
      });
 

}

function getChargeNameFilter(callback){
   var datautils = require('./dataUtils');
   
   
    var tblName  = 'charge_types_master';
     datautils.filterChargeMaster(tblName,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                debuglog(result);
                debuglog("Filter for charge names");
                callback(null,result);
              }
        });
}

function getChargeTypeFilter(callback){
   var datautils = require('./dataUtils');
   
    debuglog("This is a more complex case");
    debuglog("Ideally we have to combine from taxtype master, delivery options master");
    debuglog("and other tables but as a shortcut we will take from organizaion table");
    
       var tblName  = 'charge_types_master';
    datautils.ChargeTypes(tblName,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                debuglog(result);
                debuglog("Filter for charge types");
                callback(null,result);
              }
        });
}

function getItemStatName(statID,tblName,callback){
   var totalRows =0;
   var statName = '';
    var mastutils = require('./../Registration/masterUtils');

  
      mastutils.masterStatusNames(tblName,statID,function(err,result) {
       
         if(err) callback(6,null);
         else {
                statName = result.statusName;
                callback(null,statName); 
                debuglog("after getting status");
                debuglog(result);
              }
          });
       
 } 

 function getEnableFilter(callback){
    
    var enabledList = [{id:true,name:"Yes"},{id:false,name:"No"}]; 
    debuglog("Enabled possible values are only true and false");
    callback(null,enabledList);
    
}

function getSellerFilter(tblName,callback){
  var datautils = require('./dataUtils');
   
     datautils.filterSellerMaster(tblName,function(err,result) {
           debuglog(result);
           debuglog("after getting seller result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                debuglog(result);
                debuglog("Filter for sellers");
                callback(null,result);
              }
        });}

 exports.filterOptions = getFilters;
 


