function processRequest(req,res,data){
   if(typeof req.body.chargeId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
      getChargeData(req,res,9,data);
}


function getChargeData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.chargeId = req.body.chargeId;
    var tid = req.body.tabId;
    
    if(tid == 501)
     var tabl_name = data.orgId+'_charge_type';
    else
     var tabl_name = data.orgId+'_Revision_charge_type';
   
     var extend = require('util')._extend;
     var ChargeData = extend({},data);
     
  
      datautils.ChargeData(tabl_name,ChargeData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              getChargeTypes(req,res,3,data);
              isError = 1;
              //return;
              debuglog("result errorCode is 1");
              }
            else {
                var chargeDetails = {};
                chargeDetails.chargeId   = result.chargeId;
                chargeDetails.chargeName = result.chargeName;
                chargeDetails.chargeType = result.chargeType;
                chargeDetails.sellerId   = result.sellerId;
                chargeDetails.rateType   = result.rateType;
                chargeDetails.applyRateAt = result.applyRateAt;
                chargeDetails.rate       = result.rate; 
                chargeDetails.chargeDescription = result.chargeDescription;
                chargeDetails.enabled = result.enabled;
                chargeDetails.status     = result.status;
                chargeDetails.statusId   = result.statusId;   
                
                data.chargeDetails = chargeDetails;
                data.baseCurrency = result.baseCurrency;
                getChargeTypes(req,res,9,data);
                debuglog("No error case");
                debuglog(chargeDetails);
            }
          }
        });
} 


function getChargeTypes(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');
 
 var tabl_name = 'charge_types_master';
 
 debuglog("Note as of now charge_types_master has types embedded in it");
 debuglog("But need to do more generic later by combining types in getProductData");
 debuglog("Because any change in tax/delivery masters will need change here and filter options");
 
 
 
  datautils.ChargeMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.charges = result;
                getSellers(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function getSellers(req,res,stat,data) {
 var isError = 0;
 var menuutils = require('../Menu/utils');

 var tabl_name = data.orgId+'_sellers';
 
  menuutils.sellers(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Sllers table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.sellers = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;










