function changeChargeStatus(tablName,orgId,bizType,operatorId,chargeId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {chargeId: chargeId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createChargeStatusChngLog(orgId,bizType,operatorId,chargeId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createChargeStatusChngLog(orgId,bizType,operatorId,chargeId,stat,reason,callback){
 
  var promise = db.get('charge_type_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    chargeId: chargeId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }

 //TODO combine all these in single file just tblname and 1 column changes
exports.changeChargeStatus = changeChargeStatus;
