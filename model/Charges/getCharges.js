function getChargeTypesData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
  
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    var searchArr = [];
      
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
  
    if(tid == 501)
     var tblName = oid+'_charge_type';
    else
     var tblName = oid+'_Revision_charge_type';
  
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'charge_type_status_master',callback); }
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList,tblName);
                  }
                  else {
                      var searchArr = [];
                      var tmpArray = {status_id: {"$in":statList}};
                      searchArr.push(tmpArray);
                      debuglog(searchArr);
                      getChargeCount(req,res,stat,data,searchArr,tblName);
                    }
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList,tblName) {
   var async = require('async');
   var filterutils = require('./filterUtils');
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
      debuglog("Starting processing of header fields");
     debuglog("Note we are calling filter utils because here there are 2 filters");
     debuglog("TODO make Roles filter utils as generic and call it from everywhere");
     debuglog(key);
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,oid,function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
               filterOptions[key_name] = results;
               callback();
            }
          }); 
        }
     else
        callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
            getChargeCount(req,res,stat,data,searchArr,tblName);
           }
         else 
         getChargeSearch(req,res,stat,data,statList,tblName);
         }
      });
} 


function getChargeSearch(req,res,stat,data,statList,tblName) {

 var searchInp = req.body.searchParams;

    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
   
 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 //var userArray = {};
 //searchArr.push(userArray);
  
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
      case 'enabled':
         var tmpArray = {enabled: key};
         searchArr.push(tmpArray);
         break;
       case 'chargeName':
        var tmpArray =  {"chargeName.id": key}; 
        searchArr.push(tmpArray);
        break; 
       case 'chargeType':
        var tmpArray = {"chargeType.id": key};
        searchArr.push(tmpArray);
        break; 
      case 'seller':
       var tmpArray = {sellerId: key};
       searchArr.push(tmpArray);
       break;
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           
            getChargeCount(req,res,9,data,searchArr,tblName);
            debuglog("We will start getting data with search Params");
            debuglog(searchArr);
          }
     });
}

function getChargeCount(req,res,stat,data,statList,tblName){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
    //have only tabl_name no need for orgId or bizType
   //since table name itself will have them      
   utils.ChargeCount(tblName,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
           debuglog("After getting results");
           debuglog(results);
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getChargeTypes(req,res,9,data);
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getChargeData(req,res,9,data,statList,tblName); 
             }
            }
         });
}


function getChargeData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
      debuglog("Now start processing getproductData");
      debuglog(headerTable);
      datautils.ChargeHeader(headerTable,pageId,size,statList,function(err,result) {
         debuglog("AFter result");
         debuglog(result);
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getChargeTypes(req,res,9,data); 
             //will be handled at count itself, just adding here
            }
            else {
            chargeTypes = result.chargeType;
            debuglog(chargeTypes); 
            getChargeStatusName(req,res,9,data,chargeTypes);
            }
          }
        });
} 


function getChargeStatusName(req,res,stat,data,chargeTypes) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(chargeTypes,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames('charge_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            getSellerData(req,res,9,data,chargeTypes);
           }
      });
 

} 

function getSellerData(req,res,stat,data,chargeTypes) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var tblName = data.orgId+'_sellers';
     
      async.forEachOf(chargeTypes,function(key1,value1,callback1){
        var sellerID=key1.seller;
        debuglog(sellerID);
           datautils.sellerData(tblName,sellerID,function(err,result) {
           if(err) callback1();
         else {
                key1.seller = result.sellerName;
                callback1(); 
                debuglog("after getting seller Name");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.productData = chargeTypes;
            getChargeTypes(req,res,9,data);
           }
      });
 

} 


function getChargeTypes(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');
 
 var tabl_name = 'charge_types_master';
 
 debuglog("Note as of now charge_types_master has types embedded in it");
 debuglog("But need to do more generic later by combining types in getProductData");
 debuglog("Because any change in tax/delivery masters will need change here and filter options");
 
 
 
  datautils.ChargeMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.charges = result;
                getSellers(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function getSellers(req,res,stat,data) {
 var isError = 0;
 var menuutils = require('../Menu/utils');

 var tabl_name = data.orgId+'_sellers';
 
  menuutils.sellers(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Sllers table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.sellers = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getProductData = getChargeTypesData;









