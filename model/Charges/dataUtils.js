function getChargeCount(tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getChargeHeader(tblName,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
     
   debuglog("Note here name and type are directly strings not their corresponding Id");
    debuglog("So data is not saved in normalized form to speed up performance");
    debuglog("As this is not expected to change");
    debuglog(page);
    debuglog(skip);

     var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
         promise.each(function(doc){
        totalRows++;
        var tmpArray = {chargeId: doc.chargeId,
                        chargeName:doc.chargeName.name,
                        chargeType: doc.chargeType.name,
                        enabled: doc.enabled,
                        status: doc.status_id,
                        statusId: doc.status_id,
                        seller: doc.sellerId
                      };
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.chargeTypes = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.chargeType = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getChargeMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  debuglog("As of now charge_label and enabled of master table");
  debuglog("is not used");
  
  var promise = db.get(tblName).find({},{sort:{charge_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.id = doc.charge_id;
        tmpArr.name = doc.charge_name;
        tmpArr.types = doc.charge_types;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getFilterChargeMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  debuglog("Same as above except for Types");
  debuglog("This is used in Filter");
  
  var promise = db.get(tblName).find({},{sort:{charge_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.id = doc.charge_id;
        tmpArr.name = doc.charge_name;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getFilterSellerMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  debuglog("This is used in Filter");
  debuglog("only Authorised status is returned")
  
  var promise = db.get(tblName).find({status_id:82},{sort:{charge_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.id = doc.sellerId;
        tmpArr.name = doc.bus_name;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getChargeData(tblName,chargeData,callback){
   var totalRows =0;
   var data = {};
   debuglog("start getting data");
   debuglog(chargeData);
   
  var promise = db.get(tblName).find({chargeId:chargeData.chargeId});
   promise.each(function(doc){
        totalRows++;
        data.chargeId = doc.chargeId;
        data.chargeName = doc.chargeName.id;
        data.chargeType = doc.chargeType.id;
        data.rateType = doc.rateType;
        data.applyRateAt = doc.applyRateAt;
        data.rate = doc.rate; 
        data.status = doc.status_id;
        data.statusId = doc.status_id;
        data.chargeDescription = doc.chargeDescription;
        data.enabled = doc.enabled;
        data.sellerId = doc.sellerId;
        data.baseCurrency = doc.baseCurrency;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function checkChargeCode(tblName,ChargeData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("We are checking only in Main table");
debuglog("Any row directly submitted is not problem");
debuglog("since it will just overwrite main row when authorised");
debuglog(ChargeData);

var promise = db.get(tblName).find({chargeId: ChargeData.charge.chargeId});
promise.each(function(doc){
   status = doc.status_id;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 5;
   data.status = status;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

//We are using update for both create and Edit 
function updateChargeData(tblName,chargeId,chargeData,callback){
  
var promise = db.get(tblName).update(
  {chargeId:chargeId},
  {$set: {chargeId: chargeData.charge.chargeId,
          chargeName: chargeData.charge.chargeName,
          chargeType: chargeData.charge.chargeType,
          rateType: chargeData.charge.rateType,
          applyRateAt: chargeData.charge.applyRateAt,
          rate: chargeData.charge.rate,
          status_id: chargeData.status_id,
          sellerId: chargeData.charge.sellerId,
          enabled: chargeData.charge.enabled,
          baseCurrency: chargeData.baseCurrency,
          chargeDescription: chargeData.charge.chargeDescription,
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getChargesData(tblName,chargeIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var promise = db.get(tblName).find({chargeId: {$in: chargeIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {chargeId: doc.chargeId,chargeName:doc.chargeName.name,chargeType:doc.chargeType.name,enabled:doc.enabled,status:doc.status_id,statusId:doc.status_id,seller:doc.sellerId};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function moveChargeHeader(srcTable,destTable,chargeId,callback){

var promise1 = db.get(destTable).remove({chargeId:chargeId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({chargeId:chargeId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({chargeId:chargeId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function deleteChargeRow(tblName,chargeId,callback){
  
var promise = db.get(tblName).remove({chargeId:chargeId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getChargeTypes(tblName,callback){
   var totalRows =0;
   var resultArr = [];
   debuglog("start getting filter data");
   var idx=0;
   var fieldName = 'charge_types';
  var promise = db.get(tblName).distinct(fieldName,{});
   promise.on('complete',function(err,doc){
         debuglog("Query completed");
         debuglog(doc);
         callback(null,doc);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function checkChargeCodeMain(ChargeData,callback){
 var tblName = ChargeData.orgId+'_charge_type';
 var revisiontblName = ChargeData.orgId+'_Revision_charge_type';
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
       debuglog("Check Count in Revision also because it is possible");
       debuglog("user might have directly submitted in Create");
       checkChargeCodeRevision(doc,revisiontblName,callback);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkChargeCodeRevision(activeCount,tblName,callback){
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
      debuglog("Count from revision table and active table are");
      debuglog(doc);
      debuglog(activeCount); 
      var totalCount = doc+activeCount;
       debuglog("So the sum of 2 is");
       debuglog("This is the total of Charges created by this business");
       debuglog("Since release & recall is not possible also delete is not possible");
       debuglog("can take next Id to be incr of sum total");
       debuglog("Otherwise change this logic");
       debuglog(totalCount);
       totalCount++;
       debuglog("Will increment by 1");
       debuglog(totalCount);
       chargeId = "Charge-"+totalCount;
       debuglog("And the new  Id is");
       debuglog(chargeId);
       callback(null,chargeId);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

function checkSellerChargeExistMain(ChargeData,callback){
var totalRows = 0;
var data = {};
var status = 0;

 var tblName = ChargeData.orgId+'_charge_type';
 var revisiontblName = ChargeData.orgId+'_Revision_charge_type';

debuglog("First Check in  Main table");
debuglog(ChargeData);

var promise = db.get(tblName).find({$and:[{sellerId: ChargeData.charge.sellerId},
                                          {chargeType:ChargeData.charge.chargeType},
                                          {chargeName: ChargeData.charge.chargeName}]});
promise.each(function(doc){
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 1;
   callback(null,data);
   }
  else{
    
    checkSellerChargeExistRevision(revisiontblName,ChargeData,callback);
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function checkSellerChargeExistRevision(revTable,ChargeData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("First Check in  Main table");
debuglog(ChargeData);

var promise = db.get(revTable).find({$and:[{sellerId: ChargeData.charge.sellerId},
                                           {chargeType:ChargeData.charge.chargeType},
                                           {chargeName: ChargeData.charge.chargeName}]});
promise.each(function(doc){
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 1;
   callback(null,data);
   }
  else{
   data.errorCode = 0;
   callback(null,data);
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function getSellersData(tblName,sellerId,callback){
  var totalRows =0;
   var tmpArray = {} ;
   var promise = db.get(tblName).find({sellerId: sellerId});
   promise.each(function(doc){
    tmpArray = {
                        sellerId: doc.sellerId,
                        sellerName:doc.bus_name,
                        sellerType: doc.sellerType,
                        status: 'Authorised'
        };
    }); 
   promise.on('complete',function(err,doc){
         callback(null,tmpArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function copyChargeHeader(srcTable,destTable,chargeId,callback){

var promise1 = db.get(destTable).remove({chargeId:chargeId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({chargeId:chargeId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
           callback(null,null);
        });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

exports.ChargeCount = getChargeCount;
exports.ChargeMaster = getChargeMaster;
exports.ChargeData = getChargeData;
exports.ChargeUpdate = updateChargeData;
exports.ChargesData = getChargesData;
exports.moveChargeHeader = moveChargeHeader;
exports.checkChargeCode = checkChargeCode;
exports.ChargeHeader = getChargeHeader;
exports.deleteChargeCode = deleteChargeRow;
exports.ChargeTypes = getChargeTypes;
exports.filterChargeMaster = getFilterChargeMaster;
exports.filterSellerMaster = getFilterSellerMaster;
exports.generateChargeCode = checkChargeCodeMain;
exports.checkSellerCharge = checkSellerChargeExistMain;
exports.sellerData = getSellersData;
exports.copyChargeHeader = copyChargeHeader;






