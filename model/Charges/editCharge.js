function processRequest(req,res,data){
   if( typeof req.body.userId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getChargeCode(req,res,9,data);
}


function getChargeCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var ChargeData = extend({},data);
       ChargeData.charge = req.body.chargeDetails;
       ChargeData.status_id = 80;
       ChargeData.baseCurrency = req.body.baseCurrency;
    
      
     var tblName = data.orgId+'_charge_type'; 
     debuglog("Check if the  Id exists or not");
     debuglog(ChargeData);
     debuglog(req.body);
      datautils.checkChargeCode(tblName,ChargeData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
               if(result.errorCode == 0) 
                 processStatus(req,res,3,data);
               else{
                debuglog("If no row exists it is an error for both entered and authorised");
                debuglog("If a row exists and status is Authorised then only submit is allowed");
                debuglog("But if a row exists and status is Entered both save and submit are allowed");
                if(result.status == 82){
                  var submitop = req.body.submitOperation;
                  if(submitop != 1){
                  debuglog("This is a SAVE for Authorised row so throw error");
                  debuglog(result);
                  debuglog(submitop);
                  processStatus(req,res,5,data);
                  }
                  else{
                data.chargeId = ChargeData.charge.chargeId;
               var submitCharge = require('./submitCharges');
               submitCharge.copyToRevision(req,res,9,data,ChargeData);
               debuglog("This is different operation of copy because");
               debuglog("Here we are not actually moving the data but creating a ");
               debuglog("new entry in Pending Tab without touching Main tab");
              }
            }
              else
                 saveChargeData(req,res,9,data,ChargeData,tblName);
            }
          }
        });
} 

function saveChargeData(req,res,stat,data,ChargeData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
     debuglog("This is after getting data from request");
     debuglog(ChargeData);
     debuglog(data);
     
     data.chargeId = ChargeData.charge.chargeId;
     
    var submitop = req.body.submitOperation;
  
   
    if(   ChargeData.charge.rateType == null
       || ChargeData.charge.rateType == 'undefined'
       || ChargeData.charge.rate == null
       || ChargeData.charge.rate == 'undefined'
      )   processStatus(req,res,4,data);
    else{
    
     datautils.ChargeUpdate(tblName,data.chargeId,ChargeData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(ChargeData);
             if(submitop == 1){
             var chargeIds = [];
             chargeIds.push(data.chargeId);
             data.chargeIds = chargeIds;
               var submitCharge = require('./submitCharges');
               submitCharge.moveToRevision(req,res,9,data);
             }
             else{
                changeChargeStatus(req,res,9,data,ChargeData,tblName);
                }
            }
          });
        }
} 


function changeChargeStatus(req,res,stat,data,ChargeData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeChargeStatus(tblName,data.orgId,data.businessType,data.userId,data.chargeId,80,'Charge Type Edited',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getChargeData(req,res,9,data,ChargeData,tblName);
               }
        });
} 

function getChargeData(req,res,stat,data,ChargeData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
    
    var ChargeArr = [];
    ChargeArr.push(data.chargeId) 
   datautils.ChargesData(tblName,ChargeArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getChargeStatusName(req,res,9,data,result); 
         }
     });
    
}

function getChargeStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('charge_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.chargeDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getData = processRequest;







