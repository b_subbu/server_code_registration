function processRequest(req,res,data){
    if(typeof req.body.chargeIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkChargeStatus(req,res,9,data);
}

function checkChargeStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var ChargeArr = req.body.chargeIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.chargeIds = ChargeArr;
    
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tblName = orgId+'_Revision_charge_type'; //TODO: move to a common definition file

    async.forEachOf(ChargeArr,function(key,value,callback1){
      var ChargeData = extend({},data);
          ChargeData.charge = {};
          ChargeData.charge.chargeId = key;
      debuglog("start checking if ID exists in Revision table before authorise");
      debuglog(ChargeData)    
     datautils.checkChargeCode(tblName,ChargeData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
               else{
                 if (result.status == 81){
                   debuglog("This is entered status item");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 89){
                   debuglog("This is authorised delete item");
                   debuglog("So change status to deleted");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Authorise is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
            }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
            debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              moveToMain(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
             }
            }
           });
}



function moveToMain(req,res,stat,data,enteredItems,authorisedItems){
    
    
    debuglog("Now start moving the entries to Main Table");
    debuglog(enteredItems);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_charge_type'; //TODO: move to a common definition file
    var mainTable    = orgId+'_charge_type';
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      chargeId = key;
     async.parallel({
          relStat: function (callback){datautils.moveChargeHeader(revisionTable,mainTable,chargeId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
          if(authorisedItems.length > 0){
          debuglog("This request has some authorised to delete items too");
          debuglog(authorisedItems);
          deleteInMain(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable);
          }
          else{
          debuglog("This request has no authorised items");
           changeChargeStatus(req,res,9,data,enteredItems,mainTable,revisionTable); 
         }
        }
     });
}

function deleteInMain(req,res,stat,data,enteredItems,authorisedItems,mainTable,revisionTable){
    
    
    debuglog("Now start moving the entries to Main Table");
    debuglog("It is just enough to update status and remove from Main here");
    debuglog("But retaining functionality as is to reduce code");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
    debuglog("This may not really be required for Ready2Delete just keeping same to reduce code");
    
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      chargeId = key;
     async.parallel({
          relStat: function (callback){datautils.moveChargeHeader(revisionTable,mainTable,chargeId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeChargeStatusDelete(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable); 
         }
     });
}

function changeChargeStatusDelete(req,res,stat,data,enteredItems,authorisedItems,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(authorisedItems,function(key,value,callback1){
    chargeId = key;
    var reason = 'Charge Deleted';
       statutils.changeChargeStatus(mainTable,data.orgId,data.businessType,data.userId,chargeId,88,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 changeChargeStatus(req,res,9,data,enteredItems,mainTable,revTable);
                 debuglog("It is ok to call even if no enteredItems");
                 debuglog("As it will simply skip the loop");
             }
          });
     
}


function changeChargeStatus(req,res,stat,data,ChargeArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(ChargeArr,function(key,value,callback1){
    chargeId = key;
    var reason = '';
       statutils.changeChargeStatus(mainTable,data.orgId,data.businessType,data.userId,chargeId,82,'Charge Type Authorised',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getChargeDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getChargeDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.ChargesData(mainTable,data.chargeIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getChargeStatusName(req,res,9,data,result); 
         }
     });
    
}

function getChargeStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('charge_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.chargeData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;





