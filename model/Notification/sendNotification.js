function processRequest(req,res){
     var data = {};
     getOrgIds(req,res,data);
       debuglog("This is a batch program");
       debuglog("So no input or output params only email");
       debuglog("A controller route can be added for testing if needed");
       debuglog("For now this is created only for ORder work queues");
       debuglog("This can be made more generic later");
}

function getOrgIds(req,res,data){
   var dataUtils = require('./dataUtils');
   var usrTblName = 'user_detail';
   
   dataUtils.orgIds(usrTblName,function(err,result){
   debuglog(err);
   debuglog(typeof err);
   debuglog(result);
   debuglog(typeof result);
   if (err == 6) {
     debuglog("Error happened");
     debuglog("Send error email to MyDime Admin");
     debuglog("Strange thing happens here if i just add if(err)");
     debuglog("Then both if and else getting executed need to check this later");
     processStatus(req,res,6,data);
   }
   else{
       getOrgWorkQueues(req,res,data,result);
       debuglog("Get list of all distinc orgIds from user_detail");
       debuglog("Also get email mobile from same table");
       debuglog("TODO change this to org collection table later");
       debuglog(result);
       }
  });
}

function getOrgWorkQueues(req,res,data,orgIdCollection){
    var async=require('async');
    var dataUtils = require('./dataUtils');
    var isError = 0;
    
    async.forEachOf(orgIdCollection,function(key,value,callback){
    debuglog("in getorgworkqueues foreachof orgidcollection");
    debuglog(key);
    var orgId = key.orgId;
    var wqTblName = orgId+"_work_queue";
    
    dataUtils.orgWorkQueues(wqTblName,function(err,result){
    if(err){
     isError = 1;
     callback();
     }  
   else{
       debuglog(result);
       key.workQueues = result;
       callback();
      }
   });
  },
  function(err){
       if(err || isError == 1){
        debuglog("Error happened in getOrgWorkQueues");
        debuglog("Send error email to MyDime Admin");
        processStatus(req,res,6,data);
        }
   else {
       getWorkQueueParams(req,res,data,orgIdCollection);
       debuglog(orgIdCollection);
       debuglog("Now get param for each work Queue from ");
       debuglog("work_queue_master table");
       debuglog("Note timeout is set at master level not individual orgId level");
        }
   });

}

function getWorkQueueParams(req,res,data,orgIdCollection){
    var async=require('async');
    var dataUtils = require('./dataUtils');
    var isError = 0;
    
    async.forEachOf(orgIdCollection,function(key,value,callback){
    debuglog("in getorgworkqueueparams first foreachof orgidcollection");
    debuglog(key);
    var orgId = key.orgId;
    var wqTblName = "work_queue_master";
     async.forEachOf(key.workQueues,function(key1,value1,callback1){
     debuglog("in getorgworkqueueparams second foreachof orgidcollection");
     debuglog(key1);
    
     dataUtils.workQueueParam(wqTblName,key1.workQueueId,function(err,result){
      if(err){
        isError = 1;
         callback1();
        }  
     else{
         debuglog(result);
         debuglog(key1);
         key1.workQueueCriteria = result.additional_criterias[0];
         key1.workQueueTimeOut  = result.max_queue_time;
         key1.workQueueMessage  = null;
         debuglog(orgIdCollection);
         debuglog("Get work queue Param for the given work Queue ID");
         debuglog("From work_queue_master table");
         debuglog("Message is null for all work queues to begin with");
         callback1();
        }
     });
  },
    function(err){
       if(err || isError == 1){
        debuglog("Error happened");
        debuglog("Send error email to MyDime Admin");
        callback();
        }
   else {
       debuglog("Do for next orgId ");
       callback();
      }
   });
  },
  function(err){
       if(err || isError == 1){
        debuglog("Error happened");
        debuglog("Send error email to MyDime Admin");
        return;
        }
   else {
        getQueueItems(req,res,data,orgIdCollection);
        debuglog(orgIdCollection);
        debuglog("Now check the items in order table ");
        debuglog("for OrgId + delivery option+ confirmed status+timeout");
      }
   });
}

function getQueueItems(req,res,data,orgIdCollection){
    var async=require('async');
    var dataUtils = require('./dataUtils');
    var isError = 0;
   
       debuglog("Get entries from orgId_order_header where delivery otpion = additional_criteria ");
       debuglog("status is Confirmed and updateTime > now-max_queue_time");
       debuglog("If found any order with above criteria update orgId_message to");
       debuglog("Work Queue<xxx> exceeded prescribed time limit");
       debuglog("Repeat this each work queue of the given orgId");
       debuglog("Repeat the same for each orgId ooh");
       debuglog("Maintaing seperate message for each orgId");   
 
    async.forEachOf(orgIdCollection,function(key,value,callback){
    debuglog("in getqueueitems first foreachof orgidcollection");
    debuglog(key);
    var orgId = key.orgId;
    var ordTblName = orgId+"_order_header";
     async.forEachOf(key.workQueues,function(key1,value1,callback1){
     debuglog("in getqueueitems second foreachof orgidcollection");
     debuglog(key1);
     var filterArr = [];
     filterArr.push(key1.workQueueCriteria);
     var tmpArr = {order_status:81};
     filterArr.push(tmpArr);
     var currDate = new Date();
     debuglog("Current time is");
     debuglog(currDate);
     var expTime = new Date(currDate.getTime()-key1.workQueueTimeOut*60*1000);
     debuglog("The expirty time for queue after subtracting minutes*seconds*milli from now is");
     debuglog(key1.workQueueTimeOut);
     debuglog(expTime);
     debuglog("So all orders updated less than this expiry time and still remains in");
     debuglog("Confirmed status without check-out is an issue");
     var tmpArr = {update_time: {$lte:expTime}};
     filterArr.push(tmpArr);
     
     debuglog("So the criteria to check is");
     debuglog(filterArr);
     
     
    
     dataUtils.queueItemCount(ordTblName,filterArr,function(err,result){
      if(err){
        isError = 1;
         callback1();
        }  
     else{
         debuglog(result);
         if(result == 0 ){
         debuglog("No Item of this work queue has timed out ");
         debuglog("So nothing to do further");
         callback1();
         }
         else{
         debuglog("This work queue has some items timed out");
         debuglog("So update message for this");
         debuglog("Not sure how to add a new line in SMS");
         var message = "Note: ";
             message += key1.workQueueName;
             message += " Exceeded Prescribed time limit "
          key1.workQueueMessage = message;
          callback1();    
         }
        }
     });
  },
    function(err){
       if(err || isError == 1){
        debuglog("Error happened");
        debuglog("Send error email to MyDime Admin");
        callback();
        }
   else {
       debuglog("Do for next orgId ");
       debuglog(key);
       callback();
      }
   });
  },
  function(err){
       if(err || isError == 1){
        debuglog("Error happened");
        debuglog("Send error email to MyDime Admin");
       processStatus(req,res,6,data);
        }
   else {
        sendSMS(req,res,data,orgIdCollection);
        debuglog(orgIdCollection);
        debuglog("Now Start sending SMS/email/push notifications ");
      }
   });
}

function sendSMS(req,res,data,orgIdCollection){
       debuglog("For each orgId if message is not null ");
       debuglog("then send a sms and email with subject Work Queue Timeout");
       debuglog("Email is only for testing to be removed after testing");
       debuglog("Will need SMS gateway credentials to send SMS");

    var async=require('async');
    var dataUtils = require('./dataUtils');
    var isError = 0;
    
    var smsMessage = null;
    var smsSubject = '';
 
    async.forEachOf(orgIdCollection,function(key,value,callback){
    debuglog("in sendsms first foreachof orgidcollection");
    debuglog(key);
    var orgId = key.orgId;
    var orgMobile = key.mobile;
	var orgEmail  = key.email;
   
    
    smsMessage = '';
    smsSubject = 'Work Queue TimeOut';
    
     async.forEachOf(key.workQueues,function(key1,value1,callback1){
     debuglog("in sendsms second foreachof orgidcollection");
     debuglog(key1);
     debuglog(typeof key1.workQueueMessage);
     
     if(key1.workQueueMessage !== null){
     debuglog("This work queue has some timedout item");
     debuglog(key1);
     smsMessage += key1.workQueueMessage;
     debuglog(smsMessage);
     callback1();
     
     } 
     else {
     debuglog("This work queue has no timeout items");
     debuglog(key1);
     callback1();
     }
     
    
  },
    function(err){
       if(err || isError == 1){
        debuglog("Error happened");
        debuglog("Send error email to MyDime Admin");
        callback();
        }
   else {
     if(smsMessage == ''){
     debuglog("No SMS for this orgId");
     debuglog("Start next Org Id");
     debuglog(key);
     callback();
     }
     else{
     debuglog("SMS for this OrgId");
     debuglog(key);
     debuglog(smsMessage);
     dataUtils.sendSMSNow(orgId,orgMobile,orgEmail,smsSubject,smsMessage,function(err,result){
      if(err){
        isError = 1;
         callback();
        }  
     else{
          debuglog(result);
          debuglog("SMS sent out for the orgId");
          debuglog(key);
          callback();
         }
      });
     }
    }
   });
  },
  function(err){
       if(err || isError == 1){
        debuglog("Error happened");
        debuglog("Send error email to MyDime Admin");
       processStatus(req,res,6,data);
        }
   else {
        debuglog("No Error sent all SMS/email/push notifications ");
        processStatus(req,res,9,data);
      }
   });

}

function processStatus(req,res,stat,data){
debuglog("In Process Status");
debuglog(stat);
res.send({status:stat});
}
exports.execute = processRequest;
