function getOrgIds(tblName,callback){
   var totalRows =0;
   var resultArr = [];
  
  var fieldName = 'org_id'
  debuglog("Do a distinct by org_id if many users were to come later");
  var promise = db.get(tblName).find({});
  promise.each(function(doc){
        totalRows++;
        var tmpArr = {};
        tmpArr.orgId = doc.org_id;
        tmpArr.email = doc.bus_email;
        tmpArr.mobile = doc.bus_mobile;
        tmpArr.userId = doc.user_id;
        resultArr.push(tmpArr);
       }); 
  promise.on('complete',function(err,doc){
      debuglog("Completed and no error");
      callback(0,resultArr);
  });
   promise.on('error',function(err){
   debuglog("Completed and error");
   debuglog(err);
    callback(6,null);//system error
  });
 
} 

function getOrgWorkQueues(tblName,callback){
   var totalRows =0;
   var resultArr = [];
  
  var promise = db.get(tblName).find({});
  promise.each(function(doc){
        totalRows++;
        var tmpArr = {};
        tmpArr.workQueueId = doc.workQueueId;
        tmpArr.workQueueName = doc.workQueueName;
        resultArr.push(tmpArr);
       }); 
  promise.on('complete',function(err,doc){
            callback(null,resultArr);
  });
   promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
} 

function getWorkQueueParam(tblName,workQueueId,callback){
   var totalRows =0;
   var resultArr = {};
   var extend = require('util')._extend;
  
  var promise = db.get(tblName).find({workqueue_id:workQueueId});
  promise.each(function(doc){
        totalRows++;
        debuglog("Result of fetch is");
        debuglog(doc);
        resultArr = extend({},doc);
      }); 
  promise.on('complete',function(err,doc){
    debuglog("Work queue params of ");
    debuglog(workQueueId);
    debuglog(resultArr);
    debuglog(totalRows);
     if(totalRows != 1)
        callback(6,null);
     else
        callback(null,resultArr);
  });
   promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
} 

function getQueueItemCount(tblName,sarr,callback){
   var totalRows =0;
  
  var promise = db.get(tblName).count({$and:sarr});
  promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 
} 

function sendSMSNow(orgId,orgMobile,orgEmail,smsSubject,smsMessage,callback){
debuglog("For now we are just displaying the values");
debuglog("Once SMS gateway API credentials are provided actual SMS can be sent");
debuglog("Probably need to set some headers then");
debuglog("For now no SMS is sent just send Email ");
debuglog("Currently email sent to superuser");
debuglog("TODO change to work queue operator once Roles are done");
debuglog(orgId);
debuglog(orgMobile);
debuglog(smsSubject);
debuglog(smsMessage);
      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendmail(orgEmail,smsSubject,smsMessage);
      debuglog("email sent. No return from email and no waiting here");
      debuglog("If email fails nothing can be done by application");
  
callback(null,null);



}
exports.orgIds = getOrgIds;
exports.orgWorkQueues = getOrgWorkQueues;
exports.workQueueParam = getWorkQueueParam;
exports.queueItemCount = getQueueItemCount;
exports.sendSMSNow = sendSMSNow;


