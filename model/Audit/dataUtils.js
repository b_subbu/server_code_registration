function getAuditTable(auditData,callback){

  var totalRows =0;
  var data = {};
 
  debuglog("Get Status table name and column name");
  debuglog("Note this is hard-coded as of now can be table driven later");
  debuglog(auditData);
  
  switch(auditData.productId){
  
   case 140:
    data.statTablName = 'alert_status';
    data.statCol   = {alert_id:auditData.elementId};
    data.statMastTabl = 'alert_status_master';
    data.errorCode = 0;
    break;
 
   case 150:
    data.statTablName = 'menuitem_status';
    data.statCol   = {itemCode:auditData.elementId};
    data.statMastTabl = 'menuitem_status_master';
    data.errorCode = 0;
    break;
   
   case 160:
    data.statTablName = 'menus_status';
    data.statCol   = {menuCode:auditData.elementId};
    data.statMastTabl = 'menus_status_master';
    data.errorCode = 0;
    break;

  case 170:
    data.statTablName = 'orders_status';
    data.statCol   = {order_id:auditData.elementId};
    data.statMastTabl = 'orders_status_master';
    data.errorCode = 0;
    break;
  
   case 180:
    data.statTablName = 'invitation_status';
    data.statCol   = {invitation_code:auditData.elementId};
    data.statMastTabl = 'invitation_status_master';
    data.errorCode = 0;
    break;
  
  case 190:
    data.statTablName = 'groups_status';
    data.statCol   = {group_code:auditData.elementId};
    data.statMastTabl = 'groups_status_master';
    data.errorCode = 0;
    break;

   case 109:
    data.statTablName = 'pg_status';
    data.statCol   = {pgId:auditData.elementId};
    data.statMastTabl = 'pg_status_master';
    data.errorCode = 0;
    break;

  case 111:
    data.statTablName = 'delivery_option_status';
    data.statCol   = {deliveryId:parseInt(auditData.elementId)};
    data.statMastTabl = 'delivery_option_status_master';
    data.errorCode = 0;
    break;

 case 112:
    data.statTablName = 'tax_type_status';
    data.statCol   = {taxId:parseInt(auditData.elementId)};
    data.statMastTabl = 'tax_type_status_master';
    data.errorCode = 0;
    break;

 case 115:
    data.statTablName = 'charge_type_status';
    data.statCol   = {chargeId:auditData.elementId};
    data.statMastTabl = 'charge_type_status_master';
    data.errorCode = 0;
    break;
 
  case 116:
    data.statTablName = 'invoice_config_status';
    data.statCol   = {invoiceConfigId:parseInt(auditData.elementId)};
    data.statMastTabl = 'invoice_config_type_status_master';
    data.errorCode = 0;
    break;
    
  case 122:
    data.statTablName = 'seller_type_status';
    data.statCol   = {sellerId:parseInt(auditData.elementId)};
    data.statMastTabl = 'seller_type_status_master';
    data.errorCode = 0;
    break;

 case 131:
    data.statTablName = 'role_status';
    data.statCol   = {roleId:parseInt(auditData.elementId)};
    data.statMastTabl = 'role_status_master';
    data.errorCode = 0;
    break;

case 130:
    data.statTablName = 'user_status';
    data.statCol   = {userId:parseInt(auditData.elementId)};
    data.statMastTabl = 'user_status_master';
    data.errorCode = 0;
    break;

case 110:
    data.statTablName = 'user_status';
	data.statCol = {userId:parseInt(auditData.elementId)};
	data.statMastTabl = 'user_status_master';
	data.errorCode = 0;
	break;
	
   default:
     data.errorCode = 1;
     break;
    
  }
  callback(null,data);
}

function getAuditRows(tblName,elementId,userId,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var moment=require('moment');
   
   debuglog("TODO fix alert_status table to standardize");
  
   var page = parseInt(pgid);
   var skip = page > 0 ? ((page - 1) * size) : 0;
     
   var promise = db.get(tblName).find({$and: sarr},{sort: {create_date: -1},limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
        var status = 0;
        debuglog(doc);
        (tblName == 'alert_status')?(status=doc.alert_status_id):(status= doc.status_id);
                    
        var tmpArray = {elementId:elementId,
                        userName:   doc.operatorId,
                        status: status,
                        action: doc.reason,
                        date: moment(doc.create_date).format('L')
                      };
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
           callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 


function getHeaderRow(callback){

   var resultArr = [];
  
   
   debuglog("Header Row is hard-coded since audit Trail is common across");
   debuglog("All Product and Tab and there is no seperate product+tab for it");
   debuglog("so no search is really possible here");
   
   var tmpArr = {};
   tmpArr.id = 1001;
   tmpArr.name = 'userName';
   tmpArr.title = 'User Name';
   tmpArr.isRangeSearch = false;
   tmpArr.searchable = false;
   tmpArr.filterable = false;
   tmpArr.hidden = false;
   tmpArr.type = 'string';
   tmpArr.seq = 1;
   resultArr.push(tmpArr);
  
   var tmpArr = {};
   tmpArr.id = 1002;
   tmpArr.name = 'elementId';
   tmpArr.title = 'Element Id'
   tmpArr.isRangeSearch = false;
   tmpArr.searchable = false;
   tmpArr.filterable = false;
   tmpArr.hidden = false;
   tmpArr.type = 'string';
   tmpArr.seq = 2;
   resultArr.push(tmpArr);
  
   var tmpArr = {};
   tmpArr.id = 1003;
   tmpArr.name = 'status';
   tmpArr.title = 'Status';
   tmpArr.isRangeSearch = false;
   tmpArr.searchable = false;
   tmpArr.filterable = true;
   tmpArr.hidden = false;
   tmpArr.type = 'string';
   tmpArr.seq = 3;
   resultArr.push(tmpArr);
  
   var tmpArr = {};
   tmpArr.id = 1004;
   tmpArr.name = 'action';
   tmpArr.title = 'Action';
   tmpArr.isRangeSearch = false;
   tmpArr.searchable = false;
   tmpArr.filterable = false;
   tmpArr.hidden = false;
   tmpArr.type = 'string';
   tmpArr.seq = 4;
   resultArr.push(tmpArr);
   
   var tmpArr = {};
   tmpArr.id = 1005;
   tmpArr.name = 'date';
   tmpArr.title = 'Action Date';
   tmpArr.isRangeSearch = false;
   tmpArr.searchable = false;
   tmpArr.filterable = false;
   tmpArr.hidden = false;
   tmpArr.type = 'string';
   tmpArr.seq = 1;
   resultArr.push(tmpArr);
   
   callback(null,resultArr);
}


function getStatusFilter(tblName,callback){
   var totalRows =0;
   var resultArray = [] ;
  
   var promise = db.get(tblName).find({});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {id:doc.status_id,name:doc.status_name};
                        
        debuglog("Result of fetch is:");
        debuglog(tmpArray);
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
           callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function getAuditCount(tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getUserName(tablName,userid,callback) {

 var typeName = "";
 var data = {};

  var totalRows = 0;
  var promise = db.get(tablName).find({user_id:userid});

  promise.each(function(doc){
    totalRows++;
    userName = doc.user_name;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0){
      data.errorCode = 1;
      data.userName = "";
      callback(null,data)
      }
      else{
      data.errorCode = 0;
      data.userName = userName;
      callback(null,data);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}

exports.auditTable =  getAuditTable;
exports.auditRows  =  getAuditRows;
exports.headerRow       = getHeaderRow;
exports.statFilter      = getStatusFilter;
exports.auditCount      = getAuditCount;
exports.userName        = getUserName;


