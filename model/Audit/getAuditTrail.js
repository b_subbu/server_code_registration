function processRequest(req,res,data){
   if(typeof req.body.productId === 'undefined' || typeof req.body.elementId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getHeaderData(req,res,9,data);
}


function getHeaderData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.productId = req.body.productId;
    data.elementId = req.body.elementId;
    
     var extend = require('util')._extend;
     var PGData = extend({},data);
     
  
      datautils.headerRow(function(err,result) {
           debuglog(result);
           debuglog("after getting result for Header Row");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
                data.productHeader = result;
                getAuditTable(req,res,9,data,PGData);
                debuglog("No error case");
                debuglog("Note header row is hard-coded for now");
                debuglog("This can later change to db driven like work queues");
              }
        });
} 

function getAuditTable(req,res,stat,data,PGData) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
      datautils.auditTable(PGData,function(err,result) {
           debuglog(result);
           debuglog("after getting result for Audit table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              processStatus(req,res,3,data);
              isError = 1;
              //return;
              debuglog("result errorCode is 1");
              }
            else {
                var tblName = result.statTablName;
                var colName = result.statCol;
                var mastTbl = result.statMastTabl; 
                getStatFilter(req,res,9,data,tblName,colName,mastTbl);
                //getAuditRows(req,res,9,data,tblName,colName,mastTbl);
                debuglog("No error case");
                debuglog("Note tblname and colname is now actually hard-coded");
                debuglog("This can later change to db driven like work queues");
                debuglog(tblName);
                debuglog(colName);
                debuglog(mastTbl);
                }
            }
        });
} 


function getStatFilter(req,res,stat,data,tblName,colName,mastTbl) {
   var datautils = require('./dataUtils');
   var isError = 0;
   var statFilter = {};
   
   
    datautils.statFilter(mastTbl,function(err,result) {
           debuglog(result);
           debuglog("after getting result for Filter");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
                statFilter = {status:result};
				data.filterOptions = statFilter;
                getAuditCount(req,res,stat,data,tblName,colName,mastTbl);
                debuglog("No error case");
                debuglog("Filter options are ");
                debuglog(result);
              }
        });
} 

function getAuditCount(req,res,stat,data,tblName,colName,mastTbl) {
 var isError = 0;
 var datautils = require('./dataUtils');
 var sarr = [];
 sarr.push(colName);
 if(tblName != 'alert_status'){
 debuglog("Because alert_status is not having orgID TODO fix later");
 var tmpArr = {};
 tmpArr.org_id = data.orgId;
sarr.push(tmpArr);

 }
 
 debuglog(req.body);
 debuglog(typeof req.body.status)
 if(typeof req.body.status != 'undefined'){
 var tmpArr = {};
 tmpArr.status_id = req.body.status;
 sarr.push(tmpArr);
 }
 
 debuglog("The array for checking is:");
 debuglog(sarr);
 
  datautils.auditCount(tblName,sarr,function(err,result) {
           debuglog(result);
           debuglog("after getting audit rows count");
           if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.recordCount = result;
                getAuditRows(req,res,stat,data,tblName,colName,mastTbl);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function getAuditRows(req,res,stat,data,tblName,colName,mastTbl) {
 var isError = 0;
 var datautils = require('./dataUtils');
 var sarr = [];
 sarr.push(colName);
if(tblName != 'alert_status'){
 debuglog("Because alert_status is not having orgID TODO fix later");
 var tmpArr = {};
 tmpArr.org_id = data.orgId;
 sarr.push(tmpArr);
 }
  
 if(typeof req.body.status != 'undefined'){
 var tmpArr = {};
 tmpArr.status_id = req.body.status;
 sarr.push(tmpArr);
 }
 
 debuglog("The array for checking is:");
 debuglog(sarr);
 
  if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
  datautils.auditRows(tblName,data.elementId,data.userId,pageId,20,sarr,function(err,result) {
           debuglog(result);
           debuglog("after getting audit rows");
           debuglog("TODO get user email from user table for given user Id");
           if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                //data.auditData = result;
                getAuditStatusName(req,res,stat,data,mastTbl,result);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function getAuditStatusName(req,res,stat,data,mastTbl,groups) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(groups,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames(mastTbl,statID,function(err,result) {
           debuglog("After getting status name");
           debuglog(result);
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
              data.auditData = groups;
              getOperatorName(req,res,9,data);
           }
      });
 

} 

function getOperatorName(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
     var userTbl = 'user_detail';
     
      async.forEachOf(data.auditData,function(key1,value1,callback1){
        var userID=key1.userName;
        debuglog(userID);
           datautils.userName(userTbl,userID,function(err,result) {
           debuglog("After getting user name");
           debuglog(result);
           if(err) callback1();
         else {
                key1.userName = result.userName;
                callback1(); 
                debuglog("after populating");
                debuglog("TODO try to see if we can do with one query since user name may be same for all rows");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
               processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;







