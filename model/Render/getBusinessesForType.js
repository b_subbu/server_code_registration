function processRequest(req,res,data){
   if( typeof req.body.customerId === 'undefined' || typeof req.body.businessType === 'undefined')
      processStatus(req,res,6,data); //system error 
   else 
      getBizTypeDets(req,res,9,data);
}



function getBizTypeDets(req,res,stat,data){
    
	data.businessType = req.body.businessType;
	data.customerId = req.body.customerId;
	
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
     //call all functions async in parallel
    //but proceed only after completion and checks
    
    async.parallel({
         recStat: function(callback){utils.bizDetails(data.businessType,callback); },
         folStat: function(callback){utils.custFollow(data.customerId,callback); }  
         },
         function(err,results) {
			  debuglog(results);
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.recStat.errorCode != 0) {
             processStatus (req,res,results.recStat.errorCode,data);
             isError = 1;
             return;
            }
          if(isError == 0 && (results.folStat.errorCode != 0 && results.folStat.errorCode !=1)) {
             processStatus (req,res,results.folStat.errorCode,data);
             isError = 1;
             return;
            }
          if (isError == 0) {
             availBusiness = results.recStat.bizArr;
             followingBusiness = results.folStat.bizArray;
             setFollowFlag(req,res,9,data,availBusiness,followingBusiness);
            }
         }); 
}


function setFollowFlag(req,res,stat,data,availBiz,follBiz){
    var async = require('async');
    var utils = require('./utils');
    var resultArr = [];
    
     async.forEachOf(availBiz,function(key1,value1,callback1){
       var isFollow = false;
       async.forEachOf(follBiz,function(key2,value2,callback2){
         if(key1.orgId == key2.orgId )
           isFollow = true;
           callback2();
          },
         function(err){
            if(err){
               console.log(err);
              processStatus(req,res,6,data);
             }
            else{
               key1.isFollowing = isFollow;
               callback1();
             }
         });
       }, 
     function(err){
      if(err){
         console.log(err);
         processStatus(req,res,6,data);
        }
    else{
        data.availableBusinesses = availBiz;
        processStatus(req,res,9,data);
        }
    });    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



