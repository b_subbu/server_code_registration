function checkInvitation(tblName,customerEmail,callback){
 
  var totalRows =0;
  var data = {};
  var password = "";
  
  var promise = db.get(tblName).find({cust_email: customerEmail},{stream: true});
  promise.each(function(doc){
     totalRows++;
     data.invitationCode = doc.invitation_code;
     data.customerId = doc.cust_id;
	 password = doc.pass_word;
     });
     promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 4;
       callback(null,data); //customer email not found
       }
	  else{
		debuglog("Start getting customer record");
		debuglog(password);
        debuglog(typeof password);		
	  if(!(typeof password == "undefined" || password == null)) {
		 data.errorCode = 3; //dont allow to register if password is already set
         callback(null,data);		 
	  }
      else {
         data.errorCode = 0;
         callback(null,data);
       } 
	  }
     });
   promise.on('error',function(err){
       console.log(err);//system error
       callback(6,data);
     });
 
 }

function createCustRecord(data,bizData,devData,callback){
 
    var hash = require('node_hash/lib/hash');
    var salt = 'mydime_##123##';
    var password = devData.password;
    var pass_word_enc = hash.sha256(password,salt);

   var promise = db.get('cust_master').update(
   {cust_id: bizData.customerId},
   {$set:{ cust_name: devData.customerName,
    pass_word: pass_word_enc,
    register_date: new Date(),
    os_version: devData.osver,
    os_api_level: devData.osapi,
    cust_device: devData.device,
    cust_model: devData.model,
    cust_product: devData.product,
    device_manufacturer: devData.manufact}
  },
  {upsert:false});
  promise.on('success',function(doc){
  callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
}
 

function createCustFollowing(data,bizData,callback){
     var async = require('async');
 
 async.forEachOf(bizData.bizDets,function(key,value,callback1){
   if(key.isActive == true){
  var promise = db.get('cust_following').insert({
    cust_id: data.customerId,
    org_id: key.orgId,
    bus_type: key.bizType,
    follow_date: new Date()
  });
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  }
  else
   callback1();
  },
  function(err){
      if(err)
        callback(6,null);
      else
          {
           data.errorCode = 0;
           callback(null,data);
          }
     });
 
 
 
 }
 
function checkCustomer(email,passwd,callback){
 
  var totalRows =0;
  var data = {};
  
debuglog("Start checking email and password");  
  var promise = db.get('cust_master').find({cust_email: email},{stream: true});
  promise.each(function(doc){
      totalRows++;
      data = {customerId:doc.cust_id,pass_word:doc.pass_word,active:doc.cust_active,isFollowing:doc.is_following, errorCode:0};
	  debuglog(data);
	  debuglog(data.pass_word);
	  debuglog(typeof data.pass_word);
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1;
       callback(null,data); //customer id not found
       }
      else if(totalRows > 1){
       callback(6,data); //system error
      }
      else {
      if(data.active != true){
            data.errorCode = 2;
			callback(null,data);
	  }
	   if(typeof data.pass_word == "undefined" || data.pass_word == null) {
		   data.errorCode = 1;
		   callback(null,data);
	   }
		   
	
      if(!(typeof data.pass_word == "undefined" || data.pass_word == null)) {
		  
	  var hash = require('node_hash/lib/hash');
      var salt = 'mydime_##123##';
      var password = password;
      var pass_word_enc = hash.sha256(passwd,salt);

         if(data.pass_word != pass_word_enc)
          data.errorCode = 3;
      callback(null,data);
      } 
	  }
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
}

function checkCustomerEmail(email,callback){
 
  var totalRows =0;
  var data = {};
   
   debuglog("This is used in adminApprove for business web");
   debuglog("We take password for active customers if that record exists");
   
  var promise = db.get('cust_master').find({cust_email: email},{stream: true});
  promise.each(function(doc){
      totalRows++;
      data = {passWord:doc.pass_word,active:doc.cust_active,isFollowing:doc.is_following,errorCode:0};
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.isCustomer = false;
       callback(null,data); //customer id not found
       }
      else {
      if(data.active != true)
            data.isCustomer = false;
	  else
		   data.isCustomer = true;
	  callback(null,data);
      } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
}

function getCustomerFollowings(custId,callback){
 
  var totalRows =0;
  var data = {};
  var resultArr = [];
  
  var async = require('async');
  
  var promise = db.get('cust_following').find({cust_id: custId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    var tmpArr = {orgId:doc.org_id,bizType:doc.bus_type};
    resultArr.push(tmpArr);
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //not error also nothing else to proceed further 
       callback(null,data); 
       }
      else {
      data.errorCode = 0;
      data.bizArray = resultArr;
       callback(null,data);
       }
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
   
 }

function getBusinessDetails(bizData,callback){
  var totalRows =0;
  var data = {};
  var resultArr = [];
  
  var async = require('async');
  debuglog(bizData);
  debuglog("Start getting business details");
  async.forEachOf(bizData.businesses,function(key,value,callback1){
  var orgId = key.orgId; 
  var promise = db.get('user_master').find({$and:[{org_id:orgId },{system_user:true}]});
  promise.each(function(doc){
  
   if(doc.user_status == 9){
   totalRows++;
   var tmpArr = {bizId:doc._id+'',orgId:key.orgId,bizType:key.bizType};
     resultArr.push(tmpArr);
   }
     }); 
   promise.on('complete',function(err,doc){
      callback1();
        
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else {
          if(totalRows == 0) {
           data.errorCode = 1; //not error also nothing else to proceed further
           data.countBiz = 0;
           data.bizDetailArr = resultArr;
          callback(null,data); 
         }
         else
          {
             data.countBiz = totalRows;
             bizData.bizArr = resultArr;
           getBizNameLogo(data,bizData,callback)
         }
      }
     });
 
 }
 
function getBizNameLogo(data,bizData,callback){
 
  var totalRows =0;
  var resultArr = [];
  
  var async = require('async');
   
  async.forEachOf(bizData.bizArr,function(key,value,callback1){
  var bizId = key.bizId; 
  var promise = db.get('user_detail').find({user_id: bizId},{stream: true});
  promise.each(function(doc){
   totalRows++;
   if(doc.bus_logo){
   debuglog("With new registration flow business logo will not be sent during subscription");
   debuglog("So first check if that field is not null");
   debuglog("Then check if contains a file or only directory");
   debuglog("If null then just render null logo");
   if(doc.bus_logo.indexOf('.') > -1)
     var tmpArr = {id:bizId,name:doc.bus_name,descr:doc.bus_descr,logo:doc.bus_logo,orgId:doc.org_id};
   else
     var tmpArr = {id:bizId,name:doc.bus_name,descr:doc.bus_descr,logo:'',orgId:doc.org_id};
     }
   else
     var tmpArr = {id:bizId,name:doc.bus_name,descr:doc.bus_descr,logo:'',orgId:doc.org_id};
     
   
   resultArr.push(tmpArr);
    }); 
   promise.on('complete',function(err,doc){
          callback1();
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
              if(totalRows == 0){
                     data.errorCode = 1; //not error also nothing else to proceed further 
                     data.bizDetailArr = resultArr;
                 }
             else { 
             data.errorCode = 0;
             data.bizDetailArr = resultArr;
             }
             callback(null,data)
         }
     });
 
 }

function checkCustomerId(custId,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get('cust_master').find({cust_id: custId},{stream: true});
  promise.each(function(doc){
   totalRows++;
   
    data = {customerId:doc.cust_id,active:doc.cust_active,customerEmail:doc.cust_email,errorCode:0};
     }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 2;
       callback(null,data); //customer id not found
       }
      else if(totalRows > 1){
       callback(6,data); //system error
      }
      else {
         if(data.active != true)
            data.errorCode = 3;
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
 }


function getRecommendBusiness(custId,callback){
 
  var totalRows =0;
  var data = {};
  var bizData = {};
  var resultArr = [];
  
  //get the id of latest 4 businesses created/activated
  var promise = db.get('user_master').find({user_status:9},{sort:{_id: -1}, limit:4});
  promise.each(function(doc){
   totalRows++;
   var tmpArr = {bizId:doc._id+''};
   resultArr.push(tmpArr);
     }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //not error also nothing else to proceed further 
       callback(null,data); 
       }
      else {
           bizData.bizArr = resultArr;
           getBizNameLogo(data,bizData,callback);
          }
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
   
 }


function getBusinessTypes(callback){
   //get the id of latest 4 businesses created/activated

  var totalRows =0;
  var data = {};
  var bizData = {};
  var resultArr = [];
  var mastutils = require('../Registration/masterUtils');
  
  mastutils.masterTypes('business_type_master',function(err,results){
    if(err) callback(6,null);
    
    else {
            bizData.bizArr = results.types;
           getBizTypeCount(data,bizData,callback);
         }
  });
  
}

function getBizTypeCount(data,bizData,callback){
 
  var totalRows =0;
  
   var async = require('async');
   
   async.forEachOf(bizData.bizArr,function(key,value,callback1){
  
  //get the id of latest 4 businesses created/activated
  var promise = db.get('user_detail').count({$and:[{bus_type:key.id},{bus_name:{$ne:null}}]});
   promise.on('complete',function(err,doc){
     key.businessCount = doc;
     callback1();
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
   
 },
   function(err){
      if(err)
             {
             console.log(err);
             callback(1,null);
            }
      else
          {
           data.errorCode = 0;
           data.resultArray = bizData.bizArr;
           callback(null,data);
           }
     });
     
}

function checkBusinessType(bizType,callback){
 
  var totalRows =0;
  var data = {};
  
  biz_type = parseInt(bizType,10);
  //get the id of latest 4 businesses created/activated
   var mastutils = require('../Registration/masterUtils');
  
   mastutils.masterNames('business_type_master',biz_type,function(err,result){
      if(err) callback(6,null);
      else
          {
           if(result.errorCode == 1)
              data.errorCode = 1;
           else
              data.errorCode = 0;
          callback(null,data);
          }
     });

}

function getDetailsForBizType(biz_type,callback){
 
  var totalRows =0;
  var resultArr = [];
  var data = {};
  
  var bizType = biz_type; //bus_type is int in master but string in user_detail need to fix later
  
  var async = require('async');
  
  var promise = db.get('user_detail').find({bus_type: bizType},{stream: true});
  promise.each(function(doc){
   totalRows++;
   if(doc.bus_logo && doc.bus_logo.indexOf('.') > -1)
     var tmpArr = {orgId:doc.org_id,name:doc.bus_name,logo:doc.bus_logo,bizId:doc.user_id};
   else
     var tmpArr = {orgId:doc.org_id,name:doc.bus_name,logo:'',bizId:doc.user_id};
 debuglog(tmpArr.name);
 debuglog(typeof tmpArr.name);
 debuglog("use the fact that operators wont have bus_name set");
 debuglog("since they are not actual business");
   if(!(tmpArr.name == null || typeof tmpArr.name == 'undefined') )
       resultArr.push(tmpArr);
    }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
        data.errorCode = 0;
        data.bizArr = resultArr;
        callback(null,data); //no error but nothing to do
       }
      else {
          checkBusinessActive(resultArr,callback);
       }
   }); 
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
   
 }

function checkBusinessActive(bizData,callback){
  var totalRows =0;
  var data = {};
  var resultArr = [];
  
  var async = require('async');
  
  async.forEachOf(bizData,function(key,value,callback1){
  var orgId = key.orgId; 
  var promise = db.get('user_master').find({$and:[{org_id:orgId},{system_user:true}]});
   promise.each(function(doc){
  
   if(doc.user_status == 9){
    totalRows++;
    var tmpArr = {name:key.name,logo:key.logo,orgId:key.orgId};
    resultArr.push(tmpArr);
   }
     }); 
   promise.on('complete',function(err,doc){
         callback1();
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
     },
      function(err){
      if(err)
             callback(6,null);
      else
          {
            data.errorCode = 0;
            data.bizArr = resultArr;
           callback(null,data);
         }
     });
 
 }

function addCustomerFollowing(custId,orgId,bizType,callback){
 
  var totalRows =0;
  var resultArr = [];
  var resultArray = [];
  var bizData= [];
  var data = {};
  var bus_typ = bizType;
  
  var async = require('async');
  
    var promise = db.get('user_detail').find({$and:[{org_id:orgId },{bus_type:bus_typ}]});
   promise.each(function(doc){
     totalRows++;
     var tmpArr = {userId:doc.user_id,orgId:doc.org_id};
     resultArr.push(tmpArr);
    }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //biz not found or active 
       callback(null,data);//callback here seems to mess up in getBusinessTypes call 
       }
       else {
        checkBusinessActive(resultArr,function(err,result){
             if(err){
                callback(6,null);
               }
          else {
            if(typeof result.bizArr === 'undefined' ){
                data.errorCode =1 ;//biz not found or active
                callback(null,data);
                }
             else {
                 data.customerId = custId;
                 data.errorCode = 0;
                 var tmpArray = {orgId:result.bizArr[0].orgId,bizType:bizType,isActive:true};
                 resultArray.push(tmpArray);
                 bizData.bizDets = resultArray;
                 createCustFollowing(data,bizData,callback);
               } 
           }
        });
      }
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
}


function removeCustomerFollowing(custId,bizType,callback){
   var promise = db.get('cust_following').remove({
    cust_id: custId,
    bus_type: bizType
    });
  promise.on('success',function(doc){
    callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function getAlertsForBusiness(orgId,bizType,custEmail,pgid,size,callback){
 
  var totalRows =0;
  var resultArr = [];
  var resultArray = [];
  var bizData= [];
  var finalArray = [];
  var data = {};
  var bus_typ = bizType;
  
  var cartUtils = require('../Cart/dataUtils');
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
    
  var async = require('async');
  
  var promise = db.get('alert_release').find({$and:[{org_id: orgId},{bus_type:bus_typ}]},{sort:{update_time: -1},limit: size,skip: skip});
  promise.each(function(doc){
   totalRows++;
   if(doc.content_type_id == 62)
     var tmpArr = {id:doc.alert_id,orgId:orgId,title:doc.content_title,src:doc.image_url,type:doc.alert_type_id,contentType:doc.content_type_id,description:doc.descr,linkType:doc.link_type_id,linkId:doc.link_id};
  else
    var tmpArr = {id:doc.alert_id,orgId:orgId,title:doc.content_title,src:doc.video_url,type:doc.alert_type_id,contentType:doc.content_type_id,description:doc.descr,linkType:doc.link_type_id,linkId:doc.link_id};
   resultArray.push(tmpArr);
    }); 
   promise.on('complete',function(err,doc){
       data.errorCode = 0;
       finalArray.alerts = resultArray;
        data.alertsArr = resultArray;
        //callback(null,data);
        cartUtils.isSuperUser(custEmail,orgId,function(err,result){
         if(err)
           callback(6,null);
         else{
           if(result == 'super'){
           debuglog("This is a super user so get authorised and not yet released alerts");
           debuglog("Note:Count and page not done for super user---TODO")
           getAuthorisedAlertsForBusiness(orgId,bizType,custEmail,data,pgid,size,callback);
           }
           else{
           debuglog("This is not a super user for the given business so just return");
           callback(null,data);
           }
         }
        });
   });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
   });
}


function getAuthorisedAlertsForBusiness(orgId,bizType,custEmail,data,pgid,size,callback){
 
  var totalRows =0;
  var resultArr = [];
  var resultArray = [];
  var bizData= [];
  var finalArray = [];
  var bus_typ = bizType;
  
  var cartUtils = require('../Cart/dataUtils');
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
    
  debuglog("Get all authorised status alerts from alert_master for the given orgId");
  debuglog("Note for alerts release table is seperate and it is not orgId_** table");
  debuglog("TODO change to standard later");
    
  var promise = db.get('alert_master').find({$and:[{org_id: orgId},{alert_status_id:82}]},{sort:{update_time: -1},limit: size,skip: skip});
  promise.each(function(doc){
   totalRows++;
   if(doc.content_type_id == 62)
     var tmpArr = {id:doc.alert_id,orgId:orgId,title:doc.title,src:'',type:doc.alert_type_id,contentType:doc.content_type_id,description:doc.descr,linkType:doc.link_type_id,linkId:doc.link_id};
  else
    var tmpArr = {id:doc.alert_id,orgId:orgId,title:doc.title,src:'',type:doc.alert_type_id,contentType:doc.content_type_id,description:doc.descr,linkType:doc.link_type_id,linkId:doc.link_id};
   resultArray.push(tmpArr);
    }); 
   promise.on('complete',function(err,doc){
       data.errorCode = 0;
        getAuthorisedAlertImages(data,resultArray,callback);
        //callback(null,data);
   });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
   });
}


function getAuthorisedAlertImages(data,alertArray,callback) {
   
   debuglog("Start getting authorised images");
   debuglog(data);
   debuglog(alertArray);
   var totalRows = 0;
   var async = require('async');
   
   async.forEachOf(alertArray,function(key1,value1,callback1){
        var alertID=key1.id;
        debuglog(alertID);
        var promise = db.get('alert_images').find({alert_id: alertID});
        promise.each(function(doc){
         totalRows++;
         if(key1.contentType == 62)
             key1.src = doc.image_url;
         else
             key1.src = doc.video_url;
         }); 
       promise.on('complete',function(err,doc){
		 data.alertsArr.push(key1); 
         callback1();
       });
       promise.on('error',function(err){
       callback1();
   });        },
     function(err){
       if(err){
              callback(6,null);       
            }
      else {
           //data.alertsArr.push(alertArray);
           callback(null,data);
           }
      });
} 


function getAlertCount(orgList,callback){
 
  var totalRows =0;
  var resultArr = [];
  var resultArray = [];
  var finalArray = [];
  var data = {};
  
  //note: we are taking count for the list of all org ids here 
  //but may need to include biztype if one org can have multiple biz types  
  var async = require('async');
  
  var promise = db.get('alert_release').count({org_id: {$in: orgList}});
   promise.on('complete',function(err,doc){
       data.errorCode = 0;
       data.countAlerts = doc;
       callback(null,data);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getBusinessDetailsForAlerts(bizData,callback){
  var totalRows =0;
  var data = {};
  var resultArr = [];
  
  var async = require('async');
  
  async.forEachOf(bizData.businesses,function(key,value,callback1){
  var orgId = key.orgId; 
   var promise = db.get('user_master').find({$and:[{org_id:orgId },{system_user:true}]});
    promise.each(function(doc){
  
   if(doc.user_status == 9){
     totalRows++;
     var tmpArr = {userId:doc._id+'',orgId:key.orgId,bizType:key.bizType};
     resultArr.push(tmpArr);
   }
     }); 
   promise.on('complete',function(err,doc){
        callback1();
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
             if(totalRows == 0) {
               data.errorCode = 0; //not error also nothing else to proceed further
               data.countBiz = 0;
               data.bizDetailArr = []; 
              callback(null,data); 
             }
            else{
             data.countBiz = totalRows;
             bizData.bizArr = resultArr;
           getBizNameLogoAlerts(data,bizData,callback)
           }
         }
     });
 
 }
 
function getBizNameLogoAlerts(data,bizData,callback){
 
  var totalRows =0;
  var resultArr = [];
  
  var async = require('async');
  
  debuglog("We are using userId and not orgId here");
  debuglog("Since name and logo may not be available for all users of the orgId");
  async.forEachOf(bizData.bizArr,function(key,value,callback1){
  var userId = key.userId; 
  var promise = db.get('user_detail').find({user_id: userId},{stream: true});
  promise.each(function(doc){
   totalRows++;
   if(doc.bus_logo && doc.bus_logo.indexOf('.') > -1)
     var tmpArr = {id:doc.org_id,name:doc.bus_name,logo:doc.bus_logo,isFollowing:true};
   else
     var tmpArr = {id:doc.org_id,name:doc.bus_name,logo:'',isFollowing:true};
   resultArr.push(tmpArr);
    }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
        callback1();
       }
      else {
       callback1();
        }
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
              if(totalRows == 0){
                     data.errorCode = 1; //not error also nothing else to proceed further 
                     data.bizDetailArr = resultArr;
             }
             else { 
             data.errorCode = 0;
             data.bizDetailArr = resultArr;
             }
             callback(null,data)
         }
     });
 
 }

function getBusinessDetailsForInvitation(tblName,inviteData,callback){
   var totalRows =0;
   var data = {};
  
  var promise = db.get(tblName).find({invitationCode:inviteData.invitationCode});
   promise.each(function(doc){
        totalRows++;
        data.orgId = doc.org_id;
      }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 4; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 

function checkLinkType(linkId,linkType,orgId,bizType,callback){

   var itemutils = require('../Menu/utils');
    var grouputils = require('../Groups/dataUtils');
   
  if(linkType == 'item'){
    debuglog("start checking for item");
    debuglog("We are calling seperate function since twin loop within async is not working");
       
    debuglog(linkId);
    debuglog(linkType);
    itemutils.checkItemCode(orgId,bizType,linkId,function(err,results) { 
       debuglog("returning from checkitemCode");
       debuglog(results);
         if(err) 
            callback(6,null);
        else{
          if(results.errorCode != 5 ) {
             linkId = ''; //If item does not exits as of now then blank out the itemCode
             debuglog("Call back from empty link");
             callback(null,linkId);
            }
          else {
            if(results.itemStatus != 84)
                linkId = ''; //If item code not Inuse status then blank out linkId
            else
                linkId = linkId
              debuglog("Now we will callback from item ");
             callback(null,linkId); //do nothing for 84 item status
            }
          }
         });
         }
         
      if(linkType == 'group'){
       var tblName = orgId+"_group_header";
       var groupData = {groupCode:linkId};
       debuglog("Start checking for group");
       debuglog(groupData);
       grouputils.groupHeader(tblName,groupData,function(err,results) { 
          debuglog("returning from checkgroupCode");
          debuglog(results);
        
           if(err) {
             console.log(err);
             callback(6,null);
            }
          if(results.errorCode != 0  || results.status != 84) {
             linkId = ''; 
             debuglog("Now we will callback from group blank link");
             callback(null,linkId);
             
            }
          else{ 
            debuglog("Callback from active group");
            callback(null,linkId);
            } 
          });
         }
         
}

function getLearnAlertData(alrId,callback){
   var totalRows =0;
   var data = {};
   
   var promise = db.get('alert_release').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data.linkContent = doc.link_content;
        });
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
        getAuthorisedLearnAlertData(alrId,callback);
      else  
        callback(null,data); 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 


function getAuthorisedLearnAlertData(alrId,callback){
   var totalRows =0;
   var data = {};
   
   debuglog("No rows in release table so try to get in master table");
   debuglog("So after a alert is released this will not come here");
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data.linkContent = doc.link_content;
        });
   promise.on('complete',function(err,doc){
        callback(null,data); 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function addCustFollowOnLogin(customerId,callback){
	debuglog("This function does 2 things update is_following to true for all records of the given customerId")
	debuglog("Then create rows in cust following. If the customer Id is already following the given business");
	debuglog("Duplicate record is not created due to the check in function");
	debuglog("is_following is set as true for all the invitations of the given customer Id on first login");
	
	var promise = db.get('cust_master').update(
	{cust_id:customerId},
		{$set:{is_following:true}},
			{$multi:true, upsert:false});
	promise.on('success',function(doc){
		getInvitationsOnLogin(customerId,callback);
	});
	promise.on('error',function(){
		callback(6,null);
	});
}

function getInvitationsOnLogin(customerId,callback){
 
  var totalRows =0;
  var data = {};
  var resultArr = [];
  
  var async = require('async');
  debuglog("Note this is a quick fix getting orgId from invitation code and hard-coding business type to 1001");
  debuglog("TODO get all details from invitation header for the given invitation code and populate them");
  var promise = db.get('cust_master').find({cust_id: customerId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    var tmpArr = {customerId:doc.cust_id};
	var orgId =  doc.invitation_code.substring(0,9);
	tmpArr.orgId = orgId;
	tmpArr.businessType = 1001;
	debuglog("Array of customer data for customerId");
	debuglog(customerId);
	debuglog("is:");
	debuglog(tmpArr);
    resultArr.push(tmpArr);
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //not error also nothing else to proceed further 
       callback(null,data); 
       }
      else {
      createCustFollowingOnLogin(resultArr,callback)
       }
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
   
 }

 function createCustFollowingOnLogin(customerArray,callback){
 
  var isError = 0;
  var async = require('async');
  var data = {errorCode:0};
  var inviteUtils = require('../Invitation/dataUtils');
  async.forEachOf(customerArray,function(key1,value1,callback1){
	 debuglog("start creating cust following record for:") ;
	 debuglog(key1);
	 debuglog(value1);
	  inviteUtils.createCustFollowing('cust_following',key1,function(err,result){
		 if(err){
			isError = 1;
            debuglog(err)			;
			callback1();
		 }
         else
          callback1();			 
	    });
  },
  function(err){
	    if(err)
			callback(6,null);
	    else
			callback(null,data); 
   });
 }

exports.checkInvite = checkInvitation;
exports.createCust  = createCustRecord;
exports.checkCustomer = checkCustomer;
exports.custFollow    = getCustomerFollowings;
exports.businessDetails = getBusinessDetails;
exports.checkCustomerId = checkCustomerId;
exports.recommendBiz = getRecommendBusiness;
exports.businessTypes = getBusinessTypes;
exports.checkBizType = checkBusinessType;
exports.bizDetails   = getDetailsForBizType;
exports.addCustomerFollow = addCustomerFollowing;
exports.removeCustomerFollow = removeCustomerFollowing;
exports.getAlerts = getAlertsForBusiness;
exports.getAlertCount = getAlertCount;
exports.bizDetailsForAlerts =  getBusinessDetailsForAlerts;
exports.getBizDetails = getBusinessDetailsForInvitation;
exports.checkLink = checkLinkType;
exports.alertLinkContent = getLearnAlertData;
exports.checkCustomerEmail = checkCustomerEmail;
exports.addCustFollow = addCustFollowOnLogin;



