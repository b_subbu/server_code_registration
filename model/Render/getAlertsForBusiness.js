function processRequest(req,res,data){
      getRecommendedBiz(req,res,9,data);
}



function getRecommendedBiz(req,res,stat,data){
	
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var bizData = [];
     //call all functions async in parallel
    //but proceed only after completion and checks
      async.parallel({
         folStat: function(callback){utils.custFollow(data.customerId,callback); }  
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.folStat.errorCode != 0) {
             processStatus (req,res,results.folStat.errorCode,data);
             isError = 1;
             return;
            }
      
          if (isError == 0) {
            
             bizData.businesses = results.folStat.bizArray;
             getBizData(req,res,9,data,bizData);
            }
         }); 
  
}

function getBizData(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
   
     //call all functions async in parallel
    //but proceed only after completion and checks
    
    async.parallel({
         detailStat: function(callback){utils.bizDetailsForAlerts(bizData,callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.detailStat.errorCode != 0 ) {
             data = {};
             processStatus (req,res,results.detailStat.errorCode,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
           
             bizData.businessDetails = results.detailStat.bizDetailArr;
             getAlertCount(req,res,9,data,bizData);
            }
         }); 
}
function getAlertCount(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
    //businesses array has details of all businesses that a customer is following
    //businessDetails array has only those are active
    //since businesses array has orgid we have to take from it for active businesses
    //only
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
    
  async.forEachOf(bizData.bizArr,function(key,value,callback1){
   debuglog("bizData entries");
   debuglog(key);
  if(key.orgId == data.orgId){
       bizList.push(key.orgId);
      }
       callback1(); 
     },
      function(err){
      if(err)
             callback(6,null);
      else { 
          debuglog("Start getting alerts");
          debuglog(bizList);
          utils.getAlertCount(bizList,function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.errorCode != 0 ) {
             data = {};
             processStatus (req,res,results.errorCode,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
              data.pageId = pageId;
              data.totalRecords = results.countAlerts;
              getAlerts(req,res,stat,data,bizData);
             }
         });
         
       }
       });  
}


function getAlerts(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var finalArray = [];
    var isBiz = 0;
    var size = 20; //override here if needed
    var bizType = ""; //need in next function   this will be same for all items
    var orgId   = "";
  
    
   //call all functions async in parallel
    //but proceed only after completion and checks
      if(typeof req.body.pageId == 'undefined')
        pageId = 1;
     else
        pageId = req.body.pageId;
 
       async.forEachOf(bizData.bizArr,function(key,value,callback){
       
        bizType = key.bizType;
        reqOrgId = data.orgId;
       orgId = key.orgId;
      
       
        if(reqOrgId ==orgId){
            isBiz = 1;
               //key.isFollowing = true;
               
         
            utils.getAlerts(orgId,bizType,data.customerEmail,pageId,size,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.errorCode != 0 ) {
             data = {};
             processStatus (req,res,results.errorCode,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
           var tmpArray1 = {};
           
           tmpArray1.orgId = bizData.bizArr[value].orgId;
           tmpArray1.name = bizData.businessDetails[value].name;
           tmpArray1.logo = bizData.businessDetails[value].logo;
           tmpArray1.isFollowing = bizData.businessDetails[value].isFollowing;
           tmpArray1.alerts = results.alertsArr;
           
           finalArray.push(tmpArray1);
         
             callback();
         }
         });
         }
         else
           callback();
         },
    function(err){
      if(err)
             processStatus(req,res,6,data);
      else
          {
          if(isBiz ===0){
          processStatus(req,res,1,data);
          }
            
          else{
          data.businesses = finalArray;
           //checkItemStatus(req,res,9,data,orgId,bizType);
          processStatus(req,res,9,data);
          debuglog("No need to check Item status here. All items/groups will be rendered as-is");
          debuglog("Status check will be implemented in getGroup or getMenuItem in /customer/app/");
          debuglog("Refer buyAlret_flow_2.2");
          }
         }
     });
 
}

function checkItemStatus(req,res,stat,data,orgId,bizType){
    
    var async = require('async');
    var isError = 0;
    var finalArray = [];
    var utils = require('./utils');
    var size = 20; //override here if needed 
    
    async.forEachOf(data.businesses,function(key1,value1,callback1){
         
    async.forEachOf(key1.alerts,function(key2,value2,callback2){
       debuglog("start checking for item");
       debuglog(key2);
       if(key2.linkType == 'item' || key2.linkType == 'group'){
       utils.checkLink(key2.linkId,key2.linkType,orgId,bizType,function(err,results) { 
       
          debuglog("returning from check link type");
          debuglog(results);
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             key2.linkId = '';
             callback2();
            }
          else {
              key2.linkId = results;
              debuglog("Now we will callback from item ");
              debuglog(key2);
             callback2(); //do nothing for 84 item status
            }
         });
        }
       else{
       debuglog("These are neither item nor group some error type");
       debuglog("So we will just blank out the linkId");
       debuglog("Other wise calling fn wont return");
       key2.linkId = "";
       debuglog(key2);
       callback2();
        }
      },
    function(err){
      if(err)
           processStatus(req,res,6,data);
      else
         {
           debuglog("After completion of callback2 let us call callback1");
           debuglog(key1);
          callback1();
        }
     });
 
    },
    function(err){
      if(err)
           processStatus(req,res,6,data);
      else
           processStatus(req,res,9,data);
     });
 
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
