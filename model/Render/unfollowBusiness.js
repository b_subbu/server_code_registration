function processRequest(req,res,data){
   if(typeof req.body.businessType === 'undefined')
      processStatus(req,res,6,data); //system error 
   else 
      checkCustomerStatus(req,res,9,data);
}


function checkCustomerStatus(req,res,stat,data){
    
    var custId = data.customerId;
   var bizType = data.businessType;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
   //call all functions async in parallel
    //but proceed only after completion and checks
     async.parallel({
         folStat: function(callback){utils.custFollow(custId,callback); }  
         },
         function(err,results) {
          if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0) {
            followingBusiness = results.folStat.bizArray;
            checkIsFollowing(req,res,9,data,followingBusiness);
            }
         }); 
}


function checkIsFollowing(req,res,stat,data,follBiz){
    var async = require('async');
    var utils = require('./utils');
    var resultArr = [];
    var isFollow = false;
        
        async.forEachOf(follBiz,function(key,value,callback){
         if(data.orgId == key.orgId )
                  isFollow = true; //TODO: multiple business type per orgid
           callback();
          },
      function(err){
      if(err){
         console.log(err);
         processStatus(req,res,6,data);
        }
    else{
          if(isFollow != true)
           processStatus(req,res,4,data);
          else
           removeCustFollow(req,res,9,data);
        }
    });    
}


function removeCustFollow(req,res,stat,data){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
   //call all functions async in parallel
    //but proceed only after completion and checks
     async.parallel({
         custfolStat: function(callback){utils.removeCustomerFollow(data.customerId,data.businessType,callback); },
         },
         function(err,results) {
          if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
         if(isError == 0) {
            processStatus(req,res,9,data);
           }
         }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;


