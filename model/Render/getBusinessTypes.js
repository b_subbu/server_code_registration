function processRequest(req,res,data){
   if( typeof req.body.customerId === 'undefined')
      processStatus(req,res,6,data); //system error 
   else 
      getBizTypes(req,res,9,data);
}


function getBizTypes(req,res,stat,data){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
   
    async.parallel({
        typeStat: function(callback){utils.businessTypes(callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.typeStat.errorCode != 0 ) {
             data = {};
             processStatus (req,res,results.typeStat.errorCode,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
           
              data.availableBusinessTypes = results.typeStat.resultArray;
             processStatus(req,res,9,data);
            }
         }); 
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;


