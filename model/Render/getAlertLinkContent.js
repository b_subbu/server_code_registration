function processRequest(req,res,data){
   if(typeof req.body.alertId === 'undefined')
      processStatus(req,res,6,data); //system error 
   else 
      getRecommendedBiz(req,res,9,data);
}



function getRecommendedBiz(req,res,stat,data){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var bizData = [];
      async.parallel({
         folStat: function(callback){utils.custFollow(data.customerId,callback); }  
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.folStat.errorCode != 0) {
             processStatus (req,res,results.folStat.errorCode,data);
             isError = 1;
             return;
            }
      
          if (isError == 0) {
          debuglog("AFter getting customer followings");
          debuglog(results.folStat);
             bizData.businesses = results.folStat.bizArray;
             getBizData(req,res,9,data,bizData);
            }
         }); 
  
}

function getBizData(req,res,stat,data,bizData){
 
    var async = require('async');
    var isError = 0;
    var isBiz = 0;
    
    var orgId = req.body.orgId;
    data.orgId = orgId;
    
    debuglog(bizData)
    async.forEachOf(bizData.businesses,function(key,value,callback1){
            debuglog("bizData entries");
            debuglog(key);
        if(key.orgId == orgId){
              isBiz = 1;
        }
       callback1(); 
     },
      function(err){
      if(err)
             callback(6,null);
      else { 
      
        if(isBiz == 0){
          debuglog("Probably that Business has gone inactive");
          debuglog(data);
          processStatus(req,res,1,data);
          }
        else{
         debuglog("Everything seems to be ok now let us get the alert linkContent");
         data.alertId = req.body.alertId;
         debuglog(data);
         checkAlertId(req,res,stat,data);
         }
        
      }
    });   
 
 }
function checkAlertId(req,res,stat,data){
    
    var boardutils = require('../Boarding/utils');
    var isError = 0;
    
    debuglog("Start checking alert");
    debuglog(data);
    
    boardutils.alertStatus(data.alertId,function(err,results) {
           debuglog(results);
           debuglog("From alertStatus is")
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && (!(results.errorCode == 0 &&( results.alertStatus == 84 || results.alertStatus == 82)))) {
             data = {};
             processStatus (req,res,4,data);
             isError = 1;
             return;
            }
        if(isError == 0 && results.alertType != 12 ) {
             data = {};
             processStatus (req,res,5,data);
             isError = 1;
             return;
            }
      if (isError == 0) {
              getAlertData(req,res,stat,data);
             }
         });
         
}     


function getAlertData(req,res,stat,data){
    
   var utils = require('./utils');
    var isError = 0;
    
    debuglog("Start getting alert link contentdata");
    debuglog("If business was a super user even authorised alerts would have been rendered");
    debuglog("So get its content from alert_master here since it would not be available in alert_release");
    debuglog("TODO make alerts table as orgid_alerts like other tables");
    
    utils.alertLinkContent(data.alertId,function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
        if (isError == 0) {
              debuglog(results);
              data.linkContent = results.linkContent;
              processStatus(req,res,stat,data);
             }
         });
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;

