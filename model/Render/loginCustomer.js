function processRequest(req,res,data){
   if( typeof req.body.customerEmail === 'undefined' && typeof req.body.customerId === 'undefined')
      processStatus(req,res,6,data); //system error 
   else 
     {
      if(typeof req.body.customerEmail != 'undefined')
         checkCustomerStatus(req,res,data);
      else
         checkCustomerStatusById(req,res,data);
     }
         
}


function checkCustomerStatusById(req,res,data){
    
    var custId    = req.body.customerId;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var bizData = {};
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         custStat: function(callback){utils.checkCustomerId(custId,callback); } 
         },
         function(err,results) {
          if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             data.customerId = results.custStat.customerId;
             bizData.custId = results.custStat.customerId;
			 if(results.custStat.isFollowing == false)
				 addFollowingData(req,res,9,data,bizData);
			 else
             getFollowingData(req,res,9,data,bizData);
            }
         }); 
}

function checkCustomerStatus(req,res,data){
    
    var email = req.body.customerEmail;
    var passwd    = req.body.password;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var bizData = {};
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         custStat: function(callback){utils.checkCustomer(email,passwd,callback); } 
         },
         function(err,results) {
          if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             data.customerId = results.custStat.customerId;
             bizData.custId = results.custStat.customerId;
			 if(results.custStat.isFollowing == false)
				 addFollowingData(req,res,9,data,bizData);
			 else
                 getFollowingData(req,res,9,data,bizData);
            }
         }); 
}

function addFollowingData(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    debuglog("Since adding customer following is moved to login from downloadAPK") ;
	debuglog("Add record here and set is_following to true in cust master");
     async.parallel({
         folStat: function(callback){utils.addCustFollow(bizData.custId,callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && !(results.folStat.errorCode != 0 || results.folStat.errorCode !=1)) {
             processStatus(req,res,results.folStat.errorCode,data);
             isError = 1;
             return;
            }
          if (isError == 0) {
             bizData.businesses = results.folStat.bizArray;
              getFollowingData(req,res,9,data,bizData);
            }
         }); 
  
}
function getFollowingData(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
      async.parallel({
         folStat: function(callback){utils.custFollow(bizData.custId,callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && !(results.folStat.errorCode != 0 || results.folStat.errorCode !=1)) {
             processStatus(req,res,results.folStat.errorCode,data);
             isError = 1;
             return;
            }
          if (isError == 0) {
             bizData.businesses = results.folStat.bizArray;
              getBizData(req,res,9,data,bizData);
            }
         }); 
  
}

function getBizData(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
    async.parallel({
         detailStat: function(callback){utils.businessDetails(bizData,callback); } 
         },
         function(err,results) {
            if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && !(results.detailStat.errorCode != 0 || results.detailStat.errorCode !=1)) {
             data = {};
             processStatus (req,res,results.detailStat.errorCode,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             data.numFollowing = results.detailStat.countBiz;
             data.businessDetails = results.detailStat.bizDetailArr;
             processStatus(req,res,9,data);
            }
         }); 
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;

