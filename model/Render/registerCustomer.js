function processRequest(req,res,data){
   if( typeof req.body.customerEmail === 'undefined')
      processStatus(req,res,6,data); //system error 
   else 
      checkInvitationStatus(req,res,data);
}


function checkInvitationStatus(req,res,data){
    
    var customerEmail = req.body.customerEmail;
    var utils = require('./utils');
    var bizData = {};
    
    
    var tblName = 'cust_master';
     utils.checkInvite(tblName,customerEmail,function(err,result){ 
          if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             return;
            }
          else{
           if(result.errorCode != 0) {
             processStatus (req,res,result.errorCode,data);
             return;
            }
           else {
             bizData.customerEmail = customerEmail;
             bizData.invitationCode = result.invitationCode;
             data.customerId = result.customerId;
             bizData.customerId = result.customerId;
             getBusinessDetails(req,res,9,data,bizData);
            }
          }
         }); 
}

function getBusinessDetails(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('./utils');
   
    var orgId = bizData.invitationCode.substring(0,9); //first 9 digits of invitation Code is OrgId
    var tblName = orgId+'_invitation_header';
     utils.getBizDetails(tblName,bizData,function(err,result){ 
          if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             return;
            }
          else{
           if(result.errorCode != 0) {
             processStatus (req,res,4,data);
             return;
            }
           else {
             bizData.orgId = result.orgId;
             checkBusinessStatus(req,res,9,data,bizData);
            }
          }
         }); 
}


function checkBusinessStatus(req,res,stat,data,bizData){
    
    var async = require('async');
    var utils = require('../Roles/roleutils');
    var isError = 0;
    
     async.parallel({
         usrStat: function(callback){utils.orgStatus(bizData.orgId,callback); } 
         }, 
     function(err,results) {
           if(err){
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.usrStat.errorCode != 0) {
            processStatus(req,res,1,data);
            isError = 1;
            return;
            }
           if(isError == 0){
            bizData.businessType = results.usrStat.bizType;
            createCustomerRecord(req,res,9,data,bizData);
            }
     });

}

function createCustomerRecord(req,res,stat,data,bizData){
    var utils = require('./utils');
    var isError = 0;
    var deviceData = {};
    deviceData.customerName = req.body.customerName;
    deviceData.password = req.body.password;
    deviceData.osver = req.body.OS_VERSION;
    deviceData.osapi = req.body.OS_API_LEVEL;
    deviceData.device = req.body.DEVICE;
    deviceData.model = req.body.MODEL;
    deviceData.product = req.body.PRODUCT;
    deviceData.manufact = req.body.MANUFACTURER;
  
    utils.createCust(data,bizData,deviceData,function(err,result){ 
        if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          else{
             updateInvitationData(req,res,9,data,bizData);
            }
         }); 
}

function updateInvitationData(req,res,stat,data,bizData){
    var invitationutils = require('../Invitation/dataUtils');
    var isError = 0;
    
    var tblName = bizData.orgId+"_invitation_header"
    debuglog(bizData);
    invitationutils.updateRegisterCount(tblName,bizData,function(err,result){ 
        if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          else{
            processStatus(req,res,9,data);
            }
         }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
