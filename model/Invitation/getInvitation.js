function processRequest(req,res,data){
   if(typeof req.body.invitationCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkInvitationStatus(req,res,data);
}

function checkInvitationStatus(req,res,data){
    
     var async = require('async');
     var utils = require('./dataUtils');
     var isError = 0;
    
     async.parallel({
         inviteStat: function(callback){utils.inviteMethods(callback); }
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
              data.inviteMethods = results.inviteStat.inviteTypes;
              getInvitationData(req,res,9,data);
            }
          }); 
}


function getInvitationData(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.invitationCode = req.body.invitationCode;
    var tid = req.body.tabId;
    
    if(tid == 501)
     var tabl_name = data.orgId+'_invitation_header';
    else
     var tabl_name = data.orgId+'_Revision_invitation_header';
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
     
  
      datautils.invitationHeader(tabl_name,inviteData,function(err,result) {
      
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              processStatus(req,res,3,data);
              isError = 1;
              return;
              }
            else {
                var inviteData = {};
                inviteData.invitationCode = data.invitationCode;
                inviteData.invitationLife = result.invitationLife;
                inviteData.inviteMethod   = result.inviteMethod;
                inviteData.content = result.content;
                getInvitees(req,res,9,data,inviteData);
            }
          }
        });
} 


function getInvitees(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var tid = req.body.tabId;
  
    if(tid == 501)
     var tabl_name = data.orgId+'_invitation_details';
    else
     var tabl_name = data.orgId+'_Revision_invitation_details';
   
    datautils.invitationDetails(tabl_name,inviteData,function(err,result) {
      
          if(err) {
               processStatus(req,res,6,data);
               return;
             }
         else {
                inviteData.recipients = result.invitees;
                data.invitationData = inviteData;
                processStatus(req,res,9,data);
            }
        });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;





