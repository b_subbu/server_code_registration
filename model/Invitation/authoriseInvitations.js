function processRequest(req,res,data){
   if(typeof req.body.invitationCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkInvitationCode(req,res,9,data);
}


function checkInvitationCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   data.invitationCodes =  req.body.invitationCodes;
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
   
     var tabl_name = data.orgId+'_Revision_invitation_header'; //currently can only edit from Active tab
   
      async.forEachOf(inviteData.invitationCodes,function(key,value,callback){
       inviteData.invitationCode = key;
      datautils.invitationHeader(tabl_name,inviteData,function(err,result) {
  
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(result.errorCode != 0 ) 
              isError = 1;
           
              callback();
            }
        });
        },
       function(err){
           if(err)
              processStatus(req,res,6,data);
           else{
             if(isError == 1) 
              processStatus(req,res,3,data);
            else {
                moveToRevisionHeader(req,res,9,data,inviteData);
                //processStatus(req,res,9,data);
            }
          }
        });
} 

function moveToRevisionHeader(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
   
     var source_tabl_name = data.orgId+'_Revision_invitation_header'; //currently can only edit from Active tab
     var dest_tabl_name = data.orgId+'_invitation_header';
   
      async.forEachOf(inviteData.invitationCodes,function(key,value,callback){
  
      var invitationCode = key;
      datautils.moveHeader(source_tabl_name,dest_tabl_name,invitationCode,inviteData,function(err,result) {
  
          if(err) {
               isError = 1;
               callback();
             }
         else {
              callback();
            }
          });
        },
       function(err){
           if(err)
              processStatus(req,res,6,data);
           else{
             if(isError == 1) 
              processStatus(req,res,3,data);
            else {
                moveToRevisionDetails(req,res,9,data,inviteData);
                }
          }
        });
} 

function moveToRevisionDetails(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
   
     var source_tabl_name = data.orgId+'_Revision_invitation_details'; //currently can only edit from Active tab
     var dest_tabl_name = data.orgId+'_invitation_details';
   
      async.forEachOf(inviteData.invitationCodes,function(key,value,callback){
  
      var invitationCode = key;
      datautils.moveDetails(source_tabl_name,dest_tabl_name,invitationCode,inviteData,function(err,result) {
  
          if(err) {
               isError = 1;
               callback();
             }
         else {
              callback();
            }
          });
        },
       function(err){
           if(err)
              processStatus(req,res,6,data);
           else{
             if(isError == 1) 
              processStatus(req,res,3,data);
            else {
                 changeInvitationStatus(req,res,9,data,inviteData);
            }
          }
        });
} 

function changeInvitationStatus(req,res,stat,data,inviteData) {
   var async = require('async');
   var statutils = require('./statUtils');
   var isError = 0;
   
  var tblName = data.orgId+'_invitation_header';
    async.forEachOf(inviteData.invitationCodes,function(key,value,callback){
  
      var invitationCode = key;
  
      statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,invitationCode,82,'Invitation Approved',function(err,result) {
      
          if(err) {
               isError = 1;
               callback();
               }
          else {
              callback();
              }
        });
       },
       function(err){
        if(err || isError == 1) {
               processStatus(req,res,6,data);
               }
          else {
              getInvitationData(req,res,9,data,inviteData,tblName);
              }
        }); 
} 

function getInvitationData(req,res,stat,data,inviteData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
   
      datautils.invitationsData(tblName,data.orgId,inviteData.invitationCodes,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
              invitations = result.invitations;
              getInvitationStatusName(req,res,9,data,invitations);
              }
        }); 
} 

function getInvitationStatusName(req,res,stat,data,invitations) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(invitations,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('invitation_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.invitationData = invitations;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData =  processRequest;
