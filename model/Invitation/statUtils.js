function changeStatus(tablName,orgId,bizType,operatorId,invitationCode,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {invitationCode: invitationCode}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createStatusChngLog(orgId,bizType,operatorId,invitationCode,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createStatusChngLog(orgId,bizType,operatorId,invitationCode,stat,reason,callback){
 
  var promise = db.get('invitation_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    invitation_code: invitationCode,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }
 
exports.changeStatus = changeStatus;

