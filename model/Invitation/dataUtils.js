function getInvitationsCount(orgId,bizType,tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr});
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

//TODO see if we can combine these in common dataUtils by getting
//column names from productHeader
function getInvitationsHeader(tblName,orgId,bizType,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
   //var promise = db.get(tblName).find({$and:[{org_id:orgId},{status_id: {$in: slit}}]},{sort: {update_time: -1}, limit: size, skip: skip});
   var promise = db.get(tblName).find({$and:sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {invitationCode: doc.invitationCode,
                        numberSent:doc.numberSent,
                        numberRegistered:doc.numberRegister,
                        numberFollow:doc.numberFollow,
                        activityDate:moment(doc.update_time).format('DD-MM-YYYY'),
                        status:doc.status_id};
        
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.invitations = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.invitations = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getInvitationMethods(callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
  var promise = db.get('invitation_methods_master').find({});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {id: doc.method_id,name:doc.method_name};
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
        data.inviteTypes = resultArray;
         callback(null,data);
     });
   promise.on('error',function(err){
       console.log(err);
     });
} 

function getInvitationHeader(tblName,inviteData,callback){
   var totalRows =0;
   var data = {};
  
  var promise = db.get(tblName).find({invitationCode:inviteData.invitationCode});
   promise.each(function(doc){
        totalRows++;
        data.createDate = doc.createDate;
        data.expiryDate = doc.expiryDate;
        data.invitationLife = doc.invitationLife;
        data.numberSent = doc.numberSent;
        data.numberDownload = doc.numberDownload;
		data.inviteMethod = doc.inviteMethod;
        data.content = doc.content;
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 

function getInvitationDetails(tblName,inviteData,callback){
   var totalRows =0;
   var resultArray = [];
   var data = {};
  
  var promise = db.get(tblName).find({invitationCode:inviteData.invitationCode});
   promise.each(function(doc){
        totalRows++;
        resultArray.push(doc.emailId);
         }); 
   promise.on('complete',function(err,doc){
         data.invitees = resultArray; 
         callback(null,data);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 

function generateInvitationCode(inviteData,callback){
  var currseqnum;
  var next_seq_str;
  var totalRows = 0;

  var promise = db.get('invitation_sequence').find({orgId:inviteData.orgId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    currseqnum = doc.running_sequence;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
      db.get('invitation_sequence').insert(
      { orgId: inviteData.orgId,running_sequence: '0001'});
      invitationCode = inviteData.orgId+'0001';
      callback(null,invitationCode);
    }
    else {
      var next_sequence = parseInt(currseqnum);
      next_sequence++;
      next_seq_str = String("0000000"+next_sequence).slice(-4);
      var promise = db.get('invitation_sequence').update({ orgId:inviteData.orgId},
      {  $set: { running_sequence: next_seq_str }});
      invitationCode = inviteData.orgId+next_seq_str;
      callback(null,invitationCode);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}

//We are using update for both create and Edit since Edit is not possible after Release
//if need to Edit Invitations after Release :) change this
//also both create and edit will not change status from Entered to Save 
function updateInvitationHeader(tblName,invitationCode,inviteData,callback){

var promise = db.get(tblName).update(
      {invitationCode:invitationCode},
        {$set: {org_id: inviteData.orgId,
          numberSent: 0,
          numberDownload: 0,
          numberRegister: 0,
          numberFollow:0,
          content: inviteData.content,
          createDate: new Date,
          status_id: 80,
          invitationLife: inviteData.invitationLife,
          inviteMethod: inviteData.inviteMethod,
          expiryDate: inviteData.expiryDate,
          releaseDate: inviteData.releaseDate,
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
}    


//inviteMethod here is dummy may be useful if same ID to be sent with multiple
//invitations via different methods later
function updateInvitationDetails(tblName,invitationCode,inviteData,recepients,callback){

var async = require('async');

async.forEachOf(recepients,function(key1,value1,callback1){

 var invitationId = invitationCode+value1;
 var promise = db.get(tblName).update(
  {invitationId: invitationId},
  {$set:{ invitationCode: invitationCode,
          emailId: key1,
          inviteMethod: inviteData.inviteMethod,
          apkVersion: 1,
          isDownloaded: false
      }},
       {upsert:true}
  );  
  promise.on('success',function(err,doc){
        callback1();
       });
   promise.on('error',function(err){
       callback1();
     });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           callback(null,null);
          }
     });
 

}    

//We will delete all entries first and then recreate 
//because in Edit screen it is possible some receipents might have been deleted
//Invitation Id may change which is fine since it is not yet sent out

function editInvitationDetails(tblName,invitationCode,inviteData,recepients,callback){
 var promise = db.get(tblName).remove({invitationCode: invitationCode});  
  promise.on('success',function(err,doc){
       updateInvitationDetails(tblName,invitationCode,inviteData,recepients,callback);
       });
   promise.on('error',function(err){
       callback(6,null);
     }); 
}    

function moveInvitationHeader(srcTable,destTable,invitationCode,inviteData,callback){

var promise1 = db.get(destTable).remove({invitationCode:invitationCode});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({invitationCode:invitationCode});
  
  //there must be only one row per invitationCode 
  //this is checked in check invitation code part
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({invitationCode:invitationCode});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

//This is more complicated with callback cascading and forEachOf
//TODO move to waterfall later 
function moveInvitationDetails(srcTable,destTable,invitationCode,inviteData,callback){
 var async = require('async');
var promise1 = db.get(destTable).remove({invitationCode:invitationCode});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({invitationCode:invitationCode});
  
   promise2.on('complete',function(err,doc){
    async.forEachOf(doc,function(key,value,callback1){ 
    
     delete key._id;
     var promise3 = db.get(destTable).insert(key);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({invitationId:key.invitationId});
          promise4.on('success',function(err,doc){
            callback1(null,null);
           });
          promise4.on('error',function(err,doc){
            callback1(6,null);
          });
       });
       
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
    },
    function(err){
     if(err)
       callback(6,null);
     else
     callback(null,null); //if it reaches here means no error in promise1 or 2
     });
    }); 
    promise2.on('error',function(err,doc){
       callback(6,null);
      });
    });
   promise1.on('error',function(err,doc){
        callback(6,null);
    });
  
}

function getInvitationsData(tblName,orgId,invitationCodes,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment = require('moment');
   var promise = db.get(tblName).find({$and:[{org_id:orgId},{invitationCode: {$in: invitationCodes}}]},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {invitationCode: doc.invitationCode,numberSent:doc.numberSent,numberRegistered:doc.numberRegister,numberFollow:doc.numberFollow,activityDate:moment(doc.update_time).format('DD-MM-YYYY'),status:doc.status_id};
        
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('success',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.invitations = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.invitations = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

//set status to Expired if expiry date is less than today (i.e., expiry happened)
//And status not in Expired, deleted or Rejected
//There can be a problem where if Invitation pending in Ready2Auth for more than expiry 
//TODO do same update/delete in pending tab if required
function updateExpiryRows(tblName,callback){
   var resultArray = [] ;
   var promise = db.get(tblName).update(
   {$and:[{status_id: {$nin: [86,83]}},{expiryDate:{$lte:new Date()}}]},
   {$set:{status_id:85}},
   {$multi: true});
   promise.on('complete',function(err,doc){
         callback(null,null);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function deleteInvitationData(tblName,invitationCodes,inviteData,callback){
   var promise = db.get(tblName).remove({invitationCode: {$in: invitationCodes}});
     promise.on('complete',function(err,doc){
          callback(null,null);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 


function updateSentDetails(headerTblName,detailTblName,invitationCode,inviteData,callback){
   var promise = db.get(detailTblName).count({invitationCode:invitationCode});
     promise.on('complete',function(err,doc){
          var promise1 = db.get(headerTblName).update(
          {invitationCode:invitationCode},
          {$set:{numberSent:doc,
                releaseDate: new Date}
          },
          {upsert: false});
          promise1.on('complete',function(err,doc){
             callback(null,null);
          });
          promise1.on('error',function(err){
           callback(6,null);
          });
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function sendInvitations(tblName,invitationCode,invitationData,callback){
var totalRows = 0;
var resultArr = [];

var promise = db.get(tblName).find({invitationCode: invitationCode});

promise.each(function(doc){
 var tmpArr = {};
     tmpArr.invitationId = doc.invitationId;
     tmpArr.emailId      = doc.emailId.text;
     tmpArr.inviteMethod = doc.inviteMethod;
     resultArr.push(tmpArr);
});

promise.on('complete',function(err,doc){
 sendEmailInvitations(resultArr,invitationData);
 //TODO check inviteMethod and call different social media methods
 //TODO check isDownloaded flag to handle multiple clicks
 });
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function sendEmailInvitations(invitationArray,invitationData){

//Check for each if the link is specified replace with apk url
//else add APK url at end
var async=require('async');
 async.forEachOf(invitationArray,function(key,value,callback){
      
       var apk_url = global.nodeURL+'/customer/app/downloadAPK/?invitationId='+key.invitationId;
       var re = /http:\/\/www.mydimesystems.com\/invite/gi;
    
      var emailId = key.emailId;
      
      var emailComps = emailId.split("@");
      var emailNames = emailComps[0].split("."); //in case if name is in abc.def@xyz.com format
      
      var email_text = "Hi "+emailNames[0].charAt(0).toUpperCase()+emailNames[0].substr(1)+",<br/>";
      
      
      email_text += invitationData.content;
      if(email_text.search(re) == -1){
      
      var new_email_text = email_text;
          new_email_text += "<br/><br/>";
          new_email_text += "<b>Download Link:</b>";
          new_email_text += apk_url
          new_email_text += "<br/>";
      
      } 
      else {
        var new_email_text = email_text.replace(re,apk_url);
      }
      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendmail(key.emailId,'APK Download mail',new_email_text);
      
    },
    function(err){
    if(err)
      callback(6,null);
    else
      callback(null,null);
    });
  



}

function checkInvitationId(tblName,invitationData,callback){
var totalRows = 0;
var data = {};

var promise = db.get(tblName).find({invitationId: invitationData.invitationId});

promise.each(function(doc){
debuglog("Getting data");
debuglog(doc);
 totalRows++;
     data.invitationCode = doc.invitationCode;
     data.emailId      = doc.emailId.text;
     data.inviteMethod = doc.inviteMethod;
     data.isDownloaded = doc.isDownloaded;
     data.isRegistered = doc.isRegistered;
     data.previousRequestTimeStamp = doc.requestTimeStamp;
     
 });

promise.on('success',function(err,doc){

 if(totalRows != 1){
   data.errorCode = 1;
   callback(null,data);
   }
  else{
 
   data.errorCode = 0;
   callback(null,data); //other values populated above
   }
 //TODO handle APK version and multiple Download scenarios
 });
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function checkInvitationCode(tblName,invitationData,callback){
var totalRows = 0;
var data = {};

var promise = db.get(tblName).find({invitationCode: invitationData.invitationCode});

promise.each(function(doc){
 totalRows++;
     data.expiryDate = doc.expiryDate;
     data.orgId = doc.org_id;
     data.numberClick = doc.numberClick;
     data.numberDownload = doc.numberDownload;
     data.numberRegister  = doc.numberRegister;
     data.numberFollow    = doc.numberFollow;
});

promise.on('success',function(err,doc){

 if(totalRows != 1){
   data.errorCode = 1;
   callback(null,data);
   }
  else{
   var today = new Date();
   var expiry = new Date(data.expiryDate);
   var diff = expiry-today;
   debuglog(today);
   debuglog(expiry);
   debuglog(diff);
   if(diff < 0)
      data.errorCode = 1;
   else
      data.errorCode = 0;
   callback(null,data); 
   }
 });
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}


function updateInvitationHeaderDownloads(tblName,invitationData,callback){

var promise = db.get(tblName).update(
{invitationCode: invitationData.invitationCode},
{$set:
{numberDownload:invitationData.numberDownload,
 numberClick: invitationData.numberClick,
 numberRegister: invitationData.numberRegister,
 numberFollow: invitationData.numberFollow}});
promise.on('success',function(err,doc){
   callback(null,null); 
 });
promise.on('error',function(err){
  console.log(err);
  callback(6,null);
});

}


function updateInvitationDetailDownloads(tblName,invitationData,callback){

var promise = db.get(tblName).update(
{invitationCode: invitationData.invitationCode},
{$set:
 {isDownloaded:true,
  requestTimeStamp: new Date()
  }
});
promise.on('success',function(err,doc){
   callback(null,null); 
 });
promise.on('error',function(err){
  console.log(err);
  callback(6,null);
});

}

//These are functions from Render/Utils
//TODO retire some of them later if not required
function checkCustomer(tblName,inviteData,callback){
 
  var totalRows =0;
  var data = {};
  var password ="";
   
  var promise = db.get(tblName).find({cust_email: inviteData.email},{stream: true});
  promise.each(function(doc){
      totalRows++;
      data = {customerId:doc.cust_id, invitationCode: doc.invitation_code,isFollowing:doc.is_following};
      password = doc.pass_word;
	  debuglog("Got record for given customer id");
	  debuglog(password);
	  debuglog(data);
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0;
       createCustRecord(tblName,inviteData,data,callback);
       }
      else {
        if(typeof password == 'undefined' || password == null)
          data.errorCode = 1;
        else
          data.errorCode = 2;
        callback(null,data); 
        //customer email is already registered nothing to do further
        //otherwise customer has just downloaded but not registered
        //so allow to again download
      } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
}


//cust_id = InvitationId, cust_email invitation code
//and register date are created
//all other fields are updated in registerCustomer API
//This will be differeniated from normal row by empty Name+password fields
function createCustRecord(tblName,inviteData,data,callback){
 
   var promise = db.get(tblName).insert({
    cust_id: inviteData.invitationId,
    cust_email: inviteData.email,
    cust_name: inviteData.custName,
    pass_word: inviteData.custPasswd,
    cust_active: true,
    invitation_code: inviteData.invitationCode,
	  is_following: false,
    register_date: new Date(),
    });
  promise.on('success',function(doc){
  data.customerId = inviteData.invitationId;
  callback(null,data);
 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function checkCustomerFollowing(tblName,inviteData,callback){
 
  var totalRows =0;
  var data = {};
     
  var promise = db.get(tblName).find({$and:[{cust_id: inviteData.customerId},{org_id:inviteData.orgId}]});
  promise.each(function(doc){
      totalRows++;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0;
       createCustFollowing(tblName,inviteData,data,callback);
       }
      else {
        data.errorCode = 1;
        callback(null,data); //customer email is already following nothing to do further
      } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
}



function createCustFollowing(tblName,inviteData,data,callback){
  var promise = db.get(tblName).insert({
    cust_id: inviteData.customerId,
    org_id: inviteData.orgId,
    bus_type: inviteData.businessType,
    follow_date: new Date()
  });
  promise.on('success',function(doc){
    callback(null,data);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}


function getCustomerFollowing(inviteData,callback){

 var totalRows =0;
 var customerArr = [];

  var promise = db.get('cust_following').find({org_id: inviteData.orgId});
   promise.each(function(doc){
      totalRows++;
      customerArr.push(doc.cust_id)
   }); 
  promise.on('complete',function(doc){
    if(totalRows == 0)
      callback(null,customerArr);
    else
      getCustomerEmail(customerArr,callback);
  });
  promise.on('error',function(err){
    debuglog(err);
    callback(6,null);//system error
  });
}

function getCustomerEmail(custArr,callback){
 var resultArray = [];

  var promise = db.get('cust_master').find({cust_id: {$in: custArr}});
   promise.each(function(doc){
        resultArray.push(doc.cust_email);
        }); 
   promise.on('complete',function(err,doc){
      callback(null,resultArray);
  });
   promise.on('error',function(err){
       debuglog(err);
  });
  
}

function incrementRegistrationCount(tblName,invitationData,callback){

var promise = db.get(tblName).update(
{invitationCode: invitationData.invitationCode},
{$inc:{numberRegister:1, numberFollow:1}});
promise.on('success',function(err,doc){
   callback(null,null); 
 });
promise.on('error',function(err){
  console.log(err);
  callback(6,null);
});

}
exports.invitationCount = getInvitationsCount;
exports.invitationsHeader = getInvitationsHeader;
exports.inviteMethods = getInvitationMethods;
exports.invitationHeader = getInvitationHeader;
exports.invitationDetails = getInvitationDetails;
exports.generateInvitationCode = generateInvitationCode;
exports.invitationCreate = updateInvitationHeader;
exports.invitationDetailsCreate = updateInvitationDetails;
exports.invitationDetailsEdit = editInvitationDetails;
exports.moveHeader = moveInvitationHeader;
exports.moveDetails = moveInvitationDetails;
exports.invitationsData = getInvitationsData;
exports.deleteInvitations = deleteInvitationData;
exports.updateExpiryRows = updateExpiryRows;
exports.updateSentDetails = updateSentDetails;
exports.sendInvitations = sendInvitations;
exports.checkInvitationId = checkInvitationId;
exports.checkInvitationCode = checkInvitationCode;
exports.invitationHeaderDownloads = updateInvitationHeaderDownloads;
exports.invitationDetailDownloads = updateInvitationDetailDownloads;
exports.createCustMaster = checkCustomer;
exports.createCustFollowing =checkCustomerFollowing;
exports.registeredCustomers = getCustomerFollowing;
exports.updateRegisterCount = incrementRegistrationCount;


