function getInvitationSearch(req,res,oid,pid,tid,pgid,size,data) {

 var searchInp = data.searchParams;

 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         isOtherSearch = true;
         break;
       case 'invitationCode':
        var tmpArray = {invitationCode: key}; 
           searchArr.push(tmpArray);
           isOtherSearch = true;
         break; 
      case 'activityDate':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var minDaysArr = minDateArr[2].split("T"); //sometimes UI sends with timestamp
        var maxDaysArr = maxDateArr[2].split("T");
        var cmpDate = new Date(minDaysArr[0],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDaysArr[0],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        var tmpArray = {update_time: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {update_time: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
        isOtherSearch = true;
       break; 
       
      case 'numberSent':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       var tmpArray = {numberSent: {$gte:minCount}};
       searchArr.push(tmpArray);
       var tmpArray = {numberSent: {$lte: maxCount}};
       searchArr.push(tmpArray);
       break;
    
     case 'numberRegistered':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       var tmpArray = {numberRegister: {$gte:minCount}};
       searchArr.push(tmpArray);
       var tmpArray = {numberRegister: {$lte: maxCount}};
       searchArr.push(tmpArray);
        break;
  
    case 'numberFollow':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       var tmpArray = {numberFollow: {$gte:minCount}};
       searchArr.push(tmpArray);
       var tmpArray = {numberFollow: {$lte: maxCount}};
       searchArr.push(tmpArray);
        break;
        
        
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           getInvitationSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data);
         //Currently only status+other search combination is possible 
          }
     });
}

function getInvitationSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data){
   var totalRows =0;
   var resultArray = [] ;
   var moment = require('moment');

   var page = parseInt(pgid);
   var skip = page > 0 ? ((page - 1) * size) : 0;
      
     if(tid == 501)
      var tablName = oid+'_invitation_header';
    else
      var tablName = oid+'_Revision_invitation_header';
  
   var promise = db.get(tablName).find({$and:searchArr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
         totalRows++;
        var tmpArray = {invitationCode: doc.invitationCode,numberSent:doc.numberSent,numberRegistered:doc.numberRegister,numberFollow:doc.numberFollow,activityDate:moment(doc.update_time).format('DD-MM-YYYY'),status:doc.status_id};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.recordCount = totalRows;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       
       processStatus(req,res,9,data);
     }
      else {
        getInvitationStatName(req,res,9,data,resultArray);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);
    
	});
} 


function getInvitationStatName(req,res,stat,data,mastData) {
   var async = require('async');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
   var mastutils = require('../Registration/masterUtils');
  
    async.forEachOf(mastData,function(key1,value1,callback1){
      
        var masterData = mastData[value1]; //because mastdata is double array
        async.forEachOf(masterData,function(key2,value2,callback2){
             if(value2 == 'status'){
              var statID=key2;
          mastutils.masterStatusNames('invitation_status_master',statID,function(err,result) {
           if(err){ isError =1; callback2();}
         else {
                 //key2 = result;
                 mastData[value1]['status'] = result.statusName;
                 callback2(); 
              }
           }); 
          }
          else
              callback2();
        },
       function(err){
         if(err){
             isError =1;         
			 callback1();
			  }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    if(isError == 1)
		   	 processStatus (req,res,6,data);
        else {				
   	          populateSearchData(req,res,9,data,mastData);
			       }
         }
      });
 

}

function populateSearchData(req,res,stat,data,mastData){

 var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
     
      async.forEachOf(mastData,function(key1,value1,callback1){
      if(key1)
        itemMasters.push(key1);
      
        callback1();
        },
      function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    if(isError == 1)
			 processStatus (req,res,6,data);
            else
			{				
       data.searchData = itemMasters;
		   processStatus(req,res,9,data,mastData);
			}
        }
      });
 


}
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Search/searchData');
  
    controller.process_status(req,res,stat,data);
}

exports.searchInvitation = getInvitationSearch;


 


