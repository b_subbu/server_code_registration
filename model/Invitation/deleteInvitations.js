function processRequest(req,res,data){
   if(typeof req.body.invitationCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkInvitationCode(req,res,9,data);
}



function checkInvitationCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   data.invitationCodes = req.body.invitationCodes;
    
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
   
     var tabl_name = data.orgId+'_invitation_header'; 
   
      async.forEachOf(inviteData.invitationCodes,function(key,value,callback){
       inviteData.invitationCode = key;
      datautils.invitationHeader(tabl_name,inviteData,function(err,result) {
  
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(result.errorCode != 0 ) 
              isError = 1;
           
              callback();
            }
        });
        },
       function(err){
           if(err)
              processStatus(req,res,6,data);
           else{
             if(isError == 1) 
              processStatus(req,res,3,data);
            else {
                deleteHeader(req,res,9,data,inviteData);
                }
          }
        });
} 

function deleteHeader(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
   
     var tabl_name = data.orgId+'_invitation_header';
     datautils.deleteInvitations(tabl_name,data.invitationCodes,inviteData,function(err,result) {
  
          if(err) {
               isError = 1;
               callback();
             }
          else {
                deleteDetails(req,res,9,data,inviteData);
                }
        });
} 

function deleteDetails(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
   
     var tabl_name = data.orgId+'_invitation_details';
     datautils.deleteInvitations(tabl_name,data.invitationCodes,inviteData,function(err,result) {
  
          if(err) {
               isError = 1;
               callback();
             }
          else {
               processStatus(req,res,9,data);
            }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData =  processRequest;


