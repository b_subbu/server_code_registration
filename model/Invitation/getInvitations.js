//NOTE: this will be called from getProductData

function getInvitationsData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
   var utils = require('./dataUtils');

   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   data.pageId = pageId;
   
   var tabl_name = oid+'_invitation_header';
    //We will first expire data before getting counts
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'invitation_status_master',callback); },
         inviteStat: function(callback){utils.inviteMethods(callback); },
         expireData: function(callback){utils.updateExpiryRows(tabl_name,callback); }
     
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    data.inviteMethods = results.inviteStat.inviteTypes;
                    getFilterOptions(req,res,stat,data,statList);
                  }
                  else 
                   getInvitationsCount(req,res,stat,data,statList);
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList) {
   var async = require('async');
   var filterutils = require('../Menu/filterUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    if(tid == 501)
     var tabl_name = oid+'_invitation_header';
    else
     var tabl_name = oid+'_Revision_invitation_header';
     
 
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,'invitation_status_master',function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
          
             filterOptions[key_name] = results;
             callback();
          
          }
          }); 
        }
     else
      callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
           data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call alerts count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var userArray = {org_id: data.orgId };
           searchArr.push(userArray);
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
          getInvitationsCount(req,res,stat,data,searchArr,tabl_name);
        }
         else 
        getInvitationSearch(req,res,stat,data,statList,tabl_name);
         }
      });
} 

function getInvitationSearch(req,res,stat,data,searchArr,tabl_name) {

 var searchInp = req.body.searchParams;

 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 var userArray = {org_id: data.orgId };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         isOtherSearch = true;
         break;
       case 'invitationCode':
        var tmpArray = {invitationCode: key}; 
           searchArr.push(tmpArray);
           isOtherSearch = true;
         break; 
      case 'activityDate':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var minDaysArr = minDateArr[2].split("T"); //sometimes UI sends with timestamp
        var maxDaysArr = maxDateArr[2].split("T");
        var cmpDate = new Date(minDaysArr[0],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDaysArr[0],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        var tmpArray = {update_time: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {update_time: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
        isOtherSearch = true;
       break; 
       
      case 'numberSent':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       var tmpArray = {numberSent: {$gte:minCount}};
       searchArr.push(tmpArray);
       var tmpArray = {numberSent: {$lte: maxCount}};
       searchArr.push(tmpArray);
       break;
    
     case 'numberRegistered':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       var tmpArray = {numberRegister: {$gte:minCount}};
       searchArr.push(tmpArray);
       var tmpArray = {numberRegister: {$lte: maxCount}};
       searchArr.push(tmpArray);
        break;
  
    case 'numberFollow':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       var tmpArray = {numberFollow: {$gte:minCount}};
       searchArr.push(tmpArray);
       var tmpArray = {numberFollow: {$lte: maxCount}};
       searchArr.push(tmpArray);
        break;
        
        
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           getInvitationsCount(req,res,stat,data,searchArr,tabl_name);
         //Currently only status+other search combination is possible 
          }
     });
}


function getInvitationsCount(req,res,stat,data,statList,tabl_name){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   //TODO probably have only tabl_name no need for orgId or bizType
   //since table name itself will have them      
   debuglog("Let us start counting");
   debuglog(statList);
   utils.invitationCount(orgId,bizType,tabl_name,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data);
              //no Invitations for the business just get address book
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getInvitationHeaderData(req,res,9,data,statList,tabl_name); 
             }
            }
         });
}

function getInvitationHeaderData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var utils = require('./dataUtils');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   var orders = [];
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
     debuglog("Let us get Data");
     debuglog(statList);
  
      utils.invitationsHeader(headerTable,orgId,bizType,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data); //no Invitations for the business nothing to do further
              isError = 1;
             //will be handled at count itself, just adding here
            }
            else {
            invitations = result.invitations; 
            getInvitationStatusName(req,res,9,data,invitations);
            }
          }
        });
} 



function getInvitationStatusName(req,res,stat,data,invitations) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(invitations,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('invitation_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.productData = invitations;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getProductData = getInvitationsData;







