function processRequest(req,res,data){
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus is left for getting businessEmail")
}

function checkMasterStatus(req,res,data){
    
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var utils = require('./dataUtils');
    var isError = 0;
    var uid = data.userId;
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
             }
          if(isError == 0) {
              //data.orgId = results.usrStat.orgId;
              //data.businessType = results.usrStat.bizType;
              bizMail = results.usrStat.businessEmail,
              getInvitationCode(req,res,9,data,bizMail);
            }
          }); 
}


function getInvitationCode(req,res,stat,data,bizMail) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     var extend = require('util')._extend;
     var inviteData = extend({},data);
     
  
      datautils.generateInvitationCode(inviteData,function(err,result) {
      
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                data.invitationCode = result;
                getRegisteredCustomers(req,res,9,data,inviteData,bizMail);
            }
        });
} 



function getRegisteredCustomers(req,res,stat,data,inviteData,bizMail) {
   var async = require('async');
   var datautils = require('./dataUtils');


     datautils.registeredCustomers(inviteData,function(err,result) {
      
          if(err) {
               processStatus(req,res,6,data);
             }
         else {
                debuglog(result);
                debuglog("Registered customer array for the business");
                var registeredCustomers = result;
                deleteRegisteredCustomers(req,res,9,data,inviteData,bizMail,registeredCustomers);
              }
        });
} 

function deleteRegisteredCustomers(req,res,stat,data,inviteData,bizMail,registeredCustomers) {
   var async = require('async');
   var deletedCount = 0;
   var finalRecipients = [];
       var recipients = req.body.invitationData.recipients;
  
      async.forEachOf(recipients,function(key1,value1,callback){
        debuglog(registeredCustomers);
        debuglog(key1);
      if(registeredCustomers.indexOf(key1.text) != -1){
       debuglog("Deleting already registered customer");
       debuglog(key1);
       deletedCount++;
       debuglog(deletedCount);
       //recipients.splice(value1,1);
       debuglog("cant use splice since it shifts array causing async to fail");
       debuglog("make this null here and do check later to remove nulls")
       delete recipients[value1];
       debuglog(recipients);
        }
       callback();
      },
    function(err){
      if(err)
        callback(6,null);
      else {
           data.alreadyFollowingCount = deletedCount;
           filterDeletedInvitees(req,res,stat,data,inviteData,bizMail,recipients);
           debuglog("Do one more loop to remove all null values");
           debuglog("so that the count will match and no need to skip nulls in update");
           }
          
     });

      
} 
function filterDeletedInvitees(req,res,stat,data,inviteData,bizMail,recipients){
  var async = require('async');
  var finalRecipients = [];
  
  async.forEachOf(recipients,function(key1,value1,callback){
        debuglog(key1);
      if(key1 != null){
       debuglog("Pushing to new array");
       finalRecipients.push(key1);
       debuglog(finalRecipients);
        }
       callback();
      },
    function(err){
      if(err)
        callback(6,null);
      else {
           debuglog("final recipients");
            debuglog(finalRecipients);
           debuglog(finalRecipients.length);
           if(finalRecipients.length == 0){
             debuglog("All invitess are already following");
             debuglog("so show error and stop. Dont save");
             processStatus(req,res,7,data)
           }
           else
            saveInvitationData(req,res,stat,data,inviteData,bizMail,finalRecipients);
          }
     });

      


}

function saveInvitationData(req,res,stat,data,inviteData,bizMail,finalRecipients) {
   var async = require('async');
   var moment = require('moment');
   var datautils = require('./dataUtils');
 
 
  if(req.body.invitationData.invitationLife)
     expiryDay = parseInt(req.body.invitationData.invitationLife);
   else
     expiryDay = 3;
     
     expiryDate = moment().add(expiryDay, 'days').toDate();  

     inviteData.invitationLife = expiryDay;
     inviteData.expiryDate = expiryDate;
     inviteData.inviteMethod = req.body.invitationData.inviteMethod;
     inviteData.releaseDate = '';
     var content    = req.body.invitationData.content;
     
     inviteData.content = content;
  
      var tabl_name = data.orgId+'_invitation_header';
   
     
     datautils.invitationCreate(tabl_name,data.invitationCode,inviteData,function(err,result) {
      
          if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("Get already registered customers for this Business");
                saveInvitationDetails(req,res,9,data,inviteData,bizMail,finalRecipients);
            }
        });
} 

function saveInvitationDetails(req,res,stat,data,inviteData,bizMail,recipients) {
   var async = require('async');
   var datautils = require('./dataUtils');


     var tabl_name = data.orgId+'_invitation_details';
     datautils.invitationDetailsCreate(tabl_name,data.invitationCode,inviteData,recipients,function(err,result) {
        if(err) {
                processStatus(req,res,6,data);
                }
         else {
                changeInvitationStatus(req,res,9,data,inviteData,bizMail);
              }
        });
} 

function changeInvitationStatus(req,res,stat,data,inviteData,bizMail) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    if(req.body.submitOperation == 1){
     var submitmenu = require('./submitInvitations');
     data.invitationCodes = [];
     data.invitationCodes.push(data.invitationCode);
    submitmenu.processSubmission(req,res,9,data,bizMail);
   }
    else{ 
      var tblName = data.orgId+'_invitation_header';
      statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,data.invitationCode,80,'Invitation Created',function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
              getInvitationData(req,res,9,data,inviteData,tblName);
              }
        });
     }
} 

function getInvitationData(req,res,stat,data,inviteData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
     var inviteArr = [];
      inviteArr.push(data.invitationCode);
      datautils.invitationsData(tblName,data.orgId,inviteArr,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
              invitations = result.invitations;
              getInvitationStatusName(req,res,9,data,invitations);
              }
        }); 
} 

function getInvitationStatusName(req,res,stat,data,invitations) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(invitations,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('invitation_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.invitationData = invitations;
            if(data.alreadyFollowingCount == 0)
              processStatus(req,res,9,data);
            else
              processStatus(req,res,8,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;






