function processRequest(req,res,data){
   //THIS is a GET call
  
   var isAndroid = !!req.headers['user-agent'].match(/Android/);
  
   isAndroid = true;
  if(isAndroid == true){
    debuglog(isAndroid);
    debuglog("Is Android flag is true");
   if(typeof req.query.invitationId === 'undefined'){
         processStatus(req,res,6,data); //system error
       }
   else{ 
       checkInvitationId(req,res,9,data);
       }
    }
    else
      processStatus(req,res,5,data);
}

function checkInvitationId(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var invitationId = req.query.invitationId;
   
   var orgId = invitationId.substring(0,9); // orgId is always 9 characters 
    debuglog(orgId);
    
   var tabl_name =orgId+'_invitation_details';
  
     var extend = require('util')._extend;
     var inviteData = extend({},data);
     inviteData.orgId = orgId;
     inviteData.invitationId = invitationId;
     debuglog('inviteData:'+inviteData);
     datautils.checkInvitationId(tabl_name,inviteData,function(err,result) {
     debuglog(result.errorCode);
		      if(err)
              processStatus(req,res,6,data);
           else{
               if(result.errorCode == 1){
                   processStatus(req,res,3,data);
                   debuglog("Invitation code not found so error");
                   }
               else {
                var nowTime = new Date();
 
                if(result.previousRequestTimeStamp)
                  var earlierTimeStamp = new Date(result.previousRequestTimeStamp);
                else
                  var earlierTimeStamp = new Date(new Date().getTime()-5000) ;
                 debuglog("If previous request was made get that time");
                 debuglog("else make previous request as less than 5 seconds to avoid error")
                  
                 debuglog("The result was");
                 debuglog(result);
                 debuglog("Current time is:")
                 debuglog(nowTime);
                 debuglog("Earlier request was:")
                 debuglog(earlierTimeStamp);
                 var secondsDiff = (nowTime-earlierTimeStamp)/1000;
                 
                 debuglog("difference in seconds is:");
                 debuglog(secondsDiff);
                 if(secondsDiff < 3){
                   debuglog("This is a request coming within 3 seconds of last");
                   debuglog("Probably phone or network issues");
                   debuglog("So just allow request to die and not send back any response");
                   debuglog("So just dont do anything here and allow thread to die");
                   debuglog("This is not working with Android so just download again");
                   downloadAPKFile(req,res,stat,data);
                   //return;
                 }
                else{
                if(result.isRegistered == true){
                  processStatus(req,res,8,data);
                  debuglog("User has already registred against this invitation Id");
                  }
                  else {
                       inviteData.invitationCode = result.invitationCode;
                       inviteData.email = result.emailId;
                       inviteData.isDownloaded = result.isDownloaded;
                       checkInvitationCode(req,res,9,data,inviteData);
                       debuglog("Ok fine process further");
                       }
                  }
                }
              }
         });
} 

function checkInvitationCode(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
       var tabl_name =inviteData.orgId+'_invitation_header';
   
      datautils.checkInvitationCode(tabl_name,inviteData,function(err,result) {
            if(err)
              processStatus(req,res,6,data);
           else{
              if(result.errorCode == 1){
                 processStatus(req,res,4,data);
                 debuglog("either the invitation code does not exist");
                 debuglog("or had already expired");
                 }
              else{ 
                data.orgId = result.orgId;
                inviteData.orgId = result.orgId;
                inviteData.numberClick = result.numberClick; 
                inviteData.numberDownload = result.numberDownload;
                inviteData.numberRegister = result.numberRegister;
                inviteData.numberFollow   = result.numberFollow;
                (inviteData.numberClick != null)?inviteData.numberClick++:inviteData.numberClick=0;
                debuglog("Only number click is incremented here");
                debuglog("Other counts are incremented in those functions appropriately");
                checkMasterStatus(req,res,9,data,inviteData);
                debuglog("checkMasterStatus left asis since no orgcheck for this call");
                }
              }
        });
} 

function checkMasterStatus(req,res,stat,data,inviteData){
    
    var oid = data.orgId;
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    
     async.parallel({
         usrStat: function(callback){roleutils.orgStatus(oid,callback); },
         business: function(callback) {roleutils.emailStatus(inviteData.email,callback); } 
         },
         function(err,results) {
         debuglog(results);
         debuglog("Getting result from orgstatus and user email check");
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
             }
          if(isError == 0) {
              data.businessType = results.usrStat.bizType;
              inviteData.orgId = results.usrStat.orgId;
              inviteData.businessType = results.usrStat.bizType;
              businessData = results.business;
              checkBusinessEmail(req,res,9,data,inviteData,businessData);
            }
          }); 
}

function checkBusinessEmail(req,res,stat,data,inviteData,businessData){

               debuglog(businessData);
               if(businessData.isBusiness == false){
               debuglog("If no business record exist or business user is inactive");
               debuglog("Then no change in current flow");
               debuglog("Note password must be undefined and not as null in table");
               updateCustMaster(req,res,9,data,inviteData);
               }
               else{
               debuglog("But if active business record exists then use the name and password from there");
               debuglog("Some race conditions like first download second invite etc., may not be handled");
               debuglog("TODO later figure out all possible cases and handle them here");
               inviteData.custName = businessData.userName;
               inviteData.custPasswd = businessData.userPass;
               inviteData.numberRegister++;
               inviteData.numberFollow++;
               updateCustMaster(req,res,9,data,inviteData);
               }
}


function updateCustMaster(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   var tabl_name = 'cust_master';
      datautils.createCustMaster(tabl_name,inviteData,function(err,result) {
         debuglog(result);
           if(err)
              processStatus(req,res,6,data);
            else {
             inviteData.customerId = result.customerId;
             if(result.errorCode == 0){
              debuglog("First time download so create cust following ");
              debuglog("and just increment download click and allow file download");
              debuglog("incrementing of numberRegister and numberFollow will be done in Register customer API");
              inviteData.numberDownload++;
              updateCustFollowing(req,res,9,data,inviteData);
             }
             if (result.errorCode == 1){
              debuglog("Already downloaded but yet to register");
              debuglog("So just increment download click and allow file download");
              inviteData.numberDownload++;
              updateInvitationHeader(req,res,9,data,inviteData)
              }
             if(result.errorCode == 2){
                 debuglog(inviteData);
                 if(result.invitationCode == inviteData.invitationCode){
                   debuglog("Already registered under this same Invitation Code");
                   debuglog("So error condition but just show thanks as response");
                   debuglog("Just allow re-download without showing any message");
                   debuglog("or incrementing any counts do same for all of below :)")
                   processStatus(req,res,8,data);
                   //numberclicks will not be updated here, which may be just fine
                   }
                else{
                 var currentOrgId = inviteData.invitationId.substr(0,9);
                 var registeredOrgId = result.invitationCode.substr(0,9);
                 
                  if(currentOrgId == registeredOrgId){
                   debuglog("Already Following this business but under different Invitation Code");
                   debuglog("So just show thanks as response");
                    processStatus(req,res,8,data);
                   //numberclicks will not be updated here, which may be just fine
                  }
                  else{
                    debuglog("Already Registered but under different Business Invitation");
                    debuglog("So increment registered and following counts eventhough only add following");
                    debuglog("Add as a follower to this business");
                    debuglog("But no need to download APK or rereigster striked ");
                    inviteData.numberDownload++;
                    inviteData.numberRegister++; 
                    inviteData.numberFollow++;
                    inviteData.customerRegistered++;
                    updateCustFollowing(req,res,9,data,inviteData);
                  }
                }
              }
           }
               
        });
} 


function updateCustFollowing(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   debuglog(inviteData);
   debuglog("&&&&&&");
 
    //if customer is already following the business just ignore
    var tabl_name = 'cust_following';
      datautils.createCustFollowing(tabl_name,inviteData,function(err,result) {
  
            if(err)
              processStatus(req,res,6,data);
            else {
             if(result.errorCode == 1){
               debuglog("Already following this business somehow");
               debuglog("So just show thanks as response");
               processStatus(req,res,8,data);
              }
             else
             updateInvitationHeader(req,res,stat,data,inviteData);
            }
        });
} 

function updateInvitationHeader(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   
   //here just update numDownloads 
     var tabl_name = data.orgId+'_invitation_header';
   
        datautils.invitationHeaderDownloads(tabl_name,inviteData,function(err,result) {
         if(err)
              processStatus(req,res,6,data);
         else 
           updateInvitationDetails(req,res,stat,data,inviteData);
        });
   } 

function updateInvitationDetails(req,res,stat,data,inviteData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   //here just update isDownloaded to true
   //Currently we are not storing download time stamp here
   //rather it will be in customer master register_date
     var tabl_name = data.orgId+'_invitation_details';
    datautils.invitationDetailDownloads(tabl_name,inviteData,function(err,result) {
         if(err)
              processStatus(req,res,6,data);
         else {
           debuglog("check if registered customer or not");
           debuglog(" if already registered process status");
           debuglog("else download APK file");
           debuglog(inviteData);
           if(inviteData.customerRegistered == 1){
              processStatus(req,res,stat,data);
              debuglog("already registered customer it seems not sure how it came here");
              }
           else
             downloadAPKFile(req,res,stat,data); //we dont show success message because file download would have started
             
           }
        });

} 




function downloadAPKFile(req,res,stat,data){
var http = require('http');
var fs = require('fs');

var dest = "customer_app.apk";

  var file = app.get('publicfolder') + 'customer_app.apk';
  res.download(file,function(err){
    debuglog("Errored out");
    debuglog(err);
    });
  
  //res.redirect('back');
}

function processStatus(req,res,stat,data) {
   debuglog("Controller not changed as javascript alert is there");
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Invitation/downloadAPK');
  
    controller.process_status(req,res,stat,data);
}

exports.getData =  processRequest;


