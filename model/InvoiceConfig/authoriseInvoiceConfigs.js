function processRequest(req,res,data){
    if(typeof req.body.invoiceConfigIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkInvoiceConfigStatus(req,res,9,data);
}


function checkInvoiceConfigStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var InvoiceConfigArr = req.body.invoiceConfigIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.invoiceConfigIds = InvoiceConfigArr;
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_Revision_invoice_config'; //TODO: move to a common definition file

    async.forEachOf(InvoiceConfigArr,function(key,value,callback1){
        var InvoiceConfigData= {};
            InvoiceConfigData.invoiceConfigId = key;
        datautils.checkInvoiceConfigCode(tabl_name,InvoiceConfigData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
               else{
                 if (result.status == 81){
                   debuglog("This is entered status item");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 89){
                   debuglog("This is authorised delete item");
                   debuglog("So change status to deleted");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Authorise is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
             }
           });       
        }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
          else{
            debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              moveToMain(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
             }
          }
      });
}


function moveToMain(req,res,stat,data,enteredItems,authorisedItems){
    
    
    debuglog("Now start moving the entries to Main Table");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_invoice_config'; //TODO: move to a common definition file
    var mainTable    = orgId+'_invoice_config';
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      invoiceConfigId = key;
     async.parallel({
          relStat: function (callback){datautils.moveInvoiceConfigHeader(revisionTable,mainTable,invoiceConfigId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
            if(authorisedItems.length > 0){
          debuglog("This request has some authorised to delete items too");
          debuglog(authorisedItems);
          deleteInMain(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable);
          }
          else{
          debuglog("This request has no authorised items");
             changeInvoiceConfigStatus(req,res,9,data,enteredItems,mainTable,revisionTable); 
          } 
         }
     });
}

function deleteInMain(req,res,stat,data,enteredItems,authorisedItems,mainTable,revisionTable){
    
    
    debuglog("Now start moving the entries to Main Table");
    debuglog("It is just enough to update status and remove from Main here");
    debuglog("But retaining functionality as is to reduce code");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
    debuglog("This may not really be required for Ready2Delete just keeping same to reduce code");
    
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      invoiceConfigId = key;
     async.parallel({
          relStat: function (callback){datautils.moveInvoiceConfigHeader(revisionTable,mainTable,invoiceConfigId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeInvoiceConfigStatusDelete(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable); 
         }
     });
}

function changeInvoiceConfigStatusDelete(req,res,stat,data,enteredItems,authorisedItems,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(authorisedItems,function(key,value,callback1){
    invoiceConfigId = key;
    var reason = 'Invoice Config Deleted';
       statutils.changeInvoiceConfigStatus(mainTable,data.orgId,data.businessType,data.userId,invoiceConfigId,88,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 changeInvoiceConfigStatus(req,res,9,data,enteredItems,mainTable,revTable);
                 debuglog("It is ok to call even if no enteredItems");
                 debuglog("As it will simply skip the loop");
             }
          });
     
}


function changeInvoiceConfigStatus(req,res,stat,data,InvoiceConfigArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(InvoiceConfigArr,function(key,value,callback1){
    invoiceConfigId = key;
    var reason = '';
       statutils.changeInvoiceConfigStatus(mainTable,data.orgId,data.businessType,data.userId,invoiceConfigId,82,'Invoice Config Authorised',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.invoiceConfigIds = InvoiceConfigArr;
                 getInvoiceConfigDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getInvoiceConfigDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.InvoiceConfigsData(data.orgId,mainTable,data.invoiceConfigIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getInvoiceConfigStatusName(req,res,9,data,result); 
         }
     });
    
}

function getInvoiceConfigStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('invoice_config_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.invoiceConfigDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 



function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;






