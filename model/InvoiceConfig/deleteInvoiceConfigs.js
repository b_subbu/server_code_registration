function processRequest(req,res,data){
    if(typeof req.body.invoiceConfigIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkInvoiceConfigStatus(req,res,9,data);
}

function checkInvoiceConfigStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var InvoiceConfigArr = req.body.invoiceConfigIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.invoiceConfigIds = InvoiceConfigArr;
    
    var orgId = data.orgId;
    
    var enteredItems = [];
    var authorisedItems = [];
   
    var tabl_name = orgId+'_invoice_config'; //TODO: move to a common definition file

    async.forEachOf(InvoiceConfigArr,function(key,value,callback1){
        var InvoiceConfigData= {};
            InvoiceConfigData.invoiceConfigId = key;
        datautils.checkInvoiceConfigCode(tabl_name,InvoiceConfigData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
              else{
                  if (result.status == 80){
                   debuglog("This is entered status item");
                   debuglog("So can be deleted directly");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 82){
                   debuglog("This is authorised status item");
                   debuglog("So has to go through authoristation to delete");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Delete is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();          
             }
           });       
        }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
              else{
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
              deleteInvoiceConfig(req,res,9,data,enteredItems,authorisedItems);
             }
          }
      });
}

function deleteInvoiceConfig(req,res,stat,data,enteredItems,authorisedItems){
    
    
    debuglog("Now start deleting the entries in Revision Table");
    debuglog("Otherwise we will have 2 rows in main table with same id but different status");
     debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_invoice_config'; //TODO: move to a common definition file
    var mainTable    = orgId+'_invoice_config';
    
    async.forEachOf(enteredItems,function(key,value,callback1){
     invoiceConfigId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteInvoiceConfigCode(mainTable,invoiceConfigId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
               processStatus(req,res,6,data);
            }
        else{
          if(authorisedItems.length > 0){
          debuglog("This request has some authorised items too");
          debuglog(authorisedItems);
          deleteAuthoriseInvoiceConfigs(req,res,9,data,authorisedItems,mainTable,revisionTable);
          }
          else{
          debuglog("This request has no authorised items");
          debuglog("so call return");
          processStatus(req,res,9,data); 
         }
		    }         
     });
}

function deleteAuthoriseInvoiceConfigs(req,res,stat,data,authorisedItems,mainTable,revisionTable){
    var uid = req.body.userId;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      invoiceConfigId = key;
     async.parallel({
          relStat: function (callback){datautils.copyInvoiceConfigHeader(mainTable,revisionTable,invoiceConfigId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           debuglog("Change status of items in revision table to ready2Delete");
           changeInvoiceConfigStatus(req,res,9,data,authorisedItems,mainTable,revisionTable);
          }
      });
}


function changeInvoiceConfigStatus(req,res,stat,data,ConfigArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(ConfigArr,function(key,value,callback1){
    invoiceConfigId = key;
    var reason = 'Delete Invoice Config submitted';
       statutils.changeInvoiceConfigStatus(revTable,data.orgId,data.businessType,data.userId,invoiceConfigId,89,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getInvoiceConfigDetails(req,res,9,data,revTable);
             }
          });
     
}


function getInvoiceConfigDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog("Status may be available only for Authorised items");
    debuglog("For entered items it will be entirely deleted from system");
    debuglog(data);
     
   datautils.InvoiceConfigsData(data.orgId,revTable,data.invoiceConfigIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getInvoiceConfigStatusName(req,res,9,data,result); 
         }
     });
    
}

function getInvoiceConfigStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('invoice_config_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.chargeData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;





