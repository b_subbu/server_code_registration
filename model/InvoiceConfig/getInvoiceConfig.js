function processRequest(req,res,data){
   if(typeof req.body.invoiceConfigId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
      getInvoiceConfigData(req,res,9,data);
}


function getInvoiceConfigData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.invoiceConfigId = req.body.invoiceConfigId;
    var tid = req.body.tabId;
    
    if(tid == 501)
     var tabl_name = data.orgId+'_invoice_config';
    else
     var tabl_name = data.orgId+'_Revision_invoice_config';
   
     var extend = require('util')._extend;
     var InvoiceConfigData = extend({},data);
     
  
      datautils.InvoiceConfigData(tabl_name,InvoiceConfigData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              getMasterData(req,res,3,data);
              isError = 1;
              //return;
              debuglog("result errorCode is 1");
              }
            else {
                var invoiceConfigDetails = {};
                var buyerDetails = {};
                var orderDetails = {};
                data.sellerId = result.sellerId;
                data.invoiceConfigName = result.invoiceConfigName;
                
                invoiceConfigDetails.cin = result.cin;
                invoiceConfigDetails.tin = result.tin;
                invoiceConfigDetails.cst = result.cst;
                if(result.chargeIds != null)
                  invoiceConfigDetails.chargeIds = result.chargeIds;
                else
                invoiceConfigDetails.chargeIds = [];
                invoiceConfigDetails.disclaimer = result.disclaimer;
                invoiceConfigDetails.sellerName = result.sellerName;
                invoiceConfigDetails.sellerAddress = result.sellerAddress;
                
                buyerDetails.buyerName = result.buyerName;
                buyerDetails.billingAddress = result.billingAddress;
                buyerDetails.shippingAddress = result.shippingAddress;
                
                orderDetails.itemCode = result.itemCode;
                orderDetails.itemDetailCode = result.itemDetailCode;
                orderDetails.price = result.price;
                orderDetails.quantity = result.quantity;
                orderDetails.totals = result.totals;
                
                data.invoiceConfigDetails = invoiceConfigDetails;
                data.buyerDetails = buyerDetails;
                data.orderDetails = orderDetails;
                
                data.status = result.status;
                data.statusId = result.statusId;
                getSellerDetails(req,res,9,data,invoiceConfigDetails);
                debuglog("No error case");
                debuglog(invoiceConfigDetails);
                debuglog(data);
            }
          }
        });
} 

function getSellerDetails(req,res,stat,data,invoiceConfigDetails){

var tblName = data.orgId+'_charge_type';
var chargeUtils = require('../Charges/dataUtils');

var chargeIds = invoiceConfigDetails.chargeIds
debuglog("Start getting charges data");
debuglog(chargeIds);

chargeUtils.ChargesData(tblName,chargeIds,function(err,result){
  debuglog("After getting charges data");
  debuglog(result);
    if(err)
      processStatus(req,res,6,data);
    else{
     data.chargeDetails = result;
     debuglog(data);
     //getInvoiceConfigStatName(req,res,stat,data)
     getChargeStatusName(req,res,stat,data);
     }
  });
}

function getInvoiceConfigStatName(req,res,stat,data) {
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
        var statID=data.status;
        debuglog(statID);
        mastutils.masterStatusNames('invoice_config_type_status_master',statID,function(err,result){
           debuglog("After getting result from status");
           debuglog(result);
           if(err) processStatus(req,res,6,data);
         else {
                data.status = result.statusName;
                debuglog("after getting status");
                debuglog(data);
                getChargeStatusName(req,res,9,data);
              }
           }); 
} 

function getChargeStatusName(req,res,stat,data) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(data.chargeDetails,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames('charge_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting Charge status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
              getMasterData(req,res,9,data);
           }
      });
 

} 


function getMasterData(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');
 var async = require('async');
 
 var sellerTable = data.orgId+"_sellers";
 var chargeTable = data.orgId+"_charge_type";
 
  async.parallel({
          sellersData: function(callback) {datautils.sellerList(sellerTable,callback); }
        },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
             else {
              data.sellers = results.sellersData;
              processStatus(req,res,9,data);
             }
          });  
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;











