function changeInvoiceConfigStatus(tablName,orgId,bizType,operatorId,invoiceConfigId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {InvoiceConfigId: invoiceConfigId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createInvoiceConfigStatusChngLog(orgId,bizType,operatorId,invoiceConfigId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createInvoiceConfigStatusChngLog(orgId,bizType,operatorId,invoiceConfigId,stat,reason,callback){
 
  var promise = db.get('invoice_config_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    invoiceConfigId: invoiceConfigId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }


 exports.changeInvoiceConfigStatus = changeInvoiceConfigStatus;

