function processRequest(req,res,data){
    if(typeof req.body.invoiceConfigIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkInvoiceConfigStatus(req,res,9,data);
}


function checkInvoiceConfigStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var InvoiceConfigArr = req.body.invoiceConfigIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.invoiceConfigIds = InvoiceConfigArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_invoice_config'; //TODO: move to a common definition file

    async.forEachOf(InvoiceConfigArr,function(key,value,callback1){
        var InvoiceConfigData= {};
            InvoiceConfigData.invoiceConfigId = key;
        datautils.checkInvoiceConfigCode(tabl_name,InvoiceConfigData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });       
        }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToRevision(req,res,9,data);
          }
      });
}

function moveToRevision(req,res,stat,data){
    
    var InvoiceConfigArr = data.invoiceConfigIds;
    
    debuglog("Now start moving the entries to Revision Table");
    debuglog(InvoiceConfigArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_invoice_config'; //TODO: move to a common definition file
    var mainTable    = orgId+'_invoice_config';
   
    async.forEachOf(InvoiceConfigArr,function(key,value,callback1){
      invoiceConfigId = key;
     async.parallel({
          relStat: function (callback){datautils.moveInvoiceConfigHeader(mainTable,revisionTable,invoiceConfigId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeInvoiceConfigStatus(req,res,9,data,InvoiceConfigArr,mainTable,revisionTable); 
         }
     });
}

function copyToRevision(req,res,stat,data,InvoiceConfigData){
    
    
    debuglog("Even though this called Copy we are actually creating a new entry");
    debuglog(InvoiceConfigData);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_invoice_config'; //We will create the new entry in Revision Table
   
     if(   InvoiceConfigData.sellerId == null
       || InvoiceConfigData.sellerId == 'undefined'
       || InvoiceConfigData.invoiceConfigName == null
       || InvoiceConfigData.invoiceConfigName == 'undefined'
       || InvoiceConfigData.invoiceConfigDetails == null
       || InvoiceConfigData.invoiceConfigDetails == 'undefined'
      )
        processStatus(req,res,4,data);
  else{
       
         datautils.InvoiceConfigUpdate('',InvoiceConfigData,revisionTable,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(InvoiceConfigData);
                var InvoiceConfigArr = [];
                InvoiceConfigArr.push(InvoiceConfigData.invoiceConfigId);
                changeInvoiceConfigStatus(req,res,9,data,InvoiceConfigArr,'',revisionTable);
                }
        });
      }
}


function changeInvoiceConfigStatus(req,res,stat,data,InvoiceConfigArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(InvoiceConfigArr,function(key,value,callback1){
    invoiceConfigId = key;
    var reason = '';
       statutils.changeInvoiceConfigStatus(revTable,data.orgId,data.businessType,data.userId,invoiceConfigId,81,'Invoice Config data Submitted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.invoiceConfigIds = InvoiceConfigArr;
                 getInvoiceConfigDetails(req,res,9,data,revTable);
             }
          });
     
}


function getInvoiceConfigDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.InvoiceConfigsData(data.orgId,revTable,data.invoiceConfigIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getInvoiceConfigStatusName(req,res,9,data,result); 
         }
     });
    
}

function getInvoiceConfigStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('invoice_config_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.invoiceConfigDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
exports.moveToRevision=moveToRevision; 
exports.copyToRevision=copyToRevision;




