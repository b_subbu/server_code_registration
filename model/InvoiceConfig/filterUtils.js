function getFilters(key_name,statList,pid,tid,oid,statTblName,callback) {
  
  switch(key_name){
     case 'status':
     var tblName = 'role_status_master';
       getStatusFilter(statList,tblName,callback);  
        break;
     case 'seller':
      var tblName = oid+"_sellers";
       getSellerFilter(tblName,callback);
       break;
     default:
       callback(null,null);
       break;
  }
  
}

//for status no need to again query db since statList is already passed
//and it varies from tab to tab for other filters we can just query master
//and return

function getStatusFilter(mastData,tblName,callback) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var data = {};
     
      async.forEachOf(mastData,function(key1,value1,callback1){
        var statID=key1;
        getItemStatName(statID,tblName,function(err,result) {
           if(err){ isError =1; callback1();}
         else {
                 var tmpArray = {id:statID, name:result};
                 itemMasters.push(tmpArray);
                  callback1(); 
              }
           }); 
          },
         function(err){
       if(err){
            callback(6,null);      }
      else {
            callback(null,itemMasters);
	         }
      });
 

}

function getItemStatName(statID,tblName,callback){
   var totalRows =0;
   var statName = '';
    var mastutils = require('./../Registration/masterUtils');

  
      mastutils.masterStatusNames(tblName,statID,function(err,result) {
       
         if(err) callback(6,null);
         else {
                statName = result.statusName;
                callback(null,statName); 
                debuglog("after getting status");
                debuglog(result);
              }
          });
       
 } 

 
function getSellerFilter(tblName,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   
  var promise = db.get(tblName).find({$and:[{enabled:true},{status_id:82}]});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {id: doc.sellerId,name:doc.bus_name};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       debuglog(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}     

exports.filterOptions = getFilters;
 




