function processRequest(req,res,data){
        getInvoiceConfigCode(req,res,9,data);
}


function getInvoiceConfigCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var InvoiceConfigData = {};
       InvoiceConfigData.invoiceConfigDetails =  req.body.invoiceConfigDetails;
       InvoiceConfigData.buyerDetails = req.body.buyerDetails;
       InvoiceConfigData.orderDetails = req.body.orderDetails;
       InvoiceConfigData.status_id = 80;
       InvoiceConfigData.sellerId = req.body.sellerId;
       InvoiceConfigData.invoiceConfigName = req.body.invoiceConfigName;
       InvoiceConfigData.orgId = data.orgId;
       InvoiceConfigData.businessType = data.businessType;
      
     var tblName = data.orgId+'_invoice_config'; //create will always be in main table
     debuglog(InvoiceConfigData);
     debuglog(req.body);
      datautils.generateInvoiceConfigCode(InvoiceConfigData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
               if(err) 
                 processStatus(req,res,result.errorCode,data);
               else  {
                debuglog("The newly generated code is:");
                debuglog(result);
                InvoiceConfigData.invoiceConfigId = result
                data.invoiceConfigId = result;
                checkSellerCode(req,res,9,data,InvoiceConfigData,tblName);
                }
            }
        });
} 

function checkSellerCode(req,res,stat,data,InvoiceConfigData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     debuglog("Check if the  seller Id exists or not");
     debuglog(InvoiceConfigData);
     debuglog(req.body);
      datautils.checkSellerCode(InvoiceConfigData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 if(result.errorCode == 1)
                   processStatus(req,res,7,data);
                 else
                    saveInvoiceConfigData(req,res,9,data,InvoiceConfigData,tblName);
             }
        });
}
function saveInvoiceConfigData(req,res,stat,data,InvoiceConfigData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
     debuglog("This is after getting data from request");
     debuglog(InvoiceConfigData);
     debuglog(data);
     
     
    var submitop = req.body.submitOperation;
  
   
    if(   InvoiceConfigData.sellerId == null
       || InvoiceConfigData.sellerId == 'undefined'
       || InvoiceConfigData.invoiceConfigName == null
       || InvoiceConfigData.invoiceConfigName == 'undefined'
       || InvoiceConfigData.invoiceConfigDetails == null
       || InvoiceConfigData.invoiceConfigDetails == 'undefined'
      )
        processStatus(req,res,4,data);
    else{
    
     datautils.InvoiceConfigUpdate('',InvoiceConfigData,tblName,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(InvoiceConfigData);
               if(submitop == 1){
                var invoiceConfigIds = [];
                    invoiceConfigIds.push(data.invoiceConfigId);
                    data.invoiceConfigIds = invoiceConfigIds;
               var submitInvoiceConfig = require('./submitInvoiceConfigs');
               submitInvoiceConfig.moveToRevision(req,res,9,data);
             }
             else{
                changeInvoiceConfigStatus(req,res,9,data,InvoiceConfigData,tblName);
                }
            }
          });
        }
} 


function changeInvoiceConfigStatus(req,res,stat,data,InvoiceConfigData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeInvoiceConfigStatus(tblName,data.orgId,data.businessType,data.userId,data.invoiceConfigId,80,'Invoice Config Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getInvoiceConfigData(req,res,9,data,InvoiceConfigData,tblName);
               }
        });
} 

function getInvoiceConfigData(req,res,stat,data,InvoiceConfigData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
    
    var invoiceConfigArr = [];
    invoiceConfigArr.push(data.invoiceConfigId) 
   datautils.InvoiceConfigsData(data.orgId,tblName,invoiceConfigArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getInvoiceConfigStatusName(req,res,9,data,result); 
         }
     });
    
}

function getInvoiceConfigStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('invoice_config_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.invoiceConfigDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
exports.getData = processRequest;












