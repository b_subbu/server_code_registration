function getInvoiceConfigCount(tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getInvoiceConfigHeader(orgId,tblName,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment=require('moment');
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
     
     var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {invoiceConfigId: doc.InvoiceConfigId,
                        invoiceConfigName:doc.InvoiceConfigName,
                        status:doc.status_id,
                        statusId:doc.status_id,
                        seller: doc.sellerId,
                        date: moment(doc.update_time).format('L')
    
                      };
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 3; //no data case
       data.invoiceConfigs = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         //data.invoiceConfigs = resultArray;
         getSellerDetailsMain(orgId,resultArray,data,callback);
         //callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getInvoiceConfigMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  
  var promise = db.get(tblName).find({},{sort:{InvoiceConfig_type_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.id = doc.InvoiceConfig_type_id;
        tmpArr.name = doc.InvoiceConfig_type_name;
        tmpArr.label = doc.InvoiceConfig_label;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


 function getInvoiceConfigData(tblName,InvoiceConfigData,callback) {

  var totalRows = 0;
  var data = {};
  var result = {};
  debuglog(InvoiceConfigData);
  var promise = db.get(tblName).find({InvoiceConfigId:InvoiceConfigData.invoiceConfigId});
  promise.each(function(doc){
    totalRows++;
    result.invoiceConfigName = doc.InvoiceConfigName;
    result.sellerId = doc.sellerId;
    result.status = doc.status_id;
    result.statusId = doc.status_id;
    result.cin = doc.cin;
    result.tin = doc.tin;
    result.cst = doc.cst;
    result.chargeIds = doc.chargeIds;
    result.disclaimer = doc.disclaimer;
    result.sellerName = doc.sellerName;
    result.sellerAddress = doc.sellerAddress;
    result.itemCode = doc.itemCode;
    result.itemDetailCode = doc.itemDetailCode;
    result.quantity = doc.quantity;
	  result.price = doc.price;
    result.totals = doc.totals;
    result.buyerName = doc.buyerName;
    result.billingAddress = doc.billingAddress;
    result.shippingAddress = doc.shippingAddress;
    });
  promise.on('complete',function(err,doc){
  debuglog(totalRows);
  debuglog(doc);
  debuglog(result);
    if(totalRows == 0)  {
      result.errorCode = 1; 
      callback(null,result);
    }
    else if(totalRows > 1)
      callback(6,null); //system error should be 1 row only
    else {
      result.errorCode = 0;
      callback(null,result);
      }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(null,result);//system error
  });
}

function checkInvoiceConfigCode(tblName,InvoiceConfigData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("We are checking only in Main table");
debuglog("Any row directly submitted is not problem");
debuglog("since it will just overwrite main row when authorised");
debuglog(InvoiceConfigData);

var promise = db.get(tblName).find({InvoiceConfigId: InvoiceConfigData.invoiceConfigId});
promise.each(function(doc){
   status = doc.status_id;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 5;
   data.status = status;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}


function getInvoiceConfigsData(orgId,tblName,InvoiceConfigIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment=require('moment');
  
   var promise = db.get(tblName).find({InvoiceConfigId: {$in: InvoiceConfigIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {
                        invoiceConfigId: doc.InvoiceConfigId,
                        invoiceConfigName:doc.InvoiceConfigName,
                        status:doc.status_id,
                        statusId:doc.status_id,
                        seller: doc.sellerId,
                        date: moment(doc.update_time).format('L')
           };
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         //callback(null,resultArray);
         getSellerDetails(orgId,resultArray,callback);

     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function moveInvoiceConfigHeader(srcTable,destTable,InvoiceConfigId,callback){

var promise1 = db.get(destTable).remove({InvoiceConfigId:InvoiceConfigId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({InvoiceConfigId:InvoiceConfigId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({InvoiceConfigId:InvoiceConfigId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function copyInvoiceConfigHeader(srcTable,destTable,InvoiceConfigId,callback){

var promise1 = db.get(destTable).remove({InvoiceConfigId:InvoiceConfigId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({InvoiceConfigId:InvoiceConfigId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
           callback(null,null);
        });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function deleteInvoiceConfigRow(tblName,InvoiceConfigId,callback){
  
var promise = db.get(tblName).remove({InvoiceConfigId:InvoiceConfigId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getInvoicePicture(uid,userDets,tblName,callback){
debuglog("This fn added to get enabled fields for the given business type");
debuglog("As of now it is only DB driven, this may be needed to be included in UI later");

  getInvoicePictureClause(userDets.businessType,function(err,result){
    debuglog("If no row found for the given business type it will return as error");
    debuglog("This is not expect to happen since it is a system table")
     if(err){
         debuglog("Errored out");
        callback(6,null);
        }
    else{
          debuglog("No error, the returned value is:");
          debuglog(result);
          userDets.invoicePicture = result;
          updateInvoiceConfigDetail(uid,userDets,tblName,callback);
        }
    });
}

function updateInvoiceConfigDetail(uid,userDets,tblName,callback){
 
 var promise = db.get(tblName).update({InvoiceConfigId:userDets.invoiceConfigId},
  { $set:{
    InvoiceConfigId: userDets.invoiceConfigId,
    InvoiceConfigName: userDets.invoiceConfigName,
    sellerId: userDets.sellerId,
    cin: userDets.invoiceConfigDetails.cin,
    tin: userDets.invoiceConfigDetails.tin,
    cst: userDets.invoiceConfigDetails.cst,
    sellerName: userDets.invoiceConfigDetails.sellerName,
    sellerAddress: userDets.invoiceConfigDetails.sellerAddress,
    chargeIds: userDets.invoiceConfigDetails.chargeIds,
    disclaimer: userDets.invoiceConfigDetails.disclaimer,
    buyerName: userDets.buyerDetails.buyerName,
    billingAddress: userDets.buyerDetails.billingAddress,
    shippingAddress: userDets.buyerDetails.shippingAddress,
    itemCode: userDets.orderDetails.itemCode,
    itemDetailCode: userDets.orderDetails.itemDetailCode,
    price: userDets.orderDetails.price,
    quantity: userDets.orderDetails.quantity,
    totals: userDets.orderDetails.totals,
    invoicePicture: userDets.invoicePicture
    }},
  {upsert: true}
);
  promise.on('success',function(doc){
   callback(null,null)
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function checkInvoiceConfigCodeMain(InvoiceConfigData,callback){
 var tblName = InvoiceConfigData.orgId+'_invoice_config';
 var revisiontblName = InvoiceConfigData.orgId+'_Revision_invoice_config';
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
       debuglog("Check Count in Revision also because it is possible");
       debuglog("user might have directly submitted in Create");
       checkInvoiceConfigCodeRevision(doc,revisiontblName,callback);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkInvoiceConfigCodeRevision(activeCount,tblName,callback){
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
      debuglog("Count from revision table and active table are");
      debuglog(doc);
      debuglog(activeCount); 
      var totalCount = doc+activeCount;
       debuglog("So the sum of 2 is");
       debuglog("This is the total of InvoiceConfigs created by this business");
       debuglog("Since release & edit is not possible also delete is not possible");
       debuglog("can take next Id to be incr of sum total");
       debuglog("Otherwise change this logic");
       debuglog("Note there will be always one row in InvoiceConfig with Id as 1 this is the main business");
       debuglog(totalCount);
       totalCount++;
       debuglog("Will increment by 1");
       debuglog(totalCount);
       pgId = totalCount;
       debuglog("And the new Id is");
       debuglog(pgId);
       callback(null,pgId);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}


function getSellersData(tblName,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   debuglog("Note only Authorised sellers will be getting in this call")
   var promise = db.get(tblName).find({status_id: 82},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {
                        sellerId: doc.sellerId,
                        sellerName:doc.bus_name,
                        status:'Authorised'
        };
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function getChargesData(tblName,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   debuglog("Note only Authorised Charges will be getting in this call")
   var promise = db.get(tblName).find({status_id: 82},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {
                        chargeId: doc.chargeId,
                        chargeName:doc.chargeName,
                        chargeType:doc.chargeType,
                        sellerId: doc.sellerId,
                        status:'Authorised'
        };
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function checkSellerCodeMain(ChargeData,callback){
var totalRows = 0;
var data = {};
var status = 0;

 var tblName = ChargeData.orgId+'_invoice_config';
 var revisiontblName = ChargeData.orgId+'_Revision_invoice_config';

debuglog("First Check in  Main table");
debuglog(ChargeData);

var promise = db.get(tblName).find({sellerId: ChargeData.sellerId});
promise.each(function(doc){
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 1;
   callback(null,data);
   }
  else{
    
    checkSellerCodeRevision(revisiontblName,ChargeData,callback);
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function checkSellerCodeRevision(revTable,ChargeData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("Then Check in  Revision table");
debuglog(ChargeData);

var promise = db.get(revTable).find({sellerId: ChargeData.sellerId});
promise.each(function(doc){
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 1;
   callback(null,data);
   }
  else{
   data.errorCode = 0;
   callback(null,data);
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}


function getInvoicePictureClause(businessType,callback){
var totalRows = 0;
var pictureClause = [];

var promise = db.get('invoice_picture_master').find({business_type: businessType});
promise.each(function(doc){
   totalRows++;
   pictureClause.push(doc.picture_clause);
});

promise.on('complete',function(err,doc){
 if(totalRows != 1){
   callback(6,null);
   }
  else{
   callback(null,pictureClause);
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function getSellerDetailsMain(orgId,invoiceConfigs,data,callback){
  debuglog("TODO combine this and below fn errorCode in callback is different");

   var async= require('async');
      var tblName = orgId+"_sellers";
      async.forEachOf(invoiceConfigs,function(key1,value1,callback1){
        var sellerID=key1.seller;
        debuglog("get name for sellerId");
        debuglog(sellerID);
           getSellerName(tblName,sellerID,function(err,result) {
           debuglog("Result of seller name");
           debuglog(result);
           if(err) callback1();
         else {
                key1.seller = result.sellerName;
                callback1(); 
                debuglog("after getting seller");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              callback(6,data);
            }
      else {
            data.invoiceConfigs = invoiceConfigs;
            callback(null,data);
           }
      });
 
}



function getSellerDetails(orgId,invoiceConfigs,callback){
   var async= require('async');
      var tblName = orgId+"_sellers";
      async.forEachOf(invoiceConfigs,function(key1,value1,callback1){
        var sellerID=key1.seller;
        debuglog("get name for sellerId");
        debuglog(sellerID);
           getSellerName(tblName,sellerID,function(err,result) {
           debuglog("Result of seller name");
           debuglog(result);
           if(err) callback1();
         else {
                key1.seller = result.sellerName;
                callback1(); 
                debuglog("after getting seller");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              callback(6,data);
            }
      else {
            callback(null,invoiceConfigs);
           }
      });
 
}


function getSellerName(tblName,sellerId,callback){

   var totalRows =0;
   var data = {};
       debuglog("start getting seller name");
       debuglog(tblName);
       debuglog(sellerId);
  
   var promise = db.get(tblName).find({$and:[{sellerId: sellerId},{enabled:true},{status_id:82}]});
   promise.each(function(doc){
        totalRows++;
        data = {
                        sellerId: doc.sellerId,
                        sellerName:doc.bus_name
          };
       }); 
   promise.on('complete',function(err,doc){
        debuglog(totalRows);
        debuglog(data);
        if(totalRows != 1)
         callback(6,null);
        else
         callback(null,data);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

exports.InvoiceConfigCount = getInvoiceConfigCount;
exports.InvoiceConfigMaster = getInvoiceConfigMaster;
exports.InvoiceConfigData = getInvoiceConfigData;
exports.InvoiceConfigsData = getInvoiceConfigsData;
exports.moveInvoiceConfigHeader = moveInvoiceConfigHeader;
exports.checkInvoiceConfigCode = checkInvoiceConfigCode;
exports.InvoiceConfigHeader = getInvoiceConfigHeader;
exports.deleteInvoiceConfigCode = deleteInvoiceConfigRow;
exports.generateInvoiceConfigCode = checkInvoiceConfigCodeMain;
exports.InvoiceConfigUpdate = getInvoicePicture;
exports.sellerList = getSellersData;
exports.chargeList = getChargesData;
exports.checkSellerCode = checkSellerCodeMain;
exports.copyInvoiceConfigHeader = copyInvoiceConfigHeader;







