function getGroupsData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
  
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'groups_status_master',callback); }
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList);
                  }
                  else 
                   getGroupsCount(req,res,stat,data,statList);
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList) {
   var async = require('async');
   var filterutils = require('../Menu/filterUtils');
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    if(tid == 501)
     var tabl_name = oid+'_group_header';
    else
     var tabl_name = oid+'_Revision_group_header';
     
 
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,'groups_status_master',function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
               filterOptions[key_name] = results;
               callback();
            }
          }); 
        }
     else
        callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call alerts count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var userArray = {org_id: data.orgId };
           searchArr.push(userArray);
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
          getGroupsCount(req,res,stat,data,searchArr,tabl_name);
           }
         else 
         getGroupsSearch(req,res,stat,data,statList,tabl_name);
         }
      });
} 


function getGroupsSearch(req,res,stat,data,statList,tabl_name) {

 var searchInp = req.body.searchParams;

    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    if(tid == 501)
     var tabl_name = oid+'_group_header';
    else
     var tabl_name = oid+'_Revision_group_header';
   
 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
  debuglog("item count search is implemented in much simpler way here");
  debuglog("if this does not work look in searchUtils for full solution")
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
       case 'groupCode':
        var tmpArray =  {groupCode: {$regex:key,$options:'i'}}; ; 
        searchArr.push(tmpArray);
         break; 
       case 'groupName':
        var tmpArray =  {groupName: {$regex:key,$options:'i'}}; ; 
        searchArr.push(tmpArray);
        break; 
      case 'itemCount':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       var tmpArray2 = [];
       for(idx = minCount; idx <= maxCount; idx++){
         var tmpArray3 = {itemCodes: {$size:idx}};
         tmpArray2.push(tmpArray3)
        }
       var tmpArray1 = {$or: tmpArray2};
       debuglog("There is no range based search or CTE in mongodb");
       debuglog("So search one by one size this is not efficient way");
       debuglog("If this becomes too slow revert to fn based search in searchUtils") ;
       debuglog(tmpArray1);
       searchArr.push(tmpArray1);
       break;
    
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           
            getGroupsCount(req,res,9,data,searchArr,tabl_name);
            debuglog("We will start getting data with search Params");
            debuglog(searchArr);
          }
     });
}

function getGroupsCount(req,res,stat,data,statList,tablName){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
    //TODO probably have only tabl_name no need for orgId or bizType
   //since table name itself will have them      
   utils.groupCount(orgId,bizType,tablName,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data);
              //no Groups for the business nothing to do further
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getGroupHeaderData(req,res,9,data,statList,tablName); 
             }
            }
         });
}


function getGroupHeaderData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var utils = require('./dataUtils');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
   
      utils.groupsHeader(headerTable,orgId,bizType,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data); //no Groups for the business nothing to do further
              isError = 1;
             //will be handled at count itself, just adding here
            }
            else {
            groups = result.groups;
            debuglog(groups); 
            getGroupStatusName(req,res,9,data,groups);
            }
          }
        });
} 


function getGroupStatusName(req,res,stat,data,groups) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(groups,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames('groups_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            //data.productData = groups;
            getItemVisual(req,res,stat,data,groups);
            //processStatus(req,res,9,data);
           }
      });
 

} 

function getItemVisual(req,res,stat,data,groups) {
 var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(groups,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.firstItem,tablName,function(err,results){
     if(err){
       isError = 1;
       callback();
     }
     else {
       if(results.masterData[0] != null){
       
        debuglog("This is the item data coming for item");
        debuglog(key.firstItem);
        debuglog("Even though it has many more components in result we will use only visuals");
        debuglog("There is no checking of InUse status here");
        debuglog(results.masterData[0]);
        debuglog("Above is the data for item array");
        if(results.masterData[0]['itemVisual'] != null){
         debuglog("This is the itemVisual array");
         debuglog(results.masterData[0]['itemVisual']);
          if(results.masterData[0]['itemVisual'][0].type == 62)
             results.masterData[0]['itemVisual'][0].src = global.nodeURL+results.masterData[0]['itemVisual'][0].src;
          key.visual = results.masterData[0]['itemVisual'][0];
           }
        else
            key.visual = null;
       debuglog(key);
       debuglog(results.masterData[0]);    
       callback();
       }
       else
        callback();
    }
  });
  },
   function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else{
             data.productData =  groups;
             processStatus(req,res,9,data);
             debuglog("Item visuals obtained");
             debuglog(groups);
          }
        }
     });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

   
exports.getProductData = getGroupsData;





