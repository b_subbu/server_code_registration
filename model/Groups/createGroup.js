function processRequest(req,res,data){
   if( typeof req.body.userId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getGroupCode(req,res,9,data);
}

function getGroupCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     var extend = require('util')._extend;
     var groupData = extend({},data);
     
  
      datautils.generateGroupCode(groupData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                data.groupCode = result;
                saveGroupData(req,res,9,data,groupData);
            }
        });
} 

function saveGroupData(req,res,stat,data,groupData) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
 var itemsArr = [];
 
     groupData.groupName = req.body.menuGroupDetails.groupName;
     groupData.groupDescription = req.body.menuGroupDetails.groupDescription;
     //groupData.itemsData = req.body.itemsData;
     
     
  async.forEachOf(req.body.itemsData,function(key1,value1,callback1){

     var tmpArr = {itemCode:key1.itemCode};
     debuglog(itemsArr);
     itemsArr.push(tmpArr);
     callback1(); 

  },
  function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
           groupData.itemsData = itemsArr;
           updateGroupData(req,res,9,data,groupData);
           debuglog("No error in populating the items array now we will update table");
           debuglog(groupData);
           }
     });
 

}  

function updateGroupData(req,res,stat,data,groupData) {
   var async = require('async');
   var moment = require('moment');
   var datautils = require('./dataUtils');
 
   var tblName = data.orgId+'_group_header';
   
     
     datautils.groupCreate(tblName,data.groupCode,groupData,function(err,result) {
      
          if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saving group data");
                debuglog(groupData);
                changeGroupStatus(req,res,9,data,groupData,tblName);
            }
        });
} 


function changeGroupStatus(req,res,stat,data,groupData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,data.groupCode,80,'Group Created',function(err,result) {
         if(err) {
                   processStatus(req,res,6,data);
                 }
          else {
                 getGroupData(req,res,9,data,groupData,tblName);
               }
        });
} 

function getGroupData(req,res,stat,data,groupData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
      var groupArr = [];
      groupArr.push(data.groupCode);
      datautils.groupsData(tblName,data.orgId,groupArr,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
              groups = result.groups;
              getGroupStatusName(req,res,9,data,groups);
              }
        }); 
} 

function getGroupStatusName(req,res,stat,data,groups) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(groups,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('groups_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.groupData = groups;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;







