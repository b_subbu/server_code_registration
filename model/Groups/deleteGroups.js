function processRequest(req,res,data){
   if(typeof req.body.groupCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkGroupCodes(req,res,9,data);
}

function checkGroupCodes(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     var extend = require('util')._extend;
     var groupData = extend({},data);
    
     var groupCodes = req.body.groupCodes;
     data.groupCodes = groupCodes;
     
     var tblName = data.orgId+'_group_header'; //Delete is possible only for Active status
      
    async.forEachOf(groupCodes,function(key1,value1,callback1){
      groupData.groupCode = key1;

      datautils.checkGroupCode(tblName,groupData,function(err,result) {
         if(err) {
               isError = 1;
               //processStatus(req,res,6,data);
               callback1();
             }
            else {
                if(result.errorCode == 1){
                 //processStatus(req,res,3,data);
                 isError = 2;
                 debuglog("No rows for the given group code");
                 debuglog("Hence error");
                 debuglog(groupData);
                 callback1();
                 }
                else{
                 debuglog("Row found for the given group code");
                 debuglog("in its corresponding table");
                 debuglog("so no error");
                 debuglog(groupData);
                 callback1();
              }
            }
        });
        },
    function(err){
      if(err)
           processStatus(req,res,6,data);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else if(isError == 2)
             processStatus(req,res,3,data);
           else{
           changeGroupStatus(req,res,9,data,groupData,groupCodes,tblName);
           debuglog("No error now we will delete data");
           debuglog("Will log status change first before deleting data");
           debuglog(groupData);
           }
           }
     });
 
} 


function changeGroupStatus(req,res,stat,data,groupData,groupCodes,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
   
  async.forEachOf(groupCodes,function(key1,value1,callback1){
       var groupCode = key1;

     statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,groupCode,81,'Group Deleted',function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
             else{
                 debuglog("Status change logged");
                 callback1();
              }
          });
        },
    function(err){
      if(err)
           processStatus(req,res,6,data);
      else {
           deleteGroupData(req,res,9,data,groupData,groupCodes,tblName);
           debuglog("No error now we will delete data");
           debuglog(groupData);
           }
     });
} 

function deleteGroupData(req,res,stat,data,groupData,groupCodes,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
  datautils.deleteGroups(tblName,groupData,groupCodes,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 debuglog("Delete successful");
                 processStatus(req,res,9,data);
              }
      });
     
}  
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;

