function getGroupsCount(orgId,bizType,tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}


//TODO see if we can combine these in common dataUtils by getting
//column names from productHeader
function getGroupsHeader(tblName,orgId,bizType,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
   //var promise = db.get(tblName).find({$and:[{org_id:orgId},{status_id: {$in: slit}}]},{sort: {update_time: -1}, limit: size, skip: skip});
    var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {groupCode: doc.groupCode,
                        groupName:doc.groupName,
                        status:doc.status_id};
        var items = doc.itemCodes;                
        
        tmpArray.itemCount = items.length;
        if(items.length > 0)
         tmpArray.firstItem = items[0].itemCode;
        else
         tmpArray.firstItem = '';
         
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.groups = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.groups = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getGroupHeader(tblName,groupData,callback){
   var totalRows =0;
   var data = {};
  
  var promise = db.get(tblName).find({groupCode:groupData.groupCode});
   promise.each(function(doc){
        totalRows++;
        data.groupName = doc.groupName;
        data.groupDescription = doc.groupDescription;
        data.status = doc.status_id;
        data.itemCodes = doc.itemCodes;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function generateGroupCode(groupData,callback){
  var currseqnum;
  var next_seq_str;
  var totalRows = 0;

  var promise = db.get('group_sequence').find({orgId:groupData.orgId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    currseqnum = doc.running_sequence;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
      db.get('group_sequence').insert(
      { orgId: groupData.orgId,running_sequence: '0001'});
      groupCode =groupData.orgId+'0001';
      callback(null,groupCode);
    }
    else {
      var next_sequence = parseInt(currseqnum);
      next_sequence++;
      next_seq_str = String("0000000"+next_sequence).slice(-4);
      var promise = db.get('group_sequence').update({ orgId:groupData.orgId},
      {  $set: { running_sequence: next_seq_str }});
      groupCode = groupData.orgId+next_seq_str;
      callback(null,groupCode);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}

//We are using update for both create and Edit 
function updateGroupHeader(tblName,groupCode,groupData,callback){

var promise = db.get(tblName).update(
      {groupCode:groupCode},
        {$set: {business_id: groupData.userId,
          org_id: groupData.orgId,
          groupName: groupData.groupName,
          createDate: new Date,
          status_id: 80,
          groupDescription: groupData.groupDescription,
          itemCodes: groupData.itemsData,
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
}    


function getGroupsData(tblName,orgId,groupCodes,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment = require('moment');
   var promise = db.get(tblName).find({$and:[{org_id:orgId},{groupCode: {$in: groupCodes}}]},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {groupCode: doc.groupCode,groupName:doc.groupName,status:doc.status_id};
        var items = doc.itemCodes;                
        debuglog(items);
        debuglog(doc.itemCodes);
        
       tmpArray.itemCount = items.length;
     
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.groups = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.groups = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function checkGroupCode(tblName,groupData,callback){
var totalRows = 0;
var data = {};

var promise = db.get(tblName).find({groupCode: groupData.groupCode});

promise.each(function(doc){
   totalRows++;
});

promise.on('complete',function(err,doc){

 if(totalRows != 1){
   data.errorCode = 1;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function deleteGroups(tblName,groupData,groupCodes,callback){
 
  var promise = db.get(tblName).remove({$and:[{org_id:groupData.orgId},{groupCode: {$in: groupCodes}}]});
   promise.on('success',function(err,doc){
       callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
} 


function moveGroupHeader(srcTable,destTable,groupCode,groupData,callback){

var promise1 = db.get(destTable).remove({groupCode:groupCode});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({groupCode:groupCode});
  
  //there must be only one row per groupCode
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({groupCode:groupCode});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function checkGroupStatusCount(tblName,orgId,groupCode,forwardOrReverse,newStatus,linkCount,usedCount,callback){

   debuglog("Note business type checks are done away with");
   var totalRows = 0;
   var data={};
   var currStatus = 0;
   var currLinkCount = 0;
   var currUsedcount = 0;
   
   var promise = db.get(tblName).find({$and:[{org_id:orgId,groupCode:groupCode}]});
   promise.each(function(doc){
        debuglog("Since the group_header master table is dynamically created");
        debuglog("fields linkCount and usedCount may not exist");
        debuglog("It is also not defined in master fields so initialize here");
        debuglog("I think no need to do parseInt here since it is not request obtained");
        currStatus = doc.status_id;
        currLinkCount = parseInt(doc.linkCount) || 0;
        currUsedCount = parseInt(doc.usedCount) || 0;
        totalRows++;
       }); 
   promise.on('complete',function(err,doc){
      if (totalRows != 1){
         callback(6,null);   
        }
        else {
          if(forwardOrReverse == 'forward'){
          debuglog("This is a forward moving action");
          debuglog("So call move forward fn with incremented counts");
          var newLinkCount = currLinkCount+linkCount;
          var newUsedCount = currUsedCount+usedCount;
          debuglog(newLinkCount);
          debuglog(newUsedCount);
          moveForwardGroupStatus(tblName,orgId,groupCode,currStatus,newStatus,newLinkCount,newUsedCount,linkCount,usedCount,callback);
          }
        if(forwardOrReverse == 'reverse'){
          debuglog("This is a Revers moving action");
          debuglog("So call move forward fn with decremented counts");
          debuglog("If this becomes negative it means some error while linking");
          var newLinkCount = currLinkCount-linkCount;
          var newUsedCount = currUsedCount-usedCount;
		  (newLinkCount<0)? newLinkCount = 0:newLinkCount=newLinkCount;
		  (newUsedCount<0)? newUsedCount = 0:newUsedCount=newUsedCount;
		  debuglog("Some rare cases of dumping after editing etc., can become negative ones");
		  debuglog(newLinkCount);
          debuglog(newUsedCount);
          moveReverseGroupStatus(tblName,orgId,groupCode,currStatus,newStatus,newLinkCount,newUsedCount,linkCount,usedCount,callback);
          }
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function moveForwardGroupStatus(tblName,orgId,groupCode,currStatus,newStatus,linkCount,usedCount,origLinkCount,origUsedCount,callback){

   if(currStatus < newStatus){
   debuglog("Current status is less than the intended status");
   debuglog("so we will update status and counts");
   debuglog(currStatus);
   debuglog(newStatus);
     var promise = db.get(tblName).update({$and:[{org_id:orgId,groupCode:groupCode}]},
     {$set:{status_id:newStatus,
            linkCount: linkCount,
	 usedCount: usedCount}},
     {upsert:false}
   );
   }
   else{
   debuglog("Current status is greater than the intended status");
   debuglog("so we will update only counts");
   debuglog(currStatus);
   debuglog(newStatus);
     var promise = db.get(tblName).update({$and:[{org_id:orgId,groupCode:groupCode}]},
     {$set:{linkCount: linkCount,
	 usedCount: usedCount}},
     {upsert:false}
   );
   }
  
  promise.on('success',function(err,doc){
     debuglog("Now let us change the status of its itemCodes also");
     debuglog("We will just call menuitem moveItemStatus");
     debuglog("with new status and the link/used count indicator");
     debuglog("since there ae various possibilities of items being linked to different");
     debuglog("groups menus etc., let the logic of changing status/counts be decided there");
     debuglog("Note we are sending the original counts as received by function");
     debuglog("Not the changed ones since it is for Groups")
     debuglog(currStatus);
     debuglog(newStatus);
     changeItemsForGroups(tblName,orgId,groupCode,'forward',newStatus,origLinkCount,origUsedCount,callback);
      //callback(null,null);
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function moveReverseGroupStatus(tblName,orgId,groupCode,currStatus,newStatus,linkCount,usedCount,origLinkCount,origUsedCount,callback){

   if(newStatus == 83){
     if(usedCount == 0){
      debuglog("Intended Status is Linked and usedCount is zero");
      debuglog("so we will update status and counts");
      debuglog(currStatus);
      debuglog(newStatus);
      var promise = db.get(tblName).update({$and:[{org_id:orgId,groupCode:groupCode}]},
       {$set:{status_id:newStatus,
              linkCount: linkCount,
	   usedCount: usedCount}},
        {upsert:false}
       );
    }
   else{
        debuglog("Intended Status is Linked but usedCount is not zero");
        debuglog("hence this item is in Use by something else so we cant change status to Linked");
        debuglog("so we will update only counts");
        debuglog(currStatus);
        debuglog(newStatus);
        var promise = db.get(tblName).update({$and:[{org_id:orgId,groupCode:groupCode}]},
          {$set:{linkCount: linkCount,
		         usedCount: usedCount}},
           {upsert:false}
         );
      }
    }
   else{
    if(linkCount == 0){
      debuglog("Intended Status is Active and linkcount is zero");
      debuglog("so we will update status and counts");
      debuglog(currStatus);
      debuglog(newStatus);
      var promise = db.get(tblName).update({$and:[{org_id:orgId,groupCode:groupCode}]},
       {$set:{status_id:newStatus,
              linkCount: linkCount,
	   usedCount: usedCount}},
        {upsert:false}
       );
      }
    else{
        debuglog("Intended Status is Active but linkcount is not zero");
        debuglog("hence this item is linked to something else")
        debuglog("so we will update only counts");
        debuglog(currStatus);
        debuglog(newStatus);
        var promise = db.get(tblName).update({$and:[{org_id:orgId,groupCode:groupCode}]},
           {$set:{linkCount: linkCount,
		          usedCount: usedCount}},
           {upsert:false}
         );
       }
    }
  promise.on('success',function(err,doc){
     debuglog("Now let us change the status of its itemCodes also");
     debuglog("We will just call menuitem moveItemStatus");
     debuglog("with new status and the link/used count indicator");
     debuglog("since there ae various possibilities of items being linked to different");
     debuglog("groups menus etc., let the logic of changing status/counts be decided there");
     debuglog(currStatus);
     debuglog(newStatus);
     changeItemsForGroups(tblName,orgId,groupCode,'reverse',newStatus,origLinkCount,origUsedCount,callback);
   
      //callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function changeItemsForGroups(tblName,orgId,groupCode,forwardOrReverse,newStatus,linkCount,usedCount,callback){
  var itemCodes = [];
  var itemUtils = require('../Menu/utils');
  var itemTblName = orgId+"_menuitem_master";
  var async = require('async');
  
  var promise = db.get(tblName).find({groupCode:groupCode});
   promise.each(function(doc){
        itemCodes = doc.itemCodes;
       }); 
   promise.on('complete',function(err,doc){
    debuglog("we will now call itemstatus change async for all items");
    debuglog("but we will not wait for return status check");
    debuglog(itemCodes)
   async.forEachOf(itemCodes,function(key1,value1,callback1){ 
   itemUtils.moveItemStatus(itemTblName,orgId,key1.itemCode,forwardOrReverse,newStatus,linkCount,usedCount,function(err,result){
     debuglog("returned after changing item status");
     debuglog("but not checking error here");
    callback1(); //no need to check error here since anyhow we will call back only
   }); 
  },
  function(err){
      if(err)
        callback(6,null);
      else 
        callback(null,null);
    });
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}
exports.groupCount   = getGroupsCount;
exports.groupsHeader = getGroupsHeader;
exports.groupHeader  = getGroupHeader;
exports.generateGroupCode = generateGroupCode;
exports.groupCreate = updateGroupHeader;
exports.groupsData = getGroupsData;
exports.checkGroupCode = checkGroupCode;
exports.deleteGroups = deleteGroups;
exports.moveGroupHeader = moveGroupHeader;
exports.moveGroupStatus = checkGroupStatusCount;
exports.moveItemStatus = changeItemsForGroups;




