function processRequest(req,res,data){
   if( typeof req.body.menuGroupDetails.groupCode === 'undefined' || typeof req.body.menuGroupDetails.groupStatus === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkGroupCode(req,res,9,data);
}


function checkGroupCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     data.groupCode = req.body.menuGroupDetails.groupCode;
    
     var extend = require('util')._extend;
     var groupData = extend({},data);
    
      var status = req.body.menuGroupDetails.groupStatus;
    
    if(status == 80 || status == 83){
      var tblName = data.orgId+'_group_header';
      debuglog("ACtive and Linked Groups check in Main table");
      debuglog("If no row exists it is an error");
      }
      
    else{
      var tblName = data.orgId+'_Revision_group_header';
      debuglog("For InUse and Rejected statuses check in REvision Table");
      debuglog("But if no row exists it is not an error condition actually");
      debuglog("Just go ahead and create row");
      debuglog("Edit operation for other statuses are not mapped in product status itself so not possible");
      }
   
    groupData.status = status;
  
      datautils.checkGroupCode(tblName,groupData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                if(result.errorCode == 1){
                 if(status == 80 || status == 83){
                 processStatus(req,res,3,data);
                 debuglog("No rows for the given group code");
                 debuglog("Which is supposed to be in active or linked status");
                 debuglog("Hence error");
                 debuglog(groupData);
                 }
                 else
                    saveGroupData(req,res,9,data,groupData,tblName);
                }
                else{
                if(status != 83)
                    saveGroupData(req,res,9,data,groupData,tblName);
                 if(status == 83){
                 debuglog("First we will move all Item status of this group to Active ");
                 debuglog("Update status to Linked again at end");
                 debuglog("This is to take care of Items deleted during Edit of Linked Group");
                 changeItemStatus(req,res,9,data,groupData,tblName); 
                 debuglog(groupData);
                 }
              }
           }
        });
} 


function changeItemStatus(req,res,stat,data,groupData,tblName){
   var datautils = require('./dataUtils');
 
   datautils.moveItemStatus(tblName,data.orgId,groupData.groupCode,'reverse',80,1,0,function(err,result){
      if(err)
        processStatus(req,res,6,data);
      else{
       debuglog("Changed item statuses /link count successfully");
       saveGroupData(req,res,9,data,groupData,tblName);
       }
    });
}
function saveGroupData(req,res,stat,data,groupData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
 var itemsArr = [];
 
     groupData.groupName = req.body.menuGroupDetails.groupName;
     groupData.groupDescription = req.body.menuGroupDetails.groupDescription;
     //groupData.itemsData = req.body.itemsData;
     
     
  async.forEachOf(req.body.itemsData,function(key1,value1,callback1){
    var tmpArr = {itemCode:key1.itemCode};
     itemsArr.push(tmpArr);
     callback1(); 
  },
  function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
           groupData.itemsData = itemsArr;
           updateGroupData(req,res,9,data,groupData,tblName);
           debuglog("No error in populating the items array now we will update table");
           debuglog(groupData);
           }
     });
 

}  
    
function updateGroupData(req,res,stat,data,groupData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
   
      //Note groupCreate is an upsert with true
     datautils.groupCreate(tblName,data.groupCode,groupData,function(err,result) {
       if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved group data");
                debuglog(groupData);
                changeGroupStatus(req,res,9,data,groupData,tblName);
            }
        });
} 


function changeGroupStatus(req,res,stat,data,groupData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
    if(groupData.status == 80 || groupData.status == 83)
      var newStatus = groupData.status;
    if(groupData.status == 82 || groupData.status == 84)
      var newStatus = 85;
      
    debuglog("If active or Linked status is not changed and row will be in active tab");
    debuglog("If inUse or Rejected status is changed to Ready2Auth and row will be in pending tab");
    debuglog("for that case status in active tab will not change");
    debuglog(groupData);
    debuglog(newStatus);
     
      statutils.changeStatus(tblName,data.orgId,data.businessType,daa.userId,data.groupCode,newStatus,'Group Edited',function(err,result) {
         if(err) {
                   processStatus(req,res,6,data);
                 }
          else {
                if(newStatus == 83){
                debuglog("Now we will change the item statuses of this Group to Linked");
                debuglog("Before getting group Data");
                debuglog("Since by now group would have been saved item status will be for")
                debuglog("newly edited items only, deleted items would have gone by now");
                changeItemStatusAgain(req,res,9,data,groupData,tblName); 
                }
                else
                 getGroupData(req,res,9,data,groupData,tblName);
               }
        });
} 

function changeItemStatusAgain(req,res,stat,data,groupData,tblName){
   var datautils = require('./dataUtils');
 
   datautils.moveItemStatus(tblName,data.orgId,groupData.groupCode,'forward',83,1,0,function(err,result){
      if(err)
        processStatus(req,res,6,data);
      else{
       debuglog("Changed item statuses /link count successfully");
       getGroupData(req,res,9,data,groupData,tblName);
       }
    });
}

function getGroupData(req,res,stat,data,groupData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
      var groupArr = [];
      groupArr.push(data.groupCode);
      datautils.groupsData(tblName,data.orgId,groupArr,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
              groups = result.groups;
              getGroupStatusName(req,res,9,data,groups);
              }
        }); 
} 

function getGroupStatusName(req,res,stat,data,groups) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(groups,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('groups_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.groupData = groups;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;








