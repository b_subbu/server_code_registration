function processRequest(req,res,data){
   if( typeof req.body.groupCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkGroupCodes(req,res,9,data);
}


function checkGroupCodes(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     var extend = require('util')._extend;
     var groupData = extend({},data);
    
     var groupCodes = req.body.groupCodes;
     data.groupCodes = groupCodes;
     
     var tblName = data.orgId+'_Revision_group_header'; 
      
    async.forEachOf(groupCodes,function(key1,value1,callback1){
      groupData.groupCode = key1;

      datautils.checkGroupCode(tblName,groupData,function(err,result) {
         if(err) {
               isError = 1;
               //processStatus(req,res,6,data);
               callback1();
             }
            else {
                if(result.errorCode == 1){
                 //processStatus(req,res,3,data);
                 isError = 2;
                 debuglog("No rows for the given group code");
                 debuglog("Hence error");
                 debuglog(groupData);
                 callback1();
                 }
                else{
                 debuglog("Row found for the given group code");
                 debuglog("in its corresponding table");
                 debuglog("so no error");
                 debuglog(groupData);
                 callback1();
              }
            }
        });
        },
    function(err){
      if(err)
           processStatus(req,res,6,data);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else if(isError == 2)
             processStatus(req,res,3,data);
           else{
           //moveGroupData(req,res,9,data,groupData,groupCodes,tblName);
           debuglog("No error now we will move to main Table");
           debuglog("Let us first temporarily move all the items to Active status");
           debuglog("Before moving to main table")
           debuglog("Will log status change first before deleting data");
           changeItemStatus(req,res,stat,data,groupData,groupCodes,tblName);
           debuglog(groupData);
           }
           }
     });
 
} 

function changeItemStatus(req,res,stat,data,groupData,groupCodes,tblName){
   var datautils = require('./dataUtils');
   var async = require('async');
   
   var mainTblName = data.orgId+'_group_header';
   debuglog("Since we have to change items for the rows in main table now");
  
  async.forEachOf(groupCodes,function(key1,value1,callback1){
   datautils.moveItemStatus(mainTblName,data.orgId,key1,'reverse',80,1,1,function(err,result){
      if(err)
        processStatus(req,res,6,data);
      else
        callback1();
    });
    },
    function(err){
       if(err)
         processStatus(req,res,6,data);
      else{
        debuglog("Changed item statuses /link count successfully");
        moveGroupData(req,res,9,data,groupData,groupCodes,tblName);
      }
    });
}


function moveGroupData(req,res,stat,data,groupData,groupCodes,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   var srcTable = tblName;
   var destTable = data.orgId+'_group_header';
   
   debuglog("moving from Revision table to main table");
   debuglog(srcTable);
   debuglog(destTable); 
   
  async.forEachOf(groupCodes,function(key1,value1,callback1){
     datautils.moveGroupHeader(srcTable,destTable,key1,groupData,function(err,result) {
       if(err) {
                isError = 1;  
               //processStatus(req,res,6,data);
               callback1();
              }
         else {
                debuglog("moved group data to active tab");
                debuglog(groupData);
                callback1();
            }
        });
  },
  function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else {
              changeGroupStatus(req,res,9,data,groupData,groupCodes,srcTable,destTable);
              debuglog("now we will change status to InUse in Active tab");
              debuglog("because only InUse items would have come to Pending Tab in first place");
              debuglog(groupData);
           }
     });
 

}  
    
function changeGroupStatus(req,res,stat,data,groupData,groupCodes,srcTable,destTable) {
   var async = require('async');
   var statutils = require('./statUtils');
   var isError = 0;
   
   var newStatus = 84;
 
 //TODO create change status as array using in instead of one by one
  async.forEachOf(groupCodes,function(key1,value1,callback1){
     
    statutils.changeStatus(destTable,data.orgId,data.businessType,data.userId,key1,newStatus,'Group Approved',function(err,result) {
        if(err) {
                isError = 1;  
               //processStatus(req,res,6,data);
               callback1();
              }
         else {
                debuglog("moved group data to active tab");
                debuglog(groupData);
                callback1();
            }

        });
  },
  function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else {
              //getGroupData(req,res,9,data,groupData,groupCodes,destTable);
              changeItemStatusAgain(req,res,9,data,groupData,groupCodes,destTable);
              debuglog("now we will get Data");
              debuglog("Note: data should be obtained from Active table now");
              debuglog("First let us make all items InUse again");
              debuglog(groupData);
           }
     });
 
} 

function changeItemStatusAgain(req,res,stat,data,groupData,groupCodes,tblName){
   var datautils = require('./dataUtils');
   var async = require('async');
  
   async.forEachOf(groupCodes,function(key1,value1,callback1){
 datautils.moveItemStatus(tblName,data.orgId,groupData.groupCode,'forward',84,1,1,function(err,result){
      if(err)
        processStatus(req,res,6,data);
      else
        callback1();
      });
      },
      function(err){
       if(err)
         processStatus(req,res,9,data);
       else{
       debuglog("Changed item statuses /link count successfully");
       getGroupData(req,res,9,data,groupData,groupCodes,tblName);
       }
    });
}

function getGroupData(req,res,stat,data,groupData,groupCodes,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
      debuglog(groupCodes);
      datautils.groupsData(tblName,data.orgId,groupCodes,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
                groups = result.groups;
                getGroupStatusName(req,res,9,data,groups);
              }
        }); 
} 

function getGroupStatusName(req,res,stat,data,groups) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(groups,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('groups_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.groupData = groups;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
  
exports.getData = processRequest;









