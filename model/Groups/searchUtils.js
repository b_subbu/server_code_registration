function getGroupSearch(req,res,oid,pid,tid,pgid,size,data) {

 var searchInp = data.searchParams;

 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
 var isOtherSearch = false;
 var isCountSearch = false;
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         isOtherSearch = true;
         break;
       case 'groupCode':
        var tmpArray =  {groupCode: {$regex:key,$options:'i'}}; ; 
        searchArr.push(tmpArray);
         break; 
       case 'groupName':
        var tmpArray =  {groupName: {$regex:key,$options:'i'}}; ; 
        searchArr.push(tmpArray);
        break; 
      case 'itemCount':
       minCount = parseInt(key[0]);
       maxCount = parseInt(key[1]);
       isCountSearch = true;
       break;
    
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           
           if(isCountSearch == true){
            if(isOtherSearch == true){
            getGroupSearchData(req,res,oid,pid,tid,pgid,999,searchArr,data,minCount,maxCount,true,true);
            debuglog("This is a case of Status+count search");
            debuglog("so first we will do a filter based search for status");
            debuglog("Then remove items having count outside the range");
            debuglog("Setting size arbitarily to high value of 9999 since first rows may not be inside count range");
            debuglog("will do limiting of size=20 in count function");
            }
            else{
            getGroupSearchData(req,res,oid,pid,tid,pgid,99999,searchArr,data,minCount,maxCount,true,false);
            debuglog("This is a case of only count search");
            debuglog("so first we will get all rows for the business");
            debuglog("Then remove items having count outside the range");
            debuglog("Setting size arbitarily to high value of 99999 since first rows may not be inside count range");
            debuglog("will do limiting of size=20 in count function");
            
            }
          }
          else{
            getGroupSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data,0,0,false,false);
            debuglog("This is a case without count search");
            debuglog("so just need to get all  rows for the business as per search criteria");
            debuglog("Currently only status+count search combination is possible"); 
            debuglog("count+any other field combination is not possible so isOtherSearch is dontcare for this case");
          }
        }
     });
}

function getGroupSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data,minCount,maxCount,isCountSearch,isOtherSearch){
   var totalRows =0;
   var resultArray = [] ;
 
   var page = parseInt(pgid);
   var skip = page > 0 ? ((page - 1) * size) : 0;
      
     if(tid == 501)
      var tablName = oid+'_group_header';
    else
      var tablName = oid+'_Revision_group_header';
  
   var promise = db.get(tablName).find({$and:searchArr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
         totalRows++;
        var tmpArray = {groupCode: doc.groupCode,groupName:doc.groupName,status:doc.status_id};
        var items = doc.itemCodes;                
        tmpArray.itemCount = items.length;
     
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.recordCount = totalRows;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       processStatus(req,res,9,data);
      }
      else {
        if(isCountSearch == false){
        debuglog("There is no count search so let us get name of statuses");
        getGroupStatName(req,res,9,data,resultArray);
          }
        else{
          debuglog("There is a count search so let us filter based on count");
          debuglog("before getting name of statuses");
          filterCountSearch(req,res,9,data,minCount,maxCount,resultArray);
           }
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);
    
	});
} 

function filterCountSearch(req,res,stat,data,minCount,maxCount,tmpArray) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
  
  async.forEachOf(tmpArray,function(key,value,callback){
    if(key.itemCount >= minCount && key.itemCount <= maxCount){
      itemMasters.push(key);
      debuglog("This is falling under given count range");
      debuglog(key.itemCount);
      debuglog(minCount);
      debuglog(maxCount);
      callback();  
    
    }
   else{
      debuglog("This is not falling under given count range");
      debuglog("so ignoring this element");
      debuglog(key.itemCount);
      debuglog(minCount);
      debuglog(maxCount);
      callback();  
    
    }
  },
  function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         
            }
      else {
  		       var finalArray = itemMasters.slice(0,20);
             debuglog("Get the first 20 elements of array");
             debuglog("Since initial search size was 9999");
             debuglog(itemMasters);
             debuglog(finalArray);
             getGroupStatName(req,res,9,data,finalArray);
 			     }
         });
 

}


function getGroupStatName(req,res,stat,data,mastData) {
   var async = require('async');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
   var mastutils = require('../Registration/masterUtils');
  
    async.forEachOf(mastData,function(key1,value1,callback1){
      
        var masterData = mastData[value1]; //because mastdata is double array
        async.forEachOf(masterData,function(key2,value2,callback2){
             if(value2 == 'status'){
              var statID=key2;
              mastutils.masterStatusNames('groups_status_master',statID,function(err,result) {
              if(err){ isError =1; callback2();}
               else {
                   mastData[value1]['status'] = result.statusName;
                  callback2(); 
                }
              }); 
             }
            else
               callback2();
        },
       function(err){
         if(err){
             isError =1;         
		      	 callback1();
			  }
        else {
             callback1(); 
            }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         
             }
      else {
		    if(isError == 1)
		        	 processStatus (req,res,6,data);
        else 
               populateSearchData(req,res,9,data,mastData);
			  }
      });
 

}

function populateSearchData(req,res,stat,data,mastData){

 var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
     
      async.forEachOf(mastData,function(key1,value1,callback1){
      if(key1)
        itemMasters.push(key1);
      
        callback1();
        },
      function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		   if(isError == 1)
			  processStatus (req,res,6,data);
       else{				
         data.searchData = itemMasters;
		     processStatus(req,res,9,data,mastData);
			}
    }
      });
 


}
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Search/searchData');
  
    controller.process_status(req,res,stat,data);
}

exports.searchGroup = getGroupSearch;


 



