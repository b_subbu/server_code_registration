function processRequest(req,res,data){
   if(typeof req.body.groupCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getGroupData(req,res,9,data);
}

function getGroupData(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.groupCode = req.body.groupCode;
    var tid = req.body.tabId;
    
    var tabl_name = data.orgId+'_group_header'; //cust app data always from active tab only
   
     var extend = require('util')._extend;
     var groupData = extend({},data);
     
  
      datautils.groupHeader(tabl_name,groupData,function(err,result) {
           debuglog(result.errorCode);
           debuglog("after getting result from groupHeader");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && ( result.errorCode == 1 || result.status != 84)) {
              processStatus(req,res,4,data);
              isError = 1;
              return;
              debuglog("result errorCode is 1");
              }
            else {
                data.groupName = result.groupName;
                data.groupDescription   = result.groupDescription;
                data.groupStatus = result.status;
                itemCodes = result.itemCodes;
                getItemData(req,res,9,data,groupData,itemCodes);
                debuglog("No error case");
                debuglog(itemCodes);
                debuglog(groupData);
            }
          }
        });
} 


function getItemData(req,res,stat,data,groupData,itemCodes) {
 var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

var itemsArr = [];

var itemsLength = itemCodes.length;

if(itemsLength == 0){
 data.items = null;
 processStatus(req,res,9,data);
}
else{

 if(typeof req.body.pageId !== 'undefined')
   var pageId = parseInt(req.body.pageId);
 else
   var pageId = 1;  
 
 data.pageId = pageId;
 data.totalRecords = itemsLength;
 var strtIdx = parseInt((pageId-1)*20);
 var endIdx = strtIdx+20;
 var subItemCodes = itemCodes.slice(strtIdx,endIdx);
 
 debuglog(pageId);
 debuglog(strtIdx);
 debuglog(endIdx);
 debuglog(itemCodes);
 debuglog(subItemCodes);
  
 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(subItemCodes,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.itemCode,tablName,function(err,results){
     if(err){
     isError = 1;
     callback();
     }
     else {
       if(results.masterData[0] != null){
        var tmpArr = {};
          tmpArr.code = results.masterData[0]['itemCode'];
          tmpArr.name = results.masterData[0]['itemName'];
          tmpArr.description = results.masterData[0]['itemDescription'];
          tmpArr.price = results.masterData[0]['itemPrice'];
          tmpArr.status = results.masterData[0]['status_id'];
          if(results.masterData[0]['itemVisual'] != null)
            tmpArr.visual = results.masterData[0]['itemVisual'][0];
          else
            tmpArr.visual = null;
            
       debuglog(key);
       debuglog(results.masterData[0]);    
       debuglog(tmpArr.itemCode);
       debuglog(tmpArr.visual);   
       itemsArr.push(tmpArr);
       debuglog(itemsArr);
       callback();
       }
       else
        callback();
    }
  });
  },
   function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else{
            getItemStatusName(req,res,9,data,itemsArr);
            debuglog("Items obtained");
            debuglog(itemsArr[0]);
            }
          }
     });
  }
} 

function getItemStatusName(req,res,stat,data,items) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(items,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames('menuitem_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.items = items;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



