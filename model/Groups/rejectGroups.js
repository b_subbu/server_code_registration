function processRequest(req,res,data){
   if(typeof req.body.groupCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkGroupCodes(req,res,9,data);
}


function checkGroupCodes(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     var extend = require('util')._extend;
     var groupData = extend({},data);
    
     var groupCodes = req.body.groupCodes;
     data.groupCodes = groupCodes;
     
     var tblName = data.orgId+'_Revision_group_header'; 
      debuglog("only REady2Auth Groups will be rejected");
      debuglog(groupCodes);
      
    async.forEachOf(groupCodes,function(key1,value1,callback1){
      groupData.groupCode = key1;

      datautils.checkGroupCode(tblName,groupData,function(err,result) {
         if(err) {
               isError = 1;
               //processStatus(req,res,6,data);
               callback1();
             }
            else {
                if(result.errorCode == 1){
                 //processStatus(req,res,3,data);
                 isError = 2;
                 debuglog("No rows for the given group code");
                 debuglog("Hence error");
                 debuglog(groupData);
                 callback1();
                 }
                else{
                 debuglog("Row found for the given group code");
                 debuglog("in its corresponding table");
                 debuglog("so no error");
                 debuglog(groupData);
                 callback1();
              }
            }
        });
        },
    function(err){
      if(err)
           processStatus(req,res,6,data);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else if(isError == 2)
             processStatus(req,res,3,data);
           else{
           changeGroupStatus(req,res,9,data,groupData,groupCodes,tblName);
           debuglog("No error now we will delete data");
           debuglog("Will log status change first before deleting data");
           debuglog(groupData);
           }
           }
     });
 
} 


    
function changeGroupStatus(req,res,stat,data,groupData,groupCodes,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
   var isError = 0;
   
   var newStatus = 82;
 
 //TODO create change status as array using in instead of one by one
  async.forEachOf(groupCodes,function(key1,value1,callback1){
     
    statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,key1,newStatus,'Group Rejected',function(err,result) {
        if(err) {
                isError = 1;  
               //processStatus(req,res,6,data);
               callback1();
              }
         else {
                debuglog(groupData);
                callback1();
            }

        });
  },
  function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else {
              getGroupData(req,res,9,data,groupData,groupCodes,tblName);
              debuglog("now we will get Data");
              debuglog("Note: data should be obtained from Pending table now");
              debuglog(groupData);
           }
     });
 
} 

function getGroupData(req,res,stat,data,groupData,groupCodes,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
      debuglog(groupCodes);
      datautils.groupsData(tblName,data.orgId,groupCodes,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
                groups = result.groups;
                getGroupStatusName(req,res,9,data,groups);
              }
        }); 
} 

function getGroupStatusName(req,res,stat,data,groups) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(groups,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('groups_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.groupData = groups;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




