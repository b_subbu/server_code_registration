function getMenus(tablName,orgId,bizType,callback){ 
  var totalRows = 0;
   var data={};
   var resultArr = [];
    
   var promise = db.get(tablName).find({$and:[{org_id:orgId},{bus_type:bizType}]},{sort:{release_date: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {code:doc.menuCode,name:doc.menuName,description:doc.menuDescription};
        resultArr.push(tmpArray); 
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 1; //no item codes for this business 
         callback(null,data);   
        }
        else {
          data.errorCode = 0;
          data.countRows = totalRows;
          data.tblName = tablName;
          data.masterData = resultArr;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getMenu(tablName,orgId,bizType,menuCode,callback){ 
  var totalRows = 0;
   var data={};
   var menuData = "";
    
   var promise = db.get(tablName).find({$and:[{org_id:orgId},{bus_type:bizType},{menuCode:menuCode}]},{sort:{release_date: -1}});
   promise.each(function(doc){
        totalRows++;
          data = {code:doc.menuCode,name:doc.menuName,description:doc.menuDescription,menuData:doc.menuData};
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 1; //no item codes for this business 
         callback(null,data);   
        }
        else {
          data.errorCode = 0;
          data.countRows = totalRows;
          //data.masterData = menuData;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getDistinctValues(tablName,orgId,bizType,itemCode,fieldName,callback){ 
  var totalRows = 0;
   var data={};
    
   var promise = db.get(tablName).distinct(fieldName,{$and:[{org_id:orgId},{bus_type:bizType},{itemCode:itemCode}]});
  promise.on('complete',function(err,doc){
          data.errorCode = 0;
          data.masterData = doc;
          callback(null,data);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getSelectionData(orgId,bizType,itemCode,searchCond,table_name,callback){ 
var data = {};
var resultArray = [];
var totalRows = 0;

//create a copy of objects otherwise it pushes all attributes to
//original object
var extend = require('util')._extend;
var searchCondition = extend({},searchCond);
searchCondition.org_id = orgId;
searchCondition.bus_type = bizType;
searchCondition.itemCode = itemCode;
    var promise = db.get(table_name).find(searchCondition);
  promise.each(function(doc){
   totalRows++;
   
     var tmpArray = doc;
     resultArray.push(tmpArray);
       }); 
    
  promise.on('success',function(doc){
    data.selectionData = resultArray;
    callback(null,data);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function getDistinctSearchValues(tablName,orgId,bizType,itemCode,searchCond,fieldName,callback){ 
  var totalRows = 0;
   var data={};
 var extend = require('util')._extend;
 var searchCondition = extend({},searchCond);
 
 searchCondition.org_id = orgId;
  searchCondition.bus_type = bizType;
  searchCondition.itemCode = itemCode;
  
   var promise = db.get(tablName).distinct(fieldName,searchCondition);
  promise.on('complete',function(err,doc){
          data.errorCode = 0;
          data.masterData = doc;
          callback(null,data);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getNodeData(menuNodes,nodeId,callback){
    
    var async = require('async');
    var isError = 0;
    var linkNodes = [];
   
        async.forEachOf(menuNodes.menuData,function(key,value,callback1){
                 if(key.nodeParent == nodeId){
                     var tmpArr = key.nodeData;
                     linkNodes.push(tmpArr);
                     callback1();
                      }   
                  else
                  callback1();
             },
    function(err){
       if(err || isError == 1){
            callback(6,null);
          }
      else {
          var menuData = {};
          menuData.menuName = menuNodes.name;
          menuData.menuDescription = menuNodes.description;
          menuData.menuLinkage = linkNodes;
          callback(null,menuData);
        }
      });

}

exports.menus = getMenus;
exports.menu  = getMenu;
exports.distinctValues = getDistinctValues;
exports.selectionData = getSelectionData;
exports.distinctSearchValues = getDistinctSearchValues;
exports.nodeData = getNodeData; 
