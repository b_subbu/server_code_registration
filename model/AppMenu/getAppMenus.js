function processRequest(req,res,data){
      getMenus(req,res,9,data);
}


function getMenus(req,res,stat,data){
    
    var async = require('async');
    var utils = require('./datautils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   var tabl_name = orgId+'_release_menus_master';
         
          utils.menus(tabl_name,orgId,bizType,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.errorCode != 0 ) {
             data.menuCount = 0;
             data.menus = [];
             processStatus (req,res,4,data);
             isError = 1;
             return;
            }
            
         if (isError == 0 && results.errorCode == 0){
              data.menuCount = results.countRows;
              data.menus = results.masterData;
              getFirstChild(req,res,9,data)
            }
         });
}
//First get the menuData of each Menu Code
//Then get First child of the root node
//Since async is firing before start of next loop
//use code inside result instead of MenuCode from outer loop
function getFirstChild(req,res,stat,data){
    
    var async = require('async');
    var utils = require('./datautils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    
   var tabl_name = orgId+'_release_menus_master';
    
      async.forEachOf(data.menus,function(key,value,callback){
          menuCode = key.code;     
          utils.menu(tabl_name,orgId,bizType,menuCode,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
            callback();
            }
          if(isError == 0 && results.errorCode != 0 ) {
             data.menus = "";
             processStatus (req,res,4,data);
             isError = 1;
             callback();
            }
         if (isError == 0 && results.errorCode == 0){
            utils.nodeData(results,results.code,function(err,results1) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             callback();
            }
           else{
              key.menuLinkages = results1.menuLinkage;
              callback();
              }
            });
          }
        });
      },
      function(err){
       if(err || isError == 1){
            processStatus(req,res,6,data);
          }
      else {
         processStatus(req,res,9,data);
        }
      });

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

