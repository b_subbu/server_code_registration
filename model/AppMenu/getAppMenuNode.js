function processRequest(req,res,data){
   if(typeof req.body.menuCode === 'undefined' || typeof req.body.nodeId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getMenu(req,res,9,data);
}

function getMenu(req,res,stat,data){
    
    var async = require('async');
    var utils = require('./datautils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
    var menuCode = req.body.menuCode;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   data.menuCode = menuCode
   
   var tabl_name = orgId+'_release_menus_master';
         
          utils.menu(tabl_name,orgId,bizType,menuCode,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && results.errorCode != 0 ) {
             data.menuData = "";
             processStatus (req,res,4,data);
             isError = 1;
             return;
            }
            
         if (isError == 0 && results.errorCode == 0){
              //data.menuData = results.masterData;
              getNodeData(req,res,9,data,results);
            }
         });
}

function getNodeData(req,res,stat,data,menuNodes){
    
       var utils = require('./datautils');
       var nodeId = req.body.nodeId;
       
         utils.nodeData(menuNodes,nodeId,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           else{
              data.menuLinkages = results.menuLinkage;
              processStatus(req,res,9,data);
            }
         });

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;



