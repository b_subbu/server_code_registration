function processRequest(req,res,data){
   if( typeof req.body.itemCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItemCode(req,res,9,data);
}

function getItemCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('../Menu/utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var itemCode = req.body.itemCode;
  
      utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,4,data);
              return;
              }
            else {
            tabl_name = result.tblName; 
            data.itemCode = itemCode;
            mastFields = result.mastFields;
            itemStatus = result.itemStatus;
            if(itemStatus != 84){
             processStatus(req,res,4,data); //only InUse status
             return;
            }
            else
             getMasterData(req,res,9,data,mastFields,tabl_name);
            }
          }
        });
}


function getMasterData(req,res,stat,data,mastFields,table_name) {
   var async = require('async');
   var utils = require('../Menu/utils');
   var type = require('type-detect');
   var bizData = [];
  
    utils.masterData(data.orgId,data.businessType,data.itemCode,table_name,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 itemMasterData = result.masterData;
                  formatMasterData(req,res,9,data,mastFields,itemMasterData); 
              }
        });
} 


function formatMasterData(req,res,stat,data,mastFields,mastData) {
   var async = require('async');
   var type = require('type-detect');
   var itemMasters = {};
   var visualArray = [];
      async.forEachOf(mastData,function(key1,value1,callback1){
         async.forEachOf(key1,function(key2,value2,callback2){
             async.forEachOf(mastFields,function(key3,value3,callback3){
            var key_name = key3.name;
              if(key_name == 'itemVisual'){
                visualArray = key1.itemVisual;
                visualIndex = value3;
                callback3(); //visuals is seperate grid no need to send title
               }
            else if(key_name == value2)  {
                   itemMasters[key_name] = key2;
                   callback3();
                 }
            else
              callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             callback2(); 
           }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            callback1(); 
           }
        });
      },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         
              }
      else {
          mastFields.splice(visualIndex,1); 
          data.itemDetails = itemMasters;
          if(visualArray == null)
            data.visuals = []; 
          else
           data.visuals = visualArray;
       
          getSelectionFields(req,res,9,data);
        }
      });
 

} 

function getSelectionFields(req,res,stat,data) {
   var async = require('async');
   var utils = require('../Menu/utils');
   var type = require('type-detect');
   var bizData = [];
  
    utils.selectionFieldsOrgId(data.orgId,data.businessType,data.itemCode,function(err,result) {
       if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  selectionFields = result.selectFields;
                  selectionTable = result.table_name_detail;
                  getSelectionData(req,res,stat,data,selectionFields,selectionTable);      
                  }
            else {
                  data.selectionCriteria = [];
                  processStatus(req,res,9,data); 
                  //probably items without selection criterias
                  //so no need to populate selectionCriteria or selectionCriteriaColumns
                  }
        }
     });
} 

//Note: the fields in selection table can change from org to org and item code to itemcode
// so should use the fields definitions
function getSelectionData(req,res,stat,data,selectionFields,table_name) {
   var async = require('async');
   var utils = require('../Menu/utils');
   var type = require('type-detect');
   var bizData = [];
   var selectFields = [];
   var isSelectionCriteria = false;
   //if selection criteria columns are present then render 
   //only drop-down not data
  
  
    utils.selectionData(data.orgId,data.businessType,data.itemCode,table_name,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
             for(idx = 0; idx<selectionFields.length;idx++){
                if(selectionFields[idx].name == 'itemDetailCode' || selectionFields[idx].name == 'isDeleted')
                   continue;
                else {
                    if(selectionFields[idx].visibility == 2){
                   var tmpArray = {name:selectionFields[idx].name};
                      selectFields.push(tmpArray);
                   }
                   if(selectionFields[idx].purpose == 1)
                     isSelectionCriteria = true; //1 is selection criteria and 2 is purpose
                   
                  }
                }
                 if(isSelectionCriteria == false) {
                 //populate selectionCriteria only if no selectionCriteria fields
                  selectionData = result.selectionData;
                  formatSelectionData(req,res,9,data,selectionFields,selectionData,selectFields,table_name);
                  }
                 else{
                   //populate selectionCriteriaColumns only
                      populateSelectionFilters(req,res,9,data,selectFields,table_name);
                 } 
              }
        });
} 

function formatSelectionData(req,res,stat,data,selectionFields,selectionData,selectFields,table_name) {
   var async = require('async');
   var itemMasters = [];
   var key_sequence = 0;
      async.forEachOf(selectionData,function(key1,value1,callback1){
      subitemMasters = [];
         async.forEachOf(key1,function(key2,value2,callback2){
            async.forEachOf(selectionFields,function(key3,value3,callback3){
            var key_name = key3.name;
            var key_label = key3.name;
            var key_sequence = key3.sequence;
            if(key_name == 'isDeleted')
               callback3();
            else{
            if(key_name == value2)  {
                  //need to get sequence of detail fields here to sort later
                  var tmpArray = {};
                  tmpArray[key_label] = key2;
                  tmpArray['sequence'] = key_sequence;
                  subitemMasters.push(tmpArray); 
                  callback3();
                 }
            else
              callback3();
          }
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             callback2(); 
           }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
           itemMasters.push(subitemMasters);
            callback1(); 
           }
        });
      },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          sortSelectionData(req,res,9,data,itemMasters,selectFields,table_name);
        }
      });
 

} 

//We have to sort the selection criteria field as per order of detail fields
//because this order is important while rendering 
//This fn is reached when selectionCriteria has only one column
//All fields before price is rendered in selectionCriteria param
//All fields after price is rendered in infoColumns param
function sortSelectionData(req,res,stat,data,selectionData,selectFields,table_name) {
   var async = require('async');
   var itemMasters = [];
   var infoMasters = [];
   var key_sequence = 0;
   var subitemMasters = {};
   var subinfoMasters = {};
   var isPriceReached = false;
   
       selectData = selectionData[0];
       //because now only one row case will come here
       selectData.sort(function(a,b) {return a.sequence-b.sequence});
   
            async.forEachOf(selectData,function(key2,value2,callback2){
              async.forEachOf(key2,function(key3,value3,callback3){
                   if(value3 != 'sequence')  {
                     if (value3 == 'price'){
                      isPriceReached = true;
                      subitemMasters[value3] = key3
                     }
                     else{
                       if(isPriceReached == true)
                         subinfoMasters[value3] = key3;
                       else
                         subitemMasters[value3] = key3;
                    }
                     callback3();
                    }
               else
                 callback3();
        },
         function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
           itemMasters.push(subitemMasters);
           infoMasters.push(subinfoMasters);
           
            callback2(); 
           }
        });
      },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          data.selectionCriteria = itemMasters[0];
          data.infoColumns =    infoMasters[0];
          processStatus(req,res,9,data);
          }
      });
 

} 

function populateSelectionFilters(req,res,stat,data,selectFields,table_name) {
   var async = require('async');
   var utils = require('./datautils');
   var type = require('type-detect');
   var itemMasters = [];
   var key_sequence = 0;
   var isError = 0;
          var selectData = selectFields[0];  //the first field only for drop-down box
            async.forEachOf(selectData,function(key2,value2,callback2){
                 var fieldName = key2;
                  utils.distinctValues(table_name,data.orgId,data.businessType,data.itemCode,fieldName,function(err,result){
                  if(err) callback2();
                  
                  else{
                  
                  selectData.availableValues = result.masterData;
                  callback2();
                  }
              });
            },
        function(err){
       if(err || isError == 1){
             processStatus (req,res,6,data);
              return;         }
      else {
          data.selectionCriteriaColumns = selectData;
          processStatus(req,res,9,data);
        }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;