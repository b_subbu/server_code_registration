function processRequest(req,res,data){
   if(typeof req.body.itemCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItemCode(req,res,9,data);
}


function getItemCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var itemCodes = req.body.itemCodes;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = result.errorCode;
              isError = 1;
              //callback();
              }
            if( isError == 0 && result.itemStatus != 80 ) {
              errorCode = 7; //item not in correct status 'Active'; status id is hard-coded here
              isError = 1;
              //callback();
              }  
            if(isError == 0) {
            mastFields = result.mastFields;
            tabl_name = result.tblName; //same table name for all item codes
            //callback();
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
           
               data.itemCodes = itemCodes;
              changeItemStatus(req,res,9,data,81,mastFields,tabl_name);
              }
            }
          });
} 

function changeItemStatus(req,res,stat,data,itemStat,mastFields,mastTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
    
    
    
    var reason = '';
   async.forEachOf(data.itemCodes,function(key,value,callback){
      itemCode = key;
       statutils.changeStatus(mastTable,data.orgId,data.businessType,data.userId,itemCode,itemStat,reason,function(err,result) {
          if(err) {
                isError = 1;
               callback();
             }
           else {
            callback();
            }
        });
        },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  deleteData(req,res,9,data,mastTable);
           
              }
            }
          });
     
}
function deleteData(req,res,stat,data,table_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var isError = 0;
   var errorCode = 0;
      
     var itemCodes = req.body.itemCodes;
  
   async.forEachOf(itemCodes,function(key,value,callback){
        itemCode = key;
  
      utils.deleteItemMasterData(data.userId,data.orgId,data.businessType,itemCode,table_name,function(err,result) {
            if(err) {
               isError = 1;
               callback();
             }
          else {
                callback();
               }
      });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                 processStatus(req,res,6,data);
                }
           else{
              processStatus(req,res,9,data);
            }
          }
        }); 
}   

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;





