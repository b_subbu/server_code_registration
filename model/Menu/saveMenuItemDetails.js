function processRequest(req,res,data){
   if( typeof req.body.menuItemDetails === 'undefined' )
       processStatus(req,res,6,data); //system error
   else 
       getDetailFiles(req,res,9,data);
}


function getDetailFields(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var itemCode = req.body.menuItemDetails.itemCode;
  
    var orgId = data.orgId;
    var bizType = data.businessType;
     async.parallel({
         detailStat: function(callback){utils.checkItemCode(orgId,bizType,itemCode,callback); },
         },
         function(err,results) {
          if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
             if(isError == 0 && (results.detailStat.errorCode != 0 && results.detailStat.errorCode != 5)) {
              processStatus (req,res,results.detailStat.errorCode,data);
              isError = 1;
              return;
             }
            if( isError == 0 && (results.detailStat.itemStatus != 82 && results.detailStat.itemStatus != 84))
              {
              data.itemCode = itemCode;
              processStatus(req,res,7,data); //item not in correct status 'InUse'/Rejected status id is hard-coded here
              isError = 1;
              return;
              }
            if(isError == 0) {
                mastFields = results.detailStat.mastFields;
                mastTable = results.detailStat.tblName;
                data.itemCode = itemCode;
                //itemStat = results.detailStat.itemStatus;
                checkMandatoryFields(req,res,9,data,mastFields,mastTable);
            }
          }); 
} 


function checkMandatoryFields(req,res,stat,data,mastFields,mastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var isExists = true; //there need not be any mandatory fields for Details
   var isError = 0;
    async.forEachOf(mastFields,function(key1,value1,callback1){
       if(key1.isMandatory == true && isError == 0){
         isExists = false;
          async.forEachOf(req.body.menuItemDetails,function(key2,value2,callback2){
              if(key1.name == value2)  
                   isExists = true;
                   
              callback2();
           },
           function(err){
             if(err){
               processStatus (req,res,6,data);
                return;         
            }
            else {
               if(isExists == false || isError == 1){
               isError = 1;
               return;
              }
           }
         });
        callback1();
    }
    else
      callback1();
    },
    function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
      else {
           if(isExists == false || isError == 1){
              processStatus (req,res,4,data);
              return;
           }
      else
         createMenuItemDetailsData(req,res,9,data,mastFields,mastTable);
          }
      });
} 

function createMenuItemDetailsData(req,res,stat,data,mastFields,mastTable){
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
  
    var detailsData = req.body.menuItemDetails;
    if(typeof req.body.selectionCriteria !== 'undefined'){
      var selectionFields = req.body.selectionCriteria; //all rows must be same, so just take first row
      var minPrice = Math.min.apply(Math,selectionFields.map(function(o){return o.price;}));
      var maxPrice = Math.max.apply(Math,selectionFields.map(function(o){return o.price;}));
      }
     else
     {
      var minPrice=0;//user has not yet created the selection criteria
      var maxPrice=0;
     }
    var rev_mastTable = 'Revision_'+mastTable;
    //NOTE: table name changes here
    var itemCode = req.body.menuItemDetails.itemCode;
  
    var promise= db.get(rev_mastTable).remove({$and:[{org_id:data.orgId,bus_type:data.businessType,itemCode:itemCode}]});
   //because this may be an re-edit on reject item
      promise.on('success',function(doc){
      utils.createDetailsData(data.orgId,data.businessType,rev_mastTable,mastFields,detailsData,minPrice,maxPrice,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
           getSelectionFields(req,res,9,data,mastFields,mastTable); 
           //so we will hard-code selectionTable instead of getting from map table
           //Above is not working since in edit new fields will be added/removed
           //so create a new item in org_item_detail_map with itemCode##Revision
           //At time of approval change org_item_detail_map to replace
           //actual itemcode with itemCode##Revision and delete that entry
           //var selectionTable = data.orgId+"_menuitem_detail";
            //checkMandatoryFieldsSelection(req,res,stat,data,selectionTable,mastFields,rev_mastTable);
            }
          }
        });
      });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);
  });


}

//Note we are creating new definition in org_itemdetail_map with
//itemCode = itemCode##Revision
//TODO in approve API to drop and recreate fields while merging data
function getSelectionFields(req,res,stat,data,mastFields,mastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var itemCode = req.body.menuItemDetails.itemCode+'##Revision';
   var selectionFields = req.body.selectionCriteriaColumns; 
  
  var orgId = data.orgId;
  var bizType = data.businessType;
  
  var promise= db.get('org_itemdetail_map').remove({$and:[{org_id:orgId,bus_type:bizType,itemCode:itemCode}]});
   //because user might have removed/added fields in selection critieria so delete and recreate
      promise.on('success',function(doc){
      
      utils.createSelectionFieldsOrgId(data.orgId,data.businessType,itemCode,selectionFields,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  //selectionFields = result.selectFields;
                  selectionTable = result.table_name;
                  checkMandatoryFieldsSelection(req,res,stat,data,selectionFields,selectionTable,mastFields,mastTable);      
                  }
            else {
                  processStatus(req,res,3,data);
                  }
        }
      });
     });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);
  });

} 

function checkMandatoryFieldsSelection(req,res,stat,data,selectFields,selectTable,mastFields,mastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var isExists = true; 
   var max_selection_code = '';
   //var selectFields = req.body.selectionCriteriaColumns; 
  
   var isError = 0;
    async.forEachOf(selectFields,function(key1,value1,callback1){
         if(isError == 0 && key1.name != 'itemDetailCode'){
         isExists = false;
          async.forEachOf(req.body.selectionCriteria,function(key2,value2,callback2){
           async.forEachOf(key2,function(key3,value3,callback3){
              if(value3 == 'itemDetailCode'){
               //As per Vignesh all newly created items will be sent
               //with code jqxxx which is a default jqgrid generated code
               //so we filter them out for checking max
               //hopefully no customer generates itemcode starting with jq
               if(key3.indexOf('j') == 0 && key3.indexOf('q') == 1)
                   callback3();
                
               else {if (key3 > max_selection_code)
                    max_selection_code = key3;
               callback3();
               }
              
              }
              else{ if(key1.name == value3)  
                   isExists = true;
                   
              callback3();
              }
           },
           function(err){
             if(err){
               processStatus (req,res,6,data);
                return;         
            }
            else {
               if(isExists == false || isError == 1){
                isError = 1;
               return;
              }
           }
         });
        callback2();
        },
       function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
      else {
           callback1(); 
          }
      });  
    }
    else
      callback1();
    },
    function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
      else {
           if(isExists == false || isError == 1){
              processStatus (req,res,4,data);
              return;
           }
      else
          createMenuItemSelectionData(req,res,9,data,selectFields,selectTable,max_selection_code,mastFields,mastTable);
          }
      });
} 

function createMenuItemSelectionData(req,res,stat,data,selectFields,selectTable,maxCode,mastFields,mastTable){
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var newmaxCode = '';
  
    var selectionDatas = req.body.selectionCriteria;
    var itemCode = data.itemCode;
    
    maxCodes = maxCode.split("-");
   
    var lengthCodes = maxCodes.length; 
    
    if(lengthCodes <= 1){
    
     newmaxCode = itemCode+"-"+0;//user is just creating selection Criteria
     newIndex = 0;

    }
    else {
   
    newmaxCode = maxCode;
    }
         var rev_selectTable = 'Revision_'+selectTable;
      var promise= db.get(rev_selectTable).remove({$and:[{org_id:data.orgId,bus_type:data.businessType,itemCode:itemCode}]});
   //because this may be an re-edit on reject item
      promise.on('success',function(doc){
      
     
    async.forEachOf(selectionDatas,function(key1,value1,callback1){
    
     var selectionData = selectionDatas[value1];
      
     //as per Vignesh request new rows to be identified with non presence of isDeleted flag
      if(selectionData.itemDetailCode == '' || typeof(selectionData.itemDetailCode) == 'undefined'){
       maxCodes = newmaxCode.split("-");
            newmaxCode = '';
            var lengthCodes = maxCodes.length; 
               for(idx = 0; idx < lengthCodes-1;idx++)
                 newmaxCode += maxCodes[idx]+'-';
            var newIndex = maxCodes[lengthCodes-1]; //get last part and increment
            newIndex++;
            newmaxCode += newIndex;
             selectionData.isDeleted = 0;
         utils.insertSelectionData(data.orgId,data.businessType,itemCode,rev_selectTable,selectFields,selectionData,newmaxCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
              callback1();
            }
          }
        });
        }
     else {
        if(selectionData.isDeleted == true){
        
         utils.deleteSelectionData(data.orgId,data.businessType,itemCode,rev_selectTable,selectFields,selectionData,selectionData.itemDetailCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
            callback1();
            }
          }
        });
        }
        else {
        utils.insertSelectionData(data.orgId,data.businessType,itemCode,rev_selectTable,selectFields,selectionData,selectionData.itemDetailCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
            callback1();
            }
          }
        });
        }
        }
        },
       function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
      
      else{
          changeItemStatus(req,res,9,data,85,mastFields,mastTable); //85->Ready2Auth
         }
          
      });
      });
       promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);
  });
      
}

function changeItemStatus(req,res,stat,data,itemStat,mastFields,mastTable){
    
    var itemCode = data.itemCode;
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = '';
  
  //NOTE: status change will be on Revision* table main table will
  //continue to be in "InUse" status so as to be rendered from APP
  var revisionTabl = 'Revision_'+data.orgId+'_menuitem_master';
  statutils.changeStatus(revisionTabl,data.orgId,data.businessType,data.userId,data.itemCode,itemStat,reason,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
           else {
            getDetailsData(req,res,9,data,mastFields,mastTable);
            }
        });
     
}

function getDetailsData(req,res,stat,data,mastFields,mastTable){

  var itemCode = data.itemCode;
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = '';
  
  statutils.detailsData(data,mastFields,mastTable,function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
           else {
             data.menuItemDetails = results.menuItemDetails;
            getItemStatName(req,res,9,data);
            }
        });
 

}

function getItemStatName(req,res,stat,data){

    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    var itemMasters = [];
    //create and edit are single item operations
    //while others are bulk operations so this fn changes 
    
     async.forEachOf(data.menuItemDetails,function(key,value,callback){
    if(value == 'status') {
    statId = key;
  statutils.statName(statId,'menuitem_status_master',function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               callback();
             }
           else {
             data.menuItemDetails.status = results;
             callback();
             }
        });
        }
        else
         callback();
         },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  processStatus(req,res,9,data);
                  
              }
            }
          });

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



