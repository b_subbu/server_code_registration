function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   var userdata = "";
   var files = [];
   var idx = 0;
   var jdx = 0;
   var actions = [];
   var formidable = require('formidable');
   
   var form = new formidable.IncomingForm();
   form.uploadDir = app.get('fileuploadfolder')+'/tmp';
   form.multiples = true;
   form.parse(req, function (err, fields) {
     userdata = fields;
    });
    form.on('file',function(name,file) {
    files[idx] = file;
     idx++;
    });
    form.on('end',function() {
       checkMasterStatus(req,res,data,userdata,files);
       debuglog("checkMasterStatus retained as this is multpart post");
    });
 } 


function checkMasterStatus(req,res,data,userdata,files){

    var uid = userdata.userId;
    var itemCode = userdata.itemCode;
    var async = require('async');
    var utils = require('./utils');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    var type = require('type-detect');
    
   
    data.userId = uid;
      if( typeof uid === 'undefined' || uid === 'undefined' || type(uid) !== 'string'){
         processStatus(req,res,6,data); //system error
         return; 
         }
         
      if( typeof itemCode=== 'undefined' || itemCode === 'undefined' || type(itemCode) !== 'string'){
         processStatus(req,res,6,data); //system error
         return; 
         }
         
    data.itemCode = itemCode;
    
  async.parallel({
    usrStat: function(callback){roleutils.usrStatus(uid,callback); },
    },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    if(isError == 0 && results.usrStat.errorCode != 0) {
      processStatus (req,res,results.usrStat.errorCode,data);
      isError = 1;
      return;
    }
    if( isError == 0 && results.usrStat.userStatus != 9) {
      processStatus (req,res,2,data);
      isError = 1;
      return;
    }
    if (isError == 0) {
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              checkItemCode(req,res,9,data,userdata,files);
            }
          }); 
}


function checkItemCode(req,res,stat,data,userdata,files) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   
   var itemCode = data.itemCode;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   data.itemCode = itemCode;
  
     async.parallel({
         detailStat: function(callback){utils.checkItemCode(orgId,bizType,itemCode,callback); },
         },
         function(err,results) {
          if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
             if(isError == 0 && (results.detailStat.errorCode != 0 && results.detailStat.errorCode != 5)) {
              processStatus (req,res,results.detailStat.errorCode,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
                mastFields = results.detailStat.mastFields;
                mastTable = results.detailStat.tblName;
                getMaxImgIndex(req,res,9,data,userdata,files,mastTable);
            }
          }); 
} 

//Will try to get maximum file index using fs 
//i.e., read from directory instead of trying to get from DB
//because this is faster and more reliable
//using sync because program can't proceed without getting max index
//also we can reduce one more function call 
//use async here if any performance issue comes

function getMaxImgIndex(req,res,stat,data,userdata,files,mastTable){

          var imgDir = app.get('fileuploadfolder')+data.userId+'/'+data.itemCode+'/';
          var fs = require('fs');
          var visualIndexArr = [];
          var async=require('async');
          fs.stat(imgDir,function(err,stats){
          if(err){
            var maxIndex = 0; //directory does not exist 
           saveFiles(req,res,9,data,userdata,files,mastTable,maxIndex);
   
          }
          else{ 
          
          var visualFiles = fs.readdirSync(imgDir);
          if(visualFiles.length > 0){
          
          //Note all filenames will be in format ItemCode+"_"+IMAGE+"_"+Index
          //so we take the second item and sort it to get the maximum index from directory
          async.forEachOf(visualFiles,function(key,value,callback){
                 
                var filenameArray= key.split("_");
                var arraylength = filenameArray.length-1;
                 visualIndexArr.push(filenameArray[arraylength]);
                callback();
               },
        function(err){
          if(err){
                processStatus (req,res,6,data);
                return;         
              }
           else {
           var maxIndex = Math.max.apply(null,visualIndexArr);
           saveFiles(req,res,9,data,userdata,files,mastTable,maxIndex);
           }
      });
      }
      else {
          var maxIndex = 0;//empty directory
           saveFiles(req,res,9,data,userdata,files,mastTable,maxIndex);
      
      }
      }
    });   
       
          
}
//first we will save the files with IMAGE_0...
//then we will replace the files in userfields with src
//and then sort as per sequence and save the array

function saveFiles(req,res,stat,data,userdata,files,master_table,maxIndex) {

    var filesArray = [];
    var fs = require('fs');
    var mkdirp = require('mkdirp');
    var async = require('async');
    var newfolder = app.get('fileuploadfolder')+data.userId+'/'+encodeURIComponent(data.itemCode)+'/';
    mkdirp(newfolder,function(err,made){
     //no need to handle this error generally does not happen
     //if happens it is some severe system error and allow to crash
 
    async.forEachOf(files,function(key,value,callback){
      var imgIdx = ++maxIndex;
      var oldfile = key.path;
      var oldfilename = key.name;
      var newfile = newfolder+encodeURIComponent(userdata.itemCode)+"_IMAGE_"+imgIdx;
 
      if(err) { 
       console.log(err);
       fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove file failed");
             console.log(err);
          }
          callback();
         });
       }
     else if(files[value].size == 0) {
        fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove empty file failed");
             console.log(err);
          }
          callback();
         });
       }
      else {
        var tmpArray = {};
        var newfilename = '/upload_files/'+data.userId+'/'+encodeURIComponent(data.itemCode)+'/'+encodeURIComponent(data.itemCode)+"_IMAGE_"+imgIdx;
        
        tmpArray[oldfilename]= newfilename;
        
        filesArray.push(tmpArray);
        fs.rename(oldfile,newfile,function(err) {
         if(err) {
               console.log("Moving file failed");
               console.log(err);
           }
        debuglog(oldfile);
        debuglog(newfile);   
        callback();
      });
     }
    },
   function(err){
      if(err)
       { 
       proessStatus(req,res,6,data);
       return;
       }
      else {
          sortVisualField(req,res,9,data,userdata,filesArray,master_table);
          }
     });
   });
}


//We have to sort the selection criteria field as per order of detail fields
//because this order is important while rendering 
//also we can't get a sorted array above since can use fields only at final level
function sortVisualField(req,res,stat,data,userdata,filesArray,master_table) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
  
   var visualsArray = userdata.visuals; 
   

   var visualArray = JSON.parse(visualsArray);
  
   //console.log(visualArray);
   
   var key_sequence = 0;
   visualArray.sort(function(a,b) {return a.sequence-b.sequence});
         async.forEachOf(visualArray,function(key1,value1,callback1){
           async.forEachOf(filesArray,function(key2,value2,callback2){
               async.forEachOf(key2,function(key3,value3,callback3){
                     if(value3 == key1.file)  {
                    key1.src = key3;
                     callback3();
                    }
               else
                 callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
            callback2(); 
           }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
             callback1(); 
           }
        });
      },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
            generateFinalArray(req,res,stat,data,userdata,visualArray,master_table);
        }
      });
} 

//copy filesArray to a finalArray, just ignore isChanged and isDeleted flags when copying
//if isDeleted = 1  then donot copy that row and also remove that src if it is a file type
function generateFinalArray(req,res,stat,data,userdata,visualArray,master_table) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var finalArray = [];
  
   
        async.forEachOf(visualArray,function(key1,value1,callback1){
                   if(key1.isDeleted == 0 || typeof(key1.isDeleted) == 'undefined')  {
                     var tmpArray = {};
                     tmpArray.type = key1.type;
                     tmpArray.file = key1.file;
                     tmpArray.src  = key1.src;
                     tmpArray.sequence = key1.sequence;
                     tmpArray.description = key1.description;
                     tmpArray.size = key1.size;
                     tmpArray.isDeleted = 0;
                     finalArray.push(tmpArray);
                     callback1();
                    }
               else{
                var rimraf = require('rimraf');
                var fileUrl = key1.src;
                var file = key1.file;
                var fileUrlArr = fileUrl.split("/");
                if(fileUrlArr.length >= 4){
                //generally only filenames will have more than 4 "/" video urls only 3 or less
                //but if a video url has 5 slashes just rimraf will fail and no errors
                var lastElement = fileUrlArr.length-1;
                var fileName = app.get('fileuploadfolder')+data.userId+'/'+data.itemCode+'/'+fileUrlArr[lastElement];
                //console.log(fileName);
                //we cant directly use filename since it will change during save
                 //remove files only for images not video urls
                 rimraf(fileName,function(err,results){
                    if(err) console.log(err);});
                 }
                 callback1();
              }
                
        },
      function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         
              }
      else {
           finalArray.sort(function(a,b) {return a.sequence-b.sequence});
           //just sort again in case if any ordering changes while copying
           updateVisualField(req,res,stat,data,userdata,finalArray,master_table);
        }
      });
} 

function updateVisualField(req,res,stat,data,userdata,visualArray,master_table){

   var utils = require('./utils');
   var async = require('async');
   var org_id = data.orgId;
   var biz_type = data.businessType;
   var itemCode = data.itemCode;
   var isError = 0;
      
   var query = {};
   query['itemVisual'] = visualArray;
    //query.update_time = new Date(); may be not required to update time for image update
      var promise = db.get(master_table).update(
        {
        org_id: org_id,
        bus_type: biz_type,
        itemCode: itemCode
        },
        {$set: query}
        );
      promise.on('success',function(doc){
      processStatus(req,res,9,data);
      });
      promise.on('error',function(err){
      processStatus(req,res,6,data);
    });
}
    
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;




