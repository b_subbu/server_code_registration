function processRequest(req,res,data){
   if( typeof req.body.menuCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMenuStatus(req,res,9,data);
}


function checkMenuStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    data.menuCodes = menuArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          menuStat: function (callback){utils.checkMenuCode(tabl_name,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
           if(isError == 0 && (results.menuStat.errorCode != 0 && results.menuStat.errorCode != 5)) {
             isError = 1;
             errorCode = results.menuStat.errorCode;
             callback1();
           }
          if( isError == 0 && (results.menuStat.itemStat != 82 && results.menuStat.itemStat != 88) ) {
             errorCode = 5;
             isError = 1;
             callback1();
           }
           if(isError == 0)  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
              processStatus(req,res,6,data);
             
             }
      else
          {
           changeMenuStatus(req,res,9,data,tabl_name); 
         }
     });
}


function changeMenuStatus(req,res,stat,data,tabl_name){
    
    var menuArr = data.menuCodes;
    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = ''; //no reason is currently set for Release operation
    
     
     async.forEachOf(menuArr,function(value,key,callback1){
       var menuCode = value;
       async.parallel({
         chngStatus: function(callback){statutils.changeMenuStatus(tabl_name,data.orgId,data.businessType,data.userId,menuCode,84,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           else
             callback1();
          });
         },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
           getItemsData(req,res,9,data);
          }
     });
}

function getItemsData(req,res,stat,data){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
    
     async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
     async.parallel({
         linkData: function(callback){utils.menuLinksWithNodeIs(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           if (isError == 0 && results.linkData.errorCode == 0) {
           debuglog(results.linkData);
           debuglog("This can contain either item or group we will do corresponding processing ");
           debuglog("in next function");
            itemsArr.push(results.linkData.itemCodes);
             callback1();
            }
          else
           callback1(); //Sometimes delete seems to happen before insert-->TODO:check
         }); 
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            changeItemStatus(req,res,9,data,itemsArr);
          }
     });
}

//Note this is a special function not generic item status change
//Change status to Linked only if it is Entered
//dont change status if it is InUse may be due to other menus

function changeItemStatus(req,res,stat,data,itemsArr){
    
    var async = require('async');
    var itemUtils = require('./utils');
    var groupUtils = require('../Groups/dataUtils');
    var isError = 0;
    
    var reason = 'Menu Approval changed Status'; 
    
    var item_tabl_name = data.orgId+"_menuitem_master";
    var group_tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr[0],function(key1,value1,callback1){
     debuglog("starting processing of items arr to change status");
     debuglog(key1);
     
       if(key1.nodeIs == 'item'){
          debuglog("This is an item");
          debuglog(key1);
          debuglog("we will move the item to InUse status");
          debuglog("and increment used counter by 1");
          debuglog("but dont increment linked counter it may or may not be incremented");
          debuglog("depending on how this item was added to menu");
          debuglog("need to have seperate counters and manipualte them seperately");
          debuglog("since it is possible to directly add and remove from Released Menu");
          itemUtils.moveItemStatus(item_tabl_name,data.orgId,key1.itemCode,'forward',84,0,1,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
         }
          
    if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          debuglog("we will move the group to InUse status");
          debuglog("and increment used counter by 1");
          debuglog("but dont increment linked counter");
          debuglog("Because in normal flow linked counter would have been incremented on Authorise");
          debuglog("But in Released and edited flow link counter will not be incremented only used counter");
          debuglog("This is an anamoly difficult to fix but wont possibly have a business impact");
          groupUtils.moveGroupStatus(group_tabl_name,data.orgId,key1.itemCode,'forward',84,0,1,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
        }
       },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
            createReleaseData(req,res,9,data,tabl_name);
          }
    });
}

function createReleaseData(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    data.menuCodes = menuArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file
    var release_tabl_name = orgId+'_release_menus_master';

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          relStat: function (callback){utils.releaseData(tabl_name,release_tabl_name,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else{
           getMenuDetails(req,res,9,data,tabl_name); 
         }
     });
}

function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    async.forEachOf(data.menuCodes,function(key,value,callback){
    var menuCode = key;
    utils.menuDetail(mastTable,data.orgId,data.businessType,menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              isError =1 ;
              callback();
              }
      else  {
            resultArr.push(result.masterData);
            callback();
           }
    });
    },
    function(err){
         if(err || isError == 1) 
               processStatus(req,res,6,data);
       else
          {
          data.menuData = resultArr;
          processStatus(req,res,9,data); 
         }
     });
    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
exports.changeMenuStatus = changeMenuStatus;
      




