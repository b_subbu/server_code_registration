function getItemCodes(req,res,stat,data,mastData,menuData,tabId,itemStat) {

  var tabl_name = 'Revision_'+data.orgId+'_menus_master'; //TODO: move to a common definition file
 
    var async = require('async');
   var utils = require('./menuutils');
   var itemCodes = [];
   var groupCodes = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    var menuLinks = menuData.menuLinkage;
     async.forEachOf(menuLinks,function(key1,value1,callback1){
      delete key1.__uiNodeId;
      if(key1.isLeaf == true){
          if(key1.nodeIs == 'item'){
          debuglog("This is a item");
          debuglog(key1);
           itemCodes.push(key1.linkId);
           
          }
          if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          groupCodes.push(key1.linkId);
          }
       debuglog(key1);
       callback1();
       }
      else{
       debuglog("Not a leaf node may be link nodes");
       debuglog(key1);
       callback1();
       }
    },
   function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
     else {
          debuglog("start checking item codes and group codes");
          debuglog(itemCodes);
          debuglog(groupCodes);
          checkItemCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name,itemStat);
         }
      });
} 

function checkItemCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name,menuStat) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 7; //item not found
              isError = 1;
              }
            if( isError == 0 && (result.itemStatus != 80 && result.itemStatus != 83 && result.itemStatus != 84)) {
              errorCode = 7; //item not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                checkGroupCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name,menuStat);
              }
            }
          });
} 

function checkGroupCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name,menuStat) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   debuglog("Note business type checks are done away with");
   debuglog("TODO do away with orgId check since each data is in its own organziation table");
   
   var grp_tabl_name = orgId+'_group_header';
   async.forEachOf(groupCodes,function(key,value,callback){
      groupCode = key;
     utils.checkGroupCode(grp_tabl_name,orgId,groupCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 8; //Group not found
              isError = 1;
              }
            if( isError == 0 && (result.groupStatus != 80 && result.groupStatus != 83 && result.groupStatus != 84)) {
              errorCode = 8; //Group not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                createMenuData(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name,menuStat);
              }
            }
          });
} 

function createMenuData(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name,menuStat) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    var menuCode = mastData.id;
   
    utils.createMenuMaster(tabl_name,orgId,bizType,menuCode,mastData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
              createMenuLinks(req,res,9,data,menuCode,itemCodes,groupCodes,tabl_name,menuStat);
            }
        });
} 

function createMenuLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,mastTable,menuStat) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
    //var tabl_name = orgId+'_menus_nodes' 
    //just save links as array in master table 
    var linkData = req.body.menuData;
   
    utils.createMenuNodes(mastTable,orgId,bizType,menuCode,linkData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
              createMenuItemLinks(req,res,9,data,menuCode,itemCodes,groupCodes,mastTable,menuStat);
            }
        });
} 

function createMenuItemLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,mastTable,menuStat) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var link_tabl_name = 'Revision_'+data.orgId+'_menus_items_link'; //TODO: move to a common definition file
  
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.linkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,itemCode,'item',function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
            callback();
            }
        });
      },
      function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else
        createMenuGroupLinks(req,res,9,data,menuCode,itemCodes,groupCodes,mastTable,menuStat);
     });
} 

function createMenuGroupLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,mastTable,menuStat) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var link_tabl_name = 'Revision_'+data.orgId+'_menus_items_link'; //TODO: move to a common definition file
 
  debuglog("same function and table as menu item link");
  debuglog("but nodeIs param is changed to group");
  debuglog(groupCodes);
  
   async.forEachOf(groupCodes,function(key,value,callback){
      groupCode = key;
     utils.linkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,groupCode,'group',function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
            callback();
            }
        });
      },
      function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else
          {
            if(menuStat == 84) 
              changeMainStatus(req,res,9,data,menuCode,mastTable)
           else{
             if(menuStat == 86){
             debuglog("This is a edit on Recalled Menu");
             debuglog("so let us first move all the items and groups to active Status");
             debuglog("before deleting from Main table");
             debuglog("Authorise will move them back to Linked Status");
             debuglog("Else the Linked items/groups will remain asis");
             getItemStatus(req,res,9,data,menuCode,mastTable);
              }
            else
               removeMainData(req,res,9,data,menuCode,mastTable); //Ready2Auth
             }
 
        }       
    });
} 

function changeMainStatus(req,res,stat,data,menuCode,mastTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
   
    var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
    var menuStat = 89; //ReleasedAndEdited
   
    var reason = '';
       statutils.changeMenuStatus(tabl_name,data.orgId,data.businessType,data.userId,menuCode,menuStat,reason,function(err,result) {
          if(err) {
                     processStatus(req,res,6,data);
                }
           else {
                 data.menuCode = menuCode;
                changeMenuStatus(req,res,9,data,menuCode,88,mastTable); //ReleasedAndSubmitted
          
              }
          });
     
}

function getItemStatus(req,res,stat,data,menuCode,mastTable){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
    
  
     async.parallel({
         linkData: function(callback){utils.menuLinksWithNodeIs(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
            }
          else{
           debuglog(results.linkData);
           debuglog("This data is obtained from the main link table");
           debuglog("We will temporarily move all the items and groups");
           debuglog("from InUse status back and decrement their linked and used count");
           debuglog("Then again the release operation will increment it and ");
           debuglog("move it back to InUse status");
           debuglog("This seems to be much faster than checking 2 arrays and doing processing");
             itemsArr.push(results.linkData.itemCodes);
             changeMainItemStatus(req,res,9,data,itemsArr,menuCode,mastTable);
          }
     });
}

function changeMainItemStatus(req,res,stat,data,itemsArr,menuCode,mastTable){
    
    var async = require('async');
    var itemUtils = require('./utils');
    var groupUtils = require('../Groups/dataUtils');
    var isError = 0;
    
    var reason = 'Menu Edit changed Status'; 
    
    var item_tabl_name = data.orgId+"_menuitem_master";
    var group_tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr[0],function(key1,value1,callback1){
     debuglog("starting processing of items arr to change status");
     debuglog(key1);
     
       if(key1.nodeIs == 'item'){
          debuglog("This is an item");
          debuglog(key1);
          debuglog("we will move the item to Active status temporarily");
          debuglog("and decrement used and linked counter by 1");
          debuglog("this is to take care of Recalled Menu addition removal of items");
          itemUtils.moveItemStatus(item_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,1,function(err,results){
          if(err){
           debuglog("move reverse error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move reverse error");
           callback1();
          }
          });
         }
          
    if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          debuglog("we will move the group to Active status");
          debuglog("and decrement linked and used counter by 1");
          groupUtils.moveGroupStatus(group_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,1,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
        }
       },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
           removeMainData(req,res,9,data,menuCode,mastTable);
          }
    });
}


function removeMainData(req,res,stat,data,menuCode,mastTable){
    var async = require('async');
 
    var menuutils = require('./menuutils');
    var isError = 0;
   
    var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
    
    var reason = '';
       menuutils.deleteMenu(tabl_name,data.orgId,data.businessType,menuCode,function(err,result) {
          if(err) {
                     processStatus(req,res,6,data);
                }
           else {
                 data.menuCode = menuCode;
                changeMenuStatus(req,res,9,data,menuCode,81,mastTable); //ReleasedAndSubmitted
          
              }
          });
     
}

function changeMenuStatus(req,res,stat,data,menuCode,menuStat,mastTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
    
   
    var reason = '';
       statutils.changeMenuStatus(mastTable,data.orgId,data.businessType,data.userId,menuCode,menuStat,reason,function(err,result) {
          if(err) {
                     processStatus(req,res,6,data);
                }
           else {
                 data.menuCode = menuCode;
                 getMenuDetails(req,res,9,data,mastTable);
          
              }
          });
     
}

function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    utils.menuDetail(mastTable,data.orgId,data.businessType,data.menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else  {
            data.menuData = result.masterData;
            processStatus(req,res,9,data);
           }
    });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = getItemCodes;



