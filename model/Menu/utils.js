function getMenuMasterFields(orgId,bizType,callback){
   var totalRows = 0;
   var data={};
   var resultArr = [];
  
   var promise = db.get('org_itemmaster_map').find({$and:[{org_id:orgId,bus_type:bizType}]});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {name: doc.field_name, label:doc.field_label,isMandatory: doc.mandatory};
        resultArr.push(tmpArr);
        }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         getMenuMasterFromBizType(orgId,bizType,callback)  
        }
        else {
          data.masterFields = resultArr;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getMenuMasterFromBizType(orgId,bizType,callback){
   var totalRows = 0;
   var resultArr = [];
   var data = {};
  
   var promise = db.get('businesstype_itemsmap_master').find({$and:[{bus_type:bizType,header_field:true}]},{sort: {field_order: 1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {name: doc.field_name,label: doc.field_label,isMandatory: doc.mandatory,sequence:doc.field_order};
        resultArr.push(tmpArr);
        }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
           callback(null,3);
       }
       else {
           data.masterFields = resultArr;
           callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function getMenuMasterFromOrgId(orgId,bizType,callback){
   var totalRows = 0;
   var data={};
   var resultArr = [];
   var master_table = ''
  
   var promise = db.get('org_itemmaster_map').find({$and:[{org_id:orgId,bus_type:bizType}]},{sort: {field_order: 1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {name: doc.field_name,label:doc.field_label,isMandatory: doc.mandatory};
        master_table = doc.table_name_master;
        resultArr.push(tmpArr);
        }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         //callback(null,3); //any number is fine as long as not array or obj
         //if no entry then create menu master for org id from the biz type
         createMenuMasterFromBizType(orgId,bizType,callback);  
        }
        else {
          data.masterFields = resultArr;
          data.masterTable = master_table;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function createMenuMasterFromBizType(orgId,bizType,callback){

   var async = require('async');
   var type = require('type-detect');
  
  
   var bizData = [];
   var data = {};
  
    
    getMenuMasterFromBizType(orgId,bizType,function(err,result) {
          if(err) callback(6,data);
         else {
              if(type(result) == 'array' || type(result) === 'object') {
                  mastFields = result.masterFields;
                  createMenuItemMasterFields(orgId,bizType,mastFields,callback);      
                   }
            else  {
                    data.errorCode = 3; //No definition for bizType itself
                    callback(null,data);
                  }
            }
      });
}

function createMenuItemMasterFields(orgId,bizType,masterFields,callback){ 
  var async = require('async');
  var table_name_mast = orgId+'_menuitem_master';
  var data = {};
 
 //note data is not in normalized form
 //this is to speed up retrieval in mongo
 async.forEachOf(masterFields,function(key,value,callback1){
   var promise = db.get('org_itemmaster_map').insert({
    org_id: orgId,
    bus_type: bizType,
    table_name_master: table_name_mast,
    field_name: key.name,
    field_label: key.label,
    field_order: key.sequence,
    mandatory: key.isMandatory,
   });
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           //createCollection(table_name_mast,table_name_det,callback);
           data.errorCode = 0;
           data.masterFields = masterFields;
           data.masterTable = table_name_mast;
           callback(null,data);
          }
     });
 
}

function deleteMenuItemMasterFields(orgId,bizType,masterFields,callback){ 
  var async = require('async');
  async.forEachOf(masterFields,function(key,value,callback1){
   var promise = db.get('org_itemmaster_map').remove({
    org_id: orgId,
    bus_type: bizType,
    field_name: key
   });
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
            callback(null,null);
           }
  });
 
}
function checkMenuItemCodeExists(orgId,bizType,itemCode,callback){

   var async = require('async');
   var type = require('type-detect');
  
   var bizData = [];
   var data = {};
  
    
    getMenuMasterFromOrgId(orgId,bizType,function(err,result) {
          if(err) callback(6,data);
         else {
              if(type(result) == 'array' || type(result) === 'object') {
                   
                   tablName = result.masterTable;
                   masterFields = result.masterFields;
                  checkMenuItemCode(tablName,orgId,bizType,itemCode,masterFields,callback);      
                   }
            else  {
                    data.errorCode = 6; //should not happen trying to create detail without master
                    callback(null,data);
                  }
            }
      });
}

function checkMenuItemCode(tablName,orgId,bizType,itemCode,masterFields,callback){
   var totalRows = 0;
   var data={};
   var itemStatus = '';
   var promise = db.get(tablName).find({$and:[{org_id:orgId,bus_type:bizType,itemCode:itemCode}]});
   promise.each(function(doc){
        itemStatus = doc.status_id;
        totalRows++;
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 3; //item code not found
         data.tblName = tablName;
         data.itemCode = itemCode;
         data.mastFields = masterFields;
         callback(null,data);   
        }
        else {
          data.errorCode = 5; //itemCode found
          data.tblName = tablName;
          data.itemCode = itemCode;
          data.mastFields = masterFields;
          data.itemStatus = itemStatus;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getMenuSelectionFromBizType(bizType,callback){
   var totalRows = 0;
   var resultArr = [];
   var data = {};
  
   var promise = db.get('businesstype_itemsmap_master').find({$and:[{bus_type:bizType,header_field:false}]},{sort: {field_order:1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {name: doc.field_name,label: doc.field_label,isMandatory: doc.mandatory,order:doc.field_order,type:doc.field_type,editable:doc.field_editable,required:doc.field_required,value:doc.field_value};
        resultArr.push(tmpArr);
        }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
           data.errorCode = 0; //no details definition is not an error
           callback(null,data);
       }
       else {
           data.errorCode = 0;
           data.detailFields = resultArr;
           callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function getMenuItemSelectionsFromOrgId(orgId,bizType,itemCode,callback){
   var totalRows = 0;
   var data={};
   var resultArr = [];
   var table_name = '';
  
   var promise = db.get('org_itemdetail_map').find({$and:[{org_id:orgId,bus_type:bizType,itemCode:itemCode}]},{sort: {field_order:1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {name: doc.field_name,label:doc.field_label,isMandatory: doc.mandatory,sequence:doc.field_order,type:doc.field_type,additional:doc.field_additional_type};
            tmpArr.visibility= doc.field_visibility;
            tmpArr.width= doc.field_width;
            tmpArr.purpose=  doc.field_purpose;
            tmpArr.editable = doc.field_editable;
            tmpArr.required = doc.field_required;
            tmpArr.value    = doc.field_defaultvalue;
        table_name = doc.table_name_det;
        resultArr.push(tmpArr);
        }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         callback(null,3); //any number is fine as long as not array or obj  
        }
        else {
          data.selectFields = resultArr;
          data.table_name_detail = table_name;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function createMenuItemSelectionFields(orgId,bizType,itemCode,detailFields,callback){ 
  var async = require('async');
  var table_name_det  = orgId+'_menuitem_detail';
  var fieldOrder = 1; //because 1 will be itemDetailCode always
  
  
 //note data is not in normalized form
 //this is to speed up retrieval in mongo
 async.forEachOf(detailFields,function(key,value,callback1){
     fieldOrder++;
   var promise = db.get('org_itemdetail_map').insert({
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode,
    table_name_det: table_name_det,
    field_name: key.name,
    field_label: key.label,
    field_type: key.typeId,
    field_additional_type: key.additionalTypeId,
    field_visibility: key.visibility,
    field_width: key.width,
    field_purpose: key.purpose,
    field_order: fieldOrder,
    field_editable: key.editable,
    field_required: key.required,
    mandatory: false,
   });
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           debuglog("This may not be required as now the master fields are rendered in create and edit screens");
           
           createMenuItemSelectionFieldsFromBizType(orgId,bizType,itemCode,fieldOrder,callback);
          }
     });
 
}

function createMenuItemSelectionFieldsFromBizType(orgId,bizType,itemCode,fieldOrder,callback){ 
  var async = require('async');
  var table_name_det  = orgId+'_menuitem_detail';
  var selectFields = {};
 
     getMenuSelectionFromBizType(bizType,function(err,result){
     if(err)
        callback(6,null);
     else{
        selectFields = result.detailFields;
        createMenuItemSelectionFieldsBiz(orgId,bizType,itemCode,selectFields,fieldOrder,callback);
        }
    });
  
}

function createMenuItemSelectionFieldsBiz(orgId,bizType,itemCode,detailFields,maxfieldOrder,callback){ 
  var async = require('async');
  var table_name_det  = orgId+'_menuitem_detail';
  var data = {};
  
 //note data is not in normalized form
 //this is to speed up retrieval in mongo
 async.forEachOf(detailFields,function(key,value,callback1){
  //TODO move these to master definitions if possible
   switch(key.name){
     case 'itemDetailCode':
       fieldVisible = 2;
       fieldEditable = false;
       fieldRequired = true;
       fieldValue = null;
       //fieldOrder = 1;
       break;
     case 'units':
       fieldVisible = 2;
       fieldEditable = false;
       fieldRequired = true;
       fieldValue = 1;
       break;
     case 'stock':
       fieldVisible = 2;
       fieldEditable = true;
       fieldRequired = false;
       fieldValue = null;
       break;
    case 'price':
       fieldVisible = 2;
       fieldEditable = true;
       fieldRequired = true;
       fieldValue = null;
       break;
    }
     var promise = db.get('org_itemdetail_map').update({
   
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode,
    table_name_det: table_name_det,
    field_name: key.name},
    {$set: {
    field_label: key.label,
    field_visibility: fieldVisible,
    field_required: fieldRequired,
    field_editable: fieldEditable,
    field_defaultvalue: fieldValue,
    mandatory: key.isMandatory,
   }},
   {upsert: true}
   );
  promise.on('success',function(doc){
    callback1();
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           data.selectFields = detailFields;
           data.table_name = table_name_det;
           callback(null,data);
          }
     });
 
}


function deleteMenuItemDetailFields(orgId,bizType,itemCode,detailFields,callback){ 
  var async = require('async');
  async.forEachOf(detailFields,function(key,value,callback1){
   var promise = db.get('org_itemdetail_map').remove({
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode,
    field_name: key
   });
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
            callback(null,null);
           }
  });
 
}

function deleteMenuItemMasterData(userId,orgId,bizType,itemCode,table_name,callback){ 

  var rimraf = require('rimraf');
   var promise = db.get(table_name).remove({
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode
   });
  promise.on('success',function(doc){
    var selection_table_name = orgId+"_menuitem_detail"; 
    //if table name convention changes then change here
    //this is to avoid one more query for getting detail table name
    var imageDir = app.get('fileuploadfolder')+userId+'/'+itemCode;
    db.get(selection_table_name).remove({org_id:orgId,bus_type:bizType,itemCode:itemCode});
    db.get('org_itemdetail_map').remove({org_id:orgId,bus_type:bizType,itemCode:itemCode});
    rimraf(imageDir,function(err,results){
    if(err) console.log(err);
    });
    //we are calling both async and not waiting for callback
    //This is because even if they fail, there is no impact to user side
    //just the data may remain in the tables/upload_folder orphaned 
    
    callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
   //**************
   //*****TODO: DELETE   FROM MENUITEMDETAILS TABLE FOR THE ITEM
   //*********************************
}

function getMenuMasterData(orgId,bizType,itemCode,table_name,callback){ 
var data = {};
    var promise = db.get(table_name).find({
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode
   }, {sort: {update_time: -1}});
  promise.on('success',function(doc){
    data.masterData = doc;
    callback(null,data);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function getMenuItemMasterCodes(tablName,orgId,bizType,pgid,size,searchArr,callback){
   var totalRows = 0;
   var data={};
   var resultArr = [];
   var page = parseInt(pgid);
   var skip = page > 0 ? ((page - 1) * size) : 0;
    
   //var promise = db.get(tablName).find({$and:[{org_id:orgId},{bus_type:bizType},{status_id: {$in: statList}}]},{sort:{update_time: -1},limit: size,skip: skip});
   var promise = db.get(tablName).find({$and:searchArr},{sort:{update_time: -1},limit: size,skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        resultArr.push(doc.itemCode); //note: itemCode field is fixed in name
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 1; //no item codes for this business 
         callback(null,data);   
        }
        else {
          data.errorCode = 0;
          data.tblName = tablName;
          data.itemCodes = resultArr;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}



function getMenuItemMasterCount(orgId,bizType,tablName,sarr,callback){
 
  var totalRows =0;
  var resultArr = [];
  var resultArray = [];
  var finalArray = [];
  var data = {};
  
  //note: we are taking count for the list of all org ids here 
  //but may need to include biztype if one org can have multiple biz types  
   
  var promise = db.get(tablName).count({$and:sarr});
   promise.on('complete',function(err,doc){
       data.errorCode = 0;
       data.countItems = doc;
       callback(null,data);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}


function getMenuItemSelectionsData(orgId,bizType,itemCode,table_name,callback){ 
var data = {};
var resultArray = [];
var totalRows = 0;
 //here sort by updatetime may not be required I think just adding for consistency
    var promise = db.get(table_name).find({
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode
   },{sort: {update_time: -1}});
  promise.each(function(doc){
   totalRows++;
   
     var tmpArray = doc;
     resultArray.push(tmpArray);
       }); 
    
  promise.on('success',function(doc){
    data.selectionData = resultArray;
    callback(null,data);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function createMenuItemDetailCode(orgId,bizType,tableName,masterFields,detailsData,min,max,callback){ 
  var async = require('async');
 
 
  var promise = db.get(tableName).insert({
    org_id: orgId,
    bus_type: bizType,
    itemCode: detailsData.itemCode
    //note: itemCode is required and present in all org ids
   });
  promise.on('success',function(doc){
    createMenuItemDetailData(orgId,bizType,tableName,masterFields,detailsData,min,max,callback);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 } 
 
function createMenuItemDetailData(orgId,bizType,tableName,masterFields,detailsData,minPrice,maxPrice,callback){
 var async = require('async');
 var data = {};
 

 async.forEachOf(masterFields,function(key,value,callback1){
       var query = {};
       var keyId = key.name;
       if(keyId == 'itemCode')
          callback1();
       else{
       if(keyId == 'itemPrice'){
          if(minPrice == maxPrice)
            keyValue = minPrice;
          else  
            keyValue = minPrice+'-'+maxPrice;
          }
       else
          keyValue = detailsData[keyId];
       query[keyId] = keyValue;
       query.update_time = new Date();
       query.sellerIds = detailsData.sellerIds;
       
       debuglog("since sellerIds is not part of master fields defintion");
       debuglog("Update it manually every time");
      
   var promise = db.get(tableName).update(
   {
    org_id: orgId,
    bus_type: bizType,
    itemCode: detailsData.itemCode
    },
    {$set: query}
   );
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  }
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           //createCollection(table_name_mast,table_name_det,callback);
           data.errorCode = 0;
           callback(null,data);
          }
     });
 
}


function createMenuItemSelectionCode(orgId,bizType,itemCode,tableName,selectionFields,selectionData,callback){
 var async = require('async');
 var data = {};
 async.forEachOf(selectionData,function(key,value,callback1){
 var itemDetailCode = itemCode+"-"+value;
     
   var promise = db.get(tableName).insert(
   {
    org_id: orgId,
    bus_type: bizType,
    itemCode:itemCode,
    itemDetailCode: itemDetailCode,
    
    }
   );
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           createMenuItemSelectionData(orgId,bizType,itemCode,tableName,selectionFields,selectionData,callback)
          }
     });
 
}

function createMenuItemSelectionData(orgId,bizType,itemCode,tableName,selectionFields,selectionData,callback){
 var async = require('async');
 var data = {};

 //no need to check for selectionFields since order of data will be same
 //as the order sent initially re-ordering of grid is not implemented
 async.forEachOf(selectionData,function(key1,value1,callback1){
    var itemDetailCode = itemCode+"-"+value1;
    key1.isDeleted = 0;

  async.forEachOf(key1,function(key2,value2,callback2){
       var query = {};
       var keyId = value2;
       keyValue = key2;
         if(keyId == 'itemDetailCode'){
       callback2();
       }
      else {
       query[keyId] = keyValue;
       query.update_time = new Date();
    var promise = db.get(tableName).update(
   {
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode,
    itemDetailCode: itemDetailCode
    },
    {$set: query}
   );
  promise.on('success',function(doc){
    callback2();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  }
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           
           callback1();
          }
     });
 },
  function(err){
      if(err)
        callback(6,null);
      else {
           data.errorCode = 0;
           callback(null,data);
          }
     });
 
}

function insertMenuItemSelectionData(orgId,bizType,itemCode,tableName,selectionFields,selectionData,maxCode,callback){
 var async = require('async');
 var data = {};
 var itemDetailCode = maxCode;
      
   var promise = db.get(tableName).insert(
   {
    org_id: orgId,
    bus_type: bizType,
    itemCode:itemCode,
    itemDetailCode: itemDetailCode,
    
    }
   );
  promise.on('success',function(doc){
  updateMenuItemSelectionData(orgId,bizType,itemCode,tableName,selectionFields,selectionData,itemDetailCode,callback)
 
 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function updateMenuItemSelectionData(orgId,bizType,itemCode,tableName,selectionFields,selectionData,itemDetailCode,callback){
 var async = require('async');
 var data = {};
//no need to check for selectionFields since order of data will be same
 //as the order sent initially re-ordering of grid is not implemented
 async.forEachOf(selectionData,function(key1,value1,callback1){
       var query = {};
       var keyId = value1;
       keyValue = key1;
       query[keyId] = keyValue;
       query.update_time = new Date();
        if(keyId == 'itemDetailCode'){
       callback1();
       }
       else{
    var promise = db.get(tableName).update(
   {
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode,
    itemDetailCode: itemDetailCode
    },
    {$set: query}
   );
  promise.on('success',function(doc){
    callback1();

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  }
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           data.errorCode = 0;
           callback(null,data);
          }
     });
 
}

function deleteMenuItemSelectionData(orgId,bizType,itemCode,tableName,selectionFields,selectionData,itemDetailCode,callback){
 var async = require('async');
 var data = {};

 //no need to check for selectionFields since order of data will be same
 //as the order sent initially re-ordering of grid is not implemented
     var promise = db.get(tableName).remove(
   {
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode,
    itemDetailCode: itemDetailCode
    });
  promise.on('success',function(doc){
    data.errorCode = 0;
    callback(null,data);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function checkRevisedMenuItemCodeExists(orgId,bizType,itemCode,callback){

   var async = require('async');
   var type = require('type-detect');
  
   var bizData = [];
   var data = {};
  
    
    getMenuMasterFromOrgId(orgId,bizType,function(err,result) {
          if(err) callback(6,data);
         else {
              if(type(result) == 'array' || type(result) === 'object') {
                   
                   tablName = result.masterTable;
                   masterFields = result.masterFields;
                  checkRevisedMenuItemCode(tablName,orgId,bizType,itemCode,masterFields,callback);      
                   }
            else  {
                    data.errorCode = 6; //should not happen trying to create detail without master
                    callback(null,data);
                  }
            }
      });
}

function checkRevisedMenuItemCode(tablName,orgId,bizType,itemCode,masterFields,callback){
   var totalRows = 0;
   var data={};
   var itemStatus = '';
   var revtablName = 'Revision_'+tablName;
   var promise = db.get(revtablName).find({$and:[{org_id:orgId,bus_type:bizType,itemCode:itemCode}]});
   promise.each(function(doc){
        itemStatus = doc.status_id;
        totalRows++;
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 3; //item code not found
         data.tblName = tablName;
         data.itemCode = itemCode;
         data.mastFields = masterFields;
         callback(null,data);   
        }
        else {
          data.errorCode = 5; //itemCode found
          data.tblName = tablName;
          data.rtblName = revtablName;
          data.itemCode = itemCode;
          data.mastFields = masterFields;
          data.itemStatus = itemStatus;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function updateMenuItemDetailDataFromRevision(orgId,bizType,itemCode,tableName,masterFields,revisiontableName,callback){
 var async = require('async');
 var data = {};
 
 var promise = db.get(revisiontableName).find({$and:[{ org_id: orgId,bus_type: bizType,itemCode: itemCode}]})
  promise.on('success',function(doc){
     //even though we are sending whole doc only what is master fields will get updated 
     //we cant use directly createMenuItemDetailData here since itemVisual will be null in revision table
    createMenuItemDetailDataFromRevision(orgId,bizType,itemCode,tableName,masterFields,doc,callback);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function createMenuItemDetailDataFromRevision(orgId,bizType,itemCode,tableName,masterFields,detailsData,callback1){
 var async = require('async');
 var data = {};
 
 var detailData = detailsData[0];
   async.forEachOf(masterFields,function(key,value,callback){
       var query = {};
       var keyId = key.name;
       if(keyId != 'itemCode' && keyId != 'itemVisual'){
       keyValue = detailData[keyId];
       query[keyId] = keyValue;
       query.update_time = new Date();
       query.sellerIds = detailData['sellerIds'];
       
      var promise = db.get(tableName).update(
   {
    org_id: orgId,
    bus_type: bizType,
    itemCode: detailData.itemCode
    },
    {$set: query}
   );
  promise.on('complete',function(doc){
    callback();

 });
  promise.on('error',function(err){
    console.log(err);
    callback();//system error
  });
  }
  else {
    callback();
  }
  },
  function(err){
      if(err)
        callback1(6,null);
      else {
           //createCollection(table_name_mast,table_name_det,callback);
           data.errorCode = 0;
           callback1(null,data);
          }
     });
 
}

function updateMenuItemSelectionDataFromRevision(orgId,bizType,itemCode,tableName,selectionFields,revisiontableName,callback){
//we can't directly call update since new item detail codes might have been added
//or some detail codes deleted during revision
 var promise = db.get(revisiontableName).find({$and:[{ org_id: orgId,bus_type: bizType,itemCode: itemCode}]},'-_id');
      promise.on('success',function(doc){
   createMenuItemSelectionDataFromRevision(orgId,bizType,itemCode,tableName,selectionFields,doc,callback);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

  
}

function  createMenuItemSelectionDataFromRevision(orgId,bizType,itemCode,tableName,selectionFields,docArray,callback){
  var async = require('async');
 var data = {};
 //we will remove all selections of the given itemcode first from main table then
 //start insert row by row
  var promise = db.get(tableName).remove({$and:[{ org_id: orgId,bus_type: bizType,itemCode: itemCode}]},'-_id');
      promise.on('success',function(doc){
   
    async.forEachOf(docArray,function(key1,value1,callback1){ 
  
  var doc = key1;
   insertMenuItemSelectionData(orgId,bizType,itemCode,tableName,selectionFields,doc,doc.itemDetailCode,function(err,results){
    callback1(); //no need to check error here since anyhow we will call back only
   }); 
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           //createCollection(table_name_mast,table_name_det,callback);
           data.errorCode = 0;
           callback(null,data);
          }
     });
     });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });

}

function updateItemSelectionFieldsFromRevision(tablName,orgId,bizType,itemCode,callback){
var revisionItemCode = itemCode+'##Revision';

 //remove original item code
 //then update revision item code to original
 
 var promise = db.get(tablName).remove({$and:[{org_id: orgId,bus_type: bizType,itemCode: itemCode}]});
      promise.on('success',function(doc){
      var promise = db.get(tablName).update(
      { org_id: orgId,bus_type: bizType,itemCode: revisionItemCode},
      {$set: {itemCode: itemCode}},
      {upsert:false, multi:true});
       promise.on('success',function(doc){
        callback(null,null);
        });
      promise.on('error',function(doc){
        callback(6,null);
        });
      });
      promise.on('error',function(doc){
        callback(6,null);
   });   
 



}
function getMenuItemColumnTypes(bizType,currencyType,callback){
   var totalRows = 0;
   var resultArr = [];
   var data = {};
  
   var promise = db.get('businesstype_items_columntypes_master').find({bus_type:bizType});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {id: doc.type_id,label: doc.type_label,additionalTypes: doc.type_additionals};
        resultArr.push(tmpArr);
        }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
           callback(null,8); //no master definition error
       }
       else {
           data.errorCode = 0;
           tmpArr = {id: 5,label: 'Currency',additionalTypes: [],value:currencyType};
           resultArr.push(tmpArr);
 
           data.typeData = resultArr;
           callback(null,data);
           //getCurrencyType(data,currencyType,resultArr,callback);
           //not required since now baseCurrency is stored as code instead of id
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function getCurrencyType(data,currencyType,resultArr,callback){
    var mastutils = require('../Registration/masterUtils');
  
  mastutils.masterNames('currency_type_master',currencyType,function(err,result){
       if(err) callback(6,null);
       
       else{
       var tmpArr = {id: 5,label: 'Currency',additionalTypes: []};
           tmpArr.value = result.typeName;
           resultArr.push(tmpArr);
           data.typeData = resultArr;
           callback(null,data);
     }
});
} 

function checkMenuGroupCode(tablName,orgId,groupCode,callback){

   debuglog("Note business type checks are done away with");
   var totalRows = 0;
   var data={};
   var groupStatus = '';
   var promise = db.get(tablName).find({$and:[{org_id:orgId,groupCode:groupCode}]});
   promise.each(function(doc){
        groupStatus = doc.status_id;
        totalRows++;
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 3; //group code not found
         data.groupCode = groupCode;
         callback(null,data);   
        }
        else {
          data.errorCode = 5; //groupCode found
          data.groupCode = groupCode;
          data.groupStatus = groupStatus;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function checkMenuItemStatusCount(tblName,orgId,itemCode,forwardOrReverse,newStatus,linkCount,usedCount,callback){

   debuglog("Note business type checks are done away with");
   var totalRows = 0;
   var data={};
   var currStatus = 0;
   var currLinkCount = 0;
   var currUsedcount = 0;
   
   var promise = db.get(tblName).find({$and:[{org_id:orgId,itemCode:itemCode}]});
   promise.each(function(doc){
        debuglog("Since the menuitem master table is dynamically created");
        debuglog("fields linkCount and usedCount may not exist");
        debuglog("It is also not defined in master fields so initialize here");
        debuglog("I think no need to do parseInt here since it is not request obtained");
        currStatus = doc.status_id;
        currLinkCount = parseInt(doc.linkCount) || 0;
        currUsedCount = parseInt(doc.usedCount) || 0;
        totalRows++;
       }); 
   promise.on('complete',function(err,doc){
      if (totalRows != 1){
         callback(6,null);   
        }
        else {
          if(forwardOrReverse == 'forward'){
          debuglog("This is a forward moving action");
          debuglog("So call move forward fn with incremented counts");
          var newLinkCount = currLinkCount+linkCount;
          var newUsedCount = currUsedCount+usedCount;
          debuglog(newLinkCount);
          debuglog(newUsedCount);
          moveForwardItemStatus(tblName,orgId,itemCode,currStatus,newStatus,newLinkCount,newUsedCount,callback);
          }
        if(forwardOrReverse == 'reverse'){
          debuglog("This is a Revers moving action");
          debuglog("So call move reverse fn with decremented counts");
          debuglog("If this becomes negative it means some error while linking");
          var newLinkCount = currLinkCount-linkCount;
          var newUsedCount = currUsedCount-usedCount;
		  (newLinkCount<0)? newLinkCount = 0:newLinkCount=newLinkCount;
		  (newUsedCount<0)? newUsedCount = 0:newUsedCount=newUsedCount;
		  
		  debuglog(newLinkCount);
          debuglog(newUsedCount);
          moveReverseItemStatus(tblName,orgId,itemCode,currStatus,newStatus,newLinkCount,newUsedCount,callback);
          }
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function moveForwardItemStatus(tblName,orgId,itemCode,currStatus,newStatus,linkCount,usedCount,callback){

   if(currStatus < newStatus){
   debuglog("Current status is less than the intended status");
   debuglog("so we will update status and counts");
   debuglog(currStatus);
   debuglog(newStatus);
     var promise = db.get(tblName).update({$and:[{org_id:orgId,itemCode:itemCode}]},
     {$set:{status_id:newStatus,
            linkCount: linkCount,
            usedCount: usedCount}},
     {upsert:false}
   );
   }
   else{
   debuglog("Current status is greater than the intended status");
   debuglog("so we will update only counts");
   debuglog(currStatus);
   debuglog(newStatus);
     var promise = db.get(tblName).update({$and:[{org_id:orgId,itemCode:itemCode}]},
     {$set:{linkCount: linkCount,
            usedCount: usedCount}},
     {upsert:false}
   );
   }
  
  promise.on('success',function(err,doc){
      callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function moveReverseItemStatus(tblName,orgId,itemCode,currStatus,newStatus,linkCount,usedCount,callback){

   if(newStatus == 83){
     if(usedCount == 0){
      debuglog("Intended Status is Linked and usedCount is zero");
      debuglog("so we will update status and counts");
      debuglog(currStatus);
      debuglog(newStatus);
      var promise = db.get(tblName).update({$and:[{org_id:orgId,itemCode:itemCode}]},
       {$set:{status_id:newStatus,
              linkCount: linkCount,
              usedCount: usedCount}},
        {upsert:false}
       );
    }
   else{
        debuglog("Intended Status is Linked but usedCount is not zero");
        debuglog("hence this item is in Use by something else so we cant change status to Linked");
        debuglog("so we will update only counts");
        debuglog(currStatus);
        debuglog(newStatus);
        var promise = db.get(tblName).update({$and:[{org_id:orgId,itemCode:itemCode}]},
          {$set:{linkCount: linkCount,
                 usedCount: usedCount}},
           {upsert:false}
         );
      }
    }
   else{
    if(linkCount == 0){
      debuglog("Intended Status is Active and linkcount is zero");
      debuglog("so we will update status and counts");
      debuglog(currStatus);
      debuglog(newStatus);
      var promise = db.get(tblName).update({$and:[{org_id:orgId,itemCode:itemCode}]},
       {$set:{status_id:newStatus,
              linkCount: linkCount,
              usedCount: usedCount}},
        {upsert:false}
       );
      }
    else{
        debuglog("Intended Status is Active but linkcount is not zero");
        debuglog("hence this item is linked to something else")
        debuglog("so we will update only counts");
        debuglog(currStatus);
        debuglog(newStatus);
        var promise = db.get(tblName).update({$and:[{org_id:orgId,itemCode:itemCode}]},
           {$set:{linkCount: linkCount,
                  usedCount: usedCount}},
           {upsert:false}
         );
       }
    }
  promise.on('success',function(err,doc){
      callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getSellers(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
   var moment = require('moment');
   debuglog("Note we get only authorised sellers here");
  var promise = db.get(tblName).find({status_id:82},{sort:{seller_type_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.sellerId = doc.sellerId;
        tmpArr.sellerName = doc.bus_name;
        tmpArr.sellerType = doc.sellerType.id;
        tmpArr.status     = 'Authorised';
        tmpArr.date       = moment(doc.update_time).format('L');
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getSellersData(tblName,sellerIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment = require('moment');

   var promise = db.get(tblName).find({sellerId: {$in: sellerIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {
                        sellerId: doc.sellerId,
                        sellerName:doc.bus_name,
                        sellerType: doc.sellerType,
                        status: 'Authorised',
                        date: moment(doc.update_time).format('L')

        };
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function createCollection(table_name_mast,table_name_det,callback){
//This does not seem to work no node.js driver seems to handle DDL statements
//So we may have to create collection on first insert only
//Only issue may be it will be unindexed
//May be we can try to use index function of monk when inserting data
var mongo = require('mongoskin');

var db2 = mongo.db('mongodb://scarabAdmin:scarab123@localhost:27017/scarabDB');

//monk does not support DDL statements
db2.createCollection(table_name_mast);
//db2.collection.ensureIndex([['org_id',1],['bus_type',1],['itemCode',1]],true,function(err,replies){}); 
db2.collection(table_name_det);
//db2.collection.createIndex({org_id: 1,bus_type:1, itemCode:1, itemDetailCode:1});
db2.close();
callback();  
} 
 
exports.masterFields =  getMenuMasterFields;
exports.masterFieldsBizType =  getMenuMasterFromBizType;
exports.masterFieldsOrgId = getMenuMasterFromOrgId;
exports.createItemMaster = createMenuItemMasterFields;
exports.deleteItemMaster = deleteMenuItemMasterFields;
exports.checkItemCode = checkMenuItemCodeExists;
exports.selectionFieldsBizType = getMenuSelectionFromBizType;
exports.createSelectionFieldsOrgId = createMenuItemSelectionFields;
exports.deleteItemDetail = deleteMenuItemDetailFields;
exports.deleteItemMasterData = deleteMenuItemMasterData;
exports.masterData = getMenuMasterData;
exports.itemCodes = getMenuItemMasterCodes;
exports.itemCount = getMenuItemMasterCount;
exports.selectionFieldsOrgId =  getMenuItemSelectionsFromOrgId;
exports.selectionData = getMenuItemSelectionsData;
exports.createDetailsData = createMenuItemDetailCode;
exports.createSelectionData = createMenuItemSelectionCode;
exports.updateDetailsData = createMenuItemDetailData;
exports.insertSelectionData = insertMenuItemSelectionData;
exports.updateSelectionData = updateMenuItemSelectionData;
exports.deleteSelectionData = deleteMenuItemSelectionData;
exports.checkItemCodeRevision =  checkRevisedMenuItemCodeExists;
exports.updateDetailsFromRevision = updateMenuItemDetailDataFromRevision;
exports.updateSelectionsFromRevision = updateMenuItemSelectionDataFromRevision;
exports.columnTypes = getMenuItemColumnTypes;
exports.updateSelectionFieldsFromRevision = updateItemSelectionFieldsFromRevision;
exports.checkGroupCode = checkMenuGroupCode;
exports.moveItemStatus = checkMenuItemStatusCount;
exports.sellers = getSellers;
exports.sellersData = getSellersData;
