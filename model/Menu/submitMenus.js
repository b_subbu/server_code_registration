function processRequest(req,res,data){
  if(typeof req.body.menuCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMenuStatus(req,res,9,data);
}


function checkMenuStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    data.menuCodes = menuArr;
    var menuStatus = 0;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          menuStat: function (callback){utils.checkMenuCode(tabl_name,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
          menuStatus = results.menuStat.itemStat;
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
           if(isError == 0 && (results.menuStat.errorCode != 0 && results.menuStat.errorCode != 5)) {
             isError = 1;
             errorCode = results.menuStat.errorCode;
             callback1();
           }
          if( isError == 0 && (results.menuStat.itemStat != 80 && results.menuStat.itemStat != 85 && results.menuStat.itemStat != 86 && results.menuStat.itemStat != 87)) {
             errorCode = 5;
             isError = 1;
             callback1();
           }
           if(isError == 0)  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
              processStatus(req,res,6,data);
             
             }
      else
          {
           //changeMenuStatus(req,res,9,data,tabl_name); 
           checkValidTree(req,res,9,data,tabl_name,menuStatus);
         }
     });




}

function checkValidTree(req,res,stat,data,tabl_name,menuStatus){

    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    var menuArr = data.menuCodes;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          menuStat: function (callback){utils.checkMenuTree(tabl_name,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             errorCode = 7;
             callback1();
            }
         else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
              processStatus(req,res,6,data);
             
             }
      else
          {
           if(menuStatus == 86){
            debuglog("This is a edit on Recalled Menu");
            debuglog("so let us first move all the items and groups to active Status");
            debuglog("before moving to pending Tab");
            debuglog("Authorise will move them back to Linked Status");
            debuglog("Else the Linked items/groups will remain asis");
            getItemStatus(req,res,9,data);
          }
           else
             moveToRevision(req,res,9,data); 
         }
     });


}

function getItemStatus(req,res,stat,data){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
    
     async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
     async.parallel({
         linkData: function(callback){utils.menuLinksWithNodeIs(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           if (isError == 0 && results.linkData.errorCode == 0) {
           debuglog(results.linkData);
           debuglog("This data is obtained from the main link table");
           debuglog("We will temporarily move all the items and groups");
           debuglog("from InUse status back and decrement their linked and used count");
           debuglog("Then again the release operation will increment it and ");
           debuglog("move it back to InUse status");
           debuglog("This seems to be much faster than checking 2 arrays and doing processing");
            itemsArr.push(results.linkData.itemCodes);
             callback1();
            }
          else
           callback1(); //Sometimes delete seems to happen before insert-->TODO:check
         }); 
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            changeMainItemStatus(req,res,9,data,itemsArr);
          }
     });
}

function changeMainItemStatus(req,res,stat,data,itemsArr){
    
    var async = require('async');
    var itemUtils = require('./utils');
    var groupUtils = require('../Groups/dataUtils');
    var isError = 0;
    
    var reason = 'Menu Edit changed Status'; 
    
    var item_tabl_name = data.orgId+"_menuitem_master";
    var group_tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr[0],function(key1,value1,callback1){
     debuglog("starting processing of items arr to change status");
     debuglog(key1);
     
       if(key1.nodeIs == 'item'){
          debuglog("This is an item");
          debuglog(key1);
          debuglog("we will move the item to Active status temporarily");
          debuglog("and decrement used and linked counter by 1");
          debuglog("this is to take care of Recalled Menu addition removal of items");
          itemUtils.moveItemStatus(item_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,1,function(err,results){
          if(err){
           debuglog("move reverse error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move reverse error");
           callback1();
          }
          });
         }
          
    if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          debuglog("we will move the group to Active status");
          debuglog("and decrement linked and used counter by 1");
          groupUtils.moveGroupStatus(group_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,1,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
        }
       },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
            moveToRevision(req,res,9,data);
          }
    });
}


function moveToRevision(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var menuutils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file
    var mainTable    = orgId+'_menus_master';
    var revisionFlag = 2;
    //note revisionData tables is reversed here so as to move in another direction
   
    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          relStat: function (callback){menuutils.revisionData(mainTable,revisionTable,data.orgId,data.businessType,menuCode,revisionFlag,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           removeMainData(req,res,9,data,mainTable,revisionTable); 
         }
     });
}

function removeMainData(req,res,stat,data,mainTable,revisionTable){
    var async = require('async');
 
    var menuutils = require('./menuutils');
    var isError = 0;
    var menuArr = req.body.menuCodes;
   
    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          delStat: function (callback){menuutils.deleteMenu(mainTable,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeMenuStatus(req,res,9,data,menuCode,81,revisionTable); //ReleasedAndSubmitted 
         }
     });
      
}

function changeMenuStatus(req,res,stat,data,menuCode,menuStat,revTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
    
   
    var reason = '';
       statutils.changeMenuStatus(revTable,data.orgId,data.businessType,data.userId,menuCode,menuStat,reason,function(err,result) {
          if(err) {
                     processStatus(req,res,6,data);
                }
           else {
                 data.menuCode = menuCode;
                 getMenuDetails(req,res,9,data,revTable);
          
              }
          });
     
}


function getMenuDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    async.forEachOf(data.menuCodes,function(key,value,callback){
    var menuCode = key;
    utils.menuDetail(revTable,data.orgId,data.businessType,menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              isError =1 ;
              callback();
              }
      else  {
            resultArr.push(result.masterData);
            callback();
           }
    });
    },
    function(err){
         if(err || isError == 1) 
               processStatus(req,res,6,data);
       else
          {
          data.menuData = resultArr;
          processStatus(req,res,9,data); 
         }
     });
    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




