function processRequest(req,res,data){
   if(typeof req.body.itemCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItemCodes(req,res,9,data);
}


function getItemCodes(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   var itemCodes = req.body.itemCodes;
   
   async.forEachOf(itemCodes,function(key1,value1,callback){
     var itemCode = key1;
      utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
             //because only Linked and InUse Items are seen from this API
             //so no need to check REvision tables
            tabl_name = result.tblName; 
            mastFields = result.mastFields;
            }
           callback();
          }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             getMasterData(req,res,9,data,mastFields,tabl_name); 
           }
        });
} 

//Note: the fields in master table can change from org to org
// so should use the master fields definitions
function getMasterData(req,res,stat,data,mastFields,table_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var masterData = [];
   var itemCodes = req.body.itemCodes;
   
    
   async.forEachOf(itemCodes,function(key1,value1,callback){
     var itemCode = key1;
  
    utils.masterData(data.orgId,data.businessType,itemCode,table_name,function(err,result) {
        if(err) processStatus(req,res,6,data);
         else {
                 itemMasterData = result.masterData;
                 masterData.push(itemMasterData);
                 callback();
              }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             getItemStatusName(req,res,9,data,mastFields,masterData); 
           }
        });
 } 

function getItemStatusName(req,res,stat,data,mastFields,mastData) {
   var async = require('async');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var visualURL = '';
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var visualIndex = 0;
      
      async.forEachOf(mastData,function(key1,value1,callback1){
        var masterData = mastData[value1][0]; //because mastdata is double array
          subitemMasters = {};
        async.forEachOf(masterData,function(key2,value2,callback2){
           if(value2 == 'status_id'){
              var statID=key2;
           statutils.statName(statID,'menuitem_status_master',function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 //key2 = result;
                 mastData[value1][0]['status_id'] = result;
                 callback2(); 
              }
           }); 
          }
          
            else
              callback2();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           formatMasterData(req,res,9,data,mastFields,mastData);
        }
      });
 

} 


function formatMasterData(req,res,stat,data,mastFields,masterData) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var itemMasters = {};
   var masterDetails = [];
  
      async.forEachOf(masterData,function(key,value,callback){
        var mastData = key;
        var itemMasters = {};
      async.forEachOf(mastData,function(key1,value1,callback1){
         async.forEachOf(key1,function(key2,value2,callback2){
          if(value2 == 'status_id')
            itemMasters['status'] = key2; //because status_id is not defined in master fields
            async.forEachOf(mastFields,function(key3,value3,callback3){
            var key_name = key3.name;
              if(key_name == 'itemVisual'){
                visualArray = key1.itemVisual;
                visualIndex = value3;
                callback3(); //visuals is seperate grid no need to send title
               }
            else if(key_name == value2)  {
                   itemMasters[key_name] = key2;
                   callback3();
                 }
            else
              callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             callback2(); 
           }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          mastFields.splice(visualIndex,1); 
          masterDetails.push(itemMasters);
          callback();
         }
      });
     },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           data.menuItemDetails = masterDetails;
         processStatus(req,res,9,data);
        }
      });
 
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




