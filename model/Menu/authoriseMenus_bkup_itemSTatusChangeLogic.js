function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined' || typeof req.body.menuCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    data.userId = uid;
    
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              checkMenuStatus(req,res,9,data);
            }
          }); 
}

function checkMenuStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    data.menuCodes = menuArr;
    var itemStat = 0;
    
    var orgId = data.orgId;
    
    var tabl_name = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          menuStat: function (callback){utils.checkMenuCode(tabl_name,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
           if(isError == 0 && (results.menuStat.errorCode != 0 && results.menuStat.errorCode != 5)) {
             isError = 1;
             errorCode = results.menuStat.errorCode;
             callback1();
           }
          if( isError == 0 && (results.menuStat.itemStat != 81 && results.menuStat.itemStat != 88)) {
             errorCode = 5;
             isError = 1;
             callback1();
           }
           if(isError == 0){ 
             itemStat = results.menuStat.itemStat;
             debuglog("Because it is not possible to authorise some menus in active tab and some in pending tab");
             callback1();
             }
             
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
              processStatus(req,res,6,data);
             
             }
      else
          {
           if(itemStat == 81)
            moveToMain(req,res,9,data,itemStat);
           else
            removeMain(req,res,9,data,itemStat);
          }
     });




}
//This will remove the 89 status row
function removeMain(req,res,stat,data,itemStat){
    var async = require('async');
    var menuArr = req.body.menuCodes;

    var menuutils = require('./menuutils');
    var isError = 0;
   
    var main_tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
    
    var reason = '';
      async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
             delStat: function(callback) {menuutils.deleteMenu(main_tabl_name,data.orgId,data.businessType,menuCode,callback); }
             },
         
          function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err) {
               processStatus(req,res,6,data);
            }
      else {
             moveToMain(req,res,9,data,itemStat); //ReleasedAndSubmitted
           }
          });
     
}

function moveToMain(req,res,stat,data,itemStat){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revision_tabl_name = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file
    var main_tabl_name = orgId+'_menus_master';

    var revisionFlag = 1;
    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          relStat: function (callback){utils.revisionData(revision_tabl_name,main_tabl_name,data.orgId,data.businessType,menuCode,revisionFlag,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           removeRevisionData(req,res,9,data,main_tabl_name,revision_tabl_nameitemStat); 
         }
     });
}

function removeRevisionData(req,res,stat,data,mastTable,revisionTable,itemStat){
    var async = require('async');
    var menuArr = req.body.menuCodes;

    var menuutils = require('./menuutils');
    var isError = 0;
   
     
    var reason = '';
      async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
             delStat: function(callback) {menuutils.deleteMenu(revisionTable,data.orgId,data.businessType,menuCode,callback); }
             },
         
          function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err) {
            
               processStatus(req,res,6,data);
            }
      else {
            if(itemStat == 88){
                debuglog("This is a 88 status menu in pending tab");
                debuglog("So we will redirect it to Release API");
                debuglog("which will change menu satus as well as linked Items and groups statuses");
                var release = require('./releaseMenus');
                release.changeMenuStatus(req,res,9,data,mastTable);
            } 
            else
               {
                debuglog("This is a 81 status menu so it has not been released yet");
                debuglog("after copying the data from pending tab to main tab");
                debuglog("we will change the menu and item status here");
                debuglog(mastTable);
                debuglog(data);
                changeMenuStatus(req,res,9,data,mastTable);
              }
          });
     
}

function changeMenuStatus(req,res,stat,data,tabl_name){
    
    var menuArr = data.menuCodes;
    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = ''; //no reason is currently set for approve operation
    
     
     async.forEachOf(menuArr,function(value,key,callback1){
       var menuCode = value;
       async.parallel({
         chngStatus: function(callback){statutils.changeMenuStatus(tabl_name,data.orgId,data.businessType,menuCode,82,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             callback1();
            }
         });
         },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else{
            getItemsData(req,res,9,data);
          }
     });
}

function getItemsData(req,res,stat,data){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
    
     async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
     async.parallel({
         linkData: function(callback){utils.menuLinks(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           if (isError == 0 && results.linkData.errorCode == 0) {
           debuglog(resutls.linkData);
           debuglog("This can contain either item or group we will do corresponding processing ");
           debuglog("in next function");
            itemsArr.push(results.linkData.itemCodes);
             callback1();
            }
          else
           callback1(); //Sometimes delete seems to happen before insert-->TODO:check
         }); 
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            changeItemStatus(req,res,9,data,itemsArr,groupsArr);
          }
     });
}

//Note this is a special function not generic item status change
//Change status to Linked only if it is Entered
//dont change status if it is InUse may be due to other menus

function changeItemStatus(req,res,stat,data,itemsArr,groupsArr){
    
    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = 'Menu Approval changed Status'; 
    
    var item_tabl_name = data.orgId+"_menuitem_master";
    var group_tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr,function(key1,value1,callback1){
      async.forEachOf(key1,function(key2,value2,callback2){ 
        //because itemsArr will be double array of itemcodes
       var itemCode = key2;
       async.parallel({
         chngStatus: function(callback){statutils.chngItmStatusOnMenuChange (tabl_name,data.orgId,data.businessType,itemCode,80,83,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback2();
            }
           else 
             callback2();
            
         });
         },
         function(err){
         if(err) {
              console.log(err);
              callback1();
             }
      else {
            callback1();
          }
     });
     },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
            changeGroupStatus(req,res,stat,data,groupsArr);
            
          }
    });
}

function changeGroupStatus(req,res,stat,data,groupsArr){
    
    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = 'Menu Approval changed Status'; 
    
    var tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr,function(key1,value1,callback1){
      async.forEachOf(key1,function(key2,value2,callback2){ 
        //because itemsArr will be double array of itemcodes
       var itemCode = key2;
       async.parallel({
         chngStatus: function(callback){statutils.chngItmStatusOnMenuChange (tabl_name,data.orgId,data.businessType,itemCode,80,83,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback2();
            }
           else 
             callback2();
            
         });
         },
         function(err){
         if(err) {
              console.log(err);
              callback1();
             }
      else {
            callback1();
          }
     });
     },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
            getMenuDetails(req,res,9,data,);
            
          }
    });
}

function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    async.forEachOf(data.menuCodes,function(key,value,callback){
    var menuCode = key;
    utils.menuDetail(mastTable,data.orgId,data.businessType,menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              isError =1 ;
              callback();
              }
      else  {
            resultArr.push(result.masterData);
            callback();
           }
    });
    },
    function(err){
         if(err || isError == 1) 
               processStatus(req,res,6,data);
       else
          {
           data.menuData = resultArr;
          processStatus(req,res,9,data); 
         }
     });
    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Menu/authoriseMenus');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



