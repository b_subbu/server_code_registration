function processRequest(req,res,data){
      getMenuData(req,res,9,data);
}


function getMenuData(req,res,stat,data){

   var menuData = req.body.menuData;
   if(typeof menuData === 'null' || typeof menuData === 'undefined')
     processStatus(req,res,6,data);
  
  else{

   var async = require('async');
   var utils = require('./menuutils');
   var mastData = {};
   debuglog("not null menuData");
   debuglog(menuData);
   
  utils.parseJson(menuData,function(err,result){
    if(err) processStatus(req,res,6,data);
    else {
          debuglog("This is the information of root-node");
          debuglog(result);
          //Refer: https://github.com/substack/js-traverse 
          mastData.id = result.id; //just dummy id
          mastData.name = result.name;
          mastData.description = result.description; 
          checkMenuCodeExists(req,res,9,data,mastData,menuData);
        }
     }); 
    
 }
}

//cant do check of mandatory fields
//since if fields are not present parse will throw system error

function checkMenuCodeExists(req,res,stat,data,mastData,menuData) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file
   
   var menuCode = mastData.id;
   var itemCodes = req.body.itemCodes;
   var submitop = req.body.submitOperation;
 
   
    utils.checkMenuCode(tabl_name,orgId,bizType,menuCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 3)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
             if(submitop == 1){
             var submitmenu = require('./submitMenu');
             submitmenu.getData(req,res,9,data,mastData,menuData,501,result.itemStat);
             }
          else {
            getItemCodes(req,res,9,data,mastData,menuData,tabl_name)
            }
           }
          }
        });
} 

//Extracting itemcodes from the tree
function getItemCodes(req,res,stat,data,mastData,menuData,tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var itemCodes = [];
   var groupCodes = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    var menuLinks = menuData.menuLinkage;
     async.forEachOf(menuLinks,function(key1,value1,callback1){
      delete key1.__uiNodeId;
      if(key1.isLeaf == true){
          if(key1.nodeIs == 'item'){
          debuglog("This is a item");
          debuglog(key1);
           itemCodes.push(key1.linkId);
           
          }
          if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          groupCodes.push(key1.linkId);
          }
       debuglog(key1);
       callback1();
       }
      else{
       debuglog("Not a leaf node may be link nodes");
       debuglog(key1);
       callback1();
       }
    },
   function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
     else {
          debuglog("start checking item codes and group codes");
          debuglog(itemCodes);
          debuglog(groupCodes);
          checkItemCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name);
         }
      });
} 

function checkItemCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 7; //item not found
              isError = 1;
              }
            if( isError == 0 && (result.itemStatus != 80 && result.itemStatus != 83 && result.itemStatus != 84)) {
              errorCode = 7; //item not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                checkGroupCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name);
              }
            }
          });
} 

function checkGroupCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   debuglog("Note business type checks are done away with");
   debuglog("TODO do away with orgId check since each data is in its own organziation table");
   
   var grp_tabl_name = orgId+'_group_header';
   async.forEachOf(groupCodes,function(key,value,callback){
      groupCode = key;
     utils.checkGroupCode(grp_tabl_name,orgId,groupCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 8; //Group not found
              isError = 1;
              }
            if( isError == 0 && (result.groupStatus != 80 && result.groupStatus != 83 && result.groupStatus != 84)) {
              errorCode = 8; //Group not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                createMenuData(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name);
              }
            }
          });
} 

function createMenuData(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
   
    var menuCode = mastData.id;
   
    utils.createMenuMaster(tabl_name,orgId,bizType,menuCode,mastData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
              createMenuLinks(req,res,9,data,menuCode,itemCodes,groupCodes,tabl_name);
            }
        });
} 

function createMenuLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,mastTable) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
    //var tabl_name = orgId+'_menus_nodes' 
    //just save links as array in master table 
    var linkData = req.body.menuData;
   
    utils.createMenuNodes(mastTable,orgId,bizType,menuCode,linkData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
              createMenuItemLinks(req,res,9,data,menuCode,itemCodes,groupCodes,mastTable);
            }
        });
} 

function createMenuItemLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,mastTable) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
  
  debuglog("using same link table to store both items and groups");
  debuglog("items will be stored with nodeIs = item");
  debuglog("and groups with nodeIs = group");
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.linkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,itemCode,'item',function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
            callback();
            }
        });
      },
      function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else
          {
            createMenuGroupLinks(req,res,9,data,menuCode,itemCodes,groupCodes,mastTable); 
          }
    });
} 

function createMenuGroupLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,mastTable) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
  
  debuglog("same function and table as menu item link");
  debuglog("but nodeIs param is changed to group");
  debuglog(groupCodes);
  
   async.forEachOf(groupCodes,function(key,value,callback){
      groupCode = key;
     utils.linkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,groupCode,'group',function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
            callback();
            }
        });
      },
      function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else
          {
            changeMenuStatus(req,res,9,data,menuCode,80,mastTable); //80 is status for Entered
          }
    });
} 

function changeMenuStatus(req,res,stat,data,menuCode,menuStat,mastTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
    
    if (req.body.submitOperation == 1)
        menuStat = 81;//Ready2Auth
  
    var reason = '';
       statutils.changeMenuStatus(mastTable,data.orgId,data.businessType,data.userId,menuCode,menuStat,reason,function(err,result) {
          if(err) {
                     processStatus(req,res,6,data);
                }
           else {
                 data.menuCode = menuCode;
                 getMenuDetails(req,res,9,data,mastTable);
          
              }
          });
     
}

function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    utils.menuDetail(mastTable,data.orgId,data.businessType,data.menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else  {
            data.menuData = result.masterData;
            processStatus(req,res,9,data);
           }
    });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



