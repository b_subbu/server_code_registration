function getFilters(key_name,statList,pid,tid,tablName,callback) {
  
  switch(key_name){
     case 'status':
       getStatusFilter(statList,tablName,callback);  
        break;
     
    default:
       callback(1,null);
       break;
  }
  
}

//for status no need to again query db since statList is already passed
//and it varies from tab to tab for other filters we can just query master
//and return

function getStatusFilter(mastData,tablName,callback) {
   var async = require('async');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
   var data = {};
     
      async.forEachOf(mastData,function(key1,value1,callback1){
              var statID=key1;
         statutils.statName(statID,tablName,function(err,result) {
           if(err){ isError =1; callback1();}
         else {
                 //key2 = result;
                 var tmpArray = {id:statID, name:result};
                 itemMasters.push(tmpArray);
                  callback1(); 
              }
           }); 
          },
         function(err){
       if(err){
            callback(6,null);      }
      else {
        //data.finalArray = itemMasters;
        callback(null,itemMasters);
	   }
      });
 

}

 exports.filterOptions = getFilters;