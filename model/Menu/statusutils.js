function getItemStatus(pid,tid,statlist,statTableName,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key];    
   var promise = db.get('product_tab_status_action').find({$and:[{product_id:pid},{tab_id:tid},{stat_id:statid}]});
   promise.each(function(doc){
   var tmpArray = {id:doc.action_id};
      resultArray.push(tmpArray);
        }); 
   promise.on('complete',function(err,doc){
      var tmpArray = {id: statid, name:"ABC"};
       tmpArray.availableActions = resultArray;
       finalArray.push(tmpArray);
     
       resultArray = [];
     
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           getItemStatusName(finalArray,statTableName,callback);
           }
     });
} 


function getItemStatusName(statlist,tblName,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key].id;    
   var promise = db.get(tblName).find({status_id:statid});
   promise.each(function(doc){
        statlist[key].name = doc.status_name;
        }); 
   promise.on('complete',function(err,doc){
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
            getItemNextActionName(statlist,callback);
           }
     });
} 


function getItemNextActionName(statlist,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key].id;
   var actionlist = statlist[key].availableActions;    
  async.forEachOf(actionlist,function(value1,key1,callback2){
   var actionid = actionlist[key1].id;    
   
   var promise = db.get('item_master').find({item_id:actionid});
   promise.each(function(doc){
        statlist[key].availableActions[key1].name = doc.item_name;
        statlist[key].availableActions[key1].title = doc.item_title;
        statlist[key].availableActions[key1].sequence= doc.item_seq;
        }); 
   promise.on('complete',function(err,doc){
      callback2();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
     function(err){
      if(err)
             callback(1,null);
      else
          {
           callback1();
           
           }
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           callback(null,statlist);
            }
     });
} 

function changeItemStatus(tablName,orgId,bizType,operatorId,itemCode,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                              {itemCode: itemCode }, 
                                               {$set: {
                                                status_id: stat,
                                                update_time: new Date()
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       createStatusChngLog(orgId,bizType,operatorId,itemCode,stat,reason,callback);
     else
       callback(6,null); //system Error  
 } 
 
 function createStatusChngLog(orgId,bizType,operatorId,itemCode,stat,reason,callback){
 
  var promise = db.get('menuitem_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    itemCode: itemCode,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }
 
function getItemStatName(statid,tablName,callback){
   var totalRows =0;
    var statName = '';
  
  
   var promise = db.get(tablName).find({status_id:statid});
   promise.each(function(doc){
        statName = doc.status_name;
        }); 
   promise.on('complete',function(err,doc){
      callback(null,statName);
       
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
 } 

//This is a wrapper function copied from getMenuItem
//because it is used in response of all other API calls
function getMenuDetailsData(data,mastFields,table_name,callback) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
  
    utils.masterData(data.orgId,data.businessType,data.itemCode,table_name,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 itemMasterData = result.masterData;
                  formatData(data,mastFields,itemMasterData,callback); 
              }
        });
} 


function formatData(data,mastFields,mastData,callback) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var itemMasters = {};
   var visualArray = [];
      async.forEachOf(mastData,function(key1,value1,callback1){
         async.forEachOf(key1,function(key2,value2,callback2){
          if(value2 == 'status_id')
            itemMasters['status'] = key2; //because status_id is not defined in master fields
            async.forEachOf(mastFields,function(key3,value3,callback3){
            var key_name = key3.name;
              if(key_name == 'itemVisual'){
                visualArray = key1.itemVisual;
                visualIndex = value3;
                callback3(); //visuals is seperate grid no need to send title
               }
            else if(key_name == value2)  {
                   itemMasters[key_name] = key2;
                   callback3();
                 }
            else
              callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             callback2(); 
           }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            callback1(); 
           }
        });
      },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          mastFields.splice(visualIndex,1); 
          //data.columns = mastFields;
          data.menuItemDetails = itemMasters;
          callback(null,data);
        }
      });
 

} 

function changeMenuStatus(tablName,orgId,bizType,operatorId,menuCode,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                              {menuCode: menuCode }, 
                                               {$set: {
                                                status_id: stat,
                                                update_time: new Date()
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       createMenuStatusChngLog(orgId,bizType,operatorId,menuCode,stat,reason,callback);
     else
       callback(6,null); //system Error  
 } 
 
 function createMenuStatusChngLog(orgId,bizType,operatorId,menuCode,stat,reason,callback){
 
  var promise = db.get('menus_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    menuCode: menuCode,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}
 
function changeItemStatusOnMenuChange(tablName,orgId,bizType,itemCode,currStat,nextStat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                              {itemCode: itemCode,
                                               status_id: currStat }, 
                                               {$set: {
                                                status_id: nextStat,
                                                update_time: new Date()
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       createStatusChngLog(orgId,bizType,itemCode,nextStat,reason,callback);
     else
       callback(6,null); //system Error  
 } 
 
exports.statList = getItemStatus;
exports.changeStatus = changeItemStatus;
exports.statName = getItemStatName;
exports.detailsData = getMenuDetailsData;
exports.changeMenuStatus = changeMenuStatus;
exports.chngItmStatusOnMenuChange = changeItemStatusOnMenuChange;

