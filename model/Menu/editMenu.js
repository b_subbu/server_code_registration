function processRequest(req,res,data){
      getMenuData(req,res,9,data);
}



function getMenuData(req,res,stat,data){

   var menuData = req.body.menuData;
   if(typeof menuData === 'null' || typeof menuData === 'undefined')
     processStatus(req,res,6,data);
  
  else{

   var async = require('async');
   var utils = require('./menuutils');
   var mastData = {};
   

  
 utils.parseJson(menuData,function(err,result){
    if(err) processStatus(req,res,6,data);
    else {
          //This is the information of root-node
          mastData.id = result.id;
          mastData.name = result.name;
          mastData.description = result.description;
          checkMenuCodeExists(req,res,9,data,mastData,menuData); 
        }
     }); 
 }
}

function checkMenuCodeExists(req,res,stat,data,mastData,menuData) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   //var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file
   
   var menuCode = mastData.id;
   
   var tabId = req.body.tabId;
   var submitop = req.body.submitOperation;
   
  
      if(tabId == 502 )
          var tabl_name = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file
      else
         var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file
  
    utils.checkMenuCode(tabl_name,orgId,bizType,menuCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
              if(isError == 0 && (result.itemStat != 80 && result.itemStat != 86 && result.itemStat != 87 && result.itemStat != 84 && result.itemStat != 88)){
               processStatus(req,res,5,data);
              isError = 1;
              return;
             }
            else {
             if(submitop == 1){
                 var submitmenu = require('./submitOnEditMenu');
                 submitmenu.getData(req,res,9,data,mastData,menuData,tabId,result.itemStat);
                }
          else {
                 getItemCodes(req,res,9,data,mastData,menuData,result.itemStat,tabl_name)
               }
          }
        }
      });
} 

//Extracting itemcodes from the tree
function getItemCodes(req,res,stat,data,mastData,menuData,menuStat,tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var itemCodes = [];
   var groupCodes = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    var menuLinks = menuData.menuLinkage;
     async.forEachOf(menuLinks,function(key1,value1,callback1){
      delete key1.__uiNodeId;
      if(key1.isLeaf == true){
          if(key1.nodeIs == 'item'){
          debuglog("This is a item ");
          debuglog(key1);
           itemCodes.push(key1.linkId);
           
          }
          if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          groupCodes.push(key1.linkId);
          }
       debuglog(key1);
       callback1();
       }
      else{
       debuglog("Not a leaf node may be link nodes");
      debuglog(key1);
       callback1();
       }
    },
   function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
     else {
          debuglog("start checking item codes and group codes");
          debuglog(itemCodes);
          debuglog(groupCodes);
          checkItemCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,menuStat,tabl_name);
         }
      });
} 

function checkItemCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,menuStat,tabl_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 7; //item not found
              isError = 1;
              }
            if( isError == 0 && (result.itemStatus != 80 && result.itemStatus != 83 && result.itemStatus != 84)) {
              errorCode = 7; //item not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                checkGroupCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,menuStat,tabl_name);
              }
            }
          });
} 

function checkGroupCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,menuStat,tabl_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   debuglog("Note business type checks are done away with");
   debuglog("TODO do away with orgId check since each data is in its own organziation table");
   
   var grp_tabl_name = orgId+'_group_header';
   async.forEachOf(groupCodes,function(key,value,callback){
      groupCode = key;
     utils.checkGroupCode(grp_tabl_name,orgId,groupCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 8; //Group not found
              isError = 1;
              }
            if( isError == 0 && (result.groupStatus != 80 && result.groupStatus != 83 && result.groupStatus != 84)) {
              errorCode = 8; //Group not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                 updateMenuData(req,res,9,data,mastData,menuData,itemCodes,groupCodes,menuStat,tabl_name);
                }
            }
          });
} 

function updateMenuData(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,menuStat,tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   var tabId = req.body.tabId;
   
  
      if(tabId == 502 )
          var link_tabl_name = 'Revision_'+orgId+'_menus_items_link'; //TODO: move to a common definition file
      else
         var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
  
   
    utils.updateMenu(tabl_name,orgId,bizType,mastData.id,mastData,menuData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
               updateMenuLinks(req,res,9,data,mastData.id,menuData,itemCodes,groupCodes,menuStat,tabl_name,link_tabl_name);
            }
        });
} 

//Note: MenuLinks are the Nodes of a menu
//and MenuItemLinks are the menu->item link
function updateMenuLinks(req,res,stat,data,menuCode,menuData,itemCodes,groupCodes,menuStat,mastTable,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    var linkData = req.body.menuData;
   
   
    utils.createMenuNodes(mastTable,orgId,bizType,menuCode,linkData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
              deleteMenuItemLinks(req,res,9,data,menuCode,itemCodes,groupCodes,menuStat,mastTable,link_tabl_name);
            }
        });
} 

//we will delete all links and recreate them
//this will be faster instead of checking one by one row and updating
function deleteMenuItemLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,menuStat,tabl_name,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
     utils.delinkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,function(err,result) {
          if(err) {
               isError = 1;
              
             }
         else {
                 createMenuItemLinks(req,res,9,data,menuCode,itemCodes,groupCodes,menuStat,tabl_name,link_tabl_name);
              }
    });
} 


function createMenuItemLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,menuStat,mastTable,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
  debuglog("using same link table to store both items and groups");
  debuglog("items will be stored with nodeIs = item");
  debuglog("and groups with nodeIs = group");
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.linkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,itemCode,'item',function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
            callback();
            }
        });
      },
      function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else
          {
            createMenuGroupLinks(req,res,9,data,menuCode,itemCodes,groupCodes,menuStat,mastTable,link_tabl_name); 
          }
    });
} 

function createMenuGroupLinks(req,res,stat,data,menuCode,itemCodes,groupCodes,menuStat,mastTable,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
  debuglog("same function and table as menu item link");
  debuglog("but nodeIs param is changed to group");
  debuglog(groupCodes);
  
   async.forEachOf(groupCodes,function(key,value,callback){
      groupCode = key;
     utils.linkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,groupCode,'group',function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
            callback();
            }
        });
      },
      function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else
          {
            changeMenuStatus(req,res,9,data,menuCode,menuStat,mastTable); 
          }
    });
} 

function changeMenuStatus(req,res,stat,data,menuCode,menuStat,mastTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = '';
    
     var tabId = req.body.tabId;
 
     debuglog("The status will be same as that was original");
     debuglog("That is either entered, rejected or recalled from active Tab");
     debuglog("ReleasedAndSubmitted if SAVE from pending Tab");
     debuglog(menuStat);
     debuglog(mastTable);
      
  
      statutils.changeMenuStatus(mastTable,data.orgId,data.businessType,data.userId,menuCode,menuStat,reason,function(err,result) {
          if(err) {
                     processStatus(req,res,6,data);
                }
           else {
                 data.menuCode = menuCode;
                 getMenuDetails(req,res,9,data,mastTable);
           
              }
          });
     
}


function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    utils.menuDetail(mastTable,data.orgId,data.businessType,data.menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else  {
            data.menuData = result.masterData;
            processStatus(req,res,9,data);
           }
    });
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




