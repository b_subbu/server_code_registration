function processRequest(req,res,data){
        getMenuCode(req,res,9,data);
}

function getMenuCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
     var tabId = req.body.tabId;
 
      if(tabId == 502 )
          var tabl_name = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file
      else
         var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file
  
   
   var menuCode = req.body.menuCode;
   
   
    utils.checkMenuCode(tabl_name,orgId,bizType,menuCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
            //data.menuCode = menuCode;
            getMenuData(req,res,9,data,tabl_name,menuCode);
            }
          }
        });
} 



function getMenuData(req,res,stat,data,tabl_name,menuCode){
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
         
    utils.menuData(tabl_name,orgId,bizType,menuCode,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if (isError == 0 && results.countItems == 0){
              data.menuData = {};
              //getItemCodes(req,res,9,data); 
              //isError = 1; //not really error just to prevent next execution
              processStatus(req,res,9,data);
              return;
             }
           if (isError == 0) {
            var menuData = {};
            menuData.menuCode = menuCode;
            menuData.menuName = results.menuName;
            menuData.menuDescription = results.menuDescription;
            menuData.statusCode = results.menuStatus;
            //menuData.menuLinkage = {};
            
            data.menuData = menuData; 
            //NOTE: menuData is stored as a stringified JSTREE and rendered as-is
            //need to do additional parsing here if needed
            getMenuNodes(req,res,9,data,tabl_name,menuCode);
            }
         });
}

function getMenuNodes(req,res,stat,data,tabl_name,menuCode){
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
 
   var orgId = data.orgId;
   var bizType = data.businessType;

         
    utils.menuNodes(tabl_name,orgId,bizType,menuCode,function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if (isError == 0 && results.countItems == 0){
             data.menuData.menuLinkage = [];
              //getItemCodes(req,res,9,data); //no need to get item codes as no nodes itself 
              processStatus(req,res,9,data);
              return;
             }
           if (isError == 0) {
              //data.menuData.menuLinkage = results.menuNodes;
              //getItemCodes(req,res,9,data);
              constructMenuTree(req,res,9,data,results.menuNodes);
            }
         });
}

function constructMenuTree(req,res,stat,data,menuNodes) {
   var async = require('async');
   var type = require('type-detect');
   var itemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(menuNodes[0],function(key1,value1,callback1){
            itemMaster = key1.nodeData;
            itemMasters.push(itemMaster)
                 callback1(); 
        },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
            data.menuData.menuLinkage = itemMasters;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;







