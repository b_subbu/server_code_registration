function processRequest(req,res,data){
   if(typeof req.body.menuCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMenuStatus(req,res,9,data);
}

function checkMenuStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    data.menuCodes = menuArr;
    
    var itemStatus = 0;
    
    var orgId = data.orgId;
    
    var revisionTable = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file
    var mainTable    = orgId+'_menus_master';

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          menuStat: function (callback){utils.checkMenuCode(revisionTable,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
           if(isError == 0 && (results.menuStat.errorCode != 0 && results.menuStat.errorCode != 5)) {
             isError = 1;
             errorCode = results.menuStat.errorCode;
             callback1();
           }
          if( isError == 0 && (results.menuStat.itemStat != 81 && results.menuStat.itemStat != 88)) {
             errorCode = 5;
             isError = 1;
             itemStatus = results.menuStat.itemStat;
             callback1();
           }
           if(isError == 0){  
             itemStatus = results.menuStat.itemStat;
             callback1();
             }
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
              processStatus(req,res,6,data);
             }
      else
          {
           if(itemStatus == 81)
             moveToMain(req,res,9,data,itemStatus,mainTable,revisionTable);
           else
             processStatus(req,res,9,data);
            //for a ReleasedAndSubmitted menu no need to do anything it
            //will remain in ACtion Q tab in same status(88)
            //no need of further Processing  
            //Because if we change status to Rejected then it is not possible
            //To differiante between this and normal reject
            //maybe need another status like ReleasedAndRejected kind of thing 
         }
     });




}

function moveToMain(req,res,stat,data,itemStat,mainTable,revisionTable){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    var revisionFlag = 1;
   
    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          relStat: function (callback){utils.revisionData(revisionTable,mainTable,data.orgId,data.businessType,menuCode,revisionFlag,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeMenuStatus(req,res,9,data,itemStat,mainTable,revisionTable); 
         }
     });
}

function changeMenuStatus(req,res,stat,data,itemStat,mainTable,revisionTable){
    
    var menuArr = data.menuCodes;
    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = ''; //no reason is currently set for reject operation
     async.forEachOf(menuArr,function(value,key,callback1){
       var menuCode = value;
       async.parallel({
         chngStatus: function(callback){statutils.changeMenuStatus(mainTable,data.orgId,data.businessType,data.userId,menuCode,87,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             callback1();
            }
         });
         },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
           removeRevisionData(req,res,9,data,itemStat,mainTable,revisionTable);
          }
     });
}

function removeRevisionData(req,res,stat,data,itemStat,mainTable,revisionTable){
    var async = require('async');
    var menuArr = req.body.menuCodes;

    var menuutils = require('./menuutils');
    var isError = 0;
   
    var reason = '';
      async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
             delStat: function(callback) {menuutils.deleteMenu(revisionTable,data.orgId,data.businessType,menuCode,callback); }
             },
         
          function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err) {
            
               processStatus(req,res,6,data);
            }
      else
          {
          getItemsData(req,res,9,data); 
           }
          });
     
}


function getItemsData(req,res,stat,data){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = 'Revision_'+orgId+'_menus_items_link'; //TODO: move to a common definition file
    
     async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
     async.parallel({
         linkData: function(callback){utils.menuLinks(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           if (isError == 0 && results.linkData.errorCode == 0) {
             itemsArr.push(results.linkData.itemCodes);
             callback1();
            }
           else
            callback1(); //no data cases
         }); 
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            deleteMenuItemLinks(req,res,9,data,itemsArr,link_tabl_name);
          }
     });
}

//Delete all item links for the passed menu array first
//Then change status of items

function deleteMenuItemLinks(req,res,stat,data,itemsArr,link_tabl_name){
    
    var async = require('async');
    var statutils = require('./statusutils');
    var menuutils = require('./menuutils');
    var isError = 0;
    var menuArr = data.menuCodes;
    
      async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
       async.parallel({
         linkData: function(callback){menuutils.delinkMenuItemCode(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           else 
             callback1();
          }); 
        },
         function(err){
         if(err || isError == 1) {
              processStatus(req,res,6,data);
             }
      else
          {
           getMenuDetails(req,res,9,data,itemsArr,link_tabl_name);
          }
    });
}

//if no row in link table for the item, change item status to Entered
//otherwise just leave it, because item may be linked to other menus
//NO need of doing item status change in Reject Flow now

function getMenuDetails(req,res,stat,data){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
   
   var mastTable = data.orgId+'_menus_master'; //since 89 row does not come here 
    async.forEachOf(data.menuCodes,function(key,value,callback){
    var menuCode = key;
    utils.menuDetail(mastTable,data.orgId,data.businessType,menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              isError =1 ;
              callback();
              }
      else  {
            resultArr.push(result.masterData);
            callback();
           }
    });
    },
    function(err){
         if(err || isError == 1) 
               processStatus(req,res,6,data);
       else
          {
          data.menuData = resultArr;
          processStatus(req,res,9,data); 
         }
     });
    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




