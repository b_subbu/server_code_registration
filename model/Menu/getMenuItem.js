function processRequest(req,res,data){
    if(typeof req.body.itemCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus used only for basecurrency");
       debuglog("TODO see alternate later")
}

function checkMasterStatus(req,res,data){
    
    var uid = data.userId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var utils = require('./utils');
     var isError = 0;
    
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
       
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
         
            if(isError == 0) {
              //data.orgId = results.usrStat.orgId;
              //data.businessType = results.usrStat.bizType;
              data.baseCurrency = results.usrStat.baseCurrency;
              getItemCode(req,res,9,data);
            }
          }); 
}


function getItemCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var itemCode = req.body.itemCode;
   var tabId = req.body.tabId;
  
      utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
            if(result.itemStatus == 84){
             //because InUse Items data are stored in Revision table until approved
             //also InUse items can be directly viewed under Content TAB
              if(tabId == 502)
                tabl_name = 'Revision_'+result.tblName;
              else
                tabl_name = result.tblName;
            }
            else
              tabl_name = result.tblName; 
            data.itemCode = itemCode;
            mastFields = result.mastFields;
            getMasterData(req,res,9,data,mastFields,tabl_name);
            }
          }
        });
} 
//TODO: check and retire utils.masterFieldsOrgId


//Note: the fields in master table can change from org to org
// so should use the master fields definitions
function getMasterData(req,res,stat,data,mastFields,table_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
  
    utils.masterData(data.orgId,data.businessType,data.itemCode,table_name,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 itemMasterData = result.masterData;
                  formatMasterData(req,res,9,data,mastFields,itemMasterData); 
              }
        });
} 


function formatMasterData(req,res,stat,data,mastFields,mastData) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var itemMasters = {};
   var visualArray = [];
      async.forEachOf(mastData,function(key1,value1,callback1){
        debuglog("Start processing master data");
        debuglog(key1);
         async.forEachOf(key1,function(key2,value2,callback2){
          if(value2 == 'status_id')
            itemMasters['status'] = key2; //because status_id is not defined in master fields
          if(value2 == 'sellerIds')
            itemMasters['sellerIds'] = key2;
            async.forEachOf(mastFields,function(key3,value3,callback3){
            var key_name = key3.name;
              if(key_name == 'itemVisual'){
                visualArray = key1.itemVisual;
                visualIndex = value3;
                callback3(); //visuals is seperate grid no need to send title
               }
            else if(key_name == value2)  {
                   itemMasters[key_name] = key2;
                   callback3();
                 }
            else
              callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             callback2(); 
           }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            callback1(); 
           }
        });
      },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          mastFields.splice(visualIndex,1); 
          //data.columns = mastFields;
          data.menuItemDetails = itemMasters;
          if(visualArray == null)
            data.visuals = []; 
          else
           data.visuals = visualArray;
       
          getSelectionFields(req,res,9,data);
        }
      });
 

} 

function getSelectionFields(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
  
  //For Edited Items we save the field defintions with itemCode##REvision
  //in org_itemdetail_map until Approved
      var tabId = req.body.tabId;
  
       if(tabId == 502)
                   var itemCode = data.itemCode+'##Revision';
       else
                  var itemCode = data.itemCode;
         
    utils.selectionFieldsOrgId(data.orgId,data.businessType,itemCode,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  selectionFields = result.selectFields;
                  selectionTable = result.table_name_detail;
                  getSelectionData(req,res,stat,data,selectionFields,selectionTable);      
                  }
            else {
                  data.selectionCriteria = [];
                  processStatus(req,res,9,data); 
                  //definitions must exist by now;or trying to get data before even create
                  //or item code in details is not created somehow may be user had just completed master section and saved
                  //for all such cases we just return data without selection fields
                  }
        }
     });
} 

//Note: the fields in selection table can change from org to org and item code to itemcode
// so should use the fields definitions
function getSelectionData(req,res,stat,data,selectionFields,table_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var selectFields = [];
   var tabId = req.body.tabId;
 
         if(tabId == 502)
                tabl_name = 'Revision_'+table_name;
              else
                tabl_name = table_name;
         
    utils.selectionData(data.orgId,data.businessType,data.itemCode,tabl_name,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
             for(idx = 0; idx<selectionFields.length;idx++){
                if(selectionFields[idx].name == 'itemDetailCode' || selectionFields[idx].name == 'isDeleted')
                   continue;
                else {
           
                   var tmpArray = {name:selectionFields[idx].name,label:selectionFields[idx].label,typeId:selectionFields[idx].type,additionalTypeId:selectionFields[idx].additional};
                       tmpArray.visibility = selectionFields[idx].visibility;
                       tmpArray.width = selectionFields[idx].width;
                       tmpArray.purpose = selectionFields[idx].purpose;
                       tmpArray.editable = selectionFields[idx].editable;
                       tmpArray.required = selectionFields[idx].required;
                       if(selectionFields[idx].value !== null)
                         tmpArray.value = selectionFields[idx].value;
                   selectFields.push(tmpArray);
                  }
                }
                  data.selectionCriteriaColumns = selectFields;
             
                  selectionData = result.selectionData;
                  formatSelectionData(req,res,9,data,selectionFields,selectionData); 
              }
        });
} 

function formatSelectionData(req,res,stat,data,selectionFields,selectionData) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var itemMasters = [];
   var key_sequence = 0;
      async.forEachOf(selectionData,function(key1,value1,callback1){
      subitemMasters = [];
         async.forEachOf(key1,function(key2,value2,callback2){
            async.forEachOf(selectionFields,function(key3,value3,callback3){
            var key_name = key3.name;
            var key_label = key3.name;
            var key_sequence = key3.sequence;
            if(key_name == value2)  {
                  //subitemMasters[key_label]= key2;
                  //need to get sequence of detail fields here to sort later
                  var tmpArray = {};
                  tmpArray[key_label] = key2;
                  tmpArray['sequence'] = key_sequence;
                  subitemMasters.push(tmpArray); 
                  callback3();
                 }
            else
              callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             callback2(); 
           }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
           itemMasters.push(subitemMasters);
            callback1(); 
           }
        });
      },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          sortSelectionData(req,res,9,data,itemMasters);
        }
      });
 

} 

//We have to sort the selection criteria field as per order of detail fields
//because this order is important while rendering 
//also we can't get a sorted array above since can use fields only at final level
function sortSelectionData(req,res,stat,data,selectionData) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var itemMasters = [];
   var key_sequence = 0;
         async.forEachOf(selectionData,function(key1,value1,callback1){
          var subitemMasters = {};
           var selectData = selectionData[value1];
           selectData.sort(function(a,b) {return a.sequence-b.sequence});
            async.forEachOf(selectData,function(key2,value2,callback2){
              async.forEachOf(key2,function(key3,value3,callback3){
                   if(value3 != 'sequence')  {
                    subitemMasters[value3] = key3;
                     callback3();
                    }
               else
                 callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
            callback2(); 
           }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
           itemMasters.push(subitemMasters);
            callback1(); 
           }
        });
      },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          data.selectionCriteria = itemMasters;
          //processStatus(req,res,9,data);
          processVisuals(req,res,9,data);
        }
      });
 

} 
//This is to add URL before visual files
//since URL changes on restart, so can't save it while create
function processVisuals(req,res,stat,data) {
   var utils = require('./utils');
   var async = require('async');
  
   var itemVisuals = data.visuals;
   
        var regexp = /((http|https):\/\/)?[A-Za-z0-9\.-]{3,}\.[A-Za-z]{2}/;	
     
      async.forEachOf(itemVisuals,function(key1,value1,callback1){
     
       var vURL = key1.src; 
      
        if(regexp.test(vURL))
            ; //video url has http or https in beginning  so nothing to do
          else { key1.src = global.nodeURL+key1.src; }
          callback1();
              },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
         
          //processStatus(req,res,9,data);
          getSelectionCriteriaColumnTypes(req,res,9,data);
        }
      });
              
}

function getSelectionCriteriaColumnTypes(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var isError = 0;
  
  
   
    async.parallel({
         columnTypes: function(callback) {utils.columnTypes(data.businessType,data.baseCurrency,callback); },
        },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0 && results.columnTypes.errorCode != 0) {
              processStatus (req,res,results.columnTypes.errorCode,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
                    data.selectionCriteriaColumnTypes = results.columnTypes.typeData;
                    //processStatus(req,res,9,data);
                    getSellerDetails(req,res,stat,data);
              }
          }); 
}

function getSellerDetails(req,res,stat,data){

  var async = require('async');
   var utils = require('./utils');
   var sellerMasters = [];
   debuglog("This is the data");
   debuglog(data.menuItemDetails);
   if(data.menuItemDetails.sellerIds)
     var sellerIds = data.menuItemDetails.sellerIds;
   else
     var sellerIds =[];
   var tblName = data.orgId+'_sellers';
 
      utils.sellersData(tblName,sellerIds,function(err,result){
           debuglog(result);
           debuglog("after getting result from Sellers table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
           data.sellerDetails = result;
           getSellers(req,res,9,data);
           //processStatus(req,res,9,data);
        }
      });
   


} 

function getSellers(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./utils');

 var tabl_name = data.orgId+'_sellers';
 
  datautils.sellers(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Sellers table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.sellers = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 
      
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



