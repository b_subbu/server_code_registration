function processRequest(req,res,data){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.itemCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItemCode(req,res,9,data);
}

function getItemCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var itemCodes = req.body.itemCodes;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.checkItemCodeRevision(orgId,bizType,itemCode,function(err,results) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (results.errorCode != 0 && results.errorCode != 5)) {
              errorCode = results.errorCode;
              isError = 1;
              //callback();
              }
            if( isError == 0 && results.itemStatus != 85 ) {
              errorCode = 7; //item not in correct status 'REady2Auth'; status id is hard-coded here
              isError = 1;
              //callback();
              }  
            if(isError == 0) {
                mastFields = results.mastFields;
                mastTable = results.tblName;
                revisionmastTable = results.rtblName;  //same table name for all item codes
            //callback();
            }
            callback();  
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
           
               data.itemCodes = itemCodes;
              changeItemStatus(req,res,9,data,82,mastFields,revisionmastTable); //82->Rejected
              }
            }
          });
} 

function changeItemStatus(req,res,stat,data,itemStat,mastFields,revisionmastTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
    
    
    
    var reason = '';
   async.forEachOf(data.itemCodes,function(key,value,callback){
      itemCode = key;
       statutils.changeStatus(revisionmastTable,data.orgId,data.businessType,data.userId,itemCode,itemStat,reason,function(err,result) {
          if(err) {
                isError = 1;
               callback();
             }
           else {
            callback();
            }
        });
        },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  getDetailsData(req,res,9,data,mastFields,mastTable);
           
              }
            }
          });
     
}

function getDetailsData(req,res,stat,data,mastFields,mastTable){

    var async = require('async');
  var statutils = require('./statusutils');
    var isError = 0;
    var itemMasters = [];
    
  async.forEachOf(data.itemCodes,function(key,value,callback){
      itemCode = key;
     data.itemCode = itemCode;  
  statutils.detailsData(data,mastFields,mastTable,function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               callback();
             }
           else {
             delete data.itemCode;
             itemMasters.push(results.menuItemDetails);
             callback();
             }
        });
         },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  data.menuItemDetails = itemMasters;
                  getItemStatName(req,res,9,data);
              }
            }
          });

}

function getItemStatName(req,res,stat,data){

    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    var itemMasters = [];
    async.forEachOf(data.menuItemDetails,function(key,value,callback){
    statId = data.menuItemDetails[value].status;
  statutils.statName(statId,'menuitem_status_master',function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               callback();
             }
           else {
             data.menuItemDetails[value].status = results;
             callback();
             }
        });
         },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  processStatus(req,res,9,data);
                  
              }
            }
          });

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;






