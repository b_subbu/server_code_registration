function processRequest(req,res,data){
   if(typeof req.body.itemCodes === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
      getItemCode(req,res,9,data);
}


function getItemCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var itemCodes = req.body.itemCodes;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.checkItemCodeRevision(orgId,bizType,itemCode,function(err,results) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (results.errorCode != 0 && results.errorCode != 5)) {
              errorCode = results.errorCode;
              isError = 1;
              //callback();
              }
            if( isError == 0 && results.itemStatus != 85 ) {
              errorCode = 7; //item not in correct status 'Ready2Auth'; status id is hard-coded here
              isError = 1;
              //callback();
              }  
            if(isError == 0) {
                mastFields = results.mastFields;
                mastTable = results.tblName;
                revisionMastTable = results.rtblName;    
                //callback();
            }
           callback(); 
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
           
               data.itemCodes = itemCodes;
              //changeItemStatus(req,res,9,data,81,tabl_name);
              //no need of item status change master status should remain 'InUse'
              createMenuItemDetailsData(req,res,9,data,mastFields,mastTable,revisionMastTable)
              }
            }
          });
} 

function createMenuItemDetailsData(req,res,stat,data,mastFields,mastTable,revisionmastTable){
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
  
    var itemCodes = req.body.itemCodes;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
  
    utils.updateDetailsFromRevision(data.orgId,data.businessType,itemCode,mastTable,mastFields,revisionMastTable,function(err,result) {
        
          if(err) {
               isError = 1;
               //processStatus(req,res,6,data);
               callback();
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              //processStatus(req,res,result.errorCode,data);
              isError = 1;
              callback();
              }
            else
              callback();
           }
          });
        },
          function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                  //getSelectionFields(req,res,9,data,mastFields,mastTable,revisionmastTable);
                  createSelectionFieldsFromRevision(req,res,9,data,mastFields,mastTable,revisionmastTable);
         
                 }
            }
          });   

}

//In save item details new fields are saved in itemCode##Revision
//So now just drop the original itemCode and rename ##Revision
//To original
function createSelectionFieldsFromRevision(req,res,stat,data,mastFields,mastTable,revisionmastTable){
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
  
    var itemCodes = req.body.itemCodes;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
  
    utils.updateSelectionFieldsFromRevision('org_itemdetail_map',data.orgId,data.businessType,itemCode,function(err,result) {
        
          if(err) {
               isError = 1;
               //processStatus(req,res,6,data);
               callback();
             }
         else {
              callback();
           }
          });
        },
          function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           getSelectionFields(req,res,9,data,mastFields,mastTable,revisionmastTable);
           }
      });   

}

function getSelectionFields(req,res,stat,data,mastFields,mastTable,revisionmastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var itemCodes = req.body.itemCodes;
   var selectionFields = req.body.selectionCriteriaColumns; 
  
  var orgId = data.orgId;
  var bizType = data.businessType;
  
  //var promise= db.get('org_itemdetail_map').remove({$and:[{org_id:orgId,bus_type:bizType,itemCode:itemCode}]});
   //because user might have removed/added fields in selection critieria so delete and recreate
   //if user adds/removes columns while "Modify" it would be an issue
   //TODO handle it here if it happens
      itemCode = itemCodes[0]; //because fields and table will be same for all items of a given ORGID
  
    
      utils.selectionFieldsOrgId(data.orgId,data.businessType,itemCode,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  selectionFields = result.selectFields;
                  selectionTable = result.table_name_detail;
                  createMenuItemSelectionData(req,res,stat,data,selectionFields,selectionTable,mastFields,mastTable,revisionmastTable);      
                  }
            else {
                  processStatus(req,res,3,data);
                  }
        }
      });
} 

function createMenuItemSelectionData(req,res,stat,data,selectFields,selectTable,mastFields,mastTable,revisionmastTable){
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var itemCodes = data.itemCodes;
    var revisionselectTable = 'Revision_'+selectTable;
    
   async.forEachOf(itemCodes,function(key1,value1,callback1){
    
        itemCode = key1;
   
          utils.updateSelectionsFromRevision(data.orgId,data.businessType,itemCode,selectTable,selectFields,revisionselectTable,function(err,result) {
          if(err) {
               isError = 1;
               //processStatus(req,res,6,data);
               callback1();
               }
           else {
              callback1();
            }
          });
        },
       function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
    if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
            clearRevisionData(req,res,9,data,revisionmastTable,revisionselectTable,mastFields,mastTable);
        }
          
      });
}

//Can't use utils delete because it will delete images also
//no need to do promise and wait since a failure in delete does not create any problem
//except redundant data
function clearRevisionData(req,res,stat,data,revisionmastTable,revisionselectTable,mastFields,mastTable){
 var async = require('async');
   
 async.forEachOf(data.itemCodes,function(key1,value1,callback1){
    
        itemCode = key1;
  
db.get(revisionmastTable).remove({org_id: data.orgId,bus_type: data.businessType,itemCode: itemCode});
db.get(revisionselectTable).remove({org_id: data.orgId,bus_type: data.businessType,itemCode: itemCode});
callback1();
     },
       function(err){
      if(err){
             processStatus (req,res,6,data);
                      
            }
          else {
          getDetailsData(req,res,9,data,mastFields,mastTable);
        }
          
      });
    
   
}   

function getDetailsData(req,res,stat,data,mastFields,mastTable){

    var async = require('async');
  var statutils = require('./statusutils');
    var isError = 0;
    var itemMasters = [];
 async.forEachOf(data.itemCodes,function(key,value,callback){
      itemCode = key;
     data.itemCode = itemCode;  
  statutils.detailsData(data,mastFields,mastTable,function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               callback();
             }
           else {
             delete data.itemCode;
             itemMasters.push(results.menuItemDetails);
             callback();
             }
        });
         },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  data.menuItemDetails = itemMasters;
                  //processStatus(req,res,9,data);
                  getItemStatName(req,res,9,data);
              }
            }
          });

}

function getItemStatName(req,res,stat,data){

    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    var itemMasters = [];
    async.forEachOf(data.menuItemDetails,function(key,value,callback){
    statId = data.menuItemDetails[value].status;
  statutils.statName(statId,'menuitem_status_master',function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               callback();
             }
           else {
             data.menuItemDetails[value].status = results;
             callback();
             }
        });
         },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  processStatus(req,res,9,data);
                  
              }
            }
          });

}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;






