function getMenusData(tablName,orgId,bizType,pgid,size,sarr,callback){ 
  var totalRows = 0;
   var data={};
   var resultArr = [];
   var page = parseInt(pgid);
   var skip = page > 0 ? ((page - 1) * size) : 0;
   var moment = require('moment');
 
   var promise = db.get(tablName).find({$and:sarr},{sort:{update_time: -1},limit: size,skip: skip});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {menuCode:doc.menuCode,menuName:doc.menuName,menuDescription:doc.menuDescription,status:doc.status_id,createDate:moment(doc.create_time).format('L')};
        resultArr.push(tmpArray);
         }); 
   promise.on('complete',function(err,doc){
          data.errorCode = 0;
          data.tblName = tablName;
          data.masterData = resultArr;
          getMenuStatName(data,callback);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getMenuStatName(menuData,callback){

 var async = require('async');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
    
   var mastData = menuData.masterData;
      async.forEachOf(mastData,function(key1,value1,callback1){
          subitemMasters = {};
             var statID=key1.status;
           statutils.statName(statID,'menus_status_master',function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 key1.status =result;
                 callback1(); 
              }
           }); 
        },
       function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           callback(null,menuData);
        }
      });



}

function getMenuStatusName(menuData,callback){

   var async = require('async');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
    
           var mastData = menuData.masterData;
           var statID=mastData.status;
           statutils.statName(statID,'menus_status_master',function(err,result) {
           if(err) callback(6,null);
         else {
                 
                 mastData.status =result;
                 callback(null,menuData)
                }
           }); 
      



}

function checkMenuCode(tablName,orgId,bizType,menuCode,callback){
   var totalRows = 0;
   var data={};
   var itemStatus = '';
   var promise = db.get(tablName).find({$and:[{org_id:orgId,bus_type:bizType,menuCode:menuCode}]});
   promise.each(function(doc){
        totalRows++;
        itemStatus = doc.status_id;
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 3; //menuCode not found
         data.tblName = tablName;
        callback(null,data);   
        }
        else {
          data.errorCode = 5; //menuCode found
          data.tblName = tablName;
          data.itemStat = itemStatus;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getMenuData(tablName,orgId,bizType,menuCode,callback){ 
  var totalRows = 0;
   var data={};
   var menuData = "";
   var menuDescr = "";
   var menuName = "";
   var menuStatus = "";
    
   var promise = db.get(tablName).find({$and:[{org_id:orgId},{bus_type:bizType},{menuCode:menuCode}]});
   promise.each(function(doc){
        totalRows++;
        menuData = doc.menuData; 
		    menuDescr = doc.menuDescription;
        menuName = doc.menuName;
        menuStatus = doc.status_id;
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 0; 
         data.countItems = 0; 
         callback(null,data);   
        }
        else {
          if(totalRows == 1) {
          data.errorCode = 0;
          data.countItems = 1;
          data.menuData = menuData;
		      data.menuDescription = menuDescr;
          data.menuName = menuName;
          data.menuStatus = menuStatus;
          callback(null,data);
        }
        else
          callback(6,data); //more than 1 row is error
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getMenuDetail(tablName,orgId,bizType,menuCode,callback){ 
  var totalRows = 0;
   var data={};
   var resultArr = [];
   var tmpArr = {};
   var moment = require('moment');
   var promise = db.get(tablName).find({$and:[{org_id:orgId},{bus_type:bizType},{menuCode:menuCode}]});
   promise.each(function(doc){
        totalRows++;
        tmpArr = {menuCode:doc.menuCode,menuName:doc.menuName,menuDescription:doc.menuDescription,status:doc.status_id,createDate:moment(doc.create_time).format('L')};
       }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 1; //no item codes for this business 
         callback(null,data);   
        }
        else {
          data.errorCode = 0;
          data.tblName = tablName;
          data.masterData = tmpArr;
          getMenuStatusName(data,callback)
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function createMenuItemCodeLink(linkTable,orgId,bizType,menuCode,itemCode,nodeIs,callback){ 
 
   debuglog("both item and group will be inserted with itemCode");
   debuglog("but nodeIs will be different for both");
   debuglog(menuCode);
   debuglog(itemCode);
    var promise = db.get(linkTable).insert({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode,
    itemCode: itemCode,
    nodeIs: nodeIs,
    update_time: new Date()
   });
  promise.on('success',function(doc){
    callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function createMenuData(tblName,orgId,bizType,menuCode,mastData,callback){ 
 
    var promise = db.get(tblName).insert({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode,  
    menuName: mastData.name,
    menuDescription: mastData.description,
    create_time: new Date(),
    update_time: new Date(),
    status_id: 0 //will be updated on statchange call
    
   });
  promise.on('success',function(doc){
    callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function getMaxMenuCode(tblName,orgId,bizType,callback){ 
 
  var menuCode = null;
  var totalRows = 0;
    /**var promise = db.get(tblName).aggregate([{
    $group: {_id:'menuCode', maxMenuCode: {$max: "$menuCode"}}
    }]);
    aggregate not available in monk still---
    */
    var promise  = db.get(tblName).find({},{sort:{menuCode:-1},limit:1});
    promise.each(function(doc){
        totalRows++;
        menuCode = doc.menuCode; 
         }); 
  
    promise.on('complete',function(doc){
     if(totalRows == 0)
       callback(null,null);
     else
      callback(null,menuCode);
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}


function getMenuFieldsFromBizType(bizType,callback){
   var totalRows = 0;
   var resultArr = [];
   var data = {};
  
   var promise = db.get('businesstype_menusmap_master').find({bus_type:bizType},{sort: {field_order: 1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {name: doc.field_name,label: doc.field_label,isMandatory: doc.mandatory,sequence:doc.field_order};
        resultArr.push(tmpArr);
        }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
           callback(null,3);
       }
       else {
           data.menuFields = resultArr;
           callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function getRootFromData(menuData,callback){

   var async = require('async');
   var isExists = false; 
   var isError = 0;
   var data = {};
   
   data.id = menuData.menuLinkage[0].id;  
   data.name = menuData.menuName;
   data.description = menuData.menuDescription;
                   
       callback(null,data);
         
}

function setRootMenuCode(menuData,menuCode,callback){

   var async = require('async');
   var isExists = false; 
   var isError = 0;
   var data = {};
   
   var links = menuData.menuLinkage;
     async.forEachOf(links[0],function(key1,value1,callback1){
              if(key1.parent == '#'){  
                   isExists = true;
                   key1.id = menuCode;
                   }
             callback1();
           },
           function(err){
             if(err){
               callback(6,null);
                }
            else {
               if (isExists == false || isError == 1){
                callback(6,null);
                }
              else
                callback(null,menudata);
           }
         });
}

function updateMenuData(tblName,orgId,bizType,menuCode,mastData,menuData,callback){ 
 
    var promise = db.get(tblName).update({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode}, 
    {$set:{ 
    menuName: mastData.name,
    menuDescription: mastData.description,
    update_time: new Date(),
    status_id: 0}
    });
  promise.on('success',function(doc){
    callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function deleteMenuItemCodeLink(linkTable,orgId,bizType,menuCode,callback){ 
 
    var promise = db.get(linkTable).remove({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode,
   });
  promise.on('success',function(doc){
    callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function getMenuItemCodeLink(linkTable,orgId,bizType,menuCode,callback){ 
 
  var itemCodes = [];
  var totalRows = 0;
  var data = {};
  
    var promise = db.get(linkTable).find({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
  promise.each(function(doc){
        totalRows++;
        itemCodes.push(doc.itemCode);
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 3; //no item codes found
         callback(null,data);   
        }
        else {
          data.errorCode = 0; //menuCode found
          data.itemCodes = itemCodes;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

//same as above but including nodeIs in return param
function getMenuItemCodeLinks(linkTable,orgId,bizType,menuCode,callback){ 
 
  var itemCodes = [];
  var totalRows = 0;
  var data = {};
  
    var promise = db.get(linkTable).find({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
  promise.each(function(doc){
        totalRows++;
        var tmpArr = {itemCode:doc.itemCode,nodeIs:doc.nodeIs};
        itemCodes.push(tmpArr);
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.errorCode = 3; //no item codes found
         callback(null,data);   
        }
        else {
          data.errorCode = 0; //menuCode found
          data.itemCodes = itemCodes;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getItemMenuCodeLink(linkTable,orgId,bizType,itemCode,callback){ 
 
  var menuCodes = [];
  var totalRows = 0;
  var data = {};
  
    var promise = db.get(linkTable).find({
    org_id: orgId,
    bus_type: bizType,
    itemCode: itemCode
    });
  promise.each(function(doc){
        totalRows++;
        menuCodes.push(doc.menuCode);
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.totalRows = 0; //no item codes found
         callback(null,data);   
        }
        else {
          data.totalRows  = totalRows; 
          data.menuCodes = menuCodes;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function createReleaseData(tablName,releaseTablName,orgId,bizType,menuCode,callback){
 
   db.get(releaseTablName).remove({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
    
  //Since if released menu is editted then a row will be already present  
  var promise = db.get(releaseTablName).insert({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
  promise.on('success',function(doc){
      getMenuData(tablName,orgId,bizType,menuCode,function(err,result){
       if(err || result.errorCode != 0 || result.countItems != 1)
        callback(6,null);
       else
        updateReleaseData(releaseTablName,orgId,bizType,menuCode,result.menuName,result.menuDescription,result.menuData,callback);
      });
    });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }
 

function updateReleaseData(releaseTablName,orgId,bizType,menuCode,menuName,menuDescr,menuData,callback){
 
  var promise = db.get(releaseTablName).update({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    },
    {$set:{
    menuName: menuName,
    menuData: menuData,
	  menuDescription: menuDescr,
    release_date: new Date()}
    });
  promise.on('success',function(doc){
    createReleaseLinkData(orgId,bizType,menuCode,callback);
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

function createReleaseLinkData(orgId,bizType,menuCode,callback){
 var async = require('async');
 var linkTable = orgId+"_menus_items_link"; //TODO: no need of link tables in RELEASE remove later
 var linkReleaseTable = orgId+"_release_menus_items_link";
 
   getMenuItemCodeLinks(linkTable,orgId,bizType,menuCode,function(err,results){
    if(err) callback(6,null);
    
    else{
     var itemCodes = results.itemCodes;
        async.forEachOf(itemCodes,function(key1,value1,callback1){
          debuglog("start creating release data");
          debuglog(key1);
            createMenuItemCodeLink(linkReleaseTable,orgId,bizType,menuCode,key1.itemCode,key1.nodeIs,function(err,result){
            callback1();
         });
      },
        function(err){
             if(err){
               callback(6,null);
                }
            else {
             callback(null,null);
           }
         });
    
    }
});
}
   
   


function createRecallData(tablName,releaseTablName,releaseArcTablName,orgId,bizType,menuCode,callback){
 var linkReleaseTable = orgId+"_release_menus_items_link";
 
 var promise = db.get(linkReleaseTable).remove({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
  var promise = db.get(releaseTablName).remove({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
  promise.on('success',function(doc){
     createReleaseData(tablName,releaseArcTablName,orgId,bizType,menuCode,callback);
     //same as create release details except now data is created in ARC table
     //release_date in arc table is actually recall data 
      });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }
 
function deleteMenuData(tblName,orgId,bizType,menuCode,callback){ 
 
    var promise = db.get(tblName).remove({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode 
    });
  promise.on('success',function(doc){
    callback(null,null);
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
}

function checkValidMenuTree(tabl_name,orgId,bizType,menuCode,callback){

  getMenuData(tabl_name,orgId,bizType,menuCode,function(err,result){
    if(err)
      callback(6,null);
      
    else{
     var menuTree = result.menuData;
      validateTree(menuTree,callback);
    
    }
  });
}

function validateTree(menuTree,callback){

var traverse = require('traverse');
var isBroken = false;

traverse(menuTree).map(function(acc,x){

    if(this.node.id){
    if(this.node.isLeaf == true || this.node.isLinked == true || this.node.hasChildNodes == true) { ; }
   else {
	      isBroken = true;
      }
   }
  },[]);
  
  if(isBroken == true)
    callback(7,null);
  else
   callback(null,null);
  //it seems traverse is a sync operation it comes to end only after full execution
  //else we need to change this
  

} 

function createMenuLinkNodes_bkup(tblName,orgId,bizType,menuCode,nodesData,callback){ 
 
 var traverse = require('traverse');

 traverse(nodesData).map(function(acc,x){

    if(this.node.id){
     var promise = db.get(tblName).insert({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode,  
    nodeId: this.node.id,
    nodeData: this.node,
    nodeParent: this.node.parent,
    create_time: new Date(),
    update_time: new Date()
    });
   }
  },[]);
  
 
    callback(null,null); //just call back no checkings are possible it seems
    //if need of checkings or if above does not work then need to do
    //async instead of traverse
 }

function createMenuLinkNodes(tblName,orgId,bizType,menuCode,nodesData,callback){ 
 
 var traverse = require('traverse');
 var linkData = [];

 traverse(nodesData).map(function(acc,x){

    if(this.node.id){
    var tmpArray = {};
    tmpArray.nodeId= this.node.id,
    tmpArray.nodeData= this.node,
    tmpArray.nodeParent= this.node.parent
    linkData.push(tmpArray);
   }
  },[]);
    var promise = db.get(tblName).update({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode},
    {$set:{menuData: linkData,
    update_time: new Date() }
    });
    promise.on('success',function(doc){
    callback(null,null);
    });
    promise.on('error',function(doc){
    callback(6,null);
    });

 
    //just call back no checkings are possible it seems
    //if need of checkings or if above does not work then need to do
    //async instead of traverse
 }


 function getMenuNodes(tablName,orgId,bizType,menuCode,callback){ 
 
  var totalRows = 0;
  var data = {};
  var menuNodes = [];
  
    var promise = db.get(tablName).find({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
  promise.each(function(doc){
        totalRows++;
        menuNodes.push(doc.menuData);
         }); 
   promise.on('complete',function(err,doc){
      if (totalRows == 0){
         data.countItems = 0; 
         callback(null,data);   
        }
        else {
          data.countItems  = totalRows; 
          data.menuNodes = menuNodes;
          callback(null,data);
        }
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function createRevisionData(revisionTablName,mainTablName,orgId,bizType,menuCode,revisionFlag,callback){
   db.get(mainTablName).remove({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
    
  var promise = db.get(mainTablName).insert({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
  promise.on('success',function(doc){
      getMenuData(revisionTablName,orgId,bizType,menuCode,function(err,result){
       if(err || result.errorCode != 0 || result.countItems != 1)
        callback(6,null);
       else
        updateRevisionData(mainTablName,orgId,bizType,menuCode,result.menuName,result.menuDescription,result.menuData,revisionFlag,callback);
        
      
      });
    });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }
 

function updateRevisionData(mainTablName,orgId,bizType,menuCode,menuName,menuDescr,menuData,revisionFlag,callback){
 
  var promise = db.get(mainTablName).update({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    },
    {$set:{
    menuName: menuName,
    menuData: menuData,
	  menuDescription: menuDescr,
    create_time: new Date(),
    update_time: new Date(),
    }
    });
  promise.on('success',function(doc){
    //callback(null,null)
    createRevisionLinkData(orgId,bizType,menuCode,revisionFlag,callback);
       });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

function createRevisionLinkData(orgId,bizType,menuCode,revisionFlag,callback){
 var async = require('async');
 if(revisionFlag == 2) {
 var revisionLinkTable = orgId+"_menus_items_link";
 var mainLinkTable = 'Revision_'+orgId+"_menus_items_link";
 }
 else {
 var revisionLinkTable = 'Revision_'+orgId+"_menus_items_link";
 var mainLinkTable = orgId+"_menus_items_link";
 
 }
   db.get(mainLinkTable).remove({
    org_id: orgId,
    bus_type: bizType,
    menuCode: menuCode
    });
   getMenuItemCodeLinks(revisionLinkTable,orgId,bizType,menuCode,function(err,results){
   if(err) callback(6,null);
    else{
       var itemCodes = results.itemCodes;
       debuglog(itemCodes);
       async.forEachOf(itemCodes,function(key1,value1,callback1){
            debuglog("start creating revision data");
            debuglog(key1);
            createMenuItemCodeLink(mainLinkTable,orgId,bizType,menuCode,key1.itemCode,key1.nodeIs,function(err,result){
            callback1();
         });
      },
       function(err){
             if(err){
               callback(6,null);
                }
            else {
             db.get(revisionLinkTable).remove({
             org_id: orgId,
             bus_type: bizType,
             menuCode: menuCode
             });
             callback(null,null);
           }
         });
    
    }
});
}
   
   


 exports.menuDatas = getMenusData;
 exports.checkMenuCode = checkMenuCode;
 exports.menuData = getMenuData;
 exports.linkMenuItemCode = createMenuItemCodeLink;
 exports.createMenuMaster = createMenuData;
 exports.maxMenuCode = getMaxMenuCode;
 exports.menuFieldsBizType =  getMenuFieldsFromBizType;
 exports.parseJson = getRootFromData;
 exports.rootMenuCode = setRootMenuCode;
 exports.updateMenu = updateMenuData;
 exports.delinkMenuItemCode = deleteMenuItemCodeLink;
 exports.menuLinks = getMenuItemCodeLink;
 exports.itemLinks = getItemMenuCodeLink;
 exports.releaseData = createReleaseData;
 exports.recallData = createRecallData;
 exports.menuDetail = getMenuDetail;
 exports.deleteMenu = deleteMenuData;
 exports.checkMenuTree = checkValidMenuTree;
 exports.createMenuNodes = createMenuLinkNodes;
 exports.menuNodes = getMenuNodes;
 exports.revisionData = createRevisionData;
 exports.menuLinksWithNodeIs = getMenuItemCodeLinks;
 

 

 
