function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   var userdata = "";
   var files = [];
   var idx = 0;
   var jdx = 0;
   var actions = [];
   var formidable = require('formidable');
   
   var form = new formidable.IncomingForm();
   form.uploadDir = app.get('fileuploadfolder')+'/tmp';
   form.multiples = true;
   form.parse(req, function (err, fields) {
     userdata = fields;
    });
    form.on('file',function(name,file) {
    files[idx] = file;
     idx++;
    });
    form.on('end',function() {
       checkMasterStatus(req,res,data,userdata,files);
       debuglog("checkMasterStatus is retained as this is multipart post");
    });
 } 


function checkMasterStatus(req,res,data,userdata,files){

    var uid = userdata.userId;
    var itemCode = userdata.itemCode;
    var async = require('async');
    var utils = require('./utils');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    var type = require('type-detect');
    
   
    data.userId = uid;
      if( typeof uid === 'undefined' || uid === 'undefined' || type(uid) !== 'string'){
         processStatus(req,res,6,data); //system error
         return; 
         }
         
      if( typeof itemCode=== 'undefined' || itemCode === 'undefined' || type(itemCode) !== 'string'){
         processStatus(req,res,6,data); //system error
         return; 
         }
         
    data.itemCode = itemCode;
    
  async.parallel({
    usrStat: function(callback){roleutils.usrStatus(uid,callback); },
    },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    if(isError == 0 && results.usrStat.errorCode != 0) {
      processStatus (req,res,results.usrStat.errorCode,data);
      isError = 1;
      return;
    }
    if( isError == 0 && results.usrStat.userStatus != 9) {
      processStatus (req,res,2,data);
      isError = 1;
      return;
    }
    if (isError == 0) {
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              checkItemCode(req,res,9,data,userdata,files);
            }
          }); 
}


function checkItemCode(req,res,stat,data,userdata,files) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   
   var itemCode = data.itemCode;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   data.itemCode = itemCode;
  
     async.parallel({
         detailStat: function(callback){utils.checkItemCode(orgId,bizType,itemCode,callback); },
         },
         function(err,results) {
          if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
             if(isError == 0 && (results.detailStat.errorCode != 0 && results.detailStat.errorCode != 5)) {
              processStatus (req,res,results.detailStat.errorCode,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
                mastFields = results.detailStat.mastFields;
                mastTable = results.detailStat.tblName;
                saveFiles(req,res,9,data,userdata,files,mastTable);
            }
          }); 
} 

//first we will save the files with IMAGE_0...
//then we will replace the files in userfields with src
//and then sort as per sequence and save the array

function saveFiles(req,res,stat,data,userdata,files,master_table) {

    var filesArray = [];
    var fs = require('fs');
    var mkdirp = require('mkdirp');
    var async = require('async');
    var newfolder = app.get('fileuploadfolder')+data.userId+'/'+data.itemCode+'/';
    mkdirp(newfolder,function(err,made){
     //no need to handle this error generally does not happen
     //if happens it is some severe system error and allow to crash
 
   async.forEachOf(files,function(key,value,callback){
      var oldfile = key.path;
      var oldfilename = key.name;
      var newfile = newfolder+userdata.itemCode+"_IMAGE_"+value;
    
       if(err) { 
       console.log(err);
       fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove file failed");
             console.log(err);
          }
          callback();
         });
       }
     else if(files[value].size == 0) {
        fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove empty file failed");
             console.log(err);
          }
          callback();
         });
       }
      else {
        var tmpArray = {};
        //var newfilename = global.nodeURL+'/upload_files/'+data.userId+'/'+data.itemCode+'/'+data.itemCode+"_IMAGE_"+value;
        
        var newfilename = '/upload_files/'+data.userId+'/'+encodeURIComponent(data.itemCode)+'/'+encodeURIComponent(data.itemCode)+"_IMAGE_"+value;
        
        tmpArray[oldfilename]= newfilename;
        
        filesArray.push(tmpArray);
        fs.rename(oldfile,newfile,function(err) {
         if(err) {
               console.log("Moving file failed");
               console.log(err);
           }
        callback();
      });
     }
    },
   function(err){
      if(err)
       { 
       proessStatus(req,res,6,data);
       return;
       }
      else {
          sortVisualField(req,res,9,data,userdata,filesArray,master_table);
          }
     });
   });
}


//We have to sort the selection criteria field as per order of detail fields
//because this order is important while rendering 
//also we can't get a sorted array above since can use fields only at final level
function sortVisualField(req,res,stat,data,userdata,filesArray,master_table) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
  
   
   //REMOVE above after testing --ONLY FOR TESTING
   var visualsArray = userdata.visuals; 
   

   var visualArray = JSON.parse(visualsArray);
  
   //console.log(visualArray);
   
  
   var key_sequence = 0;
   visualArray.sort(function(a,b) {return a.sequence-b.sequence});
         async.forEachOf(visualArray,function(key1,value1,callback1){
           async.forEachOf(filesArray,function(key2,value2,callback2){
               async.forEachOf(key2,function(key3,value3,callback3){
                   if(value3 == key1.file)  {
                    key1.src = key3;
                     callback3();
                    }
               else
                 callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
            callback2(); 
           }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
             key1.isDeleted = 0;
             callback1(); 
           }
        });
      },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          updateVisualField(req,res,stat,data,userdata,visualArray,master_table);
         }
      });
} 


function updateVisualField(req,res,stat,data,userdata,visualArray,master_table){

   var utils = require('./utils');
   var async = require('async');
   var org_id = data.orgId;
   var biz_type = data.businessType;
   var itemCode = data.itemCode;
   var isError = 0;
      
   var query = {};
   query['itemVisual'] = visualArray;
    //query.update_time = new Date(); may be not required to update time for image update
      var promise = db.get(master_table).update(
        {
        org_id: org_id,
        bus_type: biz_type,
        itemCode: itemCode
        },
        {$set: query}
        );
      promise.on('success',function(doc){
      processStatus(req,res,9,data);
      });
      promise.on('error',function(err){
      processStatus(req,res,6,data);
    });
}
    



function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;



