//NOTE: this will be called from getProductData

function getMenuData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var utils = require('./utils');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'menus_status_master',callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList);
                  }
                  else 
                   getMenuCount(req,res,stat,data,statList);
                 }
          }); 
} 

//note this is not a generic function for filteroptions
//if really needed do a if isFilter = true then
//get master details from a table for that column and then populate 
//them probably in getproduct data generic functions
//anyhow master table for each column can be different 
//so not really generic we can say
//anyhow only few columns in every tab should be still manually manageable

function getFilterOptions(req,res,stat,data,statList) {
   var async = require('async');
   var utils = require('./utils');
   var statutils = require('./statusutils');
   var filterutils = require('./filterUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
   if(tid == 501)   
      var tabl_name = oid+'_menus_master';
   else
      var tabl_name = 'Revision_'+oid+'_menus_master';
     
 
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,'menus_status_master',function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
          
             filterOptions[key_name] = results;
             callback();
          
          }
          }); 
        }
     else
      callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call menus count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var userArray = {org_id: data.orgId };
           searchArr.push(userArray);
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
           getMenuCount(req,res,stat,data,searchArr,tabl_name);
           }
         else  
         getMenuSearch(req,res,stat,data,statList,tabl_name);
         }
      });
} 

function getMenuSearch(req,res,stat,data,statList,tabl_name) {

 var searchInp = req.body.searchParams;
 
 //searchParams will be of format
 //searchParams = {conTitle:{isRange:false, value:”TEST”}, status:{isRange:false,value:81}, createDate: {isRange:true, value:'01/01/2015##01/31/2015'}}
 //but we are building case by case so isRange is not really used for now
 //may be when we build a generic search tool it will be useful
 
 menuName = '';
 statusName = '';
 
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    var searchArr = [];
    
    var userArray = {org_id: oid };
    
    searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
       case 'menuCode':
        var tmpArray = {menuCode: key}; //same for itemCode and Description
           searchArr.push(tmpArray);
         break; 
       case 'menuName':
        var tmpArray = {menuName: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break; 
    
       case 'menuDescription':
        var tmpArray = {menuDescription: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break;
         
      case 'createDate':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var minDaysArr = minDateArr[2].split("T"); //sometimes UI sends with timestamp
        var maxDaysArr = maxDateArr[2].split("T");
        var cmpDate = new Date(minDaysArr[0],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDaysArr[0],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {create_time: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {create_time: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
       break; 
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           debuglog("Once searchArr is finalized we will start searching the data");
           debuglog(searchArr);
           getMenuCount(req,res,9,data,searchArr,tabl_name);
          }
     });
}

function getMenuCount(req,res,stat,data,statList,tabl_name){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   var tid = data.tabId;
         
     //Here we are actually counting menus not items
     //But since function name is already developed with itemCount we reuse it here
     //TODO may be change itemCount to tableCount or something similar to be more generic         
         utils.itemCount(orgId,bizType,tabl_name,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && (results.errorCode != 0 && results.errorCode != 1 )) {
             data = {};
             processStatus (req,res,results.errorCode,data);
             isError = 1;
             return;
            }
         if (isError == 0) {
            data.pageId = pageId;
            data.recordCount = results.countItems;
            getMenuMasterData(req,res,9,data,tabl_name,statList);
            }
         });
}

function getMenuMasterData(req,res,stat,data,mastTable,statList) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var isError = 0;
   var errorCode = 0;
   var menu = [];
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
  //This we need to call seperate since columns change
      utils.menuDatas(mastTable,orgId,bizType,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 1){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data); //no items for the business nothing to do further
              isError = 1;
             //will be handled at count itself, just adding here
            }
            else {
            data.productData = result.masterData; 
            processStatus(req,res,9,data);
            }
          }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
  
exports.getProductData = getMenuData;
exports.getData = getMenuData;





