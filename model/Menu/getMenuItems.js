function processRequest(req,res,data){
    if(typeof req.body.itemCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus used only for basecurrency");
       debuglog("TODO see alternate later")
}


function checkMasterStatus(req,res,data){
    
    var uid = data.userId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
              //data.orgId = results.usrStat.orgId;
              //data.businessType = results.usrStat.bizType;
              data.baseCurrency = results.usrStat.baseCurrency;
     
			        var roles = [];
			        var statList = []; //These are populated only if called from getProductData
              getMasterFields(req,res,9,data,roles,statList);
            }
          }); 
}


function getMasterFields(req,res,stat,data,roles,statList) {
   var async = require('async');
   var utils = require('./utils');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   data.pageId = pageId;
   
    async.parallel({
         selectionData: function(callback){utils.masterFieldsOrgId(data.orgId,data.businessType,callback); },
         statusData: function(callback) {statutils.statList(pid,tid,statList,'menuitem_status_master',callback); },
         columnTypes: function(callback) {utils.columnTypes(data.businessType,data.baseCurrency,callback); },
      },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (type(results.selectionData) !== 'array' && type(results.selectionData) !== 'object')) {
              processStatus (req,res,6,data);
              isError = 1;
              return;
             }
           if(isError == 0 && (type(results.selectionData) !== 'array' && type(results.selectionData) !== 'object')) {
              processStatus (req,res,6,data);
              isError = 1;
              return;
             }
            if(isError == 0 && results.columnTypes.errorCode != 0) {
              processStatus (req,res,results.columnTypes.errorCode,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
                  mastFields = results.selectionData.masterFields;
                  mastTable = results.selectionData.masterTable;
                  if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    data.selectionCriteriaColumnTypes = results.columnTypes.typeData;
                    getFilterOptions(req,res,stat,data,mastFields,mastTable,statList);
                  }
                  else 
                   getSelectionFields(req,res,stat,data,mastFields,mastTable,statList);
                 }
          }); 
} 

//note this is not a generic function for filteroptions
//if really needed do a if isFilter = true then
//get master details from a table for that column and then populate 
//them probably in getproduct data generic functions
//anyhow master table for each column can be different 
//so not really generic we can say
//anyhow only few columns in every tab should be still manually manageable

function getFilterOptions(req,res,stat,data,mastFields,mastTable,statList) {
   var async = require('async');
   var utils = require('./utils');
   var statutils = require('./statusutils');
   var filterutils = require('./filterUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,'menuitem_status_master',function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
          
             filterOptions[key_name] = results;
             callback();
          
          }
          }); 
        }
     else
      callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call alerts count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var userArray = {org_id: data.orgId };
           searchArr.push(userArray);
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
          getSelectionFields(req,res,stat,data,mastFields,mastTable,searchArr);
           }
         else 
           getItemSearch(req,res,stat,data,mastFields,mastTable,statList);
         }
      });
} 

function getItemSearch(req,res,stat,data,mastFields,mastTable,statList) {

 var searchInp = req.body.searchParams;
 
 //searchParams will be of format
 //searchParams = {conTitle:{isRange:false, value:”TEST”}, status:{isRange:false,value:81}, createDate: {isRange:true, value:'01/01/2015##01/31/2015'}}
 //but we are building case by case so isRange is not really used for now
 //may be when we build a generic search tool it will be useful
 
 var isPrice = 0;
 var priceArr = [];
 
 var searchArr = [];
 var userArray = {org_id: data.orgId };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
       case 'itemPrice':
       //TODO include range based filtering for itemPrice from searchUtils 
          isPrice = 1;
          priceArr = key;
          break;
       case 'itemCode':
        var tmpArray = {itemCode: {$regex:key,$options:'i'}}; //same for itemCode and Description
           searchArr.push(tmpArray);
         break; 
       case 'itemName':
        var tmpArray = {itemName: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break; 
    
       case 'itemDescription':
        var tmpArray = {itemDescription: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break; 
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           getSelectionFields(req,res,9,data,mastFields,mastTable,searchArr);
          }
     });
}

function getSelectionFields(req,res,stat,data,mastFields,mastTable,searchArr) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var selectionFields = [];
  
  debuglog("start getting selection Fields");
  debuglog(data);
  
    utils.selectionFieldsBizType(data.businessType,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  selectFields = result.detailFields;
              for(idx = 0; idx<selectFields.length;idx++){
				        if(selectFields[idx].name == 'itemDetailCode' || selectFields[idx].name == 'isDeleted')
                   continue;
                else {
                   var tmpArray = {name:selectFields[idx].name,label:selectFields[idx].label,typeId:selectFields[idx].type,editable:selectFields[idx].editable,required:selectFields[idx].required};
				           if(selectFields[idx].value !== null) tmpArray.value = selectFields[idx].value ;
                      selectionFields.push(tmpArray);
                    }
                 }
                 getItemCount(req,res,stat,data,mastFields,mastTable,selectionFields,searchArr);
               }
            else {
                  data.selectionCriteriaColumns = [];
                  data.productData = [];
                  processStatus(req,res,3,data); 
                 }
        }
     });
} 

function getItemCount(req,res,stat,data,mastFields,tabl_name,selectionFields,statList){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
   var tid = data.tabId;
    
     if(tid == 502)
         tabl_name = 'Revision_'+tabl_name;
         
         debuglog("Start getting count");
         debuglog(statList);
         utils.itemCount(orgId,bizType,tabl_name,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0 && (results.errorCode != 0 && results.errorCode != 1 )) {
             data = {};
             processStatus (req,res,results.errorCode,data);
             isError = 1;
             return;
            }
         if (isError == 0 && results.errorCode == 1){
              data.productData = [];
              data.pageId = pageId;
              data.recordCount = 0;
              //processStatus(req,res,9,data); //no items for the business nothing to do further
              getSellers(req,res,9,data);
              isError = 1;
            }
           if (isError == 0) {
            data.pageId = pageId;
            data.recordCount = results.countItems;
            getItemCodes(req,res,9,data,mastFields,tabl_name,selectionFields,statList);
             }
         });
}

function getItemCodes(req,res,stat,data,mastFields,mastTable,selectionFields,statList) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   var itemCodes = [];
   var selectionCodes = [];
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
      utils.itemCodes(mastTable,orgId,bizType,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 1){
              data.pageId = pageId;
              data.recordCount = 0;
              var visualIndex = 0;
              for(idx = 0; idx < mastFields.length; idx++){
               if(mastFields[idx].name == 'itemVisual' )
                 visualIndex = idx;
              }
              mastFields.splice(visualIndex,1); 
              data.selectionCriteriaColumns = selectionFields;
              data.productData = [];
              processStatus(req,res,9,data); //no items for the business nothing to do further
              isError = 1;
             //will be handled at count itself, just adding here
            }
            else {
            debuglog("This is the result after getting all item Codes");
            debuglog(result.itemCodes);
            tabl_name = result.tblName;  //this is data table 
            itemCodes = result.itemCodes; 
            getMasterData(req,res,9,data,mastFields,tabl_name,itemCodes,selectionFields);
            }
          }
        });
} 

//Note: the fields in master table can change from org to org
// so should use the master fields definitions
function getMasterData(req,res,stat,data,mastFields,table_name,itemCodes,selectionFields) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var masterData = [];
  
  debuglog("Start searching for itemCodes");
  debuglog(itemCodes);
   async.forEachOfSeries(itemCodes,function(key1,value1,callback1){
   var itemCode = key1;
   debuglog("Order of search");
   debuglog(value1);
   debuglog(key1);
   
    utils.masterData(data.orgId,data.businessType,itemCode,table_name,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                debuglog("Got result for");
                debuglog(value1);
                debuglog(key1);
                debuglog(result.masterData);
                 masterData.push(result.masterData);
                 callback1(); 
              }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             debuglog("start getting status names");
             debuglog(masterData);
             getItemStatusName(req,res,9,data,mastFields,masterData,selectionFields);
           }
        });
   
} 

function getItemStatusName(req,res,stat,data,mastFields,mastData,selectionFields) {
   var async = require('async');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var visualURL = '';
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var visualIndex = 0;
    
     debuglog("start getting status names now");
     debuglog(mastData); 
      async.forEachOf(mastData,function(key1,value1,callback1){
        var masterData = mastData[value1][0]; //because mastdata is double array
          subitemMasters = {};
        async.forEachOf(masterData,function(key2,value2,callback2){
             if(value2 == 'status_id'){
              var statID=key2;
           statutils.statName(statID,'menuitem_status_master',function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 mastData[value1][0]['status_id'] = result;
                 callback2(); 
              }
           }); 
          }
          
            else
              callback2();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         
             }
      else {
            debuglog("After getting status names");
            debuglog(mastData);
            formatMasterData(req,res,9,data,mastFields,mastData,selectionFields);
           }
      });
 

} 

function formatMasterData(req,res,stat,data,mastFields,mastData,selectionFields) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var visualURL = '';
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var visualIndex = 0;
     
     debuglog("Before start of formatting");
     debuglog(mastData);
      async.forEachOf(mastData,function(key1,value1,callback1){
        var masterData = mastData[value1][0]; //because mastdata is double array
          subitemMasters = {};
        async.forEachOf(masterData,function(key2,value2,callback2){
             if(value2 == 'status_id')
              subitemMasters['status']=key2;
             async.forEachOf(mastFields,function(key3,value3,callback3){
            var key_name = key3.name;
            if(key_name == value2){
             debuglog(key_name);
             debuglog(value2);
             debuglog(key2);
            if(key_name == 'itemVisual'){
              debuglog("This is item visuals array");
              debuglog(key2);
              debuglog("we will extract only the first element of that arrray");
              if(key2){
                 if(key2[0].type == 62)
                   key2[0].src = global.nodeURL+key2[0].src;
                subitemMasters[key_name] = key2[0];
                }
              else
                subitemMasters[key_name]  = null;
              debuglog("This is the extracted array");
              debuglog("Visual is populated if not null");
              debuglog(subitemMasters);
                callback3(); 
               }
           else  {
                   subitemMasters[key_name] = key2;
                   callback3();
              }
            }
            else
              callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            callback2(); 
           }
        });
      },
      function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          
          itemMasters.push(subitemMasters);
          callback1();
        }
      });
    },
  function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          if(pageId == 1){
             //mastFields.splice(visualIndex,1); //splice at loop above not working in next iteration
             data.selectionCriteriaColumns = selectionFields;
             }
          debuglog("Send product Data along with Visuals now");
          debuglog(itemMasters);
          debuglog("After formatting master Data");
          debuglog(itemMasters);
          data.productData = itemMasters;
          //processStatus(req,res,9,data);
          getSellers(req,res,9,data);
         }
      });
 

} 


function getSellers(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./utils');

 var tabl_name = data.orgId+'_sellers';
 
  datautils.sellers(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Sllers table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.sellers = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 
//This function may not be required as now everything is stored as arrays

function getMasterVisuals(req,res,stat,data,itemMasters) {
   var utils = require('./utils');
   var async = require('async');
   var itemCodeVal = '';
   
     async.forEachOf(itemMasters,function(key1,value1,callback1){
           var itemMaster = itemMasters[value1];
           async.forEachOf(itemMaster,function(key2,value2,callback2){
           if(value2 == 'itemCode')
              itemCodeVal = key2;
           if(value2 == 'itemVisual'){
     
          var vURL = key2;
          var regexp = /((http|https):\/\/)?[A-Za-z0-9\.-]{3,}\.[A-Za-z]{2}/;	
          if(regexp.test(vURL))
            itemVisual = vURL; //video url has http or https in beginning
          else
            itemVisual = global.nodeURL+vURL+'/'+itemCodeVal+'_IMAGE_0';
            //currently sending only first image change here if need all images
            
            itemMaster.itemVisual = itemVisual;
            }
           },
            function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            itemMasters.push(itemMaster);
           }
        });
        callback1();
       },
       function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
              data.itemMasters = itemMasters;
              processStatus(req,res,9,data);
          }
        });
}
      

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
exports.getProductData = getMasterFields;



