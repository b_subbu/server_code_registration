function processRequest(req,res,data){
   if(typeof req.body.itemCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItemCode(req,res,9,data);
}

function getItemCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var itemCode = req.body.itemCode;
  
      utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
            data.itemCode = itemCode;
            getSelectionFields(req,res,9,data);
            }
          }
        });
} 

function getSelectionFields(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
  
    utils.selectionFieldsOrgId(data.orgId,data.businessType,data.itemCode,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  selectionFields = result.selectFields;
                  selectionTable = result.table_name_detail;
                  getSelectionData(req,res,stat,data,selectionFields,selectionTable);      
                  }
            else {
                  data.selectionCriteria = [];
                  processStatus(req,res,9,data); 
                 }
        }
     });
} 

//Note: the fields in selection table can change from org to org and item code to itemcode
// so should use the fields definitions
function getSelectionData(req,res,stat,data,selectionFields,table_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var selectFields = [];
  
    utils.selectionData(data.orgId,data.businessType,data.itemCode,table_name,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
             for(idx = 0; idx<selectionFields.length;idx++){
                if(selectionFields[idx].name == 'itemDetailCode' || selectionFields[idx].name == 'isDeleted')
                   continue;
                else {
                   var tmpArray = {name:selectionFields[idx].name,label:selectionFields[idx].label,typeId:selectionFields[idx].type,additionalTypeId:selectionFields[idx].additional};
                       tmpArray.visibility = selectionFields[idx].visibility;
                       tmpArray.width = selectionFields[idx].width;
                       tmpArray.purpose = selectionFields[idx].purpose;
                       tmpArray.editable = selectionFields[idx].editable;
                       tmpArray.required = selectionFields[idx].required;
                       if(selectionFields[idx].value !== null)
                          tmpArray.value = selectionFields[idx].value;
                   
                   selectFields.push(tmpArray);
                  }
                }
                  data.selectionCriteriaColumns = selectFields;
             
                  selectionData = result.selectionData;
                  formatSelectionData(req,res,9,data,selectionFields,selectionData); 
              }
        });
} 

function formatSelectionData(req,res,stat,data,selectionFields,selectionData) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var itemMasters = [];
   var key_sequence = 0;
      async.forEachOf(selectionData,function(key1,value1,callback1){
      subitemMasters = [];
         async.forEachOf(key1,function(key2,value2,callback2){
            async.forEachOf(selectionFields,function(key3,value3,callback3){
            var key_name = key3.name;
            var key_label = key3.name;
            var key_sequence = key3.sequence;
            if(key_name == value2)  {
                  //subitemMasters[key_label]= key2;
                  //need to get sequence of detail fields here to sort later
                  var tmpArray = {};
                  tmpArray[key_label] = key2;
                  tmpArray['sequence'] = key_sequence;
                  subitemMasters.push(tmpArray); 
                  callback3();
                 }
            else
              callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
             callback2(); 
           }
        });
      },
     function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
           itemMasters.push(subitemMasters);
            callback1(); 
           }
        });
      },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          sortSelectionData(req,res,9,data,itemMasters);
        }
      });
 

} 

//We have to sort the selection criteria field as per order of detail fields
//because this order is important while rendering 
//also we can't get a sorted array above since can use fields only at final level
function sortSelectionData(req,res,stat,data,selectionData) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var itemMasters = [];
   var key_sequence = 0;
         async.forEachOf(selectionData,function(key1,value1,callback1){
          var subitemMasters = {};
           var selectData = selectionData[value1];
           selectData.sort(function(a,b) {return a.sequence-b.sequence});
            async.forEachOf(selectData,function(key2,value2,callback2){
              async.forEachOf(key2,function(key3,value3,callback3){
                   if(value3 != 'sequence')  {
                    subitemMasters[value3] = key3;
                     callback3();
                    }
               else
                 callback3();
        },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
            callback2(); 
           }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
           itemMasters.push(subitemMasters);
            callback1(); 
           }
        });
      },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
          data.selectionCriteria = itemMasters;
          processStatus(req,res,9,data);
        }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




