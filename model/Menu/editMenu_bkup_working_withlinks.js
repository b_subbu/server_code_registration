function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMasterStatus(req,res,data);
}


function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    data.userId = uid;
    
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              getMenuMasterFields(req,res,9,data);
            }
          }); 
}

function getMenuMasterFields(req,res,stat,data) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var masterFields = [];
  
    utils.menuFieldsBizType(data.businessType,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  menuFields = result.menuFields;
                  getMenuData(req,res,stat,data,menuFields);
                  }
            else {
                  processStatus(req,res,3,data); 
                 }
        }
     });
} 

function getMenuData(req,res,stat,data,menuFields){

   var menuData = req.body.menuData;
   if(typeof menuData === 'null' || typeof menuData === 'undefined')
     processStatus(req,res,6,data);
  
  else{

   var async = require('async');
   var utils = require('./menuutils');
   var mastData = {};
   

  
 utils.parseJson(menuData,function(err,result){
    if(err) processStatus(req,res,6,data);
    else {
          //This is the information of root-node
          mastData.id = result.id;
          mastData.name = result.name;
          mastData.description = result.description;
          checkMenuCodeExists(req,res,9,data,menuFields,mastData,menuData); 
        }
     }); 
 }
}

function checkMenuCodeExists(req,res,stat,data,mastFields,mastData,menuData) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   //var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file
   
   var menuCode = mastData.id;
   
   var tabId = req.body.tabId;
   var submitop = req.body.submitOperation;
   
  
      if(tabId == 502 )
          var tabl_name = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file
      else
         var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file
  
    utils.checkMenuCode(tabl_name,orgId,bizType,menuCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
              if(isError == 0 && (result.itemStat != 80 && result.itemStat != 86 && result.itemStat != 87 && result.itemStat != 84 && result.itemStat != 88)){
               processStatus(req,res,5,data);
              isError = 1;
              return;
             }
            else {
             if(submitop == 1){
                 var submitmenu = require('./submitOnEditMenu');
                 submitmenu.getData(req,res,9,data,menuFields,mastData,menuData,tabId,result.itemStat);
                }
          else {
                getItemCodes(req,res,9,data,mastData,menuData,tabl_name)
               }
          }
        }
      });
} 

//Extracting itemcodes from the tree
function getItemCodes(req,res,stat,data,mastData,menuData,tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var itemCodes = [];
   var groupCodes = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    var menuLinks = menuData.menuLinkage;
     async.forEachOf(menuLinks,function(key1,value1,callback1){
      delete key1.__uiNodeId;
      if(key1.isLeaf == true){
          if(key1.nodeIs == 'item'){
          debuglog("This is a item");
          debuglog(key1);
           itemCodes.push(key1.linkId);
           
          }
          if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          groupCodes.push(key1.linkId);
          }
       debuglog("Adding isDeleted as false for all nodes to handle delete of items/groups/nodes in Edit");
       key1.isDeleted = false;
       debuglog(key1);
       callback1();
       }
      else{
       debuglog("Not a leaf node may be link nodes");
       debuglog("Adding isDeleted as false for all nodes to handle delete of items/groups/nodes in Edit");
       key1.isDeleted = false;
       debuglog(key1);
       callback1();
       }
    },
   function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
     else {
          debuglog("start checking item codes and group codes");
          debuglog(itemCodes);
          debuglog(groupCodes);
          checkItemCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name);
         }
      });
} 

function checkItemCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 7; //item not found
              isError = 1;
              }
            if( isError == 0 && (result.itemStatus != 80 && result.itemStatus != 83 && result.itemStatus != 84)) {
              errorCode = 7; //item not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                checkGroupCodes(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name);
              }
            }
          });
} 

function checkGroupCodes(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   debuglog("Note business type checks are done away with");
   debuglog("TODO do away with orgId check since each data is in its own organziation table");
   
   var grp_tabl_name = orgId+'_group_header';
   async.forEachOf(groupCodes,function(key,value,callback){
      groupCode = key;
     utils.checkGroupCode(grp_tabl_name,orgId,groupCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              errorCode = 8; //Group not found
              isError = 1;
              }
            if( isError == 0 && (result.groupStatus != 80 && result.groupStatus != 83 && result.groupStatus != 84)) {
              errorCode = 8; //Group not in correct status 'Active', 'Linked' or 'InUse'; status id is hard-coded here
              isError = 1;
              }  
            if(isError == 0) {
                  ; //nothing 
            }
            callback();
          }
        });
      },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
               if(errorCode != 0)
                 processStatus(req,res,errorCode,data);
               else
                 processStatus(req,res,6,data);
                }
           else {
                 updateMenuData(req,res,9,data,mastData,menuData,itemCodes,groupCodes,tabl_name);
                }
            }
          });
} 

function updateMenuData(req,res,stat,data,mastData,menuData,itemCodes,groupCodes,tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   var tabId = req.body.tabId;
   
  
      if(tabId == 502 )
          var link_tabl_name = 'Revision_'+orgId+'_menus_items_link'; //TODO: move to a common definition file
      else
         var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
  
   
    utils.updateMenu(tabl_name,orgId,bizType,mastData.id,mastData,menuData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
               updateMenuLinks(req,res,9,data,mastData.id,itemCodes,tabl_name,link_tabl_name);
            }
        });
} 

//Note: MenuLinks are the Nodes of a menu
//and MenuItemLinks are the menu->item link
function updateMenuLinks(req,res,stat,data,menuCode,itemCodes,mastTable,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    var linkData = req.body.menuData;
   
   
    utils.createMenuNodes(mastTable,orgId,bizType,menuCode,linkData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
              getMenuItemLinks(req,res,9,data,menuCode,itemCodes,mastTable,link_tabl_name);
            }
        });
} 

//We are getting the existing item links from link table
//This is to handle any items deleted during Edit mainly of Recalled Menu
function getMenuItemLinks(req,res,stat,data,menuCode,itemCodes,mastTable,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var bizData = [];
   var isError = 0;
   var orgId = data.orgId;
   var bizType = data.businessType;
   
   
   
    utils.menuLinks(link_tabl_name,orgId,bizType,menuCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
               if(result.errorCode == 0)
                 var deletedItemCodes = result.itemCodes;
               else
                 var deletedItemCodes = [];
              
              deleteMenuItemLinks(req,res,9,data,menuCode,itemCodes,deletedItemCodes,mastTable,link_tabl_name);
              
            }
        });
} 

//we will delete all links and recreate them
//this will be faster instead of checking one by one row and updating
function deleteMenuItemLinks(req,res,stat,data,menuCode,itemCodes,deletedItemCodes,tabl_name,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
     utils.delinkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,function(err,result) {
          if(err) {
               isError = 1;
              
             }
         else {
                 createMenuItemLinks(req,res,9,data,menuCode,itemCodes,deletedItemCodes,tabl_name,link_tabl_name);
              }
    });
} 


function createMenuItemLinks(req,res,stat,data,menuCode,itemCodes,deletedItemCodes,tabl_name,link_tabl_name) {
   var async = require('async');
   var utils = require('./menuutils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   
   var orgId = data.orgId;
   var bizType = data.businessType;
  
   async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
     utils.linkMenuItemCode(link_tabl_name,orgId,bizType,menuCode,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               callback();
             }
         else {
            callback();
            }
        });
      },
      function(err){
      if(err || isError == 1)
        processStatus(req,res,6,data);
      else
          {
            var totalItemCodes = itemCodes.concat(deletedItemCodes);
            //we can combine both now since all itemProcessing is alreadydone
            changeItemStatus(req,res,9,data,menuCode,totalItemCodes,tabl_name,link_tabl_name); 
            //80 is status for Entered
          }
    });
} 

//if no row in link table for the item, change item status to Entered
//otherwise just leave it, because item may be linked to other menus
//This is to handle any items deleted in Edit of a Recalled Menu ugh..
function changeItemStatus(req,res,stat,data,menuCode,itemCodes,mastTable,link_tabl_name){
    
    var async = require('async');
    var statutils = require('./statusutils');
    var menuutils = require('./menuutils');
    var isError = 0;
    
    var reason = 'Menu Deletion changed Status'; 
    
    var tabl_name = data.orgId+"_menuitem_master";
     
     async.forEachOf(itemCodes,function(key,value,callback){
      itemCode = key;
    async.waterfall([
         function(firstcallback){
            menuutils.itemLinks(link_tabl_name,data.orgId,data.businessType,itemCode,function(err,result){ 
            if(err)
             firstcallback(err,null);
            else
             firstcallback(null,result);
             });
            },
         function(items,secondcallback){
         //because the previous fn would have deleted and recreated all rows from link table
         //if anything is left then it is due to link with other menus
         //if nothing is left then this item was probably deleted from the menu during Edit
          if(items.totalRows == 0){
          statutils.chngItmStatusOnMenuChange (tabl_name,data.orgId,data.businessType,itemCode,83,80,reason,function(err,result){
            if(err)
             secondcallback(err);
            else
             secondcallback(null);
            });
          }
          else {
           secondcallback(null); //nothing to do this item is linked to some other Menu 
          }
          }],
        function(err,results) {
         if(err)
          isError = 1;
         
         callback();
          });
        },
         function(err,results) {
          if(err|| isError == 1) {
              processStatus(req,res,6,data);
             }
      else
          {
             changeMenuStatus(req,res,9,data,menuCode,80,mastTable);
             //getMenuDetails(req,res,9,data,tabl_name);
          }
    });
}

function changeMenuStatus(req,res,stat,data,menuCode,menuStat,mastTable){
    var async = require('async');
 
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = '';
    
     var tabId = req.body.tabId;
 
  
      if(tabId == 502 )
          menuStat = 88; //SAVE from Action Q does not really change status
      else
         menuStat = 80; //because all submit operations are redirected at top itself
  
    
      statutils.changeMenuStatus(mastTable,data.orgId,data.businessType,menuCode,menuStat,reason,function(err,result) {
          if(err) {
                     processStatus(req,res,6,data);
                }
           else {
                 data.menuCode = menuCode;
                 getMenuDetails(req,res,9,data,mastTable);
           
              }
          });
     
}


function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    utils.menuDetail(mastTable,data.orgId,data.businessType,data.menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else  {
            data.menuData = result.masterData;
            processStatus(req,res,9,data);
           }
    });
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Menu/editMenu');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




