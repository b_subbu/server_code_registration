function processRequest(req,res,data){
   if(typeof req.body.menuItemDetails === 'undefined' )
       processStatus(req,res,6,data); //system error
   else 
       getDetailFields(req,res,9,data);
}


function getDetailFields(req,res,stat,data) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var itemCode = req.body.menuItemDetails.itemCode;
  
    var orgId = data.orgId;
    var bizType = data.businessType;
     async.parallel({
         detailStat: function(callback){utils.checkItemCode(orgId,bizType,itemCode,callback); },
         },
         function(err,results) {
          if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
             if(isError == 0 && (results.detailStat.errorCode != 0 && results.detailStat.errorCode != 3)) {
              processStatus (req,res,results.detailStat.errorCode,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
                mastFields = results.detailStat.mastFields;
                mastTable = results.detailStat.tblName;
                data.itemCode = itemCode;
                checkMandatoryFields(req,res,9,data,mastFields,mastTable);
            }
          }); 
} 


function checkMandatoryFields(req,res,stat,data,mastFields,mastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var isExists = true; //there need not be any mandatory fields for Details
   var isError = 0;
    async.forEachOf(mastFields,function(key1,value1,callback1){
       if(key1.isMandatory == true && isError == 0){
         isExists = false;
          async.forEachOf(req.body.menuItemDetails,function(key2,value2,callback2){
              if(key1.name == value2)  
                   isExists = true;
                   
              callback2();
           },
           function(err){
             if(err){
               processStatus (req,res,6,data);
                return;         
            }
            else {
               if(isExists == false || isError == 1){
               //processStatus (req,res,4,data); does not seem to exit loop from here
               isError = 1;
               return;
              }
           }
         });
        callback1();
    }
    else
      callback1();
    },
    function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
      else {
           if(isExists == false || isError == 1){
              processStatus (req,res,4,data);
              return;
           }
      else
         //createMenuItemDetailsData(req,res,9,data,mastFields,mastTable);
         getSelectionFields(req,res,9,data,mastFields,mastTable);
          }
      });
} 

function getSelectionFields(req,res,stat,data,mastFields,mastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var itemCode = req.body.menuItemDetails.itemCode;
  
      utils.selectionFieldsOrgId(data.orgId,data.businessType,itemCode,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  selectionFields = result.selectFields;
                  selectionTable = result.table_name_detail;
                  //checkMandatoryFieldsSelection(req,res,stat,data,selectionFields,selectionTable,mastFields,mastTable);
                  createMenuItemDetailsData(req,res,9,data,selectionFields,selectionTable,mastFields,mastTable);
                  
                  }
            else {
                  createSelectionFieldsOrgId(req,res,stat,data,mastFields,mastTable);
                  }
        }
     });
} 

function createSelectionFieldsOrgId(req,res,stat,data,mastFields,mastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var itemCode = data.itemCode;

  if(typeof req.body.selectionCriteria !== 'undefined'){
      var selectionFields = req.body.selectionCriteriaColumns; 
  
      utils.createSelectionFieldsOrgId(data.orgId,data.businessType,itemCode,selectionFields,function(err,result) {
          if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  //selectionFields = result.selectFields; //no need to get from bizType since getting all from UI now
                  selectionTable = result.table_name;
                   //checkMandatoryFieldsSelection(req,res,stat,data,selectionFields,selectionTable,mastFields,mastTable);
                     createMenuItemDetailsData(req,res,9,data,selectionFields,selectionTable,mastFields,mastTable);
             
                  }
            else {
                  processStatus(req,res,3,data);
                  }
        }
     });
     }
   else
     processStatus(req,res,9,data);
     //user had sent only details part not selection part
} 


function checkMandatoryFieldsSelection(req,res,stat,data,selectFields,selectTable,mastFields,mastTable) {
   var async = require('async');
   var utils = require('./utils');
   var type = require('type-detect');
   var isExists = true; 
   var isError = 0;
    async.forEachOf(selectFields,function(key1,value1,callback1){
         if(key1.isMandatory == true && isError == 0 && key1.name != 'itemDetailCode'){
         isExists = false;
          async.forEachOf(req.body.selectionCriteria,function(key2,value2,callback2){
           async.forEachOf(key2,function(key3,value3,callback3){
              if(key1.name == value3)  
                   isExists = true;
                   
              callback3();
           },
           function(err){
             if(err){
               processStatus (req,res,6,data);
                return;         
            }
            else {
               if(isExists == false || isError == 1){
               //processStatus (req,res,4,data); does not seem to exit loop from here
               isError = 1;
               return;
              }
           }
         });
        callback2();
        },
       function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
      else {
           callback1(); 
          }
      });  
    }
    else
      callback1();
    },
    function(err){
      if(err){
             processStatus (req,res,6,data);
              return;         
            }
      else {
           if(isExists == false || isError == 1){
              processStatus (req,res,4,data);
              return;
           }
      else
         createMenuItemDetailsData(req,res,9,data,selectFields,selectTable,mastFields,mastTable);
          //createMenuItemSelectionData(req,res,9,data,selectFields,selectTable,mastFields,mastTable);
          }
      });
} 

function createMenuItemDetailsData(req,res,stat,data,selectFields,selectTable,mastFields,mastTable){
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
  
    var detailsData = req.body.menuItemDetails;
     if(typeof req.body.selectionCriteria !== 'undefined'){
      var selectionFields = req.body.selectionCriteria; //all rows must be same, so just take first row
      var minPrice = Math.min.apply(Math,selectionFields.map(function(o){return o.price;}));
      var maxPrice = Math.max.apply(Math,selectionFields.map(function(o){return o.price;}));
      }
     else
     {
      var minPrice=0;//user has not yet created the selection criteria
      var maxPrice=0;
     }
     
    utils.createDetailsData(data.orgId,data.businessType,mastTable,mastFields,detailsData,minPrice,maxPrice,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
           createMenuItemSelectionData(req,res,9,data,selectFields,selectTable,mastFields,mastTable);
            }
          }
        });

}

function createMenuItemSelectionData(req,res,stat,data,selectFields,selectTable,mastFields,mastTable){
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
  
    var selectionData = req.body.selectionCriteria;
    var itemCode = data.itemCode;
     utils.createSelectionData(data.orgId,data.businessType,itemCode,selectTable,selectFields,selectionData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else {
            changeItemStatus(req,res,9,data,mastFields,mastTable);
            //processStatus(req,res,9,data);
            }
          }
        });

}

function changeItemStatus(req,res,stat,data,mastFields,mastTable){
    
    var itemCode = data.itemCode;
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = '';
  
  statutils.changeStatus(mastTable,data.orgId,data.businessType,data.userId,data.itemCode,80,reason,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
           else {
             getDetailsData(req,res,stat,data,mastFields,mastTable)
             
            //processStatus(req,res,9,data);
            }
        });
     
}

function getDetailsData(req,res,stat,data,mastFields,mastTable){

  var itemCode = data.itemCode;
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = '';
  
  statutils.detailsData(data,mastFields,mastTable,function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
           else {
             data.menuItemDetails = results.menuItemDetails;
             getItemStatName(req,res,9,data);
    
            }
        });
 

}

function getItemStatName(req,res,stat,data){

    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    var itemMasters = [];
    //create and edit are single item operations
    //while others are bulk operations so this fn changes 
    
     async.forEachOf(data.menuItemDetails,function(key,value,callback){
    if(value == 'status') {
    statId = key;
  statutils.statName(statId,'menuitem_status_master',function(err,results) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               callback();
             }
           else {
             data.menuItemDetails.status = results;
             callback();
             }
        });
        }
        else
         callback();
         },
      function(err){
      if(err)
        processStatus(req,res,6,data);
      else
          {
           if(isError == 1){
                  processStatus(req,res,6,data);
                }
           else {
                  processStatus(req,res,9,data);
                  
              }
            }
          });

}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




