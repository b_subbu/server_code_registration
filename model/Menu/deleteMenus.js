function processRequest(req,res,data){
   if(typeof req.body.menuCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMenuStatus(req,res,9,data);
}

function checkMenuStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    data.menuCodes = menuArr;
    var menuStatus = 0;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          menuStat: function (callback){utils.checkMenuCode(tabl_name,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
           menuStatus = results.menuStat.itemStat; 
           if(isError == 0 && (results.menuStat.errorCode != 0 && results.menuStat.errorCode != 5)) {
             isError = 1;
             errorCode = results.menuStat.errorCode;
             callback1();
           }
          if( isError == 0 && (results.menuStat.itemStat != 80 && results.menuStat.itemStat != 82 && results.menuStat.itemStat != 86 && results.menuStat.itemStat != 87)) {
             errorCode = 5;
             isError = 1;
             callback1();
           }
           if(isError == 0)  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
              processStatus(req,res,6,data);
             }
      else
          {
           getItemsData(req,res,9,data,menuStatus); 
         }
     });




}


function getItemsData(req,res,stat,data,menuStat){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
    
     async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
     async.parallel({
         linkData: function(callback){utils.menuLinksWithNodeIs(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           if (isError == 0 && results.linkData.errorCode == 0) {
           debuglog(results.linkData);
           debuglog("This can contain either item or group we will do corresponding processing ");
           debuglog("in next function");
            itemsArr.push(results.linkData.itemCodes);
             callback1();
            }
          else
           callback1(); //Sometimes delete seems to happen before insert-->TODO:check
         }); 
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
           deleteMenuItemLinks(req,res,9,data,itemsArr,link_tabl_name,menuStat);
          }
     });
}

//Delete all item links for the passed menu array first
//Then change status of items

function deleteMenuItemLinks(req,res,stat,data,itemsArr,link_tabl_name,menuStat){
    
    var async = require('async');
    var statutils = require('./statusutils');
    var menuutils = require('./menuutils');
    var isError = 0;
    var menuArr = data.menuCodes;
    
      async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
       async.parallel({
         linkData: function(callback){menuutils.delinkMenuItemCode(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           else 
             callback1();
          }); 
        },
         function(err){
         if(err || isError == 1) {
              processStatus(req,res,6,data);
             }
      else
          {
           if(menuStat == 82 || menuStat == 86){
           debuglog("Deleting Authroised or Recalled Menu");
           debuglog("so we need to reduce link count and move to active the items and groups");
           changeItemStatus(req,res,9,data,itemsArr,link_tabl_name);
          }
          else{
           debuglog("Deleting Entered or Rejected Menu");
           debuglog("NO need to reduce link count and move to active the items and groups");
           changeMenuStatus(req,res,9,data);
          
          
          }
        }
    });
}

function changeItemStatus(req,res,stat,data,itemsArr){
    
    var async = require('async');
    var itemUtils = require('./utils');
    var groupUtils = require('../Groups/dataUtils');
    var isError = 0;
    
    var reason = 'Menu Approval changed Status'; 
    
    var item_tabl_name = data.orgId+"_menuitem_master";
    var group_tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr[0],function(key1,value1,callback1){
     debuglog("starting processing of items arr to change status");
     debuglog(key1);
     
       if(key1.nodeIs == 'item'){
          debuglog("This is an item");
          debuglog(key1);
          debuglog("we will move the item to Active status");
          debuglog("and decrement linked counter by 1");
          itemUtils.moveItemStatus(item_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,0,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
         }
          
    if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          debuglog("we will move the group to Active status");
          debuglog("and decrement linked counter by 1");
          debuglog("but dont increment linked counter");
          groupUtils.moveGroupStatus(group_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,0,function(err,results){
          if(err){
           debuglog("move reverse group error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move reverse group error");
           callback1();
          }
          });
        }
       },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            changeMenuStatus(req,res,9,data);
          }
    });
}

function changeMenuStatus(req,res,stat,data){
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_menus_master'; //TODO: move to a common definition file

    var menuArr = data.menuCodes;
    var async = require('async');
    var statutils = require('./statusutils');
    var menuutils = require('./menuutils');
    var isError = 0;
    
    var reason = ''; //no reason is currently set for reject operation
    
     
     async.forEachOf(menuArr,function(value,key,callback1){
       var menuCode = value;
       async.parallel({
         //chngStatus: function(callback){statutils.changeMenuStatus(tabl_name,data.orgId,data.businessType,menuCode,87,reason,callback); },
         delMenu: function(callback){menuutils.deleteMenu(tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             callback1();
            }
         });
         },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
           processStatus(req,res,9,data);
          }
     });
}

//Because data will be deleted no point in getting details
function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    async.forEachOf(data.menuCodes,function(key,value,callback){
    var menuCode = key;
    utils.menuDetail(mastTable,data.orgId,data.businessType,menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              isError =1 ;
              callback();
              }
      else  {
            resultArr.push(result.masterData);
            callback();
           }
    });
    },
    function(err){
         if(err || isError == 1) 
               processStatus(req,res,6,data);
       else
          {
          data.menuData = resultArr;
          processStatus(req,res,9,data); 
         }
     });
    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;





