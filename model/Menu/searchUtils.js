function getItemSearch(req,res,oid,pid,tid,pgid,size,data) {

 var searchInp = data.searchParams;
 
 //searchParams will be of format
 //searchParams = {conTitle:{isRange:false, value:”TEST”}, status:{isRange:false,value:81}, createDate: {isRange:true, value:'01/01/2015##01/31/2015'}}
 //but we are building case by case so isRange is not really used for now
 //may be when we build a generic search tool it will be useful
 
 var isAlertTypeName = false;
 var isStatusName = false;
 alertTypeName = '';
 statusName = '';
 var isPrice = 0;
 var priceArr = [];
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
       case 'itemPrice':
       //TODO 
          isPrice = 1;
          priceArr = key;
          break;
       case 'itemCode':
        var tmpArray = {itemCode: {$regex:key,$options:'i'}}; //same for itemCode and Description
           searchArr.push(tmpArray);
         break; 
       case 'itemName':
        var tmpArray = {itemName: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break; 
    
       case 'itemDescription':
        var tmpArray = {itemDescription: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break; 
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           if(isPrice == 1)
           getItemSearchCountRange(req,res,oid,pid,tid,pgid,size,searchArr,data,priceArr);
           else
           getItemSearchCount(req,res,oid,pid,tid,pgid,size,searchArr,data);
          }
     });
}


function getItemSearchCount(req,res,oid,pid,tid,pgid,size,sarr,data){
   var totCount = 0;
   var tablName = '';
   
   if(tid == 501)
	   tablName = oid+'_menuitem_master';
   else
	   tablName = 'Revision_'+oid+'_menuitem_master';
    var promise = db.get(tablName).count({$and:sarr});
    promise.on('complete',function(err,doc){
      totCount = doc;
      if(totCount == 0) {
       data.resultCount = 0;
       processStatus(req,res,9,data);
       }
      else {
         getItemSearchData(req,res,oid,pid,tid,pgid,size,sarr,totCount);
         } 
     });
   promise.on('error',function(err){
       processStatus(req,res,6,data);
     
	 });
} 

function getItemSearchData(req,res,oid,pid,tid,pgid,size,sarr,totalCount){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      
	  if(tid == 501)
	   tablName = oid+'_menuitem_master';
   else
	   tablName = 'Revision_'+oid+'_menuitem_master';
   
   var promise = db.get(tablName).find({$and:sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
         var tmpArray = {itemCode: doc.itemCode,itemName:doc.itemName,itemDescription:doc.itemDescription,itemPrice:doc.itemPrice,status:doc.status_id}
        
        resultArray.push(tmpArray);
        //note it is just 5 fields here statically as per Vignesh
        //if need to add more fields then use utils get master data functions and then format		
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.recordCount = totalCount;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       
       processStatus(req,res,9,data);
     }
      else {
       data.recordCount = totalCount;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       getItemStatName(req,res,9,data,resultArray);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);
    
	});
} 

//The below two are with Range for Price
//Logic is we take all values without limit first 
//just include the conditions other than range (itemPrice)
//then filter it for Price Range
//then do a start and limit 
//i.e., get all data from db for the given orgid and then do all filtering and paging in
//frontend

function getItemSearchCountRange(req,res,oid,pid,tid,pgid,size,sarr,data,priceArr){
   var totCount = 0;
   var tablName = '';
   
   if(tid == 501)
	   tablName = oid+'_menuitem_master';
   else
	   tablName = 'Revision_'+oid+'_menuitem_master';
    var promise = db.get(tablName).count({$and:sarr});
    promise.on('complete',function(err,doc){
      totCount = doc;
      if(totCount == 0) {
       data.resultCount = 0;
       processStatus(req,res,9,data);
       }
      else {
         getItemSearchDataRange(req,res,oid,pid,tid,pgid,size,sarr,totCount,priceArr);
         } 
     });
   promise.on('error',function(err){
       processStatus(req,res,6,data);
     
	 });
} 

function getItemSearchDataRange(req,res,oid,pid,tid,pgid,size,sarr,totalCount,priceArr){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      
	  if(tid == 501)
	   tablName = oid+'_menuitem_master';
   else
	   tablName = 'Revision_'+oid+'_menuitem_master';
   
   var promise = db.get(tablName).find({$and:sarr},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
         var tmpArray = {itemCode: doc.itemCode,itemName:doc.itemName,itemDescription:doc.itemDescription,itemPrice:doc.itemPrice,status:doc.status_id}
        
        resultArray.push(tmpArray);
        //note it is just 5 fields here statically as per Vignesh
        //if need to add more fields then use utils get master data functions and then format		
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.recordCount = totalCount;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       
       processStatus(req,res,9,data);
     }
      else {
       data.recordCount = totalCount;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       filterItems(req,res,9,data,resultArray,pgid,size,priceArr);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);
    
	});
} 

//Quite complex Logic here
//if itemPrice does not contain "-" then set minPrice=maxPrice=itemPrice 
//i.e., Range is single value
//Then compare with minSearch and maxSearch from input
//if maxSearch < minPrice OR minSearch > maxPrice then it is not within range
//all other conditions falls within range 
function filterItems(req,res,stat,data,resultArray,pgid,size,priceArr){
    var async = require('async');
    var tmpArray = [];
    var finalArray = [];
    var isError = 0;
  
        async.forEachOf(resultArray,function(key1,value1,callback1){
      
        var masterData = resultArray[value1]; //because mastdata is double array
          async.forEachOf(masterData,function(key2,value2,callback2){
              if(value2 == 'itemPrice'){
              var dbPrice=key2+''; //single value prices may be saved as numeric
              if(dbPrice.indexOf("-") != -1){
              var dbPriceArr = dbPrice.split("-");
               minPrice = dbPriceArr[0];
                maxPrice = dbPriceArr[1];
               }
              else {
                minPrice = dbPrice;
                maxPrice = dbPrice;
              }    
            
            
               minSearch = priceArr[0];
               maxSearch = priceArr[1];
             
              if(maxSearch < minPrice || minSearch > maxPrice )
                callback2();
            
            else {
             tmpArray.push(masterData);
             callback2();
            
            }
          }
          else
           callback2();
        },
       function(err){
         if(err){
             isError =1;         
			 callback1();
			  }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    if(isError == 1)
			 processStatus (req,res,6,data);
            else{				
         var page = parseInt(pgid);
         var skip = page > 0 ? ((page - 1) * size) : 0;
             finalArray = tmpArray.splice(skip,size);
		      getItemStatName(req,res,9,data,finalArray);
			   }
        }
      });
 



}


function getItemStatName(req,res,stat,data,mastData) {
   var async = require('async');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
     
      async.forEachOf(mastData,function(key1,value1,callback1){
      
        var masterData = mastData[value1]; //because mastdata is double array
        async.forEachOf(masterData,function(key2,value2,callback2){
             if(value2 == 'status'){
              var statID=key2;
             
           statutils.statName(statID,'menuitem_status_master',function(err,result) {
           if(err){ isError =1; callback2();}
         else {
                 //key2 = result;
                 mastData[value1]['status'] = result;
                 callback2(); 
              }
           }); 
          }
          else
              callback2();
        },
       function(err){
         if(err){
             isError =1;         
			 callback1();
			  }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    if(isError == 1)
			 processStatus (req,res,6,data);
            else
			{				
           data.searchData = mastData;
		   processStatus(req,res,9,data);
			}
        }
      });
 

}

function getMenuSearch(req,res,oid,pid,tid,pgid,size,data) {

 var searchInp = data.searchParams;
 
 //searchParams will be of format
 //searchParams = {conTitle:{isRange:false, value:”TEST”}, status:{isRange:false,value:81}, createDate: {isRange:true, value:'01/01/2015##01/31/2015'}}
 //but we are building case by case so isRange is not really used for now
 //may be when we build a generic search tool it will be useful
 
 var isMenuName = false;
 var isStatusName = false;
 menuName = '';
 statusName = '';
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
       case 'menuCode':
        var tmpArray = {menuCode: key}; //same for itemCode and Description
           searchArr.push(tmpArray);
         break; 
       case 'menuName':
        var tmpArray = {menuName: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break; 
    
       case 'menuDescription':
        var tmpArray = {menuDescription: {$regex:key,$options:'i'}}; 
        searchArr.push(tmpArray);
         break;
         
      case 'createDate':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var minDaysArr = minDateArr[2].split("T"); //sometimes UI sends with timestamp
        var maxDaysArr = maxDateArr[2].split("T");
        var cmpDate = new Date(minDaysArr[0],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDaysArr[0],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {create_time: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {create_time: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
       break; 
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           getMenuSearchCount(req,res,oid,pid,tid,pgid,size,searchArr,data);
          }
     });
}

function getMenuSearchCount(req,res,oid,pid,tid,pgid,size,sarr,data){
   var totCount = 0;
   var tablName = '';
   
    tablName = oid+'_menus_master';
    var promise = db.get(tablName).count({$and:sarr});
    promise.on('complete',function(err,doc){
      totCount = doc;
      if(totCount == 0) {
       data.resultCount = 0;
       processStatus(req,res,9,data);
       }
      else {
         getMenuSearchData(req,res,oid,pid,tid,pgid,size,sarr,totCount,tablName);
         } 
     });
   promise.on('error',function(err){
       processStatus(req,res,6,data);
     
	 });
} 

function getMenuSearchData(req,res,oid,pid,tid,pgid,size,sarr,totalCount,tablName){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment = require('moment');
 
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
   
   var promise = db.get(tablName).find({$and:sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
         var tmpArray = {menuCode: doc.menuCode,menuName:doc.menuName,menuDescription:doc.menuDescription,status:doc.status_id,createDate:moment(doc.create_time).format('L')}
        
        resultArray.push(tmpArray);
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.recordCount = totalCount;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       
       processStatus(req,res,9,data);
     }
      else {
       data.recordCount = totalCount;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       getMenuStatName(req,res,9,data,resultArray);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);
    
	});
} 

function getMenuStatName(req,res,stat,data,mastData) {
   var async = require('async');
   var statutils = require('./statusutils');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
     
      async.forEachOf(mastData,function(key1,value1,callback1){
      
        var masterData = mastData[value1]; //because mastdata is double array
        async.forEachOf(masterData,function(key2,value2,callback2){
             if(value2 == 'status'){
              var statID=key2;
             
           statutils.statName(statID,'menus_status_master',function(err,result) {
           if(err){ isError =1; callback2();}
         else {
                 //key2 = result;
                 mastData[value1]['status'] = result;
                 callback2(); 
              }
           }); 
          }
          else
              callback2();
        },
       function(err){
         if(err){
             isError =1;         
			 callback1();
			  }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    if(isError == 1)
			 processStatus (req,res,6,data);
            else
			{				
           data.searchData = mastData;
		   processStatus(req,res,9,data);
			}
        }
      });
 

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Search/searchData');
  
    controller.process_status(req,res,stat,data);
}

exports.search =  getItemSearch;
exports.statNames = getItemStatName;
exports.searchMenu = getMenuSearch;


 
