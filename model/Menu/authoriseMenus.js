function processRequest(req,res,data){
    if(typeof req.body.menuCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMenuStatus(req,res,9,data);
}

function checkMenuStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
    data.menuCodes = menuArr;
    var itemStat = 0;
    
    var orgId = data.orgId;
    
    var tabl_name = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file

    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          menuStat: function (callback){utils.checkMenuCode(tabl_name,data.orgId,data.businessType,menuCode,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
           if(isError == 0 && (results.menuStat.errorCode != 0 && results.menuStat.errorCode != 5)) {
             isError = 1;
             errorCode = results.menuStat.errorCode;
             callback1();
           }
          if( isError == 0 && (results.menuStat.itemStat != 81 && results.menuStat.itemStat != 88)) {
             errorCode = 5;
             isError = 1;
             callback1();
           }
           if(isError == 0){ 
             itemStat = results.menuStat.itemStat;
             debuglog("Because it is not possible to authorise some menus in active tab and some in pending tab");
             callback1();
             }
             
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
              processStatus(req,res,6,data);
             
             }
      else
          {
           if(itemStat == 81)
            moveToMain(req,res,9,data,itemStat);
           else
            getMainItemsData(req,res,9,data,itemStat);
          }
     });




}

function getMainItemsData(req,res,stat,data,itemStat){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
    
     async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
     async.parallel({
         linkData: function(callback){utils.menuLinksWithNodeIs(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           if (isError == 0 && results.linkData.errorCode == 0) {
           debuglog(results.linkData);
           debuglog("This data is obtained from the main link table");
           debuglog("We will temporarily move all the items and groups");
           debuglog("from InUse status back and decrement their linked and used count");
           debuglog("Then again the release operation will increment it and ");
           debuglog("move it back to InUse status");
           debuglog("This seems to be much faster than checking 2 arrays and doing processing");
            itemsArr.push(results.linkData.itemCodes);
             callback1();
            }
          else
           callback1(); //Sometimes delete seems to happen before insert-->TODO:check
         }); 
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            changeMainItemStatus(req,res,9,data,itemsArr,itemStat);
          }
     });
}

function changeMainItemStatus(req,res,stat,data,itemsArr,itemStat){
    
    var async = require('async');
    var itemUtils = require('./utils');
    var groupUtils = require('../Groups/dataUtils');
    var isError = 0;
    
    var reason = 'Menu Approval changed Status'; 
    
    var item_tabl_name = data.orgId+"_menuitem_master";
    var group_tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr[0],function(key1,value1,callback1){
     debuglog("starting processing of items arr to change status");
     debuglog(key1);
     
       if(key1.nodeIs == 'item'){
          debuglog("This is an item");
          debuglog(key1);
          debuglog("we will move the item to Active status temporarily");
          debuglog("and decrement used and linked counter by 1");
          debuglog("this is to take care of Released and Edited Menu addition removal of items");
          itemUtils.moveItemStatus(item_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,1,function(err,results){
          if(err){
           debuglog("move reverse error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move reverse error");
           callback1();
          }
          });
         }
          
    if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          debuglog("we will move the group to Active status");
          debuglog("and decrement linked and used counter by 1");
          groupUtils.moveGroupStatus(group_tabl_name,data.orgId,key1.itemCode,'reverse',80,1,1,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
        }
       },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
            removeMain(req,res,9,data,itemStat);
          }
    });
}

//Now let us remove the 89 status row
function removeMain(req,res,stat,data,itemStat){
    var async = require('async');
    var menuArr = req.body.menuCodes;

    var menuutils = require('./menuutils');
    var isError = 0;
   
    var main_tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
    
    var reason = '';
      async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
             delStat: function(callback) {menuutils.deleteMenu(main_tabl_name,data.orgId,data.businessType,menuCode,callback); }
             },
         
          function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err) {
               processStatus(req,res,6,data);
            }
      else {
             moveToMain(req,res,9,data,itemStat); //ReleasedAndSubmitted
           }
          });
     
}

function moveToMain(req,res,stat,data,itemStat){
    
    var uid = req.body.userId;
    var menuArr = req.body.menuCodes;
    
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revision_tabl_name = 'Revision_'+orgId+'_menus_master'; //TODO: move to a common definition file
    var main_tabl_name = orgId+'_menus_master';

    var revisionFlag = 1;
    async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
          relStat: function (callback){utils.revisionData(revision_tabl_name,main_tabl_name,data.orgId,data.businessType,menuCode,revisionFlag,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           removeRevisionData(req,res,9,data,main_tabl_name,revision_tabl_name,itemStat); 
         }
     });
}

function removeRevisionData(req,res,stat,data,mastTable,revisionTable,itemStat){
    var async = require('async');
    var menuArr = req.body.menuCodes;

    var menuutils = require('./menuutils');
    var isError = 0;
   
     
    var reason = '';
      async.forEachOf(menuArr,function(key,value,callback1){
      menuCode = key;
     async.parallel({
             delStat: function(callback) {menuutils.deleteMenu(revisionTable,data.orgId,data.businessType,menuCode,callback); }
             },
         
          function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err) {
            
               processStatus(req,res,6,data);
            }
      else {
            if(itemStat == 88){
                debuglog("This is a 88 status menu in pending tab");
                debuglog("So we will redirect it to Release API");
                debuglog("which will change menu satus as well as linked Items and groups statuses");
                var release = require('./releaseMenus');
                release.changeMenuStatus(req,res,9,data,mastTable);
            } 
            else
               {
                debuglog("This is a 81 status menu so it has not been released yet");
                debuglog("after copying the data from pending tab to main tab");
                debuglog("we will change the menu and item status here");
                debuglog(mastTable);
                debuglog(data);
                changeMenuStatus(req,res,9,data,mastTable);
              }
          }
        });
     
}

function changeMenuStatus(req,res,stat,data,tabl_name){
    
    var menuArr = data.menuCodes;
    var async = require('async');
    var statutils = require('./statusutils');
    var isError = 0;
    
    var reason = ''; //no reason is currently set for approve operation
    
     
     async.forEachOf(menuArr,function(value,key,callback1){
       var menuCode = value;
       async.parallel({
         chngStatus: function(callback){statutils.changeMenuStatus(tabl_name,data.orgId,data.businessType,data.userId,menuCode,82,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             callback1();
            }
         });
         },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else{
            getItemsData(req,res,9,data);
          }
     });
}

function getItemsData(req,res,stat,data){
    
    var menuArr = data.menuCodes;
    var itemsArr = [];
    var async = require('async');
    var utils = require('./menuutils');
    var isError = 0;
    var orgId = data.orgId;
  
    var link_tabl_name = orgId+'_menus_items_link'; //TODO: move to a common definition file
    
     async.forEachOf(menuArr,function(value,key,callback1){
             var menuCode = value;
  
     async.parallel({
         linkData: function(callback){utils.menuLinksWithNodeIs(link_tabl_name,data.orgId,data.businessType,menuCode,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             callback1();
            }
           if (isError == 0 && results.linkData.errorCode == 0) {
           debuglog(results.linkData);
           debuglog("This can contain either item or group we will do corresponding processing ");
           debuglog("in next function");
            itemsArr.push(results.linkData.itemCodes);
             callback1();
            }
          else
           callback1(); //Sometimes delete seems to happen before insert-->TODO:check
         }); 
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            changeItemStatus(req,res,9,data,itemsArr);
          }
     });
}

//Note this is a special function not generic item status change
//Change status to Linked only if it is Entered
//dont change status if it is InUse may be due to other menus

function changeItemStatus(req,res,stat,data,itemsArr){
    
    var async = require('async');
    var itemUtils = require('./utils');
    var groupUtils = require('../Groups/dataUtils');
    var isError = 0;
    
    var reason = 'Menu Approval changed Status'; 
    
    var item_tabl_name = data.orgId+"_menuitem_master";
    var group_tabl_name = data.orgId+"_group_header";
     
     async.forEachOf(itemsArr[0],function(key1,value1,callback1){
     debuglog("starting processing of items arr to change status");
     debuglog(key1);
     
       if(key1.nodeIs == 'item'){
          debuglog("This is an item");
          debuglog(key1);
          debuglog("we will move the item to Linked status");
          debuglog("and increment linked counter by 1");
          debuglog("but dont increment used counter");
          debuglog("need to have seperate counters and manipualte them seperately");
          debuglog("since it is possible to directly add and remove from Released Menu");
          itemUtils.moveItemStatus(item_tabl_name,data.orgId,key1.itemCode,'forward',83,1,0,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
         }
          
    if(key1.nodeIs == 'group'){
          debuglog("This is a group");
          debuglog(key1);
          debuglog("we will move the group to Linked status");
          debuglog("and increment linked counter by 1");
          debuglog("but dont increment used counter");
          groupUtils.moveGroupStatus(group_tabl_name,data.orgId,key1.itemCode,'forward',83,1,0,function(err,results){
          if(err){
           debuglog("move forward error");
           debuglog(err);
           isError = 1;
           callback1();
          }
          else{
           debuglog("no move forward error");
           callback1();
          }
          });
        }
       },
     function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else
          {
            var tabl_name = data.orgId+'_menus_master'; //TODO: move to a common definition file
            getMenuDetails(req,res,9,data,tabl_name);
          }
    });
}

function getMenuDetails(req,res,stat,data,mastTable){
    
   var resultArr = [];
   var async = require('async');
   var utils = require('./menuutils');
   var isError = 0;
    
    async.forEachOf(data.menuCodes,function(key,value,callback){
    var menuCode = key;
    utils.menuDetail(mastTable,data.orgId,data.businessType,menuCode,function(err,result){
         if(err || result.errorCode != 0) {
              console.log(err);
              isError =1 ;
              callback();
              }
      else  {
            resultArr.push(result.masterData);
            callback();
           }
    });
    },
    function(err){
         if(err || isError == 1) 
               processStatus(req,res,6,data);
       else
          {
           data.menuData = resultArr;
           processStatus(req,res,9,data); 
         }
     });
    
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



