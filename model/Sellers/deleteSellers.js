function processRequest(req,res,data){
    checkSellerStatus(req,res,9,data);
}

function checkSellerStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var SellerArr = req.body.sellerIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.sellerIds = SellerArr;
    
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tblName = orgId+'_sellers'; //TODO: move to a common definition file

    async.forEachOf(SellerArr,function(key,value,callback1){
      var SellerData = extend({},data);
          SellerData.sellerId = key;
      debuglog("start checking if ID exists in Main table before delete");
      debuglog(SellerData)    
     datautils.checkSellerCode(tblName,SellerData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
              else{
                  if (result.status == 80){
                   debuglog("This is entered status item");
                   debuglog("So can be deleted directly");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 82){
                   debuglog("This is authorised status item");
                   debuglog("So has to go through authoristation to delete");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Delete is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              deleteSellers(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
              
               }
           }
      });
}


function deleteSellers(req,res,stat,data,enteredItems,authorisedItems){
    var uid = req.body.userId;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_sellers'; //delete is possible only in Main Table currently
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      sellerId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteSellerCode(mainTable,sellerId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
       else{
          if(authorisedItems.length > 0){
          debuglog("This request has some authorised items too");
          debuglog(authorisedItems);
          deleteAuthoriseSellers(req,res,9,data,authorisedItems);
          }
          else{
          debuglog("This request has no authorised items");
          debuglog("so call return");
          processStatus(req,res,9,data); 
        }
		}
      });
}

function deleteAuthoriseSellers(req,res,stat,data,authorisedItems){
    var uid = req.body.userId;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_sellers';
    var revisionTable = data.orgId+'_Revision_sellers';
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      sellerId = key;
     async.parallel({
          relStat: function (callback){datautils.copySellerHeader(mainTable,revisionTable,sellerId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           debuglog("Change status of items in revision table to ready2Delete");
           changeSellerStatus(req,res,9,data,authorisedItems,mainTable,revisionTable);
          }
      });
}


function changeSellerStatus(req,res,stat,data,authorisedItems,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(authorisedItems,function(key,value,callback1){
    sellerId = key;
    var reason = 'Delete Seller submitted';
       statutils.changeSellerStatus(revisionTable,data.orgId,data.businessType,data.userId,sellerId,89,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getSellerDetails(req,res,9,data,revisionTable);
             }
          });
     
}


function getSellerDetails(req,res,stat,data,revisionTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog("Status may be available only for Authorised items");
    debuglog("For entered items it will be entirely deleted from system");
    debuglog(data);
     
   datautils.SellersData(revisionTable,data.sellerIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getSellerStatusName(req,res,9,data,result); 
         }
     });
    
}

function getSellerStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('seller_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.sellerDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
exports.getData = processRequest;




