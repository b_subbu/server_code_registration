function processRequest(req,res,data){
      checkSellerStatus(req,res,9,data);
}


function checkSellerStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var SellerArr = req.body.sellerIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.sellerIds = SellerArr;
    
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tblName = orgId+'_Revision_sellers'; //TODO: move to a common definition file

    async.forEachOf(SellerArr,function(key,value,callback1){
      var SellerData = extend({},data);
          SellerData.sellerId = key;
      debuglog("start checking if ID exists in Revision table before authorise");
      debuglog(SellerData)    
     datautils.checkSellerCode(tblName,SellerData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
            else{
                 if (result.status == 81){
                   debuglog("This is entered status item");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 89){
                   debuglog("This is authorised delete item");
                   debuglog("So change status to deleted");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Authorise is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              moveToMain(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
             }
           }
      });
}



function moveToMain(req,res,stat,data,enteredItems,authorisedItems){
    
    debuglog("Now start moving the entries to Main Table");
    debuglog(enteredItems);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_sellers'; //TODO: move to a common definition file
    var mainTable    = orgId+'_sellers';
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      sellerId = key;
     async.parallel({
          relStat: function (callback){datautils.moveSellerHeader(revisionTable,mainTable,sellerId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
            if(authorisedItems.length > 0){
          debuglog("This request has some authorised to delete items too");
          debuglog(authorisedItems);
          deleteInMain(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable);
          }
          else{
          debuglog("This request has no authorised items");
           changeSellerStatus(req,res,9,data,enteredItems,mainTable,revisionTable); 
           }
         }
     });
}

function deleteInMain(req,res,stat,data,enteredItems,authorisedItems,mainTable,revisionTable){
    
    
    debuglog("Now start moving the entries to Main Table");
    debuglog("It is just enough to update status and remove from Main here");
    debuglog("But retaining functionality as is to reduce code");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
    debuglog("This may not really be required for Ready2Delete just keeping same to reduce code");
    
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      sellerId = key;
     async.parallel({
          relStat: function (callback){datautils.moveSellerHeader(revisionTable,mainTable,sellerId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeSellerStatusDelete(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable); 
         }
     });
}

function changeSellerStatusDelete(req,res,stat,data,enteredItems,authorisedItems,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(authorisedItems,function(key,value,callback1){
    sellerId = key;
    var reason = 'Seller Deleted';
       statutils.changeSellerStatus(mainTable,data.orgId,data.businessType,data.userId,sellerId,88,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 changeSellerStatus(req,res,9,data,enteredItems,mainTable,revisionTable);
                 debuglog("It is ok to call even if no enteredItems");
                 debuglog("As it will simply skip the loop");
             }
          });
     
}


function changeSellerStatus(req,res,stat,data,SellerArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(SellerArr,function(key,value,callback1){
    sellerId = key;
    var reason = '';
       statutils.changeSellerStatus(mainTable,data.orgId,data.businessType,data.userId,sellerId,82,'Seller Authorised',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.sellerIds = SellerArr;
                 getSellerDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getSellerDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.SellersData(mainTable,data.sellerIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getSellerStatusName(req,res,9,data,result); 
         }
     });
    
}

function getSellerStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('seller_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.sellerDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
exports.getData = processRequest;





