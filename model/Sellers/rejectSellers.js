function processRequest(req,res,data){
       checkSellerStatus(req,res,9,data);
}

function checkSellerStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var SellerArr = req.body.sellerIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.sellerIds = SellerArr;
    
    var orgId = data.orgId;
    
    var tblName = orgId+'_Revision_sellers'; //TODO: move to a common definition file

    async.forEachOf(SellerArr,function(key,value,callback1){
      var SellerData = extend({},data);
          SellerData.sellerId = key;
      debuglog("start checking if ID exists in Revision table before authorise");
      debuglog(SellerData)    
     datautils.checkSellerCode(tblName,SellerData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
            deleteSeller(req,res,9,data);
           }
      });
}


function deleteSeller(req,res,stat,data){
    
    var SellerArr = data.sellerIds;
    
    debuglog("Now start deleting the entries in Revision Table");
    debuglog("Otherwise we will have 2 rows in main table with same id but different status");
    debuglog(SellerArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_sellers'; //TODO: move to a common definition file
    var mainTable    = orgId+'_sellers';
    
    async.forEachOf(SellerArr,function(key,value,callback1){
     sellerId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteSellerCode(revisionTable,sellerId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
               processStatus(req,res,6,data);
            }
      else
          {
          changeSellerStatus(req,res,9,data,SellerArr,mainTable,revisionTable); 
         }
     });
}

function changeSellerStatus(req,res,stat,data,SellerArr,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(SellerArr,function(key,value,callback1){
    sellerId = key;
    var reason = 'Seller Rejected';
       statutils.changeSellerStatus(revisionTable,data.orgId,data.businessType,data.userId,sellerId,87,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.sellerIds = SellerArr;
                 getSellerDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getSellerDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.SellersData(mainTable,data.sellerIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getSellerStatusName(req,res,9,data,result); 
         }
     });
    
}

function getSellerStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('seller_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.sellerDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
exports.getData = processRequest;






