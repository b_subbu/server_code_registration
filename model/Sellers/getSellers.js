function getSellerTypesData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
   var mastutils = require('../Registration/masterUtils');
 
  
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    var searchArr = [];
      
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
  
    if(tid == 501)
     var tblName = oid+'_sellers';
    else
     var tblName = oid+'_Revision_sellers';
  
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'seller_type_status_master',callback); },
         currStat: function(callback){mastutils.masterTypes('currency_type_master',callback); }
        },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                data.currencies = results.currStat.types;
        
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList,tblName);
                  }
                  else {
                      var searchArr = [];
                      var tmpArray = {status_id: {"$in":statList}};
                      searchArr.push(tmpArray);
                      debuglog(searchArr);
                      getSellerCount(req,res,stat,data,searchArr,tblName);
                    }
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList,tblName) {
   var async = require('async');
   var filterutils = require('../Menu/filterUtils');
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
           debuglog("key name");
           debuglog(key_name);
        filterutils.filterOptions(key_name,statList,pid,tid,'seller_type_status_master',function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
               filterOptions[key_name] = results;
               callback();
            }
          }); 
        }
     else
        callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call alerts count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog("We cant compare orgid here since seller table does not have");
           debuglog(statList);
           var searchArr = [];
           //var userArray = {org_id: data.orgId };
           //searchArr.push(userArray);
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
           getSellerCount(req,res,stat,data,searchArr,tblName);
           }
         else 
           getSellerSearch(req,res,stat,data,statList,tblName);
         }
      });
}      


function getSellerSearch(req,res,stat,data,statList,tblName) {

 var searchInp = req.body.searchParams;

    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
   
 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 //var userArray = {};
 //searchArr.push(userArray);
  
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
       case 'date':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var cmpDate = new Date(minDateArr[2],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDateArr[2],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {update_time: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {update_time: {$lte: cmpDate2}};
        searchArr.push(tmpArray); 
        break; 
       case 'sellerName':
        var tmpArray = {sellerName: {$regex:key,$options:'i'}};
        searchArr.push(tmpArray);
        break; 
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           
            getSellerCount(req,res,9,data,searchArr,tblName);
            debuglog("We will start getting data with search Params");
            debuglog(searchArr);
          }
     });
}

function getSellerCount(req,res,stat,data,statList,tblName){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
    utils.SellerCount(tblName,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
           debuglog("After getting results");
           debuglog(results);
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getSellerTypes(req,res,9,data);
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getSellerData(req,res,9,data,statList,tblName); 
             }
            }
         });
}


function getSellerData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var utils = require('./dataUtils');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
   
      utils.SellerHeader(headerTable,pageId,size,statList,function(err,result) {
      debuglog(result);
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getSellerTypes(req,res,9,data); 
             //will be handled at count itself, just adding here
            }
            else {
            sellerTypes = result.sellerTypes;
            debuglog(sellerTypes); 
            getSellerStatusName(req,res,9,data,sellerTypes);
            }
          }
        });
} 


function getSellerStatusName(req,res,stat,data,sellerTypes) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(sellerTypes,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames('seller_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.productData = sellerTypes;
            getSellerTypes(req,res,9,data);
           }
      });
 

} 


function getSellerTypes(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');

 var tabl_name = 'seller_types_master';
 
  datautils.SellerMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.sellerTypes = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getProductData = getSellerTypesData;









