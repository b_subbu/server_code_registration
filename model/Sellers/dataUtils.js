function getSellerCount(tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getSellerHeader(tblName,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment=require('moment');
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
     
     var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {sellerId: doc.sellerId,
                        sellerName:doc.bus_name,
                        readonly: doc.enabled,
                        status:doc.status_id,
                        statusId:doc.status_id,
                        date: moment(doc.update_time).format('L')
      
                      };
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 3; //no data case
       data.sellerTypes = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.sellerTypes = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getSellerMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  
  var promise = db.get(tblName).find({},{sort:{seller_type_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.id = doc.seller_type_id;
        tmpArr.name = doc.seller_type_name;
        tmpArr.label = doc.seller_label;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


 function getSellerData(tblName,SellerData,callback) {

  var totalRows = 0;
  var data = {};
  var result = {};
  var promise = db.get(tblName).find({sellerId:SellerData.sellerId});
  promise.each(function(doc){
    totalRows++;
    result.sellerType = doc.sellerType;
    doc.bus_name ? data.displayName = doc.bus_name : '';
    doc.reg_name ? data.registeredName = doc.reg_name : '';
    doc.bus_addr ? data.sellerAddress = doc.bus_addr : ''; //if value is null dont assing property
    doc.bus_email ? data.sellerEmail = doc.bus_email : '';
    doc.bus_mobile ? data.sellerMobile = doc.bus_mobile : '';
    doc.bus_url ? data.website = doc.bus_url : '';
    doc.bus_descr ? data.description = doc.bus_descr : '';
    doc.country_id ? data.country = doc.country_id : '';
    doc.state_id ? data.state = doc.state_id : '';
    doc.city_id ? data.city = doc.city_id : '';
    doc.street ? data.street = doc.street : '';
    doc.building ? data.building = doc.building : '';
    doc.zip ? data.zipCode = doc.zip : '' ;
    doc.lat ? data.latitude = doc.lat : '' ;
    doc.longi ? data.longitude = doc.longi : '' ;
    doc.currency_id ? data.baseCurrency = doc.currency_id : '' ;
    doc.cin_number ? data.cinNumber = doc.cin_number: '';
    doc.vat_number ? data.vatNumber = doc.vat_number: '';
    doc.tin_number ? data.tinNumber = doc.tin_number: '';
    doc.cst_number ? data.cstNumber = doc.cst_number: '';
    doc.status_id ? result.status = doc.status_id : '';
    doc.status_id ? result.statusId = doc.status_id : '';
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0)  {
      result.errorCode = 1; //no partial save case
      result.subscription = {};
      callback(null,result);
    }
    else if(totalRows > 1)
      callback(6,null); //system error should be 1 row only
    else {
      result.errorCode = 0;
      result.businessDetails = data;
      callback(null,result);
      }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(null,result);//system error
  });
}

function checkSellerCode(tblName,SellerData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("We are checking only in Main table");
debuglog("Any row directly submitted is not problem");
debuglog("since it will just overwrite main row when authorised");
debuglog(SellerData);

var promise = db.get(tblName).find({sellerId: SellerData.sellerId});
promise.each(function(doc){
   status = doc.status_id;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 5;
   data.status = status;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}


function getSellersData(tblName,sellerIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment=require('moment');
  
   var promise = db.get(tblName).find({sellerId: {$in: sellerIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {
                        sellerId: doc.sellerId,
                        sellerName:doc.bus_name,
                        readonly: doc.enabled,
                        status:doc.status_id,
                        statusId:doc.status_id
                        
        };
        tmpArray.date = moment(doc.update_time).format('L');
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function moveSellerHeader(srcTable,destTable,sellerId,callback){

var promise1 = db.get(destTable).remove({sellerId:sellerId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({sellerId:sellerId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({sellerId:sellerId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function deleteSellerRow(tblName,sellerId,callback){
  
var promise = db.get(tblName).remove({sellerId:sellerId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function checkSellerCodeMain(SellerData,callback){
 var tblName = SellerData.orgId+'_sellers';
 var revisiontblName = SellerData.orgId+'_Revision_sellers';
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
       debuglog("Check Count in Revision also because it is possible");
       debuglog("user might have directly submitted in Create");
       checkSellerCodeRevision(doc,revisiontblName,callback);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkSellerCodeRevision(activeCount,tblName,callback){
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
      debuglog("Count from revision table and active table are");
      debuglog(doc);
      debuglog(activeCount); 
      var totalCount = doc+activeCount;
       debuglog("So the sum of 2 is");
       debuglog("This is the total of Sellers created by this business");
       debuglog("Since release & edit is not possible also delete is not possible");
       debuglog("can take next Id to be incr of sum total");
       debuglog("Otherwise change this logic");
       debuglog("Note there will be always one row in seller with Id as 1 this is the main business");
       debuglog(totalCount);
       totalCount++;
       debuglog("Will increment by 1");
       debuglog(totalCount);
       pgId = totalCount;
       debuglog("And the new Id is");
       debuglog(pgId);
       callback(null,pgId);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}


function updateSellerDetail(uid,userDets,tblName,callback){

 debuglog("This is same as in Admin updateSellerDetail");
 debuglog("But since Front end needs to have seller XXX instead of businessXXX");
 debuglog("we have this new api");
 debuglog("And one new field basecurrency for seller which can be different from business maybe");
 
 var promise = db.get(tblName).update({sellerId:userDets.sellerId},
  { $set:{
    sellerId: userDets.sellerId,
    sellerType: userDets.sellerType,
    bus_name: userDets.displayName,
    reg_name: userDets.registeredName,
    bus_addr: userDets.sellerAddress,
    bus_email: userDets.sellerEmail,
    bus_mobile: userDets.sellerMobile,
    bus_url: userDets.website,
    bus_descr: userDets.description,
    country_id: userDets.country,
    state_id: userDets.state,
    city_id: userDets.city,
    street: userDets.street,
    building: userDets.building,
    zip: userDets.zipCode,
    lat: parseFloat(userDets.latitude),
    longi: parseFloat(userDets.longitude),
    currency_id: userDets.baseCurrency,
    cin_number: userDets.cinNumber,
    vat_number: userDets.vatNumber,
    tin_number: userDets.tinNumber,
    cst_number: userDets.cstNumber,
	  enabled: false,
	  status_id: userDets.status_id,
    baseCurrency: userDets.baseCurrency
    }},
  {upsert: true}
);
  promise.on('success',function(doc){
   callback(null,null)
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function copySellerHeader(srcTable,destTable,sellerId,callback){

var promise1 = db.get(destTable).remove({sellerId:sellerId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({sellerId:sellerId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
           callback(null,null);
        });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

exports.SellerCount = getSellerCount;
exports.SellerMaster = getSellerMaster;
exports.SellerData = getSellerData;
exports.SellersData = getSellersData;
exports.moveSellerHeader = moveSellerHeader;
exports.checkSellerCode = checkSellerCode;
exports.SellerHeader = getSellerHeader;
exports.deleteSellerCode = deleteSellerRow;
exports.generateSellerCode = checkSellerCodeMain;
exports.SellerUpdate = updateSellerDetail;
exports.copySellerHeader = copySellerHeader;





