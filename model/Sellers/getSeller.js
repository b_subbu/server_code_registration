function processRequest(req,res,data){
   if(typeof req.body.sellerId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
       
   debuglog("checkMasterStatus is not removed here");
   debuglog("This is just used for getting currencies");
   debuglog("So double getting from user table");
   debuglog("If we add currency in main call then all api will get currency");
   debuglog("which may not be desirable");    
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var mastutils = require('../Registration/masterUtils');
    var utils = require('./dataUtils');
    var isError = 0;
    data.userId = uid;
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         currStat: function(callback){mastutils.masterTypes('currency_type_master',callback); }
        },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
           if(isError == 0) {
              data.currencies = results.currStat.types;
              getSellerData(req,res,9,data);
            }
          }); 
}


function getSellerData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.sellerId = req.body.sellerId;
    var tid = req.body.tabId;
    
    if(tid == 501)
     var tabl_name = data.orgId+'_sellers';
    else
     var tabl_name = data.orgId+'_Revision_sellers';
   
     var extend = require('util')._extend;
     var SellerData = extend({},data);
     
  
      datautils.SellerData(tabl_name,SellerData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              getSellerTypes(req,res,3,data);
              isError = 1;
              //return;
              debuglog("result errorCode is 1");
              }
            else {
                var sellerDetails = {};
                sellerDetails.sellerId = data.sellerId;
                sellerDetails.sellerType = result.sellerType;
                sellerDetails.sellerDetails = result.businessDetails;
                data.sellerData = sellerDetails;
                data.status = result.status;
                data.statusId = result.statusId;
                getSellerTypes(req,res,9,data);
                debuglog("No error case");
                debuglog(sellerDetails);
            }
          }
        });
} 


function getSellerTypes(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');

 var tabl_name = 'seller_types_master';
 
  datautils.SellerMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.sellerTypes = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getData = processRequest;










