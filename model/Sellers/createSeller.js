function processRequest(req,res,data){
        getSellerCode(req,res,9,data);
}

function getSellerCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var SellerData = {};
       SellerData =  req.body.sellerDetails;
       SellerData.status_id = 80;
       SellerData.sellerType = req.body.sellerType;
       SellerData.orgId = data.orgId;
      
     var tblName = data.orgId+'_sellers'; //create will always be in main table
     debuglog("Check if the  Id exists or not");
     debuglog(SellerData);
     debuglog(req.body);
      datautils.generateSellerCode(SellerData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
               if(err) 
                 processStatus(req,res,result.errorCode,data);
               else  {
                debuglog("The newly generated seller code is:");
                debuglog(result);
                SellerData.sellerId = result
                data.sellerId = result;
                saveSellerData(req,res,9,data,SellerData,tblName);
                }
            }
        });
} 

function saveSellerData(req,res,stat,data,SellerData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
     debuglog("This is after getting data from request");
     debuglog(SellerData);
     debuglog(data);
     
     
    var submitop = req.body.submitOperation;
  
   
    if(   SellerData.sellerType == null
       || SellerData.sellerType == 'undefined'
       || SellerData.displayName == null
       || SellerData.displayName == 'undefined'
       || SellerData.sellerEmail == null
       || SellerData.sellerEmail == 'undefined'
      )
        processStatus(req,res,4,data);
    else{
    
     datautils.SellerUpdate('',SellerData,tblName,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(SellerData);
               if(submitop == 1){
                var sellerIds = [];
                    sellerIds.push(data.sellerId);
                    data.sellerIds = sellerIds;
               var submitSeller = require('./submitSellers');
               submitSeller.moveToRevision(req,res,9,data);
             }
             else{
                changeSellerStatus(req,res,9,data,SellerData,tblName);
                }
            }
          });
        }
} 


function changeSellerStatus(req,res,stat,data,SellerData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeSellerStatus(tblName,data.orgId,data.businessType,data.userId,data.sellerId,80,'Seller Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getSellerData(req,res,9,data,SellerData,tblName);
               }
        });
} 

function getSellerData(req,res,stat,data,SellerData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
    
    var sellerArr = [];
    sellerArr.push(data.sellerId) 
   datautils.SellersData(tblName,sellerArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getSellerStatusName(req,res,9,data,result); 
         }
     });
    
}

function getSellerStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('seller_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.sellerDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 



function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
exports.getData = processRequest;











