function changeSellerStatus(tablName,orgId,bizType,operatorId,sellerId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {sellerId: sellerId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createSellerStatusChngLog(orgId,bizType,operatorId,sellerId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createSellerStatusChngLog(orgId,bizType,operatorId,sellerId,stat,reason,callback){
 
  var promise = db.get('seller_type_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    sellerId: sellerId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }


 exports.changeSellerStatus = changeSellerStatus;
