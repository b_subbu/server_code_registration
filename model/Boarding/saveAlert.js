<<<<<<< HEAD
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   var userdata = "";
   var files = [];
   var idx = 0;
   var jdx = 0;
   var actions = [];
   var formidable = require('formidable');
   
   var form = new formidable.IncomingForm();
   form.uploadDir = app.get('fileuploadfolder')+'/tmp';
   form.multiples = true;
   form.on('field', function(name, value) {
       if(name == 'enabledAnalytics[]') {
        actions[jdx] = parseInt(value);
        jdx++;
        }
      });
   form.parse(req, function (err, fields) {
     userdata = fields;
    });
    form.on('file',function(name,file) {
    files[idx] = file;
     idx++;
    });
   form.on('end',function() {
      checkMasterStatus(req,res,data,userdata,actions,files);
      debuglog("checkMasterStatus is used here because it is multipart form data");
    });
 } 


function checkMasterStatus(req,res,data,userdata,actions,files){

    var uid = userdata.userId;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var type = require('type-detect');
    
   
    data.userId = uid;
      if( typeof uid === 'undefined' || uid === 'undefined' || type(uid) !== 'string'){
         processStatus(req,res,6,data); //system error
         //exit(0); //does not seem to work in multi-thread mode
         return; 
         }
 
  var async = require('async');
  var roleutils = require('../Roles/roleutils');
  var isError = 0;
 
//call all functions async in parallel
//but proceed only after completion and checks


  async.parallel({
    bizStat: function(callback){roleutils.usrStatus(uid,callback); },
    },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    if(isError == 0 && results.bizStat.errorCode != 0) {
      processStatus (req,res,results.bizStat.errorCode,data);
      isError = 1;
      return;
    }
    if( isError == 0 && results.bizStat.userStatus != 9) {
      processStatus (req,res,2,data);
      isError = 1;
      return;
    }
    if (isError == 0) {
      data.orgId = results.bizStat.orgId;
      processAlert(req,res,9,data,userdata,actions,files);
    }
  });
}

function processAlert(req,res,stat,data,userdata,actions,files) {
  isError = 0;
  var type = require('type-detect');
  var ops = 'save';
  var alrId = '';
  
 var alrTitle = userdata.contentTitle;
 var alrType  = userdata.alertType
 
 if( typeof userdata.alertId === 'undefined' || userdata.alertId === 'undefined' || type(userdata.alertId) !== 'string' || userdata.alertId.length <=0 ){
    ops = 'save';
    debuglog("This is a new Alert so we will create it");
    }
 else {
   alrId = userdata.alertId;
   debuglog("This is a edit opertion");
   debuglog(alrId);
   if(userdata.submitOperation == "1")
     ops = 'submit';
   else
     ops = 'editSave';
      
 
 }
 if( isError == 0 &&(alrTitle === 'undefined' || alrTitle.length < 1 || alrTitle === null || alrTitle > 60)) {
    processStatus(req,res,3,data);
    isError = 1;
    return;
   }
 
  if( isError == 0) {
           if(ops == 'save'){
              debuglog("Now let us create the alert");
              debuglog(alrType);
              if(alrType == 11){
               debuglog("Let us create shop alert");
               createShopAlertDetail(req,res,data,userdata,actions,files);
               }
              else{
                debuglog("Let us create Learn alert");
                debuglog("Note there are only 2 types now but if more types would come in future");
                debuglog("need to add them here");
                debuglog(alrType);
                createLearnAlertDetail(req,res,data,userdata,actions,files);
               }
              }
           else{
              debuglog("Now let us edit the alert");
              debuglog(alrType);
              if(alrType == 11){
               debuglog("Let us edit shop alert");
               updateShopAlertDetail(req,res,data,userdata,actions,files,alrId,ops)
              }
            else{
                debuglog("Let us edit Learn alert");
                debuglog("Note there are only 2 types now but if more types would come in future");
                debuglog("need to add them here");
                debuglog(alrType);
                updateLearnAlertDetail(req,res,data,userdata,actions,files,alrId,ops);
               }
              }
          
              
     }
}            


function createShopAlertDetail(req,res,data,userdata,actions,files){

 //use a running sequence for alert_id instead of default mongoDB id
 //because this may be referenced from outside
 
   var utils = require('./utils');
   var moment = require('moment');
   var org_id = data.orgId;
   
   utils.alertSequence(org_id,function(err,result){
   if(err){
    console.log(err);
    processStatus(req,res,6,data);
    return;
   
   }
   else {  
    
      if(userdata.submitOperation == "1")
         var alrStatus = 81;
      else
         var alrStatus = 80;    

    var promise = db.get('alert_master').insert({
      alert_id: result,
      org_id: org_id,
      title: userdata.contentTitle,
      descr: userdata.contentDescription,
      content_type_id: parseInt(userdata.contentType),
      alert_status_id: alrStatus,
      alert_type_id: parseInt(userdata.alertType),
      content_target: userdata.contentTarget,
      alert_life: parseInt(userdata.alertLife),
      link_type_id: userdata.linkType,
      link_id: userdata.linkId,
      create_date: new Date(),
      update_time: new Date(),
      template_id: 0,
    });
    promise.on('success',function(doc){
      data.alertId = result;
      saveFiles(req,res,data,userdata,actions,files);
     createAlertStatus(req,res,data,userdata,actions,files);
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
    }
  });
}

function createLearnAlertDetail(req,res,data,userdata,actions,files){

 //use a running sequence for alert_id instead of default mongoDB id
 //because this may be referenced from outside
 
   var utils = require('./utils');
   var moment = require('moment');
   var org_id = data.orgId;
   
   debuglog("This is Learn alert");
   debuglog("Very similar to shop alert but input fields change");
   
   utils.alertSequence(org_id,function(err,result){
   if(err){
    console.log(err);
    processStatus(req,res,6,data);
    return;
   
   }
   else {  
    
      if(userdata.submitOperation == "1")
         var alrStatus = 81;
      else
         var alrStatus = 80;    

    var promise = db.get('alert_master').insert({
      alert_id: result,
      org_id: org_id,
      title: userdata.contentTitle,
      descr: userdata.contentDescription,
      content_type_id: parseInt(userdata.contentType),
      alert_status_id: alrStatus,
      alert_type_id: parseInt(userdata.alertType),
      content_target: userdata.contentTarget,
      alert_life: parseInt(userdata.alertLife),
      link_content: userdata.linkContent,
      unique_token: userdata.uniqueToken,
      create_date: new Date(),
      update_time: new Date(),
      template_id: 0,
    });
    promise.on('success',function(doc){
      data.alertId = result;
      debuglog("now save images for this alert Id");
      debuglog(data);
      saveFiles(req,res,data,userdata,actions,files);
      createAlertStatus(req,res,data,userdata,actions,files);
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
    }
  });
}

function createAlertStatus(req,res,data,userdata,actions,files){
  var alert_id = data.alertId;
  if(userdata.submitOperation == "1")
         var alrStatus = 81;
      else
         var alrStatus = 80;    

  var promise = db.get('alert_status').insert({
    alert_id: alert_id,
    alert_status_id: alrStatus,
    operatorId: userdata.userId,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     saveAlertImages(req,res,data,userdata,actions,files);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function updateShopAlertDetail(req,res,data,userdata,actions,files,alrId,ops){

 //use a running sequence for alert_id instead of default mongoDB id
 //because this may be referenced from outside
 
   var utils = require('./utils');
   var moment = require('moment');
   
   var org_id = data.orgId;
   if(ops == 'submit')
     alrStat = 81;//Ready2Auth
    else
     alrStat = 80;
    
    var promise = db.get('alert_master').update(
      {alert_id: alrId},
      {$set:{
      org_id: org_id,
      title: userdata.contentTitle,
      descr: userdata.contentDescription,
      content_type_id: parseInt(userdata.contentType),
      alert_status_id: alrStat,
      alert_type_id: parseInt(userdata.alertType),
      content_target: userdata.contentTarget,
      alert_life: parseInt(userdata.alertLife),
      link_type_id: userdata.linkType,
      link_id: userdata.linkId,
      create_date: new Date(),
      template_id: 0,
    }
    });
    promise.on('success',function(doc){
      data.alertId = alrId;
      saveFiles(req,res,data,userdata,actions,files);
     updateAlertStatus(req,res,data,userdata,actions,files,alrId,ops);
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
}


function updateLearnAlertDetail(req,res,data,userdata,actions,files,alrId,ops){

 //use a running sequence for alert_id instead of default mongoDB id
 //because this may be referenced from outside
 
   var utils = require('./utils');
   var moment = require('moment');
   
   var org_id = data.orgId;
   if(ops == 'submit')
     alrStat = 81;//Ready2Auth
    else
     alrStat = 80;
    
    var promise = db.get('alert_master').update(
      {alert_id: alrId},
      {$set:{
      org_id: org_id,
      title: userdata.contentTitle,
      descr: userdata.contentDescription,
      content_type_id: parseInt(userdata.contentType),
      alert_status_id: alrStat,
      alert_type_id: parseInt(userdata.alertType),
      content_target: userdata.contentTarget,
      alert_life: parseInt(userdata.alertLife),
      link_content: userdata.linkContent,
      unique_token: userdata.uniqueToken,
      create_date: new Date(),
      template_id: 0,
    }
    });
    promise.on('success',function(doc){
      data.alertId = alrId;
      debuglog(" Save images for this alert Id");
      saveFiles(req,res,data,userdata,actions,files);
 
      updateAlertStatus(req,res,data,userdata,actions,files,alrId,ops);
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
}

function updateAlertStatus(req,res,data,userdata,actions,files,alrId,ops){
    if(ops == 'submit'){
     alrStat = 81;//Ready2Auth
     reason = ''
     }
    else{
     alrStat = 80;
     reason = 'Edit and Save';
     }
   
  
  var promise = db.get('alert_status').insert({
    alert_id: alrId,
    alert_status_id: alrStat,
    operatorId: userdata.userId,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     updateAlertImages(req,res,data,userdata,actions,files,alrId,ops);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function saveFiles(req,res,data,userdata,actions,files) {
    var fs = require('fs');
    var mkdirp = require('mkdirp');
    var newfolder = app.get('fileuploadfolder')+userdata.userId+'/'+data.alertId+'/';
    mkdirp(newfolder,function(err,made){

   for(idx = 0; idx < files.length;idx++) {
      var oldfile = files[idx].path;
      //var newfile = newfolder+files[idx].name;
      var newfile = newfolder+data.alertId+"_IMAGE_"+idx;
    
       if(err) { 
       console.log(err);
       fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove file failed");
             console.log(err);
          }
          return;
         });
       }
     else if(files[idx].size == 0) {
        fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove empty file failed");
             console.log(err);
          }
          return;
         });
       }
      else {
        fs.rename(oldfile,newfile,function(err) {
         if(err) {
               console.log("Moving file failed");
               console.log(err);
           }
        return;
      });
     }
    }
   });
}


function saveAlertImages(req,res,data,userdata,actions,files){
    var alert_id = data.alertId;
    var isError = 0;
    //user can save multiple times so every time we remove existing files
    //and then insert new files here
    //This seems to be the only way to remove any unwanted images 
    db.get('alert_images').remove({alert_id: alert_id});
    
    if(files.length > 0) {
      for(idx = 0; idx < files.length;idx++) {
           var filename = files[idx].name;
           if(filename) {
             //formidable adds a blank row even if no file so need to insert 
           //for this filename will not be set
        
            //var alert_image = '/upload_files/'+userdata.userId+'/'+data.alertId+'/'+filename;
           var alert_image = '/upload_files/'+userdata.userId+'/'+encodeURIComponent(data.alertId)+'/'+encodeURIComponent(data.alertId)+'_IMAGE_'+idx;
           
           var promise = db.get('alert_images').insert( 
                                               {alert_id: alert_id,
                                                image_url: alert_image,
                                                image_seq: idx,
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
         }
       }
       }
       else {
          var promise = db.get('alert_images').insert( 
                                               {alert_id: alert_id,
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
       }
     if(isError == 0)
       saveAlertActions(req,res,data,actions,userdata,actions);
     else
       processStatus(req,res,6,data);
}


function updateAlertImages(req,res,data,userdata,actions,files,alrId,ops){
    var isError = 0;
   
   // In edit and submit screens users can either
   //1: Delete the existing image and add new ones
   //2: Or just add new ones
   //3: Or may be change from Image to video totally
   //for now we just add code to insert images if new files are added
   //+update video urls
   //the case of deleted images can be handled later
   //this needs to add an array of urls in frontend and then compare and delete
   //because we dont send files rather urls
    
    if(files.length > 0) {
      for(idx = 0; idx < files.length;idx++) {
           var filename = files[idx].name;
           //formidable adds a blank row even if no file so need to insert 
           //for this filename will not be set
           if(filename) {
       //var alert_image = '/upload_files/'+userdata.userId+'/'+data.alertId+'/'+filename;
         var alert_image = '/upload_files/'+userdata.userId+'/'+encodeURIComponent(data.alertId)+'/'+encodeURIComponent(data.alertId)+'_IMAGE_'+idx;
                var promise = db.get('alert_images').insert( 
                                               {alert_id: alrId,
                                                image_url: alert_image,
                                                image_seq: idx,
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
         }
        }
       }
      
          var promise = db.get('alert_images').update(
                                              {alert_id: alrId }, 
                                               {$set: {
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       saveAlertActions(req,res,data,actions,userdata,actions);
     else
       processStatus(req,res,6,data);
}

function saveAlertActions(req,res,data,actions,userdata,actions){
    var alert_id = data.alertId;
    var isError = 0;
   
    //user can save multiple times so every time we remove existing actions before save
    //This seems to be the only way to uncheck a box 
    db.get('alert_actions').remove({alert_id: alert_id});
   
    for (jdx =0; jdx < actions.length; jdx++) {
      var curr_action = actions[jdx];
      var promise = db.get('alert_actions').insert( 
                                                   {alert_id: alert_id,
                                                    action_type_id: curr_action,
                                                    create_date: new Date()
                                                   });
       promise.on('success',function(doc){
           //processStatus(req,res,9,data);
         });
       promise.on('error',function(err){
            console.log(err);
            isError = 1;
           //processStatus(req,res,6,data);//system error 
         });
      }
     
    if(isError == 0)
     getAlertData(req,res,9,data);
     //processStatus(req,res,9,data);
    else
     processStatus(req,res,6,data);
}

function getAlertData(req,res,stat,data){
    
    var aid = data.alertId;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    
     async.parallel({
         alrData: function(callback){utils.alertData(aid,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             data.alertData = results.alrData;
             processStatus(req,res,9,data);
            }
         }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   var userdata = "";
   var files = [];
   var idx = 0;
   var jdx = 0;
   var actions = [];
   var formidable = require('formidable');
   var form = new formidable.IncomingForm();
   form.uploadDir = app.get('fileuploadfolder')+'/tmp';
   form.multiples = true;
   form.on('field', function(name, value) {
       if(name == 'alertActions[]') {
        actions[jdx] = value;
        jdx++;
        }
      });
   form.parse(req, function (err, fields) {
     userdata = fields;
    });
    form.on('file',function(name,file) {
    files[idx] = file;
     idx++;
    });
    form.on('end',function() {
      checkMasterStatus(req,res,data,userdata,actions,files);
    });
 } 


function checkMasterStatus(req,res,data,userdata,actions,files){

    var uid = userdata.userId;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var type = require('type-detect');
    
   
    data.userId = uid;
      if( typeof uid === 'undefined' || uid === 'undefined' || type(uid) !== 'string'){
         processStatus(req,res,6,data); //system error
         //exit(0); //does not seem to work in multi-thread mode
         return; 
         }
 
  var async = require('async');
  var roleutils = require('../Roles/roleutils');
  var isError = 0;
 
//call all functions async in parallel
//but proceed only after completion and checks


  async.parallel({
    bizStat: function(callback){roleutils.usrStatus(uid,callback); },
    },
  function(err,results) {
    if(err) {
      console.log(err);
      processStatus(req,res,6,data);//system error
      isError = 1;
      return;
    }
    if(isError == 0 && results.bizStat.errorCode != 0) {
      processStatus (req,res,results.bizStat.errorCode,data);
      isError = 1;
      return;
    }
    if( isError == 0 && results.bizStat.userStatus != 9) {
      processStatus (req,res,2,data);
      isError = 1;
      return;
    }
    if (isError == 0) {
      data.orgId = results.bizStat.orgId;
      processAlert(req,res,9,data,userdata,actions,files);
    }
  });
}

function processAlert(req,res,stat,data,userdata,actions,files) {
  isError = 0;
  var type = require('type-detect');
  var ops = 'save';
  var alrId = '';
  
 var alrTitle = userdata.contentTitle;
 
 if( typeof userdata.alertId === 'undefined' || userdata.alertId === 'undefined' || type(userdata.alertId) !== 'string' || userdata.alertId.length <=0 )
    ops = 'save';
 else {
   alrId = userdata.alertId;
   if(userdata.submitOperation == "1")
     ops = 'submit';
   else
     ops = 'editSave';
      
 
 }
  /**
   *No validations at server end except for Title  
  if( alrType === 'undefined' || alrType.length < 1 || alrType === null || type(parseInt(alrType)) !== 'number') {
    processStatus(req,res,3,data);
    isError = 1;
    return;
  }
  if( isError == 0 &&(conType === 'undefined' || conType.length < 1 || conType === null || type(parseInt(conType)) !== 'number')) {
    processStatus(req,res,4,data);
    isError = 1;
    return;
  }
  if( isError == 0 &&(alrLife === 'undefined' || alrLife.length < 1 || alrLife === null || type(parseInt(alrLife)) !== 'number')) {
    processStatus(req,res,5,data);
    isError = 1;
    return;
  }

  var alrLifeNumeric = parseInt(alrLife);
  if( isError == 0 &&(alrLifeNumeric < 0 || alrLifeNumeric > 200 )) {
    processStatus(req,res,5,data);
    isError = 1;
    return;
  }
  
  */
  
 if( isError == 0 &&(alrTitle === 'undefined' || alrTitle.length < 1 || alrTitle === null || alrTitle > 60)) {
    processStatus(req,res,3,data);
    isError = 1;
    return;
   }
 
  if( isError == 0) {
           if(ops == 'save')
              createAlertDetail(req,res,data,userdata,actions,files);
           else
              updateAlertDetail(req,res,data,userdata,actions,files,alrId,ops)
     }
}            


function createAlertDetail(req,res,data,userdata,actions,files){

 //use a running sequence for alert_id instead of default mongoDB id
 //because this may be referenced from outside
 
   var utils = require('./utils');
   var moment = require('moment');
   var org_id = data.orgId;
   
    /*data.contentTitle = userdata.contentTitle;
    data.date = moment().format('L');
    data.alertType = parseInt(userdata.alertType);
    data.status = 80;
    data.contentTarget = userdata.contentTarget;
    data.views = 0; //need to do after implementing rendering 
    */
    
   utils.alertSequence(org_id,function(err,result){
   if(err){
    console.log(err);
    processStatus(req,res,6,data);
    return;
   
   }
   else {  
    
      if(userdata.submitOperation == "1")
         var alrStatus = 81;
      else
         var alrStatus = 80;    

    var promise = db.get('alert_master').insert({
      alert_id: result,
      org_id: org_id,
      title: userdata.contentTitle,
      descr: userdata.contentDescription,
      content_type_id: parseInt(userdata.contentType),
      alert_status_id: alrStatus,
      alert_type_id: parseInt(userdata.alertType),
      content_target: userdata.contentTarget,
      alert_life: parseInt(userdata.alertLife),
      create_date: Date('now'),
      template_id: 0,
    });
    promise.on('success',function(doc){
      data.alertId = result;
      saveFiles(req,res,data,userdata,actions,files);
     createAlertStatus(req,res,data,userdata,actions,files);
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
    }
  });
}

function createAlertStatus(req,res,data,userdata,actions,files){
  var alert_id = data.alertId;
  if(userdata.submitOperation == "1")
         var alrStatus = 81;
      else
         var alrStatus = 80;    

  var promise = db.get('alert_status').insert({
    alert_id: alert_id,
    alert_status_id: alrStatus,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     saveAlertImages(req,res,data,userdata,actions,files);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}

function updateAlertDetail(req,res,data,userdata,actions,files,alrId,ops){

 //use a running sequence for alert_id instead of default mongoDB id
 //because this may be referenced from outside
 
   var utils = require('./utils');
   var moment = require('moment');
   
   var org_id = data.orgId;
   if(ops == 'submit')
     alrStat = 81;//Ready2Auth
    else
     alrStat = 80;
    
    /*data.contentTitle = userdata.contentTitle;
    data.date = moment().format('L');
    data.alertType = parseInt(userdata.alertType);
    data.status = alrStat;
    data.contentTarget = userdata.contentTarget;
    data.views = 0; //need to do after implementing rendering 
    */
    var promise = db.get('alert_master').update(
      {alert_id: alrId},
      {$set:{
      org_id: org_id,
      title: userdata.contentTitle,
      descr: userdata.contentDescription,
      content_type_id: parseInt(userdata.contentType),
      alert_status_id: alrStat,
      alert_type_id: parseInt(userdata.alertType),
      content_target: userdata.contentTarget,
      alert_life: parseInt(userdata.alertLife),
      create_date: Date('now'),
      template_id: 0,
    }
    });
    promise.on('success',function(doc){
      data.alertId = alrId;
      saveFiles(req,res,data,userdata,actions,files);
     updateAlertStatus(req,res,data,userdata,actions,files,alrId,ops);
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
}

function updateAlertStatus(req,res,data,userdata,actions,files,alrId,ops){
    if(ops == 'submit'){
     alrStat = 81;//Ready2Auth
     reason = ''
     }
    else{
     alrStat = 80;
     reason = 'Edit and Save';
     }
   
  
  var promise = db.get('alert_status').insert({
    alert_id: alrId,
    alert_status_id: alrStat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     updateAlertImages(req,res,data,userdata,actions,files,alrId,ops);
  });
  promise.on('error',function(err){
    console.log(err);
    processStatus(req,res,6,data);//system error
  });
}



function saveFiles(req,res,data,userdata,actions,files) {
    var fs = require('fs');
    var mkdirp = require('mkdirp');
    var newfolder = app.get('fileuploadfolder')+userdata.userId+'/'+data.alertId+'/';
    mkdirp(newfolder,function(err,made){

   for(idx = 0; idx < files.length;idx++) {
      var oldfile = files[idx].path;
      var newfile = newfolder+files[idx].name;
    
       if(err) { 
       console.log(err);
       fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove file failed");
             console.log(err);
          }
          return;
         });
       }
     else if(files[idx].size == 0) {
        fs.unlink(oldfile,function(err){
          if(err) {
             console.log("remove empty file failed");
             console.log(err);
          }
          return;
         });
       }
      else {
        fs.rename(oldfile,newfile,function(err) {
         if(err) {
               console.log("Moving file failed");
               console.log(err);
           }
        return;
      });
     }
    }
   });
}


function saveAlertImages(req,res,data,userdata,actions,files){
    var alert_id = data.alertId;
    var isError = 0;
   
    //user can save multiple times so every time we remove existing files
    //and then insert new files here
    //This seems to be the only way to remove any unwanted images 
    db.get('alert_images').remove({alert_id: alert_id});
    
    if(files) {
      for(idx = 0; idx < files.length;idx++) {
           var filename = files[idx].name;
           if(filename) {
             //formidable adds a blank row even if no file so need to insert 
           //for this filename will not be set
        
            var alert_image = '/upload_files/'+userdata.userId+'/'+data.alertId+'/'+filename;
           var promise = db.get('alert_images').insert( 
                                               {alert_id: alert_id,
                                                image_url: alert_image,
                                                image_seq: idx,
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
         }
       }
       }
       else {
          var promise = db.get('alert_images').insert( 
                                               {alert_id: alert_id,
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
       }
     if(isError == 0)
       saveAlertActions(req,res,data,actions,userdata,actions);
     else
       processStatus(req,res,6,data);
}


function updateAlertImages(req,res,data,userdata,actions,files,alrId,ops){
    var isError = 0;
   
   // In edit and submit screens users can either
   //1: Delete the existing image and add new ones
   //2: Or just add new ones
   //3: Or may be change from Image to video totally
   //for now we just add code to insert images if new files are added
   //+update video urls
   //the case of deleted images can be handled later
   //this needs to add an array of urls in frontend and then compare and delete
   //because we dont send files rather urls
    
    if(files) {
      for(idx = 0; idx < files.length;idx++) {
           var filename = files[idx].name;
           //formidable adds a blank row even if no file so need to insert 
           //for this filename will not be set
           if(filename) {
            var alert_image = '/upload_files/'+userdata.userId+'/'+data.alertId+'/'+filename;
           var promise = db.get('alert_images').insert( 
                                               {alert_id: alrId,
                                                image_url: alert_image,
                                                image_seq: idx,
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
         }
        }
       }
      
          var promise = db.get('alert_images').update(
                                              {alert_id: alrId }, 
                                               {$set: {
                                                image_text: userdata.imageText,
                                                video_url: userdata.videoURL,
                                                create_date: new Date()
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       saveAlertActions(req,res,data,actions,userdata,actions);
     else
       processStatus(req,res,6,data);
}

function saveAlertActions(req,res,data,actions,userdata,actions){
    var alert_id = data.alertId;
    var isError = 0;
   
    //user can save multiple times so every time we remove existing actions before save
    //This seems to be the only way to uncheck a box 
    db.get('alert_actions').remove({alert_id: alert_id});
   
    for (jdx =0; jdx < actions.length; jdx++) {
      var curr_action = actions[jdx];
      var promise = db.get('alert_actions').insert( 
                                                   {alert_id: alert_id,
                                                    action_type_id: curr_action,
                                                    create_date: new Date()
                                                   });
       promise.on('success',function(doc){
           //processStatus(req,res,9,data);
         });
       promise.on('error',function(err){
            console.log(err);
            isError = 1;
           //processStatus(req,res,6,data);//system error 
         });
      }
     
    if(isError == 0)
     getAlertData(req,res,9,data);
     //processStatus(req,res,9,data);
    else
     processStatus(req,res,6,data);
}

function getAlertData(req,res,stat,data){
    
    var aid = data.alertId;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    
     async.parallel({
         alrData: function(callback){utils.alertData(aid,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             data.alertData = results.alrData;
             processStatus(req,res,9,data);
            }
         }); 
}

function processStatus(req,res,stat,data) {
  var controllerpath = app.get('controller');
  var controller = require(controllerpath+'Boarding/saveAlert');

  controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

>>>>>>> DEV/master
