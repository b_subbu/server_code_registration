<<<<<<< HEAD
function processRequest(req,res,data){
   if(typeof req.body.alertId === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
     getMasterTypes(req,res,data);
      
      
}


function getMasterTypes(req,res,data){
    
    var async = require('async');
    var mastutils = require('../Registration/masterUtils.js')
    var utils = require('./utils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         conType: function(callback){mastutils.masterTypes('content_type_master',callback); },
         alrType: function(callback){mastutils.masterTypes('alert_type_master',callback); },
         actType: function(callback){mastutils.masterTypes('action_type_master',callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.conType.errorCode != 0) {
             processStatus (req,res,4,data);
             isError = 1;
             return;
           }
          if(isError == 0 && results.alrType.errorCode != 0) {
             processStatus (req,res,3,data);
             isError = 1;
             return;
           }
           if(isError == 0 && results.actType.errorCode != 0) {
             processStatus (req,res,5,data);
             isError = 1;
             return;
           } 
           if (isError == 0) {
             //data.orgId = results.bizStat.orgId;
             //data.businessType = results.bizStat.bizType;
             data.alertTypes = results.alrType.types;
             data.contentTypes = results.conType.types;
             data.availableAnalytics = results.actType.types;
             getAlertData(req,res,9,data);
            }
         }); 
}


function getAlertData(req,res,stat,data){
    
    var aid = req.body.alertId;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    //data.alertId = aid;
    
    
     async.parallel({
         alrData: function(callback){utils.alertEditData(aid,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             data.alertData = results.alrData;
             getLinkedName(req,res,9,data);
             //processStatus(req,res,9,data);
            }
         }); 
}

function getLinkedName(req,res,stat,data){

  debuglog("we will start getting the linked Item names");
  debuglog("note it will be always one item/group per alert");
  if(data.alertData.linkType == 'item' ){
   var tblName = data.orgId+'_menuitem_master'
   var itemCode = data.alertData.linkId;
   var promise= db.get(tblName).find({itemCode: itemCode});
  }
  else if(data.alertData.linkType == 'group'){
   var tblName = data.orgId+'_group_header'
   var groupCode = data.alertData.linkId;
   var promise= db.get(tblName).find({groupCode: groupCode});
  }
  else{
  debuglog("if it is not linked to any entity we will directly render response");
  debuglog(data.alertData);
  processStatus(req,res,9,data);
  }
  if( data.alertData.linkType == 'item' || data.alertData.linkType == 'group'){
  promise.each(function(doc){
    if(data.alertData.linkType == 'item')
     data.linkedTo = doc.itemName;
    if(data.alertData.linkType == 'group')
     data.linkedTo = doc.groupName; 
  });
  promise.on('success',function(doc){
    processStatus(req,res,9,data);
  });
  promise.on('error',function(doc){
    processStatus(req,res,6,data);
  });
  }
  

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
exports.getData = processRequest;



=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined' || typeof req.body.alertId === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMasterStatus(req,res,data);
}


function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var async = require('async');
    var roleutils = require('../Roles/roleutils.js');
    var utils = require('./utils');
    var isError = 0;
    data.userId = uid;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         bizStat: function(callback){roleutils.usrStatus(uid,callback); },
         conType: function(callback){utils.contentTypes(callback); },
         alrType: function(callback){utils.alertTypes(callback); },
         actType: function(callback){utils.actionTypes(callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.bizStat.errorCode != 0) {
             processStatus (req,res,results.bizStat.errorCode,data);
             isError = 1;
             return;
            }
          if(isError == 0 && results.conType.errorCode != 0) {
             processStatus (req,res,results.conType.errorCode,data);
             isError = 1;
             return;
           }
          if(isError == 0 && results.alrType.errorCode != 0) {
             processStatus (req,res,results.alrType.errorCode,data);
             isError = 1;
             return;
           }
           if(isError == 0 && results.actType.errorCode != 0) {
             processStatus (req,res,results.actType.errorCode,data);
             isError = 1;
             return;
           } 
           if( isError == 0 && results.bizStat.userStatus != 9) {
             processStatus (req,res,2,data);
             isError = 1;
             return;
           }
          if (isError == 0) {
             data.orgId = results.bizStat.orgId;
             data.alertTypes = results.alrType.alertTypes;
             data.contentTypes = results.conType.contentTypes;
             data.actionTypes = results.actType.actionTypes;
             getAlertData(req,res,9,data);
            }
         }); 
}


function getAlertData(req,res,stat,data){
    
    var aid = req.body.alertId;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    //data.alertId = aid;
    
    
     async.parallel({
         alrData: function(callback){utils.alertEditData(aid,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             data.alertData = results.alrData;
             processStatus(req,res,9,data);
            }
         }); 
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Boarding/editAlert');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



>>>>>>> DEV/master
