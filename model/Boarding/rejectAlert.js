<<<<<<< HEAD
function processRequest(req,res,data){
    if(typeof req.body.alertIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkAlertStatus(req,res,data);
}


function checkAlertStatus(req,res,data){
    
    var alrArr = req.body.alertIds;
 
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    data.alertIds = alrArr;
     
    //call all functions async in parallel
    //but proceed only after completion and checks
   
   async.forEachOf(alrArr,function(value,key,callback1){
     async.parallel({
         alrStat: function (callback){utils.alertStatus(value,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.alrStat.errorCode != 0) {
             processStatus (req,res,results.alrStat.errorCode,data);
             isError = 1;
             return;
           }
          if (isError == 0) {
             callback1();
           }
         });
       }, 
    function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
                }
          else {
           changeAlertStatus(req,res,9,data); 
         }
     }); 
}


function changeAlertStatus(req,res,stat,data){
    
    var alrArr = data.alertIds;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    var reason = ''; //req.body.rejectReason; not done for now 
    
    async.forEachOf(alrArr,function(value,key,callback1){
      async.parallel({
            chngStatus: function(callback){utils.alertStatusChange(value,data.userId,87,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
              callback1();
            }
          });
         },
        function(err){
          if(err) {
                console.log(err);
                processStatus(req,res,6,data);
                }
          else  {
                getAlertData(req,res,9,data);
               }
         }); 
}

function getAlertData(req,res,stat,data){
    
    var alrArr = data.alertIds;
    var resultArr = [];
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
   async.forEachOf(alrArr,function(value,key,callback1){
     async.parallel({
          alrData: function(callback){utils.alertData(value,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             resultArr.push(results.alrData);
             callback1();
           }
         });
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
        else {
            data.alertData = resultArr;
            processStatus(req,res,9,data);
           }
     }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined' || typeof req.body.alertIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMasterStatus(req,res,data);
}


function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var alrArr = req.body.alertIds;
 
    var async = require('async');
    var roleutils = require('../Roles/roleutils.js');
    var utils = require('./utils');
    var isError = 0;
    data.userId = uid;
    data.alertIds = alrArr;
     
    //call all functions async in parallel
    //but proceed only after completion and checks
   
   async.forEachOf(alrArr,function(value,key,callback1){
     async.parallel({
         bizStat: function(callback){roleutils.usrStatus(uid,callback); },
         alrStat: function (callback){utils.alertStatus(value,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.bizStat.errorCode != 0) {
             processStatus (req,res,results.bizStat.errorCode,data);
             isError = 1;
             return;
            }
          if(isError == 0 && results.alrStat.errorCode != 0) {
             processStatus (req,res,results.alrStat.errorCode,data);
             isError = 1;
             return;
           }
          if( isError == 0 && results.bizStat.userStatus != 9) {
             processStatus (req,res,2,data);
             isError = 1;
             return;
           }
          if (isError == 0) {
              data.orgId = results.bizStat.orgId;
              callback1();
           }
         });
       }, 
    function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
                }
          else {
           changeAlertStatus(req,res,9,data); 
         }
     }); 
}


function changeAlertStatus(req,res,stat,data){
    
    var alrArr = data.alertIds;
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
    var reason = ''; //req.body.rejectReason; not done for now 
    
    async.forEachOf(alrArr,function(value,key,callback1){
      async.parallel({
            chngStatus: function(callback){utils.alertStatusChange(value,87,reason,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
              callback1();
            }
          });
         },
        function(err){
          if(err) {
                console.log(err);
                processStatus(req,res,6,data);
                }
          else  {
                getAlertData(req,res,9,data);
               }
         }); 
}

function getAlertData(req,res,stat,data){
    
    var alrArr = data.alertIds;
    var resultArr = [];
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    
   async.forEachOf(alrArr,function(value,key,callback1){
     async.parallel({
          alrData: function(callback){utils.alertData(value,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             resultArr.push(results.alrData);
             callback1();
           }
         });
        },
         function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
        else {
            data.alertData = resultArr;
            processStatus(req,res,9,data);
           }
     }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Boarding/rejectAlert');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




>>>>>>> DEV/master
