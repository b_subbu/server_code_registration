<<<<<<< HEAD
function processRequest(req,res,data){
       getMasterTypes(req,res,data);
       
}


function getMasterTypes(req,res,data){
    
    var async = require('async');
    var mastutils = require('../Registration/masterUtils.js')
    var utils = require('./utils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         conType: function(callback){mastutils.masterTypes('content_type_master',callback); },
         alrType: function(callback){mastutils.masterTypes('alert_type_master',callback); },
         actType: function(callback){mastutils.masterTypes('action_type_master',callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.conType.errorCode != 0) {
             processStatus (req,res,4,data);
             isError = 1;
             return;
           }
          if(isError == 0 && results.alrType.errorCode != 0) {
             processStatus (req,res,3,data);
             isError = 1;
             return;
           }
           if(isError == 0 && results.actType.errorCode != 0) {
             processStatus (req,res,5,data);
             isError = 1;
             return;
           } 
          if (isError == 0) {
             //data.orgId = results.bizStat.orgId;
             //data.businessType = results.bizStat.bizType;
             data.alertTypes = results.alrType.types;
             data.contentTypes = results.conType.types;
             data.availableAnalytics = results.actType.types;
             processStatus(req,res,9,data);
            }
         }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;


=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkMasterStatus(req,res,data);
}


function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var async = require('async');
    var roleutils = require('../Roles/roleutils.js');
    var utils = require('./utils');
    var isError = 0;
    data.userId = uid;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         bizStat: function(callback){roleutils.usrStatus(uid,callback); },
         conType: function(callback){utils.contentTypes(callback); },
         alrType: function(callback){utils.alertTypes(callback); },
         actType: function(callback){utils.actionTypes(callback); } 
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           if(isError == 0 && results.bizStat.errorCode != 0) {
             processStatus (req,res,results.bizStat.errorCode,data);
             isError = 1;
             return;
            }
          if(isError == 0 && results.conType.errorCode != 0) {
             processStatus (req,res,results.conType.errorCode,data);
             isError = 1;
             return;
           }
          if(isError == 0 && results.alrType.errorCode != 0) {
             processStatus (req,res,results.alrType.errorCode,data);
             isError = 1;
             return;
           }
           if(isError == 0 && results.actType.errorCode != 0) {
             processStatus (req,res,results.actType.errorCode,data);
             isError = 1;
             return;
           } 
           if( isError == 0 && results.bizStat.userStatus != 9) {
             processStatus (req,res,2,data);
             isError = 1;
             return;
           }
          if (isError == 0) {
             data.orgId = results.bizStat.orgId;
             data.alertTypes = results.alrType.alertTypes;
             data.contentTypes = results.conType.contentTypes;
             data.actionTypes = results.actType.actionTypes;
             processStatus(req,res,9,data);
            }
         }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Boarding/createAlert');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;


>>>>>>> DEV/master
