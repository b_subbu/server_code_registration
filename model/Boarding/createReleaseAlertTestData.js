function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
      checkMasterStatus(req,res,data);
}


function checkMasterStatus(req,res,data){
  var alertID = ['BusC000300002','BusC000300003','AruC000350014','JusC000370002'];
  data.alertIds = alertID;
  getAlertData(req,res,9,data);
 }

 function getAlertData(req,res,stat,data){
    
   var alrArr = data.alertIds;
   var resultArr = [];
   var async = require('async');
   var utils = require('./utils');
   var isError = 0;
    
    async.forEachOf(alrArr,function(value,key,callback1){
      async.parallel({
          alrData: function(callback){utils.alertData(value,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
             resultArr.push(results.alrData);
             callback1();
              }
         }); 
      },
      function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else  {
            data.alertData = resultArr;
            createReleaseData(req,res,9,data);
            //processStatus(req,res,9,data);
           }
      });
}


function createReleaseData(req,res,stat,data){
    
   var alrArr = data.alertIds;
   var async = require('async');
   var utils = require('./releaseUtils');
   var isError = 0;
   var alrDataArr = data.alertData;
   //var orgId = data.orgId;
   var orgId = 'JusC00037';
   
    
    async.forEachOf(alrArr,function(value1,key1,callback1){
     async.forEachOf(alrDataArr,function(value2,key2,callback2){
        if (alrDataArr[key2].id == value1)
            var dataArr = alrDataArr[key2];
           if(typeof dataArr != 'undefined'){
      async.parallel({
          alrData: function(callback){utils.releaseData(value1,dataArr,orgId,callback); },
         },
         function(err,results) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
          if (isError == 0) {
              callback1(null);//actually this is not required but there is no break in node.js it seems
              }
          
         }); 
         }
      },
    function(err) {
           if(err) {
             console.log(err);
             processStatus(req,res,err,data);
             isError = 1;
             return;
            }
           if (isError == 0) {
              callback1(null);
              }
         }); 
      },
      function(err){
         if(err) {
              console.log(err);
              processStatus(req,res,6,data);
             }
      else  {
             processStatus(req,res,9,data);
           }
      });
}


function processStatus(req,res,stat,data) {
    
console.log("success");
console.log(stat);
console.log(data);	
}
   
exports.getData = processRequest;