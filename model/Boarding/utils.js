<<<<<<< HEAD
function checkAlertStatus(alrId,callback){
   var totalRows =0;
   var data = {};
   
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(docuser){
        totalRows++;
        data.alertStatus = docuser.alert_status_id;
        data.contentType = docuser.content_type_id;
        data.alertType = docuser.alert_type_id;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 3; //alert not found
       callback(null,data);
       }
     else if(totalRows > 1) {
        data.errorCode = 6;//system error  
        callback(null,data)
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data); 
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(null,data);
     });
} 

function getAlertSequence(business_name,callback){
var totalRows = 0;
var promise = db.get('alert_sequence').find({bus_name:business_name},{stream: true});
  promise.each(function(doc){
    totalRows++;
    currseqnum = doc.running_sequence;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
      db.get('alert_sequence').insert({ 
        bus_name: business_name,
        running_sequence: '0001'
      });
      alert_seq = business_name+'0001';
      callback(null,alert_seq);
      }
    else if(totalRows > 1)
      callback(6,null);
    else {
      var next_sequence = parseInt(currseqnum);
      next_sequence++;
      next_seq_str = String("000000"+next_sequence).slice(-4);
      var promise = db.get('alert_sequence').update({ bus_name:business_name},
      { $set: { running_sequence: next_seq_str }});
      alert_seq = business_name+next_seq_str;
      callback(null,alert_seq);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);
  });
}

function getAlertData(alrId,callback){
   var totalRows =0;
   var data = {};
   var moment = require('moment');
   //all are returned as ids, if required get names and pass
   
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data = {id:alrId,conTitle:doc.title,date:moment(doc.create_time).format('L'),alertType:doc.alert_type_id,target:doc.content_target,views:"0"};
       
        data.status = doc.alert_status_id;
        });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(3,data); //alert not found
       }
     else if(totalRows > 1) {
        callback(6,data); //system error
       }
      else {
         getAlertTypeName(alrId,data,callback); 
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 


function getAlertEditData(alrId,callback){
   var totalRows =0;
   var data = {};
   var moment = require('moment');
   //all are returned as ids, if required get names and pass
   
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data = {alertId:alrId,alertType:doc.alert_type_id,contentType:doc.content_type_id,contentTitle:doc.title};
        data.contentDescription= doc.descr;
        data.contentTarget = doc.content_target;
        data.alertLife = doc.alert_life;
        if(doc.alert_type_id == 11){
         debuglog("This is shop alert");
         debuglog("So we will send linkType info");
         debuglog(doc.alert_type_id);
         data.linkType  = doc.link_type_id;
         data.linkId    = doc.link_id;
        }
        else{
         debuglog("This is Learn Alert");
         debuglog("Note as now only 2 types Shop and Learn");
         debuglog("If new types come need to handle here");
         debuglog(doc.alert_type_id);
         data.linkContent = doc.link_content;
         data.uniqueToken = doc.unique_token;
        }
        });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(3,data); //alert not found
       }
     else if(totalRows > 1) {
        callback(6,data); //system error
       }
      else {
         getAlertImage(alrId,data,callback)
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 

function getAlertTypeName(alrId,data,callback){

   var totalRows =0;
   var alrType = data.alertType;
   var mastutils = require('../Registration/masterUtils');
  
    mastutils.masterNames('alert_type_master',alrType,function(err,result){
      if(err) callback(6,null);
      else
        data.alertType = result.typeName;
        getAlertStatusName(alrId,data,callback)
       
        });
}

function getAlertStatusName(alrId,data,callback){

   var totalRows =0;
   var mastutils = require('../Registration/masterUtils');
   var tblName = 'alert_status_master'

    var statid = data.status;
    debuglog(statid);    
   mastutils.masterStatusNames(tblName,statid,function(err,result) {
      if(err) callback(6,null);
       else {
              data.status= result.statusName;
              callback(null,data); 
            }
        });
}

function getAlertImage(alrId,data,callback){

 
   var promise = db.get('alert_images').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
   //AS of now it is only 1 image per alert 
   //if multi images comes change here
        data.imageURL = global.nodeURL+doc.image_url;
        data.imageText = doc.image_text;
        data.videoURL = doc.video_url;
    
        }); 
   promise.on('complete',function(err,doc){
       //no need to check rows since images may or may not be there
         getAlertActions(alrId,data,callback) 
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 

function getAlertActions(alrId,data,callback){
   var totalRows =0;
   var resultArray = [];
   
   var promise = db.get('alert_actions').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
   totalRows++;
   
     //var tmpArray = [doc.action_type_id];
     resultArray.push(doc.action_type_id);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       //not error just no actions
       callback(null,data);
       }
      else {
         data.enabledAnalytics = resultArray;
         callback(null,data);
          
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
} 


function changeAlertStatus(alrId,operatorId,stat,reason,callback){

 var isError = 0;
 var promise = db.get('alert_master').update(
                                              {alert_id: alrId }, 
                                               {$set: {
                                                alert_status_id: stat,
                                                update_time: new Date()
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       createStatusChngLog(alrId,operatorId,stat,reason,callback);
     else
       callback(6,null); //system Error  
 } 
 
 function createStatusChngLog(alrId,operatorId,stat,reason,callback){
 
  var promise = db.get('alert_status').insert({
    alert_id: alrId,
    alert_status_id: stat,
    operatorId: operatorId,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }

function getAlertContentCount(oid,pid,tid,pgid,size,sarr,callback){
   var totCount = 0;
   var data = {};
  
    //var promise = db.get('alert_master').count({$and:[{org_id:oid},{alert_status_id: {$in: slit}}]});
     var promise = db.get('alert_master').count({$and:sarr});
    
    promise.on('complete',function(err,doc){
      totCount = doc;
      if(totCount == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = 0;
       callback(null,data);
       }
      else {
         getAlertContentData(oid,pid,tid,pgid,size,sarr,totCount,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

//if this file grows too big then seperate out one file per Product
//or product+tab
function getAlertContentData(oid,pid,tid,pgid,size,sarr,totalCount,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
   var promise = db.get('alert_master').find({$and:sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
        if(tid == 501)
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_date).format('DD-MM-YYYY'),alertType:doc.alert_type_id,status:doc.alert_status_id,target:doc.content_target,views:"0"}
        if(tid == 502)
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_time).format('DD-MM-YYYY'),alertType:doc.alert_type_id,status:doc.alert_status_id}
        
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
        //date probably should check with alert_status table to get date of the given status
        //Views will come only after we complete APP so keep it zero until 
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = totalCount;
       data.resultArray = resultArray;
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.resultCount = totalCount;
         getAlertTypeData(data,resultArray,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function getAlertTypeData(data,resultArray,callback){
   var totalRows =0;
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    
  
  async.forEachOf(resultArray,function(value,key,callback1){
  var alert_typeid = resultArray[key].alertType;
    mastutils.masterNames('alert_type_master',alert_typeid,function(err,result){
      if(err) callback(6,null);
      else
        resultArray[key].alertType = result.typeName;
        callback1();
    
        });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           data.resultArray = resultArray;
           getAlertStatData(data,resultArray,callback);
          }
     });
     
} 


function getAlertStatData(data,resultArray,callback){
    var totalRows =0;
    var async = require('async');
    var tblName = 'alert_status_master';
    var mastutils = require('../Registration/masterUtils');
  
  
  async.forEachOf(resultArray,function(value,key,callback1){
   var statid = resultArray[key].status;
   
   mastutils.masterStatusNames(tblName,statid,function(err,result) {
           if(err) callback1();
         else {
                resultArray[key].status = result.statusName;
                callback1(); 
              }
        });
      },   
       function(err){
      if(err)
             callback(1,null);
      else
          {
           debuglog("Final Array is:");
           debuglog(resultArray);
           data.resultArray = resultArray;
           callback(null,data);
           }
     });
} 




exports.alertStatus = checkAlertStatus;
exports.alertSequence = getAlertSequence;
exports.alertData = getAlertData;
exports.alertEditData = getAlertEditData;
exports.alertStatusChange = changeAlertStatus;
exports.alertCount = getAlertContentCount;

=======
function getAlertTypes(callback) {      
     var alertTypes = [];
     var data = {};
    
     var totalRows = 0;
     var promise = db.get('alert_type_master').find({});
 
    promise.each(function(doc){
        totalRows++;
        var temparray = {id:doc.alert_type_id,name:doc.alert_type};
        alertTypes.push(temparray);
    }); 
    promise.on('complete',function(err,doc){
       if(totalRows == 0){
            data.errorCode = 3; //Data error--Alert types
            callback(null,data);
       }
      else {
         data.alertTypes = alertTypes;
         data.errorCode = 0;
         callback(null,data);
      }
   });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 3; //Data error--Alert types
       callback(null,data);
   });
    
}

function getContentTypes(callback) {      
     var contTypes = [];
     var data = {};
    
     var totalRows = 0;
     var promise = db.get('content_type_master').find({});
 
    promise.each(function(doc){
        totalRows++;
        var temparray = {id:doc.content_type_id,name:doc.content_type};
        contTypes.push(temparray);
    }); 
    promise.on('complete',function(err,doc){
       if(totalRows == 0){
            data.errorCode = 4; //Data error--Content Types
            callback(null,data);
      }
      else {
         data.contentTypes = contTypes;
         data.errorCode = 0;
         callback(null,data);
      }
   });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 4; //Data error--Content Types
       callback(null,data);
   });
}

function getActionTypes(callback) {      
     var actTypes = [];
     var data = {};
    
     var totalRows = 0;
     var promise = db.get('action_type_master').find({});
 
    promise.each(function(doc){
        totalRows++;
        var temparray = {id:doc.action_type_id,name:doc.action_type};
        actTypes.push(temparray);
    }); 
    promise.on('complete',function(err,doc){
       if(totalRows == 0){
            data.errorCode = 5; //Data error--Action Types
            callback(null,data);
      }
      else {
         data.actionTypes = actTypes;
         data.errorCode = 0;
         callback(null,data);
      }
    });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 5; //Data error--Action Types
       callback(null,data);
    });
}

function checkAlertStatus(alrId,callback){
   var totalRows =0;
   var data = {};
   
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(docuser){
        totalRows++;
        data.alertStatus = docuser.alert_status_id;
        data.contentType = docuser.content_type_id;
        data.alertType = docuser.alert_type_id;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 3; //alert not found
       callback(null,data);
       }
     else if(totalRows > 1) {
        data.errorCode = 6;//system error  
        callback(null,data)
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data); 
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(null,data);
     });
} 

function getAlertSequence(business_name,callback){
var totalRows = 0;
var promise = db.get('alert_sequence').find({bus_name:business_name},{stream: true});
  promise.each(function(doc){
    totalRows++;
    currseqnum = doc.running_sequence;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
      db.get('alert_sequence').insert({ 
        bus_name: business_name,
        running_sequence: '0001'
      });
      alert_seq = business_name+'0001';
      callback(null,alert_seq);
      }
    else if(totalRows > 1)
      callback(1,null);
    else {
      var next_sequence = parseInt(currseqnum);
      next_sequence++;
      next_seq_str = String("000000"+next_sequence).slice(-4);
      var promise = db.get('alert_sequence').update({ bus_name:business_name},
      { $set: { running_sequence: next_seq_str }});
      alert_seq = business_name+next_seq_str;
      callback(null,alert_seq);
    }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(null,alert_seq);
  });
}

function getAlertData(alrId,callback){
   var totalRows =0;
   var data = {};
   var moment = require('moment');
   //all are returned as ids, if required get names and pass
   
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data = {id:alrId,conTitle:doc.title,date:moment(doc.create_time).format('L'),alertType:doc.alert_type_id,target:doc.content_target,views:"0"};
       
        data.status = doc.alert_status_id;
        });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(3,data); //alert not found
       }
     else if(totalRows > 1) {
        callback(6,data); //system error
       }
      else {
         //getAlertImage(alrId,data,callback)
         getAlertTypeName(alrId,data,callback); 
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 


function getAlertEditData(alrId,callback){
   var totalRows =0;
   var data = {};
   var moment = require('moment');
   //all are returned as ids, if required get names and pass
   
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data = {alertId:alrId,alertType:doc.alert_type_id,contentType:doc.content_type_id,contentTitle:doc.title};
        data.contentDescription= doc.descr;
        data.contentTarget = doc.content_target;
        data.alertLife = doc.alert_life;
        });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(3,data); //alert not found
       }
     else if(totalRows > 1) {
        callback(6,data); //system error
       }
      else {
         getAlertImage(alrId,data,callback)
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 

function getAlertTypeName(alrId,data,callback){

   var totalRows =0;
   var alrType = data.alertType;
   
   var promise = db.get('alert_type_master').find({alert_type_id:alrType},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data.alertType = doc.alert_type;
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(6,data); 
       }
     else if(totalRows > 1) {
        callback(6,data); //system error
       }
      else {
         getAlertStatusName(alrId,data,callback)
         //callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });




}

function getAlertStatusName(alrId,data,callback){

   var totalRows =0;
   var alrType = data.status;
   
   var promise = db.get('alert_status_master').find({alert_status_id:alrType},{stream: true});
   promise.each(function(doc){
        totalRows++;
        data.status = doc.alert_status;
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(6,data); 
       }
     else if(totalRows > 1) {
        callback(6,data); //system error
       }
      else {
         //getAlertStatusName(alrId,data,callback)
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });




}

function getAlertImage(alrId,data,callback){
var baseURL= "http://ec2-52-24-99-39.us-west-2.compute.amazonaws.com:3000";
 
   var promise = db.get('alert_images').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
   //AS of now it is only 1 image per alert 
   //if multi images comes change here
        data.imageURL = baseURL+doc.image_url;
        data.imageText = doc.image_text;
        data.videoURL = doc.video_url;
    
        }); 
   promise.on('complete',function(err,doc){
       //no need to check rows since images may or may not be there
         getAlertActions(alrId,data,callback) 
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
} 

function getAlertActions(alrId,data,callback){
   var totalRows =0;
   var tmpArray = {};
   var resultArray = [];
   
   var promise = db.get('alert_actions').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
   totalRows++;
   
     var tmpArray = {id:doc.action_type_id};
     resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       //not error just no actions
       callback(null,data);
       }
      else {
         data.alertActions = resultArray;
         callback(null,data);
          
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
} 


function changeAlertStatus(alrId,stat,reason,callback){

 var isError = 0;
 var promise = db.get('alert_master').update(
                                              {alert_id: alrId }, 
                                               {$set: {
                                                alert_status_id: stat,
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       createStatusChngLog(alrId,stat,reason,callback);
     else
       callback(6,null); //system Error  
 } 
 
 function createStatusChngLog(alrId,stat,reason,callback){
 
  var promise = db.get('alert_status').insert({
    alert_id: alrId,
    alert_status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }

exports.alertTypes = getAlertTypes;
exports.contentTypes = getContentTypes;
exports.actionTypes = getActionTypes;
exports.alertStatus = checkAlertStatus;
exports.alertSequence = getAlertSequence;
exports.alertData = getAlertData;
exports.alertEditData = getAlertEditData;
exports.alertStatusChange = changeAlertStatus;
>>>>>>> DEV/master
