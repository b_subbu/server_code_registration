function getAlertsData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
   
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   data.pageId = pageId;
      async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'alert_status_master',callback); }
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList);
                  }
                  else{ 
                    var searchArr = [];
                      var tmpArray = {status_id: {"$in":statList}};
                      searchArr.push(tmpArray);
                      debuglog(searchArr);
                    getAlertsCount(req,res,stat,data,searchArr);
                   }
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList) {
   var async = require('async');
   var filterutils = require('../Roles/filterUtils');
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    if(tid == 501)
     var tabl_name = 'alert_master';
    else
     var tabl_name = 'alert_master';
     //TODO seperate Revision and main Table and add Org_id as per other Modules
     
 
    var filterOptions = {};
    
    var headerFields = data.productHeader;
    debuglog(headerFields);
     async.forEachOf(headerFields,function(key,value,callback){
     debuglog("Starting processing of header fields");
     debuglog("Note we are calling Roles filter utils because here there are 2 filters");
     debuglog("Alert types and alert status");
     debuglog("TODO make Roles filter utils as generic and call it from everywhere");
     debuglog(key);
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
               filterOptions[key_name] = results;
               callback();
            }
          }); 
        }
     else
        callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call alerts count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var userArray = {org_id: data.orgId };
           searchArr.push(userArray);
           var tmpArray = {alert_status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
          getAlertsCount(req,res,stat,data,searchArr,tabl_name);
           }
         else
           getAlertSearch(req,res,stat,data,statList,tabl_name);
         }
      });
} 

function getAlertSearch(req,res,stat,data,statList,tabl_name) {

 var searchInp = req.body.searchParams;

  debuglog("Alert type name and status name search is removed") ;
  debuglog("since they are purely filter based search now");
  debuglog("refer model\Search\searchData for reference");
 var searchArr = [];
 var userArray = {org_id: data.orgId };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'conTitle':
       var tmpArray = {title: {$regex:key,$options:'i'}};
         searchArr.push(tmpArray);
         break; 
       case 'alertType':
         var tmpArray = {alert_type_id: key};
         searchArr.push(tmpArray);
         break; 
       case 'status':
        var tmpArray = {alert_status_id: key};
        searchArr.push(tmpArray);
        break;
       case 'date':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var cmpDate = new Date(minDateArr[2],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDateArr[2],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {create_date: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {create_date: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
       break;
       case 'target':
       var tmpArray = {content_target: key};
       searchArr.push(tmpArray);
       break;
       case 'views':
       //Views to do not yet implemented 
          break;
       default:
           processStatus(req,res,6,data);
            break;
          
     
     }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           getAlertsCount(req,res,stat,data,searchArr,tabl_name);
          }
     });
   
 
} 

function getAlertsCount(req,res,stat,data,statList,tablName){
    
    var async = require('async');
    var utils = require('./utils');
    var isError = 0;
    var totalCount = 0;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pid = data.productId;
   var tid = data.tabId;
   
   var size = 20; //20 rows per page
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
       var statA
       
    //TODO split count and get data separately
    //pass table naem as argument once orgId based table name is done
   utils.alertCount(orgId,pid,tid,pageId,size,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data);
              //no Alerts for the business nothing to do further
              }
             else {
              data.pageId = pageId;
              data.recordCount = results.resultCount;
              data.productData = results.resultArray;
             processStatus(req,res,9,data); 
             }
            }
         });
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getProductData = getAlertsData;






