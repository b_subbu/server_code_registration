function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   var userdata = "";
   var files = [];
   var idx = 0;
   var jdx = 0;
   var actions = [];
   var formidable = require('formidable');
   
   var form = new formidable.IncomingForm();
   form.uploadDir = app.get('fileuploadfolder')+'/tmp';
   form.multiples = true;
   form.parse(req, function (err, fields) {
     userdata = fields;
    });
    form.on('file',function(name,file) {
    files[idx] = file;
     idx++;
    });
   form.on('end',function() {
      checkMasterStatus(req,res,data,userdata,actions,files);
      debuglog("checkMasterStatus is left asis since multipart form data");
    });
 } 


function checkMasterStatus(req,res,data,userdata,actions,files){

    var uid = userdata.userId;
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    var type = require('type-detect');
    var unqTok = userdata.uniqueToken;
    
   
    data.userId = uid;
      if( typeof uid === 'undefined' || uid === 'undefined' || type(uid) !== 'string'){
         processStatus(req,res,6,data); //system error
         //exit(0); //does not seem to work in multi-thread mode
         return; 
         }
     if( typeof unqTok === 'undefined' || unqTok === 'undefined'){
         processStatus(req,res,6,data); //system error
         return; 
         }
 
 
 
  async.parallel({bizStat: function(callback){roleutils.usrStatus(uid,callback); },
       },
      function(err,results) {
       debuglog(results);
       if(err) {
         console.log(err);
         processStatus(req,res,6,data);//system error
         isError = 1;
         return;
       }
       if(isError == 0 && results.bizStat.errorCode != 0) {
        processStatus (req,res,results.bizStat.errorCode,data);
        isError = 1;
        return;
       }
       if( isError == 0 && results.bizStat.userStatus != 9) {
        processStatus (req,res,2,data);
        isError = 1;
        return;
      }
      if (isError == 0) {
        data.orgId = results.bizStat.orgId;
        getMaxImgIndex(req,res,9,data,userdata,files);
       }
     });
}


//Will try to get maximum file index using fs 
//i.e., read from directory with name uniqueToken
//using sync because program can't proceed without getting max index
//use async here if any performance issue comes

function getMaxImgIndex(req,res,stat,data,userdata,files){

 debuglog(userdata);
          var imgDir = app.get('fileuploadfolder')+data.userId+'/'+userdata.uniqueToken+'/';
          var fs = require('fs');
          var visualIndexArr = [];
          fs.stat(imgDir,function(err,stats){
          debuglog("read files from directroy");
          debuglog(imgDir);
          debuglog("and result is:");
          debuglog(stats);
          debuglog(err);
          if(err){
            var maxIndex = 0; //directory does not exist 
            saveFiles(req,res,9,data,userdata,files,maxIndex);
           }
          else{ 
          var visualFiles = fs.readdirSync(imgDir);
          debuglog(visualFiles);
          if(visualFiles.length > 0){
          debuglog("already some files are saved under this unique index");
          debuglog(visualFiles.length);
          debuglog("So we will save the current file as the next image");
          debuglog("The last element of the array will have the maximum file number since it is stored in ascending order");
          var lastIndex = visualFiles.length-1;
          var maxIndex = visualFiles[lastIndex];
          debuglog(maxIndex);
          saveFiles(req,res,9,data,userdata,files,maxIndex);
    
           }
      else {
           var maxIndex = 0; 
           debuglog("directory exists but no files there");
           debuglog("probably deleted by script or something")
           saveFiles(req,res,9,data,userdata,files,maxIndex);
         }
      }
    });   
       
          
}
function saveFiles(req,res,stat,data,userdata,files,fileIndex) {
    var newfileIndex = 0;
    var fs = require('fs');
    var mkdirp = require('mkdirp');
    var async = require('async');
    var newfolder = app.get('fileuploadfolder')+userdata.userId+'/'+userdata.uniqueToken+'/';
    mkdirp(newfolder,function(err,made){
    if(err){
    
    debuglog("Error in creating new directory");
    debuglog(err);
    data.imageURL='';
    debuglog(data);
    processStatus(req,res,6,data);
    }
   else{
    debuglog("successfully created new directory");
    debuglog(made); 
    debuglog(newfolder);
    debuglog("now let us start saving the files");
    debuglog(files.length);
    
   async.forEachOf(files,function(key,value,callback) {
      var oldfile = key.path;
      if(key.size == 0) {
        fs.unlink(oldfile,function(err){
          if(err) {
             debuglog("remove empty file failed");
             debuglog(err);
          }
          callback();
         });
       }
      else {
      newfileIndex = ++fileIndex; 
      var newfile = newfolder+newfileIndex;
      debuglog("starting processing of");
      debuglog(value);
      debuglog(key);
      debuglog(newfileIndex);
      debuglog(newfile);
 
        fs.rename(oldfile,newfile,function(err) {
         if(err) {
               debuglog("Moving file failed");
               debuglog(err);
               callback();
           }
        else{
          debuglog("File saved successfully");
          debuglog(newfile);
          debuglog(oldfile);
          callback();
        }
      });
     }
    },
  function(err){
      if(err){
        debuglog("Some error has happened");
        debuglog(err);
        data.imageURL = '';
        debuglog(data);
        processStatus(req,res,6,data);
        }
      else {
           debuglog("Files are saved successfully");
           if(newfileIndex > 0)
            var image_url = global.nodeURL+'/upload_files/'+userdata.userId+'/'+userdata.uniqueToken+'/'+fileIndex;
          else
            var image_url = '';
           debuglog("fn returnImageURL is needed only if we allow multiple file uploads in this fn");
              //returnImageURL(req,res,data,userdata,files);
           debuglog("since we dont allow now let us just return data");
           data.imageURL = image_url;
           debuglog(data);
           processStatus(req,res,9,data);   
          }
       });  
     }
   });
}


function returnImageURL(req,res,data,userdata,files){

    var image_url = "";
    debuglog("Currently only one file upload is allowed in this API");
    debuglog("So we just return a single image url");
    debuglog("If multiple file uploads are enabled latter then use files.length");
    debuglog("and return urls as an array");
    if(files.length > 0) {
      for(idx = 0; idx < files.length;idx++) {
           if(files[idx].size > 0) {
           var filename = files[idx].name;
           var image_url = global.nodeURL+'/upload_files/'+userdata.userId+'/'+encodeURIComponent(filename);
           debuglog("using nodeURL in image url this needs to be replaced by CDN later TODO");
           debuglog(image_url);
           }
           else {
           var image_url = '';
           debuglog("This is empty file so url will be empty");
           }
     } 
      data.imageFile = image_url;
      processStatus(req,res,9,data);
      debuglog("We will return response now with image url");
      debuglog(data);
     } 
     else{
       processStatus(req,res,6,data);
       data.imageFile = null;
       debuglog("Actually this should not happen because file check is done earlier itself");
       debuglog("But still if this happens then we will just return error");
       debuglog(data);
       }

}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;


