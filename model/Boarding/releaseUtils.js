function createReleaseData(alrId,alrArr,orgId,callback){
 
  var promise = db.get('alert_release').insert({
    alert_id: alrId,
    org_id: orgId,
     content_title: alrArr.conTitle,
    release_date: alrArr.date,
    });
  promise.on('success',function(doc){
      getAlertData(alrId,orgId,callback);
    });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }


 //Note there is no INSERT INTO  SELECT * FROM 
//equivalent in mongo
function getAlertData(alrId,orgId,callback){
 var totalRows =0;
 var dataArr = {};
   
   var promise = db.get('alert_master').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        dataArr = {conType:doc.content_type_id,alrType:doc.alert_type_id,descr:doc.descr,updTime:doc.update_time};
        dataArr.linkType  = doc.link_type_id;
        dataArr.linkId    = doc.link_id;
        dataArr.linkContent = doc.link_content;
         });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(6,null); //sys error
       }
      else if (totalRows > 1){
      callback(6,null); //sys error
      }
      else {
        updateAlertData(alrId,orgId,dataArr,callback)
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 


function updateAlertData(alrId,orgId,datArr,callback){
  var promise = db.get('alert_release').update(
      {alert_id: alrId},
      {$set:{
        content_type_id: datArr.conType,
        alert_type_id: datArr.alrType,
        descr: datArr.descr ,
        update_time: datArr.updTime,
        link_type_id: datArr.linkType,
        link_id: datArr.linkId,
        link_content: datArr.linkContent
        }
       });
   promise.on('success',function(err,doc){
        getImageData(alrId,orgId,callback)
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

//Note there is no INSERT INTO  SELECT * FROM 
//equivalent in mongo
function getImageData(alrId,orgId,callback){
 var totalRows =0;
 var dataArr = {};
   
   var promise = db.get('alert_images').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        dataArr = {imageURL:doc.image_url,videoURL:doc.video_url,imageText:doc.image_text};
         });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       getBizType(alrId,orgId,callback); //no images found so just update biz type and return
       }
      else if (totalRows > 1){
      callback(6,null); //currently only one image/video per alert need to change here if required later
      }
      else {
        updateReleaseImage(alrId,orgId,dataArr,callback)
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 


function updateReleaseImage(alrId,orgId,imgArr,callback){
   
   var promise = db.get('alert_release').update(
      {alert_id: alrId},
      {$set:{
       image_url: imgArr.imageURL, 
       video_url: imgArr.videoURL,
       image_text: imgArr.imageText
       }
       });
   promise.on('success',function(err,doc){
        getBizType(alrId,orgId,callback)
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function getBizType(alrId,orgId,callback){
 var totalRows =0;
 var dataArr = {};
   
   var promise = db.get('user_detail').find({org_id:orgId},{stream: true});
   promise.each(function(doc){
        totalRows++;
         dataArr = {bizType:doc.bus_type};
         });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(6,null); //system error
       }
      else {
        updateBizType(alrId,dataArr,callback)
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 


function updateBizType(alrId,bizArr,callback){
    
   var promise = db.get('alert_release').update(
      {alert_id: alrId},
      {$set:{
     bus_type: bizArr.bizType
        }
       });
   promise.on('success',function(err,doc){
       callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function removeReleaseData(alrId,reason,callback){
 var totalRows =0;
 var dataArr = {};
   
   var promise = db.get('alert_release').find({alert_id:alrId},{stream: true});
   promise.each(function(doc){
        totalRows++;
        dataArr = {orgId:doc.org_id,conTitle:doc.content_title,conType:doc.content_type_id,alrType:doc.alert_type_id};
        dataArr.relDate = doc.release_date;
        dataArr.imgURL = doc.image_url;
        dataArr.vidURL = doc.video_url;
        dataArr.imgText = doc.image_text;
        dataArr.bizType = doc.bus_type;
        dataArr.linkType  = doc.link_type_id;
        dataArr.linkId    = doc.link_id;
        dataArr.linkContent = doc.link_content;
         });
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(6,null); //sys error
       }
      else if (totalRows > 1){
      callback(6,null); //sys error
      }
      else {
        insertArcData(alrId,reason,dataArr,callback)
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function insertArcData(alrId,reason,alrArr,callback){
 
  var promise = db.get('alert_release_arc').insert({
    alert_id: alrId,
    org_id: alrArr.orgId,
    content_title: alrArr.conTitle,
    release_date: alrArr.relDate,
    content_type_id: alrArr.conType,
    alert_type_id: alrArr.alrType,
    image_url: alrArr.imgURL,
    video_url: alrArr.vidURL,
    image_text: alrArr.imgText,
    bus_type: alrArr.bizType,
    descr: alrArr.descr,
    recall_date: Date('now'),
    recall_reason: reason,
    link_type_id: alrArr.linkType,
    link_id: alrArr.linkId,
    link_content: alrArr.linkContent
  });
  promise.on('success',function(doc){
      removeAlertData(alrId,callback);
    });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }
 
function removeAlertData(alrId,callback){
    
   var promise = db.get('alert_release').remove(
      {alert_id: alrId});
   promise.on('success',function(err,doc){
       callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

exports.releaseData = createReleaseData;
exports.recallData =  removeReleaseData; 

