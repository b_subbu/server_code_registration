function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.customerId === 'undefined' || typeof req.body.businessId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.businessId;
    var cid = req.body.customerId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');  //TODO move all to /utils folder
    var custutils = require('../Render/utils');
    var isError = 0;
    data.customerId = cid;
    data.businessId = uid;
    var custEmail = "";
    
    
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         custStat: function(callback){custutils.checkCustomerId(cid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,1,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
            if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              custEmail = results.custStat.customerEmail;
              getCartItems(req,res,9,data,custEmail);
            }
          }); 
}

function getCartItems(req,res,stat,data,custEmail) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var extend = require('util')._extend;
  
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var custId = data.customerId;
   var deliveryTblName = data.orgId+'_delivery_option';
   
   var checkData = extend({},data);
   async.parallel({
         billAddress: function(callback){datautils.custDetails('cust_bill_details',custId,callback); },
         shipAddress: function(callback){datautils.custDetails('cust_ship_details',custId,callback); },
         itemDetails: function(callback){datautils.getCart('shopping_cart',checkData,callback);},
         deliveryDetails: function(callback){datautils.getDeliveryOptions(deliveryTblName,checkData,callback);} 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.billAddress.errorCode != 0 && results.billAddress.errorCode != 1)) {
              processStatus (req,res,6,data);
              isError = 1;
              return;
             }
           if(isError == 0 && (results.shipAddress.errorCode !=0 && results.shipAddress.errorCode != 1)) {
              processStatus (req,res,6,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.itemDetails.errorCode !=0 ) {
              processStatus (req,res,4,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.deliveryDetails.errorCode !=0){
             processStatus(req,res,5,data);
             isError = 1;
             return;
          }
            if(isError == 0) {
             if(results.billAddress.errorCode == 1)
               data.billingDetails = [];
             else
               data.billingDetails = results.billAddress.custDetails;
            if(results.shipAddress.errorCode == 1)
               data.shippingDetails = [];
            else
               data.shippingDetails = results.shipAddress.custDetails;
             debuglog(results.billAddress);
             data.deliveryOptions = results.deliveryDetails.optionDetails;
             data.deliveryOption = results.billAddress.custDetails.deliveryOption;
             data.payOption = results.billAddress.custDetails.payOption;
             
             var itemsArray = results.itemDetails.items;
             getItemDetails(req,res,9,data,itemsArray,custEmail);
            }
          });
}



function getItemDetails(req,res,stat,data,itemsArr,custEmail) {

var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(itemsArr,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.itemCode,tablName,function(err,results){
     if(err){
     isError = 1;
     callback();
     }
     else {
       key['itemDetails'] = results;
       var searchCond = {itemDetailCode:key.itemDetailCode};
       var tablNameSelection = data.orgId+'_menuitem_detail'; //TODO-->move to common definitions file
       renderUtils.selectionData(data.orgId,data.businessType,key.itemCode,searchCond,tablNameSelection,function(err,results1){
        if(err){
          isError = 1;
          callback();
         }  
     else {
       key['itemSelection'] = results1;
       callback();
      }
     });
    }
   });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else
            calculateTotals(req,res,9,data,itemsArr,custEmail);
          }
     }); 
} 

//Need to move Tax and deduction calculations to separate utils
//later  TODO
//There will be only one row in itemDetails and itemSelection
//But the function will return in array so use [0]
function calculateTotals(req,res,stat,data,itemsArr,custEmail){
 var processArr = [];
 var actualPrice = 0;
 var async = require('async');
    debuglog(itemsArr);
    
   async.forEachOf(itemsArr,function(key,value,callback){
     debuglog(key);
     var tmpArr = {};
     tmpArr.code = key.itemCode;
     tmpArr.name = key.itemDetails.masterData[0].itemName;
     tmpArr.itemDetailCode = key.itemDetailCode;
     tmpArr.sellerId = key.itemDetails.masterData[0].sellerIds[0];
     var qty = key.qty;
     tmpArr.qty =  qty;
     
     if(key.itemDetails.masterData[0].itemVisual)
      tmpArr.visual = key.itemDetails.masterData[0].itemVisual[0];
     else
      tmpArr.visual = null;
     //Assuming visual array is sorted by sequence
     //else first sort by sequence and then get first item
     var uprice = parseFloat(key.itemSelection.selectionData[0].price);
     
     tmpArr.unitPrice = uprice;
     var price = +(Math.round((qty*uprice) + "e+2")  + "e-2");    
     tmpArr.price = price;
     
     actualPrice += price;
   
     //Round to last 2 digits
     //If this does not work then use toFixed method slightly slower
     //Refer https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
     //To fix rounding problems for decimals
     
     debuglog("Taxes are no more considered from items");
     debuglog("It is from charge_type table now");
    /** 
     if(key.itemDetails.masterData[0].vat)
      var tax1Percent = key.itemDetails.masterData[0].vat;
     else
      var tax1Percent = 0; 
     if(key.itemDetails.masterData[0].serviceTax)
      var tax2Percent = key.itemDetails.masterData[0].serviceTax;
     else
      var tax2Percent = 0; 
    
     tmpArr.tax ={vat:tax1Percent,serviceTax:tax2Percent} 
     var tax1 = +(Math.round(((price*tax1Percent)/100) + "e+2")+"e-2");
     var tax2 = +(Math.round(((price*tax2Percent)/100) + "e+2")+"e-2"); 
     */
     processArr.push(tmpArr);
     
     callback();
     
  },
  function(err){
      if(err)
        callback(6,null);
      else {
             data.cartItems = processArr;
             //calculateGrandTotal(req,res,9,data,custEmail);
             getCharges(req,res,9,data,custEmail,actualPrice);
            }
     }); 


}

function getCharges(req,res,stat,data,custEmail,actualPrice) {

var async = require('async');
var tablName = data.orgId+'_charge_type'; //TODO-->move to common definitions file
var isError = 0;

 var dataUtils = require('./dataUtils'); 
    
    dataUtils.chargeData(tablName,function(err,result){
     if(err){
       processStatus(req,res,6,data);
     }
     else {
        var chargeData = result.chargeData;
        debuglog("After getting result");
        debuglog(result);
        calculateGrandTotal(req,res,stat,data,custEmail,chargeData,actualPrice);
      }
     });
}

function calculateGrandTotal(req,res,stat,data,custEmail,chargeData,actualPrice){
 var processArr = [];
 var async = require('async');
 var totals = {};
      totals.actualPrice = actualPrice;
      totals.totalPrice = actualPrice;
      totals.baseCurrency = "INR"; //TODO get from definitions after MVP
      totals.discount = 0; ////TODO actual discount calculation after MVP
      totals.totalTax = 0;
   debuglog("The actual price before calculations is:");
   debuglog(actualPrice);
      
   async.forEachOf(chargeData,function(key,value,callback){
   debuglog("Charge item:");
   debuglog(key);
   debuglog(value);
   var chargeType = key.chargeType;
   
   if(key.applyRateAt == 2){
   debuglog("This is a charge applicable on Totals");
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var totAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      totals[chargeType] = totAmount;
      totals.totalTax += totAmount;
      totals.totalPrice += totAmount;
      debuglog(totAmount);
      debuglog(totals);
      callback();
     }
    else{
      debuglog("Rate at Percentage");
      var totAmount = +(Math.round(((actualPrice*key.rate)/100) + "e+2")+"e-2");
      totals[chargeType] = totAmount;
      totals.totalTax += totAmount;
      totals.totalPrice += totAmount;
       debuglog(totAmount);
       debuglog(totals);
      callback();
     }
    }
    else{
  
  debuglog("This charge is applicable on item level");
  var tmpTotals = 0;
   async.forEachOf(data.cartItems,function(key1,value1,callback1){
      debuglog("For item");
      debuglog(key1);
      debuglog(value1);
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var totAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      tmpTotals += totAmount;
      key1[chargeType] = totAmount;
      callback1();
     }
    else{
      debuglog("Rate at Percentage");
      var totAmount = +(Math.round(((key1.price*key.rate)/100) + "e+2")+"e-2");
      tmpTotals += totAmount;
      key1[chargeType] = totAmount;
      callback1();
     }
    },
  function(err){
      if(err)
        callback(6,null);
      else {
      totals[chargeType] = tmpTotals;
      totals.totalTax += tmpTotals;
      totals.totalPrice += tmpTotals;
      debuglog(tmpTotals);
      debuglog(totals);
     
      callback();
     }
    });
   }
   },
   function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
        data.totals = totals;
            checkSuperUser(req,res,9,data,custEmail);
       }
    }); 

    
 }
    
   
function checkSuperUser(req,res,stat,data,custEmail){
debuglog("Currently checking whether cust email is in business detail table");
debuglog("If yes then considered as super user else normal user");
debuglog("TODO add role check later");
debuglog(custEmail);

var datautils = require('./dataUtils');

datautils.isSuperUser(custEmail,function(err,result){
         if(err)
            processStatus(req,res,6,data);
         else{
          if(result == 'super'){
            var tblName = data.orgId+'_payment_gateway';
            datautils.PGReleaseData(tblName,function(err,result){
              if(err)
                processStatus(req,res,6,data);
              else{
              debuglog("No Error");
              debuglog(result);
              data.paymentGateways = result;
              processStatus(req,res,9,data);
              }
            });
          }
          else
            data.paymentGateways = [];
            processStatus(req,res,9,data);
          }
      });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Cart/getItemsInCart');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;



