function processRequest(req,res,data){
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus left asis for getting customer email")
}

function checkMasterStatus(req,res,data){
    
    var cid = data.customerId;
    var async = require('async');
    var custutils = require('../Render/utils');
    var isError = 0;
    var custEmail = "";
    
    
    
     async.parallel({
         custStat: function(callback){custutils.checkCustomerId(cid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
         if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
            if(isError == 0) {
              custEmail = results.custStat.customerEmail;
              getCartItems(req,res,9,data,custEmail);
            }
          }); 
}

function getCartItems(req,res,stat,data,custEmail) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var sellerutils = require('../Sellers/dataUtils');
   var type = require('type-detect');
   var extend = require('util')._extend;
  
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var custId = data.customerId;
   var deliveryTblName = data.orgId+'_delivery_option';
   var sellerTblName   = data.orgId+'_sellers';
   
   var sellerData = {};
       sellerData.sellerId = 1;
       debuglog("For now will get details of root seller only");
   
   var checkData = extend({},data);
   async.parallel({
         billAddress: function(callback){datautils.custDetails('cust_bill_details',custId,callback); },
         shipAddress: function(callback){datautils.custDetails('cust_ship_details',custId,callback); },
         itemDetails: function(callback){datautils.getCart('shopping_cart',checkData,callback);},
         deliveryDetails: function(callback){datautils.getDeliveryOptions(deliveryTblName,checkData,callback);},
         sellerDetails: function(callback){sellerutils.SellerData(sellerTblName,sellerData,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.billAddress.errorCode != 0 && results.billAddress.errorCode != 1)) {
              processStatus (req,res,6,data);
              isError = 1;
              return;
             }
           if(isError == 0 && (results.shipAddress.errorCode !=0 && results.shipAddress.errorCode != 1)) {
              processStatus (req,res,6,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.itemDetails.errorCode !=0 ) {
              processStatus (req,res,4,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.deliveryDetails.errorCode !=0){
             processStatus(req,res,5,data);
             isError = 1;
             return;
          }
            if(isError == 0) {
             if(results.billAddress.errorCode == 1)
               data.billingDetails = [];
             else
               data.billingDetails = results.billAddress.custDetails;
            if(results.shipAddress.errorCode == 1)
               data.shippingDetails = [];
            else
               data.shippingDetails = results.shipAddress.custDetails;
             debuglog(results.deliveryDetails);
             debuglog("Delivery and cust details");
             debuglog(results.billAddress);
             data.deliveryOptions = results.deliveryDetails.optionDetails;
             data.deliveryOption = results.billAddress.custDetails.deliveryOption;
             data.payOption = results.billAddress.custDetails.payOption;
             debuglog("No need to check for result code of sellers since it is root seller for now");
             debuglog(results.sellerDetails);
             
             debuglog("The below is to match params of billing and shipping details");
             var sellerDetails = {};
                 sellerDetails.firstName = results.sellerDetails.businessDetails.displayName;
                 sellerDetails.lastName  = results.sellerDetails.businessDetails.registeredName;
                 sellerDetails.address   = results.sellerDetails.businessDetails.building+' '+results.sellerDetails.businessDetails.street;
                 sellerDetails.city =      results.sellerDetails.businessDetails.city.name;
                 sellerDetails.state =     results.sellerDetails.businessDetails.state.name;
                 sellerDetails.country =   results.sellerDetails.businessDetails.country.countryName;
                 sellerDetails.zipCode =   results.sellerDetails.businessDetails.zipCode;
                 sellerDetails.phone   =   results.sellerDetails.businessDetails.sellerMobile;
             
             var itemsArray = results.itemDetails.items;
             getItemDetails(req,res,9,data,itemsArray,custEmail,sellerDetails);
            }
          });
}



function getItemDetails(req,res,stat,data,itemsArr,custEmail,sellerDetails) {

var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(itemsArr,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.itemCode,tablName,function(err,results){
     if(err){
     isError = 1;
     callback();
     }
     else {
       key['itemDetails'] = results;
       var searchCond = {itemDetailCode:key.itemDetailCode};
       var tablNameSelection = data.orgId+'_menuitem_detail'; //TODO-->move to common definitions file
       renderUtils.selectionData(data.orgId,data.businessType,key.itemCode,searchCond,tablNameSelection,function(err,results1){
        if(err){
          isError = 1;
          callback();
         }  
     else {
       key['itemSelection'] = results1;
       callback();
      }
     });
    }
   });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else
            calculateTotals(req,res,9,data,itemsArr,custEmail,sellerDetails);
          }
     }); 
} 

//Need to move Tax and deduction calculations to separate utils
//later  TODO
//There will be only one row in itemDetails and itemSelection
//But the function will return in array so use [0]
function calculateTotals(req,res,stat,data,itemsArr,custEmail,sellerDetails){
 var processArr = [];
 var actualPrice = 0;
 var async = require('async');
    debuglog(itemsArr);
    
   async.forEachOf(itemsArr,function(key,value,callback){
     debuglog(key);
     var tmpArr = {};
     tmpArr.code = key.itemCode;
     tmpArr.name = key.itemDetails.masterData[0].itemName;
     tmpArr.itemDetailCode = key.itemDetailCode;
     tmpArr.sellerId = key.itemDetails.masterData[0].sellerIds[0];
     var qty = key.qty;
     tmpArr.qty =  qty;
     
     if(key.itemDetails.masterData[0].itemVisual)
      tmpArr.visual = key.itemDetails.masterData[0].itemVisual[0];
     else
      tmpArr.visual = null;
     //Assuming visual array is sorted by sequence
     //else first sort by sequence and then get first item
     var uprice = parseFloat(key.itemSelection.selectionData[0].price);
     
     tmpArr.unitPrice = uprice;
     var price = +(Math.round((qty*uprice) + "e+2")  + "e-2");    
     tmpArr.price = price;
     
     actualPrice += price;
   
     //Round to last 2 digits
     //If this does not work then use toFixed method slightly slower
     //Refer https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
     //To fix rounding problems for decimals
     
     debuglog("Taxes are no more considered from items");
     debuglog("It is from charge_type table now");
     processArr.push(tmpArr);
     
     callback();
     
  },
  function(err){
      if(err)
        callback(6,null);
      else {
             data.cartItems = processArr;
             getCharges(req,res,9,data,custEmail,actualPrice,sellerDetails);
            }
     }); 


}

function getCharges(req,res,stat,data,custEmail,actualPrice,sellerDetails) {

var async = require('async');
var tablName = data.orgId+'_charge_type'; //TODO-->move to common definitions file
var isError = 0;

 var dataUtils = require('./dataUtils'); 
    
    dataUtils.chargeData(tablName,function(err,result){
     if(err){
       processStatus(req,res,6,data);
     }
     else {
        var chargeData = result.chargeData;
        debuglog("After getting result");
        debuglog(result);
        calculateGrandTotal(req,res,stat,data,custEmail,chargeData,actualPrice,sellerDetails);
      }
     });
}

function calculateGrandTotal(req,res,stat,data,custEmail,chargeData,actualPrice,sellerDetails){
 var processArr = [];
 var async = require('async');
 var totals = {};
      totals.actualPrice = actualPrice;
      totals.totalPrice = actualPrice;
      totals.baseCurrency = "INR"; //TODO get from definitions after MVP
      totals.discount = 0; ////TODO actual discount calculation after MVP
      totals.totalTax = 0;
   debuglog("The actual price before calculations is:");
   debuglog(actualPrice);
      
   async.forEachOf(chargeData,function(key,value,callback){
   debuglog("Charge item:");
   debuglog(key);
   debuglog(value);
   var chargeType = key.chargeType;
   
   if(key.applyRateAt == 2){
   debuglog("This is a charge applicable on Totals");
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var totAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      totals[chargeType] = totAmount;
      totals.totalTax += totAmount;
      totals.totalPrice += totAmount;
      debuglog(totAmount);
      debuglog(totals);
      callback();
     }
    else{
      debuglog("Rate at Percentage");
      var totAmount = +(Math.round(((actualPrice*key.rate)/100) + "e+2")+"e-2");
      totals[chargeType] = totAmount;
      totals.totalTax += totAmount;
      totals.totalPrice += totAmount;
       debuglog(totAmount);
       debuglog(totals);
      callback();
     }
    }
    else{
  
  debuglog("This charge is applicable on item level");
  var tmpTotals = 0;
   async.forEachOf(data.cartItems,function(key1,value1,callback1){
      debuglog("For item");
      debuglog(key1);
      debuglog(value1);
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var totAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      tmpTotals += totAmount;
      key1[chargeType] = totAmount;
      callback1();
     }
    else{
      debuglog("Rate at Percentage");
      var totAmount = +(Math.round(((key1.price*key.rate)/100) + "e+2")+"e-2");
      tmpTotals += totAmount;
      key1[chargeType] = totAmount;
      callback1();
     }
    },
  function(err){
      if(err)
        callback(6,null);
      else {
      totals[chargeType] = tmpTotals;
      totals.totalTax += tmpTotals;
      totals.totalPrice += tmpTotals;
      debuglog(tmpTotals);
      debuglog(totals);
     
      callback();
     }
    });
   }
   },
   function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
        data.totals = totals;
            filterDeliveryOptions(req,res,9,data,custEmail,sellerDetails);
       }
    }); 

    
 }
    
   
function filterDeliveryOptions(req,res,stat,data,custEmail,sellerDetails){
debuglog("Currently checking whether cust email is in business detail table");
debuglog("If yes then considered as super user else normal user");
debuglog("TODO add role check later");
debuglog("If super user then populate paymentgateways array");
debuglog("If pg array is not null then deliver all delivery options");
debuglog("Not super user then dont populate paymentgateways array");
debuglog("But check if delivery option is cash/online and render accordingly")
debuglog(custEmail);

var dataUtils = require('./dataUtils');
var tblName = data.orgId+'_payment_gateway';
var isSuperUser = false;
var async = require('async');
var isError = 0;
          
   async.parallel({
         superStat: function(callback){dataUtils.isSuperUser(custEmail,data.orgId,callback); },
         payStat: function(callback){dataUtils.PGReleaseData(tblName,callback); } 
         },
         function(err,results) {
         debuglog("Super and PG check result");
         debuglog(results);
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
            if(results.superStat == 'super'){
              isSuperUser = true;
              data.sellerDetails  = sellerDetails;
              data.paymentGateways = results.payStat;
              }
            else{
             isSuperUser = false;
             data.paymentGateways = [];
             data.sellerDetails = [];
            
            }
            
             var pgs = results.payStat;  
             setPaymentFlag(req,res,9,data,isSuperUser,pgs);
            }
          });
          
}

function setPaymentFlag(req,res,stat,data,isSuperUser,pgs){

var onlinePayments = 0;

if(pgs.length == 0){
debuglog("There are no PG released so online payments can't be made ");
onlinePayments = 0;
}
if(pgs.length == 2){
debuglog("There are 2 PG released so one is Test and one is Prod");
debuglog("Enable Online payment");
debuglog("Note this is a short cut since it may be possible 2 Test PG released for different")
debuglog("Service providers");
debuglog("TODO handle this usecase later");
onlinePayments = 1;
}
if(pgs.length == 1){

if(isSuperUser == false && pgs[0].gatewayType.label == 'Prod')
  onlinePayments = 1;

if(isSuperUser == false && pgs[0].gatewayType.label == 'Test')
  onlinePayments = 0;
  
if(isSuperUser == true)
  onlinePayments = 1;

}
 renderDeliveryOptions(req,res,9,data,onlinePayments);
}
function renderDeliveryOptions(req,res,stat,data,onlinePayments){
var async=require('async');
if(onlinePayments == 1){
debuglog("Both cash and online are enabled so just render");
processStatus(req,res,9,data);

}
else{

  async.forEachOf(data.deliveryOptions,function(key,value,callback){
   debuglog("deliveryoption being processed");
   debuglog(key);
    async.forEachOf(key.payOptions,function(key1,value1,callback1){
      debuglog("payoption for the deliveryoption is");
      debuglog(key1);
        if(key1.label == 'Online')
           key1.enabled = false;
       callback1();
     },
     function(err){
      if(err)
        callback();
      else
        callback();
     });
  },
  function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
             processStatus(req,res,9,data);
            }
     });

}

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;



