function createShoppingCart(tablName,insertData,callback){
   
   var promise = db.get(tablName).insert({
   org_id: insertData.orgId,
   customer_id: insertData.customerId,
   itemCode: insertData.itemCode,
   itemDetailCode: insertData.itemDetailCode,
   qty: insertData.qty,
   shopping_time: new Date()
   });
  promise.on('success',function(doc){
    callback(null,null);

 });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
} 

function checkShoppingCart(tablName,checkData,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get(tablName).find({
    $and:[{org_id:checkData.orgId},
          {customer_id: checkData.customerId},
          {itemCode: checkData.itemCode},
          {itemDetailCode: checkData.itemDetailCode}]
          });
  promise.each(function(doc){
   totalRows++;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1;
       callback(null,data); //entry not found in cart 
       }
      else if(totalRows > 1){
       callback(6,data); //system error
      }
      else {
         data.errorCode = 0;
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
 }

 function removeShoppingCart(tablName,checkData,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get(tablName).remove({
    org_id:checkData.orgId,
    customer_id: checkData.customerId,
    itemCode: checkData.itemCode,
    itemDetailCode: checkData.itemDetailCode
          });
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
 }

function updateShoppingCart(tablName,updateData,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get(tablName).update({
    org_id:updateData.orgId,
    customer_id: updateData.customerId,
    itemCode: updateData.itemCode,
    itemDetailCode: updateData.itemDetailCode
          },
    {$set: {qty: updateData.qty}},
    {upsert:false});
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
 }

 function getShoppingCart(tablName,checkData,callback){
 
  var totalRows =0;
  var data = {};
  var resultArr = [];
 
  //render in same order as user added to cart  
  var promise = db.get(tablName).find({
    $and:[{org_id:checkData.orgId},
          {customer_id: checkData.customerId}
          ]},
          {sort:{shopping_time:1}});
  promise.each(function(doc){
  var tmpArr = {itemCode:doc.itemCode,itemDetailCode:doc.itemDetailCode,qty:doc.qty};
  resultArr.push(tmpArr);
   totalRows++;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1;
       data.items = [];
       callback(null,data); //entry not found in cart 
       }
      else {
         data.errorCode = 0;
         data.items = resultArr;
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
 }

function getCustomerDetails(tablName,custId,callback){
 
  var totalRows =0;
  var data = {};
  var resultArr = {};
 
  var promise = db.get(tablName).find({customer_id:custId});
  promise.each(function(doc){
  //resultArr.push(doc);
  resultArr = doc;
   totalRows++;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1;
       data.custDetails = [];
       callback(null,data); //No customer details, first time shopping 
       }
      else {
         data.errorCode = 0;
         data.custDetails = resultArr; //multiple rows error is not checked will be taken care while creating
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
 }

 function getCartCount(tablName,checkData,callback){
 
  var data = {};
 
  var promise = db.get(tablName).count({
    $and:[{org_id:checkData.orgId},
          {customer_id: checkData.customerId}
          ]}
          );
  promise.on('complete',function(err,doc){
       data.countItems = doc;
       callback(null,data); //entry not found in cart 
       });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
 
 }

function getPGReleaseData(tblName,callback){
   var totalRows =0;
   var result = [];
   
  
  debuglog("Will get the list of all Released Payment Gateways");
  debuglog("Including Test for the given Business");
  var promise = db.get(tblName).find({status_id:84},{sort:{gatewayPriority: 1}});
   promise.each(function(doc){
        totalRows++;
        var data = {};
        data.pgId = doc.pgId;
        data.gatewayId = doc.gatewayId;
        data.gatewayName = doc.name;
        data.gatewayType = doc.gatewayType;
        data.gatewayPriority = doc.gatewayPriority;
        result.push(data);
       
   }); 
   promise.on('complete',function(err,doc){
         callback(null,result);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function checkSuperUser(custEmail,orgId,callback){
   var totalRows =0;
   
  
  debuglog("Will check if a row exists in user_master table");
  debuglog("for the given email and orgId and active");
  debuglog("Because the same email may be follower of multiple org")
  var promise = db.get('user_master').find({$and:[{email:custEmail},{org_id:orgId},{user_status:9}]});
   promise.each(function(doc){
        totalRows++;
          
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
         callback(null,null);
       }
      else {
          callback(null,'super');
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getDeliveryOptions(tblName,DelData,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
   debuglog("start getting enabled and authorised delivery data");
   debuglog(DelData);
   
  var promise = db.get(tblName).find({$and:[{status_id:82},{enabled:true}]});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {};
        tmpArr.deliveryId = doc.deliveryId;
        tmpArr.deliveryName = doc.deliveryName;
        tmpArr.payOptions = doc.payOptions;
        resultArr.push(tmpArr);
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.optionDetails = resultArr;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getChargeData(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
   debuglog("start getting enabled and authorised charge data");
   debuglog("Currently we get only for Main Branch");
   debuglog("TODO after MVP use actual sellerId")
   
  var promise = db.get(tblName).find(
             {$and:[
             {sellerId:1},{status_id:82},{enabled:true},
             {$or:[{'chargeName.name':'Tax'},{'chargeName.name':'Service'}]}]});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {};
        tmpArr.chargeType = doc.chargeType.name;
        tmpArr.rateType = doc.rateType;
        tmpArr.applyRateAt = doc.applyRateAt;
        tmpArr.rate = doc.rate;
        resultArr.push(tmpArr);
   }); 
   promise.on('complete',function(err,doc){
         data.chargeData = resultArr;
         callback(null,data);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

exports.createCart =  createShoppingCart;
exports.checkCart  =  checkShoppingCart;
exports.removeCart =  removeShoppingCart;
exports.updateCart =  updateShoppingCart;
exports.getCart    =  getShoppingCart;
exports.custDetails = getCustomerDetails;
exports.countCart   = getCartCount;
exports.PGReleaseData = getPGReleaseData;
exports.isSuperUser = checkSuperUser;
exports.getDeliveryOptions = getDeliveryOptions;
exports.chargeData = getChargeData;

