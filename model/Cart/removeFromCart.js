function processRequest(req,res,data){
   if(typeof req.body.itemCode === 'undefined' ||typeof req.body.itemDetailCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItemCode(req,res,9,data);
}


function getItemCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var custId = data.customerId;
   
   var itemCode = req.body.itemCode;
   var itemDetailCode = req.body.itemDetailCode;
   
   data.itemCode = itemCode;
   data.itemDetailCode = itemDetailCode;
     var extend = require('util')._extend;

     var checkData = extend({},data);
         datautils.checkCart('shopping_cart',checkData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 0) {
              processStatus(req,res,4,data);
              return;
              }
            else {
              removeItemFromCart(req,res,9,data);
            }
          }
        });
}



function removeItemFromCart(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var extend = require('util')._extend;

   var deleteData = extend({},data);
   
    datautils.removeCart('shopping_cart',deleteData,function(err,result) {
        if(err) processStatus(req,res,6,data);
         else 
              getCartItems(req,res,9,data);
               
        });
} 

//TODO: see how these can go into common utils
function getCartItems(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var extend = require('util')._extend;
  
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var custId = data.customerId;
   
   var checkData = extend({},data);
   async.parallel({
          itemDetails: function(callback){datautils.getCart('shopping_cart',checkData,callback);} 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
          if(isError == 0 && results.itemDetails.errorCode !=0 ) {
            //because after removing last item it will be empty Cart and eror Code
            //will be 1 which is fine, just send response as success 
              processStatus (req,res,9,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
            
              var itemsArray = results.itemDetails.items;
              getItemDetails(req,res,9,data,itemsArray);
            }
          });
}



function getItemDetails(req,res,stat,data,itemsArr) {

var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(itemsArr,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.itemCode,tablName,function(err,results){
     if(err){
     isError = 1;
     callback();
     }
     else {
       key['itemDetails'] = results;
       var searchCond = {itemDetailCode:key.itemDetailCode};
       var tablNameSelection = data.orgId+'_menuitem_detail'; //TODO-->move to common definitions file
       renderUtils.selectionData(data.orgId,data.businessType,key.itemCode,searchCond,tablNameSelection,function(err,results1){
        if(err){
          isError = 1;
          callback();
         }  
     else {
       key['itemSelection'] = results1;
       callback();
      }
     });
    }
   });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else
            calculateTotals(req,res,9,data,itemsArr);
          }
     }); 
} 

//There will be only one row in itemDetails and itemSelection
//But the function will return in array so use [0]
function calculateTotals(req,res,stat,data,itemsArr){
 var processArr = [];
 var async = require('async');
 
   async.forEachOf(itemsArr,function(key,value,callback){
     var tmpArr = {};
     tmpArr.code = key.itemCode;
     tmpArr.name = key.itemDetails.masterData[0].itemName;
     tmpArr.itemDetailCode = key.itemDetailCode;
     var qty = key.qty;
     tmpArr.qty =  qty;
     
     if(key.itemDetails.masterData[0].itemVisual)
      tmpArr.visual = key.itemDetails.masterData[0].itemVisual[0];
     else
      tmpArr.visual = [];
     //Assuming visual array is sorted by sequence
     //else first sort by sequence and then get first item
     var uprice = parseFloat(key.itemSelection.selectionData[0].price);
     
     tmpArr.unitPrice = uprice;
     var price = +(Math.round((qty*uprice) + "e+2")  + "e-2");    
     tmpArr.price = price;
   
     //Round to last 2 digits
     //If this does not work then use toFixed method slightly slower
     //Refer https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
     //To fix rounding problems for decimals
     
     processArr.push(tmpArr);
     callback();
     
  },
  function(err){
      if(err)
        callback(6,null);
      else {
             //data.cartItems = processArr;
             calculateGrandTotal(req,res,9,data,processArr);
            }
     }); 


}

function calculateGrandTotal(req,res,stat,data,processArr){
 var async = require('async');
 var actualPrice = 0;
 var totalPrice = 0;
 var baseCurrency = 'INR'; //TODO get from definitions after MVP
 var discount     = 0; //TODO actual discount calculation after MVP
 var cartItem = {};
   async.forEachOf(processArr,function(key,value,callback){
   price = key.price;
   actualPrice += price;
   callback();
     
  },
  function(err){
      if(err)
        callback(6,null);
      else {
             var totals = {};
             totals.actualPrice = actualPrice;
             totals.totalPrice = actualPrice;
             totals.baseCurrency = baseCurrency;
             totals.discount = discount;
             data.totals = totals;
             processStatus(req,res,9,data);
            }
     }); 


}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

