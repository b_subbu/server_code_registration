function processRequest(req,res,data){
        getCount(req,res,9,data);
}


function getCount(req,res,stat,data){

  var utils = require('./dataUtils');
  var extend = require('util')._extend;

   var insertData = extend({},data);
   
    
    utils.countCart('shopping_cart',insertData,function(err,result) {
        if(err) processStatus(req,res,6,data);
         else{ 
              data.cartCount = result.countItems;
              processStatus(req,res,9,data);
              }
               
        });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

