function processRequest(req,res,data){
   if( typeof req.body.itemCode === 'undefined' ||typeof req.body.itemDetailCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItemCode(req,res,9,data);
}

function getItemCode(req,res,stat,data) {
   var async = require('async');
   var utils = require('../Menu/utils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var itemCode = req.body.itemCode;
  
      utils.checkItemCode(orgId,bizType,itemCode,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 5)) {
              processStatus(req,res,4,data);
              return;
              }
            else {
            data.itemCode = itemCode;
            itemStatus = result.itemStatus;
            if(itemStatus != 84){
              processStatus(req,res,4,data); //only InUse status
              return;
            }
            else
             getSelectionFields(req,res,9,data);
            }
          }
        });
}



function getSelectionFields(req,res,stat,data) {
   var async = require('async');
   var utils = require('../Menu/utils');
   var type = require('type-detect');
   var bizData = [];
  
    utils.selectionFieldsOrgId(data.orgId,data.businessType,data.itemCode,function(err,result) {
       if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object') {
                  selectionFields = result.selectFields;
                  selectionTable = result.table_name_detail;
                  getSelectionData(req,res,stat,data,selectionFields,selectionTable);      
                  }
            else {
                  processStatus(req,res,5,data); 
                }
        }
     });
} 

function getSelectionData(req,res,stat,data,selectionFields,table_name) {
   var async = require('async');
   var utils = require('../Menu/utils');
   var type = require('type-detect');
   var bizData = [];
   var selectFields = [];
  
    var itemDetailCode = req.body.itemDetailCode;
    data.itemDetailCode = itemDetailCode;
    utils.selectionData(data.orgId,data.businessType,data.itemCode,table_name,function(err,result) {
        if(err) processStatus(req,res,6,data);
         else 
               checkItemDetailCode(req,res,9,data,result.selectionData);
               
        });
} 

function checkItemDetailCode(req,res,stat,data,selectionData) {

  var itemDetailCode = data.itemDetailCode;
  var async = require('async');
  var isExists = false;
  
      async.forEachOf(selectionData,function(key1,value1,callback1){
         async.forEachOf(key1,function(key2,value2,callback2){
            if('itemDetailCode' == value2)  {
                if(itemDetailCode == key2)
                   isExists = true;
                  callback2();
                 }
            else
              callback2();
        },
      function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
        else {
             callback1(); 
           }
        });
      },
    
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
         if(isExists == false)
           processStatus(req,res,5,data);
        else
          checkItemInCart(req,res,9,data);
        }
      });
 

} 

function checkItemInCart(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var custId = data.customerId;
   
   var itemCode = req.body.itemCode;
   var itemDetailCode = req.body.itemDetailCode;
   
   data.itemCode = itemCode;
   data.itemDetailCode = itemDetailCode;
 
     var extend = require('util')._extend;
     var checkData = extend({},data);
  
      //If item is already in cart it will return error code as zero
      //may be user trying to add again with refresh or something
      datautils.checkCart('shopping_cart',checkData,function(err,result) {
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode != 1) {
              processStatus(req,res,7,data);
              return;
              }
            else {
              addItemToCart(req,res,9,data);
            }
          }
        });
}

function addItemToCart(req,res,stat,data) {
   var utils = require('./dataUtils');
   var extend = require('util')._extend;

   var insertData = extend({},data);
   
    if(req.body.qty === undefined || req.body.qty === null)
     var qty = 1; //just default if quantity not sent by any chance
   else
     var qty = req.body.qty;
    
    insertData.qty = qty;
    
    utils.createCart('shopping_cart',insertData,function(err,result) {
        if(err) processStatus(req,res,6,data);
         else 
              getCount(req,res,9,data);
               
        });
} 

function getCount(req,res,stat,data){

  var utils = require('./dataUtils');
  var extend = require('util')._extend;

   var insertData = extend({},data);
   
    
    utils.countCart('shopping_cart',insertData,function(err,result) {
        if(err) processStatus(req,res,6,data);
         else{ 
              data.cartCount = result;
              processStatus(req,res,9,data);
              }
               
        });
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
