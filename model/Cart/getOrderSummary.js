function processRequest(req,res,data){
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus left asis for getting customer email")

}

function checkMasterStatus(req,res,data){
    
    var cid = data.customerId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var custutils = require('../Render/utils');
    var isError = 0;
    var custEmail = "";
    
    
    
     async.parallel({
         custStat: function(callback){custutils.checkCustomerId(cid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
        if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
            if(isError == 0) {
              custEmail = results.custStat.customerEmail;
              getCartItems(req,res,9,data,custEmail);
            }
          }); 
}

function getCartItems(req,res,stat,data,custEmail) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var extend = require('util')._extend;
  
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var custId = data.customerId;
   var deliveryTblName = data.orgId+'_delivery_option';
   
   var checkData = extend({},data);
   async.parallel({itemDetails: function(callback){datautils.getCart('shopping_cart',checkData,callback);}
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.itemDetails.errorCode !=0 ) {
              processStatus (req,res,4,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
                data.billingDetails = req.body.billingDetails;
                data.shippingDetails = req.body.shippingDetails;
             
            var itemsArray = results.itemDetails.items;
             getItemDetails(req,res,9,data,itemsArray,custEmail);
            }
          });
}



function getItemDetails(req,res,stat,data,itemsArr,custEmail) {

var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(itemsArr,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.itemCode,tablName,function(err,results){
     if(err){
     isError = 1;
     callback();
     }
     else {
       key['itemDetails'] = results;
       var searchCond = {itemDetailCode:key.itemDetailCode};
       var tablNameSelection = data.orgId+'_menuitem_detail'; //TODO-->move to common definitions file
       renderUtils.selectionData(data.orgId,data.businessType,key.itemCode,searchCond,tablNameSelection,function(err,results1){
        if(err){
          isError = 1;
          callback();
         }  
     else {
       key['itemSelection'] = results1;
       callback();
      }
     });
    }
   });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else
            getInvoiceConfigDetails(req,res,9,data,itemsArr,custEmail);
          }
     }); 
} 


function getInvoiceConfigDetails(req,res,stat,data,itemsArr,custEmail) {
   var async = require('async');
   var isError = 0;
      
   var sellerutils = require('../Sellers/dataUtils');
   var configutils = require('../Checkout/dataUtils');  
 
 debuglog("Currently we are calling another function to get InvoiceConfig Details");
 debuglog("But this can be split into two to get Auhtorised Invoiceconfig Id of sellerId");
 debuglog("And then use it in dataUtils of InvoiceConfig to get data TODO");
 
  var sellerTabl = data.orgId+'_sellers';
  var configTabl = data.orgId+'_invoice_config';
    
  var sellerData = {};
       sellerData.sellerId = 1;
  
  async.parallel({
         sellerDets: function(callback){sellerutils.SellerData(sellerTabl,sellerData,callback); },
         configDets: function(callback){configutils.invoiceConfigData(configTabl,sellerData,callback); }
        },
         function(err,results) {
          debuglog("After results");
          debuglog(results);
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.sellerDets.errorCode != 0) {
              processStatus (req,res,5,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.configDets.errorCode != 0) {
              processStatus (req,res,5,data);
              isError = 1;
              return;
             }
           if(isError == 0) {
                var sellerDetails = results.sellerDets.businessDetails;
                var configDetails = results.configDets;
                debuglog("After getting results");
                debuglog(sellerDetails);
                debuglog(configDetails);
                setOrderData(req,res,9,data,sellerDetails,configDetails,itemsArr,custEmail);
            }
         });     
 }


function setOrderData(req,res,stat,data,sellerDetails,configDetails,itemsArr,custEmail){
debuglog("Template file,order.jade, is in views directory since express requires it to be there");
debuglog("TODO move this to utils since lots of variables needs to be set");
debuglog("And this may be common for cash and PayU Payments");

var async = require('async');
var moment = require('moment');
var currDate = moment().format('DD-MM-YYYY');

var orderData = {};
if(configDetails.sellerName == true)
  orderData.sellerName = sellerDetails.displayName;
else
  orderData.sellerName = '';

if(configDetails.sellerAddress == true){  
orderData.sellerAddress1 = sellerDetails.street;
orderData.sellerAddress2 = sellerDetails.city.name+' '+sellerDetails.state.name+' '+sellerDetails.country.countryName;
orderData.sellerAddress3 = sellerDetails.zipCode;
}
else{
orderData.sellerAddress1 = '';
orderData.sellerAddress2 = '';
orderData.sellerAddress3 = '';

}

if(configDetails.cin == true)
 orderData.cinNumber = sellerDetails.cinNumber;
else
 orderData.cinNumber = '';
 
if(configDetails.tin == true)
  orderData.tinNumber = sellerDetails.tinNumber;
else
  orderData.tinNumber = '';

if(configDetails.cst == true)  
  orderData.cstNumber = sellerDetails.cstNumber;
else
  orderData.cstNumber = '';

//orderData.orderId = data.orderId;
//orderData.orderDate = currDate;

if(configDetails.buyerName == true){
  orderData.buyerName =       data.billingDetails.firstName+' '+data.billingDetails.lastName;
  }
else
 orderData.buyerName == '';  
 
 
orderData.buyerEmail =     custEmail; //this may not be used in order html
if(configDetails.billingAddress == true){
orderData.billingAddress1 = data.billingDetails.address;
orderData.billingAddress2 = data.billingDetails.city+' '+data.billingDetails.state+' '+data.billingDetails.country;
orderData.billingAddress3 = data.billingDetails.zipCode;
}
else{
orderData.billingAddress1 = '';
orderData.billingAddress2 = '';
orderData.billingAddress3 = '';
}

if(configDetails.shippingAddress == true){
orderData.shippingAddress1 = data.shippingDetails.address;
orderData.shippingAddress2 = data.shippingDetails.city+' '+data.shippingDetails.state+' '+data.shippingDetails.country;
orderData.shippingAddress3 = data.shippingDetails.zipCode;
}
else{
orderData.shippingAddress1 = '';
orderData.shippingAddress2 = '';
orderData.shippingAddress3 = '';

}

//orderData.paymentMethod = 'Cash';

orderData.disclaimer = configDetails.disclaimer;


debuglog("The data for items is:");
debuglog(itemsArr);

var itemDetailArr = [];
var subTotal = 0;

async.forEachOf(itemsArr,function(key,value,callback){
  debuglog(key);
  debuglog(key.itemSelection);
  var tmpArr = {};
      if(configDetails.itemCode == true)
       tmpArr.itemCode = key.itemCode;
      else
       tmpArr.itemCode = '';
      if(configDetails.itemDetailCode == true)  
        tmpArr.itemDetailCode = key.itemDetailCode;
      else
        tmpArr.itemDetailCode = '';
      if(configDetails.price == true)
        tmpArr.price = key.itemSelection.selectionData[0].price;
      else
        tmpArr.price = '';
      if(configDetails.quantity == true)  
        tmpArr.qty = key.qty;
      else
        tmpArr.qty = '';
      var lineItemTotal = +(Math.round((key.itemSelection.selectionData[0].price*key.qty) + "e+2")+"e-2");   
      if(configDetails.totals == true)    
       tmpArr.total = lineItemTotal;
      else
       tmpArr.total = '';
      subTotal += lineItemTotal;
      itemDetailArr.push(tmpArr);
      callback();
   
},
  function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
           orderData.items = itemDetailArr; 
           orderData.subTotal = subTotal;
           //orderData.baseCurrency = sellerDetails.baseCurrency;
           
           debuglog("hard-coded utf8 code for Indian Rupee");
           debuglog("TODO create currency symbol table and get code from it");
           orderData.baseCurrency = "&#8377;";
           debuglog("The data for populating template is:");
           debuglog(orderData);

           setCharges(req,res,9,data,sellerDetails,configDetails,orderData);
  }
 });

}
 
function setCharges(req,res,stat,data,sellerDetails,configDetails,orderData){
debuglog("Get all charges as defined in InvoiceConfig Details");
debuglog("For MVP it is only Applicable on Totals");
debuglog("TODO for charges applicable on item level");

var chargeutils = require('../Checkout/dataUtils');
var async = require('async');
var chargeArr = [];
var deliveryOption = req.body.deliveryOption;

var baseAmount = orderData.subTotal;
var grandTotal = orderData.subTotal;

var chargeIds = configDetails.chargeIds;
var tblName   = data.orgId+"_charge_type";
  chargeutils.chargeData(tblName,chargeIds,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
                debuglog("Result of charge is:");
                debuglog(result);
                debuglog("If chargeName.name is Tax then just put chargeType name as label");
                debuglog("Calculate the totals as % or absolute and add to grandtotal");
                debuglog("If not Tax, then check if id matches delivery option");
                debuglog("As of now charge can be only tax or delivery cost");
                debuglog("Not others also charges are applied only on totals for now");
                debuglog("Added service type charge which is similar to Tax");
                var chargeData = result.chargeData;
   async.forEachOf(chargeData,function(key,value,callback){
   debuglog("Charge item:");
   debuglog(key);
   debuglog(value);
   var chargeType = key.chargeType;
   var tmpArr = [];
   tmpArr[0] = chargeType.name;
   
   if(key.chargeName.name == 'Tax' || key.chargeName.name == 'Service'){
   debuglog("This is a Tax type charge");
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var thisAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    else{
      debuglog("Rate at Percentage");
      var thisAmount = +(Math.round(((baseAmount*key.rate)/100) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    }
    else {
    debuglog("This is not a Tax so would depend on delivery option passed");
    
   if(key.chargeType.id == deliveryOption){
        debuglog("This charge is applicable for the given delivery option");
        debuglog("Note charge types are reversed between tax and other!!!");
     tmpArr[0] = key.chargeName.name;
      
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var thisAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    else{
      debuglog("Rate at Percentage");
      var thisAmount = +(Math.round(((baseAmount*key.rate)/100) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    }
    else
      callback();
    } 
    }, 
    function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
        orderData.charges = chargeArr;
        orderData.grandTotal = grandTotal;
        //renderOrder(req,res,stat,data,orderData);
        convertUndefined(req,res,stat,data,orderData);
        //processStatus(req,res,9,data);
        //debuglog("No process status return for success case");
       }
    });
  }
 });
} 

function convertUndefined(req,res,stat,data,orderData){
debuglog("Will convert all undefined properties to null");
debuglog("also buyername address can have undefined in the middle of string which is not a type");
debuglog("tough to combine in assignwith because items is an object");

var _ = require('lodash');
var finalData = {};

_.assignWith(finalData,orderData,modifyValue);


 
debuglog(finalData);
renderOrder(req,res,stat,data,finalData);
}

function modifyValue(srcValue,objValue){

var _ = require('lodash');
 
 if (_.isUndefined(objValue)){
  debuglog("this is an undefined value so return empty string");
  return '';
 }
 
 if(_.isArray(objValue) || _.isNumber(objValue)){
 debuglog("This is an array probably items charges etc., so dont do anything");
 return objValue;
 }

 if(_.isString(objValue)){
 debuglog("This is a string probably can have a undefined in the middle if any part of a field is not defined");
 debuglog(objValue);
 var x = _.replace(objValue,"undefined","");
 debuglog(x);
 debuglog("will return this modified string then");
 return x;
 
 }

}
function renderOrder(req,res,stat,data,orderData){
           debuglog("Final data for rendering along with template is:");
           debuglog(orderData);
           //data.orderData = orderData;
            res.render('order.jade',orderData,function(err,htmlfile){
	          debuglog(htmlfile);
            console.log(err);
            data.htmlData = htmlfile;
            processStatus(req,res,9,data);
            });
           //res.render('order.jade',orderData);
}
 
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'customerappController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;


