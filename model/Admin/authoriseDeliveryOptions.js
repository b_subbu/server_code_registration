function processRequest(req,res,data){
   if(typeof req.body.deliveryIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkDelStatus(req,res,9,data);
}


function checkDelStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var DelArr = req.body.deliveryIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.deliveryIds = DelArr;
  
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tblName = orgId+'_Revision_delivery_option'; //TODO: move to a common definition file

    async.forEachOf(DelArr,function(key,value,callback1){
      var DelData = extend({},data);
          DelData.delivery = {};
          DelData.delivery.deliveryId = key;
      debuglog("start checking if deliveryID exists in Revision table before authorise");
      debuglog(DelData)    
     datautils.checkDelCode(tblName,DelData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             else{
                   if (result.status == 81){
                   debuglog("This is entered status item");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 89){
                   debuglog("This is authorised delete item");
                   debuglog("So change status to deleted");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
             }
             debuglog("Since Authorise is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
            }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              moveToMain(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
             }
           }
      });
}



function moveToMain(req,res,stat,data,enteredItems,authorisedItems){
    
    //var DelArr = data.deliveryIds;
    
    debuglog("Now start moving the entries to Main Table");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_delivery_option'; //TODO: move to a common definition file
    var mainTable    = orgId+'_delivery_option';
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
    debuglog("This may not really be required for Ready2Delete just keeping same to reduce code");
    
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      deliveryId = key;
      debuglog("in forEach of enteredItems");
      debuglog(enteredItems);
      debuglog(key);
      debuglog(deliveryId);
     async.parallel({
          queStat: function (callback){datautils.workQueue(revisionTable,orgId,deliveryId,callback); },
          relStat: function (callback){datautils.moveDelHeader(revisionTable,mainTable,deliveryId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
          if(authorisedItems.length > 0){
          debuglog("This request has some authorised items too");
          debuglog(authorisedItems);
          deleteInMain(req,res,9,data,enteredItems,authorisedItems);
          }
          else{
          debuglog("This request has no authorised items");
          changeDelStatus(req,res,9,data,enteredItems,mainTable,revisionTable); 
         }
         }
     });
}

function deleteInMain(req,res,stat,data,enteredItems,authorisedItems){
    
    
    debuglog("Now start moving the entries to Main Table");
    debuglog("It is just enough to update status and remove from Main here");
    debuglog("But retaining functionality as is to reduce code");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_delivery_option'; //TODO: move to a common definition file
    var mainTable    = orgId+'_delivery_option';
    
    debuglog("Note this will delete main data and copy Revison Data i.e, overwrite");
    debuglog("This may not really be required for Ready2Delete just keeping same to reduce code");
    
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      deliveryId = key;
     async.parallel({
          queStat: function (callback){datautils.removeWorkQueue(revisionTable,orgId,deliveryId,callback); },
          relStat: function (callback){datautils.moveDelHeader(revisionTable,mainTable,deliveryId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeDelStatusDelete(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable); 
         }
     });
}

function changeDelStatusDelete(req,res,stat,data,enteredItems,authorisedItems,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(authorisedItems,function(key,value,callback1){
    deliveryId = key;
    var reason = 'Delivery option Deleted';
       statutils.changeDelStatus(mainTable,data.orgId,data.businessType,data.userId,deliveryId,88,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 changeDelStatus(req,res,9,data,enteredItems,mainTable,revTable);
                 debuglog("It is ok to call even if no enteredItems");
                 debuglog("As it will simply skip the loop");
             }
          });
     
}


function changeDelStatus(req,res,stat,data,DelArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(DelArr,function(key,value,callback1){
    deliveryId = key;
    var reason = '';
       statutils.changeDelStatus(mainTable,data.orgId,data.businessType,data.userId,deliveryId,82,'Delivery Option Authorised',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                  getDelDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getDelDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.DelsData(mainTable,data.deliveryIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getDelStatusName(req,res,9,data,result); 
         }
     });
    
}

function getDelStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('delivery_option_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.deliveryData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
   
exports.getData = processRequest;



