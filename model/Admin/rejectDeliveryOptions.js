function processRequest(req,res,data){
   if(typeof req.body.deliveryIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkDelStatus(req,res,9,data);
}


function checkDelStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var DelArr = req.body.deliveryIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.deliveryIds = DelArr;
    
    var orgId = data.orgId;
    
    var tblName = orgId+'_Revision_delivery_option'; //TODO: move to a common definition file

    async.forEachOf(DelArr,function(key,value,callback1){
      var DelData = extend({},data);
          DelData.delivery = {};
          DelData.delivery.deliveryId = key;
      debuglog("start checking if deliveryID exists in Revision table before reject");
      debuglog(DelData)    
     datautils.checkDelCode(tblName,DelData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             deleteDeliveryOptions(req,res,9,data);
           }
      });
}



function deleteDeliveryOptions(req,res,stat,data){
    
    var DelArr = data.deliveryIds;
    
    debuglog("Now start deleting the entries in Revision Table");
    debuglog("Otherwise we will have 2 rows in main table with same id but different status");
    debuglog(DelArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_delivery_option'; //TODO: move to a common definition file
    var mainTable    = orgId+'_delivery_option';
    
    async.forEachOf(DelArr,function(key,value,callback1){
      deliveryId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteDelCode(revisionTable,deliveryId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
               processStatus(req,res,6,data);
            }
      else
          {
          changeDelStatus(req,res,9,data,DelArr,mainTable,revisionTable); 
         }
     });
}

//Just keep these fns in case if they need to move Rejected rows to main table later
function changeDelStatus(req,res,stat,data,DelArr,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    debuglog("Note here actual status change should not happen so the item will remain in same status");
    debuglog("Below is called only for AuditTrail");
    async.forEachOf(DelArr,function(key,value,callback1){
    deliveryId = key;
    var reason = '';
       statutils.changeDelStatus(revisionTable,data.orgId,data.businessType,data.userId,deliveryId,87,'Delivery Option Rejected',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.deliveryIds = DelArr;
                 getDelDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getDelDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.DelsData(mainTable,data.deliveryIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getDelStatusName(req,res,9,data,result); 
         }
     });
    
}

function getDelStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('delivery_option_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.deliveryData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
   
exports.getData = processRequest;




