function getPaymentGatewaysData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
  
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    var searchArr = [];
      
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
  
    if(tid == 501)
     var tblName = oid+'_payment_gateway';
    else
     var tblName = oid+'_Revision_payment_gateway';
  
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'pg_status_master',callback); }
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList,tblName);
                  }
                  else {
                      var searchArr = [];
                      var tmpArray = {status_id: {"$in":statList}};
                      searchArr.push(tmpArray);
                      debuglog(searchArr);
                      getPGCount(req,res,stat,data,searchArr,tblName);
                    }
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList,tblName) {
   var async = require('async');
   var filterutils = require('./filterUtils');
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.PGFilterOptions(key_name,statList,pid,tid,function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
               filterOptions[key_name] = results;
               callback();
            }
          }); 
        }
     else
        callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call alerts count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           //var userArray = {org_id: data.orgId };
           //searchArr.push(userArray);
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
            getPGCount(req,res,stat,data,searchArr,tblName);
           }
         else 
         getPGSearch(req,res,stat,data,statList,tblName);
         }
      });
} 


function getPGSearch(req,res,stat,data,statList,tblName) {

 var searchInp = req.body.searchParams;

    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
   
 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 //var userArray = {};
 //searchArr.push(userArray);
  
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
      case 'name':
        var tmpArray =  {name: {$regex:key,$options:'i'}}; ; 
        searchArr.push(tmpArray);
        break;
      case 'type':
        var tmpArray = {'gatewayType.id':key};
        searchArr.push(tmpArray);
        break; 
      case 'priority':
       var tmpArray1 = {gatewayPriority: parseInt(key)};
       searchArr.push(tmpArray1);
       break;
      case 'date':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var cmpDate = new Date(minDateArr[2],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDateArr[2],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {update_time: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {update_time: {$lte: cmpDate2}};
        searchArr.push(tmpArray);    
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           
            getPGCount(req,res,9,data,searchArr,tblName);
            debuglog("We will start getting data with search Params");
            debuglog(searchArr);
          }
     });
}

function getPGCount(req,res,stat,data,statList,tblName){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
    //TODO probably have only tabl_name no need for orgId or bizType
   //since table name itself will have them      
   utils.PGCount(orgId,bizType,tblName,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
           debuglog("After getting results");
           debuglog(results);
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getPaymentGateways(req,res,9,data);
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getPGData(req,res,9,data,statList,tblName); 
             }
            }
         });
}


function getPGData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var utils = require('./dataUtils');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
   
      utils.PGHeader(headerTable,orgId,bizType,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getPaymentGateways(req,res,9,data); //no PG for the business nothing to do further
             //will be handled at count itself, just adding here
            }
            else {
            gateways = result.gateways;
            debuglog(gateways); 
            getPGStatusName(req,res,9,data,gateways);
            }
          }
        });
} 


function getPGStatusName(req,res,stat,data,gateways) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(gateways,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames('pg_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.productData = gateways;
            getPaymentGateways(req,res,9,data);
           }
      });
 

} 


function getPaymentGateways(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');

 var tabl_name = 'payment_gateways_master';
 
  datautils.PGMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from PG Master table");
           debuglog("Gateway Types are hard-coded for now");
           debuglog("So take care label matches everywhere");
           debuglog("TODO create gatewaytypes master later");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.paymentGateways = result;
                var subTypes = [{id:1,label:'Test'},{id:2,label:'Prod'}];
                data.gateways = [{id:1,name:'PayU',subTypes:subTypes}];
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getProductData = getPaymentGatewaysData;






