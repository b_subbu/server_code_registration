function processRequest(req,res,data){
    if(typeof req.body.deliveryIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkDelStatus(req,res,9,data);
}


function checkDelStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var DelArr = req.body.deliveryIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.deliveryIds = DelArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_delivery_option'; //TODO: move to a common definition file

    async.forEachOf(DelArr,function(key,value,callback1){
        var DelData= {};
          DelData.delivery = {};
          DelData.delivery.deliveryId = key;
        datautils.checkDelCode(tabl_name,DelData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });       }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToRevision(req,res,9,data);
          }
      });
}

function moveToRevision(req,res,stat,data){
    
    var DelArr = data.deliveryIds;
    
    debuglog("Now start moving the entries to Revision Table");
    debuglog(DelArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_delivery_option'; //TODO: move to a common definition file
    var mainTable    = orgId+'_delivery_option';
   
    async.forEachOf(DelArr,function(key,value,callback1){
      deliveryId = key;
     async.parallel({
          relStat: function (callback){datautils.moveDelHeader(mainTable,revisionTable,deliveryId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeDelStatus(req,res,9,data,DelArr,mainTable,revisionTable); 
         }
     });
}

function copyToRevision(req,res,stat,data,DelData){
    
    
    debuglog("Even though this called Copy we are actually creating a new entry");
    debuglog(DelData);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_delivery_option'; //We will create the new entry in Revision Table
   
       if(   DelData.delivery.enabled == null
       || DelData.delivery.enabled == 'undefined'
       || DelData.delivery.payOptions  == null
       || DelData.delivery.payOptions  == 'undefined' 
      )
        processStatus(req,res,4,data);
    else{
         datautils.DelUpdate(revisionTable,data.deliveryId,DelData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(DelData);
                var DelArr = [];
                DelArr.push(DelData.delivery.deliveryId);
                changeDelStatus(req,res,9,data,DelArr,'',revisionTable);
                }
        });
      }
}


function changeDelStatus(req,res,stat,data,DelArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(DelArr,function(key,value,callback1){
    deliveryId = key;
    var reason = '';
       statutils.changeDelStatus(revTable,data.orgId,data.businessType,data.userId,deliveryId,81,'Delivery Option Submitted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.deliveryIds = DelArr;
                 getDelDetails(req,res,9,data,revTable);
             }
          });
     
}


function getDelDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.DelsData(revTable,data.deliveryIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getDelStatusName(req,res,9,data,result); 
         }
     });
    
}

function getDelStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('delivery_option_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.deliveryData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
   
exports.getData = processRequest;
exports.moveToRevision=moveToRevision; 
exports.copyToRevision=copyToRevision;

