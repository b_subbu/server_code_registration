function processRequest(req,res,data){
        getDelCode(req,res,9,data);
}


function getDelCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var DelData = extend({},data);
       DelData.delivery = req.body.deliveryOptionDetails;
       DelData.status_id = 80;
      
     var tblName = data.orgId+'_delivery_option'; //create will always be in main table
     debuglog("Check if the delivery Id exists or not");
     debuglog(DelData);
     debuglog(req.body);
      datautils.checkDelCode(tblName,DelData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
               if(result.errorCode != 0) 
                 processStatus(req,res,result.errorCode,data);
               else
                saveDelData(req,res,9,data,DelData,tblName);
            }
        });
} 

function saveDelData(req,res,stat,data,DelData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
     debuglog("This is after getting data from request");
     debuglog(DelData);
     debuglog(data);
     
     data.deliveryId = DelData.delivery.deliveryId;
     
    var submitop = req.body.submitOperation;
  
   
    if(   DelData.delivery.enabled == null
       || DelData.delivery.enabled == 'undefined'
       || DelData.delivery.payOptions  == null
       || DelData.delivery.payOptions  == 'undefined'
       || DelData.delivery.workQueue == null
       || DelData.delivery.workQueue == 'undefined' 
      )
        processStatus(req,res,4,data);
    else{
    
     datautils.DelUpdate(tblName,data.deliveryId,DelData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(DelData);
             if(submitop == 1){
             var delIds = [];
             delIds.push(data.deliveryId);
             data.deliveryIds = delIds;
               var submitDel = require('./submitDeliveryOptions');
               submitDel.moveToRevision(req,res,9,data);
             }
             else{
                changeDelStatus(req,res,9,data,DelData,tblName);
                }
            }
        });
        }
} 


function changeDelStatus(req,res,stat,data,DelData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeDelStatus(tblName,data.orgId,data.businessType,data.userId,data.deliveryId,80,'Delivery Option Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getDelData(req,res,9,data,DelData,tblName);
               }
        });
} 

function getDelData(req,res,stat,data,DelData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
    
    var delArr = [];
    delArr.push(data.deliveryId) 
   datautils.DelsData(tblName,delArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getDelStatusName(req,res,9,data,result); 
         }
     });
    
}

function getDelStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('delivery_option_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.deliveryData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
   
exports.getData = processRequest;









