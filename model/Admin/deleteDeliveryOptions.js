function processRequest(req,res,data){
    if(typeof req.body.deliveryIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkDelStatus(req,res,9,data);
}


function checkDelStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var DelArr = req.body.deliveryIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.deliveryIds = DelArr;
    
    var enteredItems = [];
    var authorisedItems = [];
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_delivery_option'; //TODO: move to a common definition file

    async.forEachOf(DelArr,function(key,value,callback1){
        var DelData= {};
          DelData.delivery = {};
          DelData.delivery.deliveryId = key;
        datautils.checkDelCode(tabl_name,DelData,function(err,result){
           debuglog("Result of check del code");
           debuglog(result);
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
               else{
                  if (result.status == 80){
                   debuglog("This is entered status item");
                   debuglog("So can be deleted directly");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 82){
                   debuglog("This is authorised status item");
                   debuglog("So has to go through authoristation to delete");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Delete is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
             }
           });       }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              deleteOptions(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
              
               }
             }
      });
}

function deleteOptions(req,res,stat,data,enteredItems,authorisedItems){
    var uid = req.body.userId;
    //var DelArr = req.body.deliveryIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_delivery_option'; //delete is possible only in Main Table currently
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      deliveryId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteDelCode(mainTable,deliveryId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
          if(authorisedItems.length > 0){
          debuglog("This request has some authorised items too");
          debuglog(authorisedItems);
          deleteAuthoriseOptions(req,res,9,data,authorisedItems);
          }
          else{
          debuglog("This request has no authorised items");
          debuglog("so call return");
          processStatus(req,res,9,data); 
        }
       }
      });
}


function deleteAuthoriseOptions(req,res,stat,data,authorisedItems){
    var uid = req.body.userId;
    //var DelArr = req.body.deliveryIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_delivery_option';
    var revisionTable = data.orgId+'_Revision_delivery_option';
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      deliveryId = key;
     async.parallel({
          relStat: function (callback){datautils.copyDelHeader(mainTable,revisionTable,deliveryId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           debuglog("Change status of items in revision table to ready2Delete");
           changeDelStatus(req,res,9,data,authorisedItems,'',revisionTable);
          }
      });
}


function changeDelStatus(req,res,stat,data,DelArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(DelArr,function(key,value,callback1){
    deliveryId = key;
    var reason = 'Delete Delivery Option submitted';
       statutils.changeDelStatus(revTable,data.orgId,data.businessType,data.userId,deliveryId,89,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.deliveryIds = DelArr;
                 getDelDetails(req,res,9,data,revTable);
             }
          });
     
}


function getDelDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.DelsData(revTable,data.deliveryIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getDelStatusName(req,res,9,data,result); 
         }
     });
    
}

function getDelStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('delivery_option_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.deliveryData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
   
exports.getData = processRequest;


