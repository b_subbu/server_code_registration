function processRequest(req,res,data){
   if(typeof req.body.taxIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkTaxStatus(req,res,9,data);
}

function checkTaxStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var TaxArr = req.body.taxIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.taxIds = TaxArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_tax_type'; //TODO: move to a common definition file

    async.forEachOf(TaxArr,function(key,value,callback1){
        var TaxData= {};
            TaxData.tax = {};
            TaxData.tax.taxId = key;
        datautils.checkTaxCode(tabl_name,TaxData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });       }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToRevision(req,res,9,data);
          }
      });
}

function moveToRevision(req,res,stat,data){
    
    var TaxArr = data.taxIds;
    
    debuglog("Now start moving the entries to Revision Table");
    debuglog(TaxArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_tax_type'; //TODO: move to a common definition file
    var mainTable    = orgId+'_tax_type';
   
    async.forEachOf(TaxArr,function(key,value,callback1){
      taxId = key;
     async.parallel({
          relStat: function (callback){datautils.moveTaxHeader(mainTable,revisionTable,taxId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeTaxStatus(req,res,9,data,TaxArr,mainTable,revisionTable); 
         }
     });
}

function copyToRevision(req,res,stat,data,TaxData){
    
    
    debuglog("Even though this called Copy we are actually creating a new entry");
    debuglog(TaxData);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_tax_type'; //We will create the new entry in Revision Table
   
     if(   TaxData.tax.enabled == null
       || TaxData.tax.enabled == 'undefined'
       || TaxData.tax.taxName == null
       || TaxData.tax.taxName == 'undefined'
       || TaxData.tax.taxId == null
       || TaxData.tax.taxId == 'undefined'
      )
        processStatus(req,res,4,data);
    else{
         datautils.TaxUpdate(revisionTable,data.taxId,TaxData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(TaxData);
                var TaxArr = [];
                TaxArr.push(TaxData.tax.taxId);
                changeTaxStatus(req,res,9,data,TaxArr,'',revisionTable);
                }
        });
      }
}


function changeTaxStatus(req,res,stat,data,TaxArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(TaxArr,function(key,value,callback1){
    taxId = key;
    var reason = '';
       statutils.changeTaxStatus(revTable,data.orgId,data.businessType,data.userId,taxId,81,'Tax Type Submitted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.taxIds = TaxArr;
                 getTaxDetails(req,res,9,data,revTable);
             }
          });
     
}


function getTaxDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.TaxesData(revTable,data.taxIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getTaxStatusName(req,res,9,data,result); 
         }
     });
    
}

function getTaxStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('tax_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.taxTypeData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getData = processRequest;
exports.moveToRevision=moveToRevision; 
exports.copyToRevision=copyToRevision;


