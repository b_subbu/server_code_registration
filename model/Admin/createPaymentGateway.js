function processRequest(req,res,data){
    if(req.body.paymentGatewayDetails.gatewayType.label == 'Test'){
              getPGCode(req,res,9,data);
              debuglog("This is a Test type PG so just proceed further");
    }
    else{
            checkTestCompleted(req,res,9,data);
            debuglog("This is a Prod type PG so first check if Test type exists");
            debuglog("And business has completed test for that before proceeding");
    }
}

function checkTestCompleted(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var PGData = extend({},data);
       PGData.gateway = req.body.paymentGatewayDetails;
    
   
   var tblName = data.orgId+'_payment_gateway'; 
   
  
      datautils.checkPGTested(tblName,PGData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
              if(result.errorCode == 1){
                debuglog("Either Test PG is not created or not authorised");
                debuglog("Or not test completed");
                debuglog(result);
                processStatus(req,res,5,data);
                }
                else {
                debuglog("Testing completed so let us proceed to create Prod now");
                debuglog(result);
                isPGExists(req,res,9,data,PGData);
            }
           } 
        });
} 

function isPGExists(req,res,stat,data,PGData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
 
   var tblName = data.orgId+'_payment_gateway'; 
   
  
      datautils.checkPGExist(tblName,PGData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
              if(result.errorCode == 1){
                debuglog("PG name+type already exists");
                debuglog(result);
                processStatus(req,res,7,data);
                }
                else {
                debuglog("Testing completed so let us proceed to create Prod now");
                debuglog(result);
                getPGCode(req,res,9,data);
            }
           } 
        });
} 


function getPGCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var PGData = extend({},data);
     
  
      datautils.generatePGCode(PGData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                data.pgId = result;
                //savePGData(req,res,9,data,PGData);
                getGatewayID(req,res,9,data,PGData);
            }
        });
} 

function getGatewayID(req,res,stat,data,PGData) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     PGData.gateway = req.body.paymentGatewayDetails;
     PGData.status_id = 80;
   
  
      datautils.gatewayID(PGData,function(err,result) {
         if(err ) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
              if(result.errorCode == 1){
              debuglog("Could not get a master entry for the given name+type");
              debuglog("possibly Frontend did not pass name or type correctly");
              processStatus(req,res,4,data);
              
              }
              else{
                PGData.gateway.gatewayId = result.gatewayId;
                debuglog("Got gateway Id ok");
                debuglog(PGData);
                savePGData(req,res,9,data,PGData);
                }
            }
        });
} 

function savePGData(req,res,stat,data,PGData) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
    
     debuglog("This is after getting data from request");
     debuglog("we are not saving urls in payment gateway table");
     debuglog("That has to get from master table");
     debuglog("If it becomes slow then can directly save here but only changes can be an issue");
     debuglog(PGData);
     debuglog(data);
     
    var submitop = req.body.submitOperation;
  
    var tblName = data.orgId+'_payment_gateway'; //create will always be in main table
         
    if(   PGData.gateway.gatewayPriority == null
       || PGData.gateway.gatewayPriority == 'undefined'
       || PGData.gateway.gatewayPriority == 0 
       || PGData.gateway.paymentGatewayParams.key == null 
       || PGData.gateway.paymentGatewayParams.salt == null
       || PGData.gateway.paymentGatewayParams.key == 'undefined'
       || PGData.gateway.paymentGatewayParams.salt == 'undefined' )
        processStatus(req,res,4,data);
    else{
     datautils.PGUpdate(tblName,data.pgId,PGData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved PG data");
                debuglog(PGData);
             if(submitop == 1){
             var pgIds = [];
             pgIds.push(data.pgId);
             data.pgIds = pgIds;
               var submitPG = require('./submitPaymentGateways');
               submitPG.moveToRevision(req,res,9,data);
             }
             else
                changePGStatus(req,res,9,data,PGData,tblName);
            }
        });
        }
} 


function changePGStatus(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,data.pgId,80,'Payment Gateway Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getPGData(req,res,9,data,PGData,tblName);
               }
        });
} 

function getPGData(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
      var PGArr = [];
      PGArr.push(data.pgId);
      datautils.PGSData(tblName,PGArr,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
             debuglog("After getting PG Data");
             debuglog(result); 
              pgs = result;
              getPGStatusName(req,res,9,data,pgs);
              }
        }); 
} 

function getPGStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('pg_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.pgData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
   
exports.getData = processRequest;








