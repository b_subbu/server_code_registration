function processRequest(req,res,data){
    if( typeof req.body.pgId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkPGCode(req,res,9,data);
}


function checkPGCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var PGData = extend({},data);
       PGData.gateway = {};
   PGData.gateway.pgId = req.body.pgId;
   
    var tblName = data.orgId+'_payment_gateway'; //edit will always be in main table
   
     
  
      datautils.checkPGCode(tblName,PGData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                if(result.errorCode != 0){
                debuglog("Checking of PGId has failed");
                debuglog(result);
                data.pgId = PGData.gateway.pgId;
                processStatus(req,res,result.errorCode,data);
                }
                else{
                debuglog("Checking of PGId is successful");
                debuglog(result);
                data.pgId = PGData.gateway.pgId;
                PGData.thisPriority  = result.priority;
                checkPGCodeForRecall(req,res,9,data,PGData,tblName);
               }
            }
        });
} 


function checkPGCodeForRecall(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   
   datautils.checkPGCodeForRecall(tblName,PGData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                if(result.errorCode != 0){
                debuglog("Checking of PGId has failed");
                debuglog(result);
                processStatus(req,res,result.errorCode,data);
                }
                else{
                debuglog("Checking of PGId priority is successful");
                debuglog("So we will resequence the Other PG now");
                debuglog(result);
                PGData.releasedPG = result.PGS;
                checkPGPriority(req,res,9,data,PGData,tblName);
               }
            }
        });
} 

function checkPGPriority(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
   if(PGData.releasedPG.length == 1){
     processStatus(req,res,5,data);
     debuglog("Since only one PG is released we dont allow business to Recall it");
     debuglog("As otherwise customers cant pay in Cart");
     debuglog(PGData);
   }
   else{
    async.forEachOf(PGData.releasedPG,function(key,value,callback){
     debuglog("Now checking for PG----");
     debuglog(key);
     debuglog(PGData.thisPriority);
     if(key.priority <= PGData.thisPriority) {
      debuglog("This is less than the recalled PG so no change");
      debuglog("Or may be this is the PG that is being recalled");
      callback();
     }
     else{
      key.priority--;
      debuglog("Priority is reduced by one to resequence");
      debuglog(key);
      callback();
      }
     },
     function(err){
       if(err)
        processStatus(req,res,6,data);
      else{
       debuglog("Now we will update the remaining PGS with resequenced priority");
       debuglog(PGData);
       changePGPriorities(req,res,stat,data,PGData,tblName);
      }
     });
    }
} 


function changePGPriorities(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
   async.forEachOf(PGData.releasedPG,function(key,value,callback){
     debuglog("Now changing priority for PG----");
     debuglog(key)
     datautils.changePGPriority(tblName,key,function(err,result) {
         if(err) {
               isError = 1;
               callback();
             }
            else 
              callback();
        });
      },
     function(err){
       if(err || isError == 1)
        processStatus(req,res,6,data);
      else{
       debuglog("Now we will update the recalled PGS ");
       debuglog(PGData);
       changePGStatus(req,res,stat,data,PGData,tblName);
      }
     });
} 


function changePGStatus(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,data.pgId,86,'Payment Gateway Recalled',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getPGData(req,res,9,data,PGData,tblName);
               }
        });
} 

function getPGData(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
      var PGArr = [];
      PGArr.push(data.pgId);
      datautils.PGSData(tblName,PGArr,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
             debuglog("After getting PG Data");
             debuglog(result); 
             pgs = result;
             getPGStatusName(req,res,9,data,pgs);
           }
        }); 
} 

function getPGStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('pg_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.pgData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
   
exports.getData = processRequest;


