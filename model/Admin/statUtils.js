function changeStatus(tablName,orgId,bizType,operatorId,pgId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {pgId: pgId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createStatusChngLog(orgId,bizType,operatorId,pgId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createStatusChngLog(orgId,bizType,operatorId,pgId,stat,reason,callback){
 
  var promise = db.get('pg_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    pgId: pgId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }
 

 function changeDeliveryStatus(tablName,orgId,bizType,operatorId,deliveryId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {deliveryId: deliveryId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createDelStatusChngLog(orgId,bizType,operatorId,deliveryId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createDelStatusChngLog(orgId,bizType,operatorId,deliveryId,stat,reason,callback){
 
  var promise = db.get('delivery_option_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    deliveryId: deliveryId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }

function changeTaxStatus(tablName,orgId,bizType,operatorId,taxId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {taxId: taxId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createTaxStatusChngLog(orgId,bizType,operatorId,taxId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createTaxStatusChngLog(orgId,bizType,operatorId,taxId,stat,reason,callback){
 
  var promise = db.get('tax_type_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    taxId: taxId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }

exports.changeStatus = changeStatus;
exports.changeDelStatus = changeDeliveryStatus;
exports.changeTaxStatus = changeTaxStatus;


