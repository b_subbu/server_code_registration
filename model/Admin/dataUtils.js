function saveBusinessDetails(uid,userDets,callback){

 debuglog("This is same as in Registration dataUtils updateUserDetails");
 debuglog("But this does not update password and orgId since it is only in registration flow");
 debuglog("And some non editable fields");
 
 var promise = db.get('user_detail').update({_id:uid},
  { $set:{
    user_id: uid,
    bus_name: userDets.displayName,
    bus_addr: userDets.businessAddress,
    bus_mobile: userDets.businessMobile,
    bus_url: userDets.website,
    bus_logo: userDets.businessLogo,
    bus_descr: userDets.description,
    country_id: userDets.country,
    state_id: userDets.state,
    city_id: userDets.city,
    street: userDets.street,
    building: userDets.building,
    zip: userDets.zipCode,
    lat: parseFloat(userDets.latitude),
    longi: parseFloat(userDets.longitude),
    currency_id: userDets.baseCurrency,
    vat_number: userDets.vatNumber,
    tin_number: userDets.tinNumber,
    cst_number: userDets.cstNumber,
	baseCurrency: userDets.baseCurrency
    }},
  {upsert: true}
);
  promise.on('success',function(doc){
   callback(null,null)
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function getPGCount(orgId,bizType,tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}


//TODO see if we can combine these in common dataUtils by getting
//column names from productHeader
function getPGHeader(tblName,orgId,bizType,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
     var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {pgId: doc.pgId,
                        name:doc.name,
                        type:doc.gatewayType.label,
                        status:doc.status_id,
                        statusId:doc.status_id,
                        priority:doc.gatewayPriority,
                        date: moment(doc.update_time).format('L')};
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.gateways = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.gateways = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getPGData(tblName,PGData,callback){
   var totalRows =0;
   var data = {};
  
  var promise = db.get(tblName).find({pgId:PGData.pgId});
   promise.each(function(doc){
        totalRows++;
        data.gatewayId = doc.gatewayId;
        data.name = doc.name;
        data.gatewayType = doc.gatewayType;
        data.gatewayPriority = doc.gatewayPriority;
        data.key = doc.key;
        data.salt = doc.salt;
        data.paymentTypes   = doc.paymentTypes;
        data.defaultPaymentType = doc.defaultPaymentType;
        data.paymentSubTypes = doc.paymentSubTypes;
        data.customMessage = doc.customMessage;
        data.statusId = doc.status_id;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getPGMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  
  var promise = db.get(tblName).find({},{sort:{gateway_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.gatewayId   = doc.gateway_id;
        tmpArr.gatewayName = doc.gateway_name;
        tmpArr.gatewayType = doc.gateway_type;
        tmpArr.paymentTypes = doc.payment_types;
        tmpArr.paymentSubTypes = doc.payment_sub_types;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function checkPGCodeMain(PGData,callback){
 var tblName = PGData.orgId+'_payment_gateway';
 var revisiontblName = PGData.orgId+'_Revision_payment_gateway';
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
       debuglog("Check Count in Revision also because it is possible");
       debuglog("user might have directly submitted in Create");
       checkPGCodeRevision(doc,revisiontblName,callback);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkPGCodeRevision(activeCount,tblName,callback){
 
 var promise = db.get(tblName).count({});
 promise.on('complete',function(err,doc){
      debuglog("Count from revision table and active table are");
      debuglog(doc);
      debuglog(activeCount); 
      var totalCount = doc+activeCount;
       debuglog("So the sum of 2 is");
       debuglog("This is the total of PGS created by this business");
       debuglog("Since release & edit is not possible also delete is not possible");
       debuglog("can take next PGId to be incr of sum total");
       debuglog("Otherwise change this logic");
       debuglog(totalCount);
       totalCount++;
       debuglog("Will increment by 1");
       debuglog(totalCount);
       pgId = "PG-"+totalCount;
       debuglog("And the new PG Id is");
       debuglog(pgId);
       callback(null,pgId);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

//We are using update for both create and Edit 
function updatePGData(tblName,pgId,pgData,callback){
    var crypto = require('crypto'),
      algorithm = 'aes-256-ctr',
       password = 'd6F3Efeq';
  
   var key = crypto.createHash("sha256").update(password, "ascii").digest();
   var iv = "1234567890123456";
   var secretKey  = pgData.gateway.paymentGatewayParams.key;
   var secretSalt = pgData.gateway.paymentGatewayParams.salt;
   
  if(secretKey != null && secretSalt != null){
var cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
cipher.update(secretKey, "ascii");
var secretKey = cipher.final("hex");

var cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
cipher.update(secretSalt, "ascii");
var secretSalt = cipher.final("hex");
  
var promise = db.get(tblName).update(
  {pgId:pgId},
  {$set: {gatewayId: pgData.gateway.gatewayId,
          name: pgData.gateway.gatewayName,
          gatewayPriority: pgData.gateway.gatewayPriority,
          gatewayType: pgData.gateway.gatewayType,
          status_id: pgData.status_id,
          key: secretKey,
          salt: secretSalt,
          paymentTypes: pgData.gateway.paymentGatewayParams.paymentTypes,
          paymentSubTypes: pgData.gateway.paymentGatewayParams.paymentSubTypes,
          defaultPaymentType: pgData.gateway.paymentGatewayParams.defaultPaymentType,
          customMessage: pgData.gateway.paymentGatewayParams.customMessage, 
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
    }
  else{
 var promise = db.get(tblName).update(
  {pgId:pgId},
  {$set: {gatewayId: pgData.gateway.gatewayId,
          name: pgData.gateway.gatewayName,
          gatewayPriority: pgData.gateway.gatewayPriority,
          gatewayType: pgData.gateway.gatewayType,
          status_id: pgData.status_id,
          paymentTypes: pgData.gateway.paymentGatewayParams.paymentTypes,
          paymentSubTypes: pgData.gateway.paymentGatewayParams.paymentSubTypes,
          defaultPaymentType: pgData.gateway.paymentGatewayParams.defaultPaymentType,
          customMessage: pgData.gateway.paymentGatewayParams.customMessage, 
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
 
  
  }
}

function getPaymentGatewaysData(tblName,pgIds,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment=require('moment');
  
   var promise = db.get(tblName).find({pgId: {$in: pgIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {pgId: doc.pgId,
                        name:doc.name,
                        type:doc.gatewayType.label,
                        status:doc.status_id,
                        statusId:doc.status_id,
                        priority:doc.gatewayPriority,
                        date: moment(doc.update_time).format('L')};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function movePGHeader(srcTable,destTable,pgId,callback){

var promise1 = db.get(destTable).remove({pgId:pgId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({pgId:pgId});
  
  //there must be only one row per groupCode
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({pgId:pgId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function copyPGHeader(srcTable,destTable,pgId,callback){

var promise1 = db.get(destTable).remove({pgId:pgId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({pgId:pgId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
           callback(null,null);
        });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function checkPGCode(tblName,pgData,callback){
var totalRows = 0;
var data = {};
var priority = 0;

var promise = db.get(tblName).find({pgId: pgData.gateway.pgId});

promise.each(function(doc){
   totalRows++;
   priority = doc.gatewayPriority;
   status   = doc.status_id;
});

promise.on('complete',function(err,doc){

 if(totalRows != 1){
   data.errorCode = 3;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    data.priority = priority;
    data.status   = status;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function checkPGCodeForRelease(tblName,pgData,callback){
var totalRows = 0;
var data = {};
var tmpArr = [];

var promise = db.get(tblName).find({status_id:84});

promise.each(function(doc){
   totalRows++;
  tmpArr.push(doc.gatewayPriority);
});

promise.on('complete',function(err,doc){

 if(totalRows == 0){
   data.errorCode = 0;
   data.priority  = 0;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    var maxPriority = Math.max.apply(Math,tmpArr);
    data.priority = maxPriority;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function checkPGCodeForRecall(tblName,pgData,callback){
var totalRows = 0;
var data = {};
var resultArr =[];

var promise = db.get(tblName).find({status_id:84});

promise.each(function(doc){
   totalRows++;
   var tmpArr = {pgId:doc.pgId, priority: doc.gatewayPriority}
   resultArr.push(tmpArr);
});

promise.on('complete',function(err,doc){

 if(totalRows == 0){
   data.errorCode = 6;
   debuglog("Probably some one already recalled this PG?");
   debuglog("Otherwise how this condition is possible?");
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    data.PGS = resultArr;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function changePGPriority(tblName,pgData,callback){
 var promise = db.get(tblName).update({pgId:pgData.pgId},
  { $set:{gatewayPriority:pgData.priority}},
  {upsert: false}
  );
  promise.on('success',function(doc){
   callback(null,null)
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function deletePGRow(tblName,pgId,callback){
  
var promise = db.get(tblName).remove({pgId:pgId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getDelCount(tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getDelMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  
  var promise = db.get(tblName).find({},{sort:{deliveryId:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.deliveryId = doc.delivery_id;
        tmpArr.deliveryName = doc.delivery_name;
        tmpArr.enabled = doc.enabled;
        tmpArr.payOptions = doc.pay_options;
        tmpArr.workQueue  = doc.work_queue;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getDelData(tblName,DelData,callback){
   var totalRows =0;
   var data = {};
   debuglog("start getting delivery data");
   debuglog(DelData);
   
  var promise = db.get(tblName).find({deliveryId:DelData.deliveryId});
   promise.each(function(doc){
        totalRows++;
        data.deliveryId = doc.deliveryId;
        data.deliveryName = doc.deliveryName;
        data.enabled = doc.enabled;
        data.payOptions = doc.payOptions;
        data.workQueue  = doc.workQueue;
        data.status = doc.status_id;
        data.statusId = doc.status_id;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function checkDelCode(tblName,delData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("We are checking only in Main table");
debuglog("Any row directly submitted is not problem");
debuglog("since it will just overwrite main row when authorised");
debuglog(delData);

var promise = db.get(tblName).find({deliveryId: delData.delivery.deliveryId});
promise.each(function(doc){
   status = doc.status_id;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 5;
   data.status = status;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

//We are using update for both create and Edit 
function updateDelData(tblName,deliveryId,delData,callback){
  
var promise = db.get(tblName).update(
  {deliveryId:deliveryId},
  {$set: {deliveryId: delData.delivery.deliveryId,
          deliveryName: delData.delivery.deliveryName,
          enabled: delData.delivery.enabled,
          payOptions: delData.delivery.payOptions,
          workQueue: delData.delivery.workQueue,
          status_id: delData.status_id,
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getDelsData(tblName,deliveryIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var promise = db.get(tblName).find({deliveryId: {$in: deliveryIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {deliveryId: doc.deliveryId,deliveryName:doc.deliveryName,status:doc.status_id,statusId:doc.status_id,enabled:doc.enabled};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function moveDelHeader(srcTable,destTable,deliveryId,callback){

var promise1 = db.get(destTable).remove({deliveryId:deliveryId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({deliveryId:deliveryId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({deliveryId:deliveryId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function copyDelHeader(srcTable,destTable,deliveryId,callback){

debuglog("Same as move but dont delete from source table");
var promise1 = db.get(destTable).remove({deliveryId:deliveryId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({deliveryId:deliveryId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
            callback(null,null);
           });
      promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function getDelHeader(tblName,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
     
      var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {deliveryId: doc.deliveryId,
                        deliveryName:doc.deliveryName,
                        status:doc.status_id,
                        enabled:doc.enabled,
                        statusId:doc.status_id};
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.gateways = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.delOptions = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function deleteDeliveryRow(tblName,deliveryId,callback){
  
var promise = db.get(tblName).remove({deliveryId:deliveryId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getTaxCount(tablName,sarr,callback){
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getTaxHeader(tblName,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
     
     var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {taxId: doc.taxId,
                        taxName:doc.taxName,
                        enabled: doc.enabled,
                        status:doc.status_id,
                        statusId:doc.status_id
                      };
                      debuglog("Tmp array is:");
                      debuglog(tmpArray);
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.taxOptions = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         debuglog("return final array:");
         debuglog(resultArray);
         data.taxOptions = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getTaxMaster(tblName,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
  
  var promise = db.get(tblName).find({},{sort:{tax_id:1}});
   promise.each(function(doc){
        var tmpArr = {};
        tmpArr.taxId = doc.tax_id;
        tmpArr.taxName = doc.tax_name;
        tmpArr.enabled = doc.enabled;
        tmpArr.label = doc.tax_label;
        resultArr.push(tmpArr);
  }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getTaxData(tblName,TaxData,callback){
   var totalRows =0;
   var data = {};
   debuglog("start getting Tax data");
   debuglog(TaxData);
   
  var promise = db.get(tblName).find({taxId:TaxData.taxId});
   promise.each(function(doc){
        totalRows++;
        data.taxId = doc.taxId;
        data.taxName = doc.taxName;
        data.enabled = doc.enabled;
        data.taxLabel = doc.taxLabel;
        data.description = doc.description; 
        data.status = doc.status_id;
        data.statusId = doc.status_id;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function checkTaxCode(tblName,TaxData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("We are checking only in Main table");
debuglog("Any row directly submitted is not problem");
debuglog("since it will just overwrite main row when authorised");
debuglog(TaxData);

var promise = db.get(tblName).find({taxId: TaxData.tax.taxId});
promise.each(function(doc){
   status = doc.status_id;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 5;
   data.status = status;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

//We are using update for both create and Edit 
function updateTaxData(tblName,taxId,taxData,callback){
  
var promise = db.get(tblName).update(
  {taxId:taxId},
  {$set: {taxId: taxData.tax.taxId,
          taxName: taxData.tax.taxName,
          enabled: taxData.tax.enabled,
          taxLabel: taxData.tax.label,
          description: taxData.tax.description,
          status_id: taxData.status_id,
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getTaxesData(tblName,taxIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var promise = db.get(tblName).find({taxId: {$in: taxIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {taxId: doc.taxId,taxName:doc.taxName,statusId:doc.status_id,status:doc.status_id,enabled:doc.enabled};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}

function moveTaxHeader(srcTable,destTable,taxId,callback){

var promise1 = db.get(destTable).remove({taxId:taxId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({taxId:taxId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({taxId:taxId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function copyTaxHeader(srcTable,destTable,taxId,callback){

var promise1 = db.get(destTable).remove({taxId:taxId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({taxId:taxId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
           callback(null,null);
        });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function deleteTaxRow(tblName,taxId,callback){
  
var promise = db.get(tblName).remove({taxId:taxId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getSellerCount(tblName,callback){
debuglog("We are getting here counts of branches and sellers");
debuglog("Seller table will also have 1 row with type Main which is of the registered business");
debuglog("This will be considered for Seller count");
debuglog("Looks like aggregate/groupby is still not available in monk");

var totalBranches = 0;
var totalSellers  = 0;
var totalRows = 0;
var data = {};

var promise = db.get(tblName).find({});
   
   promise.each(function(doc){
        totalRows++;
        if (doc.seller_type == 'Main') totalSellers++;
        if (doc.seller_type == 'Branch') totalBranches++;
        if (doc.seller_type == 'Seller') totalSellers++;
        
        //note the keys must match to name as defined in Tab header for the given product+tab
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       data.totalBranches = 0;
       data.totalSellers = 0;
       callback(null,data);
       }
      else {
         data.errorCode = 0; 
         data.totalBrancehs = totalBranches;
         data.totalSellers = totalSellers;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}


function updateSellerDetail(uid,userDets,tblName,callback){

 debuglog("This is same as in saveBusines Details");
 debuglog("But this does not update password and orgId since sellers dont have them");
 debuglog("And some non editable fields");
 debuglog("Also this row is always created with enabled=true so that it cant be edited in seller tab");
 debuglog("Plus its status will be authorised so it can't be deleted also");
 debuglog("Because it was changed from editable to readonly so false became true");
 
 var promise = db.get(tblName).update({sellerId:userDets.sellerId},
  { $set:{
    sellerId: userDets.sellerId,
    sellerType: userDets.sellerType,
    bus_name: userDets.displayName,
    bus_addr: userDets.businessAddress,
    bus_email: userDets.businessEmail,
    bus_mobile: userDets.businessMobile,
    bus_url: userDets.website,
    bus_logo: userDets.businessLogo,
    bus_descr: userDets.description,
    country_id: userDets.country,
    state_id: userDets.state,
    city_id: userDets.city,
    street: userDets.street,
    building: userDets.building,
    zip: userDets.zipCode,
    lat: parseFloat(userDets.latitude),
    longi: parseFloat(userDets.longitude),
    currency_id: userDets.baseCurrency,
	cin_number: userDets.cinNumber,
    vat_number: userDets.vatNumber,
    tin_number: userDets.tinNumber,
    cst_number: userDets.cstNumber,
	enabled: true,
	status_id: userDets.status_id
    }},
  {upsert: true}
);
  promise.on('success',function(doc){
   callback(null,null)
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}


function getWorkQueue(tblName,orgId,deliveryId,callback){
var workQueue = {};
var totalRows = 0;

var promise = db.get(tblName).find({deliveryId: deliveryId});
promise.each(function(doc){
   workQueue = doc.workQueue;
   totalRows++;
});
promise.on('complete',function(err,doc){
 if(totalRows == 1){
   var workQueueId =  workQueue.id;
   var workQueueName = workQueue.name;
   var tblName = orgId+'_work_queue';
   createWorkQueue(tblName,workQueueId,workQueueName,callback);
   }
  else{
   callback(null,null); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});
debuglog("This is a tricky function");
debuglog("Data is being moved from Revision to Main table");
debuglog("So data must be read from Revision table before movement");
debuglog("Or main table after movement");
debuglog("So dont do error handling for now");

}

function createWorkQueue(tblName,wqId,wqName,callback){

 debuglog("It may be possible that the work queue already exists");
 debuglog("By previous authorisations so just use upsert true");
 debuglog("As of now there is no seperate authorization or status for workqueues");
 debuglog("only workQueueId here must match with work_queue_master work_queue_id");
 
 var promise = db.get(tblName).update({workQueueId:wqId},
  { $set:{
    workQueueName: wqName
  }},
  {upsert: true}
  );
  promise.on('success',function(doc){
   callback(null,null)
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
  
}

function isPGTested(tblName,PGData,callback){
   var totalRows =0;
   var data = {};
   
   debuglog("Status must be  released");
   debuglog(PGData);
  
  var promise = db.get(tblName).find({
     $and:[{name:PGData.gateway.gatewayName},
           {'gatewayType.label': 'Test'},
           {status_id: 84}
           ]});
   promise.each(function(doc){
        totalRows++;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function isPGExist(tblName,PGData,callback){
   var totalRows =0;
   var data = {};
   
   debuglog("Check if same PG name+type exists");
   debuglog(PGData);
  
  var promise = db.get(tblName).find({
     $and:[{name:PGData.gateway.gatewayName},
           {'gatewayType.label': PGData.gateway.gatewayType.label}
           ]});
   promise.each(function(doc){
        totalRows++;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 1) {
       data.errorCode = 1; //yes, exist
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getGatewayId(PGData,callback){

  var totalRows =0;
   var data = {};
   var gatewayId = 0;
  
  var tblName = 'payment_gateways_master'; 
   debuglog("Get GAteway ID based on name and type passed in:");
   debuglog(PGData);
  
  var promise = db.get(tblName).find({
     $and:[{gateway_name:PGData.gateway.gatewayName},
           {gateway_type: PGData.gateway.gatewayType.label}
           ]});
   promise.each(function(doc){
        totalRows++;
        gatewayId = doc.gateway_id;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.gatewayId = gatewayId;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function removeWorkQueue(tblName,orgId,deliveryId,callback){
var workQueue = {};
var totalRows = 0;

var promise = db.get(tblName).find({deliveryId: deliveryId});
promise.each(function(doc){
   workQueue = doc.workQueue;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   var workQueueId =  workQueue.id;
   var workQueueName = workQueue.name;
   var tblName = orgId+'_work_queue';
   removeWorkQueueEntry(tblName,workQueueId,workQueueName,callback);
   }
  else{
   callback(null,null); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});
}

function removeWorkQueueEntry(tblName,wqId,wqName,callback){
 var promise = db.get(tblName).remove({workQueueId:wqId});
  promise.on('success',function(doc){
   debuglog("Work queue removed");
   callback(null,null);
  });
  promise.on('error',function(err){
   debuglog(err);
   callback(6,null);
  });
  
}


exports.saveBusinessDetails =  saveBusinessDetails;
exports.PGCount = getPGCount;
exports.PGHeader = getPGHeader;
exports.PGData   = getPGData;
exports.PGMaster = getPGMaster;
exports.generatePGCode = checkPGCodeMain;
exports.PGUpdate = updatePGData;
exports.PGSData = getPaymentGatewaysData;
exports.movePGHeader = movePGHeader;
exports.checkPGCode = checkPGCode;
exports.checkPGCodeForRelease = checkPGCodeForRelease;
exports.checkPGCodeForRecall  = checkPGCodeForRecall;
exports.changePGPriority = changePGPriority;
exports.DelCount = getDelCount;
exports.DelMaster = getDelMaster;
exports.DelData = getDelData;
exports.DelUpdate = updateDelData;
exports.DelsData = getDelsData;
exports.moveDelHeader = moveDelHeader;
exports.checkDelCode = checkDelCode;
exports.DelHeader = getDelHeader;
exports.deleteDelCode = deleteDeliveryRow;
exports.TaxCount = getTaxCount;
exports.TaxMaster = getTaxMaster;
exports.TaxData = getTaxData;
exports.TaxUpdate = updateTaxData;
exports.TaxesData = getTaxesData;
exports.moveTaxHeader = moveTaxHeader;
exports.checkTaxCode = checkTaxCode;
exports.TaxHeader = getTaxHeader;
exports.deleteTaxCode = deleteTaxRow;
exports.sellerDets = getSellerCount;
exports.saveSellerDetails = updateSellerDetail;
exports.workQueue   = getWorkQueue;
exports.checkPGTested = isPGTested;
exports.gatewayID = getGatewayId;
exports.checkPGExist = isPGExist;
exports.copyDelHeader = copyDelHeader;
exports.removeWorkQueue = removeWorkQueue;
exports.copyTaxHeader = copyTaxHeader;
exports.copyPGHeader  = copyPGHeader;
exports.deletePGCode = deletePGRow;



