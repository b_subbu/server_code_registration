function processRequest(req,res,data){
       checkPGCode(req,res,9,data);
}

function checkPGCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var PGData = extend({},data);
   PGData.gateway = req.body.paymentGatewayDetails;
   
    var tblName = data.orgId+'_payment_gateway'; //edit will always be in main table
   
     
  
      datautils.checkPGCode(tblName,PGData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                if(result.errorCode != 0){
                debuglog("Checking of PGId has failed");
                debuglog(result);
                data.pgId = PGData.gateway.pgId;
                processStatus(req,res,result.errorCode,data);
                }
                else{
                debuglog("Checking of PGId is successfule");
                debuglog("So we will save the params now");
                debuglog(result);
                data.pgId = PGData.gateway.pgId;
                getGatewayID(req,res,9,data,PGData,tblName);
               }
            }
        });
} 

function getGatewayID(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
      datautils.gatewayID(PGData,function(err,result) {
         if(err ) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
              if(result.errorCode == 1){
              debuglog("Could not get a master entry for the given name+type");
              debuglog("possibly Frontend did not pass name or type correctly");
              processStatus(req,res,4,data);
              
              }
              else{
                PGData.gateway.gatewayId = result.gatewayId;
                debuglog("Got gateway Id ok");
                debuglog(PGData);
                savePGData(req,res,9,data,PGData,tblName);
                }
            }
        });
} 

function savePGData(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
  
     debuglog("we are not saving urls in payment gateway table");
     debuglog("That has to get from master table");
     debuglog("If it becomes slow then can directly save here but only changes can be an issue");
     debuglog(PGData);
     debuglog(data);
     
    var submitop = req.body.submitOperation;
  
   
    if(   PGData.gateway.gatewayPriority == null
       || PGData.gateway.gatewayPriority == 'undefined'
       || PGData.gateway.gatewayPriority == 0 
      )
        processStatus(req,res,4,data);
    else{
     datautils.PGUpdate(tblName,data.pgId,PGData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
             debuglog("saved PG data");
             debuglog(PGData);
             if(submitop == 1){
               var pgIds = [];
               pgIds.push(data.pgId);
               data.pgIds = pgIds;
               var submitPG = require('./submitPaymentGateways');
               submitPG.moveToRevision(req,res,9,data);
             }
             else
                changePGStatus(req,res,9,data,PGData,tblName);
            }
          });
        }
} 


function changePGStatus(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,data.pgId,80,'Payment Gateway Edited',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getPGData(req,res,9,data,PGData,tblName);
               }
        });
} 

function getPGData(req,res,stat,data,PGData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
      var PGArr = [];
      PGArr.push(data.pgId);
      datautils.PGSData(tblName,PGArr,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
               }
          else {
             debuglog("After getting PG Data");
             debuglog(result); 
             pgs = result;
             getPGStatusName(req,res,9,data,pgs);
           }
        }); 
} 

function getPGStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('pg_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.pgData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
   
exports.getData = processRequest;









