function processRequest(req,res,data){
   if( typeof req.body.pgIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkPGStatus(req,res,9,data);
}


function checkPGStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var PGArr = req.body.pgIds;
    
    data.userId = uid;
    
    var enteredItems = [];
    var authorisedItems = [];
  
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.pgIds = PGArr;
    
    var orgId = data.orgId;
    
    var tblName = orgId+'_payment_gateway'; //TODO: move to a common definition file

    async.forEachOf(PGArr,function(key,value,callback1){
      var pgData= {};
          pgData.gateway = {};
          pgData.gateway.pgId = key;
         datautils.checkPGCode(tblName,pgData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 0)
               isError = result.errorCode;
                else{
                  if (result.status == 80){
                   debuglog("This is entered status item");
                   debuglog("So can be deleted directly");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 82){
                   debuglog("This is authorised status item");
                   debuglog("So has to go through authoristation to delete");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Delete is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();          
            }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else {
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              changePGStatus(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
               }
           }
      });
}


function changePGStatus(req,res,stat,data,enteredItems,authorisedItems,tblName){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(enteredItems,function(key,value,callback1){
    pgId = key;
  
    statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,pgId,85,'Payment Gateway Deleted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                  }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           if(authorisedItems.length > 0){
             debuglog("This request has some authorised items too");
             debuglog(authorisedItems);
             deleteAuthorisePG(req,res,9,data,authorisedItems);
          }
          else{
              debuglog("This request has no authorised items");
              debuglog("so proceed further");
              getPGDetails(req,res,9,data,tblName);
             }
          }
          });
     
}

function deleteAuthorisePG(req,res,stat,data,authorisedItems){
    var uid = req.body.userId;
   
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    debuglog("Start removing authorised items");
    debuglog(authorisedItems);
    var mainTable    = data.orgId+'_payment_gateway';
    var revisionTable = data.orgId+'_Revision_payment_gateway';
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
    debuglog("In for each loop");
    debuglog(key);
      PGId = key;
     async.parallel({
          relStat: function (callback){datautils.copyPGHeader(mainTable,revisionTable,PGId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           debuglog("Change status of items in revision table to ready2Delete");
           changePGStatusDelete(req,res,9,data,authorisedItems,mainTable,revisionTable);
          }
      });
}


function changePGStatusDelete(req,res,stat,data,PGArr,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(PGArr,function(key,value,callback1){
    pgId = key;
    var reason = 'Delete Payment Gateway submitted';
     statutils.changeStatus(revisionTable,data.orgId,data.businessType,data.userId,pgId,89,reason,function(err,result) {
         if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getPGDetails(req,res,9,data,revisionTable);
             }
          });
     
}



function getPGDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.PGSData(mainTable,data.pgIds,function(err,result) {
       if(err) {
                processStatus(req,res,6,data);
              }
      else  {
                getPGStatusName(req,res,9,data,result); 
            }
     });
    
}

function getPGStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('pg_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.pgData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
   
exports.getData = processRequest;



