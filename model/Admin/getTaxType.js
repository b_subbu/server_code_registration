function processRequest(req,res,data){
    if(typeof req.body.taxId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getTaxData(req,res,9,data);
}


function getTaxData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.taxId = req.body.taxId;
    var tid = req.body.tabId;
    
    if(tid == 501)
     var tabl_name = data.orgId+'_tax_type';
    else
     var tabl_name = data.orgId+'_Revision_tax_type';
   
     var extend = require('util')._extend;
     var TaxData = extend({},data);
     
  
      datautils.TaxData(tabl_name,TaxData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              getTaxTypes(req,res,3,data);
              isError = 1;
              //return;
              debuglog("result errorCode is 1");
              }
            else {
                var taxTypeDetails = {};
                taxTypeDetails.taxId = data.taxId;
                taxTypeDetails.taxName = result.taxName;
                taxTypeDetails.enabled = result.enabled;
                taxTypeDetails.label   = result.taxLabel;
                taxTypeDetails.description = result.description;
                taxTypeDetails.status       = result.status;
                taxTypeDetails.statusId     = result.statusId;   
                
                data.taxTypeDetails = taxTypeDetails;
                getTaxTypes(req,res,9,data);
                debuglog("No error case");
                debuglog(taxTypeDetails);
            }
          }
        });
} 


function getTaxTypes(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');

 var tabl_name = 'tax_types_master';
 
  datautils.TaxMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.taxTypes = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getData = processRequest;









