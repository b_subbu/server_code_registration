function processRequest(req,res,data){
       getTaxCode(req,res,9,data);
}


function getTaxCode(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var TaxData = extend({},data);
       TaxData.tax = req.body.taxTypeDetails;
       TaxData.status_id = 80;
      
     var tblName = data.orgId+'_tax_type'; //create will always be in main table
     debuglog("Check if the  Id exists or not");
     debuglog(TaxData);
     debuglog(req.body);
      datautils.checkTaxCode(tblName,TaxData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
               if(result.errorCode != 0) 
                 processStatus(req,res,result.errorCode,data);
               else
                saveTaxData(req,res,9,data,TaxData,tblName);
            }
        });
} 

function saveTaxData(req,res,stat,data,TaxData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
     debuglog("This is after getting data from request");
     debuglog(TaxData);
     debuglog(data);
     
     data.taxId = TaxData.tax.taxId;
     
    var submitop = req.body.submitOperation;
  
   
    if(   TaxData.tax.enabled == null
       || TaxData.tax.enabled == 'undefined'
       || TaxData.tax.taxName == null
       || TaxData.tax.taxName == 'undefined'
       || TaxData.tax.taxId == null
       || TaxData.tax.taxId == 'undefined'
      )
        processStatus(req,res,4,data);
    else{
    
     datautils.TaxUpdate(tblName,data.taxId,TaxData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(TaxData);
             if(submitop == 1){
             var taxIds = [];
             taxIds.push(data.taxId);
             data.taxIds = taxIds;
               var submitTax = require('./submitTaxTypes');
               submitTax.moveToRevision(req,res,9,data);
             }
             else{
                changeTaxStatus(req,res,9,data,TaxData,tblName);
                }
            }
        });
        }
} 


function changeTaxStatus(req,res,stat,data,TaxData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeTaxStatus(tblName,data.orgId,data.businessType,data.userId,data.taxId,80,'Tax Type Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getTaxData(req,res,9,data,TaxData,tblName);
               }
        });
} 

function getTaxData(req,res,stat,data,TaxData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
    
    var taxArr = [];
    taxArr.push(data.taxId) 
   datautils.TaxesData(tblName,taxArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getTaxStatusName(req,res,9,data,result); 
         }
     });
    
}

function getTaxStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('tax_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.taxTypeDetails = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 



function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getData = processRequest;










