function processRequest(req,res,data){
   if(typeof req.body.pgIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkPGStatus(req,res,9,data);
}

function checkPGStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var PGArr = req.body.pgIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.pgIds = PGArr;
    
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_Revision_payment_gateway'; //TODO: move to a common definition file

    async.forEachOf(PGArr,function(key,value,callback1){
      var pgData= {};
          pgData.gateway = {};
          pgData.gateway.pgId = key;
        datautils.checkPGCode(tabl_name,pgData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 0)
               isError = result.errorCode;
               if (result.status == 81){
                   debuglog("This is entered status item");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 89){
                   debuglog("This is authorised delete item");
                   debuglog("So change status to deleted");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
             debuglog("Since Authorise is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToMain(req,res,9,data,enteredItems,authorisedItems);
           }
      });
}



function moveToMain(req,res,stat,data,enteredItems,authorisedItems){
    
    
    debuglog("Now start moving the non-authorised entries to Main Table");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_payment_gateway'; //TODO: move to a common definition file
    var mainTable    = orgId+'_payment_gateway';
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      pgId = key;
     async.parallel({
          relStat: function (callback){datautils.movePGHeader(revisionTable,mainTable,pgId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changePGStatus(req,res,9,data,enteredItems,authorisedItems,mainTable,revisionTable); 
         }
     });
}


function changePGStatus(req,res,stat,data,enteredItems,authorisedItems,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(enteredItems,function(key,value,callback1){
    pgId = key;
  
    statutils.changeStatus(mainTable,data.orgId,data.businessType,data.userId,pgId,87,'Payment Gateway Rejected',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                  }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
            if(authorisedItems.length > 0){
             debuglog("This request has some authorised items too");
             debuglog(authorisedItems);
             deleteAuthorisePG(req,res,9,data,authorisedItems,mainTable,revisionTable);
            }
           else{
              debuglog("This request has no authorised items");
              debuglog("so proceed further");
              getPGDetails(req,res,9,data,mainTable);
              }
            }
          });
     
}

function deleteAuthorisePG(req,res,stat,data,authorisedItems,mainTable,revisionTable){
    
    
    debuglog("Now start deleting the entries in Revision Table");
    debuglog("Otherwise we will have 2 rows in main table with same id but different status");
    debuglog(authorisedItems);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    
    async.forEachOf(authorisedItems,function(key,value,callback1){
      pgId = key;
     async.parallel({
          relStat: function (callback){datautils.deletePGCode(revisionTable,pgId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
               processStatus(req,res,6,data);
            }
      else
          {
          changePGStatusDelete(req,res,9,data,authorisedItems,mainTable,revisionTable); 
         }
     });
}

function changePGStatusDelete(req,res,stat,data,authorisedItems,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    debuglog("Note here actual status change should not happen so the item will remain in same status");
    debuglog("Below is called only for AuditTrail");
  
    async.forEachOf(authorisedItems,function(key,value,callback1){
    pgId = key;
    var reason = 'Payment Gateway deletion Rejected';
       statutils.changeStatus(revisionTable,data.orgId,data.businessType,data.userId,pgId,87,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getPGDetails(req,res,9,data,mainTable);
             }
          });
     
}

function getPGDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.PGSData(mainTable,data.pgIds,function(err,result) {
       if(err) {
                processStatus(req,res,6,data);
              }
      else  {
                getPGStatusName(req,res,9,data,result); 
            }
     });
    
}

function getPGStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('pg_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.pgData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



