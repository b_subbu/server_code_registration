function getTaxTypesData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
  
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    var searchArr = [];
      
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
  
    if(tid == 501)
     var tblName = oid+'_tax_type';
    else
     var tblName = oid+'_Revision_tax_type';
  
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'tax_type_status_master',callback); }
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList,tblName);
                  }
                  else {
                      var searchArr = [];
                      var tmpArray = {status_id: {"$in":statList}};
                      searchArr.push(tmpArray);
                      debuglog(searchArr);
                      getTaxCount(req,res,stat,data,searchArr,tblName);
                    }
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList,tblName) {
   var async = require('async');
   var filterutils = require('./filterUtils');
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
      debuglog("Starting processing of header fields");
     debuglog("Note we are calling filter utils because here there are 2 filters");
     debuglog("TODO make Roles filter utils as generic and call it from everywhere");
     debuglog(key);
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.taxFilterOptions(key_name,statList,pid,tid,function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
               filterOptions[key_name] = results;
               callback();
            }
          }); 
        }
     else
        callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
            getTaxCount(req,res,stat,data,searchArr,tblName);
           }
         else 
         getTaxSearch(req,res,stat,data,statList,tblName);
         }
      });
} 


function getTaxSearch(req,res,stat,data,statList,tblName) {

 var searchInp = req.body.searchParams;

    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
   
 statusName = '';
 
 var minCount = 0;
 var maxCount = 0;
 
 var searchArr = [];
 //var userArray = {};
 //searchArr.push(userArray);
  
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
       case 'taxName':
        var tmpArray =  {deliveryName: {$regex:key,$options:'i'}}; ; 
        searchArr.push(tmpArray);
        break; 
       case 'enabled':
        var tmpArray = {enabled: key};
        searchArr.push(tmpArray);
        break; 
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           
            getTaxCount(req,res,9,data,searchArr,tblName);
            debuglog("We will start getting data with search Params");
            debuglog(searchArr);
          }
     });
}

function getTaxCount(req,res,stat,data,statList,tblName){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
    //have only tabl_name no need for orgId or bizType
   //since table name itself will have them      
   utils.TaxCount(tblName,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
           debuglog("After getting results");
           debuglog(results);
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getTaxTypes(req,res,9,data);
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getTaxData(req,res,9,data,statList,tblName); 
             }
            }
         });
}


function getTaxData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var utils = require('./dataUtils');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
   
      utils.TaxHeader(headerTable,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getTaxTypes(req,res,9,data); 
             //will be handled at count itself, just adding here
            }
            else {
            taxTypes = result.taxOptions;
            debuglog(taxTypes); 
            getTaxStatusName(req,res,9,data,taxTypes);
            }
          }
        });
} 


function getTaxStatusName(req,res,stat,data,taxTypes) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(taxTypes,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
        debuglog(key1);
           mastutils.masterStatusNames('tax_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.productData = taxTypes;
            getTaxTypes(req,res,9,data);
           }
      });
 

} 


function getTaxTypes(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');

 var tabl_name = 'tax_types_master';
 
  datautils.TaxMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.taxTypes = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
  
exports.getProductData = getTaxTypesData;








