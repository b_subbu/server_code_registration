function processRequest(req,res){
  var userdata = "";
  var files = "";
  var formidable = require('formidable');
  var form = new formidable.IncomingForm();
  form.uploadDir = app.get('fileuploadfolder')+'/tmp';
  form.parse(req, function (err, fields) {
    userdata = fields;
  });
  form.on('file',function(name,file) {
    files = file;
  });
  form.on('end',function() {
    checkMasterStatus(req,res,userdata,files);
    debuglog("checkMasterStatus is left as is to get values from multipart")
  });
}

function checkMasterStatus(req,res,userdata,files){
    var data = {};
    var uid = userdata.userId;
    var isError = 0;
    data.userId = uid;
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
  
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
        },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,1,data);
              isError = 1;
              return;
             }
             
          if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
			        data.baseCurrency = results.usrStat.baseCurrency;
              updateBusinessProfile(req,res,data,userdata,files);
            
            if(files)
               saveFile(req,res,data,userdata,files);
           }
          }); 
}

function saveFile(req,res,data,userdata,files) {
  var fs = require('fs');
  var mkdirp = require('mkdirp');
  var oldfile = files.path;
  var newfolder = app.get('fileuploadfolder')+userdata.userId+'/';
  var newfile = newfolder+files.name;
  mkdirp(newfolder,function(err,made){

    if(err) {
      console.log(err);
      fs.unlink(oldfile,function(err){
        if(err) {
          console.log("remove file failed");
          console.log(err);
        }
        return;
      });
    }

    else if(files.size == 0) {
      fs.unlink(oldfile,function(err){
        if(err) {
          console.log("remove empty file failed");
          console.log(err);
        }
        return;
      });
    }
    else {
        fs.rename(oldfile,newfile,function(err) {
        if(err) {
          console.log("Moving file failed");
          console.log(err);
        }
        return;
      });
    }
  });
}


function updateBusinessProfile(req,res,data,userdata,files) {

  debuglog("There can be only one file for this request");
  debuglog("If no files are uploaded then the logo will be empty");
  debuglog(files.size);
  if(files.size > 0){
  debuglog("This request has file uploaded");
  debuglog(files.name);
  var filename = files.name;  //there can be only one file
  var business_logo = '/upload_files/'+userdata.userId+'/'+encodeURIComponent(filename);
  }
  else
  var business_logo = userdata.businessLogo;
 
  var async = require('async');
  var datautils = require('./dataUtils');
  var userstatutils = require('../Users/statUtils');
  var isError = 0;
  var uid = userdata.userId;
  
  var cid =  userdata;
      cid.businessLogo = business_logo;
      cid.sellerId = 1;
      cid.sellerType = {id:11,name:'Main'};
	  cid.status_id = 82;
      
      debuglog("We also create a row in orgId_seller table");
      debuglog("Since this is the main business seller id will be 1 always");
      debuglog("and seller type as Main and its status will be authorised");
	  
	  debuglog("Call user status change with user id as 1 and status as authorised");
	  debuglog("This is used in Audit Trail for Company profile");
      
      var tblName = data.orgId+'_sellers';
	  var usrtblName = data.orgId+'_users';
      debuglog(tblName);
      
     async.parallel({
         usrStat: function(callback){datautils.saveBusinessDetails(uid,cid,callback); },
         sellerStat: function(callback){datautils.saveSellerDetails(uid,cid,tblName,callback); },
		 profileStat: function(callback){userstatutils.changeUserStatus(usrtblName,data.orgId,data.businessType,data.userId,1,84,'Company Profile Changed',callback);}
         },
         function(err,results) {
            if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
          else{
               processStatus(req,res,9,data);
            }
          });
 
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

