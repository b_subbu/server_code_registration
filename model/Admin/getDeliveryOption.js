function processRequest(req,res,data){
   if(typeof req.body.deliveryId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getDelData(req,res,9,data);
}


function getDelData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.deliveryId = req.body.deliveryId;
    var tid = req.body.tabId;
    
    if(tid == 501)
     var tabl_name = data.orgId+'_delivery_option';
    else
     var tabl_name = data.orgId+'_Revision_delivery_option';
   
     var extend = require('util')._extend;
     var DelData = extend({},data);
     
  
      datautils.DelData(tabl_name,DelData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              getDeliveryOptions(req,res,3,data);
              isError = 1;
              //return;
              debuglog("result errorCode is 1");
              }
            else {
                var deliveryOptionDetails = {};
                deliveryOptionDetails.deliveryId = data.deliveryId;
                deliveryOptionDetails.deliveryName = result.deliveryName;
                deliveryOptionDetails.enabled = result.enabled;
                deliveryOptionDetails.payOptions   = result.payOptions;
                deliveryOptionDetails.workQueue    = result.workQueue;
                deliveryOptionDetails.status       = result.status;
                deliveryOptionDetails.statusId     = result.statusId;   
                
                data.deliveryOptionDetails = deliveryOptionDetails;
                getDeliveryOptions(req,res,9,data);
                debuglog("No error case");
                debuglog(deliveryOptionDetails);
            }
          }
        });
} 


function getDeliveryOptions(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');

 var tabl_name = 'delivery_options_master';
 
  datautils.DelMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.deliveryOptions = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
} 
   
exports.getData = processRequest;








