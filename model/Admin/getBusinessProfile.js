function processRequest(req,res,data){
   var totalRows = 0;
   var user_stat = "";
   var businessId = "";
   
    var promise = db.get('user_master').find({$and:[{org_id: data.orgId},{system_user:true}]});
     promise.each(function(docuser){
      totalRows++;
      businessId = docuser._id;
      user_stat = docuser.user_status;
    });
    promise.on('complete',function(err,doc){
      if(totalRows == 0)
        processStatus(req,res,6,data);
      else if(totalRows > 1)
        processStatus(req,res,6,data); //system error should be 1 row only
      else {
        if (user_stat == 9)
          getUserDetail(req,res,businessId,user_stat,data);
        else
          processStatus(req,res,1,data);
      }
    });
    promise.on('error',function(err){
      console.log(err);
      processStatus(req,res,6,data);//system error
    });
}

function  getUserDetail(req,res,businessId,user_stat,data){

    var async = require('async');
    var datautils = require('../Registration/dataUtils');
    var isError = 0;
    data.subscription = {};
    
     async.parallel({
         usrStat: function(callback){datautils.usrDets(businessId,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.usrStat.errorCode != 0 && results.usrStat.errorCode !=1)) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
              data.subscription.businessDetails = results.usrStat.subscription;
              //getMasterNames(req,res,9,data);
              getSellers(req,res,9,data);
              }
          }); 
  }

function getMasterNames(req,res,stat,data){
    
    var async = require('async');
    var mastutils = require('../Registration/masterUtils');
    var isError = 0;
  
  
    if(data.subscription.businessDetails.businessType)
       var bizid =  parseInt(data.subscription.businessDetails.businessType);
     else
       var bizid = 0;
  
     if(data.subscription.businessDetails.baseCurrency)
       var cid =  data.subscription.businessDetails.baseCurrency;
     else
       var cid = '';
     
     async.parallel({
         bizStat:  function(callback){mastutils.masterNames('business_type_master',bizid,callback); },
         currStat: function(callback){mastutils.masterNames('currency_type_master',cid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            else {
              data.subscription.businessDetails.baseCurrency = results.currStat.typeName;
              getSellers(req,res,9,data);
            }
          }); 
}

function  getSellers(req,res,stat,data){

    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    
    var tblName= data.subscription.businessDetails.orgId+"_sellers";
    
     async.parallel({
         usrStat: function(callback){datautils.sellerDets(tblName,callback); },
         },
         function(err,results) {
          debuglog(results);
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.usrStat.errorCode != 0 && results.usrStat.errorCode !=1)) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
              data.subscription.businessDetails.totalBranches = results.usrStat.totalBranches;
              data.subscription.businessDetails.totalSellers  = results.usrStat.totalSellers;
              getMasterTypes(req,res,9,data);
              }
          }); 
  }

function getMasterTypes(req,res,stat,data){
    
    var async = require('async');
    var mastutils = require('../Registration/masterUtils');
    var isError = 0;
    
     async.parallel({
         currStat: function(callback){mastutils.masterTypes('currency_type_master',callback); },
         bizStat: function(callback){mastutils.masterTypes('business_type_master',callback);} 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && (results.currStat.errorCode != 0 && results.currStat.errorCode !=1)) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && (results.bizStat.errorCode != 0 && results.bizStat.errorCode !=1)) {
             processStatus (req,res,results.bizStat.errorCode,data);
             isError = 1;
             return;
            }
          if(isError == 0) {
              data.businessTypes = results.bizStat.types;
              data.currencies = results.currStat.types;
              processStatus(req,res,9,data);
            }
          }); 
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;


