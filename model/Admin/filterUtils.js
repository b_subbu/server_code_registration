function getFilters(key_name,statList,pid,tid,callback) {
  
  switch(key_name){
     case 'status':
     var tblName = 'delivery_option_status_master';
       getStatusFilter(statList,tblName,callback);  
        break;
     case 'enabled':
       getEnableFilter(callback);
       break;
    default:
       callback(null,null);
       break;
  }
  
}

function getTaxFilters(key_name,statList,pid,tid,callback) {
  
  switch(key_name){
     case 'status':
     var tblName = 'tax_type_status_master';
       getStatusFilter(statList,tblName,callback);  
        break;
     case 'enabled':
       getEnableFilter(callback);
       break;
    default:
       callback(null,null);
       break;
  }
  
}

function getPGFilters(key_name,statList,pid,tid,callback) {
  
  switch(key_name){
     case 'status':
     var tblName = 'pg_status_master';
       getStatusFilter(statList,tblName,callback);  
        break;
     case 'type':
       getTypeFilter(callback);
       break;
    default:
       callback(null,null);
       break;
  }
  
}


//for status no need to again query db since statList is already passed
//and it varies from tab to tab for other filters we can just query master
//and return

function getStatusFilter(mastData,tblName,callback) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var data = {};
     
      async.forEachOf(mastData,function(key1,value1,callback1){
        var statID=key1;
        getItemStatName(statID,tblName,function(err,result) {
           if(err){ isError =1; callback1();}
         else {
                 var tmpArray = {id:statID, name:result};
                 itemMasters.push(tmpArray);
                  callback1(); 
              }
           }); 
          },
         function(err){
       if(err){
            callback(6,null);      }
      else {
            callback(null,itemMasters);
	         }
      });
 

}

function getEnableFilter(callback){
    
    var enabledList = [{id:true,name:"Yes"},{id:false,name:"No"}]; 
    debuglog("Enabled possible values are only true and false");
    callback(null,enabledList);
    
}

function getItemStatName(statID,tblName,callback){
   var totalRows =0;
   var statName = '';
    var mastutils = require('./../Registration/masterUtils');

  
      mastutils.masterStatusNames(tblName,statID,function(err,result) {
       
         if(err) callback(6,null);
         else {
                statName = result.statusName;
                callback(null,statName); 
                debuglog("after getting status");
                debuglog(result);
              }
          });
       
 } 

 function getTypeFilter(callback){
    
    var typeList = [{id:1,name:"Test"},{id:2,name:"Prod"}]; 
    callback(null,typeList);
    
}

 exports.filterOptions = getFilters;
 exports.taxFilterOptions = getTaxFilters;
 exports.PGFilterOptions = getPGFilters; 
 

