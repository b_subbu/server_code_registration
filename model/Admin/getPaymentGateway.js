function processRequest(req,res,data){
  if( typeof req.body.pgId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getPGData(req,res,9,data);
}

function getPGData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.pgId = req.body.pgId;
    var tid = req.body.tabId;
    
    if(tid == 501)
     var tabl_name = data.orgId+'_payment_gateway';
    else
     var tabl_name = data.orgId+'_Revision_payment_gateway';
   
     var extend = require('util')._extend;
     var PGData = extend({},data);
     
  
      datautils.PGData(tabl_name,PGData,function(err,result) {
           debuglog(result);
           debuglog("after getting result from PG table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              getPaymentGateways(req,res,3,data);
              isError = 1;
              //return;
              debuglog("result errorCode is 1");
              }
            else {
                var paymentGatewayParams = {};
                var paymentGatewayDetails = {};
                paymentGatewayDetails.pgId = data.pgId;
                paymentGatewayDetails.statusId = result.statusId;
                paymentGatewayParams.key = result.key;
                paymentGatewayParams.salt = result.salt;
                paymentGatewayParams.paymentTypes   = result.paymentTypes;
                paymentGatewayParams.defaultPaymentType = result.defaultPaymentType;
                paymentGatewayParams.paymentSubTypes = result.paymentSubTypes;
                paymentGatewayParams.customMessage = result.customMessage;
                paymentGatewayParams.baseCurrency = "INR";
                paymentGatewayDetails.gatewayId = result.gatewayId;
                paymentGatewayDetails.gatewayName = result.name;
                paymentGatewayDetails.gatewayType = result.gatewayType;
                paymentGatewayDetails.gatewayPriority = result.gatewayPriority;
                paymentGatewayDetails.paymentGatewayParams = paymentGatewayParams;
                
                
                data.paymentGatewayDetails = paymentGatewayDetails;
                getPaymentGateways(req,res,9,data);
                debuglog("No error case");
                debuglog("Note base currency is hard-coded to INR");
                debuglog("When PG is enabled for foreign currency change here");
                debuglog("After through testing !!!");
                debuglog(paymentGatewayParams);
            }
          }
        });
} 


function getPaymentGateways(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');

 var tabl_name = 'payment_gateways_master';
 
  datautils.PGMaster(tabl_name,function(err,result) {
           debuglog(result);
           debuglog("after getting result from PG Master table");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.paymentGateways = result;
                var subTypes = [{id:1,label:'Test'},{id:2,label:'Prod'}];
                data.gateways = [{id:1,name:'PayU',subTypes:subTypes}];
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
exports.getData = processRequest;







