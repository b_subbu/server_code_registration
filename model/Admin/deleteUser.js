function processRequest(req,res,data){
   var uid = data.userId;
    if( typeof uid === 'undefined' || typeof uid === null)
      processStatus(req,res,6,data); //system error
   else {
   
     var ObjectId = require('mongodb').ObjectID;
     var user_id = new ObjectId(uid);
     
    
	  //can run async no need to wait for completion or status of result
	  db.get('user_master').remove({_id: user_id});
	  db.get('user_detail').remove({user_id: uid});
	  db.get('user_otp').remove({user_id: user_id});
	  db.get('service_status').remove({user_id: user_id});
    db.get('user_role_link').remove({user_id: uid});
    db.get('user_rolegroup_link').remove({user_id: uid});
    
   
   //also delete from user_role_link
       processStatus(req,res,9,data)
   }
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Admin/deleteUser');
  
    controller.process_delete(req,res,stat,data);
}
   
exports.getData = processRequest;

