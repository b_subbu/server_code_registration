function processRequest(req,res,data){
   if( typeof req.body.pgIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkPGStatus(req,res,9,data);
}


function checkPGStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var PGArr = req.body.pgIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.pgIds = PGArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_payment_gateway'; //TODO: move to a common definition file

    async.forEachOf(PGArr,function(key,value,callback1){
        var pgData= {};
          pgData.gateway = {};
          pgData.gateway.pgId = key;
        datautils.checkPGCode(tabl_name,pgData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 0)
               isError = result.errorCode;
             callback1();
             }
           });       }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToRevision(req,res,9,data);
          }
      });
}

function moveToRevision(req,res,stat,data){
    
    var PGArr = data.pgIds;
    
    debuglog("Now start moving the entries to Revision Table");
    debuglog(PGArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_payment_gateway'; //TODO: move to a common definition file
    var mainTable    = orgId+'_payment_gateway';
   
    async.forEachOf(PGArr,function(key,value,callback1){
      pgId = key;
     async.parallel({
          relStat: function (callback){datautils.movePGHeader(mainTable,revisionTable,pgId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changePGStatus(req,res,9,data,PGArr,mainTable,revisionTable); 
         }
     });
}


function changePGStatus(req,res,stat,data,PGArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(PGArr,function(key,value,callback1){
    pgId = key;
    var reason = '';
       statutils.changeStatus(revTable,data.orgId,data.businessType,data.userId,pgId,81,'Payment Gateway Submitted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.pgIds = PGArr;
                 getPGDetails(req,res,9,data,revTable);
             }
          });
     
}


function getPGDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.PGSData(revTable,data.pgIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getPGStatusName(req,res,9,data,result); 
         }
     });
    
}

function getPGStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('pg_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.pgData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}   
   
exports.getData = processRequest;
exports.moveToRevision=moveToRevision; 
