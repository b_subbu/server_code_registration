function processRequest(req,res,data){
    if( typeof req.body.taxIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkTaxStatus(req,res,9,data);
}

function checkTaxStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var TaxArr = req.body.taxIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.taxIds = TaxArr;
    
    var orgId = data.orgId;
    var enteredItems = [];
    var authorisedItems = [];
   
   
    var tabl_name = orgId+'_tax_type'; //TODO: move to a common definition file

    async.forEachOf(TaxArr,function(key,value,callback1){
        var TaxData= {};
            TaxData.tax = {};
            TaxData.tax.taxId = key;
        datautils.checkTaxCode(tabl_name,TaxData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
         else{
                  if (result.status == 80){
                   debuglog("This is entered status item");
                   debuglog("So can be deleted directly");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 82){
                   debuglog("This is authorised status item");
                   debuglog("So has to go through authoristation to delete");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Delete is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();
          }
       });       
     }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
               deleteOptions(req,res,9,data,enteredItems,authorisedItems);
         }
      });
}

function deleteOptions(req,res,stat,data,enteredItems,authorisedItems){
    var uid = req.body.userId;
     
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_tax_type'; //delete is possible only in Main Table currently
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      taxId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteTaxCode(mainTable,taxId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
     else {
          if(authorisedItems.length > 0){
          debuglog("This request has some authorised items too");
          debuglog(authorisedItems);
          deleteAuthoriseOptions(req,res,9,data,authorisedItems);
          }
          else{
          debuglog("This request has no authorised items");
          debuglog("so call return");
          processStatus(req,res,9,data); 
        }
       }     
      });
}

function deleteAuthoriseOptions(req,res,stat,data,authorisedItems){
    var uid = req.body.userId;
    //var DelArr = req.body.deliveryIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_tax_type';
    var revisionTable = data.orgId+'_Revision_tax_type';
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      taxId = key;
     async.parallel({
          relStat: function (callback){datautils.copyTaxHeader(mainTable,revisionTable,taxId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           debuglog("Change status of items in revision table to ready2Delete");
           changeTaxStatus(req,res,9,data,authorisedItems,mainTable,revisionTable);
          }
      });
}


function changeTaxStatus(req,res,stat,data,TaxArr,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(TaxArr,function(key,value,callback1){
    taxId = key;
     var reason = 'Delete Tax submitted';
      statutils.changeTaxStatus(revisionTable,data.orgId,data.businessType,data.userId,taxId,89,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 debuglog("can get details only for authorised items");
                 debuglog("since other array would have got deleted");
                 getTaxDetails(req,res,9,data,revisionTable);
             }
          });
     
}


function getTaxDetails(req,res,stat,data,revisionTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.TaxesData(revisionTable,data.taxIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getTaxStatusName(req,res,9,data,result); 
         }
     });
    
}

function getTaxStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('tax_type_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.taxTypeData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;



