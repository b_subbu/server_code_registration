//These functions are same like Orders* functions except with customerId included
function getInvoicesCount(orgId,bizType,custId,tablName,statList,callback){
 
  var promise = db.get(tablName).count({$and: 
     [{org_id: orgId},{customer_id:custId},{order_status: {$in: statList}}]});
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}


function getInvoiceHeader(tblName,orgId,custId,bizType,pgid,size,slit,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
   var promise = db.get(tblName).find({$and:
   [{org_id:orgId},{customer_id: custId},{order_status: {$in: slit}}]},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {invoiceId: doc.order_id,amount:0,date:moment(doc.create_date).format('DD-MM-YYYY'),status:doc.order_status};
        
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
        //amount will come only in getPayment details so keep it zero until 
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.orders = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.orders = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


exports.invoiceCount = getInvoicesCount;
exports.invoiceHeader = getInvoiceHeader;
