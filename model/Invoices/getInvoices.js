function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.businessId === 'undefined' || typeof req.body.customerId === 'undefined')
      processStatus(req,res,6,data); //system error 
   else 
      checkMasterStatus(req,res,data);
}


function checkMasterStatus(req,res,data){
    
    var uid = req.body.businessId;
    var cid = req.body.customerId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var custutils = require('../Render/utils');
    var isError = 0;
    data.customerId = cid;
    data.businessId = uid;
    
    
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         custStat: function(callback){custutils.checkCustomerId(cid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,1,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
            if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              getInvoiceCount(req,res,9,data);
            }
          }); 
}


function getInvoiceCount(req,res,stat,data){
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    var invoiceList = [];
    
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
     var statList = [81,83,84,85]; //Confirmed, Ready2Ship,shipped and delivered Orders
 
    var tblName = data.orgId+'_order_header';   
        
         datautils.invoiceCount(data.orgId,data.bizType,data.customerId,tblName,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
           else {
            data.pageId = pageId;
            data.totalRecords = results;
            getInvoices(req,res,stat,data,statList,tblName);
             }
         });
}

//TODO move to utils and make status list dynamic
//also need to add paging 
//need orderHeader seperately but other functions can use Orders/datautils
//since it is orderwise and no need for customerId
function getInvoices(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   var orders = [];
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var customerId = data.customerId;
   var pageId = 1;
   var size = 20;
  

   
      datautils.invoiceHeader(headerTable,orgId,customerId,bizType,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.invoices = [];
              processStatus(req,res,9,data); //no Orders for the business nothing to do further
              isError = 1;
             //will be handled at count itself, just adding here
            }
            else {
            orders = result.orders; 
            getOrderPaymentData(req,res,9,data,orders);
            }
          }
        });
} 


function getOrderPaymentData(req,res,stat,data,orders) {
   var async = require('async');
   var datautils = require('../Orders/dataUtils');
   var type = require('type-detect');
   var masterData = [];
  var payment_tabl_name = data.orgId+'_order_payment';
     
   async.forEachOf(orders,function(key,value,callback){
   var invoiceId = key.invoiceId;
   
   
    datautils.orderPayment(payment_tabl_name,invoiceId,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 key['amount'] = result;
                 callback(); 
              }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            getOrderStatusName(req,res,9,data,orders);
           }
        });
   
} 

function getOrderStatusName(req,res,stat,data,orders) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(orders,function(key1,value1,callback1){
        var statID=key1.status;
           statutils.statName(statID,'orders_status_master',function(err,result) {
           if(err) callback1();
         else {
                key1.status = result;
                 callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           data.invoices = orders;
           processStatus(req,res,9,data);
        }
      });
 

} 
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Invoices/getInvoices');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




