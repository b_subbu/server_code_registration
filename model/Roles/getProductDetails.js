<<<<<<< HEAD
function processRequest(req,res,data){
   if(  typeof req.body.productId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getItems(req,res,9,data);
}

//Note: tabs is implemented without checking role id

function getItems(req,res,stat,data){
 var async = require('async');
    var utils = require('./roleutils');
     var type = require('type-detect');
  
    data.productId = req.body.productId;
    var isError = 0;
    var modules = [];
    var submoduleid =data.productId;
    
    debuglog("start gettting modules for submoduleid:");
    debuglog(submoduleid);
       
      utils.tabsList(submoduleid,function(err,results) {
               if(err){ processStatus(req,res,6,data);    }
                  else {
                          if(type(results.items) === 'array') {
                           
                            data.productTabs = {};
                            data.productTabs = results.items;
                            getItemDetails(req,res,9,data);
                               }
                            else {
                               getItemDetailsTabless(req,res,9,data);
                            } 
                        }
                    });  
}

function getItemDetails(req,res,stat,data) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
   
     var itemArray = data.productTabs;
     if(type(itemArray) !== 'array'){
         processStatus(req,res,5,data);
         return;
    }
    
    var moduleid = data.productId
  
    async.forEachOf(itemArray,function(value,key,callback){
       var itemid = itemArray[key].id;
       //data.productTabs[key].itemActions = [];
       data.productTabs[key].nonItemActions = [];
        
       utils.itemList(moduleid,itemid,function(err,result) {
         if(err) return callback(err);
         else {
            if(type(result.itemActions) == 'array')
                 {
                  for(idx = 0; idx<result.itemActions.length; idx++){
                    if (result.itemActions[idx].itemlevel == true) 
                       ; //data.productTabs[key].itemActions.push(result.itemActions[idx]); not needed
                    else
                       data.productTabs[key].nonItemActions.push(result.itemActions[idx]);
                  }
                 }
            callback(); //next item in foreachof
           }
        });
        },
      function(err){
      if(err)
           processStatus(req,res,6,data);
      else
         processStatus(req,res,9,data);
      });  
}

function getItemDetailsTabless(req,res,stat,data) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
   
   
    var moduleid = data.productId
    var itemid = 999; 
      //all tabless products have special tabid 999 in module_item_link
      //because 0 has other significance there
      
       productDetails = {};
       var nonItemActions = [];
        
       utils.itemList(moduleid,itemid,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
            if(type(result.itemActions) == 'array')
                 {
                  //hopefully async is not required here
                  //since array should be small always
                  for(idx = 0; idx<result.itemActions.length; idx++){
                    if (result.itemActions[idx].itemlevel == true) 
                       ; //data.productTabs[key].itemActions.push(result.itemActions[idx]); not needed
                    else
                       nonItemActions.push(result.itemActions[idx]);
                    
                    
                  }
                }
            productDetails.nonItemActions = nonItemActions
            data.productDetails = productDetails;    
            processStatus(req,res,9,data);
           }
        });
       
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Roles/getProductDetails');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined' || typeof req.body.productId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var productid = req.body.productId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    data.userId = uid;
    data.productId = productid;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         usrStat: function(callback){utils.usrStatus(uid,callback); },
         roleStat: function(callback){utils.usrRoles(uid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.roleStat.errorCode != 0) {
              processStatus (req,res,results.roleStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
             
          if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              var roles = results.roleStat.rolesArray;
              getItems(req,res,9,data,roles);
            }
          }); 
}
//Note: this is for superuser implementation
//for actual usage use itemNamePermission for cases 
//permission =9 and otherwise

function getItems(req,res,stat,data,roles){
 var async = require('async');
    var utils = require('./roleutils');
     var type = require('type-detect');
  
    var isError = 0;
    var modules = [];
    var submoduleid =data.productId;
       var roleid = roles[0].roleId;
       
      utils.tabsList(roleid,submoduleid,function(err,results) {
               if(err){ processStatus(req,res,6,data);    }
                  else {
                         if(type(results.items) === 'array') {
                           
                            data.productTabs = {};
                            data.productTabs = results.items;
                            getItemDetails(req,res,9,data);
                               }
                            else {
                              processStatus(req,res,4,data);
                            } 
                        }
                    });  
}

function getItemDetails(req,res,stat,data,roles) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
   
     var itemArray = data.productTabs;
     if(type(itemArray) !== 'array'){
         processStatus(req,res,5,data);
         return;
    }
    
    var moduleid = data.productId
  
    async.forEachOf(itemArray,function(value,key,callback){
       var itemid = itemArray[key].id;
       //data.productTabs[key].itemActions = [];
       data.productTabs[key].nonItemActions = [];
        
       //role id is hardcoded as superuser
       utils.itemList(18,moduleid,itemid,function(err,result) {
         if(err) return callback(err);
         else {
            if(type(result.itemActions) == 'array')
                 {
                  for(idx = 0; idx<result.itemActions.length; idx++){
                    if (result.itemActions[idx].itemlevel == true) 
                       ; //data.productTabs[key].itemActions.push(result.itemActions[idx]); not needed
                    else
                       data.productTabs[key].nonItemActions.push(result.itemActions[idx]);
                    
                    
                  }
                 //data.productTabs[key].tabActions = result.itemActions;
                 }
            callback(); //next item in foreachof
           }
        });
        },
      function(err){
      if(err)
           processStatus(req,res,6,data);
      else
         processStatus(req,res,9,data);
      });  
}


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Roles/getProductDetails');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;

>>>>>>> DEV/master
