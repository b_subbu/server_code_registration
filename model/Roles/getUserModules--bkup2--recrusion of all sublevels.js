function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    data.userId = uid;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         usrStat: function(callback){utils.usrStatus(uid,callback); },
         roleStat: function(callback){utils.usrRoles(uid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.roleStat.errorCode != 0) {
              processStatus (req,res,results.roleStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
          if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              var roles = results.roleStat.rolesArray;
              getUserModules(req,res,9,data,roles);
            }
          }); 
}

function getUserModules(req,res,stat,data,roles){
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var modules = [];
    
    async.forEachOf(roles,function(value,key,callback){
      
        var roleid = roles[key].roleId;
       
        utils.roleModules(roleid,function(err,result) {
           if(err) return callback(err);
           else {
                if(result.rolesArray)
                    data.modules = result.rolesArray;
                 callback(); //next item in foreachof
                }
            });
           },
      function(err){
        if(err)
            processStatus(req,res,6,data);
        else 
            getModuleDetails(req,res,9,data,roles);
      });  
}

function getModuleDetails(req,res,stat,data,roles) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
   
     var moduleArray = data.modules;
    if(type(moduleArray) !== 'array'){
         processStatus(req,res,4,data);
         return;
    }
  
    async.forEachOf(moduleArray,function(value,key,callback){
       var moduleid = moduleArray[key].id;
       
       utils.moduleName(moduleid,function(err,result) {
         if(err) return callback(err);
         else {
            if(result)
                 data.modules[key].name = result; //module name null is not an error
            callback(); //next item in foreachof
           }
        });
        },
      function(err){
      if(err)
           processStatus(req,res,6,data);
      else
          getSubModule(req,res,9,data,roles,0,0,moduleArray);
      });  
}

//Get list of all submodules of the first module
//that does not have permission =0
//except for permission=9 we have to do a double check
//of module->submoudles map and permissions for the roles

//writing recruisve function is more difficult in node due to async
//so it is implemented using more functions rather
function getSubModule(req,res,stat,data,roles,level,moduleEntry,submodules) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
    var subModulesArray = [];
   
   //level 0 means it is coming from main module call
   if(level == 0 ) {
         var submoduleArray = data.modules;
          //get submodules of only non-zero or non-one modules
          //i.e., not hidden or read-only modules
          for(idx = 0; idx < submoduleArray.length; idx++) {
             if(submoduleArray[idx].permission == 0 || submoduleArray[idx].permission == 1)
                 continue;
            else{
                  var submoduleid = submoduleArray[idx].id;
                  moduleEntry = idx;
                  break;
               }
            }
        utils.subModules(submoduleid,function(err,results) {
          if(err){
              processStatus(req,res,6,data);
            }
        else {
           //if submodules are present then populate that to data and recurse again
           //else call get work item
             if(type(results.subModules) === 'array') {
                     level++;
                     getSubModules(req,res,stat,data,roles,level,moduleEntry,results.subModules);
                    }
              else{
                   data.subModules = {}; //no submodules for this user
                   //getItemsList(req,res,9,data,roles,level,submodules);
                   //getitems not needed for now check in backup file if required
                     processStatus(req,res,9,data);
                  }
              }  
         });
        }
     else{
            if(level == 1)
                  data.modules[moduleEntry].subModule = submodules;
            else
                 {
                    var submoduleid = submodules[0].id;
                    data.modules[moduleEntry].subModule[0].subModule = submodules;
                }
                 
                 // data.modules[moduleEntry].subModule[0][level]= submodules;
                
             
            //now call for next level in submodules   
            var submoduleid = submodules[0].id;
            var moduleid = submodules[0].moduleId;
            utils.subModules(submoduleid,function(err,results) {
            if(err)
                       processStatus(req,res,6,data);
            else{
           //if submodules are present then populate that to data and recurse again
           //else call get work item
           if(type(results.subModules) === 'array') {
                for(idx = 0; idx< results.subModules.length; idx++)
                  subModulesArray[idx] = {id:results.subModules[idx].id,permission:submodules[0].permission,sequence:results.subModules[idx].sequence};
                 level++;
                getSubModules(req,res,stat,data,roles,level,moduleEntry,subModulesArray);
              }
            else
                    processStatus(req,res,9,data);
             }
           });
       }
}

//Get the name and permission of submodules passed
//if modules permission is 9 then no need to check further permissions
//because it is special permission that all submodules and items becomes
//automatically 2

//This function will return the array of submodules
//for the passed submodule along with their name and permission 
function getSubModules(req,res,stat,data,roles,level,moduleEntry,submodules) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
    var subModulesArray = [];
    var modulePermission = 0;
    
    var moduleId = submodules[0].id;
    var moduleArray = data.modules;
    for(idx = 0; idx < moduleArray.length; idx++){
       if (idx == moduleEntry) {
           modulePermission = moduleArray[idx].permission;
           break;
          }
    }
    
    if(modulePermission == 9) {
       async.forEachOf(submodules,function(value,key,callback){
       var moduleid = submodules[key].id;
       
       utils.moduleName(moduleid,function(err,result) {
         if(err) return callback(err);
         else {
              submodules[key].permission = 2;//read-write permission
              if(result)
                 submodules[key].name = result; //module name null is not an error
             callback(); //next item in foreachof
           }
         });
        },
      function(err){
      if(err)
          processStatus(req,res,6,data);
      else
          getSubModule(req,res,9,data,roles,level,moduleEntry,submodules);
      });
    }
    else{
      async.forEachOf(submodules,function(value,key,callback){
        var moduleid = submodules[key].id;
     
       //note here we are getting permission only for first role of given user
       //TODO make this an recursion of recrusion to get multiple roles for one user
       utils.moduleNamePermission(moduleid,roles[0].roleId,function(err,result) {
         if(err) return callback(err);
         else {
               submodules[key].permission = result.modulePermission;
              if(result)
                  submodules[key].name = result.moduleName; //module name null is not an error
             callback(); //next item in foreachof
             }
          });
        },
      function(err){
      if(err)
          processStatus(req,res,6,data);
      else
          getSubModule(req,res,9,data,roles,level,moduleEntry,submodules);
      });
    }
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Roles/getUserModules');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
