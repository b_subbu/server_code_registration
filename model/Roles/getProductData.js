<<<<<<< HEAD
function processRequest(req,res,data){
    if( typeof req.body.productId === 'undefined' || typeof req.body.tabId === 'undefined' || typeof req.body.pageId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus is left asis for baseCurrency");
}

function checkMasterStatus(req,res,data){
    
    var productid = req.body.productId;
    var tabid = req.body.tabId;
    var pageid = req.body.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    data.productId = productid;
    data.tabId = tabid;
    data.pageId = pageid;
    
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
    debuglog("Role is just passed to all getXXX functions but not be used for now");
    
     async.parallel({
         usrStat: function(callback){utils.usrStatus(data.userId,callback); },
         rolStat: function(callback){utils.userRole(data.userId,callback); }
          },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0) {
              //data.orgId = results.usrStat.orgId;
              //data.businessType = results.usrStat.bizType;
			        data.baseCurrency = results.usrStat.baseCurrency;
              var roles = results.rolStat.result; 
              getHeader(req,res,9,data,roles);
            }
          }); 
}

function getHeader(req,res,stat,data,roles){
    var uid = data.userId;
    var productid = data.productId;
    var tabid = data.tabId;
    var pageid = data.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var statList = {};
    
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         tabStat: function(callback){utils.tabStatus(productid,tabid,callback); },
         tabHead: function(callback){utils.tabHeader(productid,tabid,callback); } 
         },
         function(err,results) {
			  if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabStat.errorCode != 0) {
              processStatus (req,res,results.tabStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabHead.errorCode != 0) {
              processStatus (req,res,results.tabHead.errorCode,data);
              isError = 1;
              return;
             }
              
          if(isError == 0) {
              data.productHeader = results.tabHead.headerList;
              statList = results.tabStat.statusList;
              getDatas(req,res,9,data,roles,statList);
            }
          }); 
}

function getHeaderQueue(req,res,stat,data,statList,roles){
    var uid = data.userId;
    var productid = data.productId;
    var tabid = data.tabId;
    var pageid = data.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         tabHead: function(callback){utils.tabHeader(productid,tabid,callback); } 
         },
         function(err,results) {
			  if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabHead.errorCode != 0) {
              processStatus (req,res,results.tabHead.errorCode,data);
              isError = 1;
              return;
             }
              
          if(isError == 0) {
              data.productHeader = results.tabHead.headerList;
			        getDatas(req,res,9,data,roles,statList);
            }
          }); 
}

//Note: This is generic only uptil here
//need to do getData individually for each tab inside datautils
//this is because of the nature of requests

//If alerts we call functions in this same file
//TODO move them to alert folder later
//Else call the corresponding folder and pass the data
//it will be processed from there to continue

function getDatas(req,res,stat,data,roles,statList) {
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var datautils = require('./datautils.js')
    var isError = 0;
   
  switch(pid){
     case 140: //alert note: hard-coded 
       switch(tid){
         case 501:
         case 502:
       		var model = require('../Boarding/getAlerts');
            model.getProductData(req,res,stat,data,roles,statList);
            break;
          default:
            processStatus(req,res,6,data); //only 2 tabs defined for alerts
            break;
      }
      break;
    case 150:
      switch(tid){
         case 501:
         case 502:
            var model = require('../Menu/getMenuItems');
            model.getProductData(req,res,stat,data,roles,statList);
            break;
         default:
            processStatus(req,res,6,data); //only 2 tabs defined for menu items
            break;
       }
      break;
 
   case 160:
      switch(tid){
         case 501:
         case 502:
            var model = require('../Menu/getMenus');
            model.getProductData(req,res,stat,data,roles,statList);
            break;
         default:
            processStatus(req,res,6,data); 
            break;
       }
      break;
     
	 case 170:
          var model = require('../Orders/getOrders');
            model.getProductData(req,res,stat,data,roles,statList);
            break;
     
     case 180:
          var model = require('../Invitation/getInvitations');
            model.getProductData(req,res,stat,data,roles,statList);
            break;
    	
	 case 190:
          var model = require('../Groups/getGroups');
            model.getProductData(req,res,stat,data,roles,statList);
            break;
	
	case 109:
	     var model = require('../Admin/getPaymentGateways');
             model.getProductData(req,res,stat,data,roles,statList);
             break;
   
   case 111:
        var model = require('../Admin/getDeliveryOptions');
            model.getProductData(req,res,stat,data,roles,statList);
            break;			
    	
	
	case 112:
        var model = require('../Admin/getTaxTypes');
            model.getProductData(req,res,stat,data,roles,statList);
            break;			
    	
	case 115:
        var model = require('../Charges/getCharges');
            model.getProductData(req,res,stat,data,roles,statList);
            break;			
    	
	case 116:
        var model = require('../InvoiceConfig/getInvoiceConfigs');
            model.getProductData(req,res,stat,data,roles,statList);
            break;			
    	
	
	case 141:
            getFilterOptions(req,res,stat,data,roles,statList); //just dummy call for alert templates
            break;
  
  	case 122:
        var model = require('../Sellers/getSellers');
            model.getProductData(req,res,stat,data,roles,statList);
            break;			
	  case 131:
	    var model = require('../Users/getRoles');
		    model.getProductData(req,res,stat,data,roles,statList);
			break;
    
    case 130:
	    var model = require('../Users/getUsers');
		    model.getProductData(req,res,stat,data,roles,statList);
			break;
  
    default:
       processStatus(req,res,6,data); 
       break;
  
  
  }
  
}

function getFilterOptions(req,res,stat,data,roles,statList) {
   var async = require('async');
   var filterutils = require('./filterUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
          
             filterOptions[key_name] = results;
             callback();
          
          }
          }); 
        }
     else
      callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions; 
         getData(req,res,stat,data,roles,statList);
         }
      });
} 

function getData(req,res,stat,data,roles,statList) {
    var uid = data.userId;
    var oid = data.orgId;
    var productid = data.productId;
    var tabid = data.tabId;
    var pageid = data.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var datautils = require('./datautils.js')
    var isError = 0;
    
     async.parallel({
         tabData: function(callback){datautils.tabData(oid,productid,tabid,pageid,statList,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabData.errorCode != 0) {
              processStatus (req,res,results.tabStat.errorCode,data);
              isError = 1;
              return;
             }
          if(isError == 0) {
              if(pageid == 1)
                 {
                  data.recordCount = results.tabData.resultCount;
                  data.productData = results.tabData.resultArray;
                  getNextStatus(req,res,9,data,statList);
                 }
              else
                 {
                  data.recordCount = results.tabData.resultCount;
                  data.productData = results.tabData.resultArray;
                  processStatus(req,res,9,data);
                }
            }
          }); 
}

function getNextStatus(req,res,stat,data,statList){
    var uid = data.userId;
    var productid = data.productId;
    var tabid = data.tabId;
    var pageid = data.pageId;
    var statutils = require('../Orders/statusUtils');
 
    var isError = 0;
   
       statutils.statList(productid,tabid,statList,'alert_status_master',function(err,result) {
       debuglog("After getting result");
       debuglog(result);
         if(err) return callback(err);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                       data.statusList = result;
                    
                  }
                  processStatus(req,res,9,data);
                 }
            
        });

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Roles/getProductData');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
exports.getQueue = getHeaderQueue;


=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined' || typeof req.body.productId === 'undefined' || typeof req.body.tabId === 'undefined' || typeof req.body.pageId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var productid = req.body.productId;
    var tabid = req.body.tabId;
    var pageid = req.body.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    data.userId = uid;
    data.productId = productid;
    data.tabId = tabid;
    data.pageId = pageid;
    
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         usrStat: function(callback){utils.usrStatus(uid,callback); },
         roleStat: function(callback){utils.usrRoles(uid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.roleStat.errorCode != 0) {
              processStatus (req,res,results.roleStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
             
          if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              var roles = results.roleStat.rolesArray;
              getHeader(req,res,9,data,roles);
            }
          }); 
}
//Note: this is for superuser implementation
// i think at this level permissions may not be needed to check
//permissions are applicable only until submodule level
//just keep roles here in case for future requirements

function getHeader(req,res,stat,data,roles){
    var uid = data.userId;
    var productid = data.productId;
    var tabid = data.tabId;
    var pageid = data.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var statList = {};
    
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         tabStat: function(callback){utils.tabStatus(productid,tabid,callback); },
         tabHead: function(callback){utils.tabHeader(productid,tabid,callback); } 
         },
         function(err,results) {
            if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabStat.errorCode != 0) {
              processStatus (req,res,results.tabStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabHead.errorCode != 0) {
              processStatus (req,res,results.tabHead.errorCode,data);
              isError = 1;
              return;
             }
              
          if(isError == 0) {
              data.productHeader = results.tabHead.headerList;
              //processStatus(req,res,9,data);
              statList = results.tabStat.statusList;
              getData(req,res,9,data,roles,statList);
            }
          }); 
}
//Note: This is generic only uptil here
//need to do getData individually for each tab inside datautils
//this is because of the nature of requests
function getData(req,res,stat,data,roles,statList) {
    var uid = data.userId;
    var oid = data.orgId;
    var productid = data.productId;
    var tabid = data.tabId;
    var pageid = data.pageId;
    var async = require('async');
    var utils = require('./roleutils');
    var datautils = require('./datautils.js')
    var isError = 0;
    
    //console.log("in getData");
    //console.log(data);
    //console.log(statList);
     //only one function just using parallel for consistency
    
     async.parallel({
         tabData: function(callback){datautils.tabData(oid,productid,tabid,pageid,statList,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabData.errorCode != 0) {
              processStatus (req,res,results.tabStat.errorCode,data);
              isError = 1;
              return;
             }
          if(isError == 0) {
              if(pageid == 1)
                 {
                  data.recordCount = results.tabData.resultCount;
                  data.productData = results.tabData.resultArray;
                  getNextStatus(req,res,9,data,statList);
                 }
              else
                 {
                  data.recordCount = results.tabData.resultCount;
                  data.productData = results.tabData.resultArray;
                  processStatus(req,res,9,data);
                }
            }
          }); 
}

function getNextStatus(req,res,stat,data,statList){
    var uid = data.userId;
    var productid = data.productId;
    var tabid = data.tabId;
    var pageid = data.pageId;
    var async = require('async');
    var utils = require('./datautils');
    var type = require('type-detect');
  
    var isError = 0;
   
     
        utils.statList(productid,tabid,statList,function(err,result) {
         if(err) return callback(err);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                       data.statusList = result;
                    
                  }
                  //processStatus(req,res,9,data);
                  getAlertTypes(req,res,9,data);
                  }
            
        });

}

function getAlertTypes(req,res,stat,data){
    
    var async = require('async');
    var utils = require('./../Boarding/utils');
    var type = require('type-detect');
  
    var isError = 0;
   
     
        utils.alertTypes(function(err,result) {
         if(err) return callback(err);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                       data.alertTypeList = result.alertTypes;
                    
                  }
                  processStatus(req,res,9,data);
                  }
            
        });

}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Roles/getProductData');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;


>>>>>>> DEV/master
