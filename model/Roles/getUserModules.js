<<<<<<< HEAD
function processRequest(req,res,data){
        getUserRole(req,res,9,data);
}


function getUserRole(req,res,stat,data){
     var utils = require('./roleutils');
    var isError = 0;
    
    
   utils.userRole(data.userId,function(err,result){
    if(err){
    debuglog(err);
    processStatus(req,res,6,data);
    }
    else{
    debuglog(result);
    getUserModules(req,res,9,data,result);
    }
   }); 
}


function getUserModules(req,res,stat,data,role) {
    var utils = require('./roleutils');
    var isError = 0;

    var tblName = data.orgId+'_roles';
    
    utils.roleModules(tblName,role,function(err,result){
    if(err){
    debuglog(err);
    processStatus(req,res,6,data);
    }
    else{
    debuglog(result);
    var roles=result
    generateRenderModules(req,res,9,data,roles);
    }
   }); 

}

function generateRenderModules(req,res,stat,data,roles){
      debuglog("This will generate the render modules");
      debuglog("That will be just the list of all modules with enabled true");
      debuglog("also enabled attribute will be stripped off");
      debuglog("TODO save this as an field in collection so next time login can be faster");
      var utils = require('./roleutils');
   utils.renderModules(roles,function(err,result){
    if(err){
    debuglog(err);
    processStatus(req,res,6,data);
    }
    else{
    debuglog(result);
    data.modules = result
    processStatus(req,res,9,data);
    }
  });
}
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
=======
//NOTE: this is a scale down version for only superuser
//and 2-level deep modules
//fully functional logic is in backup
//use that if and when need arises

function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    data.userId = uid;
    
    //call all functions async in parallel
    //but proceed only after completion and checks
    
     async.parallel({
         usrStat: function(callback){utils.usrStatus(uid,callback); },
         roleStat: function(callback){utils.usrRoles(uid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.roleStat.errorCode != 0) {
              processStatus (req,res,results.roleStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
             
          if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              var roles = results.roleStat.rolesArray;
              getUserModules(req,res,9,data,roles);
            }
          }); 
}

function getUserModules(req,res,stat,data,roles){
 var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var modules = [];
    
    async.forEachOf(roles,function(value,key,callback){
      
        var roleid = roles[key].roleId;
       
        utils.roleModules(roleid,function(err,result) {
           if(err) return callback(err);
           else {
                if(result.rolesArray)
                    data.modules = result.rolesArray;
                 callback(); //next item in foreachof
                }
            });
           },
      function(err){
        if(err)
            processStatus(req,res,6,data);
        else 
            getModuleDetails(req,res,9,data,roles);
      });    
}


function getModuleDetails(req,res,stat,data,roles) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
   
     var moduleArray = data.modules;
    if(type(moduleArray) !== 'array'){
         processStatus(req,res,4,data);
         return;
    }
  
    async.forEachOf(moduleArray,function(value,key,callback){
       var moduleid = moduleArray[key].id;
       
       utils.moduleName(moduleid,function(err,result) {
         if(err) return callback(err);
         else {
            if(result)
                 data.modules[key].name = result; //module name null is not an error
            callback(); //next item in foreachof
           }
        });
        },
      function(err){
      if(err)
           processStatus(req,res,6,data);
      else
          getSubModule(req,res,9,data,roles,0,0,moduleArray);
      });  
}



//Get list of all submodules of the first module
//that does not have permission =0
//except for permission=9 we have to do a double check
//of module->submoudles map and permissions for the roles
//note: permission==9 is implicit for all since superuser role

//writing recruisve function is more difficult in node due to async
//so it is implemented using more functions rather
function getSubModule(req,res,stat,data,roles,level,moduleEntry,submodules) {
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');
    var subModulesArray = [];
   
    if(level == 0 ) {
         var submoduleArray = data.modules;
          async.forEachOf(submoduleArray,function(value,key,callback) {
                var submoduleid = submoduleArray[key].id;
              
                utils.subModules(submoduleid,function(err,results) {
                 if(err){ processStatus(req,res,6,data);    }
                  else {
                         if(type(results.subModules) === 'array') {
                              data.modules[key].subModules = results.subModules;
                              callback();
                              
                        }
              else{
                     callback(); 
                  }
                }
              });
              },
              function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {level++;
                    getSubModulesNew(req,res,9,data,roles,level,moduleEntry,submodules);
                    }
                 });  
            }
         
    if(level == 1) {
        var submoduleArray = data.modules;
          async.forEachOf(submoduleArray,function(value,key,callback) {
               var subsubmoduleArray = submoduleArray[key];
                 async.forEachOf(subsubmoduleArray,function(value,key,callback) {
                    var submoduleid = subsubmoduleArray[key].id;
             
                utils.subModules(submoduleid,function(err,results) {
                 if(err){ processStatus(req,res,6,data);    }
                  else {
                         if(type(results.subModules) === 'array') {
                              data.modules[key].subModules[key] = results.subModules;
                              
                        }
                    
              else{
                    callback(); 
                  }
                }
              });
              },
              function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    callback();
                  });  
            },
          function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                     level++;
                     getSubModulesNew(req,res,9,data,roles,level,moduleEntry,submodules);
                    }
                 });
        }
  if(level == 2) {
        var submoduleArray = data.modules;
            async.forEachOf(submoduleArray,function(value,key1,callback1) {
                var subsubmoduleArray = submoduleArray[key1].subModules;
                       async.forEachOf(subsubmoduleArray,function(value,key3,callback2) {
                         var submoduleid = subsubmoduleArray[key3].id;
                   utils.subModules(submoduleid,function(err,results) {
                 if(err){ processStatus(req,res,6,data);    }
                  else {
                         if(type(results.subModules) === 'array') {
                               data.modules[key1].subModules[key3].subModules = results.subModules;
                               callback2();
                        }
                    
              else{
                    callback2(); 
                  }
                }
              });
              },
              
          function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                      callback1();
                      }
                 });
        },
         function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                     
                     level++;
                     getSubModulesNew(req,res,9,data,roles,level,moduleEntry,submodules);
                    }
                 });
       }  
        if(level == 3){
        processStatus(req,res,9,data);
        
        }
 
       
}
//Get the name and permission of submodules passed
//if modules permission is 9 then no need to check further permissions
//because it is special permission that all submodules and items becomes
//automatically 2

//This function will return the array of submodules
//for the passed submodule along with their name and permission 

function getSubModulesNew(req,res,stat,data,roles,level,moduleEntry,submodules) {
   var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var type = require('type-detect');

  if(level == 2) {
        var submoduleArray = data.modules;
          async.forEachOf(submoduleArray,function(value,key1,callback1) {
               var subsubmoduleArray = submoduleArray[key1].subModules;
                 async.forEachOf(subsubmoduleArray,function(value,key2,callback) {
                    var submoduleid = subsubmoduleArray[key2].id;
               
                utils.moduleName(submoduleid,function(err,results) {
                 if(err){ processStatus(req,res,6,data);    }
                  else {
                         
                            if(results){  data.modules[key1].subModules[key2].name = results;
                              data.modules[key1].subModules[key2].permission= 2;
                              callback();
                              }
                            else
                             callback();
                              
                        }
                    
              
              });
              },
            
          function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                    callback1();
              }
                 });
        },
       function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                    getSubModule(req,res,9,data,roles,level,moduleEntry,submodules);
              }
        
        });
        }
      else if(level == 3){
          var moduleArray = data.modules;
          async.forEachOf(moduleArray,function(value,key1,callback1) {
               var submoduleArray = moduleArray[key1].subModules;
                 async.forEachOf(submoduleArray,function(value,key2,callback2) {
                    var subsubmoduleArray = submoduleArray[key2].subModules;
                 async.forEachOf(subsubmoduleArray,function(value,key3,callback) {
                    var submoduleid = subsubmoduleArray[key3].id;
                    
                  
                utils.moduleName(submoduleid,function(err,results) {
                 if(err){ processStatus(req,res,6,data);    }
                  else {
                         
                            if(results){  data.modules[key1].subModules[key2].subModules[key3].name = results;
                              data.modules[key1].subModules[key2].subModules[key3].permission= 2;
                              callback();
                              }
                            else
                             callback();
                              
                        }
                    
              
              });
              },
            
          function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                    callback2();
              }
                 });
        },
        
         function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                    callback1();
              }
                 });
        },
       function(err){
                  if(err)
                    processStatus(req,res,6,data);
                   else
                    {
                    getSubModule(req,res,9,data,roles,level,moduleEntry,submodules);
              }
        
        });
   
       }
       else
        getSubModule(req,res,9,data,roles,level,moduleEntry,submodules);
}

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Roles/getUserModules');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
>>>>>>> DEV/master
