function processRequest(req,res,data){
    if( typeof req.body.productId === 'undefined' || typeof req.body.tabId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getHeader(req,res,9,data);
}


function getHeader(req,res,stat,data){
    var uid = data.userId;
    var productid = req.body.productId;
    var tabid = req.body.tabId;
    var pageid = req.body.pageId;
    
    data.productId = productid;
    data.tabId = tabid;
    data.pageId = pageid;
    
    var async = require('async');
    var utils = require('./roleutils');
    var isError = 0;
    var statList = {};
    
     async.parallel({
         tabHead: function(callback){utils.tabHeader(productid,tabid,callback); } 
         },
         function(err,results) {
			  if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.tabHead.errorCode != 0) {
              processStatus (req,res,results.tabHead.errorCode,data);
              isError = 1;
              return;
             }
              
          if(isError == 0) {
              data.productHeader = results.tabHead.headerList;
              processStatus(req,res,9,data);
            }
          }); 
}
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Roles/getProductHeader');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;



