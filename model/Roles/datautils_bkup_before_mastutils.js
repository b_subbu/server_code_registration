function getTabData(oid,pid,tid,pgid,slist,callback){
  var size = 20; //setting common for all pages override individually if needed
  switch(pid){
     case 140: //alert note: hard-coded 
       switch(tid){
         case 501:
         case 502:
            getAlertContentCount(oid,pid,tid,pgid,size,slist,callback);
            break;
            
         default:
            callback(1,null);
            break;
       }
      break;
     
     case 141:
       var data = {};
       data.errorCode = 0;
       data.resultCount = 0;
       data.resultArray = [];
       callback(null,data);
       break;
       
    default:
       callback(1,null);
       break;
  
  
  }
} 

function getStatList(pid,tid,slist,callback){
  switch(pid){
     case 140: //alert note: hard-coded 
       switch(tid){
         case 501:
         case 502:
            getAlertContentStatus(pid,tid,slist,callback);
            break;
            
         default:
            break;
       
       
       }
       break;
       
       case 141:
         callback(null,null);
       break;
  
       
    default:
       break;
  
  
  
  
  
  
  
  
  }
} 

function getAlertContentCount(oid,pid,tid,pgid,size,slit,callback){
   var totCount = 0;
   var data = {};
  
    var promise = db.get('alert_master').count({$and:[{org_id:oid},{alert_status_id: {$in: slit}}]});
    promise.on('complete',function(err,doc){
      totCount = doc;
      if(totCount == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = 0;
       callback(null,data);
       }
      else {
         getAlertContentData(oid,pid,tid,pgid,size,slit,totCount,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

//if this file grows too big then seperate out one file per Product
//or product+tab
function getAlertContentData(oid,pid,tid,pgid,size,slit,totalCount,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
   var promise = db.get('alert_master').find({$and:[{org_id:oid},{alert_status_id: {$in: slit}}]},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
        if(tid == 501)
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_date).format('DD-MM-YYYY'),alertType:doc.alert_type_id,status:doc.alert_status_id,target:doc.content_target,views:"0"}
        if(tid == 502)
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_time).format('DD-MM-YYYY'),alertType:doc.alert_type_id,status:doc.alert_status_id}
        
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
        //date probably should check with alert_status table to get date of the given status
        //Views will come only after we complete APP so keep it zero until 
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = totalCount;
       data.resultArray = resultArray;
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.resultCount = totalCount;
         getAlertTypeData(data,resultArray,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function getAlertTypeData(data,resultArray,callback){
   var totalRows =0;
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    
  
  async.forEachOf(resultArray,function(value,key,callback1){
  var alert_typeid = resultArray[key].alertType;
    mastutils.masterNames('alert_type_master',alert_typeid,function(err,result){
      if(err) callback(6,null);
      else
        resultArray[key].alertType = result.typeName;
        callback1();
    
        });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           data.resultArray = resultArray;
           getAlertStatData(data,resultArray,callback);
          }
     });
     
} 


function getAlertStatData(data,resultArray,callback){
   var totalRows =0;
    var async = require('async');
  
  async.forEachOf(resultArray,function(value,key,callback1){
   var statid = resultArray[key].status;
      
   var promise = db.get('alert_status_master').find({alert_status_id: statid});
   promise.each(function(doc){
        resultArray[key].status = doc.alert_status;
 
        }); 
   promise.on('complete',function(err,doc){
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           data.resultArray = resultArray;
           callback(null,data);
           }
     });
} 



function getAlertContentStatus(pid,tid,statlist,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key];    
   var promise = db.get('product_tab_status_action').find({$and:[{product_id:pid},{tab_id:tid},{stat_id:statid}]});
   promise.each(function(doc){
   var tmpArray = {id:doc.action_id};
      resultArray.push(tmpArray);
        }); 
   promise.on('complete',function(err,doc){
      var tmpArray = {id: statid, name:"ABC"};
       tmpArray.availableActions = resultArray;
       finalArray.push(tmpArray);
     
       resultArray = [];
     
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           //data.result = finalArray;
           //callback(null,finalArray);
           getAlertStatusName(finalArray,callback);
           }
     });
} 


function getAlertStatusName(statlist,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key].id;    
   var promise = db.get('alert_status_master').find({alert_status_id:statid});
   promise.each(function(doc){
        statlist[key].name = doc.alert_status;
        }); 
   promise.on('complete',function(err,doc){
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           //data.result = finalArray;
           //callback(null,statlist);
            getAlertNextActionName(statlist,callback);
           }
     });
} 


function getAlertNextActionName(statlist,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key].id;
   var actionlist = statlist[key].availableActions;    
  async.forEachOf(actionlist,function(value1,key1,callback2){
   var actionid = actionlist[key1].id;    
   
   var promise = db.get('item_master').find({item_id:actionid});
   promise.each(function(doc){
        statlist[key].availableActions[key1].name = doc.item_name;
        statlist[key].availableActions[key1].title = doc.item_title;
        statlist[key].availableActions[key1].sequence= doc.item_seq;
        }); 
   promise.on('complete',function(err,doc){
      callback2();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
     function(err){
      if(err)
             callback(1,null);
      else
          {
           //data.result = finalArray;
           callback1();
           
           }
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           //data.result = finalArray;
           callback(null,statlist);
            }
     });
} 


function getWorkQueues(tblName,callback){
   var totalRows =0;
   var resultArray = [] ;
   var promise = db.get(tblName).find({});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {id: doc.workQueueId,name:doc.workQueueName};
        resultArray.push(tmpArray);
      }); 
   promise.on('complete',function(err,doc){
       callback(null,resultArray);
     });
   promise.on('error',function(err){
       callback(6,null);
     });
} 


exports.tabData = getTabData;
exports.statList = getStatList;
exports.workQueues = getWorkQueues;
