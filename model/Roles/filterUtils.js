function getFilters(key_name,statList,pid,tid,callback) {
  
  switch(key_name){
     case 'status':
       getStatusFilter(statList,callback);  
        break;
     case 'alertType':
       getAlertFilter(callback);
       break;
    default:
       callback(null,null);
       break;
  }
  
}

//for status no need to again query db since statList is already passed
//and it varies from tab to tab for other filters we can just query master
//and return

function getStatusFilter(mastData,callback) {
   var async = require('async');
   //var statutils = require('../Menu/statusutils');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
   var data = {};
     
      async.forEachOf(mastData,function(key1,value1,callback1){
        var statID=key1;
        getItemStatName(statID,function(err,result) {
           if(err){ isError =1; callback1();}
         else {
                 var tmpArray = {id:statID, name:result};
                 itemMasters.push(tmpArray);
                  callback1(); 
              }
           }); 
          },
         function(err){
       if(err){
            callback(6,null);      }
      else {
        callback(null,itemMasters);
	      }
      });
 

}

function getAlertFilter(callback){
    
    var async = require('async');
    var mastutils = require('./../Registration/masterUtils');
    var type = require('type-detect');
  
    var isError = 0;
   
     
        mastutils.masterTypes('alert_type_master',function(err,result) {
         if(err) return callback(err);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                       alertTypeList = result.types;
                    
                  }
                  callback(null,alertTypeList);
                  }
            
        });

}

//can reuse statutils of menu if we make alert_status_id to status_id
//TODO later
function getItemStatName(statid,callback){
   var totalRows =0;
   var statName = '';
   var mastutils = require('../Registration/masterUtils');
   var tblName = 'alert_status_master';
   
   mastutils.masterStatusNames(tblName,statid,function(err,result) {
           if(err) callback(6,null);
         else {
                statName = result.statusName;
                callback(null,statName); 
              }
        });
} 

 exports.filterOptions = getFilters;
