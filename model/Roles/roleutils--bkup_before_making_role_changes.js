function checkUserStatus(uid,callback){
   var totalRows =0;
   var data = {};
   
   var promise = db.get('user_master').find({_id:uid},{stream: true});
   promise.each(function(docuser){
        totalRows++;
        data.userStatus = docuser.user_status;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //user not found
       callback(null,data);
       }
     else if(totalRows > 1) {
        data.errorCode = 6;//system error  
        callback(null,data)
       }
      else {
         data.errorCode = 0; //no error
         getUserDetails(uid,data,callback); 
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function getUserDetails(uid,data,callback){
   var totalRows =0;
   
   var promise = db.get('user_detail').find({_id:uid},{stream: true});
   promise.each(function(docuser){
        totalRows++;
        data.orgId = docuser.org_id;
        data.bizType = docuser.bus_type;
		data.baseCurrency = docuser.currency_id;
		data.businessEmail = docuser.bus_email;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 6; //system error
       callback(null,data);
       }
     else if(totalRows > 1) {
        data.errorCode = 6;//system error  
        callback(null,data)
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function checkUserRoles(uid,callback){
   var totalRows =0;
   var data = {};
   var rolesArray = [];
   
   var promise = db.get('user_role_link').find({user_id:uid},{sort: {role_seq:1}});
   promise.each(function(docuser){
        totalRows++;
        var tmpArray = {roleId:docuser.role_id,roleseq:docuser.role_seq};
        rolesArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 3; //No Roles status message
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.rolesArray = rolesArray;
         callback(null,data); 
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

/**
function getRoleModules(roleid,callback){
   var totalRows =0;
   var data = {};
   var rolesArray = [];
   
   //note: module ids 1-100 are modules others are submodules
   //limit to only modules here to avoid dump of everything
   var promise = db.get('role_module_permission').find({$and: [{role_id:roleid},{module_id: {$lt: 100}}]},{sort: {module_seq:1}});
   promise.each(function(docuser){
        totalRows++;
        var tmpArray = {id:docuser.module_id,permission:docuser.permission,sequence:docuser.module_seq};
        //var tmpArray = [docuser.module_id,docuser.permission,docuser.module_seq];
       //return in the format as required by frontend developer
        
        rolesArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
         data.errorCode = 0; //Not error just this role does not have modules
         callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.rolesArray = rolesArray;
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function getRoleModule(roleid,moduleid,callback){
   var totalRows =0;
   var data = {};
   var rolesArray = [];
   
   var promise = db.get('role_module_permission').find({$and: [{role_id:roleid},{module_id:moduleid}]},{sort: {module_seq:1}});
   promise.each(function(docuser){
        totalRows++;
        //var tmpArray = {roleId:docuser.role_id,moduleId:docuser.module_id,permission:docuser.permission,moduleseq:docuser.module_seq};
        var tmpArray = [docuser.module_id,docuser.permission,docuser.module_seq];
       //return in the format as required by frontend developer
         
        rolesArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
         data.errorCode = 0; //Not error just this role does not have modules
         callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.rolesArray = rolesArray;
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

**/

function getModuleName(moduleid,callback){
   var totalRows =0;
   var moduleName = "";
   
   var promise = db.get('module_master').find({module_id:moduleid});
   promise.each(function(docuser){
        totalRows++;
        moduleName = docuser.module_name;
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
          callback(null,null); //not error just modulename becomes null
       }
      else{
          callback(null,moduleName);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,null);
     });
} 


function getSubModules(moduleid,callback){
   var totalRows =0;
   var moduleName = "";
   var data = {};
   var submodulesArray = [];
   var promise = db.get('module_submodule_link').find({module_id:moduleid},{sort: {sub_module_seq:1}});
   promise.each(function(docuser){
        totalRows++;
        var tmpArray = {id:docuser.sub_module_id,sequence:docuser.sub_module_seq};
       
        submodulesArray.push(tmpArray);
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
          data.errorCode = 0;
          callback(null,data);
       }
      else{
          data.errorCode = 0;
          data.subModules = submodulesArray;
          callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 


function getModuleNamePermission(moduleid,roles,callback){
   var totalRows =0;
   var moduleName = "";
   
   var promise = db.get('module_master').find({module_id:moduleid});
   promise.each(function(docuser){
          totalRows++;
          moduleName = docuser.module_name;
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0)
          getRoleModuleList(roles,moduleid,'',callback);//modulename becomes null
      else 
          getRoleModuleList(roles,moduleid,moduleName,callback)
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,null);
     });
} 

function getRoleModuleList(roleid,moduleid,modulename,callback){
   var totalRows =0;
   var data = {};
   var modulepermission = "";
   
   var promise = db.get('role_module_permission').find({$and: [{role_id:roleid},{module_id:moduleid}]});
   promise.each(function(docuser){
        totalRows++;
        modulepermission = docuser.permission; //it must be only 1 row for role+module or error
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
          data.errorCode = 0; //Not error this role does not have modules
          data.moduleName = modulename;
          data.modulePermission = 0; //undefined item is zero--dont show
          callback(null,data);
       }
      else {
         data.errorCode = 0;
         data.moduleName  =modulename;
         data.modulePermission = modulepermission;
         callback(null,data);
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function getTabsList(moduleid,callback){
      var totalRows =0;
   var data = {};
   var itemsArray = {};
   var itemArray = [];
   
   
   var promise = db.get('module_item_link').find({module_id:moduleid});
   promise.each(function(docuser){
        //totalRows++;
          if(docuser.tab_id == "0")
             {  var tmpArray = {id: docuser.item_id};
                  
               itemArray.push(tmpArray);
               totalRows++;
              }
          else{
         ;
            } 
          });
        promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //Not error this module does not have items 
       callback(null,data);
       }
      else {
          data.errorCode = 0;
          itemsArray.items=itemArray;
          getTabName(moduleid,data,itemArray,itemsArray,callback)
      } 
     });
    promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 



function getItemsList(moduleid,tabid,callback){
   var totalRows =0;
   var data = {};
   var itemsArray = {};
   var itemArray = [];
   
   
   var promise = db.get('module_item_link').find({$and:[{module_id:moduleid},{tab_id:tabid}]});
   promise.each(function(docuser){
        totalRows++;
          if(docuser.tab_id == 0)
              ;
          else{
          var tmpArray = {id:docuser.item_id};
             itemArray.push(tmpArray);
            } 
          });
        promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //Not error this module does not have items 
       callback(null,data);
       }
      else {
         getItemDetail(roleid,moduleid,data,itemArray,itemsArray,callback)
      } 
     });
    promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function getTabName(moduleid,data,itemArray,itemsArray,callback1){
   var async = require('async');
   var isError = 0;
    
      async.forEachOf(itemsArray.items,function(value,key,callback){
       var itemid = itemsArray.items[key].id;
  
       
       getItemName(itemid,function(err,result) {
         if(err) return callback(err);
         else {
            if(result)
                {
                  itemsArray.items[key].name = result.name;
                  itemsArray.items[key].title = result.title;
                  itemsArray.items[key].sequence = result.seq;
                 }
            callback();
      
           }
        });
   
      },
       function(err){
      if(err)
             callback1(1,null);
      else
          {
           itemsArray.itemActions=itemArray;
           callback1(null,itemsArray);
           }
     });


} 

function getItemDetail(moduleid,data,itemArray,itemsArray,callback1){
   var async = require('async');
   var isError = 0;
    
    async.forEachOf(itemArray,function(value,key,callback){
       var itemid = itemArray[key].id;
       
       getItemName(itemid,function(err,result) {
         if(err) return callback(err);
         else {
            if(result)
                {
                  itemArray[key].name = result.name; //item name null is not an error
                  itemArray[key].title = result.title;
                  itemArray[key].sequence = result.seq;
                  itemArray[key].itemlevel = result.wi_item;
                  
              }
                
             callback(); //next item in foreachof
           }
        });
        },
      function(err){
      if(err)
             callback1(1,null);
      else
          {
           itemsArray.itemActions=itemArray;
           callback1(null,itemsArray);
           }
     });
} 

function getItemPermissionList(roleid,moduleid,callback){
   var totalRows =0;
   var data = {};
   var itemsArray = [];
   
   
   var promise = db.get('module_item_link').find({module_id:moduleid});
   promise.each(function(docuser){
          totalRows++;
          var tmpArray = [moduleid,docuser.item_id,docuser.item_seq];
          itemsArray.push(tmpArray);
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; 
       callback(null,data);
       }
      else {
         getItemPermissionDetail(roleid,moduleid,itemsArray,callback)
       } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function getItemPermissionDetail(roleid,moduleid,itemArray,callback1){
   var async = require('async');
   var isError = 0;
    
    async.forEachOf(itemArray,function(value,key,callback){
       var itemid = itemArray[key][1];
       
       getItemName(itemid,function(err,result) {
         if(err) return callback(err);
         else {
            if(result)
                  itemArray[key][3] = result; //item name null is not an error
             callback(); //next item in foreachof
           }
         });
        },
      function(err){
      console.log(err);
      if(err)
          callback1(1,null);
      else
         getItemPermissions(roleid,moduleid,itemArray,callback1);
      });
} 

function getItemPermissions(roleid,moduleid,itemArray,callback1){
   var async = require('async');
   var isError = 0;
    
    async.forEachOf(itemArray,function(value,key,callback){
       var itemid = itemArray[key][1];
       
       getItemPermission(roleid,itemid,function(err,result) {
         if(err) return callback(err);
         else {
                 itemArray[key][4] = result; //if null is handled as zero
                 callback(); //next item in foreachof
             }
          });
        },
      function(err){
      if(err)
          callback1(1,null);
      else
          callback1(null,itemArray);
      });
} 

function getItemPermission(roleid,itemid,callback){
   var totalRows =0;
   var itempermission = 0;
   
    var promise = db.get('role_item_permission').find({$and: [{role_id:roleid},{item_id:itemid}]});
    promise.each(function(docuser){
        totalRows++;
        itempermission = docuser.permission; //it must be only 1 row for role+item or error
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) 
          callback(null,0); //not defined item is zero permission-->dont show
       else 
         callback(null,itempermission);
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function getItemName(itemid,callback){
   var totalRows =0;
   var moduleName = "";
   var resultArray = {};
   
   var promise = db.get('item_master').find({item_id:itemid});
   promise.each(function(docuser){
        totalRows++;
        resultArray = {name:docuser.item_name,title:docuser.item_title,seq:docuser.item_seq,wi_item:docuser.wi_level};
         }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) 
        callback(null,null);
      else 
        callback(null,resultArray);
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,null);
     });
} 

//Note table product_tab_header is not in normalized form
//this is to reduce 1 query to get details
//also this is not used anywhere else, so ok
function getTabHeader(prodid,tabid,callback){
   var totalRows =0;
   var tabsArray = [];
   var data = {};
   //var resultArray = {};
   
    var promise = db.get('product_tab_header').find({$and: [{product_id:prodid},{tab_id:tabid}]},{sort: {ele_seq:1}});
    promise.each(function(doc){
        totalRows++;
        var tmpArray = {id:doc.ele_id,name:doc.ele_name,title:doc.ele_title,isRangeSearch:doc.isRange,searchable:doc.isSearch,filterable:doc.isFilter,hidden:doc.isHidden,type:doc.ele_type,seq:doc.ele_seq};
       tabsArray.push(tmpArray);
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) 
         {data.errorCode = 5;
          callback(null,data); //Header not defined error
          }
       else{  
          data.errorCode = 0;
          data.headerList = tabsArray;
          callback(null,data);
         }
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 


//Note table product_tab_status 
//status column is same as individual entity status
//like for Alert product Id the status will be same from alert_status_master alert_ids
//this function is to define status types per given tab wise
//So names should be got from alert_status_master in datautils (non-generic)
function getTabStatus(prodid,tabid,callback){
   var totalRows =0;
   var resultArray = [];
   var data = {};
   
    var promise = db.get('product_tab_status').find({$and: [{product_id:prodid},{tab_id:tabid}]},{sort: {stat_seq:1}});
    promise.each(function(doc){
        totalRows++;
        resultArray.push(doc.stat_id);
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) 
          {
          data.errorCode = 7;
          callback(null,data); //No Statuses error
          }
      else{  
          data.errorCode = 0;
          data.statusList = resultArray;
          callback(null,data);
         }
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function checkOrgStatus(orgId,callback){

  var totalRows = 0; 
  var orgActive = 0;
  var userId = '';
  var data = {};
  
   debuglog("OrgId check if active");
	 debuglog("i.e., the system user of the given orgId must be active to consider orgId is active");
     var promise = db.get('user_master').find({$and:[{org_id:orgId},{system_user:true}]});
     promise.each(function(doc){
       totalRows++;
       orgActive = doc.user_status;
	   userId = doc._id;
     });
     promise.on('complete',function(err,doc){
       if(totalRows == 0 || orgActive != 9) {
		 data.errorCode = 2;		 
         callback(null,data);
        }
      else {
         debuglog("orgId is fine so let us check userId");
		      getUserDetails(userId,data,callback); 
          }
     });
     promise.on('error',function(err){
      debuglog(err);
     callback(6,null);
    });
  }

function getUserRole(userId,callback){

var roleId = 0;
var totalRows = 0;

var promise = db.get('user_detail').find({user_id: userId});
promise.each(function(doc){
   roleId = doc.role_id;
   totalRows++;
});
promise.on('complete',function(err,doc){
 if(totalRows != 1){
   callback(6,null);
   }
  else{
     callback(null,roleId); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});
}


function getRoleModules(tblName,roleId,callback){

var modules = [];
var totalRows = 0;
var promise = db.get(tblName).find({roleId: roleId});
promise.each(function(doc){
   modules = doc.productList;
   totalRows++;
});
promise.on('complete',function(err,doc){
 if(totalRows != 1){
   callback(6,null);
   }
  else{
     callback(null,modules); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});
}


exports.usrStatus = checkUserStatus;
exports.usrRoles = checkUserRoles;
exports.moduleName = getModuleName;
exports.subModules = getSubModules;
exports.moduleNamePermission = getModuleNamePermission;
exports.itemList = getItemsList; 
exports.itemPermissionList = getItemPermissionList;
exports.tabsList = getTabsList;
exports.tabHeader = getTabHeader;
exports.tabStatus = getTabStatus;
exports.orgStatus = checkOrgStatus;
exports.userRole = getUserRole;
exports.roleModules = getRoleModules;
