var masterTables = ['action_type_master','alert_status_master','alert_type_master','business_type_master','content_type_master','item_master','module_item_link','module_master','module_submodule_link','product_tab_header','product_tab_status','product_tab_status_action','role_item_permission','role_master','role_module_permission','rolegroup_master','rolegroup_role_link','status_master'];

var txTables = ['alert_actions','alert_images','alert_master','alert_release','alert_release_arc','alert_sequence','alert_status','cust_following','cust_master','invitation_sequence','invite_master','service_status','srcn_sequence','user_detail','user_master','user_otp','user_role_link','user_rolegroup_link'];

//mongo --verbose alert_master_create_script.js


function dumpMasterTables(req,res){
 var async = require('async');
  
 async.forEachOf(masterTables,function(key,value,callback){
        console.log(key);
		console.log(value);

        dumpMasterTable(key,callback);
		callback();
    },
    function (err) {
        // 5 seconds have passed
		res.send("success");
    }
);
}
function dumpMasterTable(tblName,callback){
	  require('shelljs');
	console.log("Inside dumpTable Master ");
	 var args = ['--db', 'scarabDB', '--collection', tblName, "--out", '/tmp/']
      , mongodump = spawn('/usr/local/bin/mongodump', args);
    mongodump.stdout.on('data', function (data) {
      console.log('stdout: ' + data);
    });
    mongodump.stderr.on('data', function (data) {
      console.log('stderr: ' + data);
    });
    mongodump.on('exit', function (code) {
      console.log('mongodump exited with code ' + code);
    });
  }

exports.getData = dumpMasterTables;


