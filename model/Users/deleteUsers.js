function processRequest(req,res,data){
   if(typeof req.body.userCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkUserStatus(req,res,9,data);
}



function checkUserStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var UserArr = req.body.userCodes;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.userCodes = UserArr;
    
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tblName = orgId+'_users'; //TODO: move to a common definition file

    async.forEachOf(UserArr,function(key,value,callback1){
      var UserData = extend({},data);
          UserData.users = {};
          UserData.users.userCode = key;
      debuglog("start checking if ID exists in Revision table before delete");
      debuglog("delete is possible only in main table");
      debuglog(UserData)    
     datautils.checkUserCode(tblName,UserData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
          else{
                  if (result.status == 80){
                   debuglog("This is entered status item");
                   debuglog("So can be deleted directly");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 82){
                   debuglog("This is authorised status item");
                   debuglog("So has to go through authoristation to delete");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Delete is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();    
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
            else{
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              deleteUsers(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
               }
          }
      });
}


function deleteUsers(req,res,stat,data,enteredItems,authorisedItems){
    
     
    debuglog("Now start deleting the entries in Main Table");
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var mainTable    = orgId+'_users';
    
    async.forEachOf(enteredItems,function(key,value,callback1){
      userId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteUserCode(mainTable,userId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
               processStatus(req,res,6,data);
            }
      else
          {
          if(authorisedItems.length > 0){
           debuglog("This request has some authorised items too");
           debuglog(authorisedItems);
           deleteAuthoriseUsers(req,res,9,data,authorisedItems);
          }
          else{
           debuglog("This request has no authorised items");
           debuglog("so call return");
           processStatus(req,res,9,data); 
           }           
        }
     });
}


function deleteAuthoriseUsers(req,res,stat,data,authorisedItems){
    var uid = req.body.userId;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_users';
    var revisionTable = data.orgId+'_Revision_users';
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      userId = key;
     async.parallel({
          relStat: function (callback){datautils.copyUserHeader(mainTable,revisionTable,userId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           debuglog("Change status of items in revision table to ready2Delete");
           changeUserStatus(req,res,9,data,authorisedItems,mainTable,revisionTable);
          }
      });
}

function changeUserStatus(req,res,stat,data,UserArr,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(UserArr,function(key,value,callback1){
    userId = key;
    var reason = 'User Delete Submitted';
       statutils.changeUserStatus(revisionTable,data.orgId,data.businessType,data.userId,userId,89,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getUserDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getUserDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.UsersData(mainTable,data.userCodes,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getUserStatusName(req,res,9,data,result); 
         }
     });
    
}

function getUserStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('user_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.userData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;






