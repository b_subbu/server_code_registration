function processRequest(req,res,data){
debuglog("This function is to just make model standardized across all");
debuglog(data);
debuglog("Now check master status is moved to controller itself");
debuglog("Just maintaing some values for compatibility issues")
getRoleId(req,res,9,data);
}


function getRoleId(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var RoleData = extend({},data);
       RoleData.roles = req.body.roleDetails;
       RoleData.status_id = 80;
      
     var tblName = data.orgId+'_roles'; //create will always be in main table
      datautils.generateRoleId(RoleData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                debuglog("Generated new Role ID");
                 debuglog(result);
                 data.roleId = result;
                 RoleData.roleId = result;
                 checkRoleName(req,res,9,data,RoleData,tblName);
             }
        });
} 


function checkRoleName(req,res,stat,data,RoleData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     debuglog("Check if the  Role name already exists or not");
     debuglog("in master and revision tables since same name can create confusion");
     debuglog("While checking we are checking both system and business rolenames");
     debuglog("so that business can inadvertenly create a system role");
     debuglog(RoleData);
     debuglog(req.body);
      datautils.checkRoleName(RoleData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 if(result.errorCode == 1)
                   processStatus(req,res,5,data);
                 else
                   saveRoleData(req,res,9,data,RoleData,tblName);
             }
        });
} 


function saveRoleData(req,res,stat,data,RoleData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   
     RoleData.roles.roleType = 2;
     
     debuglog("This is after getting data from request");
     debuglog("All Roles created by user will be Business type only");
     debuglog(RoleData);
     debuglog(data);
     
    var submitop = req.body.submitOperation;
  
   
    if(   RoleData.roles.roleName == null
       || RoleData.roles.roleName == 'undefined'
       || Array.isArray(RoleData.roles.productList) == false
      )
        processStatus(req,res,4,data);
    else{
    
     datautils.RoleUpdate(tblName,data.roleId,RoleData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(RoleData);
             if(submitop == 1){
             var roleIds = [];
             roleIds.push(data.roleId);
             data.roleIds = roleIds;
               var submitRole = require('./submitRoles');
               submitRole.moveToRevision(req,res,9,data);
             }
             else{
                changeRoleStatus(req,res,9,data,RoleData,tblName);
                }
            }
         });
        }
} 


function changeRoleStatus(req,res,stat,data,RoleData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeRoleStatus(tblName,data.orgId,data.businessType,data.userId,data.roleId,80,'New Business Role Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getRoleData(req,res,9,data,RoleData,tblName);
               }
        });
} 

function getRoleData(req,res,stat,data,RoleData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   
    debuglog("Start getting data");
    debuglog(data);
    
    var roleArr = [];
    roleArr.push(data.roleId) 
    datautils.RolesData(tblName,roleArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getRoleStatusName(req,res,9,data,result); 
         }
     });
    
}

function getRoleStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('role_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.roleData = pgs;
            processStatus(req,res,9,data);
           }
      });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;












