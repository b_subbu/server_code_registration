function processRequest(req,res,data){
  if( typeof req.body.roleId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getRoleData(req,res,9,data);
}


function getRoleData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
   data.roleId = req.body.roleId;
   var tid = req.body.tabId;
    
    if(tid == 502)
     var tabl_name = data.orgId+'_Revision_roles';
    else
     var tabl_name = data.orgId+'_roles';
   
     var extend = require('util')._extend;
     var RoleData = extend({},data);
         RoleData.roles = {};
         RoleData.roles.roleId = data.roleId;    
     
  
      datautils.RoleData(tabl_name,RoleData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
               processStatus(req,res,3,data);
               debuglog("result errorCode is 1");
              }
            else {
                var roleDetails = {};
                roleDetails.roleId   = result.roleId;
                roleDetails.roleName = result.roleName;
                roleDetails.roleType = result.roleType;
                roleDetails.productList   = result.productList;
                roleDetails.status     = result.status;
                roleDetails.statusId   = result.statusId;
                data.roleDetails = roleDetails;   
                processStatus(req,res,9,data);
                debuglog("No error case");
                debuglog(roleDetails);
            }
          }
        });
} 

function getProduct(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');
 

 var tblName = 'product_master';
 
  datautils.products(tblName,data.businessType,function(err,result) {
           debuglog(result);
           debuglog("after getting result for product");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.products = result[0].products;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;











