function processRequest(req,res,data){
   if( typeof req.body.roleIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkRoleStatus(req,res,9,data);
}

function checkRoleStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var roleArr = req.body.roleIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.roleIds = roleArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_roles'; //TODO: move to a common definition file

    async.forEachOf(roleArr,function(key,value,callback1){
        var RoleData= {};
            RoleData.roles = {};
            RoleData.roles.roleId = key;
        datautils.checkRoleCode(tabl_name,RoleData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });       }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToRevision(req,res,9,data);
          }
      });
}

function moveToRevision(req,res,stat,data){
    
    var RoleArr = data.roleIds;
    
    debuglog("Now start moving the entries to Revision Table");
    debuglog(RoleArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_roles'; //TODO: move to a common definition file
    var mainTable    = orgId+'_roles';
   
    async.forEachOf(RoleArr,function(key,value,callback1){
      roleId = key;
     async.parallel({
          relStat: function (callback){datautils.moveRoleHeader(mainTable,revisionTable,roleId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeRoleStatus(req,res,9,data,RoleArr,mainTable,revisionTable); 
         }
     });
}

function copyToRevision(req,res,stat,data,RoleData){
    
    
    debuglog("Even though this called Copy we are actually creating a new entry");
    debuglog(RoleData);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_roles'; //We will create the new entry in Revision Table
   
       if(   RoleData.roles.roleName == null
       || RoleData.roles.roleName == 'undefined'
       || Array.isArray(RoleData.roles.productList) == false
      )
   processStatus(req,res,4,data);
    else{
         datautils.RoleUpdate(revisionTable,data.roleId,RoleData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(RoleData);
                var RoleArr = [];
                RoleArr.push(RoleData.roles.roleId);
                changeRoleStatus(req,res,9,data,RoleArr,'',revisionTable);
                }
        });
      }
}


function changeRoleStatus(req,res,stat,data,RoleArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(RoleArr,function(key,value,callback1){
    roleId = key;
    var reason = '';
       statutils.changeRoleStatus(revTable,data.orgId,data.businessType,data.userId,roleId,81,'Role Submitted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.roleIds = RoleArr;
                 getRoleDetails(req,res,9,data,revTable);
             }
          });
     
}


function getRoleDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.RolesData(revTable,data.roleIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getRoleStatusName(req,res,9,data,result); 
         }
     });
    
}

function getRoleStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('role_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.roleData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

   
exports.getData = processRequest;
exports.moveToRevision=moveToRevision; 
exports.copyToRevision=copyToRevision;




