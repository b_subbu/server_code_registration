function processRequest(req,res,data){
        getRoleId(req,res,9,data);
}


function getRoleId(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
      
             
   var extend = require('util')._extend;
   var RoleData = extend({},data);
       RoleData.roles = req.body.roleDetails;
       RoleData.status_id = 80;
    
        data.roleId = RoleData.roles.roleId;
       RoleData.roles.roleType = 2;
             
      
     var tblName = data.orgId+'_roles'; 
     debuglog("Check if the  Id exists or not");
     debuglog(RoleData);
     debuglog(req.body);
      datautils.checkRoleCode(tblName,RoleData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
               if(result.errorCode == 0) 
                 processStatus(req,res,3,data);
               else{
                debuglog("If no row exists it is an error for both entered and authorised");
                debuglog("If a row exists and status is Authorised then only submit is allowed");
                debuglog("But if a row exists and status is Entered both save and submit are allowed");
                if(result.status == 82){
                  var submitop = req.body.submitOperation;
                  if(submitop != 1){
                  debuglog("This is a SAVE for Authorised row so throw error");
                  debuglog(result);
                  debuglog(submitop);
                  processStatus(req,res,5,data);
                  }
                  else{
                var submitRoles = require('./submitRoles');
               submitRoles.copyToRevision(req,res,9,data,RoleData);
               debuglog("This is different operation of copy because");
               debuglog("Here we are not actually moving the data but creating a ");
               debuglog("new entry in Pending Tab without touching Main tab");
              }
            }
              else
                 checkRoleNameEdit(req,res,9,data,RoleData,tblName);
            }
          }
        });
} 

function checkRoleNameEdit(req,res,stat,data,RoleData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     debuglog("Check if the  Role name already exists or not");
     debuglog("same as checkRoleName but will exclude current RoleId");
     debuglog(RoleData);
     debuglog(req.body);
      datautils.checkRoleNameEdit(RoleData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 if(result.errorCode == 1)
                   processStatus(req,res,5,data);
                 else
                   saveRoleData(req,res,9,data,RoleData,tblName);
             }
        });
} 

function saveRoleData(req,res,stat,data,RoleData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
    
     debuglog("This is after getting data from request");
     debuglog(RoleData);
     debuglog(data);
     
     data.roleId = RoleData.roles.roleId;
     
    var submitop = req.body.submitOperation;
  
   
     if(   RoleData.roles.roleName == null
       || RoleData.roles.roleName == 'undefined'
       || Array.isArray(RoleData.roles.productList) == false
      )
    processStatus(req,res,4,data);
    else{
    
     datautils.RoleUpdate(tblName,data.roleId,RoleData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(RoleData);
             if(submitop == 1){
             var roleIds = [];
             roleIds.push(data.roleId);
             data.roleIds = roleIds;
               var submitRoles = require('./submitRoles');
               submitRoles.moveToRevision(req,res,9,data);
             }
             else{
                changeRoleStatus(req,res,9,data,RoleData,tblName);
                }
            }
          });
        }
} 


function changeRoleStatus(req,res,stat,data,RoleData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeRoleStatus(tblName,data.orgId,data.businessType,data.userId,data.roleId,80,'Role Edited',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getRoleData(req,res,9,data,RoleData,tblName);
               }
        });
} 

function getRoleData(req,res,stat,data,RoleData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
    
    var RoleArr = [];
    RoleArr.push(data.roleId) 
   datautils.RolesData(tblName,RoleArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getRoleStatusName(req,res,9,data,result); 
         }
     });
    
}

function getRoleStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('role_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.roleData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 



function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;








