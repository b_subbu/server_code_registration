function changeRoleStatus(tablName,orgId,bizType,operatorId,roleId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {roleId: roleId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createRoleStatusChngLog(orgId,bizType,operatorId,roleId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createRoleStatusChngLog(orgId,bizType,operatorId,roleId,stat,reason,callback){
 
  var promise = db.get('role_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    roleId: roleId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }

function changeUserStatus(tablName,orgId,bizType,operatorId,userId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                       {userId: userId}, 
                                       {$set: {
                                             status_id: stat,
                                             update_time: new Date()
                                              }
                                       });
     promise.on('success',function(doc){
           createUserStatusChngLog(orgId,bizType,operatorId,userId,stat,reason,callback);
        });
     promise.on('error',function(err){
          console.log(err);
          callback(6,null);
      });
} 
 
 function createUserStatusChngLog(orgId,bizType,operatorId,userId,stat,reason,callback){
 
  var promise = db.get('user_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    userId: userId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }


 //TODO combine all these in single file just tblname and 1 column changes
exports.changeRoleStatus = changeRoleStatus;
exports.changeUserStatus = changeUserStatus;

