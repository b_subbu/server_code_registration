function processRequest(req,res,data){
   if(typeof req.body.roleIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkRoleStatus(req,res,9,data);
}

function checkRoleStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var RoleArr = req.body.roleIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.roleIds = RoleArr;
    
    var orgId = data.orgId;
    
    var tblName = orgId+'_Revision_roles'; //TODO: move to a common definition file

    async.forEachOf(RoleArr,function(key,value,callback1){
      var RoleData = extend({},data);
          RoleData.roles = {};
          RoleData.roles.roleId = key;
      debuglog("start checking if ID exists in Revision table before reject");
      debuglog(RoleData)    
     datautils.checkRoleCode(tblName,RoleData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             deleteRoles(req,res,9,data);
           }
      });
}



function deleteRoles(req,res,stat,data){
    
    var RoleArr = data.roleIds;
    
    debuglog("Now start deleting the entries in Revision Table");
    debuglog("Otherwise we will have 2 rows in main table with same id but different status");
    debuglog(RoleArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_roles'; //TODO: move to a common definition file
    var mainTable    = orgId+'_roles';
    
    async.forEachOf(RoleArr,function(key,value,callback1){
      roleId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteRoleCode(revisionTable,roleId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1) {
               processStatus(req,res,6,data);
            }
      else
          {
           changeRoleStatus(req,res,9,data,RoleArr,mainTable,revisionTable); 
         }
     });
}


function changeRoleStatus(req,res,stat,data,RoleArr,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(RoleArr,function(key,value,callback1){
    roleId = key;
    var reason = 'Delete Role Rejected';
       statutils.changeRoleStatus(revisionTable,data.orgId,data.businessType,data.userId,roleId,87,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getRoleDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getRoleDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.RolesData(mainTable,data.roleIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getRoleStatusName(req,res,9,data,result); 
         }
     });
    
}

function getRoleStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('role_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.roleData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;







