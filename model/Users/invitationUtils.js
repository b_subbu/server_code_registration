function getUserDetail(tblName,userData,callback){

var datautils = require('./dataUtils');
var data = {};

   datautils.UserData(tblName,userData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
                data.errorCode = 6;
                callback(null,data)
              }
         else {
             if(result.errorCode != 0 ) {
              data.errorCode = 6;
              callback(null,data)
              }
            else {
                userData.users.userCode   = result.userCode;
                userData.users.userName = result.userName;
                userData.users.userType = result.userType;
                userData.users.userEmail   = result.userEmail;
                userData.users.userPhone   =  result.userPhone;
                userData.users.userRole    =  result.userRole;
                userData.userMongo = result.userMongo;
                if(result.userMongo == null || typeof result.userMongo == undefined)
                  createUserMasterEntry(tblName,userData,callback);
                else
                 {
                 debuglog("This is a reactivating case");
                 updateUserMasterEntry(tblName,userData,callback);
                 } 
                debuglog("No error case");
                debuglog(userData);
            }
          }
        });



}


function createUserMasterEntry(tblName,userData,callback){

debuglog("Will create new user with business user and status as active");
debuglog(userData);

 var promise = db.get('user_master').insert({ 
     email:userData.users.userEmail,
     mobile: userData.users.userPhone,
     org_id: userData.orgId,
     system_user: false,
     activ_date: new Date(),
     user_status:9});
   promise.on('success',function(doc){
      userData.userMongo = doc._id;
      createUserDetailEntry(tblName,userData,callback);
    });
    promise.on('error',function(err){
      console.log(err);
      callback(6,null);//system error
    });
}

function updateUserMasterEntry(tblName,userData,callback){

debuglog("Will update user with fields to handle changes if any");
debuglog("orgId and active date does not change");
debuglog("Make upsert as true in case if they are recall invation and then invite again");
debuglog(userData);

 var promise = db.get('user_master').update(
    {_id:userData.userMongo},
    {$set:{ 
     email:userData.users.userEmail,
     mobile: userData.users.userPhone,
     system_user: false,
     org_id: userData.orgId,
     activ_date: new Date(),
     user_status:9}},
    {upsert:true} );
   promise.on('success',function(doc){
      createUserDetailEntry(tblName,userData,callback);
    });
    promise.on('error',function(err){
      console.log(err);
      callback(6,null);//system error
    });
}

function createUserDetailEntry(tblName,userData,callback){

   var async = require('async');
   var utils = require('../Registration/chanceUtils');
   var custUtils = require('../Render/utils');
   var isError = 0;
    
    var orgId = userData.orgId;

//call all functions async in parallel
//no error checking done now do at time of integration to admin dashboard


    async.parallel({
            passWord: function (callback) {utils.getPASSWORD(callback);},
            customer:function (callback) {custUtils.checkCustomerEmail(userData.users.userEmail,callback);}
    
        },
        function (err, results) {
            if (err) {
                console.log(err);
                processStatus(req, res, 6, data);//system error
                isError = 1;
                return;
            }
            else {
                debuglog(results);
               debuglog("If no customer record existing then update password as below");
               debuglog("If customer record exists but without password then updated business web");
               debuglog("And customer app with below password and username");
               debuglog("If customer record exists with password then update");
               debuglog("with that of below but here customername and username can be different");
               debuglog("Also set newpass as true and email as different since cant");
               debuglog("send decrypted password");
               debuglog("Note cust record is created with active as true and registration date in invitation module ");
               debuglog("In case if that was deactived active will become so isCustomer also false");
               debuglog("Password can be still changed but not an issue since anyhow customer is inactive");
              
               if(results.customer.isCustomer == false ||
                  results.customer.passWord   == null  ||
                  typeof results.customer.passWord   == 'undefined'){
                debuglog("Either customer is not found or inactive");
                debuglog("Or has downloaded APK but not yet registered");
             
                var pass_word = results.passWord;

                var email_text = "Dear ,<br/><br/>";
                email_text += "<br/>please find below details for login to your Koi account.<br/>";
                email_text += "<br/>Organization ID: " + orgId;
                email_text += "<br/>Password: " + pass_word;
                email_text += "<br/>Please change password on first login";
                email_text += "<br/>Thanks";

                var modelpath = app.get('model');
                var model = require(modelpath + 'email');
                model.sendmail(userData.users.userEmail, 'Account Activation mail', email_text);

                var hash = require('node_hash/lib/hash');
                var salt = 'mydime_##123##';
                var pass_word_enc = hash.sha256(pass_word, salt);
          
                debuglog("Update cust_record with name and password");
                debuglog("If no cust record then update fails and nothing happends");
                debuglog("TODO move this to standard functions since this is a quickfix");
               
                db.get('cust_master').update(
                    {cust_email: userData.users.userEmail},
                    {$set: {
                            cust_name: userData.users.userName,
                            pass_word: pass_word_enc,
                           }
                         },
                         {upsert:false}
                    );
               
          var userid_str = userData.userMongo + ''; //to convert objectid to string
          
          var promise = db.get('user_detail').update(
                        {_id: userData.userMongo}, 
                        {$set:
                           {_id: userData.userMongo,
                           user_id: userid_str,
                           user_name: userData.users.userName,
                           bus_email: userData.users.userEmail,
                           bus_mobile: userData.users.userPhone,
                           bus_type: userData.businessType,
                           regist_date: new Date(),
                           org_id: orgId,
                           pass_word: pass_word_enc,
                           role_id: userData.users.userRole.roleId,
                           activ_date: new Date(),
                           new_pass:true }
                         },
                        {upsert:true});
                 promise.on('success',function(doc){
                    updateMongoId(tblName,userData,callback);
                  });
                  promise.on('error',function(err){
                    console.log(err);
                    callback(6,null);//system error
                  });
            }
         if(results.customer.isCustomer == true && results.customer.passWord   != null){
            var email_text = "Dear Customer,<br/><br/> Thank you for subscription";
                email_text += "<br/>please find below details for login to your account.<br/>";
                email_text += "<br/>Organization ID: " + orgId;
                email_text += "<br/>Password: 'use same as your customer app'";
                email_text += "<br/>Thanks";
         
            var modelpath = app.get('model');
            var model = require(modelpath + 'email');
            model.sendmail(userData.users.userEmail, 'Account Activation mail', email_text);
   
              var userid_str = userData.userMongo + ''; //to convert objectid to string
          
              var promise = db.get('user_detail').update(
                        {_id: userData.userMongo}, 
                        {$set:
                           {_id: userData.userMongo,
                           user_id: userid_str,
                           user_name: userData.users.userName,
                           bus_email: userData.users.userEmail,
                           bus_mobile: userData.users.userPhone,
                           bus_type: userData.businessType,
                           regist_date: new Date(),
                           org_id: orgId,
                           pass_word: results.customer.passWord,
                           role_id: userData.users.userRole.roleId,
                           activ_date: new Date(),
                           new_pass:false }
                         },
                        {upsert:true});
                 promise.on('success',function(doc){
                    updateMongoId(tblName,userData,callback);
                  });
                  promise.on('error',function(err){
                    console.log(err);
                    callback(6,null);//system error
                  });           
         }
      }
  });
}

function updateMongoId(tblName,userData,callback){

var data = {};
debuglog("We are updating the entry with MongoId of user");
debuglog("This will be useful in uninviting because user could change email in edit");
debuglog("and submit again for authorised status");  
var promise = db.get(tblName).update(
  {userId:userData.users.userCode},
  {$set: {mongoId: userData.userMongo,
          update_time: new Date}},
        {upsert:false}
        );  
  promise.on('success',function(err,doc){
        data.errorCode = 0;
        callback(null,data);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getUserDetailUnInvite(tblName,userData,callback){

var datautils = require('./dataUtils');
var data = {};

   datautils.UserData(tblName,userData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
                data.errorCode = 6;
                callback(null,data)
              }
         else {
             if(result.errorCode != 0 ) {
              data.errorCode = 6;
              callback(null,data)
              }
            else {
                userData.users.userCode   = result.userCode;
                userData.users.userName = result.userName;
                userData.users.userType = result.userType;
                userData.users.userEmail   = result.userEmail;
                userData.users.userPhone   =  result.userPhone;
                userData.users.userRole    =  result.userRole;
                userData.users.userMongo   = result.userMongo;
                deleteUserMasterEntry(tblName,userData,callback);
                debuglog("No error case");
                debuglog(userData);
            }
          }
        });
}

function deleteUserMasterEntry(tblName,userData,callback){

debuglog("Will delete based on MongoId and not email since it may be");
debuglog("Possible email was changed in edit")
debuglog(userData);

 var promise = db.get('user_master').remove({_id:userData.users.userMongo}); 
   promise.on('success',function(doc){
      deleteUserDetailEntry(tblName,userData,callback);
    });
    promise.on('error',function(err){
      console.log(err);
      callback(6,null);//system error
    });
}

function deleteUserDetailEntry(tblName,userData,callback){
 var data = {};
debuglog("_id of user_master and user_detail are same always")
 var promise = db.get('user_detail').remove({_id:userData.users.userMongo}); 
   promise.on('success',function(doc){
        data.errorCode = 0;
        callback(null,data);
    });
    promise.on('error',function(err){
      console.log(err);
      callback(6,null);//system error
    });
}


function getUserDetailDeActivate(tblName,userData,callback){

var datautils = require('./dataUtils');
var data = {};

   datautils.UserData(tblName,userData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
                data.errorCode = 6;
                callback(null,data)
              }
         else {
             if(result.errorCode != 0 ) {
              data.errorCode = 6;
              callback(null,data)
              }
            else {
                userData.users.userCode   = result.id;
                userData.users.userName = result.userName;
                userData.users.userType = result.userType;
                userData.users.userEmail   = result.userEmail;
                userData.users.userPhone   =  result.userPhone;
                userData.users.userRole    =  result.userRole;
                userData.users.userMongo   = result.userMongo;
                deactivateUserMasterEntry(tblName,userData,callback);
                debuglog("No error case");
                debuglog(userData);
            }
          }
        });
}

function deactivateUserMasterEntry(tblName,userData,callback){

debuglog("Will deactivate based on MongoId and not email since it may be");
debuglog("Possible email was changed in edit")
debuglog(userData);
var data = {};

 var promise = db.get('user_master').update(
     {_id:userData.users.userMongo},
     {$set:{user_status:8}}); 
   promise.on('success',function(doc){
       data.errorCode = 0;
        callback(null,data);
    });
    promise.on('error',function(err){
      console.log(err);
      callback(6,null);//system error
    });
}

exports.sendInvitation = getUserDetail;
exports.unsendInvitation = getUserDetailUnInvite;
exports.deactivateUser = getUserDetailDeActivate;
