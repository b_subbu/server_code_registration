function processRequest(req,res,data){
getUserId(req,res,9,data);
}


function getUserId(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
   var extend = require('util')._extend;
   var UserData = extend({},data);
       UserData.users = req.body.userDetails;
       UserData.status_id = 80;
      
     var tblName = data.orgId+'_users'; //create will always be in main table
      datautils.generateUserId(UserData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 debuglog("Generated new User");
                 debuglog(result);
                 UserData.users.userCode = result;
                 checkUserEmail(req,res,9,data,UserData,tblName);
             }
        });
} 


function checkUserEmail(req,res,stat,data,UserData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     debuglog("Check if the  Email already exists or not");
     debuglog("in this Org or any Org across the system");
     debuglog("in any status");
     debuglog(UserData);
     datautils.checkEmailExist(UserData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 if(result.errorCode == 1)
                   processStatus(req,res,5,data);
                 else
                   saveUserData(req,res,9,data,UserData,tblName);
             }
        });
} 


function saveUserData(req,res,stat,data,UserData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   
     UserData.users.userType = 2;
     
     debuglog("This is after getting data from request");
     debuglog("All Users created by user will be Business type only");
     debuglog(UserData);
     debuglog(data);
     
    var submitop = req.body.submitOperation;
  
   
    if(   UserData.users.userName == null
       || UserData.users.userName == 'undefined'
       || UserData.users.userEmail == null
       || UserData.users.userEmail == 'undefined'
       || UserData.users.userRole  == 'null'
       || UserData.users.userRole  == 'undefined'
      )
        processStatus(req,res,4,data);
    else{
    
     datautils.UserUpdate(tblName,UserData.users.userCode,UserData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(UserData);
             if(submitop == 1){
             var userCodes = [];
             userCodes.push(UserData.users.userCode);
             data.userCodes = userCodes;
               var submitUser = require('./submitUsers');
               submitUser.moveToRevision(req,res,9,data);
             }
             else{
                changeUserStatus(req,res,9,data,UserData,tblName);
                }
            }
         });
        }
} 


function changeUserStatus(req,res,stat,data,UserData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeUserStatus(tblName,data.orgId,data.businessType,data.userId,UserData.users.userCode,80,'New User Created',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getUserData(req,res,9,data,UserData,tblName);
               }
        });
} 

function getUserData(req,res,stat,data,UserData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   
    debuglog("Start getting data");
    debuglog(data);
    
    var userArr = [];
    userArr.push(UserData.users.userCode) 
    datautils.UsersData(tblName,userArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getUserStatusName(req,res,9,data,result); 
         }
     });
    
}

function getUserStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('user_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.userData = pgs;
            processStatus(req,res,9,data);
           }
      });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;













