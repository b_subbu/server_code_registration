function processRequest(req,res,data){
   if( typeof req.body.userCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkUserStatus(req,res,9,data);
}

function checkUserStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var userArr = req.body.userCodes;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.userCodes = userArr;
    
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_users'; //TODO: move to a common definition file

    async.forEachOf(userArr,function(key,value,callback1){
        var UserData= {};
            UserData.users = {};
            UserData.users.userCode = key;
        datautils.checkUserCode(tabl_name,UserData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });       }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             moveToRevision(req,res,9,data);
          }
      });
}

function moveToRevision(req,res,stat,data){
    
    var UserArr = data.userCodes;
    
    debuglog("Now start moving the entries to Revision Table");
    debuglog(UserArr);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_users'; //TODO: move to a common definition file
    var mainTable    = orgId+'_users';
   
    async.forEachOf(UserArr,function(key,value,callback1){
      userId = key;
     async.parallel({
          relStat: function (callback){datautils.moveUserHeader(mainTable,revisionTable,userId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
           
         });
         }, 
    function(err){
         if(err || isError == 1) {
             if(errorCode != 0) 
               processStatus(req,res,errorCode,data);
             else
               processStatus(req,res,6,data);
            }
      else
          {
           changeUserStatus(req,res,9,data,UserArr,mainTable,revisionTable); 
         }
     });
}

function copyToRevision(req,res,stat,data,UserData){
    
    
    debuglog("Even though this called Copy we are actually creating a new entry");
    debuglog(UserData);
    debuglog(data);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
     
    var orgId = data.orgId;
    
    var revisionTable = orgId+'_Revision_users'; //We will create the new entry in Revision Table
   
      if(   UserData.users.userName == null
       || UserData.users.userName == 'undefined'
       || UserData.users.userEmail == null
       || UserData.users.userEmail == 'undefined'
       || UserData.users.userRole  == 'null'
       || UserData.users.userRole  == 'undefined'
      )
        processStatus(req,res,4,data);
    else{
         datautils.UserUpdate(revisionTable,UserData.users.userCode,UserData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(UserData);
                var UserArr = [];
                UserArr.push(UserData.users.userCode);
                changeUserStatus(req,res,9,data,UserArr,'',revisionTable);
                }
        });
      }
}


function changeUserStatus(req,res,stat,data,UserArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(UserArr,function(key,value,callback1){
    userId = key;
    var reason = '';
       statutils.changeUserStatus(revTable,data.orgId,data.businessType,data.userId,userId,81,'User Submitted',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.userCodes = UserArr;
                 getUserDetails(req,res,9,data,revTable);
             }
          });
     
}


function getUserDetails(req,res,stat,data,revTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.UsersData(revTable,data.userCodes,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getUserStatusName(req,res,9,data,result); 
         }
     });
    
}

function getUserStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('user_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.userData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}

   
exports.getData = processRequest;
exports.moveToRevision=moveToRevision; 
exports.copyToRevision=copyToRevision;





