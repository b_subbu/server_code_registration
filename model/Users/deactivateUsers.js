function processRequest(req,res,data){
    if( typeof req.body.userCodes === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkUserStatus(req,res,9,data);
}


function checkUserStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var UserArr = req.body.userCodes;
    
    debuglog("This is the input array");
    debuglog(UserArr);
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.userCodes = UserArr;
    
    var orgId = data.orgId;
    
    var tblName = orgId+'_users'; //TODO: move to a common definition file

    async.forEachOf(UserArr,function(key,value,callback1){
      var UserData = extend({},data);
          UserData.users = {};
          UserData.users.userCode = key;
     debuglog(UserData)    
     datautils.checkUserCode(tblName,UserData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             deactivateUsers(req,res,9,data);
           }
      });
}


function deactivateUsers(req,res,stat,data){
    
    var uid = req.body.userId;
    var UserArr = req.body.userCodes;
    
    var async = require('async');
    var inviteutils = require('./invitationUtils');
    var extend = require('util')._extend;
 
    var isError = 0;
    var errorCode = 0;
    data.userCode = UserArr;
    
    var orgId = data.orgId;
    
    var tblName = orgId+'_users'; //TODO: move to a common definition file

    async.forEachOf(UserArr,function(key,value,callback1){
      var UserData = extend({},data);
          UserData.users = {};
          UserData.users.userCode = key;
      debuglog(UserData);
      debuglog("Now start deactivating the user row in master and main tables");    
     inviteutils.deactivateUser(tblName,UserData,function(err,result) {
          if(err) {
             console.log(err);
             isError = 6;
             callback1();
            }
            else{  
             if(result.errorCode != 0)
               isError = 6;
               
             callback1();
             }
           });
         }, 
    function(err){
         if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else
             changeUserStatus(req,res,9,data,UserArr,tblName,'');
           }
      });
}




function changeUserStatus(req,res,stat,data,UserArr,mainTable,revTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(UserArr,function(key,value,callback1){
    userId = key;
    var reason = '';
       statutils.changeUserStatus(mainTable,data.orgId,data.businessType,data.userId,userId,85,'User Deactivated',function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 data.userCodes = UserArr;
                 getUserDetails(req,res,9,data,mainTable);
             }
          });
     
}


function getUserDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.UsersData(mainTable,data.userCodes,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getUserStatusName(req,res,9,data,result); 
         }
     });
    
}

function getUserStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('user_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.userData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;










