debuglog("This is similar to getUserModules");
debuglog("But without the permission roles part");
debuglog("This is only used for generating new ProductData array whenever")
debuglog("A new module is added or removed");
debuglog("The result of this can be used to update product_master");
debuglog("This will not probably generate work queues add it manually")

function getModules(orgId,callback){
    var utils = require('./dataUtils');
    var tblName = 'module_submodule_link';
    var totalRows =0;
    var modulesArray = [];
    var data = {};
    data.orgId = orgId;

    debuglog("All modules less than 100 are considered as Main Modules");
    debuglog("So this will get from module_submodule_link all modules less than 100")
  
   var promise = db.get('module_submodule_link').find({module_id: {$lte:100}},{sort: {sub_module_seq:1}});
   promise.each(function(docuser){
        totalRows++;
        var tmpArray = {id:docuser.module_id,sequence:docuser.sub_module_seq};
        modulesArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         data.errorCode = 0; //no error possible here since modules are system populated
         data.modules = modulesArray;
         getModuleDetails(data,callback);
     });
   promise.on('error',function(err){
       debuglog(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });}


function getModuleDetails(data,callback) {
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
   
    var moduleArray = data.modules;
    
    debuglog("The Module array is");
    debuglog(moduleArray);
   
    async.forEachOf(moduleArray,function(value,key,callback1){
       var moduleid = moduleArray[key].id;
       
       roleutils.moduleName(moduleid,function(err,result) {
         if(err) return callback(err);
         else {
            if(result)
                 data.modules[key].name = result; //module name null is not an error
                 callback1(); //next item in foreachof
           }
         });
        },
      function(err){
      if(err)
           callabck(6,data);
      else
          getSubModule(data,0,0,moduleArray,callback);
      });  
}



//Get list of all submodules of the first module
//writing recruisve function is more difficult in node due to async
//so it is implemented using more functions rather
function getSubModule(data,level,moduleEntry,submodules,callback) {
    var async = require('async');
    var utils = require('../Roles//roleutils');
    var isError = 0;
    var type = require('type-detect');
    var subModulesArray = [];
   
    if(level == 0 ) {
         var submoduleArray = data.modules;
          async.forEachOf(submoduleArray,function(value,key,callback1) {
                var submoduleid = submoduleArray[key].id;
              
                utils.subModules(submoduleid,function(err,results) {
                 if(err){ callback1(6,data);    }
                  else {
                         if(type(results.subModules) === 'array') {
                            data.modules[key].subModules = results.subModules;
                            callback1();
                        }
              else{
                     callback1(); 
                  }
                }
               });
              },
              function(err){
                  if(err)
                    callback(6,data);
                   else
                    { level++;
                      getSubModulesNew(data,level,moduleEntry,submodules,callback);
                    }
              });  
            }
         
    if(level == 1) {
        var submoduleArray = data.modules;
         debuglog("The submoduleArray at level 1 is:");
         debuglog(submoduleArray);
        
          async.forEachOf(submoduleArray,function(value,key,callback1) {
               var subsubmoduleArray = submoduleArray[key];
                 async.forEachOf(subsubmoduleArray,function(value,key,callback2) {
                    var submoduleid = subsubmoduleArray[key].id;
             
                utils.subModules(submoduleid,function(err,results) {
                 if(err){ callback2(6,data);    }
                  else {
                         if(type(results.subModules) === 'array') {
                              data.modules[key].subModules[key] = results.subModules;
                        }
                      else{
                       callback2(); 
                      }
                    }
               });
              },
              function(err){
                  if(err)
                    callback1(6,data);
                   else
                    callback1();
                  });  
            },
          function(err){
                  if(err)
                    callback(6,data);
                   else
                    {
                     level++;
                     getSubModulesNew(data,level,moduleEntry,submodules,callback);
                    }
                 });
        }
  if(level == 2) {
        var submoduleArray = data.modules;
         debuglog("The submoduleArray at level 2 is:");
         debuglog(submoduleArray);
            async.forEachOf(submoduleArray,function(value,key1,callback1) {
                var subsubmoduleArray = submoduleArray[key1].subModules;
                 async.forEachOf(subsubmoduleArray,function(value,key3,callback2) {
                     var submoduleid = subsubmoduleArray[key3].id;
                     utils.subModules(submoduleid,function(err,results) {
                      if(err){ callback2(6,data);    }
                       else {
                             if(type(results.subModules) === 'array') {
                                data.modules[key1].subModules[key3].subModules = results.subModules;
                                callback2();
                            }
                        else{
                             callback2(); 
                            }
                        }
                      });
                     },
                function(err){
                      if(err)
                       callback1(6,data);
                      else
                        {
                         callback1();
                        }
                     });
               },
              function(err){
                  if(err)
                    callback(6,data);
                   else{
                     level++;
                     getSubModulesNew(data,level,moduleEntry,submodules,callback);
                    }
        });  
        }
        if(level == 3){
            getWorkQueues(data,callback);
        }
}

//This function will return the array of submodules
//for the passed submodule along with their name and permission 

function getSubModulesNew(data,level,moduleEntry,submodules,callback) {
   var async = require('async');
    var utils = require('../Roles/roleutils');
    var isError = 0;
    var type = require('type-detect');

  if(level == 2) {
        var submoduleArray = data.modules;
         debuglog("The new submoduleArray at level 2 is:");
         debuglog(submoduleArray);
        
          async.forEachOf(submoduleArray,function(value,key1,callback1) {
               var subsubmoduleArray = submoduleArray[key1].subModules;
                 async.forEachOf(subsubmoduleArray,function(value,key2,callback2) {
                    var submoduleid = subsubmoduleArray[key2].id;
               
                utils.moduleName(submoduleid,function(err,results) {
                 if(err){ callback2(6,data);    }
                  else {
                        if(results){  
                             data.modules[key1].subModules[key2].name = results;
                             callback2();
                              }
                        else
                           callback2();
                        }
                });
              },
          function(err){
                  if(err)
                    callback1(6,data);
                   else
                    {
                    callback1();
              }
                 });
        },
       function(err){
                  if(err)
                   callback(6,data);
                   else
                    {
                    getSubModule(data,level,moduleEntry,submodules,callback);
              }
        
        });
        }
      else if(level == 3){
          var moduleArray = data.modules;
           debuglog("The submoduleArray at level 3 is:");
           debuglog(submoduleArray);
        
          async.forEachOf(moduleArray,function(value,key1,callback1) {
               var submoduleArray = moduleArray[key1].subModules;
                 async.forEachOf(submoduleArray,function(value,key2,callback2) {
                    var subsubmoduleArray = submoduleArray[key2].subModules;
                 async.forEachOf(subsubmoduleArray,function(value,key3,callback3) {
                    var submoduleid = subsubmoduleArray[key3].id;
                    
                  
                utils.moduleName(submoduleid,function(err,results) {
                 if(err){ callback3(6,data);    }
                  else {
                          if(results){  
                              data.modules[key1].subModules[key2].subModules[key3].name = results;
                             callback3();
                              }
                            else
                             callback3();
                         }
                  });
              },
          function(err){
                  if(err)
                    callback2(6,data);
                   else
                    {
                    callback2();
                    }
            });
        },
        function(err){
                  if(err)
                    callback1(6,data);
                   else
                    {
                    callback1();
              }
                 });
        },
       function(err){
                  if(err)
                    callback(6,data);
                   else
                    {
                    getSubModule(data,level,moduleEntry,submodules,callback);
                    }
          });
       }
       else
        getSubModule(data,level,moduleEntry,submodules,callback);
}

function getWorkQueues(data,callback){
debuglog("This is a temporary work-around to get work queues");
debuglog("And populate them under Work Queues tab, id:104");
debuglog("TODO after MVP make this more generic with permissions, etc.,");

var datautils = require('../Roles/datautils');
var tblName   = data.orgId+'_work_queue';
var async     = require('async');

  datautils.workQueues(tblName,function(err,results) {
                 if(err){ processStatus(req,res,6,data);    }
                  else {
                      debuglog("Work queues obtained for this business is");
                      debuglog(results);
                      debuglog(data.modules[1].subModules) 
                      async.forEachOf(data.modules[1].subModules,function(key,value,callback1){
                       debuglog("Start processing data");
                       debuglog("Only for product 104 i.e., WorkQueues we need to set");
                       debuglog(key);
                       if(key.id == 104){
                          key.subModules = results;
                          callback1();
                        }
                       else
                        callback1();
                  },
                  function(err){
                  if(err)
                   callback(6,data);
                   else
                    callback(null,data);
            });
      }
  });
}

exports.getproducts = getModules;

