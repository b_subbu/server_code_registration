function processRequest(req,res,data){
        checkUserId(req,res,9,data);
}


function checkUserId(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
  
      
             
   var extend = require('util')._extend;
   var UserData = extend({},data);
       UserData.users = req.body.userDetails;
       UserData.status_id = 80;
    
      UserData.users.userType = 2;
           
      
     var tblName = data.orgId+'_users'; 
     debuglog("Check if the  Id exists or not");
     debuglog(UserData);
     datautils.checkUserCode(tblName,UserData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
               if(result.errorCode == 0) 
                 processStatus(req,res,3,data);
               else{
                debuglog("If no row exists it is an error for both entered and authorised");
                debuglog("If a row exists and status is Authorised then only submit is allowed");
                debuglog("But if a row exists and status is Entered both save and submit are allowed");
                if(result.status == 82 || result.status == 85){
                  var submitop = req.body.submitOperation;
                  if(submitop != 1){
                  debuglog("This is a SAVE for Authorised or Deactivate row so throw error");
                  debuglog(result);
                  debuglog(submitop);
                  processStatus(req,res,5,data);
                  }
                  else{
               UserData.mongoId = result.mongoId;
               var submitUsers = require('./submitUsers');
               submitUsers.copyToRevision(req,res,9,data,UserData);
               
               debuglog("This is different operation of copy because");
               debuglog("Here we are not actually moving the data but creating a ");
               debuglog("new entry in Pending Tab without touching Main tab");
              }
            }
              else
                 checkUserEmailEdit(req,res,9,data,UserData,tblName);
            }
          }
        });
} 

function checkUserEmailEdit(req,res,stat,data,UserData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
     debuglog("Check if the  Email already exists or not");
     debuglog("same as checkUserEmail but exclude the given id");
     debuglog(UserData);
     datautils.checkEmailExistEdit(UserData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
             }
            else {
                 if(result.errorCode == 1)
                   processStatus(req,res,2,data);
                 else
                   saveUserData(req,res,9,data,UserData,tblName);
             }
        });
} 

function saveUserData(req,res,stat,data,UserData,tblName) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
    
     debuglog("This is after getting data from request");
     debuglog(UserData);
     debuglog(data);
     
     
    var submitop = req.body.submitOperation;
  
   
      if(   UserData.users.userName == null
       || UserData.users.userName == 'undefined'
       || UserData.users.userEmail == null
       || UserData.users.userEmail == 'undefined'
       || UserData.users.userRole  == 'null'
       || UserData.users.userRole  == 'undefined'
      )
       processStatus(req,res,4,data);
    else{
    
     datautils.UserUpdate(tblName,UserData.users.userCode,UserData,function(err,result) {
         if(err) {
               processStatus(req,res,6,data);
              }
         else {
                debuglog("saved data");
                debuglog(UserData);
             if(submitop == 1){
             var userCodes = [];
             userCodes.push(UserData.users.userCode);
             data.userCodes = userCodes;
               var submitUsers = require('./submitUsers');
               submitUsers.moveToRevision(req,res,9,data);
             }
             else{
                changeUserStatus(req,res,9,data,UserData,tblName);
                }
            }
          });
        }
} 


function changeUserStatus(req,res,stat,data,UserData,tblName) {
   var async = require('async');
   var statutils = require('./statUtils');
  
    statutils.changeUserStatus(tblName,data.orgId,data.businessType,data.userId,UserData.users.userCode,80,'User Edited',function(err,result) {
         if(err) {
                  processStatus(req,res,6,data);
                 }
          else {
                 getUserData(req,res,9,data,UserData,tblName);
               }
        });
} 

function getUserData(req,res,stat,data,UserData,tblName) {
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   
    debuglog("Start getting data");
    debuglog(data);
    
    var userArr = [];
    userArr.push(UserData.users.userCode); 
    datautils.UsersData(tblName,userArr,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getUserStatusName(req,res,9,data,result); 
         }
     });
    
}

function getUserStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('user_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.userData = pgs;
            processStatus(req,res,9,data);
           }
      });
} 



function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;









