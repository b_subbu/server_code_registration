function processRequest(req,res,data){
  if( typeof req.body.userCode === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getUserData(req,res,9,data);
}


function getUserData(req,res,stat,data) {
   var datautils = require('./dataUtils');
   var isError = 0;
   
   data.userCode = req.body.userCode;
   var tid = req.body.tabId;
    
    if(tid == 502)
     var tabl_name = data.orgId+'_Revision_users';
    else
     var tabl_name = data.orgId+'_users';
   
     var extend = require('util')._extend;
     var UserData = extend({},data);
         UserData.users = {};
         UserData.users.userCode = data.userCode;    
     
  
      datautils.UserData(tabl_name,UserData,function(err,result) {
           debuglog(result);
           debuglog("after getting result");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
               processStatus(req,res,3,data);
               debuglog("result errorCode is 1");
              }
            else {
                var userDetails = {};
                userDetails.userCode   = result.userCode;
                userDetails.userName = result.userName;
                userDetails.userType = result.userType;
                userDetails.userEmail   = result.userEmail;
                userDetails.userPhone   =  result.userPhone;
                userDetails.userRole    =  result.userRole;
                userDetails.status     = result.status;
                userDetails.statusId   = result.statusId;
                data.userDetails = userDetails;   
                getRoles(req,res,9,data);
                debuglog("No error case");
                debuglog(userDetails);
            }
          }
        });
} 

function getRoles(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');
 

 var tblName = data.orgId+'_roles';
 
  datautils.UserRoles(tblName,function(err,result) {
           debuglog(result);
           debuglog("after getting result for roles");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.roles = result;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;












