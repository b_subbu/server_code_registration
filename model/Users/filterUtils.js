function getFilters(key_name,statList,pid,tid,oid,callback) {
  
  switch(key_name){
     case 'status':
     var tblName = 'role_status_master';
       getStatusFilter(statList,tblName,callback);  
        break;
     case 'roleType':
       getRoleTypeFilter(callback);
       break;
     default:
       callback(null,null);
       break;
  }
  
}

//for status no need to again query db since statList is already passed
//and it varies from tab to tab for other filters we can just query master
//and return

function getStatusFilter(mastData,tblName,callback) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var data = {};
     
      async.forEachOf(mastData,function(key1,value1,callback1){
        var statID=key1;
        getItemStatName(statID,tblName,function(err,result) {
           if(err){ isError =1; callback1();}
         else {
                 var tmpArray = {id:statID, name:result};
                 itemMasters.push(tmpArray);
                  callback1(); 
              }
           }); 
          },
         function(err){
       if(err){
            callback(6,null);      }
      else {
            callback(null,itemMasters);
	         }
      });
 

}

function getItemStatName(statID,tblName,callback){
   var totalRows =0;
   var statName = '';
    var mastutils = require('./../Registration/masterUtils');

  
      mastutils.masterStatusNames(tblName,statID,function(err,result) {
       
         if(err) callback(6,null);
         else {
                statName = result.statusName;
                callback(null,statName); 
                debuglog("after getting status");
                debuglog(result);
              }
          });
       
 } 

 function getRoleTypeFilter(callback){
    
    var enabledList = [{id:1,name:"System"},{id:2,name:"Business"}]; 
    debuglog("this is a static list with types system and business");
    callback(null,enabledList);
    
}

function getUserFilters(key_name,statList,pid,tid,oid,callback) {
  
  switch(key_name){
     case 'status':
     var tblName = 'user_status_master';
       getUserStatusFilter(statList,tblName,callback);  
        break;
     case 'userRole':
       var tblName = oid+"_roles";
       getUserRoleFilter(tblName,callback);
       break;
     default:
       callback(null,null);
       break;
  }
  
}

//for status no need to again query db since statList is already passed
//and it varies from tab to tab for other filters we can just query master
//and return

function getUserStatusFilter(mastData,tblName,callback) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var data = {};
     
      async.forEachOf(mastData,function(key1,value1,callback1){
        var statID=key1;
        getItemStatName(statID,tblName,function(err,result) {
           if(err){ isError =1; callback1();}
         else {
                 var tmpArray = {id:statID, name:result};
                 itemMasters.push(tmpArray);
                  callback1(); 
              }
           }); 
          },
         function(err){
       if(err){
            callback(6,null);      }
      else {
            callback(null,itemMasters);
	         }
      });
 

}


function getUserRoleFilter(tblName,callback){
  getUserFilterRoles(tblName,function(err,result) {
           debuglog(result);
           debuglog("after getting result for roles");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                callback(null,result);
              }
        });   
}


function getUserFilterRoles(tblName,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   debuglog("Same as get roles but only id+name for using in filter");
   
   var promise = db.get(tblName).find({status_id:82},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {id: doc.roleId,name:doc.roleName};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       debuglog(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}     

exports.filterOptions = getFilters;
exports.roleTypes = getRoleTypeFilter;
exports.userfilterOptions = getUserFilters;

 



