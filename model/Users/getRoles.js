function getRolesData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('../Orders/statusUtils');
  
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
    var searchArr = [];
      
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
  
    if(tid == 501)
     var tblName = oid+'_roles';
    else
     var tblName = oid+'_Revision_roles';
  
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'role_status_master',callback); }
         },
         function(err,results) {
           if(err) {
              debuglog(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList,tblName);
                  }
                  else {
                      var searchArr = [];
                      var tmpArray = {status_id: {"$in":statList}};
                      searchArr.push(tmpArray);
                      debuglog(searchArr);
                      getRoleCount(req,res,stat,data,searchArr,tblName);
                    }
                 }
          }); 
} 

//TODO combine all getFilterOptions in productData function if possible

function getFilterOptions(req,res,stat,data,statList,tblName) {
   var async = require('async');
   var filterutils = require('./filterUtils');
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
      debuglog("Starting processing of header fields");
     debuglog("Note we are calling filter utils because here there are 2 filters");
     debuglog("TODO make Roles filter utils as generic and call it from everywhere");
     debuglog(key);
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,oid,function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
               filterOptions[key_name] = results;
               callback();
            }
          }); 
        }
     else
        callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
         data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call count fn without search array");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog(statList);
           var searchArr = [];
           var tmpArray = {status_id: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
            getRoleCount(req,res,stat,data,searchArr,tblName);
           }
         else 
         getRoleSearch(req,res,stat,data,statList,tblName);
         }
      });
} 


function getRoleSearch(req,res,stat,data,statList,tblName) {

 var searchInp = req.body.searchParams;

    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
 
 var searchArr = [];
    var async = require('async');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {status_id: key};
         searchArr.push(tmpArray);
         break;
      case 'roleId':
         var tmpArray = {roleId: key};
         searchArr.push(tmpArray);
         break;
       case 'roleName':
        var tmpArray =  {roleName: key}; 
        searchArr.push(tmpArray);
        break; 
       case 'roleType':
        var tmpArray = {roleType: key};
        searchArr.push(tmpArray);
        break; 
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           
            getRoleCount(req,res,9,data,searchArr,tblName);
            debuglog("We will start getting data with search Params");
            debuglog(searchArr);
          }
     });
}

function getRoleCount(req,res,stat,data,statList,tblName){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
       
    //have only tabl_name no need for orgId or bizType
   //since table name itself will have them      
   utils.RoleCount(tblName,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
           debuglog("After getting results");
           debuglog(results);
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getProduct(req,res,9,data);
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getRoleData(req,res,9,data,statList,tblName); 
             }
            }
         });
}


function getRoleData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
      debuglog("Now start processing getproductData");
      debuglog(headerTable);
      datautils.RoleHeader(headerTable,pageId,size,statList,function(err,result) {
         debuglog("AFter result");
         debuglog(result);
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              getProduct(req,res,9,data); 
             //will be handled at count itself, just adding here
            }
            else {
            roles = result.roles;
            debuglog(roles); 
            getRoleStatusName(req,res,9,data,roles);
            }
          }
        });
} 


function getRoleStatusName(req,res,stat,data,roles) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(roles,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(statID);
           mastutils.masterStatusNames('role_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
                debuglog("after getting status");
                debuglog(key1);
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.productData = roles;
            getProduct(req,res,9,data);
            }
      });
 

} 


function getProduct(req,res,stat,data) {
 var isError = 0;
 var datautils = require('./dataUtils');
 

 var tblName = 'product_master';
 
  datautils.products(tblName,data.businessType,function(err,result) {
           debuglog(result[0].products[2]);
           debuglog("after getting result for product");
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
                data.products = result[0].products;
                processStatus(req,res,stat,data);
                debuglog("No error case");
                debuglog(data);
              }
        });
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getProductData = getRolesData;










