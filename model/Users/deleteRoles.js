function processRequest(req,res,data){
   if(typeof req.body.roleIds === 'undefined')
      processStatus(req,res,6,data); //system error
   else 
      checkRoleStatus(req,res,9,data);
}


function checkRoleStatus(req,res,stat,data){
    
    var uid = req.body.userId;
    var RoleArr = req.body.roleIds;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    data.roleIds = RoleArr;
  
    var enteredItems = [];
    var authorisedItems = [];
  
    var orgId = data.orgId;
    
    var tabl_name = orgId+'_roles'; //TODO: move to a common definition file

    async.forEachOf(RoleArr,function(key,value,callback1){
        var RoleData= {};
            RoleData.roles = {};
            RoleData.roles.roleId = key;
        datautils.checkRoleCode(tabl_name,RoleData,function(err,result){
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
            else{  
             if(result.errorCode != 5)
               isError = 3;
               else{
                  if (result.status == 80){
                   debuglog("This is entered status item");
                   debuglog("So can be deleted directly");
                   enteredItems.push(key);
                   debuglog(enteredItems);
                  }
                  if(result.status == 82){
                   debuglog("This is authorised status item");
                   debuglog("So has to go through authoristation to delete");
                   authorisedItems.push(key);
                   debuglog(authorisedItems);
                  }
                }
             debuglog("Since Delete is defined only for above 2 statuses");
             debuglog("No need to check for other statuses here");  
             callback1();          
            }
           });       
          }, 
     function(err){
        if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
          else{
              debuglog("delete of authorised items do separate process");
              debuglog("But keep in same file so has to handle responses correctly");
              debuglog("Donot separate out in different files");
              deleteRoles(req,res,9,data,enteredItems,authorisedItems);
              debuglog("The request may contain either Entered or Authorised or Both");
              debuglog("So handle accordingly")
              }
          }
      });
}

function deleteRoles(req,res,stat,data,enteredItems,authorisedItems){
    var uid = req.body.userId;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_roles'; //delete is possible only in Main Table currently
   
    async.forEachOf(enteredItems,function(key,value,callback1){
      roleId = key;
     async.parallel({
          relStat: function (callback){datautils.deleteRoleCode(mainTable,roleId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
         else{
          if(authorisedItems.length > 0){
          debuglog("This request has some authorised items too");
          debuglog(authorisedItems);
          deleteAuthoriseRoles(req,res,9,data,authorisedItems);
          }
          else{
          debuglog("This request has no authorised items");
          debuglog("so call return");
          processStatus(req,res,9,data); 
        }
		   }      
      });
}

function deleteAuthoriseRoles(req,res,stat,data,authorisedItems){
    var uid = req.body.userId;
    
    var async = require('async');
    var datautils = require('./dataUtils');
    var isError = 0;
    var errorCode = 0;
    
    
    var mainTable    = data.orgId+'_roles';
    var revisionTable = data.orgId+'_Revision_roles';
   
    async.forEachOf(authorisedItems,function(key,value,callback1){
      roleId = key;
     async.parallel({
          relStat: function (callback){datautils.copyRoleHeader(mainTable,revisionTable,roleId,callback);  }
         },
         function(err,results) {
           if(err) {
             console.log(err);
             isError = 1;
             callback1();
            }
          else  
             callback1();
          });
         }, 
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
           debuglog("Change status of items in revision table to ready2Delete");
           changeRoleStatus(req,res,9,data,authorisedItems,mainTable,revisionTable);
          }
      });
}

function changeRoleStatus(req,res,stat,data,authorisedItems,mainTable,revisionTable){
    var async = require('async');
 
    var statutils = require('./statUtils');
    var isError = 0;
    
    async.forEachOf(authorisedItems,function(key,value,callback1){
    roleId = key;
    var reason = 'Delete Role Submitted';
       statutils.changeRoleStatus(revisionTable,data.orgId,data.businessType,data.userId,roleId,89,reason,function(err,result) {
          if(err) {
                    isError = 1;
                    callback1();
                }
          else
             callback1();
        });
      },
    function(err){
         if(err || isError == 1)
           processStatus(req,res,6,data);
        else {
                 getRoleDetails(req,res,9,data,revisionTable);
             }
          });
     
}


function getRoleDetails(req,res,stat,data,mainTable){
    
   var resultArr = [];
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    debuglog("Start getting data");
    debuglog(data);
     
   datautils.RolesData(mainTable,data.roleIds,function(err,result) {
       if(err) {
             processStatus(req,res,6,data);
              }
      else  {
          getRoleStatusName(req,res,9,data,result); 
         }
     });
    
}

function getRoleStatusName(req,res,stat,data,pgs) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting status names");
    debuglog(pgs);  
    async.forEachOf(pgs,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('role_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            debuglog("After gettting status names");
            debuglog(pgs);
            data.roleData = pgs;
            processStatus(req,res,9,data);
           }
      });
 

} 


function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;





