function getRoleCount(tablName,sarr,callback){
   debuglog("TODO try to Combine all count fn in utils since they are same");
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getRoleHeader(tblName,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
   var page = parseInt(pgid);
   var skip = page > 0 ? ((page - 1) * size) : 0;
     
    debuglog("Role header data");
    debuglog("Note roleType is saved as id but got as name directly");
    debuglog("If multiple types then obviously change this");
    debuglog(page);
    debuglog(skip);

     var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
        promise.each(function(doc){
        totalRows++;
        var tmpArray = {roleId: doc.roleId,
                        roleName:doc.roleName,
                        roleType: (doc.roleType == 1 ? 'System' : 'Business'),
                        readonly: (doc.roleType == 1 ? true : false),
                        status: doc.status_id,
                        statusId: doc.status_id
                      };
       debuglog("Returned row");
       debuglog(tmpArray);                      
        resultArray.push(tmpArray);
        debuglog("final array");
        debuglog(resultArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.roles = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.roles = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function getRoleData(tblName,roleData,callback){
   var totalRows =0;
   var data = {};
   debuglog("start getting data");
   debuglog(roleData);
   
  var promise = db.get(tblName).find({roleId:roleData.roles.roleId});
   promise.each(function(doc){
        totalRows++;
        data.roleId = doc.roleId;
        data.roleName = doc.roleName;
        data.roleType = (doc.roleType == 1 ? 'System' : 'Business');
        data.productList = doc.productList;
        data.status = doc.status_id;
        data.statusId = doc.status_id;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}


function checkRoleIdMain(RoleData,callback){
 var tblName = RoleData.orgId+'_roles';
 var revisiontblName = RoleData.orgId+'_Revision_roles';
 var maxId = 0;
 
 var promise = db.get(tblName).find({},{sort:{roleId:-1},limit:1});
 promise.on('complete',function(err,doc){
       debuglog("Max is not available in monk");
       debuglog("If roleId is less than 100 then it is only system roles");
       debuglog("start business roles with 101");
       debuglog("else increment current max with +1");
       debuglog("Check Count in Revision also because it is possible");
       debuglog("user might have directly submitted in Create");
       debuglog(doc.length)
       if(doc.length >0){
       maxId = parseInt(doc[0].roleId);
       debuglog(doc);
       debuglog(maxId);
       debuglog(typeof maxId);
       debuglog(typeof doc);
       if(maxId < 101 || typeof maxId === 'undefined') maxId = 100;
       }
       else
        maxId = 100;
       checkRoleIdRevision(maxId,revisiontblName,callback);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkRoleIdRevision(maxMainId,tblName,callback){
 
 var maxId = 0;
 var maxRevId = 0;
 
 var promise = db.get(tblName).find({},{sort:{roleId:-1},limit:1});
 promise.on('complete',function(err,doc){
      debuglog("Max from revision table and active table are");
      if(doc.length >0){
       maxRevId = parseInt(doc[0].roleId);
       if(maxRevId < 101 || typeof maxRevId === 'undefined') maxRevId = 100;
      debuglog(maxRevId);
      debuglog(maxMainId);
      }
      else
       maxRevId = 100;
      if(maxRevId > maxMainId)
         maxId = maxRevId;
      else
         maxId = maxMainId;
         
         maxId++;
         
       debuglog("Maximum is incremented by 1 and returned");
       debuglog("So if both are empty Id will start from 101");
       debuglog("else maximum of main/revision +1");
       debuglog(maxId);
       callback(null,maxId);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

function checkRoleNameMain(RoleData,callback){
 var tblName = RoleData.orgId+'_roles';
 var revisiontblName = RoleData.orgId+'_Revision_roles';
 var data = {};
 
 var promise = db.get(tblName).count({roleName:RoleData.roles.roleName});
 promise.on('complete',function(err,doc){
      debuglog("If given name not found then check in Revision");
      debuglog("If found return error")
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else
        checkRoleNameRevision(RoleData,revisiontblName,callback);
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkRoleNameRevision(RoleData,tblName,callback){
 
 var data = {};
 var promise = db.get(tblName).count({roleName:RoleData.roles.roleName});
 promise.on('complete',function(err,doc){
      debuglog("If given name not found return no error");
      debuglog("Rreturn error")
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else{
        data.errorCode = 0;
        callback(null,data);
        }
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

//We are using update for both create and Edit 
function updateRoleData(tblName,roleId,RoleData,callback){
  
var promise = db.get(tblName).update(
  {roleId:roleId},
  {$set: {roleId: roleId,
          roleName: RoleData.roles.roleName,
          roleType: RoleData.roles.roleType,
          productList: RoleData.roles.productList,
          status_id: RoleData.status_id,
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getRolesData(tblName,roleIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var promise = db.get(tblName).find({roleId: {$in: roleIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var roleType = (doc.roleType == 1 ? 'System' : 'Business');
        var tmpArray = {roleId: doc.roleId,roleName:doc.roleName,roleType:roleType,status:doc.status_id,statusId:doc.status_id};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       debuglog(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}     

function getProducts(tblName,bizType,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
     
    debuglog("Products data");
    debuglog("Note if now row for biztype in product_master it is returned as error");
    debuglog("Because it should not happen");
    
     var promise = db.get(tblName).find({business_type:bizType});
         promise.each(function(doc){
        totalRows++;
        resultArray.push(doc);
        debuglog("final array");
        debuglog(resultArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       callback(6,null);
       }
      else {
         //callback(null,resultArray);
         createEnabledFlag(resultArray,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
} 

function createEnabledFlag(productData,callback){ 
 
 var traverse = require('traverse');
 debuglog("This function adds an enabled=false for every node of the array");
 debuglog("This is required by frontend developer to send back checkboxes with false");
 

 traverse(productData).forEach(function(x){

   debuglog(this.node);
   debuglog(x);
   debuglog("processing node");
    if(this.node.id){
        this.node.enabled = true;
        debuglog("added enabled true attribute");
        debuglog(this.node);
     }
  },[]);
  
    debuglog("just before return");
    debuglog(productData[0].products[2]);
    callback(null,productData); //just call back no checkings are possible it seems
    //if need of checkings or if above does not work then need to do
    //async instead of traverse
 }

function moveRoleHeader(srcTable,destTable,roleId,callback){

var promise1 = db.get(destTable).remove({roleId:roleId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({roleId:roleId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({roleId:roleId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function checkRoleCode(tblName,RoleData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("We are checking only in Main table");
debuglog("Any row directly submitted is not problem");
debuglog("since it will just overwrite main row when authorised");
debuglog(RoleData);

var promise = db.get(tblName).find({roleId: RoleData.roles.roleId});
promise.each(function(doc){
   status = doc.status_id;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 5;
   data.status = status;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function checkRoleNameEditMain(RoleData,callback){
 var tblName = RoleData.orgId+'_roles';
 var revisiontblName = RoleData.orgId+'_Revision_roles';
 var data = {};
 
 var promise = db.get(tblName).count({$and:[{roleName:RoleData.roles.roleName},{roleId:{$ne:RoleData.roles.roleId}}]});
 promise.on('complete',function(err,doc){
      debuglog("If given name not found then check in Revision");
      debuglog("If found return error")
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else
        checkRoleNameEditRevision(RoleData,revisiontblName,callback);
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkRoleNameEditRevision(RoleData,tblName,callback){
 
 var data = {};
 var promise = db.get(tblName).count({$and:[{roleName:RoleData.roles.roleName},{roleId:{$ne:RoleData.roles.roleId}}]});
 promise.on('complete',function(err,doc){
      debuglog("If given name not found return no error");
      debuglog("Return error")
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else{
        data.errorCode = 0;
        callback(null,data);
        }
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

function deleteRoleRow(tblName,roleId,callback){
  
var promise = db.get(tblName).remove({roleId:roleId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

/*******
 *Users functions below
 */

function getUserCount(tablName,sarr,callback){
   debuglog("TODO try to Combine all count fn in utils since they are same");
  var promise = db.get(tablName).count({$and: sarr });
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}
  
function getUserHeader(tblName,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   
   var moment=require('moment');
  
   var page = parseInt(pgid);
   var skip = page > 0 ? ((page - 1) * size) : 0;
     
    debuglog("User header data");
    debuglog("Note userType is saved as id but got as name directly");
    debuglog("If multiple types then obviously change this");
    debuglog(page);
    debuglog(skip);

     var promise = db.get(tblName).find({$and: sarr},{sort: {update_time: -1}, limit: size, skip: skip});
        promise.each(function(doc){
        totalRows++;
        var tmpArray = {userCode: doc.userId,
                        userName:doc.userName,
                        userEmail:doc.userEmail,
                        userRole: doc.userRole.roleName,
                        readonly: (doc.userType == 1 ? true : false),
                        status: doc.status_id,
                        statusId: doc.status_id,
                        date: moment(doc.update_time).format('L')
                      };
       debuglog("Returned row");
       debuglog(tmpArray);                      
       resultArray.push(tmpArray);
       debuglog("final array");
       debuglog(resultArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.users = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.users = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function getUserRoles(tblName,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   debuglog("Return all authorised Roles of this org");
   
   var promise = db.get(tblName).find({status_id:82},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var roleType = (doc.roleType == 1 ? 'System' : 'Business');
        var tmpArray = {roleId: doc.roleId,roleName:doc.roleName,roleType:roleType,status:doc.status_id};
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       debuglog(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}     

function getUserData(tblName,userData,callback){
   var totalRows =0;
   var data = {};
   debuglog("start getting data");
   debuglog(userData);
   
  var promise = db.get(tblName).find({userId:userData.users.userCode});
   promise.each(function(doc){
        totalRows++;
        data.userCode = doc.userId;
        data.userName = doc.userName;
        data.userType = (doc.userType == 1 ? 'System' : 'Business');
        data.userEmail = doc.userEmail;
        data.userPhone = doc.userPhone;
        data.userRole = doc.userRole;
        data.userMongo = doc.mongoId;
        data.status = doc.status_id;
        data.statusId = doc.status_id;
   }); 
   promise.on('complete',function(err,doc){
      if(totalRows != 1) {
       data.errorCode = 1; //no data case
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function checkUserIdMain(UserData,callback){
 var tblName = UserData.orgId+'_users';
 var revisiontblName = UserData.orgId+'_Revision_users';
 var maxId = 0;
 
 var promise = db.get(tblName).find({},{sort:{userId:-1},limit:1});
 promise.on('complete',function(err,doc){
       debuglog("Max is not available in monk");
       debuglog("If userId is less than 100 then it is only system ");
       debuglog("start business users with 101");
       debuglog("else increment current max with +1");
       debuglog("Check Count in Revision also because it is possible");
       debuglog("user might have directly submitted in Create");
       debuglog(doc.length)
       if(doc.length >0){
       maxId = parseInt(doc[0].userId);
       debuglog(doc);
       debuglog(maxId);
       debuglog(typeof maxId);
       debuglog(typeof doc);
       if(maxId < 101 || typeof maxId === 'undefined') maxId = 100;
       }
       else
        maxId = 100;
       checkUserIdRevision(maxId,revisiontblName,callback);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkUserIdRevision(maxMainId,tblName,callback){
 
 var maxId = 0;
 var maxRevId = 0;
 
 var promise = db.get(tblName).find({},{sort:{userId:-1},limit:1});
 promise.on('complete',function(err,doc){
      debuglog("Max from revision table and active table are");
      if(doc.length >0){
       maxRevId = parseInt(doc[0].userId);
       if(maxRevId < 101 || typeof maxRevId === 'undefined') maxRevId = 100;
      debuglog(maxRevId);
      debuglog(maxMainId);
      }
      else
       maxRevId = 100;
      if(maxRevId > maxMainId)
         maxId = maxRevId;
      else
         maxId = maxMainId;
         
         maxId++;
         
       debuglog("Maximum is incremented by 1 and returned");
       debuglog("So if both are empty Id will start from 101");
       debuglog("else maximum of main/revision +1");
       debuglog(maxId);
       callback(null,maxId);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

function checkEmailExistMain(UserData,callback){
 var tblName = UserData.orgId+'_users';
 var revisiontblName = UserData.orgId+'_Revision_users';
 var data = {};
 
 var promise = db.get(tblName).count({userEmail:UserData.users.userEmail});
 promise.on('complete',function(err,doc){
      debuglog("If given mail not found then check in Revision");
      debuglog("If found return error")
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else
        checkEmailExistRevision(UserData,revisiontblName,callback);
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkEmailExistRevision(UserData,tblName,callback){
 
 var data = {};
 var promise = db.get(tblName).count({userEmail:UserData.users.userEmail});
 promise.on('complete',function(err,doc){
      debuglog("If given mail not found then check in Main User table");
      debuglog("If found return error");
      debuglog("Because this may be called only for create/edit at which time");
      debuglog("This mail is not supposed to exist in main user table");
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else{
         checkEmailExistMaster(UserData,callback);
       }
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

function checkEmailExistMaster(UserData,callback){
 
 var data = {};
 var promise = db.get('user_master').count({email:UserData.users.userEmail});
 promise.on('complete',function(err,doc){
      debuglog("If given mail not found then return success");
      debuglog("If found return error");
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else{
         data.errorCode = 0;
         callback(null,data)
       }
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

//We are using update for both create and Edit 
function updateUserData(tblName,userId,UserData,callback){
  
var promise = db.get(tblName).update(
  {userId:userId},
  {$set: {userId: userId,
          userName: UserData.users.userName,
          userType: UserData.users.userType,
          userEmail: UserData.users.userEmail,
          userPhone: UserData.users.userPhone,
          userRole:  UserData.users.userRole,
          status_id: UserData.status_id,
          mongoId: UserData.mongoId,
          update_time: new Date}},
        {upsert:true}
        );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function getUsersData(tblName,userIds,callback){
  var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var moment = require('moment');
   var promise = db.get(tblName).find({userId: {$in: userIds}},{sort: {update_time: -1}});
   promise.each(function(doc){
        totalRows++;
        var tmpArray = {userCode: doc.userId,userName:doc.userName,userEmail:doc.userEmail,userRole:doc.userRole.roleName,  readonly: (doc.userType == 1 ? true : false),status:doc.status_id,statusId:doc.status_id};
          tmpArray.date = moment(doc.update_time).format('L');
   resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArray);
     });
   promise.on('error',function(err){
       debuglog(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
}     


function checkUserCode(tblName,UserData,callback){
var totalRows = 0;
var data = {};
var status = 0;

debuglog("We are checking only in Main table");
debuglog("Any row directly submitted is not problem");
debuglog("since it will just overwrite main row when authorised");
debuglog("MongoID is used for reactivating users")
debuglog(UserData);

var promise = db.get(tblName).find({userId: UserData.users.userCode});
promise.each(function(doc){
   status = doc.status_id;
   mongoId = doc.mongoId;
   totalRows++;
});

promise.on('complete',function(err,doc){
 if(totalRows != 0){
   data.errorCode = 5;
   data.status = status;
   data.mongoId = mongoId;
   callback(null,data);
   }
  else{
    data.errorCode = 0;
    callback(null,data); 
   }
});
promise.on('error',function(err){
  console.log(err);
  callback(6,null);

});

}

function moveUserHeader(srcTable,destTable,userId,callback){

var promise1 = db.get(destTable).remove({userId:userId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({userId:userId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
       
        var promise4 = db.get(srcTable).remove({userId:userId});
          promise4.on('success',function(err,doc){
            callback(null,null);
           });
          promise4.on('error',function(err,doc){
            callback(6,null);
          });
       });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function checkEmailExistEditMain(UserData,callback){
 var tblName = UserData.orgId+'_users';
 var revisiontblName = UserData.orgId+'_Revision_users';
 var data = {};
 
 var promise = db.get(tblName).count({$and:[{userEmail:UserData.users.userEmail},{userId:{$ne:UserData.users.userCode}}]});
 promise.on('complete',function(err,doc){
      debuglog("If given name not found then check in Revision");
      debuglog("If found return error")
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else
        checkEmailExistEditRevision(UserData,revisiontblName,callback);
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     }); 

}

function checkEmailExistEditRevision(UserData,tblName,callback){
 
 var data = {};
 var promise = db.get(tblName).count({$and:[{userEmail:UserData.users.userEmail},{userId:{$ne:UserData.users.userCode}}]});
 promise.on('complete',function(err,doc){
      debuglog("If given name not found call check in master table which is common for");
      debuglog("both create and edit since userId is not excluded there");
      debuglog("Return error")
      if(doc != 0){
         data.errorCode = 1;
         callback(null,data)
         }
       else{
         checkEmailExistMaster(UserData,callback);
        }
    });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     }); 

}

function deleteUserRow(tblName,userId,callback){
  
var promise = db.get(tblName).remove({userId:userId});
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}

function copyRoleHeader(srcTable,destTable,roleId,callback){

var promise1 = db.get(destTable).remove({roleId:roleId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({roleId:roleId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
           callback(null,null);
        });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}

function copyUserHeader(srcTable,destTable,userId,callback){

var promise1 = db.get(destTable).remove({userId:userId});
  promise1.on('success',function(err,doc){
    var promise2 = db.get(srcTable).find({userId:userId});
  
  //there must be only one row per Id
   promise2.on('complete',function(err,doc){
    delete doc[0]._id;
     var promise3 = db.get(destTable).insert(doc);
       promise3.on('success',function(err,doc){
           callback(null,null);
        });
     promise3.on('error',function(err,doc){
        callback(6,null);
     });
   });
   promise2.on('error',function(err,doc){
        callback(6,null);
  });
 }); 
promise1.on('error',function(err,doc){
        callback(6,null);
 });
}


exports.RoleCount = getRoleCount;
exports.RoleHeader = getRoleHeader;
exports.RoleData   = getRoleData;
exports.generateRoleId =  checkRoleIdMain;
exports.checkRoleName = checkRoleNameMain;
exports.RoleUpdate = updateRoleData;
exports.RolesData = getRolesData;
exports.moveRoleHeader = moveRoleHeader;
exports.checkRoleCode = checkRoleCode;
exports.checkRoleNameEdit = checkRoleNameEditMain;
exports.deleteRoleCode = deleteRoleRow;
exports.products   = getProducts;
exports.UserCount = getUserCount;
exports.UserHeader = getUserHeader;
exports.UserRoles = getUserRoles;
exports.UserData = getUserData;
exports.generateUserId = checkUserIdMain;
exports.checkEmailExist = checkEmailExistMain;
exports.UserUpdate = updateUserData;
exports.UsersData = getUsersData;
exports.checkUserCode = checkUserCode;
exports.moveUserHeader = moveUserHeader;
exports.checkEmailExistEdit = checkEmailExistEditMain;
exports.deleteUserCode = deleteUserRow;
exports.copyRoleHeader = copyRoleHeader;
exports.copyUserHeader = copyUserHeader;





