function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.customerId === 'undefined' || typeof req.body.businessId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.businessId;
    var cid = req.body.customerId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');  //TODO move all to /utils folder
    var custutils = require('../Render/utils');
    var isError = 0;
    data.customerId = cid;
    data.businessId = uid;
    
    
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
         custStat: function(callback){custutils.checkCustomerId(cid,callback); } 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,1,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
            if(isError == 0) {
              customerEmail = results.custStat.customerEmail;
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              checkCart(req,res,9,data,customerEmail);
            }
          }); 
}

function checkCart(req,res,stat,data,customerEmail) {
   var async = require('async');
   var cartutils = require('../Cart/dataUtils');
   var extend = require('util')._extend;
  
   var isError = 0;
   
   
   var checkData = extend({},data);
   async.parallel({
         itemDetails: function(callback){cartutils.getCart('shopping_cart',checkData,callback);} 
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
          if(isError == 0 && results.itemDetails.errorCode !=0 ) {
              processStatus (req,res,4,data);
              isError = 1;
              return;
             }
            if(isError == 0) {
              var itemsArray = results.itemDetails.items; 
              getItemDetails(req,res,9,data,itemsArray,customerEmail);
            }
          });
}

function getItemDetails(req,res,stat,data,itemsArr,customerEmail) {

var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(itemsArr,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.itemCode,tablName,function(err,results){
     if(err){
     isError = 1;
     callback();
     }
     else {
       key['itemDetails'] = results;
       var searchCond = {itemDetailCode:key.itemDetailCode};
       var tablNameSelection = data.orgId+'_menuitem_detail'; //TODO-->move to common definitions file
       renderUtils.selectionData(data.orgId,data.businessType,key.itemCode,searchCond,tablNameSelection,function(err,results1){
        if(err){
          isError = 1;
          callback();
         }  
     else {
       key['itemSelection'] = results1;
       callback();
      }
     });
    }
   });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else
            createCustDetails(req,res,9,data,itemsArr,customerEmail);
          }
     }); 
} 

function createCustDetails(req,res,stat,data,itemsArray,customerEmail) {
   var async = require('async');
   var datautils = require('./dataUtils');
   
   var extend = require('util')._extend;
   
   var billDetails = req.body.billingDetails;
   
   var billingDetails = extend({},billDetails);
       billingDetails.custId = data.customerId;
       billingDetails.custEmail = customerEmail;
       
   //frontend will send both billing and shipping Details
   //incase if shipping Details is not send then use billing Details itself as shipping Details 
   if(req.body.shippingDetails){
     var shipDetails = req.body.shippingDetails;
     var shippingDetails = extend({},shipDetails);
         shippingDetails.custId = data.customerId;
         shippingDetails.custemail = customerEmail;
        }
   else
     var shippingDetails = billingDetails;
     
   
   
   async.parallel({
         billDetails: function(callback){datautils.createCustBill('cust_bill_details',billingDetails,callback);},
         shipDetails: function(callback){datautils.createCustShip('cust_ship_details',shippingDetails,callback);}
          },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              return;
             }
           else {
              getPaymentGatewayDetails(req,res,9,data,itemsArray,customerEmail);
            }
          });
}

function getPaymentGatewayDetails(req,res,stat,data,itemsArray,customerEmail) {
   var async = require('async');
   var datautils = require('./dataUtils');
    var crypto = require('crypto'),
      algorithm = 'aes-256-ctr',
       password = 'd6F3Efeq';
      
    var secretKey = 'eCwWELxi'; //This is actually SALT for PayU
  
   
   
  //getPayment Gateway details for data.businessId
  //if no row exists or if merchantid/key are blank then send error code 5
  //For payU it seems merchant_id = key and key=salt
  
  //else
  //get options as defined for the given payment gateway id
  // this would contain  successurl, failureurl and cancelurl for
  //given payment gateway id+ all other required parameter name and value as blank
  //This can be defined as json properties in dataUtils and returned
  //like redirecturl, options
  
   datautils.paymentGateway(data.businessId,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
    
     if(result.errorCode == 1){
       //JUST for DEMO create PayU, MyDime Test credentials
       //TODO remove after demo it should give payment gateway not found error as below
        var cipher = crypto.createCipher(algorithm,password)
        var crypted = cipher.update(secretKey,'utf8','hex')
            crypted += cipher.final('hex');
  
       secretKey = crypted;
       var gatewayValues = {id:'11',merchantId:'gtKFFx',key:secretKey}
       generateOrderId(req,res,9,data,itemsArray,gatewayValues,customerEmail);
   
       //processStatus(req,res,5,data);
       }
     else {
     var gatewayValues = {id:result.gatewayId,merchantId:result.merchantId,key:result.key};
     generateOrderId(req,res,9,data,itemsArray,gatewayValues,customerEmail);
     }
    }
  });
  
}

function generateOrderId(req,res,stat,data,itemsArray,gatewayValues,customerEmail) {
   var async = require('async');
   var datautils = require('./dataUtils');

  datautils.orderId(data.orgId,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
     var orderId = result.orderId;
     var txnId = result.txnId;
     data.orderId = orderId;
     setPaymentGatewayOptions(req,res,stat,data,itemsArray,txnId,gatewayValues,customerEmail);
    }
  });
  
}

//Need to encrypt, decrypt based on individual GatewayIds
//Also each gateway can have different keys in options
//So set this as seperate function 
function setPaymentGatewayOptions(req,res,stat,data,itemsArray,txnId,gatewayValues,customerEmail) {
   var async = require('async');
   var datautils = require('./dataUtils');
   
  //decrypt merchantid/key OR key/salt as per definition
  //generate hash like
  //var hash_string = key+'|'+txnid+'|'+amount+'|'+productinfo+'|'+firstname+'|'+email+'|||||||||||'+SALT;

   
  //set option hash, key,salt from above
  //set all other options from request.body
  //We send orgId,businessId and get back so as to search corresponding order tables on response
  
    var billDetails = req.body.billingDetails;
    var orderDetails = {orderId:data.orderId,txnId:txnId,amount:req.body.amount,orgId:data.orgId,bizId:data.businessId,custId:data.customerId,custEmail:customerEmail};
 
   datautils.paymentOptions(req.body.billingDetails,orderDetails,gatewayValues,function(err,result){
    if(err)
       processStatus(req,res,5,data); //gateway issues
    else{
    
     var rUrl = result.redirectUrl;
     var postParams = result.postParams;
   
     data.redirectUrl = rUrl;
     data.postParams = postParams;
     createOrderDetails(req,res,stat,data,itemsArray,orderDetails);
     }
  });
}

//create orgid_order_header and orgid_order_detail tables
//with itemsArray values
//Will remove cart entry for the customer on sucess/failure payment process
function createOrderDetails(req,res,stat,data,itemsArray,orderDetails) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
  datautils.createOrder(data,orderDetails,itemsArray,function(err,result){
    if(err)
       processStatus(req,res,6,data); //system Error
    else
       processStatus(req,res,9,data);
    });


}
 
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Checkout/checkout');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
