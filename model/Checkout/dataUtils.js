function updateCustBilling(tablName,updateData,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get(tablName).update(
  {customer_id: updateData.custId},
  {$set:
    {firstName: updateData.firstName,
    lastName: updateData.lastName,
    address: updateData.address,
    city: updateData.city,
    state: updateData.state,
    country: updateData.country,
    zipCode: updateData.zipCode,
    phone: updateData.phone,
    deliveryOption: updateData.deliveryOption,
    payOption: updateData.payOption,
    create_date: new Date()
    }},
    {upsert:true});
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
 
 }


 //Both are exactly same but not sure if will change
 //TODO if not changing then remove this method 
 //just pass table name different to above method
function updateCustShipping(tablName,updateData,callback){
 
  var totalRows =0;
  var data = {};
  
  if(typeof updateData.custId === 'undefined')
    callback(null,null); //because shipping details can be empty
  
  else{ 
  var promise = db.get(tablName).update(
  {customer_id: updateData.custId},
  {$set:
    {firstName: updateData.firstName,
    lastName: updateData.lastName,
    address: updateData.address,
    city: updateData.city,
    state: updateData.state,
    country: updateData.country,
    zipCode: updateData.zipCode,
    phone: updateData.phone,
    create_date: new Date()
    }},
    {upsert:true});
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
  }
 
 }

//First generate order Id
//Order Id= OrgId+YYYYMMDD+sequence number from order_sequence
//Then get Txn Id
//if no row exists in order_payment table for order_id then
//txn_id = orderId+"_1"
//else txn_id = txn_id+1;

function getOrderId(orgId,invoicePicture,callback){
 
  var totalRows =0;
  var data = {};
  var moment = require('moment');
  var curr_time = moment().format('DDMMYYYY');
 
  var currseqnum = "";
  debuglog("Currently Invoice picture has orgId and counter as mandatory");
  debuglog("only date can be made on/off so there is no need to change sequence table");
  debuglog("TODO when this becomes more dyanmic with many columns this may become a seperate file itself");
  debuglog(invoicePicture);
  
  var tblName = orgId+"_order_sequence"; 
  
  var promise = db.get(tblName).find({org_id:orgId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    currseqnum = doc.running_sequence;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     //first order for this orginazation for today; 
      db.get(tblName).insert(
       { org_id: orgId,
         running_sequence: '00001' //insert as string to maintain 000 
      });
      if(invoicePicture.date == true)
        orderId = orgId+curr_time+'0001';
      else
        orderId = orgId+'0001';
        
      getOrderTxnId(orgId,orderId,callback);
    }
    else {
      var next_sequence = parseInt(currseqnum);
      next_sequence++;
      next_seq_str = String("0000000"+next_sequence).slice(-5);
      var promise = db.get(tblName).update(
       { org_id: orgId,
       },
      {  $set: { running_sequence: next_seq_str }},
      {upsert: false});
      if(invoicePicture.date == true)
        orderId = orgId+curr_time+next_seq_str;
      else
        orderId = orgId+next_seq_str;
        
      getOrderTxnId(orgId,orderId,callback);
   }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }
 
function getOrderTxnId(orgId,orderId,callback){
 
  var totalRows =0;
  var data = {};
  var txnid = "";
  
  var tblName = orgId+"_order_payment"; 
  
  var promise = db.get(tblName).find({org_id:orgId,order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    txnid = doc.txn_id;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     //first TXN for this order  
     var txnId = orderId+'_1';
     data.orderId = orderId;
     data.txnId = txnId;
     callback(null,data);
    }
    else {
      txnIdArr = txnid.split("_");
      var txnArrlength = txnIdArr.length-1;
      maxtxnId = parseInt(tnxIdArr[txnArrlength]);
      maxtxnId++;
      var txnId = orderId+'_'+maxtxnId;
     data.orderId = orderId;
     data.txnId = txnId;
     callback(null,data);
   }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

function getPaymentGateway(tblName,isSuperUser,callback){
 
  var totalRows =0;
  var data = {};
  
  var promise = db.get(tblName).find({$and:
             [{status_id:84},
              {'gatewayType.label': 'Prod'},
             {gatewayPriority:1}]});
  promise.each(function(doc){
        totalRows++;
        data.gatewayId = doc.gatewayId;
        data.name = doc.name;
        data.gatewayPriority = doc.gatewayPriority;
        data.key = doc.key;
        data.salt = doc.salt;
        data.paymentTypes   = doc.paymentTypes;
        data.defaultPaymentType = doc.defaultPaymentType;
        data.paymentSubTypes = doc.paymentSubTypes;
        data.customMessage = doc.customMessage;
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
      if(isSuperUser == 1){
      debuglog("This is a super user so check if Test PG is available");
      debuglog("And render it");
      getTestPaymentGateway(tblName,callback);
      }
      else{
       debuglog("this is a normal buyer so error out if no Prod PG is released");
       data.errorCode = 1;
       callback(null,data);
       }
     }
    else {
          getPaymentGatewayMaster(data,callback);
         }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }


 function getTestPaymentGateway(tblName,callback){
 
  var totalRows =0;
  var data = {};
  
  var promise = db.get(tblName).find({$and:
             [{status_id:84},
              {'gatewayType.label': 'Test'},
             {gatewayPriority:1}]});
  promise.each(function(doc){
        totalRows++;
        data.gatewayId = doc.gatewayId;
        data.name = doc.name;
        data.gatewayPriority = doc.gatewayPriority;
        data.key = doc.key;
        data.salt = doc.salt;
        data.paymentTypes   = doc.paymentTypes;
        data.defaultPaymentType = doc.defaultPaymentType;
        data.paymentSubTypes = doc.paymentSubTypes;
        data.customMessage = doc.customMessage;
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
       debuglog("Neither Test nor Prod PG is released for this business");
       data.errorCode = 1;
       callback(null,data);
     }
    else {
          getPaymentGatewayMaster(data,callback);
         }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

 function getPaymentGatewayById(tblName,pgId,callback){
 
  var totalRows =0;
  var data = {};
  
  var promise = db.get(tblName).find({pgId:pgId});
  promise.each(function(doc){
        totalRows++;
        data.gatewayId = doc.gatewayId;
        data.name = doc.name;
        data.gatewayPriority = doc.gatewayPriority;
        data.key = doc.key;
        data.salt = doc.salt;
        data.paymentTypes   = doc.paymentTypes;
        data.defaultPaymentType = doc.defaultPaymentType;
        data.paymentSubTypes = doc.paymentSubTypes;
        data.customMessage = doc.customMessage;
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
       debuglog("strange error PG Id is not found");
       data.errorCode = 1;
       callback(null,data);
    }
    else {
          getPaymentGatewayMaster(data,callback);
         }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }


 function getPaymentGatewayMaster(tmpArr,callback){
 
  var totalRows =0;
  var data = {};
  
  var promise = db.get('payment_gateways_master').find({$and:[{gateway_id:tmpArr.gatewayId},{gateway_name:tmpArr.name}]});
  promise.each(function(doc){
        totalRows++;
        tmpArr.gatewayURL = doc.gateway_url;
        tmpArr.successURL = doc.success_url;
        tmpArr.failureURL = doc.failure_url;
        tmpArr.cancelURL  = doc.cancel_url;
     });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
       data.errorCode = 1;
       callback(null,data);
     }
    else {
          data.errorCode = 0;
          data.paymentGateway = tmpArr;
          callback(null,data);
         }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

 function setPaymentGatewayParams(billDetails,orderDetails,gateway,callback){
 
  debuglog("Start setting payment gateway params");
  debuglog(gateway);
  
  debuglog("For now both test and Prod are set with same functions");
  debuglog("But if number of input or output params would change then create new fn");
 //will form options array depending on gateway
 //since it can change from gateway to gateway
 //Note 11->PayU-TEST
  switch(gateway.gatewayId){
  
    case 11:
      setPayUTest(billDetails,orderDetails,gateway,callback);
      break;
      
    case 12:
      setPayUTest(billDetails,orderDetails,gateway,callback);
      break;
      
    default:
      callback(5,null);
      break;
    }
}

function setPayUTest(billDetails,orderDetails,gateway,callback){
 
 //TODO custom message and payment sub types
 
   var crypto = require('crypto'),
       algorithm = 'aes-256-ctr',
       password = 'd6F3Efeq';
  
   var hash = require('node_hash/lib/hash');
 
   var key = crypto.createHash("sha256").update(password, "ascii").digest();
   var iv = "1234567890123456";
   var secretKey  = gateway.key;
   var secretSalt = gateway.salt;
   
    //password and algo to match with used in dataUtils of Admin 
    //when creating encryption This is common for all gateways
  
    var txnid =orderDetails.txnId; 
		var amount=orderDetails.amount;
		var productinfo=orderDetails.orderId;
		var firstname=billDetails.firstName;
		var email=orderDetails.custEmail;
    var orgId = orderDetails.orgId;
    var custId = orderDetails.custId;
   
    debuglog("Value before decryption");
    debuglog(secretKey);
    debuglog(secretSalt);
     
    var decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
        decipher.update(secretKey, "hex");
    var decryptedKey = decipher.final("ascii");
  

    var decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
        decipher.update(secretSalt, "hex");
    var decryptedSalt = decipher.final("ascii");
 
    debuglog("Values after decryption");
    debuglog(decryptedKey);
    debuglog(decryptedSalt); 
    
    //should be in exact same format as per pdf of PayU otherwise it will error out
    var hash_string = decryptedKey+'|'+txnid+'|'+amount+'|'+productinfo+'|'+firstname+'|'+email+'|'+orgId+'|'+custId+'|||||||||'+decryptedSalt;
	  debuglog(hash_string);
    debuglog("*************");
	 
   debuglog("Start setting the payment type and subtypes");
   debuglog("First check in payment sub type");
   debuglog("if present then add that as string");
   debuglog("else check in payment type if present add it as string");
   debuglog("payment subtype must be of form VISA|MasterCard etc., as defined by PayU");
   debuglog("PayU defines only 5 payment types as of now");
    
   var payment_types = "";
   
   if(gateway.paymentSubTypes != null && gateway.paymentSubTypes.hasOwnProperty('creditcard')){
     var creditCardTypes = gateway.paymentSubTypes.creditcard;
     debuglog("This is the array got for creditcard");
     debuglog("lets convert it into pipe seperated string");
     var creditCardTypeString = creditCardTypes.join("|");
     debuglog(creditCardTypeString);
     debuglog("and then append to paymenttypes")
     payment_types += creditCardTypeString; 
    }
   else if(gateway.paymentTypes.indexOf('creditcard') > -1){
    debuglog("This has no subtypes just the credit card main type");
    debuglog("so we will append only main type to paymenttypes");
    payment_types += 'creditcard';
   }
   else
    ;
    
  if(gateway.paymentSubTypes != null && gateway.paymentSubTypes.hasOwnProperty('debitcard')){
     var debitCardTypes = gateway.paymentSubTypes.debitcard;
     var debitCardTypeString = debitCardTypes.join("|");
     debuglog(debitCardTypeString);
     debuglog("and then append to paymenttypes")
     payment_types += "|"+debitCardTypeString; 
    }
   else if(gateway.paymentTypes.indexOf('debitcard') > -1){
    payment_types += "|"+'debitcard';
   }
   else
    ;    
     

  if(gateway.paymentSubTypes != null && gateway.paymentSubTypes.hasOwnProperty('netbanking')){
     var netbankingTypes = gateway.paymentSubTypes.netbanking;
     var netbankingTypeString = netbankingTypes.join("|");
     debuglog(netbankingTypeString);
     payment_types += "|"+netbankingTypeString; 
    }
   else if(gateway.paymentTypes.indexOf('netbanking') > -1){
    payment_types += "|"+'netbanking';
   }
   else
    ;    
     
 
  if(gateway.paymentSubTypes != null && gateway.paymentSubTypes.hasOwnProperty('EMI')){
     var EMITypes = gateway.paymentSubTypes.EMI;
     var EMITypeString = EMITypes.join("|");
     debuglog(EMITypeString);
     payment_types += "|"+EMITypeString; 
    }
   else if(gateway.paymentTypes.indexOf('EMI') > -1){
    payment_types += "|"+'EMI';
   }
   else
    ;    
 
  if(gateway.paymentSubTypes != null && gateway.paymentSubTypes.hasOwnProperty('cashcard')){
     var cashcardTypes = gateway.paymentSubTypes.cashcard;
     var cashcardTypeString = cashcardTypes.join("|");
     debuglog(cashcardTypeString);
     payment_types += "|"+cashcardTypeString; 
    }
   else if(gateway.paymentTypes.indexOf('cashcard') > -1){
    payment_types += "|"+'cashcard';
   }
   else
    ;    
    
    debuglog("So the final payment types are:");
    debuglog(payment_types);
   
   if(gateway.hasOwnProperty('defaultPaymentType')){
      (gateway.defaultPaymentType == 'debitcard') ? defaultPay = 'DC' :'';
      (gateway.defaultPaymentType == 'creditcard') ?defaultPay = 'CC' :'';
      (gateway.defaultPaymentType == 'netbanking') ?defaultPay = 'NB': '';
      (gateway.defaultPaymentType == 'EMI') ?defaultPay = 'EMI' :'';
      (gateway.defaultPaymentType == 'cashcard') ?defaultPay = 'CASH': '';
     }
   else
     var defaultPay = 'CC';
   
	  var payment_gateway_hash = hash.sha512(hash_string.toString());
    var nodeURL = global.nodeURL;
    
    var options = {
          firstname:billDetails.firstName,
          lastname:billDetails.lastName,
          phone:billDetails.phone,
          key:decryptedKey, 
          hash:payment_gateway_hash,
          surl: nodeURL+gateway.successURL,
          curl: nodeURL+gateway.cancelURL,
          furl: nodeURL+gateway.failureURL,
          txnid:orderDetails.txnId,
          productinfo:orderDetails.orderId,
          amount:orderDetails.amount,
          email:orderDetails.custEmail,
          udf1: orgId,
          udf2: custId,
          custom_note: gateway.customMessage,
          enforce_paymethod: payment_types,
          pg: defaultPay
        };
    
     var redirectUrl = gateway.gatewayURL; 
     var data = {redirectUrl:redirectUrl,postParams:options};
     callback(null,data);
}

 function createOrderHeader(ordData,ordDetails,items,callback){
 
  var totalRows =0;
 
 var tablName = ordData.orgId+'_order_header';
    
  var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    org_id: ordData.orgId,
    customer_id: ordData.customerId,
    delivery_option: ordData.deliveryOption,
    order_status: 80,
    create_date: new Date(),
    update_time: new Date
    });
    promise.on('success',function(doc){
         createOrderDetails(ordData,ordDetails,items,callback);
      });
   promise.on('error',function(err){
       debuglog(err);
       //system error
       callback(6,null);
     });
 
 }

function createOrderDetails(ordData,ordDetails,itemsArr,callback){
  var async = require('async');
 
  var totalRows =0;
  var isError = 0;
 
  var tablName = ordData.orgId+'_order_detail';
  var lineItem = 1;
  
  debuglog(ordData);
  debuglog(ordDetails);
  debuglog(itemsArr);
  
  async.forEachOf(itemsArr,function(key,value,callback1){
    if(isError == 0){
      var uprice = parseFloat(key.itemSelection.selectionData[0].price);
      var qty = key.qty;
      
     
     var Total = +(Math.round((qty*uprice) + "e+2")  + "e-2");
     //Round to last 2 digits
    //TODO generalize tax processing later 
    //Saving unit price, tax % here since if price changes
    //then difficult to reconcile order & price for refunds
    
    var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    line_item: lineItem,
    itemCode: key.itemCode,
    itemDetailCode: key.itemDetailCode,
    qty : key.qty,
    unitPrice: uprice,
    total: Total
    });
    promise.on('success',function(doc){
         lineItem++;
         callback1();
      });
   promise.on('error',function(err){
       debuglog(err);
       //system error
       isError = 1;
       callback1();
     }); 
     }
    else
     callback1(); 
  },
  function(err){
      if(err)
        callback(6,null);
      else
          createOrderStatus(ordData,80,'Check Out Created Order',callback);
      }); 
}

function createOrderStatus(ordData,status,reason,callback){
 
 var totalRows =0;
 
 var tablName = 'orders_status';
    
 var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    org_id: ordData.orgId,
    status_id: status,
    reason: reason,
    create_date: new Date(),
    });
    promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       debuglog(err);
       //system error
       callback(6,null);
     });
 
}


function updateOrderHeader(ordData,status,callback){
 
 var totalRows =0;
 
 var tablName = ordData.orgId+'_order_header';
    
  var promise = db.get(tablName).update(
  {order_id: ordData.orderId},
  {$set:{ order_status: status,
         update_time: new Date}},
  {upsert:false}
  );
    promise.on('success',function(doc){
         createOrderStatus(ordData,status,'Payment Gateway Response',callback);
      });
   promise.on('error',function(err){
       debuglog(err);
       //system error
       callback(6,null);
     });
 
}

function createOrderPayment(ordData,status,errorCode,options,gatewayValues,callback){
 
  var totalRows =0;
 
  var tablName = ordData.orgId+'_order_payment';
 
 //TODO: this is for response from PayU
 //Make this function generic so as to handle response from multiple payment gateways
 //Order header and order details are fine since it is internal params
 //need to handle txnid and mihpayid others can be common across all PG
    
  var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    txn_id: options.txnid,
    pay_option: 'online',
    pg_id: gatewayValues.gatewayId,
    payment_status: status,
    error_code: errorCode,
    amount: parseFloat(options.amount).toFixed(2),
    pg_unique_ref: options.mihpayid,
    pg_response: options,
    create_date: new Date(),
    update_time: new Date
    });
    promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       debuglog(err);
       //system error
       callback(6,null);
     });
 
 }

//This is similar to removeShoppingCart in Cart Utils
//But this is for all rows of customer while that is for one item
 function removeShoppingCartForCustomer(tablName,ordData,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get(tablName).remove({
    customer_id: ordData.customerId,
          });
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       debuglog(err);
       //system error
       callback(6,data);
     });
 
 }
 
 function getOrderHeader(ordData,callback){
 
  var totalRows =0;
  var data = {};

  var tblName = ordData.orgId+"_order_header";
  var orderId = ordData.orderId; 
  
  var promise = db.get(tblName).find({order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    data.orderId = doc.order_id;
    data.orderStatus = doc.order_status;
    data.deliveryOption = doc.delivery_option;
   	data.customerId  = doc.customer_id;
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     data.errorCode = 1;
     callback(null,data);
    }
    else {
       getOrderDetails(ordData,data,callback);
   }
  });
  promise.on('error',function(err){
    debuglog(err);
    callback(6,null);//system error
  });
 }

function getOrderDetails(ordData,data,callback){
 
  var totalRows =0;
  var resultArr = [];
  
  var tblName = ordData.orgId+"_order_detail";
  var orderId = ordData.orderId; 
  
  var promise = db.get(tblName).find({order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    var tmpArray ={itemCode:doc.itemCode,itemDetailCode:doc.itemDetailCode,qty:doc.qty};
    tmpArray.total = doc.total;
    resultArr.push(tmpArray);
   });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     data.errorCode = 1;
     callback(null,data);
    }
    else {
      data.orderDetails = resultArr;
      getOrderPayment(ordData,data,callback);
   }
  });
  promise.on('error',function(err){
    debuglog(err);
    callback(6,null);//system error
  });
 }

 function getOrderPayment(ordData,data,callback){
 
  var totalRows =0;
  var resultArr = [];
  var tmpArr = {};
  
  var tblName = ordData.orgId+"_order_payment";
  var orderId = ordData.orderId; 
  
  var promise = db.get(tblName).find({order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    tmpArray ={txnId:doc.txn_id,amount:doc.amount,status:doc.payment_status,errorCode:doc.error_code};
   if(doc.pay_option == 'cash'){
    pgResponse= '';
    paymentMode = 'Cash';
	tmpArray.paymentMode = paymentMode;
   } 
   else {
    pgResponse = doc.pg_response;
    paymentMode = 'Online'+pgResponse.mode;
    tmpArray.paymentMode = paymentMode;
    }
    });
  promise.on('complete',function(err,doc){
     data.paymentDetails = tmpArray;
      getOrderStatusName(ordData,data,callback);
  });
  promise.on('error',function(err){
    debuglog(err);
    callback(6,null);//system error
  });
 }

function getOrderStatusName(ordData,data,callback){
  var mastutils = require('../Registration/masterUtils');
  var statusId = data.orderStatus; 
  
   mastutils.masterStatusNames('orders_status_master',statusId,function(err,result) {
           if(err) callback(6,null);
         else {
               data.orderStatus = result.statusName;
               getCustomerDetails(ordData,data,callback); 
              }
           }); 
}
 
function getCustomerDetails(ordData,data,callback1){

   var async = require('async');
   var datautils = require('../Cart/dataUtils');
   var extend = require('util')._extend;
  
   var tabl_name = '';
   var isError = 0;
   
   var orgId = ordData.orgId;
   var bizType = ordData.businessType;
   var custId = data.customerId;
   
   var checkData = extend({},data);
   async.parallel({
         billAddress: function(callback){datautils.custDetails('cust_bill_details',custId,callback); },
         shipAddress: function(callback){datautils.custDetails('cust_ship_details',custId,callback); },
         },
         function(err,results) {
           if(err) {
              debuglog(err);
             callback1(6,null);//system error
             isError = 1;
              return;
             }
           if(isError == 0 && results.billAddress.errorCode != 0 ) {
              callback1(6,null);//no billing Address is an error
              isError = 1;
              return;
             }
           if(isError == 0 && results.shipAddress.errorCode !=0 ) {
              callback1(6,null);//no shipping Address is an error
              isError = 1;
              return;
             }
            if(isError == 0) {
               data.billingDetails = results.billAddress.custDetails;
               data.shippingDetails = results.shipAddress.custDetails;
               callback1(null,data);
            }
          });
}

function createCashOrderPayment(ordData,status,errorCode,callback){
 
  var totalRows =0;
 
  var tablName = ordData.orgId+'_order_payment';
 
 //TODO: this is for response from Cash payments
    
  var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    txn_id: ordData.txnId,
    pay_option: 'cash',
    payment_status: status,
    error_code: errorCode,
    amount: parseFloat(ordData.amount).toFixed(2),
    create_date: new Date(),
    update_time: new Date
    });
    promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       debuglog(err);
       //system error
       callback(6,null);
     });
 
 }

 function getDeliveryCost(tblName,sellerId,deliveryOption,callback){
 
  var totalRows =0;
  var result = "";
  var data = {};
  
  debuglog("Due to some weird way taxes are done cant use directly deliveryoption");
  debuglog("so get name and then use it in query");
  debuglog("This may work since business cant edit names in charges");
  debuglog("There is no need to check delivery option itself here because it is done in getItemsIncart");
  debuglog("So just get the charge name here and get charges from next function");
  
  var promise = db.get('delivery_options_master').find({$and:[{delivery_id:deliveryOption},
                       {enabled:true}]});
  promise.each(function(doc){
      totalRows++;
      result = doc.delivery_name;
    //TODO consider % later
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     data.errorCode = 1;
     debuglog("No enabled charges for this business so error out as setup issues")
     callback(null,data);
    }
    else {
      data.errorCode = 0;
     getDeliveryCostByOption(tblName,sellerId,result,callback);
     debuglog("Now deliveryoption has changed from id to name");
      //callback(null,data);
   }
  });
  promise.on('error',function(err){
    debuglog(err);
    callback(6,null);//system error
  });
 }


function getDeliveryCostByOption(tblName,sellerId,deliveryOption,callback){
 
  var totalRows =0;
  var resultArr = [];
  var data = {};
  
  debuglog("For now only fixed cost is considered i.e., rateType=1");
  debuglog("Delivery cost as % of amount i.e., rateType=1 will be done after MVP");
  
  var promise = db.get(tblName).find({$and:[{sellerId:sellerId},
                       {'chargeType.name':deliveryOption},
                       {enabled:true},
                       {status_id:82}]});
  promise.each(function(doc){
      totalRows++;
      var tmpArr = {};
        tmpArr.chargeType = doc.chargeType.name;
        tmpArr.rateType = doc.rateType;
        tmpArr.applyRateAt = doc.applyRateAt;
        tmpArr.rate = doc.rate;
        resultArr.push(tmpArr);
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     //data.errorCode = 1;
	 data.errorCode = 0;
	 debuglog("No charges defined for the deliveryoption is not an error anymore");
	 debuglog("Just pass empty array back");
	 data.deliveryData = resultArr;
     callback(null,data);
    }
    else {
      data.errorCode = 0;
      data.deliveryData = resultArr;
      callback(null,data);
   }
  });
  promise.on('error',function(err){
    debuglog(err);
    callback(6,null);//system error
  });
 }

 function getInvoiceConfigData(tblName,InvoiceConfigData,callback) {

  var totalRows = 0;
  var data = {};
  var result = {};
  debuglog(InvoiceConfigData);
  var promise = db.get(tblName).find({$and:[{sellerId:InvoiceConfigData.sellerId},
                                           {status_id: 82}]});
  promise.each(function(doc){
    totalRows++;
    result.invoiceConfigName = doc.InvoiceConfigName;
    result.cin = doc.cin;
    result.tin = doc.tin;
    result.cst = doc.cst;
    result.chargeIds = doc.chargeIds;
    result.disclaimer = doc.disclaimer;
    result.sellerName = doc.sellerName;
    result.sellerAddress = doc.sellerAddress;
    result.itemCode = doc.itemCode;
    result.itemDetailCode = doc.itemDetailCode;
    result.quantity = doc.quantity;
    result.price = doc.price;
    result.totals = doc.totals;
    result.buyerName = doc.buyerName;
    result.billingAddress = doc.billingAddress;
    result.shippingAddress = doc.shippingAddress;
    });
  promise.on('complete',function(err,doc){
  debuglog(totalRows);
  debuglog(doc);
  debuglog(result);
    if(totalRows == 0)  {
      result.errorCode = 1; 
      callback(null,result);
    }
    else if(totalRows > 1)
      callback(6,null); //system error should be 1 row only
    else {
      result.errorCode = 0;
      callback(null,result);
      }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(null,result);//system error
  });
}

function getChargeData(tblName,chargeIds,callback){
   var totalRows =0;
   var data = {};
   var resultArr = [];
   debuglog("This is similar to cart getChargeData");
   debuglog("Except we use chargeIds here");
   debuglog("sellerId check is redundant")
   
  var promise = db.get(tblName).find(
             {$and:[
             {sellerId:1},{status_id:82},{enabled:true},
             {chargeId:{$in:chargeIds}}]});
   promise.each(function(doc){
        totalRows++;
        var tmpArr = {};
        tmpArr.chargeName = doc.chargeName;
        tmpArr.chargeType = doc.chargeType;
        tmpArr.rateType = doc.rateType;
        tmpArr.applyRateAt = doc.applyRateAt;
        tmpArr.rate = doc.rate;
        resultArr.push(tmpArr);
   }); 
   promise.on('complete',function(err,doc){
         data.chargeData = resultArr;
         callback(null,data);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getInvoicePicture(tblName,sellerId,callback){


   var totalRows =0;
   var resultArr = [];
   
  var promise = db.get(tblName).find(
             {$and:[{sellerId:1},{status_id:82}]});
   promise.each(function(doc){
        totalRows++;
        debuglog(doc);
        resultArr.push(doc.invoicePicture[0]);
        debuglog(resultArr);
   }); 
   promise.on('complete',function(err,doc){
         callback(null,resultArr[0]);
     });
   promise.on('error',function(err){
       console.log(err);
       callback(6,null);
     });
}


function getOrderItemDetails(orgId,itemCode,itemDetailCode,callback){
   var totalRows =0;
   var data = {};
  
   var tblName = orgId+"_menuitem_master";
   
   var itemName = "";
   debuglog("This is similar to menuitem and order getOrderItemDetails");
   debuglog("But this does not check for field_purpose since field_purpose");
   debuglog("is null for price as it is not rendered for selection"); 
     
   var promise = db.get(tblName).find({itemCode:itemCode});
   promise.each(function(doc){
        totalRows++;
        itemName = doc.itemName;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       data.itemName = "";
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.itemName = itemName;
		     getOrderSelectionData(orgId,itemCode,itemDetailCode,data,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getOrderSelectionData(orgId,itemCode,itemDetailCode,data,callback){
   var totalRows =0;
   var async = require('async');
 
  var tblName = orgId+"_menuitem_detail";
  
    var promise = db.get(tblName).find({
             $and:[{itemCode:itemCode},{itemDetailCode:itemDetailCode}]
         });
   promise.each(function(doc){
        totalRows++;
        itemPrice = doc.price;
       }); 
  promise.on('success',function(doc){
     if(totalRows == 0) {
       data.errorCode = 1; //no data case and some strange error
       data.price = 0;
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.price = itemPrice;
		     callback(null,data);
         } 
   });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
} 


exports.createCustBill =  updateCustBilling;
exports.createCustShip =  updateCustShipping;
exports.orderId        = getOrderId;
exports.paymentGateway = getPaymentGateway;
exports.paymentOptions = setPaymentGatewayParams;
exports.createOrder    = createOrderHeader;
exports.updateOrder    = updateOrderHeader;
exports.updatePayment  = createOrderPayment;
exports.removeShoppingCartForCustomer = removeShoppingCartForCustomer;
exports.orderDetails = getOrderHeader;
exports.updateCashPayment = createCashOrderPayment;
exports.deliveryCost = getDeliveryCost;
exports.invoiceConfigData = getInvoiceConfigData;
exports.chargeData = getChargeData;
exports.paymentGatewayById = getPaymentGatewayById;
exports.invoicePicture = getInvoicePicture;
exports.moreOrderDetails = getOrderItemDetails;
