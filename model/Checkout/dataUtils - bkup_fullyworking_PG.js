function updateCustBilling(tablName,updateData,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get(tablName).update(
  {customer_id: updateData.custId},
  {$set:
    {firstName: updateData.firstName,
    lastName: updateData.lastName,
    address: updateData.address,
    city: updateData.city,
    state: updateData.state,
    country: updateData.country,
    zipCode: updateData.zipCode,
    phone: updateData.phone,
    create_date: new Date()
    }},
    {upsert:true});
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
 
 }


 //Both are exactly same but not sure if will change
 //TODO if not changing then remove this method 
 //just pass table name different to above method
function updateCustShipping(tablName,updateData,callback){
 
  var totalRows =0;
  var data = {};
  
  if(typeof updateData.custId === 'undefined')
    callback(null,null); //because shipping details can be empty
  
  else{ 
  var promise = db.get(tablName).update(
  {customer_id: updateData.custId},
  {$set:
    {firstName: updateData.firstName,
    lastName: updateData.lastName,
    address: updateData.address,
    city: updateData.city,
    state: updateData.state,
    country: updateData.country,
    zipCode: updateData.zipCode,
    phone: updateData.phone,
    create_date: new Date()
    }},
    {upsert:true});
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
  }
 
 }

//First generate order Id
//Order Id= OrgId+YYYYMMDD+sequence number from order_sequence
//Then get Txn Id
//if no row exists in order_payment table for order_id then
//txn_id = orderId+"_1"
//else txn_id = txn_id+1;

function getOrderId(orgId,callback){
 
  var totalRows =0;
  var data = {};
  var moment = require('moment');
  var curr_time = moment().format('DDMMYYYY');
  var currseqnum = "";
 
  var tblName = orgId+"_order_sequence"; 
  
  var promise = db.get(tblName).find({org_id:orgId,dateString:curr_time},{stream: true});
  promise.each(function(doc){
    totalRows++;
    currseqnum = doc.running_sequence;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     //first order for this orginazation for today; 
      db.get(tblName).insert(
       { org_id: orgId,
         dateString: curr_time,
        running_sequence: '00001' //insert as string to maintain 000 
      });
      orderId = orgId+curr_time+'0001';
      getOrderTxnId(orgId,orderId,callback);
    }
    else {
      var next_sequence = parseInt(currseqnum);
      next_sequence++;
      next_seq_str = String("0000000"+next_sequence).slice(-5);
      var promise = db.get(tblName).update(
       { org_id: orgId,
         dateString: curr_time
      },
      {  $set: { running_sequence: next_seq_str }},
      {upsert: false});
      orderId = orgId+curr_time+next_seq_str;
      getOrderTxnId(orgId,orderId,callback);
   }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }
 
function getOrderTxnId(orgId,orderId,callback){
 
  var totalRows =0;
  var data = {};
  var txnid = "";
  
  var tblName = orgId+"_order_payment"; 
  
  var promise = db.get(tblName).find({org_id:orgId,order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    txnid = doc.txn_id;
  });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     //first TXN for this order  
     var txnId = orderId+'_1';
     data.orderId = orderId;
     data.txnId = txnId;
     callback(null,data);
    }
    else {
      txnIdArr = txnid.split("_");
      var txnArrlength = txnIdArr.length-1;
      maxtxnId = parseInt(tnxIdArr[txnArrlength]);
      maxtxnId++;
      var txnId = orderId+'_'+maxtxnId;
     data.orderId = orderId;
     data.txnId = txnId;
     callback(null,data);
   }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

function getPaymentGateway(bizId,callback){
 
  var totalRows =0;
  var data = {};
  var gatewayid = "";
  var merchantid = "";
  var key     = "";
  
  var tblName = "user_payment_gateway"; 
  
  var promise = db.get(tblName).find({user_id:bizId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    gatewayId = doc.gateway_id;
    merchantId = doc.merchant_id;
    key        = doc.secret_key;
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
    data.errorCode = 1;
     callback(null,data);
    }
    else {
    if(gatewayId == '' || merchantId == '' || key == '' || gatewayId == null || merchantId == null || key == null)
    {
     data.errorCode = 1;
     callback(null,data);
    }
   else {
       data.key = key;
       data.merchantId = merchantId;
       data.id = gatewayId;
       data.errorCode = 1;
       callback(null,data);
       }
   }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

 function setPaymentGatewayParams(billDetails,orderDetails,gateway,callback){
 
 //will form options array depending on gateway
 //since it can change from gateway to gateway
 //TODO Decrypt key,merchantid
 //Create hash string
 //11->PayU
   var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq'; 
    //password and algo to match with used in dataUtils 
    //when creating encryption
    //This is common for all gateways
  
   var hash = require('node_hash/lib/hash');
   
  switch(gateway.id){
  
     case"11":
    var txnid =orderDetails.txnId; 
		var amount=orderDetails.amount;
		var productinfo=orderDetails.orderId;
		var firstname=billDetails.firstName;
		var email=orderDetails.custEmail;
    var orgId = orderDetails.orgId;
    var bizId = orderDetails.bizId;
    var custId = orderDetails.custId;
    
    var key = gateway.merchantId ; //TODO decrypt this  later when getting from DB
    var salt = gateway.key;
	
      var decipher = crypto.createDecipher(algorithm,password)
      var dec = decipher.update(gateway.key,'hex','utf8')
      dec += decipher.final('utf8');
      SALT = dec;
      
      var decipher = crypto.createDecipher(algorithm,password)
      var dec = decipher.update(gateway.merchantId,'hex','utf8')
      dec += decipher.final('utf8');
      key = dec;
      
      key = gateway.merchantId;
      	
   
   	
    //should be in exact same format as per pdf of PayU otherwise it will error out
    var hash_string = key+'|'+txnid+'|'+amount+'|'+productinfo+'|'+firstname+'|'+email+'|'+orgId+'|'+bizId+'|'+custId+'||||||||'+SALT;
	  console.log(hash_string);
    console.log("*************");
	  
	  var payment_gateway_hash = hash.sha512(hash_string.toString());
    var nodeURL = global.nodeURL;
         var options = {
          firstname:billDetails.firstName,
          lastname:billDetails.lastName,
          phone:billDetails.phone,
          key:key, 
          hash:payment_gateway_hash,
          surl: nodeURL+'/sucessPayUPayment',
          curl:nodeURL+'/cancelPayUPayment',
          furl:nodeURL+'/failurePayUPayment',
          txnid:orderDetails.txnId,
          productinfo:orderDetails.orderId,
          amount:orderDetails.amount,
          email:orderDetails.custEmail,
          udf1: orgId,
          udf2: bizId,
          udf3: custId
        };
     var redirectUrl = 'https://test.payu.in/_payment'; //This is PayU test url
     var data = {redirectUrl:redirectUrl,postParams:options};
     callback(null,data);
     break;
     
     default:
     callback(5,null);
     break;
    }
 }

 function createOrderHeader(ordData,ordDetails,items,callback){
 
  var totalRows =0;
 
 var tablName = ordData.orgId+'_order_header';
    
  var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    business_id: ordData.businessId,
    org_id: ordData.orgId,
    customer_id: ordData.customerId,
    order_status: 80,
    create_date: new Date(),
    update_time: new Date
    });
    promise.on('success',function(doc){
         createOrderDetails(ordData,ordDetails,items,callback);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
 
 }

 function createOrderDetails(ordData,ordDetails,itemsArr,callback){
    var async = require('async');
 
  var totalRows =0;
  var isError = 0;
 
 var tablName = ordData.orgId+'_order_detail';
 var lineItem = 1;
  
  
  async.forEachOf(itemsArr,function(key,value,callback1){
    if(isError == 0){
    var uprice = parseFloat(key.itemSelection.selectionData[0].price);
     if(key.itemDetails.masterData[0].vat)
      var tax1Percent = key.itemDetails.masterData[0].vat;
     else
      var tax1Percent = 0; 
     if(key.itemDetails.masterData[0].serviceTax)
      var tax2Percent = key.itemDetails.masterData[0].serviceTax;
     else
      var tax2Percent = 0; 
      
      var qty = key.qty;
      
     
     var total = +(Math.round((qty*uprice) + "e+2")  + "e-2");
     
     var tax1 = +(Math.round(((total*tax1Percent)/100) + "e+2")+"e-2");
     var tax2 = +(Math.round(((total*tax2Percent)/100) + "e+2")+"e-2"); 
    
     var Total = total+tax1+tax2;
     //Round to last 2 digits
    //TODO generalize tax processing later 
    //Saving unit price, tax % here since if price changes
    //then difficult to reconcile order & price for refunds
    
    var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    line_item: lineItem,
    itemCode: key.itemCode,
    itemDetailCode: key.itemDetailCode,
    qty : key.qty,
    unitPrice: uprice,
    vat: tax1Percent,
    serviceTax: tax2Percent,
    total: Total
    });
    promise.on('success',function(doc){
         lineItem++;
         callback1();
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       isError = 1;
       callback1();
     }); 
     }
    else
     callback1(); 
  },
  function(err){
      if(err)
        callback(6,null);
      else
          createOrderStatus(ordData,80,'Check Out Created Order',callback);
      }); 
  
 
 }

 function createOrderStatus(ordData,status,reason,callback){
 
  var totalRows =0;
 
 var tablName = 'orders_status';
    
  var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    business_id: ordData.businessId,
    org_id: ordData.orgId,
    status_id: status,
    reason: reason,
    create_date: new Date(),
    });
    promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
 
 }


 function updateOrderHeader(ordData,status,callback){
 
  var totalRows =0;
 
 var tablName = ordData.orgId+'_order_header';
    
  var promise = db.get(tablName).update(
  {order_id: ordData.orderId},
  {$set:{ order_status: status,
         update_time: new Date}},
  {upsert:false}
  );
    promise.on('success',function(doc){
         createOrderStatus(ordData,status,'Payment Gateway Response',callback);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
 
 }

 function createOrderPayment(ordData,status,errorCode,options,gatewayValues,callback){
 
  var totalRows =0;
 
  var tablName = ordData.orgId+'_order_payment';
 
 //TODO: this is for response from PayU
 //Make this function generic so as to handle response from multiple payment gateways
 //Order header and order details are fine since it is internal params
 //need to handle txnid and mihpayid others can be common across all PG
    
  var promise = db.get(tablName).insert({
    order_id: ordData.orderId,
    txn_id: options.txnid,
    pg_id: gatewayValues.id,
    payment_status: status,
    error_code: errorCode,
    amount: parseFloat(options.amount).toFixed(2),
    pg_unique_ref: options.mihpayid,
    pg_response: options,
    create_date: new Date(),
    update_time: new Date
    });
    promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,null);
     });
 
 }

//This is similar to removeShoppingCart in Cart Utils
//But this is for all rows of customer while that is for one item
 function removeShoppingCartForCustomer(tablName,ordData,callback){
 
  var totalRows =0;
  var data = {};
   
  var promise = db.get(tablName).remove({
    customer_id: ordData.customerId,
          });
   promise.on('success',function(doc){
         callback(null,null);
      });
   promise.on('error',function(err){
       console.log(err);
       //system error
       callback(6,data);
     });
 
 }
 
 function getOrderHeader(ordData,callback){
 
  var totalRows =0;
  var data = {};

  var tblName = ordData.orgId+"_order_header";
  var orderId = ordData.orderId; 
  
  var promise = db.get(tblName).find({order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    data.orderId = doc.order_id;
    data.orderStatus = doc.order_status;
	data.customerId  = doc.customer_id;
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     data.errorCode = 1;
     callback(null,data);
    }
    else {
       getOrderDetails(ordData,data,callback);
   }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

function getOrderDetails(ordData,data,callback){
 
  var totalRows =0;
  var resultArr = [];
  
  var tblName = ordData.orgId+"_order_detail";
  var orderId = ordData.orderId; 
  
  var promise = db.get(tblName).find({order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    var tmpArray ={itemCode:doc.itemCode,itemDetailCode:doc.itemDetailCode,qty:doc.qty};
    tmpArray.vat = doc.vat;
    tmpArray.serviceTax = doc.serviceTax;
    tmpArray.total = doc.total;
    resultArr.push(tmpArray);
    //TODO round vat, servicetax and total
    //DONOT take these details from item table since price could have changed by now 
    });
  promise.on('complete',function(err,doc){
    if(totalRows == 0) {
     data.errorCode = 1;
     callback(null,data);
    }
    else {
      data.orderDetails = resultArr;
      getOrderPayment(ordData,data,callback);
   }
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

 function getOrderPayment(ordData,data,callback){
 
  var totalRows =0;
  var resultArr = [];
  var tmpArr = {};
  
  var tblName = ordData.orgId+"_order_payment";
  var orderId = ordData.orderId; 
  
  var promise = db.get(tblName).find({order_id:orderId},{stream: true});
  promise.each(function(doc){
    totalRows++;
    tmpArray ={txnId:doc.txn_id,amount:doc.amount,status:doc.payment_status,errorCode:doc.error_code};
    pgResponse = doc.pg_response;
    paymentMode = pgResponse.mode;
    tmpArray.mode = paymentMode;
    });
  promise.on('complete',function(err,doc){
     data.paymentDetails = tmpArray;
      getOrderStatusName(ordData,data,callback);
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 }

function getOrderStatusName(ordData,data,callback){
  var mastutils = require('../Registration/masterUtils');
  var statusId = data.orderStatus; 
  
   mastutils.masterStatusNames('orders_status_master',statusId,function(err,result) {
           if(err) callback(6,null);
         else {
               data.orderStatus = result.statusName;
               getCustomerDetails(ordData,data,callback); 
              }
           }); 
}
 
function getCustomerDetails(ordData,data,callback1){

   var async = require('async');
   var datautils = require('../Cart/dataUtils');
   var extend = require('util')._extend;
  
   var tabl_name = '';
   var isError = 0;
   
   var orgId = ordData.orgId;
   var bizType = ordData.businessType;
   var custId = data.customerId;
   
   var checkData = extend({},data);
   async.parallel({
         billAddress: function(callback){datautils.custDetails('cust_bill_details',custId,callback); },
         shipAddress: function(callback){datautils.custDetails('cust_ship_details',custId,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
             callback1(6,null);//system error
             isError = 1;
              return;
             }
           if(isError == 0 && results.billAddress.errorCode != 0 ) {
              callback1(6,null);//no billing Address is an error
              isError = 1;
              return;
             }
           if(isError == 0 && results.shipAddress.errorCode !=0 ) {
              callback1(6,null);//no shipping Address is an error
              isError = 1;
              return;
             }
            if(isError == 0) {
               data.billingDetails = results.billAddress.custDetails;
               data.shippingDetails = results.shipAddress.custDetails;
               callback1(null,data);
            }
          });
}


exports.createCustBill =  updateCustBilling;
exports.createCustShip =  updateCustShipping;
exports.orderId        = getOrderId;
exports.paymentGateway = getPaymentGateway;
exports.paymentOptions = setPaymentGatewayParams;
exports.createOrder    = createOrderHeader;
exports.updateOrder    = updateOrderHeader;
exports.updatePayment  = createOrderPayment;
exports.removeShoppingCartForCustomer = removeShoppingCartForCustomer;
exports.orderDetails = getOrderHeader;



