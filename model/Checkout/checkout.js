function processRequest(req,res){
    var data = {};
   if( typeof req.body.customerId === 'undefined' 
     || typeof req.body.deliveryOption === 'undefined'
    || typeof req.body.payOption === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus left asis since this is not through customerappcontroller");
}

function checkMasterStatus(req,res,data){
    
    var oid = req.body.orgId;
    var cid = req.body.customerId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');  //TODO move all to /utils folder
    var custutils = require('../Render/utils');
    var isError = 0;
    data.customerId = cid;
    data.orgId = oid;
    data.deliveryOption = req.body.deliveryOption;
    data.payOption = req.body.payOption;
    
    
    
     async.parallel({
         usrStat: function(callback){roleutils.orgStatus(oid,callback); },
         custStat: function(callback){custutils.checkCustomerId(cid,callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.custStat.errorCode != 0) {
             processStatus (req,res,results.custStat.errorCode,data);
             isError = 1;
             return;
            }
            if(isError == 0) {
              customerEmail = results.custStat.customerEmail;
              data.orgId = results.usrStat.orgId;
              data.businessType = results.usrStat.bizType;
              checkCart(req,res,9,data,customerEmail);
            }
          }); 
}

function checkCart(req,res,stat,data,customerEmail) {
   var async = require('async');
   var cartutils = require('../Cart/dataUtils');
   var extend = require('util')._extend;
  
   var isError = 0;
   var isSuperUser = 0;
   
   
   var checkData = extend({},data);
   async.parallel({
         itemDetails: function(callback){cartutils.getCart('shopping_cart',checkData,callback);},
         superUser: function(callback) {cartutils.isSuperUser(customerEmail,data.orgId,callback); } 
         },
         function(err,results) {
          debuglog("Results for itemDetails and superuser is");
          debuglog(results);
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
          if(isError == 0 && results.itemDetails.errorCode !=0 ) {
              processStatus (req,res,4,data);
              isError = 1;
              return;
             }
          if(isError == 0 && results.superUser == 'super'){
            isSuperUser = 1;
          }
            if(isError == 0) {
              var itemsArray = results.itemDetails.items; 
              getItemDetails(req,res,9,data,itemsArray,customerEmail,isSuperUser);
            }
          });
}

function getItemDetails(req,res,stat,data,itemsArr,customerEmail,isSuperUser) {

var async = require('async');
var tablName = data.orgId+'_menuitem_master'; //TODO-->move to common definitions file
var isError = 0;

 var itemUtils = require('../Menu/utils');
 var renderUtils = require('../AppMenu/datautils'); 
 async.forEachOf(itemsArr,function(key,value,callback){
   itemUtils.masterData(data.orgId,data.businessType,key.itemCode,tablName,function(err,results){
     if(err){
     isError = 1;
     callback();
     }
     else {
       key['itemDetails'] = results;
       var searchCond = {itemDetailCode:key.itemDetailCode};
       var tablNameSelection = data.orgId+'_menuitem_detail'; //TODO-->move to common definitions file
       renderUtils.selectionData(data.orgId,data.businessType,key.itemCode,searchCond,tablNameSelection,function(err,results1){
        if(err){
          isError = 1;
          callback();
         }  
     else {
       key['itemSelection'] = results1;
       callback();
      }
     });
    }
   });
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           if(isError == 1)
             processStatus(req,res,6,data);
           else
            getDeliveryCost(req,res,9,data,itemsArr,customerEmail,isSuperUser);
          }
     }); 
} 

function getDeliveryCost(req,res,stat,data,itemsArray,customerEmail,isSuperUser) {
   var async = require('async');
   var datautils = require('./dataUtils');
  
   debuglog("Get Delivery cost based on orgId");
   debuglog("And delivery option as passed in body from charges table");
   debuglog("TODO Add various rules based calcuation after MVP");
   debuglog("For now we are considering only the Main Branch");
   debuglog("TODO add actual sellerId based processing later")
   
   var tblName = data.orgId+"_charge_type";
   var deliveryOption = data.deliveryOption;
   var sellerId = 1; //req.body.sellerId; 
   
   datautils.deliveryCost(tblName,sellerId,deliveryOption,function(err,result){
    debuglog(result);
    if(err)
         processStatus(req,res,6,data);
    else{
     if(result.errorCode == 1)
         processStatus(req,res,5,data);
     else {
        debuglog("after getting result from delivery cost fn");
        debuglog("Note as of now only fixed delivery cost is considered");
        debuglog("% of amount type TODO");
        debuglog(result);
        var deliveryCost = result.deliveryData;
        calculateDeliveryCost(req,res,9,data,itemsArray,customerEmail,deliveryCost,isSuperUser);
       }
    }
  });
  
}


function calculateDeliveryCost(req,res,stat,data,itemsArray,customerEmail,deliveryCost,isSuperUser){
 var processArr = [];
 var async = require('async');
 var amount = req.body.amount;
 var totalPrice = 0;
 
   debuglog("The actual price before calculations is:");
   debuglog(amount);
   debuglog("And the delivery costs are:");
   debuglog(deliveryCost);
      
   async.forEachOf(deliveryCost,function(key,value,callback){
   debuglog("Charge item:");
   debuglog(key);
   debuglog(value);
   var chargeType = key.chargeType;
   
   if(key.applyRateAt == 2 || key.applyRateAt == null){
   debuglog("This is a charge applicable on Totals");
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var totAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      totalPrice += totAmount;
      debuglog(totAmount);
      debuglog(totalPrice);
      callback();
     }
    else{
      debuglog("Rate at Percentage");
      var totAmount = +(Math.round(((actualPrice*key.rate)/100) + "e+2")+"e-2");
      totalPrice += totAmount;
       debuglog(totAmount);
       debuglog(totalPrice);
      callback();
     }
    }
    else{
  
  debuglog("This charge is applicable on item level");
  debuglog("TODO after MVP not applicable for MVP");
  debuglog("For now it is decided to leave applyRateAt as null")
  var tmpTotals = 0;
   async.forEachOf(itemsArray,function(key1,value1,callback1){
      debuglog("For item");
      debuglog(key1);
      debuglog(value1);
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var totAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      tmpTotals += totAmount;
      key1[chargeType] = totAmount;
      callback1();
     }
    else{
      debuglog("Rate at Percentage");
      var totAmount = +(Math.round(((key1.price*key.rate)/100) + "e+2")+"e-2");
      tmpTotals += totAmount;
      key1[chargeType] = totAmount;
      callback1();
     }
    },
  function(err){
      if(err)
        callback(6,null);
      else {
      debuglog(totalPrice);
      callback();
     }
    });
   }
   },
   function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
         var deliveryCost = +(Math.round((req.body.amount+totalPrice) + "e+2")  + "e-2");
 
        createCustDetails(req,res,9,data,itemsArray,customerEmail,deliveryCost,isSuperUser);
        }
    }); 

    
 }

function createCustDetails(req,res,stat,data,itemsArray,customerEmail,deliveryCost,isSuperUser) {
   var async = require('async');
   var datautils = require('./dataUtils');
  
   var invoiceConfigTblName= data.orgId+'_invoice_config';
   
   var extend = require('util')._extend;
   
   var billDetails = req.body.billingDetails;
   
   var billingDetails = extend({},billDetails);
       billingDetails.custId = data.customerId;
       billingDetails.custEmail = customerEmail;
       billingDetails.deliveryOption = data.deliveryOption;
       billingDetails.payOption = data.payOption; 
       
   //frontend will send both billing and shipping Details
   //incase if shipping Details is not send then use billing Details itself as shipping Details
   //getting invoiceconfig details of only main seller TODO change to actual seller later 
   if(req.body.shippingDetails){
     var shipDetails = req.body.shippingDetails;
     var shippingDetails = extend({},shipDetails);
         shippingDetails.custId = data.customerId;
         shippingDetails.custEmail = customerEmail;
        }
   else
     var shippingDetails = billingDetails;
     
   async.parallel({
         billDetails: function(callback){datautils.createCustBill('cust_bill_details',billingDetails,callback);},
         shipDetails: function(callback){datautils.createCustShip('cust_ship_details',shippingDetails,callback);},
         pictureDetails: function(callback){datautils.invoicePicture(invoiceConfigTblName,1,callback);}
          },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              return;
             }
           else {
              var invoicePicture = results.pictureDetails;
              if(data.payOption == 1){
               debuglog("This is a cash order");
               debuglog("So no need to get or set any payment gateway details");
               debuglog("just create order and call successCashPayment API to remove cart item");
               debuglog("And render response");
               debuglog("testing gitpush from desktop");
               generateCashOrderId(req,res,9,data,itemsArray,customerEmail,deliveryCost,invoicePicture);
              }
              else{
              debuglog("This is an online payment");
              debuglog("So do all processings as required");
              getPaymentGatewayDetails(req,res,9,data,itemsArray,customerEmail,deliveryCost,isSuperUser,invoicePicture);
              }
            }
          });
}

function getPaymentGatewayDetails(req,res,stat,data,itemsArray,customerEmail,deliveryCost,isSuperUser,invoicePicture) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var pgId  = 0;
  
   debuglog("Get Released status PG with priority-1");
   debuglog("TODO implement on fail get next PG in failure url");
   debuglog("If no row then we will send error and not proceed further");
   debuglog("As customer cant checkout without PG"); 
  
   debuglog("This fn will also get success failure urls payment.. from ");
   debuglog("PG master for the defined gateway Id");
   
   var tblName = data.orgId+"_payment_gateway";
   if( typeof req.body.pgId != 'undefined' && req.body.pgId != null)
       pgId = req.body.pgId;  
       debuglog("super user check and pg id is:");
       debuglog(isSuperUser);
       debuglog(pgId);
    if(isSuperUser == 1 && pgId != 0 ){
     debuglog("This is a super user with specific pgId so get that details");
     debuglog(pgId)
       datautils.paymentGatewayById(tblName,pgId,function(err,result){
    if(err)
         processStatus(req,res,6,data);
    else{
     if(result.errorCode == 1)
         processStatus(req,res,5,data);
     else {
        debuglog("after getting result from paymentGateway fn");
        debuglog(result);
        var gatewayValues = result.paymentGateway;
        generateOrderId(req,res,9,data,itemsArray,gatewayValues,customerEmail,deliveryCost,invoicePicture);
      }
     }
   });
  } else {
   datautils.paymentGateway(tblName,isSuperUser,function(err,result){
    if(err)
         processStatus(req,res,6,data);
    else{
     if(result.errorCode == 1)
         processStatus(req,res,5,data);
     else {
        debuglog("after getting result from paymentGateway fn");
        debuglog(result);
        var gatewayValues = result.paymentGateway;
        generateOrderId(req,res,9,data,itemsArray,gatewayValues,customerEmail,deliveryCost,invoicePicture);
      }
     }
   });
  }
}

function generateOrderId(req,res,stat,data,itemsArray,gatewayValues,customerEmail,deliveryCost,invoicePicture) {
   var async = require('async');
   var datautils = require('./dataUtils');

  datautils.orderId(data.orgId,invoicePicture,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
     var orderId = result.orderId;
     var txnId = result.txnId;
     data.orderId = orderId;
     setPaymentGatewayOptions(req,res,stat,data,itemsArray,txnId,gatewayValues,customerEmail,deliveryCost);
    }
  });
  
}

function generateCashOrderId(req,res,stat,data,itemsArray,customerEmail,deliveryCost,invoicePicture) {
   var async = require('async');
   var datautils = require('./dataUtils');

  datautils.orderId(data.orgId,invoicePicture,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
     var orderId = result.orderId;
     var txnId = result.txnId;
     data.orderId = orderId;
  
    ///var totAmount = +(Math.round((req.body.amount+deliveryCost) + "e+2")  + "e-2");
    var totAmount = deliveryCost;
    debuglog("Now delivery cost is already added earlier");
    debuglog("Total amount after adding delivery cost is:");
    debuglog(totAmount);
        var orderDetails = {orderId:orderId,txnId:txnId,amount:totAmount,orgId:data.orgId,customerId:data.customerId,custEmail:customerEmail};
        orderDetails.deliveryOption = req.body.deliveryOption;
        
        datautils.createOrder(data,orderDetails,itemsArray,function(err,result){
         if(err)
            processStatus(req,res,6,data); //system Error
          else{
            debuglog("Cash order Id is generated ");
            debuglog("Now let us process the success cash payment API");
            var cashPay = require('./successCashPayment.js');
             cashPay.processData(req,res,9,data,orderDetails)
            }
         });
       }
  });
}

//Need to encrypt, decrypt based on individual GatewayIds
//Also each gateway can have different keys in options
//So set this as seperate function 
function setPaymentGatewayOptions(req,res,stat,data,itemsArray,txnId,gatewayValues,customerEmail,deliveryCost) {
   var async = require('async');
   var datautils = require('./dataUtils');
   
  //decrypt merchantid/key OR key/salt as per definition
  //generate hash like
  //var hash_string = key+'|'+txnid+'|'+amount+'|'+productinfo+'|'+firstname+'|'+email+'|||||||||||'+SALT;
 
  //We send orgId and get back so as to search corresponding order tables on response
     var totAmount = deliveryCost;
    debuglog("Now delivery cost is already added earlier");
    debuglog("Total amount after adding delivery cost is:");
    debuglog("TODO log amount and delivery cost seperately somewhere");
    debuglog("May be required later probably");
    debuglog(totAmount);
  
    var billDetails = req.body.billingDetails;
    var orderDetails = {orderId:data.orderId,txnId:txnId,amount:totAmount,orgId:data.orgId,custId:data.customerId,custEmail:customerEmail};
    orderDetails.deliveryOption = req.body.deliveryOption;
        
   datautils.paymentOptions(req.body.billingDetails,orderDetails,gatewayValues,function(err,result){
    if(err)
       processStatus(req,res,5,data); //gateway issues
    else{
         var rUrl = result.redirectUrl;
         var postParams = result.postParams;
   
         data.redirectUrl = rUrl;
         data.postParams = postParams;
         createOrderDetails(req,res,stat,data,itemsArray,orderDetails);
       }
    });
}

//create orgid_order_header and orgid_order_detail tables
//with itemsArray values
//Will remove cart entry for the customer on sucess/failure payment process
function createOrderDetails(req,res,stat,data,itemsArray,orderDetails) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
  datautils.createOrder(data,orderDetails,itemsArray,function(err,result){
    if(err)
       processStatus(req,res,6,data); //system Error
    else
       processStatus(req,res,9,data);
    });


}
 
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Checkout/checkout');
  
    controller.process_status(req,res,stat,data);
}

exports.getData = processRequest;
