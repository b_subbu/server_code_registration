function processRequest(request,response) {
    var datautils = require('./dataUtils');
 
    var data = {}; 
 
    var options= request.body;
    debuglog("This is the response received from PayU");
    debuglog(options);
    
    var status = options.status; 
    var options= request.body;
    var orgId = options.udf1;
    var custId = options.udf2;
   
    var tblName = orgId+"_payment_gateway";
     debuglog("we are always passing superuser as true here");
     debuglog("because actual filtering would have been done at checkout itself")
    
   datautils.paymentGateway(tblName,1,function(err,result){
    if(err)
       processStatus(request,response,6,data);
    else{
    
      if(result.errorCode == 1)
         processStatus(request,response,5,data);
       else {
        debuglog("after getting result from paymentGateway fn");
        debuglog(result);
        var gatewayValues = result.paymentGateway;   
        updateOrderId(request,response,7,data,options,gatewayValues,orgId,custId);
      }
     }
  });
  
}

function updateOrderId(req,res,stat,data,options,gateway,orgId,custId) {
   var async = require('async');
   var datautils = require('./dataUtils');
  
   var crypto = require('crypto'),
       algorithm = 'aes-256-ctr',
       password = 'd6F3Efeq';
  
   var key = crypto.createHash("sha256").update(password, "ascii").digest();
   var iv = "1234567890123456";
   var secretKey  = gateway.key;
   var secretSalt = gateway.salt;     var data = {};
   
   var hash = require('node_hash/lib/hash');
  
   var orderId = options.productinfo;
    
  
    debuglog("Value before decryption");
    debuglog(secretKey);
    debuglog(secretSalt);
     
    var decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
        decipher.update(secretKey, "hex");
    var decryptedKey = decipher.final("ascii");
  

    var decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
        decipher.update(secretSalt, "hex");
    var decryptedSalt = decipher.final("ascii");
 
    debuglog("Values after decryption");
    debuglog(decryptedKey);
    debuglog(decryptedSalt); 
  
     var hash_string = decryptedSalt+'|'+options.status+'|'+options.udf1+'|'+options.udf2+'|||||||||'+options.email+'|'+options.firstname+'|'+options.productinfo+'|'+options.amount+'|'+options.txnid+'|'+decryptedKey;
 
	   var payment_response_hash = hash.sha512(hash_string.toString());
		
     var status = 82; 
     var ErrorCode = 'PayU returned Failure '+options.error_Message;
    //probably no need to check security error for cancelled or failed TX
       
     var ordData = {orderId:orderId,orgId:orgId,customerId:custId}  
     datautils.updateOrder(ordData,status,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
     data.orderId = orderId;
     data.errorCode = ErrorCode;
     data.customerId = custId;
     data.orgId = orgId;
     
     updateOrderPayment(req,res,7,data,ordData,status,gateway,options);
    }
  });
  
}

function updateOrderPayment(req,res,stat,data,ordData,status,gatewayValues,options) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
   paymentStatus = 'Failed';
 
 var ErrorCode = data.errorCode;
   datautils.updatePayment(ordData,paymentStatus,ErrorCode,options,gatewayValues,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
        getOrderDetails(req,res,7,data);
    }
  });
  
}

//Donot remove cart for failed Payments

function getOrderDetails(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
     var extend = require('util')._extend;
   
   var ordDetails = extend({},data);
  
   datautils.orderDetails(ordDetails,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
       data.orderStatus    = result.orderStatus;
       data.billingDetails = result.billingDetails;
       data.shippingDetails = result.shippingDetails;
       data.orderDetails    = result.orderDetails;
       data.paymentDetails =  result.paymentDetails;
       processStatus(req,res,7,data);
    }
  });
  
}
 
 
 function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Checkout/failurePayUPayment');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;


