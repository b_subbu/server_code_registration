function processRequest(request,response) {
   
   //Need to use model to save OrderId return status 
   //Need to get key and salt based on business id.
      var async = require('async');
     var datautils = require('./dataUtils');
     var data = {};

    var hash = require('node_hash/lib/hash');
     //var key='C0Dr8m'; //This is generic test key
    var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq'; 
 
      var options= request.body;
     //console.log(options);
     var status = options.status; 
     var options= request.body;
     var bizId = options.udf2;
     var orgId = options.udf1;
     var custId = options.udf3;
   
    var secretKey = 'eCwWELxi'; //This is actually SALT for PayU
      
     datautils.paymentGateway(bizId,function(err,result){
    if(err)
       processStatus(request,response,6,data);
    else{
    
     if(result.errorCode == 1){
       //JUST for DEMO create PayU, MyDime Test credentials
       //TODO remove after demo it should give payment gateway not found error as below
        var cipher = crypto.createCipher(algorithm,password)
        var crypted = cipher.update(secretKey,'utf8','hex')
            crypted += cipher.final('hex');
  
       secretKey = crypted;
       var gatewayValues = {id:'11',merchantId:'gtKFFx',key:secretKey}
       updateOrderId(request,response,9,data,options,gatewayValues,bizId,orgId,custId);
   
       //processStatus(req,res,5,data);
       }
     else {
        var gatewayValues = {id:result.gatewayId,merchantId:result.merchantId,key:result.key};
        updateOrderId(requset,response,9,data,options,gatewayValues,bizId,orgId,custId);
     }
    }
  });
  
}

function updateOrderId(req,res,stat,data,options,gateway,bizId,orgId,custId) {
   var async = require('async');
   var datautils = require('./dataUtils');

    var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq'; 
     var data = {};
   
   var hash = require('node_hash/lib/hash');

   var orderId = options.productinfo;
    
    var key = gateway.merchantId ; //TODO decrypt this  if required
    var salt = gateway.key;
	
      var decipher = crypto.createDecipher(algorithm,password)
      var dec = decipher.update(gateway.key,'hex','utf8')
      dec += decipher.final('utf8');
      SALT = dec;
      
    var hash_string = SALT+'|'+options.status+'||||||||'+options.udf3+'|'+options.udf2+'|'+options.udf1+'|'+options.email+'|'+options.firstname+'|'+options.productinfo+'|'+options.amount+'|'+options.txnid+'|'+key;
    //console.log(hash_string);
    var generated_response_hash = hash.sha512(hash_string.toString());
	
    //console.log(generated_response_hash);
   
  //compare generated hash with actual returned by PG
  //be very careful with number of pipes in string when adding/removing params
  
    if(generated_response_hash == options.hash && options.status == 'success'){
       var status = 81; //Payment->success === Ready2Ship
       var ErrorCode = '';
       }
    else
       {
       var status = 82; //Payment failed due to security error
       var ErrorCode = 'Security Check failed';
       }
       
     var ordData = {orderId:orderId,businessId:bizId,orgId:orgId,customerId:custId}  
     datautils.updateOrder(ordData,status,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
     data.orderId = orderId;
     data.errorCode = ErrorCode;
     data.customerId = custId;
     data.businessId = bizId;
     data.orgId = orgId;
     //data.businessType = bizType; //TODO: send biz type as a param and get back in response
       updateOrderPayment(req,res,9,data,ordData,status,gateway,options);
    }
  });
  
}

function updateOrderPayment(req,res,stat,data,ordData,status,gatewayValues,options) {
   var async = require('async');
   var datautils = require('./dataUtils');
   if(status == 81)
     paymentStatus = 'Success';
   else
    paymentStatus = 'Failed';
 
 var ErrorCode = data.errorCode;
   datautils.updatePayment(ordData,paymentStatus,ErrorCode,options,gatewayValues,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
        removeFromCart(req,res,9,data,ordData,status);
        
    }
  });
  
}

//TODO send all order details, billing details etc., to show in Invoice screen
function removeFromCart(req,res,stat,data,ordData,status) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
   datautils.removeShoppingCartForCustomer('shopping_cart',ordData,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
       if(status != 81)
        processStatus(req,res,7,data);
       else
        getOrderDetails(req,res,9,data);
    }
  });
  
}


function getOrderDetails(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
     var extend = require('util')._extend;
   
   var ordDetails = extend({},data);
  
   datautils.orderDetails(ordDetails,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
       data.orderStatus    = result.orderStatus;
       data.billingDetails = result.billingDetails;
       data.shippingDetails = result.shippingDetails;
       //data.orderDetails    = result.orderDetails;
       var orderDetails = result.orderDetails;
       data.paymentDetails =  result.paymentDetails;
       //processStatus(req,res,9,data);
       getMoreOrderDetails(req,res,9,data,orderDetails);
    }
  });
  
}
  
function getMoreOrderDetails(req,res,stat,data,ordDetails){

   var async = require('async');
   var datautils = require('../Orders/dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
     var extend = require('util')._extend;
     var orgId  = data.orgId;
     
      async.forEachOf(ordDetails,function(key1,value1,callback1){
        var itemCode=key1.itemCode;
        var itemDetailCode = key1.itemDetailCode;
        
            datautils.moreOrderDetails(orgId,itemCode,itemDetailCode,function(err,result) {
         if(err) callback1();
         else {
                key1.itemName = result.itemName;
                key1.selectionData = result.selectionData;
                 callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           data.orderDetails = ordDetails;
           processStatus(req,res,9,data);
        }
      }); 
}      

 
 function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Checkout/successPayUPayment');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;

