function processRequest(request,response,status,data,ordData) {
   
     var datautils = require('./dataUtils');
   
     debuglog("This is the response received from checkout");
    debuglog(ordData);
    
    var orgId = data.orgId;
    var custId = data.customerId;
   
        updateOrderId(request,response,9,data,orgId,custId,ordData);
}

function updateOrderId(req,res,stat,data,orgId,custId,ordData) {
  
   var orderId = ordData.orderId;
   var datautils = require('./dataUtils');
     
  //compare generated hash with actual returned by PG
  //be very careful with number of pipes in string when adding/removing params
  
       var status = 81; //Payment->success === Ready2Ship
       var ErrorCode = '';
 
    datautils.updateOrder(ordData,status,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
     data.orderId = ordData.orderId;
     data.errorCode = ErrorCode;
     data.customerId = custId;
     data.orgId = orgId;
     //data.businessType = bizType; //TODO: send biz type as a param and get back in response if needed
     updateOrderPayment(req,res,9,data,ordData,status);
    }
  });
  
}

function updateOrderPayment(req,res,stat,data,ordData,status) {
   var async = require('async');
   var datautils = require('./dataUtils');
   debuglog("For cash orders order status will always be success");
   debuglog("we create payment status with Pending");
   debuglog("This needs to be manually updated to Success by Business later")
   if(status == 81)
     paymentStatus = 'Pending'; 
   else
    paymentStatus = 'Failed';
 
 var ErrorCode = '';
   datautils.updateCashPayment(ordData,paymentStatus,ErrorCode,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
        removeFromCart(req,res,9,data,ordData,status);
      }
  });
  
}

//TODO send all order details, billing details etc., to show in Invoice screen
function removeFromCart(req,res,stat,data,ordData,status) {
   var async = require('async');
   var datautils = require('./dataUtils');
 
   datautils.removeShoppingCartForCustomer('shopping_cart',ordData,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
       if(status != 81)
        processStatus(req,res,7,data);
       else
        getOrderDetails(req,res,9,data,ordData);
    }
  });
  
}


function getOrderDetails(req,res,stat,data,ordData) {
   var async = require('async');
   var datautils = require('./dataUtils');
     var extend = require('util')._extend;
   
   var ordDetails = extend({},data);
  
   datautils.orderDetails(ordDetails,function(err,result){
    if(err)
       processStatus(req,res,6,data);
    else{
    
       debuglog("result from orderDetails");
       debuglog(result);
       data.orderStatus    = result.orderStatus;
       data.billingDetails = result.billingDetails;
       data.shippingDetails = result.shippingDetails;
       //data.orderDetails    = result.orderDetails;
       var orderDetails = result.orderDetails;
       data.paymentDetails =  result.paymentDetails;
       ordData.deliveryOption = result.deliveryOption;
       //processStatus(req,res,9,data);
       getMoreOrderDetails(req,res,9,data,orderDetails,ordData);
    }
  });
  
}
  
function getMoreOrderDetails(req,res,stat,data,ordDetails,ordData){

   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
     var extend = require('util')._extend;
     var orgId  = data.orgId;
     
      async.forEachOf(ordDetails,function(key1,value1,callback1){
        var itemCode=key1.itemCode;
        var itemDetailCode = key1.itemDetailCode;
        
            datautils.moreOrderDetails(orgId,itemCode,itemDetailCode,function(err,result) {
         if(err) callback1();
         else {
                debuglog("result for more item details for");
                debuglog(key1);
                debuglog("is");
                debuglog(result)
                key1.itemName = result.itemName;
                //key1.itemPrice = result.itemPrice;
                key1.itemPrice = result.price;
                //key1.selectionData = result.selectionData;
                debuglog(key1);
                 callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           data.orderDetails = ordDetails;
           getInvoiceConfigDetails(req,res,9,data,ordData);
        }
      }); 
}      

function getInvoiceConfigDetails(req,res,stat,data,ordData) {
   var async = require('async');
   var isError = 0;
      
   var sellerutils = require('../Sellers/dataUtils');
   var configutils = require('./dataUtils');  
 
 debuglog("Currently we are calling another function to get InvoiceConfig Details");
 debuglog("But this can be split into two to get Auhtorised Invoiceconfig Id of sellerId");
 debuglog("And then use it in dataUtils of InvoiceConfig to get data TODO");
 
  var sellerTabl = data.orgId+'_sellers';
  var configTabl = data.orgId+'_invoice_config';
    
  var sellerData = {};
       sellerData.sellerId = 1;
  
  async.parallel({
         sellerDets: function(callback){sellerutils.SellerData(sellerTabl,sellerData,callback); },
         configDets: function(callback){configutils.invoiceConfigData(configTabl,sellerData,callback); }
        },
         function(err,results) {
          debuglog("After results");
          debuglog(results);
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.sellerDets.errorCode != 0) {
              processStatus (req,res,5,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.configDets.errorCode != 0) {
              processStatus (req,res,5,data);
              isError = 1;
              return;
             }
           if(isError == 0) {
                var sellerDetails = results.sellerDets.businessDetails;
                var configDetails = results.configDets;
                debuglog("After getting results");
                debuglog(sellerDetails);
                debuglog(configDetails);
                setPDFData(req,res,9,data,sellerDetails,configDetails,ordData);
            }
         });     
 }


function setPDFData(req,res,stat,data,sellerDetails,configDetails,ordData){
debuglog("This fn generates PDF, saves in folder and emails it");
debuglog("This is called async since no need to wait until mail sent to render response");
debuglog("only basic version done now need to do all features later");
debuglog("Template file is in views directory since express requires it to be there");
debuglog("TODO Invoice.pdf name must be Invoice ID_CustomerID_DDMMYYYY");
debuglog("TODO move setting of pdfData to utils since lots of variables needs to be set");
debuglog("And this may be common for cash and PayU Payments");

var async = require('async');
var moment = require('moment');
var currDate = moment().format('DD-MM-YYYY');

var pdfData = {};
if(configDetails.sellerName == true)
  pdfData.sellerName = sellerDetails.displayName;
else
  pdfData.sellerName = '';

if(configDetails.sellerAddress == true){  
pdfData.sellerAddress1 = sellerDetails.street;
pdfData.sellerAddress2 = sellerDetails.city.name+' '+sellerDetails.state.name+' '+sellerDetails.country.countryName;
pdfData.sellerAddress3 = sellerDetails.zipCode;
}
else{
pdfData.sellerAddress1 = '';
pdfData.sellerAddress2 = '';
pdfData.sellerAddress3 = '';

}

if(configDetails.cin == true)
 pdfData.cinNumber = sellerDetails.cinNumber;
else
 pdfData.cinNumber = '';
 
if(configDetails.tin == true)
  pdfData.tinNumber = sellerDetails.tinNumber;
else
  pdfData.tinNumber = '';

if(configDetails.cst == true)  
  pdfData.cstNumber = sellerDetails.cstNumber;
else
  pdfData.cstNumber = '';

pdfData.orderId = data.orderId;
pdfData.orderDate = currDate;

if(configDetails.buyerName == true)
  pdfData.buyerName =       data.billingDetails.firstName+' '+data.billingDetails.lastName;
else
 pdfData.buyerName == '';  
 
 
pdfData.buyerEmail =      ordData.custEmail;
if(configDetails.billingAddress == true){
pdfData.billingAddress1 = data.billingDetails.address;
pdfData.billingAddress2 = data.billingDetails.city+' '+data.billingDetails.state+' '+data.billingDetails.country;
pdfData.billingAddress3 = data.billingDetails.zipCode;
}
else{
pdfData.billingAddress1 = '';
pdfData.billingAddress2 = '';
pdfData.billingAddress3 = '';
}

if(configDetails.shippingAddress == true){
pdfData.shippingAddress1 = data.shippingDetails.address;
pdfData.shippingAddress2 = data.shippingDetails.city+' '+data.shippingDetails.state+' '+data.shippingDetails.country;
pdfData.shippingAddress3 = data.shippingDetails.zipCode;
}
else{
pdfData.shippingAddress1 = '';
pdfData.shippingAddress2 = '';
pdfData.shippingAddress3 = '';

}

pdfData.paymentMethod = 'Cash';

pdfData.disclaimer = configDetails.disclaimer;


debuglog("The data for items is:");
debuglog(data.orderDetails);

var itemsArr = data.orderDetails;
var itemDetailArr = [];
var subTotal = 0;

async.forEachOf(itemsArr,function(key,value,callback){
  var tmpArr = {};
      if(configDetails.itemCode == true)
       tmpArr.itemCode = key.itemCode;
      else
       tmpArr.itemCode = '';
      if(configDetails.itemDetailCode == true)  
        tmpArr.itemDetailCode = key.itemDetailCode;
      else
        tmpArr.itemDetailCode = '';
      if(configDetails.price == true)
        tmpArr.price = key.itemPrice;
      else
        tmpArr.price = '';
      if(configDetails.quantity == true)  
        tmpArr.qty = key.qty;
      else
        tmpArr.qty = '';
      var lineItemTotal = +(Math.round((key.itemPrice*key.qty) + "e+2")+"e-2");   
      if(configDetails.totals == true)    
       tmpArr.total = lineItemTotal;
      else
       tmpArr.total = '';
      subTotal += lineItemTotal;
      itemDetailArr.push(tmpArr);
      callback();
   
},
  function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
           pdfData.items = itemDetailArr; 
           pdfData.subTotal = subTotal;
          //pdfData.baseCurrency = sellerDetails.baseCurrency;
           
           debuglog("hard-coded utf8 code for Indian Rupee");
           debuglog("TODO create currency symbol table and get code from it");
           pdfData.baseCurrency = "&#8377;";
           debuglog("The data for populating template is:");
           debuglog(pdfData);

           setCharges(req,res,9,data,sellerDetails,configDetails,ordData,pdfData);
  }
 });

}
 
function setCharges(req,res,stat,data,sellerDetails,configDetails,ordData,pdfData){
debuglog("Get all charges as defined in InvoiceConfig Details");
debuglog("For MVP it is only Applicable on Totals");
debuglog("TODO for charges applicable on item level");

debuglog("OrdData is:");
debuglog(ordData);

var datautils = require('./dataUtils');
var async = require('async');
var chargeArr = [];

var baseAmount = pdfData.subTotal;
var grandTotal = pdfData.subTotal;

var chargeIds = configDetails.chargeIds;
var tblName   = data.orgId+"_charge_type";
  datautils.chargeData(tblName,chargeIds,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
                debuglog("Result of charge is:");
                debuglog(result);
                debuglog(ordData);
                debuglog("If chargeName.name is Tax or Service then just put chargeType name as label");
                debuglog("Calculate the totals as % or absolute and add to grandtotal");
                debuglog("If not Tax, then check if id matches delivery option");
                debuglog("As of now charge can be only tax or delivery cost");
                debuglog("Not others also charges are applied only on totals for now");
                var chargeData = result.chargeData;
   async.forEachOf(chargeData,function(key,value,callback){
   debuglog("Charge item:");
   debuglog(key);
   debuglog(value);
   var chargeType = key.chargeType;
   var tmpArr = [];
   tmpArr[0] = chargeType.name;
   
   if(key.chargeName.name == 'Tax' || key.chargeName.name == 'Service'){
   debuglog("This is a Tax type charge");
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var thisAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    else{
      debuglog("Rate at Percentage");
      var thisAmount = +(Math.round(((baseAmount*key.rate)/100) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    }
    else {
    debuglog("This is not a Tax so would depend on delivery option passed");
    
   if(key.chargeType.id == ordData.deliveryOption){
        debuglog("This charge is applicable for the given delivery option");
        debuglog("Note charge types are reversed between tax and other!!!");
     tmpArr[0] = key.chargeName.name;
      
    if(key.rateType == 2){
     debuglog("Rate at fixed rate");
      var thisAmount = +(Math.round((key.rate) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    else{
      debuglog("Rate at Percentage");
      var thisAmount = +(Math.round(((baseAmount*key.rate)/100) + "e+2")+"e-2");
      tmpArr[1] = thisAmount;
      chargeArr.push(tmpArr);
      grandTotal += thisAmount;
      debuglog(thisAmount);
      debuglog(grandTotal);
      debuglog(chargeArr);
      callback();
     }
    }
    else
      callback();
    } 
    }, 
    function(err){
      if(err)
        processStatus(req,res,6,data);
      else {
        pdfData.charges = chargeArr;
        pdfData.grandTotal = grandTotal;
        convertUndefined(req,res,stat,data,pdfData);
        //processStatus(req,res,9,data);

       }
    });
  }
 });
} 

function convertUndefined(req,res,stat,data,pdfData){
debuglog("Will convert all undefined properties to null");
debuglog("also buyername address can have undefined in the middle of string which is not a type");
debuglog("tough to combine in assignwith because items is an object");

var _ = require('lodash');
var finalData = {};

_.assignWith(finalData,pdfData,modifyValue);


 
debuglog(finalData);
generatePDF(req,res,stat,data,finalData);
}

function modifyValue(srcValue,objValue){

var _ = require('lodash');
 
 if (_.isUndefined(objValue)){
  debuglog("this is an undefined value so return empty string");
  return '';
 }
 
 if(_.isArray(objValue) || _.isNumber(objValue)){
 debuglog("This is an array probably items charges etc., so dont do anything");
 return objValue;
 }

 if(_.isString(objValue)){
 debuglog("This is a string probably can have a undefined in the middle if any part of a field is not defined");
 debuglog(objValue);
 var x = _.replace(objValue,"undefined","");
 debuglog(x);
 debuglog("will return this modified string then");
 return x;
 
 }

}

function generatePDF(req,res,stat,data,pdfData){
            var fs = require('fs');
            var pdf = require('html-pdf');
            
            
           //var invoiceNumber = data.customerId+"_"+data.orderId+"_"+pdfData.orderDate;
           var invoiceNumber = data.orderId;
           
           var fileName = data.customerId+"_"+invoiceNumber+"_"+pdfData.orderDate+".pdf";
           var filePath = app.get('fileuploadfolder')+'Invoices/';
           
           pdfData.invoiceNumber = invoiceNumber;
           var pdfFile = filePath+fileName;
           debuglog("The data for populating template is:");
           debuglog(pdfData);

           res.render('invoice.jade',pdfData,function(err,htmlfile){
	          //console.log(htmlfile);
            data.htmlData = htmlfile;
            debuglog(err);
            debuglog(data);
            processStatus(req,res,9,data);
            debuglog("No need to wait for email before sending response")
            debuglog("TODO check err here in case if any error happens while creating PDF");
              pdf.create(htmlfile).toFile(pdfFile, function(err, res) {
               if (err) {console.log(err); return;}
                 sendEmail(req,res,9,data,pdfFile,pdfData.buyerEmail)
                
            });
          });
}
 
function sendEmail(req,res,stat,data,pdfFile,toAddr){
          
          var subject = 'Invoice Attached';
          var body  = 'PFA Invoice for your purchase';
          
      var modelpath=app.get('model');
      var model = require(modelpath+'email');
      model.sendPDF(toAddr,subject,body,pdfFile);
      debuglog("email sent. No return from email and no waiting here");
      debuglog("If email fails nothing can be done by application");
      //processStatus(req,res,9,data);
  }
 

 function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Checkout/successCashPayment');
  
    controller.process_status(req,res,stat,data);
}
   
exports.processData = processRequest;


