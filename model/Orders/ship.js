//This file is not currently used
//TO rewrite fully later after shipping module is ready
function processRequest(req,res,data){
   if(typeof req.body.orderId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkOrder(req,res,9,data);
}

function checkOrder(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
   data.orderId = req.body.orderId;
  
     var extend = require('util')._extend;
     var ordData = extend({},data);
     
  
      datautils.checkOrder(ordData,function(err,result) {
      
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              processStatus(req,res,7,data);
              isError = 1;
              return;
              }
            else {
               var custId = result.customerId;
                updateDelivery(req,res,9,data,custId);
             }
          }
        });
} 

function updateDelivery(req,res,stat,data,custId) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
     var extend = require('util')._extend;
     var ordData = extend({},data);
     ordData.customerId = custId;
     
  
      datautils.updateDelivery(ordData,function(err,result) {
      
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
               changeOrderStatus(req,res,9,data);
            }
        });
} 

function changeOrderStatus(req,res,stat,data) {
   var async = require('async');
   var statutils = require('./statusUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
  var tblName = data.orgId+'_order_header';
     
      statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,data.orderId,84,'Shipped',function(err,result) {
      
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
          else {
               getOrdersData(req,res,9,data);
            }
        });
} 

//TODO move to utils and make status list dynamic
function getOrdersData(req,res,stat,data) {
   var async = require('async');
   var utils = require('./dataUtils');
   var type = require('type-detect');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   var orders = [];
   
 
     var extend = require('util')._extend;
     var ordData = extend({},data);
     
   
      utils.orderData(ordData,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.orderDetails = {};
              processStatus(req,res,9,data); //no Orders for the business nothing to do further
              isError = 1;
             //will be handled at count itself, just adding here
            }
            else {
            orders = result.orders; 
            getOrderPaymentData(req,res,9,data,orders);
            }
          }
        });
} 


function getOrderPaymentData(req,res,stat,data,orders) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var masterData = [];
  var payment_tabl_name = data.orgId+'_order_payment';
 
   async.forEachOf(orders,function(key,value,callback){
   var orderId = key.orderId;
   
   
    datautils.orderPayment(payment_tabl_name,orderId,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 key['amount'] = result;
                 callback(); 
              }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         }
         else {
            getOrderStatusName(req,res,9,data,orders);
           }
        });
   
} 

function getOrderStatusName(req,res,stat,data,orders) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting order status name");
    debuglog(orders);
   
     async.forEachOf(orders,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(key1);
        debuglog(statID);
          mastutils.masterStatusNames('orders_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                 callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           data.orderDetails = orders;
           processStatus(req,res,9,data);
        }
      });
 

} 
      
      
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




