function processRequest(req,res,data){
   if( typeof req.body.queueId == 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       getQueueData(req,res,9,data);
       debuglog("This must be a more generic workflow model later");
       debuglog("For now this handles only the four delivery options");
       debuglog("TODO after mvp move this to seperate model/folder");
}


function getQueueData(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var qid = req.body.queueId;
   var isError = 0;
   var errorCode = 0;
   var tblName = 'work_queue_master';
   data.queueId = qid;
   
    
      datautils.queueDetails(tblName,qid,function(err,result) {
      
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              processStatus(req,res,3,data);
              isError = 1;
              return;
              }
            else {
                data.productId    = result.product_id;
                data.tabId = result.tab_id;
                var statList = result.status_ids;
                var searchArr = result.additional_criterias;
                processQueue(req,res,9,data,statList,searchArr);
            }
          }
        });
} 

function processQueue(req,res,stat,data,statList,searchArr){

  data.searchArr = searchArr;
  var model = require('../Roles/getProductData');
  var roles = '';
  debuglog("Roles handling is not done for now");
      
  model.getQueue(req,res,stat,data,statList,roles);
  
     
}      

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;





