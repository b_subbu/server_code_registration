function getOrdersCount(orgId,bizType,tablName,sarr,callback){
 
  var totalRows =0;
  var resultArr = [];
  var resultArray = [];
  var finalArray = [];
  var data = {};
  
  //note: we are taking count for the list of all org ids here 
  //but may need to include biztype if one org can have multiple biz types  
   
  var promise = db.get(tablName).count({$and: sarr});
   promise.on('complete',function(err,doc){
       callback(null,doc);
        });
   promise.on('error',function(err){
       console.log(err);
       callback(6,data);
     });
}

function getOrderHeaders(tblName,orgId,bizType,pgid,size,sarr,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
     //var pdfFilePath = app.get('fileuploadfolder')+'Invoices/'; //not sure why this is not working
     var pdfFilePath = '/upload_files/Invoices/';
     
           
   var promise = db.get(tblName).find({$and:sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
        var ordDate = moment(doc.create_date).format('DD-MM-YYYY');
        var tmpArray = {orderId: doc.order_id,amount:0,date:ordDate,status:doc.order_status};
            tmpArray.pdfUrl = global.nodeURL+pdfFilePath+doc.customer_id+"_"+doc.order_id+"_"+ordDate+".pdf";
        resultArray.push(tmpArray);
        debuglog("The invoice id and order id will be same");
        debuglog("But invoice number inside an invoice can change dynamically");
        debuglog(tmpArray);
        //amount will come only in getPayment details so keep it zero until 
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.orders = [];
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.orders = resultArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function getOrderHeader(ordData,callback){
  var totalRows =0;
  var data = {};
  var tmpArray = {};
  var moment = require('moment');
 
  var tblName = ordData.orgId+"_order_header";
  var orderId = ordData.orderId;   
  
  var promise = db.get(tblName).find({order_id:orderId});
   promise.each(function(doc){
        totalRows++;
       tmpArray = {orderId: doc.order_id,amount:0,date:moment(doc.create_date).format('DD-MM-YYYY'),status:doc.order_status};
        //amount will come only in getPayment details so keep it zero until 
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.orders = tmpArray;
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.orders = tmpArray;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 


function getOrderPayment(tblName,orderId,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
   var amount = 0;
  
   var promise = db.get(tblName).find({order_id:orderId});
   promise.each(function(doc){
        amount = doc.amount;
       }); 
   promise.on('complete',function(err,doc){
         callback(null,amount);
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
} 

function checkOrderId(ordData,callback){
   var totalRows =0;
   var data = {};
  
   var tblName = ordData.orgId+"_order_header";
   var orderId = ordData.orderId;
   
   var customerId = ""; 
     
   var promise = db.get(tblName).find({order_id:orderId});
   promise.each(function(doc){
        totalRows++;
        customerId = doc.customer_id;
        deliveryOption = doc.delivery_option;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       data.customerId = "";
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.customerId = customerId;
         data.deliveryOption = deliveryOption;
         callback(null,data);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function updatePackageId(ordData,callback){
   var tblName = ordData.orgId+"_order_ship";
   var orderId = ordData.orderId;
   
   
   //update shipping status to Packed
   //and order status to Ready2Ship in next call 
   debuglog("These are all just dummy implementations");
   debuglog("Will need to do more detailed after shipping gateway integration"); 
  var promise = db.get(tblName).update(
  {order_id: ordData.orderId},
  {$set:{ customer_id: ordData.customerId,
          ship_status: 'Packed',
          package_id: ordData.packageId,
          package_date: new Date,
          update_time: new Date}},
       {upsert:true}
  );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
} 

function updateWaybillId(ordData,callback){
   var tblName = ordData.orgId+"_order_ship";
   var orderId = ordData.orderId;
   
   
   var promise = db.get(tblName).update(
  {order_id: ordData.orderId},
  {$set:{ customer_id: ordData.customerId,
          ship_status: 'Shipped',
          waybill_id: ordData.waybillId,
          tracking_id: ordData.trackingId,
          shipping_date: new Date,
          update_time: new Date}},
       {upsert:true}
  );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
} 

function updateDeliveryDate(ordData,callback){
   var tblName = ordData.orgId+"_order_ship";
   var orderId = ordData.orderId;
   
   //TODO update all other delivery details after MVP
   var promise = db.get(tblName).update(
  {order_id: ordData.orderId},
  {$set:{ customer_id: ordData.customerId,
          ship_status: 'Delivered',
          delivery_date: new Date,
          update_time: new Date}},
       {upsert:true}
  );  
  promise.on('success',function(err,doc){
        callback(null,null);
       });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,null);
     });
} 

//TODO move this to somewhere in menu item utils
function getOrderItemDetails(orgId,itemCode,itemDetailCode,callback){
   var totalRows =0;
   var data = {};
  
   var tblName = orgId+"_menuitem_master";
   
   var itemName = ""; 
     
   var promise = db.get(tblName).find({itemCode:itemCode});
   promise.each(function(doc){
        totalRows++;
        itemName = doc.itemName;
		itemPrice = doc.itemPrice;
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       data.itemName = "";
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.itemName = itemName;
		 data.itemPrice = itemPrice;
         getOrderSelectionCriteriaFields(orgId,itemCode,itemDetailCode,data,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getOrderSelectionCriteriaFields(orgId,itemCode,itemDetailCode,data,callback){
   var totalRows =0;
   var fieldsArr = [];
  
   var tblName = "org_itemdetail_map";
   
   
   //Get all visible and selection criteria columns
   //Discard hidden and info fields  
   var promise = db.get(tblName).find({
    $and:[{itemCode:itemCode},{field_visibility:2},{field_purpose:1}]
    });
   promise.each(function(doc){
        totalRows++;
        fieldsArr.push(doc.field_name);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 1; //no data case
       data.selectionData = {};
       callback(null,data);
       }
      else {
         getOrderSelectionData(orgId,itemCode,itemDetailCode,data,fieldsArr,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

function getOrderSelectionData(orgId,itemCode,itemDetailCode,data,fieldsArr,callback){
   var totalRows =0;
   var async = require('async');
 
  var tblName = orgId+"_menuitem_detail";
  
    var promise = db.get(tblName).find({
         $and:[{itemCode:itemCode},{itemDetailCode:itemDetailCode}]
         });
  promise.on('success',function(doc){
   filterOrderSelectionData(data,fieldsArr,doc,callback);
  });
  promise.on('error',function(err){
    console.log(err);
    callback();//system error
  });
 
} 

function filterOrderSelectionData(data,fieldsArr,fieldsValue,callback){
   var totalRows =0;
   var async = require('async');
   var selectionArray = {};
 
  async.forEachOf(fieldsArr,function(key1,value1,callback1){
    var fieldId = key1;
    if(fieldId == 'itemDetailCode'  || fieldId == 'isDeleted')
       callback1();
    else{
        var selectionValue = fieldsValue[0][fieldId];
        selectionArray[fieldId] = selectionValue;
        callback1();
       }
  
  },
  function(err){
      if(err)
        callback(6,null);
      else {
           data.errorCode = 0;
           data.selectionData = selectionArray;
           callback(null,data);
          }
     });
 
 
} 

function getQueueDetails(tblName,queueId,callback){
   var totalRows =0;
   var resultArr = {};
  
  var promise = db.get(tblName).find({workqueue_id:queueId});
  promise.each(function(doc){
        totalRows++;
        resultArr = doc;
        debuglog(doc);
        debuglog(doc[0]);
        debuglog(resultArr);
       }); 
  promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       resultArr.errorCode = 1; //no data case
       callback(null,resultArr);
       }
      else{
            
            resultArr.errorCode = 0;
            callback(null,resultArr);
         } 
     });
   promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
} 

exports.orderCount = getOrdersCount;
exports.orderHeader = getOrderHeaders;
exports.orderPayment = getOrderPayment;
exports.checkOrder   = checkOrderId;
exports.updatePackage = updatePackageId;
exports.updateWaybill = updateWaybillId;
exports.updateDelivery = updateDeliveryDate;
exports.moreOrderDetails = getOrderItemDetails;
exports.orderData = getOrderHeader;
exports.queueDetails = getQueueDetails;


