function getOrderSearch(req,res,oid,pid,tid,pgid,size,data) {

 var searchInp = data.searchParams;

 var isAmountSearch = false;
 var isOtherSearch  = false; 
 statusName = '';
 
 var minAmt = 0;
 var maxAmt = 0;
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
 
 console.log(userArray);
 console.log(searchArr);
 console.log("$$$$$$$$$$");
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {order_status: key};
         searchArr.push(tmpArray);
         isOtherSearch = true;
         break;
       case 'orderId':
        var tmpArray = {order_id: key}; 
           searchArr.push(tmpArray);
           isOtherSearch = true;
         break; 
      case 'date':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var minDaysArr = minDateArr[2].split("T"); //sometimes UI sends with timestamp
        var maxDaysArr = maxDateArr[2].split("T");
        var cmpDate = new Date(minDaysArr[0],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDaysArr[0],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {create_date: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {create_date: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
        isOtherSearch = true;
       break; 
       
      case 'amount':
       minAmt = parseFloat(key[0]);
       maxAmt = parseFloat(key[1]);
       isAmountSearch = true;
        
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           if(isOtherSearch == true) 
            getOtherSearch(req,res,oid,pid,tid,pgid,size,searchArr,data,isAmountSearch,minAmt,maxAmt);
           else
            getAmountSearch(req,res,oid,pid,tid,pgid,size,searchArr,data,"",false,minAmt,maxAmt);
         //It can be a combination of amount+other search
         //or only amount or only other search
         //if only amount then recheck against order_header table to get order status
         //if combo then no need since first order_header is search 
          }
     });
}

function getOtherSearch(req,res,oid,pid,tid,pgid,size,searchArr,data,isAmountSearch,minAmt,maxAmt){
   var totalRows =0;
   var resultArray = [] ;
   var moment = require('moment');
 
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      
    var tablName = oid+'_order_header';
   var promise = db.get(tablName).find({$and:searchArr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
         totalRows++;
        var tmpArray = {orderId: doc.order_id,amount:0,date:moment(doc.create_date).format('DD-MM-YYYY'),status:doc.order_status};
        
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.recordCount = totalRows;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       
       processStatus(req,res,9,data);
     }
      else {
       data.recordCount = totalRows;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       if(isAmountSearch == true)
         getAmountSearch(req,res,oid,pid,tid,pgid,size,searchArr,data,resultArray,true,minAmt,maxAmt);
       else
         getOrderStatName(req,res,9,data,resultArray);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);
    
	});
} 

function getAmountSearch(req,res,oid,pid,tid,pgid,size,searchArr,data,resultArr,isOtherSearch,minAmt,maxAmt){
   var totalRows =0;
   var resultArray = [] ;
   var moment = require('moment');
   var async = require('async');
 
   
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
          var tablName = oid+'_order_payment';
  
      console.log(searchArr);
      console.log("*******");
   if(isOtherSearch == true){
       async.forEachOf(resultArr,function(key,value,callback){
       var promise = db.get(tablName).find({order_id:key.orderId});
        promise.each(function(doc){
        var amount = doc.amount;
        if(amount <= maxAmt && amount >= minAmt)
           key.amount = doc.amount; 
        else
          delete resultArr[value]; //otherwise it does not fall in range 
        }); 
   promise.on('complete',function(err,doc){
       callback();
      });
      promise.on('error',function(err){
       console.log(err);
       callback();
   	});
   },
       function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    getOrderStatName(req,res,9,data,resultArr);
			 }
      });
    }   
   else {
   
   var promise = db.get(tablName).find({$and:[{amount: {$lte: maxAmt}},
                                              {amount: {$gte: minAmt}}]},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
         totalRows++;
        var tmpArray = {orderId: doc.order_id,amount:doc.amount,date:moment(doc.create_date).format('DD-MM-YYYY'),status:81};
        //TODO get actual status from order_header table
        resultArray.push(tmpArray);
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.recordCount = totalRows;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       
       processStatus(req,res,9,data);
     }
      else {
       data.recordCount = totalRows;
       data.productId = pid;
       data.tabId = tid;
       data.pageId = pgid;
       if(isOtherSearch == false)
         getStatusFromHeader(req,res,oid,pid,tid,pgid,size,resultArray,data);
       else
         getOrderStatName(req,res,9,data,resultArray);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       processStatus(req,res,6,data);
    
	});
  }
} 


function getStatusFromHeader(req,res,oid,pid,tid,pgid,size,resultArr,data){
   var totalRows =0;
   var resultArray = [] ;
   var moment = require('moment');
   var async = require('async');
 
   
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
          var tablName = oid+'_order_header';
  
     async.forEachOf(resultArr,function(key,value,callback){
       var promise = db.get(tablName).find({order_id:key.orderId});
        promise.each(function(doc){
           key.status= doc.order_status; 
        }); 
   promise.on('complete',function(err,doc){
       callback();
      });
      promise.on('error',function(err){
       console.log(err);
       callback();
   	});
   },
       function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    getOrderStatName(req,res,9,data,resultArr);
			 }
      });
    
} 

function getOrderStatName(req,res,stat,data,mastData) {
   var async = require('async');
   var type = require('type-detect');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
   var mastutils = require('../Registration/masterUtils');
  
    async.forEachOf(mastData,function(key1,value1,callback1){
      
        var masterData = mastData[value1]; //because mastdata is double array
        async.forEachOf(masterData,function(key2,value2,callback2){
             if(value2 == 'status'){
              var statID=key2;
          mastutils.masterStatusNames('orders_status_master',statID,function(err,result) {
           if(err){ isError =1; callback2();}
         else {
                 //key2 = result;
                 mastData[value1]['status'] = result.statusName;
                 callback2(); 
              }
           }); 
          }
          else
              callback2();
        },
       function(err){
         if(err){
             isError =1;         
			 callback1();
			  }
         else {
            callback1(); 
           }
        });
      },
    function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    if(isError == 1)
			 processStatus (req,res,6,data);
            else {				
   	              populateSearchData(req,res,9,data,mastData);
			          }
         }
      });
 

}

function populateSearchData(req,res,stat,data,mastData){

 var async = require('async');
   var statutils = require('./statusUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var isError = 0;
   var previousStat = 0;
   var previousResult = '';
     
      async.forEachOf(mastData,function(key1,value1,callback1){
      if(key1)
        itemMasters.push(key1);
      
        callback1();
        },
      function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
		    if(isError == 1)
			 processStatus (req,res,6,data);
            else
			{				
       data.searchData = itemMasters;
		   processStatus(req,res,9,data,mastData);
			}
        }
      });
 


}
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Search/searchData');
  
    controller.process_status(req,res,stat,data);
}

exports.searchOrder = getOrderSearch;


 

