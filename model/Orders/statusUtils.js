function getOrderStatus(pid,tid,statlist,statTableName,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key];    
   var promise = db.get('product_tab_status_action').find({$and:[{product_id:pid},{tab_id:tid},{stat_id:statid}]});
   promise.each(function(doc){
   var tmpArray = {id:doc.action_id};
      resultArray.push(tmpArray);
        }); 
   promise.on('complete',function(err,doc){
      var tmpArray = {id: statid, name:"ABC"}; //name will be populated below
       tmpArray.availableActions = resultArray;
       finalArray.push(tmpArray);
     
       resultArray = [];
     
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           getOrderStatusName(finalArray,statTableName,callback);
           }
     });
} 


function getOrderStatusName(statlist,tblName,callback){
   var totalRows =0;
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');

    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key].id;    
   mastutils.masterStatusNames(tblName,statid,function(err,result) {
           if(err) callback1();
         else {
                statlist[key].name = result.statusName;
                callback1(); 
              }
        });
      },
      function(err){
      if(err)
             callback(1,null);
      else
          {
            getOrderNextActionName(statlist,callback);
           }
     });
} 


function getOrderNextActionName(statlist,callback){
   var totalRows =0;
    var async = require('async');
    var data = {};
    var tmpData = [];
    var resultArray = [];
    var finalArray = [];
  
  async.forEachOf(statlist,function(value,key,callback1){
   var statid = statlist[key].id;
   var actionlist = statlist[key].availableActions;    
  async.forEachOf(actionlist,function(value1,key1,callback2){
   var actionid = actionlist[key1].id;    
   
   var promise = db.get('item_master').find({item_id:actionid});
   promise.each(function(doc){
        statlist[key].availableActions[key1].name = doc.item_name;
        statlist[key].availableActions[key1].title = doc.item_title;
        statlist[key].availableActions[key1].sequence= doc.item_seq;
        }); 
   promise.on('complete',function(err,doc){
      callback2();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
     function(err){
      if(err)
             callback(1,null);
      else
          {
           callback1();
           
           }
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           callback(null,statlist);
            }
     });
} 

function changeOrderStatus(tablName,orgId,bizType,operatorId,orderId,stat,reason,callback){

 var isError = 0;
 var promise = db.get(tablName).update(
                                              {order_id: orderId}, 
                                               {$set: {
                                                order_status: stat,
                                                update_time: new Date()
                                                }
                                               });
            promise.on('success',function(doc){
                 //continue; //not possible to use inside promise
            });
            promise.on('error',function(err){
                  console.log(err);
                  isError = 1;
                  //break;
            });
     
     if(isError == 0)
       createStatusChngLog(orgId,bizType,operatorId,orderId,stat,reason,callback);
     else
       callback(6,null); //system Error  
 } 
 
 function createStatusChngLog(orgId,bizType,operatorId,orderId,stat,reason,callback){
 
  var promise = db.get('orders_status').insert({
    org_id: orgId,
    business_type: bizType,
    operatorId: operatorId,
    order_id: orderId,
    status_id: stat,
    reason: reason,
    create_date: new Date()
  });
  promise.on('success',function(doc){
     callback(null,null); //no error
  });
  promise.on('error',function(err){
    console.log(err);
    callback(6,null);//system error
  });
 
 
 
 }
 
exports.statList = getOrderStatus;
exports.changeStatus = changeOrderStatus;


