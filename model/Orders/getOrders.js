//NOTE: this will be called from getProductData
//NOTE: this will be called from getWorkQueueItems not from getProductData anymore

function getOrdersData(req,res,stat,data,roles,statList) {
   var async = require('async');
   var statutils = require('./statusUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   
   var search = data.searchArr;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
  
    var tabl_name = oid+'_order_header';
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   data.pageId = pageId;
   
     async.parallel({
         statusData: function(callback) {statutils.statList(pid,tid,statList,'orders_status_master',callback); },
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
            if(isError == 0) {
                 if(pageId == 1 || typeof req.body.searchParams != 'undefined') {
                    data.statusList = results.statusData;
                    getFilterOptions(req,res,stat,data,statList,search,tabl_name);
                  }
                  else 
                   {
                      var searchArr = [];
                      var tmpArray = {status_id: {"$in":statList}};
                      searchArr.push(tmpArray);
                      searchArr.push(search[0]);
                      debuglog(searchArr);
                    getOrdersCount(req,res,stat,data,searchArr,tabl_name);
                   }
                 }
          }); 
} 

//note this is not a generic function for filteroptions
//if really needed do a if isFilter = true then
//get master details from a table for that column and then populate 
//them probably in getproduct data generic functions
//anyhow master table for each column can be different 
//so not really generic we can say
//anyhow only few columns in every tab should be still manually manageable

function getFilterOptions(req,res,stat,data,statList,search,tabl_name) {
   var async = require('async');
   var filterutils = require('../Menu/filterUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
  
    var uid = data.userId;
    var oid = data.orgId;
    var pid = data.productId;
    var tid = data.tabId;
    var pageid = data.pageId;
      
 
    var filterOptions = {};
    
    var headerFields = data.productHeader;
     async.forEachOf(headerFields,function(key,value,callback){
       if (key.filterable == true){
           var key_name = key.name;
        filterutils.filterOptions(key_name,statList,pid,tid,'orders_status_master',function(err,results){
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              callback();
             }
          else {
          
             filterOptions[key_name] = results;
             callback();
          
          }
          }); 
        }
     else
      callback();
      },
       function(err){
      if(err || isError == 1)
         processStatus(req,res,6,data);//system error 
      else
         {
          data.filterOptions = filterOptions;
         debuglog("if searchParams is passed then call fn to populate searchArr");
         debuglog("else directly call alerts count fn without search array");
         debuglog("Search is the criteria as obtained from work_queue_master");
         debuglog("It is pushed into search array so that it is included in search");
         debuglog("As of now directly first element is pushed");
         debuglog("TODO parse the array and push elements one by one if more than one");
         debuglog(req.body.searchParams);
         debuglog(typeof req.body.searchParams);
         if(typeof req.body.searchParams == 'undefined'){ 
           debuglog("This is without searchParams");
           debuglog("so we will make statList array into json object");
           debuglog("To be compatible with searchdata input");
           debuglog("To Do change order_status to status_id to standardize")
           debuglog(statList);
           var searchArr = [];
           var userArray = {org_id: data.orgId };
           searchArr.push(userArray);
           searchArr.push(search[0]);
           var tmpArray = {order_status: {"$in":statList}};
           searchArr.push(tmpArray);
           debuglog(searchArr);
          getOrdersCount(req,res,stat,data,searchArr,tabl_name);
           }
         else
         getOrderSearch(req,res,stat,data,statList,search,tabl_name);
         }
      });
} 

function getOrderSearch(req,res,stat,data,statList,search,tabl_name) {

 var searchInp = req.body.searchParams;

 statusName = '';
 
 var minAmt = 0;
 var maxAmt = 0;
 
 var searchArr = [];
 var userArray = {org_id: data.orgId };
 searchArr.push(userArray);
 searchArr.push(search[0]);
 
 debuglog(searchArr);
 debuglog("Starting Search");
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'status':
         var tmpArray = {order_status: key};
         searchArr.push(tmpArray);
         break;
       case 'orderId':
        var tmpArray = {order_id: key}; 
           searchArr.push(tmpArray);
          break; 
      case 'date':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
        var minDaysArr = minDateArr[2].split("T"); //sometimes UI sends with timestamp
        var maxDaysArr = maxDateArr[2].split("T");
        var cmpDate = new Date(minDaysArr[0],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDaysArr[0],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {create_date: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {create_date: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
        isOtherSearch = true;
       break; 
       
      case 'amount':
       minAmt = parseFloat(key[0]);
       maxAmt = parseFloat(key[1]);
       var tmpArray = {numberSent: {$gte:minAmt}};
       searchArr.push(tmpArray);
       var tmpArray = {numberSent: {$lte: maxAmt}};
       searchArr.push(tmpArray);
       debuglog("Amount is yet to implement");
       debuglog("since it is in order_payment table and we have to combine the two");
       debuglog("Need to implement this when rewriting Order flow with new fields");
      }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
            getOrdersCount(req,res,stat,data,searchArr,tabl_name);
          }
     });
}

function getOrdersCount(req,res,stat,data,statList,tabl_name){
    
    var async = require('async');
    var utils = require('./dataUtils');
    var isError = 0;
    var totalCount = 0;
    var bizList = [];
    
   var orgId = data.orgId;
   var bizType = data.businessType;
   
    if(typeof req.body.pageId == 'undefined')
        pageId = 1;
    else
       pageId = req.body.pageId;
   
   var tid = data.tabId;
         
   utils.orderCount(orgId,bizType,tabl_name,statList,function(err,results) { 
           if(err) {
             console.log(err);
             processStatus(req,res,6,data);//system error
             isError = 1;
             return;
            }
          if(isError == 0  ) {
            if(results == 0){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data); 
              //no Orders for the business nothing to do further
              }
             else {
              data.pageId = pageId;
              data.recordCount = results;
              getOrderHeaderData(req,res,9,data,statList,tabl_name); 
             }
            }
         });
}

function getOrderHeaderData(req,res,stat,data,statList,headerTable) {
   var async = require('async');
   var utils = require('./dataUtils');
   var type = require('type-detect');
   var isError = 0;
   var errorCode = 0;
   var size = 20;
   var orders = [];
   
   
   var orgId = data.orgId;
   var bizType = data.businessType;
   var pageId = data.pageId;
  
   
      utils.orderHeader(headerTable,orgId,bizType,pageId,size,statList,function(err,result) {
         if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1)) {
              processStatus(req,res,result.errorCode,data);
              isError = 1;
              return;
              }
            else if (isError == 0 && result.errorCode == 3){
              data.pageId = pageId;
              data.recordCount = 0;
              data.productData = [];
              processStatus(req,res,9,data); //no Orders for the business nothing to do further
              isError = 1;
             //will be handled at count itself, just adding here
            }
            else {
            orders = result.orders; 
            getOrderPaymentData(req,res,9,data,orders);
            }
          }
        });
} 


function getOrderPaymentData(req,res,stat,data,orders) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var masterData = [];
  var payment_tabl_name = data.orgId+'_order_payment';
 
   async.forEachOf(orders,function(key,value,callback){
   var orderId = key.orderId;
   
   
    datautils.orderPayment(payment_tabl_name,orderId,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
                 key['amount'] = result;
                 callback(); 
              }
        });
      },
       function(err){
         if(err){
             processStatus (req,res,6,data);
              return;         
              }
         else {
               getOrderStatusName(req,res,9,data,orders);
              }
        });
   
} 

function getOrderStatusName(req,res,stat,data,orders) {
   var async = require('async');
   var mastutils = require('../Registration/masterUtils');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
     
      async.forEachOf(orders,function(key1,value1,callback1){
        var statID=key1.status;
           mastutils.masterStatusNames('orders_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
              processStatus (req,res,6,data);
              return;        
            }
      else {
            data.productData = orders;
            processStatus(req,res,9,data);
           }
      });
 

} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getProductData = getOrdersData;
exports.getData = getOrdersData;






