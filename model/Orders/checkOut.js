function processRequest(req,res,data){
   if(typeof req.body.orderIds === 'undefined' )
       processStatus(req,res,6,data); //system error
   else 
       checkOrder(req,res,9,data);
}


function checkOrder(req,res,stat,data) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var isError = 0;
   
    data.orderIds = req.body.orderIds;
     
     async.forEachOf(data.orderIds,function(key,value,callback){ 
      var ordData = {};
          ordData.orderId = key;
          ordData.orgId = data.orgId;
          debuglog("Checking for");
          debuglog(ordData);
         datautils.checkOrder(ordData,function(err,result) {
           if(err){
              isError = 6;
              callback();
             }
         else {
            if(isError == 0 && result.errorCode == 1 ) {
               isError = 7;
               callback();
              }
            else {
               callback();
               }
             }
        });
      },
      function(err){
      if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
              debuglog("All passed orderIds are fine so change status now");
              changeOrderStatus(req,res,9,data);
             }
          }
     });
} 

function changeOrderStatus(req,res,stat,data) {
   var async = require('async');
   var statutils = require('./statusUtils');
    var isError = 0;
  
    var tblName = data.orgId+'_order_header';
     
     async.forEachOf(data.orderIds,function(key,value,callback){ 
          orderId = key;
          debuglog("changing status for");
          debuglog(orderId);
        statutils.changeStatus(tblName,data.orgId,data.businessType,data.userId,orderId,90,'Checked Out',function(err,result) {
           if(err) {
               isError = 6;
               callback();
             }
          else {
               callback();
            }
        });
      },
     function(err){
      if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
              debuglog("All orderIds status changed now get details");
              getOrdersData(req,res,9,data);
             }
          }
     });
    
} 

//TODO move to utils and make status list dynamic
function getOrdersData(req,res,stat,data) {
   var async = require('async');
   var utils = require('./dataUtils');
   var isError = 0;
   var orders = [];
   
 
    async.forEachOf(data.orderIds,function(key,value,callback){ 
      var ordData = {};
          ordData.orderId = key;
          ordData.orgId = data.orgId;
          debuglog("getting details for");
          debuglog(ordData);
       utils.orderData(ordData,function(err,result) {
         if(err) {
               isError = 6;
               callback();
             }
         else {
             if(isError == 0 && (result.errorCode != 0 && result.errorCode != 1 && result.errorCode != 3)) {
              isError = result.errorCode;
              callback();
              }
            else {
            orders.push(result.orders);
            callback(); 
            //getOrderPaymentData(req,res,9,data,orders);
            }
          }
        });
      },
    function(err){
      if(err)
               processStatus(req,res,6,data);
         else{
           if(isError != 0)
             processStatus(req,res,isError,data);
           else{
              debuglog("now get order payment details for");
              debuglog(orders);
              getOrderPaymentData(req,res,9,data,orders);
            }
          }
     });
} 


function getOrderPaymentData(req,res,stat,data,orders) {
   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var masterData = [];
  var payment_tabl_name = data.orgId+'_order_payment';
 
   async.forEachOf(orders,function(key,value,callback){
   var orderId = key.orderId;
   
   
    datautils.orderPayment(payment_tabl_name,orderId,function(err,result) {
           if(err) processStatus(req,res,6,data);
         else {
                 key['amount'] = result;
                 callback(); 
              }
        });
      },
       function(err){
         if(err){
              processStatus (req,res,6,data);
              return;         
             }
         else {
              getOrderStatusName(req,res,9,data,orders);
           }
        });
   
} 

function getOrderStatusName(req,res,stat,data,orders) {
   var async = require('async');
   var itemMasters = [];
   var subitemMasters = [];
   var itemMaster = {};
   var mastutils = require('../Registration/masterUtils');
    debuglog("start getting order status name");
    debuglog(orders);
   
     async.forEachOf(orders,function(key1,value1,callback1){
        var statID=key1.status;
        debuglog(key1);
        debuglog(statID);
          mastutils.masterStatusNames('orders_status_master',statID,function(err,result) {
           if(err) callback1();
         else {
                key1.status = result.statusName;
                 callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           data.orderDetails = orders;
           processStatus(req,res,9,data);
        }
      });
 

} 
      
      
function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;

