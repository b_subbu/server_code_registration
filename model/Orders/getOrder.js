function processRequest(req,res,data){
    if( typeof req.body.orderId === 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
       debuglog("checkMasterStatus is there for getting baseCurrency");
       debuglog("TODO change later");
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var orderId = req.body.orderId;
    //var oid = req.body.orgId; ---For future user
    var async = require('async');
    var roleutils = require('../Roles/roleutils');
    var isError = 0;
    //data.businessId = uid;
    data.orderId = orderId;
    
     async.parallel({
         usrStat: function(callback){roleutils.usrStatus(uid,callback); },
       
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
         
            if(isError == 0) {
              //data.orgId = results.usrStat.orgId;
              //data.businessType = results.usrStat.bizType;
              data.baseCurrency = results.usrStat.baseCurrency;
              getOrderData(req,res,9,data);
            }
          }); 
}


function getOrderData(req,res,stat,data) {
   var async = require('async');
   var datautils = require('../Checkout/dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
  
     var extend = require('util')._extend;
     var ordData = extend({},data);
     
  
      datautils.orderDetails(ordData,function(err,result) {
      
          if(err) {
               isError = 1;
               processStatus(req,res,6,data);
               return;
             }
         else {
             if(isError == 0 && result.errorCode == 1 ) {
              processStatus(req,res,3,data);
              isError = 1;
              return;
              }
            else {
                data.orderStatus    = result.orderStatus;
                data.deliveryOption = result.deliveryOption;
                data.payOption = result.paymentDetails.paymentMode;
                data.billingDetails = result.billingDetails;
                data.shippingDetails = result.shippingDetails;
                //data.orderDetails    = result.orderDetails;
                data.paymentDetails =  result.paymentDetails;
               getMoreOrderDetails(req,res,9,data,result.orderDetails);
            }
          }
        });
} 

function getMoreOrderDetails(req,res,stat,data,ordDetails){

   var async = require('async');
   var datautils = require('./dataUtils');
   var type = require('type-detect');
   var bizData = [];
   var isError = 0;
   var errorCode = 0;
   var tabl_name = '';
   
     var extend = require('util')._extend;
     var orgId  = data.orgId;
     
      async.forEachOf(ordDetails,function(key1,value1,callback1){
        var itemCode=key1.itemCode;
        var itemDetailCode = key1.itemDetailCode;
        
            datautils.moreOrderDetails(orgId,itemCode,itemDetailCode,function(err,result) {
            if(err) callback1();
         else {
                key1.itemName = result.itemName;
                key1.selectionData = result.selectionData;
                 callback1(); 
              }
           }); 
        },
     function(err){
       if(err){
             processStatus (req,res,6,data);
              return;         }
      else {
           data.orderDetails = ordDetails;
           processStatus(req,res,9,data);
        }
      }); 
}      

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'businesswebController');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;




