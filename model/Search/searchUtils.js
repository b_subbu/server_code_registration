<<<<<<< HEAD
function getAlertContentCount(oid,tid,pgid,size,sarr,callback){
   var totCount = 0;
   var data = {};
  
    var promise = db.get('alert_master').count({$and:sarr});
    promise.on('complete',function(err,doc){
      totCount = doc;
      if(totCount == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = 0;
       callback(null,data);
       }
      else {
         getAlertContentData(oid,tid,pgid,size,sarr,totCount,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

//if this file grows too big then seperate out one file per Product
//or product+tab
function getAlertContentData(oid,tid,pgid,size,sarr,totalCount,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
   var promise = db.get('alert_master').find({$and:sarr},{sort: {update_time: -1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
        if(tid == '501')
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_date).format('DD-MM-YYYY'),alertType:doc.alert_type_id,status:doc.alert_status_id,target:doc.content_target,views:"0"}
        if(tid == '502')
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_date).format('DD-MM-YYYY'),alertType:doc.alert_type_id,status:doc.alert_status_id}
        
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
        //date probably should check with alert_status table to get date of the given status
        //Views will come only after we complete APP so keep it zero until 
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = totalCount;
       data.resultArray = resultArray;
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.resultCount = totalCount;
         getAlertTypeData(data,resultArray,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

//TODO: inside of getting each element of list
//get only if the id changes
//because of async doing with flags is not working correct
//need to figure out a work around without iterating through array
//because iteration of array and mongo call may be more or less same time
function getAlertTypeData(data,resultArray,callback){
    var totalRows =0;
    var async = require('async');
    var mastutils = require('../Registration/masterUtils');
  
  async.forEachOf(resultArray,function(value,key,callback1){
   var alert_typeid = resultArray[key].alertType;
    mastutils.masterNames('alert_type_master',alert_typeid,function(err,result){
      if(err) callback(6,null);
      else
        resultArray[key].alertType = result.typeName;
        callback1();
    
        });
       },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           data.resultArray = resultArray;
           getAlertStatData(data,resultArray,callback);
          }
     });
     
} 


function getAlertStatData(data,resultArray,callback){
   var totalRows =0;
    var async = require('async');
  
  async.forEachOf(resultArray,function(value,key,callback1){
   var statid = resultArray[key].status;
      
   var promise = db.get('alert_status_master').find({alert_status_id: statid});
   promise.each(function(doc){
        resultArray[key].status = doc.alert_status;
 
        }); 
   promise.on('complete',function(err,doc){
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           data.resultArray = resultArray;
           callback(null,data);
           }
     });
} 



function getAlertStatusId(statName,callback){
   var totalRows = 0;
   var alertId = "";
   var data={};
  
    var promise = db.get('alert_status_master').find({alert_status: {$regex:statName, $options:'i'}});
   promise.each(function(doc){
        totalRows++;
        alertId = doc.alert_status_id;
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0 || totalRows > 1) 
         {data.errorCode = 6;
          data.resultCount = 0;
          callback(null,data); //just ignore this condition for search 
          }
       else{  
          data.errorCode = 0;
          data.resultCount = 1;
          data.statType = alertId;
          callback(null,data);
         }
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

exports.alertData =  getAlertContentCount;
exports.alertStatId = getAlertStatusId;
=======
function getAlertContentCount(oid,tid,pgid,size,sarr,callback){
   var totCount = 0;
   var data = {};
  
    var promise = db.get('alert_master').count({$and:sarr});
    promise.on('complete',function(err,doc){
      totCount = doc;
      if(totCount == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = 0;
       callback(null,data);
       }
      else {
         getAlertContentData(oid,tid,pgid,size,sarr,totCount,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

//if this file grows too big then seperate out one file per Product
//or product+tab
function getAlertContentData(oid,tid,pgid,size,sarr,totalCount,callback){
   var totalRows =0;
   var resultArray = [] ;
   var data = {};
  
      var page = parseInt(pgid);
      var skip = page > 0 ? ((page - 1) * size) : 0;
      var moment = require('moment');
     
   var promise = db.get('alert_master').find({$and:sarr},{sort: {alert_id:1}, limit: size, skip: skip});
   promise.each(function(doc){
        totalRows++;
        if(tid == '501')
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_time).format('L'),alertType:doc.alert_type_id,status:doc.alert_status_id,target:doc.content_target,views:"0"}
        if(tid == '502')
        var tmpArray = {id: doc.alert_id,conTitle:doc.title,date:moment(doc.create_time).format('L'),alertType:doc.alert_type_id,status:doc.alert_status_id}
        
        resultArray.push(tmpArray);
        //note the keys must match to name as defined in Tab header for the given product+tab
        //date probably should check with alert_status table to get date of the given status
        //Views will come only after we complete APP so keep it zero until 
       }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0) {
       data.errorCode = 0; //no data case
       data.resultCount = totalCount;
       data.resultArray = resultArray;
       callback(null,data);
       }
      else {
         data.errorCode = 0; //no error
         data.resultCount = totalCount;
         getAlertTypeData(data,resultArray,callback);
         } 
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
} 

//TODO: inside of getting each element of list
//get only if the id changes
//because of async doing with flags is not working correct
//need to figure out a work around without iterating through array
//because iteration of array and mongo call may be more or less same time
function getAlertTypeData(data,resultArray,callback){
   var totalRows =0;
    var async = require('async');
  
  async.forEachOf(resultArray,function(value,key,callback1){
  var alert_typeid = resultArray[key].alertType;
   var promise = db.get('alert_type_master').find({alert_type_id: alert_typeid});
   promise.each(function(doc){
        resultArray[key].alertType = doc.alert_type;
         }); 
   promise.on('complete',function(err,doc){
       callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           data.resultArray = resultArray;
           getAlertStatData(data,resultArray,callback);
          }
     });
     
} 


function getAlertStatData(data,resultArray,callback){
   var totalRows =0;
    var async = require('async');
  
  async.forEachOf(resultArray,function(value,key,callback1){
   var statid = resultArray[key].status;
      
   var promise = db.get('alert_status_master').find({alert_status_id: statid});
   promise.each(function(doc){
        resultArray[key].status = doc.alert_status;
 
        }); 
   promise.on('complete',function(err,doc){
      callback1();
       
     });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(6,data);
     });
     },
      function(err){
      if(err)
             callback(1,null);
      else
          {
           data.resultArray = resultArray;
           callback(null,data);
           }
     });
} 


function getAlertTypeId(typeName,callback){
   var totalRows = 0;
   var alertId = "";
   var data={};
  
    var promise = db.get('alert_type_master').find({alert_type: {$regex:typeName, $options:'i'}});
   promise.each(function(doc){
        totalRows++;
        alertId = doc.alert_type_id;
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0 || totalRows > 1) 
         {data.errorCode = 6;
          data.resultCount = 0;
          callback(null,data); //just ignore this condition for search 
          }
       else{  
          data.errorCode = 0;
          data.resultCount = 1;
          data.alertType = alertId;
          callback(null,data);
         }
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

function getAlertStatusId(statName,callback){
   var totalRows = 0;
   var alertId = "";
   var data={};
  
    var promise = db.get('alert_status_master').find({alert_status: {$regex:statName, $options:'i'}});
   promise.each(function(doc){
        totalRows++;
        alertId = doc.alert_status_id;
        }); 
   promise.on('complete',function(err,doc){
      if(totalRows == 0 || totalRows > 1) 
         {data.errorCode = 6;
          data.resultCount = 0;
          callback(null,data); //just ignore this condition for search 
          }
       else{  
          data.errorCode = 0;
          data.resultCount = 1;
          data.statType = alertId;
          callback(null,data);
         }
      });
   promise.on('error',function(err){
       console.log(err);
       data.errorCode = 6;//system error
       callback(1,data);
     });
} 

exports.alertData =  getAlertContentCount;
exports.alertTypeId = getAlertTypeId;
exports.alertStatId = getAlertStatusId;
>>>>>>> DEV/master
