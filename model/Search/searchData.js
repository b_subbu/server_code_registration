<<<<<<< HEAD
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined' || typeof req.body.productId == 'undefined' || typeof req.body.tabId == 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var async = require('async');
    var utils = require('../Roles/roleutils');
    var isError = 0;
    data.userId = uid;

    var pid = req.body.productId;
    var tid = req.body.tabId;

     data.productId = pid;
     data.tabId = tid;
     
     
     async.parallel({
         usrStat: function(callback){utils.usrStatus(uid,callback); },
         tabHead: function(callback){utils.tabHeader(pid,tid,callback); } 
        
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
            if(isError == 0 && results.tabHead.errorCode != 0) {
              processStatus (req,res,results.tabHead.errorCode,data);
              isError = 1;
              return;
             }             
          if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              getSearchData(req,res,9,data);
            }
          }); 
}



function getSearchData(req,res,stat,data) {

var oid = data.orgId;
var pid = data.productId;
var tid = data.tabId;

data.searchParams = req.body.searchParams;

 if( typeof req.body.pageId === 'undefined')
  var pgid = 1;
else
  var pgid = req.body.pageId;
  
  data.pageId = pgid;

  var size = 20; //setting common for all pages override individually if needed
  //note below table, field names are hard-coded
  // if number of tabs becomes large then map them to product_tab_header
  //and get tablename and field names from there
  //may need additional queries then
  
  switch(pid){
     case 140: //alert 
            var alertSearch = require('../Boarding/SearchUtils');
			//getAlertSearch(req,res,oid,pid,tid,pgid,size,data);
			alertSearch.search(req,res,oid,pid,tid,pgid,size,data);
            break;
            
     case 150:
           var itemSearch = require('../Menu/searchUtils');
           itemSearch.search(req,res,oid,pid,tid,pgid,size,data);
           break;
	
	 case 160:
	      var menuSearch = require('../Menu/searchUtils');
		  menuSearch.searchMenu(req,res,oid,pid,tid,pgid,size,data);
		  break;
		  
	 case 170:
	     var orderSearch = require('../Orders/searchUtils');
		 orderSearch.searchOrder(req,res,oid,pid,tid,pgid,size,data);
		 break;
	
	 case 180:
	    var inviteSearch = require('../Invitation/searchUtils');
		inviteSearch.searchInvitation(req,res,oid,pid,tid,pgid,size,data);
	    break;
    
     case 190:
	    var inviteSearch = require('../Groups/searchUtils');
		inviteSearch.searchGroup(req,res,oid,pid,tid,pgid,size,data);
	    break;
    	
     default:
        processStatus(req,res,6,data);
        break;
  }
} 

function getAlertSearch(req,res,oid,pid,tid,pgid,size,data) {

 var searchInp = data.searchParams;
 var isAlertTypeName = false;
 var isStatusName = false;
 alertTypeName = '';
 statusName = '';
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'conTitle':
       var tmpArray = {title: {$regex:key,$options:'i'}};
         searchArr.push(tmpArray);
         break; 
       case 'alertType':
         var tmpArray = {alert_type_id: key};
         searchArr.push(tmpArray);
         break; 
       case 'status':
        if(type(key) == 'string'){
          isStatusName = true;
          statusName = key;
         }
       else { var tmpArray = {alert_status_id: key};
         searchArr.push(tmpArray);
         }
         break;
       case 'date':
        var minDate = key[0];
        var maxDate = key[1];
        var minDateArr = minDate.split("-"); //note: date is in dd-mm-yyyy format
        var maxDateArr = maxDate.split("-");
         var cmpDate = new Date(minDateArr[2],parseInt(minDateArr[1])-1,minDateArr[0]); 
        var cmpDate2 = new Date(maxDateArr[2],parseInt(maxDateArr[1])-1,parseInt(maxDateArr[0])+1);
         //mm sent by UI is from 0, we need to check upto next day 00:00 hours to get this day
        //var tmpArray = {create_date: {$gte: cmpDate, $lte: cmpDate2}};
        var tmpArray = {create_date: {$gte:cmpDate}};
        searchArr.push(tmpArray);
        var tmpArray = {create_date: {$lte: cmpDate2}};
        searchArr.push(tmpArray);
       break;
       case 'target':
       var tmpArray = {content_target: key};
       searchArr.push(tmpArray);
       break;
       case 'views':
       //Views to do not yet implemented 
          break;
       default:
           processStatus(req,res,6,data);
            break;
          
     
     }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
          if(isStatusName == true)
             getStatusNameData(req,res,oid,pid,tid,pgid,size,searchArr,statusName,data);
           else
           getAlertSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data);
          }
     });
   
 
} 
function getStatusNameData(req,res,oid,pid,tid,pgid,size,searchArr,statName,data) {
   var async = require('async');
   var utils = require('./searchUtils');
   var type = require('type-detect');
  
    
    utils.alertStatId(statName,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                      if(result.resultCount > 0){
                         var tmpArray = {alert_status_id: result.statType};
                          searchArr.push(tmpArray);
                      }
                else {
                        var tmpArray = {alert_status_id: -007}; //just a random number so that result set is empty
                          searchArr.push(tmpArray);
                      
                      }
                    
                  }
               getAlertSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data);
                  }
            
        });
 
} 

function getAlertSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data) {
   var async = require('async');
   var utils = require('./searchUtils');
   var type = require('type-detect');
  
    
    utils.alertData(oid,tid,pgid,size,searchArr,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                       data.recordCount = result.resultCount;
                       if(result.resultCount > 0)
                        data.searchData = result.resultArray;
                    
                  }
                  processStatus(req,res,9,data);
                  }
            
        });
 
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Search/searchData');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
=======
function processRequest(req,res){
   var totalRows = 0;
   var user_stat = "";
   var data = {};
   if( typeof req.body.userId === 'undefined' || typeof req.body.productId == 'undefined' || typeof req.body.tabId == 'undefined')
       processStatus(req,res,6,data); //system error
   else 
       checkMasterStatus(req,res,data);
}

function checkMasterStatus(req,res,data){
    
    var uid = req.body.userId;
    var async = require('async');
    var utils = require('../Roles/roleutils');
    var isError = 0;
    data.userId = uid;

    var pid = req.body.productId;
    var tid = req.body.tabId;

     data.productId = pid;
     data.tabId = tid;
     
     
     async.parallel({
         usrStat: function(callback){utils.usrStatus(uid,callback); },
         tabHead: function(callback){utils.tabHeader(pid,tid,callback); } 
        
         },
         function(err,results) {
           if(err) {
              console.log(err);
              processStatus(req,res,6,data);//system error
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.errorCode != 0) {
              processStatus (req,res,results.usrStat.errorCode,data);
              isError = 1;
              return;
             }
           if(isError == 0 && results.usrStat.userStatus != 9) {
              processStatus (req,res,2,data);
              isError = 1;
              return;
             }
            if(isError == 0 && results.tabHead.errorCode != 0) {
              processStatus (req,res,results.tabHead.errorCode,data);
              isError = 1;
              return;
             }             
          if(isError == 0) {
              data.orgId = results.usrStat.orgId;
              getSearchData(req,res,9,data);
            }
          }); 
}



function getSearchData(req,res,stat,data) {

var oid = data.orgId;
var pid = data.productId;
var tid = data.tabId;

data.searchParams = req.body.searchParams;

 if( typeof req.body.pageId === 'undefined')
  var pgid = 1;
else
  var pgid = req.body.pageId;
  
  data.pageId = pgid;

  var size = 20; //setting common for all pages override individually if needed
  //note below table, field names are hard-coded
  // if number of tabs becomes large then map them to product_tab_header
  //and get tablename and field names from there
  //may need additional queries then
  
  switch(pid){
     case 140: //alert 
            getAlertSearch(req,res,oid,pid,tid,pgid,size,data);
            break;
            
         default:
           processStatus(req,res,6,data);
            break;
  }
} 

function getAlertSearch(req,res,oid,pid,tid,pgid,size,data) {

 var searchInp = data.searchParams;
 var isAlertTypeName = false;
 var isStatusName = false;
 alertTypeName = '';
 statusName = '';
 
 var searchArr = [];
 var userArray = {org_id: oid };
 searchArr.push(userArray);
 
 
    var async = require('async');
    var type  = require('type-detect');
    
    async.forEachOf(searchInp,function(key,value,callback){
     switch(value){
       case 'conTitle':
       var tmpArray = {title: {$regex:key,$options:'i'}};
         searchArr.push(tmpArray);
         break; 
       case 'alertType':
         if(type(key) == 'string'){
          isAlertTypeName = true;
          alertTypeName = key;
         }
       else {
         var tmpArray = {alert_type_id: key};
         searchArr.push(tmpArray);
         }
         break; 
       case 'status':
        if(type(key) == 'string'){
          isStatusName = true;
          statusName = key;
         }
       else { var tmpArray = {alert_status_id: key};
         searchArr.push(tmpArray);
         }
         break;
       case 'date':
        var cmpDate = new Date(key);
        var cmpDate2 =new Date();
        cmpDate2.setDate(cmpDate.getDate()+1);
        //query on date range from to
        //instead of doing date_parts which is more time consuming
        var tmpArray = {create_date: {$gte: cmpDate, $lt: cmpDate2}};
        searchArr.push(tmpArray);
       break;
       case 'target':
       var tmpArray = {content_target: key};
       searchArr.push(tmpArray);
       break;
       case 'views':
       //Views to do not yet implemented 
          break;
       default:
           processStatus(req,res,6,data);
            break;
          
     
     }
       callback();
    
    },
    function(err){
      if(err)
          processStatus(req,res,6,data);
       else
          {
           if(isAlertTypeName == true)
             getAlertNameData(req,res,oid,pid,tid,pgid,size,searchArr,alertTypeName,data);
           else if(isStatusName == true)
             getStatusNameData(req,res,oid,pid,tid,pgid,size,searchArr,statusName,data);
           else
           getAlertSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data);
          }
     });
   
 
} 

function getAlertNameData(req,res,oid,pid,tid,pgid,size,searchArr,alrName,data) {
   var async = require('async');
   var utils = require('./searchUtils');
   var type = require('type-detect');
  
    
    utils.alertTypeId(alrName,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                      if(result.resultCount > 0){
                         var tmpArray = {alert_type_id: result.alertType};
                          searchArr.push(tmpArray);
                      }
                      else {
                        var tmpArray = {alert_type_id: -007}; //just a random number so that result set is empty
                          searchArr.push(tmpArray);
                      
                      }
       
                    
                  }
               getAlertSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data);
                  }
            
        });
 
} 

function getStatusNameData(req,res,oid,pid,tid,pgid,size,searchArr,statName,data) {
   var async = require('async');
   var utils = require('./searchUtils');
   var type = require('type-detect');
  
    
    utils.alertStatId(statName,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                      if(result.resultCount > 0){
                         var tmpArray = {alert_status_id: result.statType};
                          searchArr.push(tmpArray);
                      }
                else {
                        var tmpArray = {alert_status_id: -007}; //just a random number so that result set is empty
                          searchArr.push(tmpArray);
                      
                      }
                    
                  }
               getAlertSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data);
                  }
            
        });
 
} 

function getAlertSearchData(req,res,oid,pid,tid,pgid,size,searchArr,data) {
   var async = require('async');
   var utils = require('./searchUtils');
   var type = require('type-detect');
  
    
    utils.alertData(oid,tid,pgid,size,searchArr,function(err,result) {
         if(err) processStatus(req,res,6,data);
         else {
            if(type(result) == 'array' || type(result) === 'object')
                 {
                       data.recordCount = result.resultCount;
                       if(result.resultCount > 0)
                        data.searchData = result.resultArray;
                    
                  }
                  processStatus(req,res,9,data);
                  }
            
        });
 
} 

function processStatus(req,res,stat,data) {
    var controllerpath = app.get('controller');
    var controller = require(controllerpath+'Search/searchData');
  
    controller.process_status(req,res,stat,data);
}
   
exports.getData = processRequest;
>>>>>>> DEV/master
