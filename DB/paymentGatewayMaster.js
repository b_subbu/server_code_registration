conn = new Mongo();
db = conn.getDB('scarabDB');



db.createCollection("payment_gateways_master");

db.payment_gateways_master.update(
  {},
  { $set: {gateway_id: Number},
    $set: {gateway_name: String},
    $set: {gateway_type: String},
    $set: {gateway_url: String},
    $set: {success_url: String},
    $set: {failure_url: String},
    $set: {cancel_url: String},
    $set: {payment_types: Array},
    $set: {payment_sub_types: Array}
  },
  { multi: true}

);

//We are keeping type_id, type_name as field names for all master tables
//to have single function for getting master type and names

db.payment_gateways_master.createIndex({ gateway_id:1},{unique: true});

