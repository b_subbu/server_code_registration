db.getCollection("businesstype_menusmap_master").insert({bus_type:1001,
                                          field_name:'menuCode',
                                          field_label: 'Menu Code',
                                          field_order: 1,
                                          mandatory: true
                                          });
db.getCollection("businesstype_menusmap_master").insert({bus_type:1001,
                                          field_name:'menuName',
                                          field_label: 'Name',
                                          field_order: 2,
                                          mandatory: true
                                          });
db.getCollection("businesstype_menusmap_master").insert({bus_type:1001,
                                          field_name:'menuDescription',
                                          field_label: 'Description',
                                          field_order: 3,
                                          mandatory: false
                                          });


db.getCollection("businesstype_menusmap_master").insert({bus_type:1002,
                                          field_name:'menuCode',
                                          field_label: 'Menu Code',
                                          field_order: 1,
                                          mandatory: true
                                          });
db.getCollection("businesstype_menusmap_master").insert({bus_type:1002,
                                          field_name:'menuName',
                                          field_label: 'Name',
                                          field_order: 2,
                                          mandatory: true
                                          });
db.getCollection("businesstype_menusmap_master").insert({bus_type:1002,
                                          field_name:'menuDescription',
                                          field_label: 'Description',
                                          field_order: 3,
                                          mandatory: false
                                          });
