conn = new Mongo();
db = conn.getDB('scarabDB');



db.createCollection("tax_types_master");

db.tax_types_master.update(
  {},
  { $set: {tax_id: Number},
    $set: {tax_name: String},
    $set: {tax_label: String},
    $set: {enabled: Boolean}
  },
  { multi: true}

);

db.tax_types_master.createIndex({ tax_id:1},{unique: true});

