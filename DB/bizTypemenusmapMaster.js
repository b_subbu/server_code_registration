conn = new Mongo();
db = conn.getDB('scarabDB');



db.businesstype_menusmap_master.drop();

db.createCollection("businesstype_menusmap_master");

db.businesstype_menusmap_master.update(
  {},
  { 
    $set: {bus_type: String},
    $set: {field_name: String},
    $set: {field_label: String},
    $set: {field_order: Number},
    $set: {mandatory: Boolean}
    },
  { multi: true}

);


db.businesstype_menusmap_master.createIndex({ org_id:1,bus_type:1});
