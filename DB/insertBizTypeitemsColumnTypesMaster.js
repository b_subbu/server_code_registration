use scarabDB
//Even though business type is not really required as per requirement
//just having it here maybe for future use
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1001,
                                          type_id:1,
                                          type_label: 'Number',
                                          type_additionals:[]
                                          });
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1001,
                                          type_id:2,
                                          type_label: 'String',
                                          type_additionals:[]
                                          });
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1001,
                                          type_id:3,
                                          type_label: 'Weight',
                                          type_additionals:[{id:31,label:'Grams'},{id:32,label:'KiloGrams'}]
                                          });
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1001,
                                          type_id:4,
                                          type_label: 'Size',
                                          type_additionals:[{id:41,label:'Inches'},{id:42,label:'MM'},{id:43,label:'Meters'}]
                                          });
                                          
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1002,
                                          type_id:1,
                                          type_label: 'Number',
                                          type_additionals:[]
                                          });
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1002,
                                          type_id:2,
                                          type_label: 'String',
                                          type_additionals:[]
                                          });
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1002,
                                          type_id:3,
                                          type_label: 'Weight',
                                          type_additionals:[{id:31,label:'Grams'},{id:32,label:'KiloGrams'}]
                                          });
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:1002,
                                          type_id:4,
                                          type_label: 'Size',
                                          type_additionals:[{id:41,label:'Inches'},{id:42,label:'MM'},{id:43,label:'Meters'}]
                                          });
                                          
/** currency is no more a system wide column type definition but will be base currency definition                                         
db.getCollection("businesstype_items_columntypes_master").insert({bus_type:'1001',
                                          type_id:5,
                                          type_label: 'Currency',
                                          type_additionals:[{id:51,label:'INR'}]
                                          });
**/                                          

