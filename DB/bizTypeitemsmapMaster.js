use scarabDB;

db.businesstype_itemsmap_master.drop();

db.createCollection("businesstype_itemsmap_master");

db.businesstype_itemsmap_master.update(
  {},
  { 
    $set: {bus_type: String},
    $set: {field_name: String},
    $set: {field_label: String},
    $set: {header_field: Boolean},
    $set: {field_order: Number},
    $set: {mandatory: Boolean}
    },
  { multi: true}

);


db.businesstype_itemsmap_master.createIndex({ bus_type:1});
