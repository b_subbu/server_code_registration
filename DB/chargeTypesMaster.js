conn = new Mongo();
db = conn.getDB('scarabDB');



db.createCollection("charge_types_master");

db.charge_types_master.update(
  {},
  { $set: {charge_id: Number},
    $set: {charge_name: String},
    $set: {charge_label: String},
    $set: {enabled: Boolean},
    $set: {charge_types: Array}
  },
  { multi: true}

);

db.charge_types_master.createIndex({ charge_id:1},{unique: true});
