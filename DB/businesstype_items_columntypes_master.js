conn = new Mongo();
db = conn.getDB('scarabDB');



db.businesstype_items_columntypes_master.drop();

db.createCollection("businesstype_items_columntypes_master");

db.businesstype_items_columntypes_master.update(
  {},
  { 
    $set: {bus_type: String},
    $set: {type_id: String},
    $set: {type_label: String},
    $set: {type_additionals: Arrays}
    },
  { multi: true}

);


db.businesstype_items_columntypes_master.createIndex({ bus_type:1});

