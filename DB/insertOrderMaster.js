use scarabDB
db.getCollection("orders_status_master").insert({status_id:80,
                                              status_name: "Entered"
                                             });
db.getCollection("orders_status_master").insert({status_id:81,
                                              status_name: "Ready2Ship"
                                             }); 
//Ready2Ship means payment was successful                                             
db.getCollection("orders_status_master").insert({status_id:82,
                                              status_name: "Payment Failed"
                                             });
db.getCollection("orders_status_master").insert({status_id:83,
                                              status_name: "Shipped"
                                             });
db.getCollection("orders_status_master").insert({status_id:84,
                                              status_name: "Delivered"
                                             });
db.getCollection("orders_status_master").insert({status_id:85,
                                              status_name: "Cancelled by Business"
                                             });
db.getCollection("orders_status_master").insert({status_id:86,
                                              status_name: "Returned"
                                             });
db.getCollection("orders_status_master").insert({status_id:87,
                                              status_name: "Rejected By Customer"
                                             });
db.getCollection("orders_status_master").insert({status_id:88,
                                                status_name:"Ready2Refund"
                                                });                                
db.getCollection("orders_status_master").insert({status_id:89,
                                                status_name:"Refunded"
                                                });     
