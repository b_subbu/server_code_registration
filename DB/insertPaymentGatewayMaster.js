db.getCollection("payment_gateways_master").insert({
   gateway_id: 11,
   gateway_name: "PayU",
   gateway_type: "Test",
   gateway_url: "https://test.payu.in/_payment",
   success_url: "/sucessPayUPayment",
   failure_url: "/failurePayUPayment",
   cancel_url: "/cancelPayUPayment",
   payment_types: [{id:'creditcard',label:'Credit Card'},{id:'debitcard',label:'Debit Card'},{id:'netbanking',label:'Net Banking'}],
   payment_sub_types: { "debitcard" : [{id:"VISA",label:"VISA"},{id:"SMAE",label:"State Bank Mastero Card"}],
                       "netbanking" : [{id:"AXIB",label:"Axis Bank"}]
      }
});

db.getCollection("payment_gateways_master").insert({
   gateway_id: 12,
   gateway_name: "PayU",
   gateway_type: "Prod",
   gateway_url: "https://payu.in/_payment",
   success_url: "/sucessPayUPayment",
   failure_url: "/failurePayUPayment",
   cancel_url: "/cancelPayUPayment",
   payment_types: [{id:'creditcard',label:'Credit Card'},{id:'debitcard',label:'Debit Card'},{id:'netbanking',label:'Net Banking'}],
   payment_sub_types: { "debitcard" : [{id:"VISA",label:"VISA"},{id:"SMAE",label:"State Bank Mastero Card"}],
                       "netbanking" : [{id:"AXIB",label:"Axis Bank"}]
      }
});

//Add nodeURL to success, failure and cancel before rendering since
//url will change between DEV, Staging and Prod
