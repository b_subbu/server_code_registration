use scarabDB

db.createCollection("currency_type_master");

db.currency_type_master.update(
  {},
  { $set: {type_id: Number},
    $set: {type_name: String}
  },
  { multi: true}

);


db.currency_type_master.createIndex({ type_id:1},{unique: true});
db.currency_type_master.createIndex({ type_name:1},{unique: true});
