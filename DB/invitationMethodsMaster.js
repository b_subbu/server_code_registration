conn = new Mongo();
db = conn.getDB('scarabDB');



db.invitation_methods_master.drop();

db.createCollection("invitation_methods_master");

db.invitation_methods_master.update(
  {},
  { 
    $set: {method_id: Number},
    $set: {method_name: String},
    },
  { multi: true}

);


db.invitation_methods_master.createIndex({ method_id:1});

