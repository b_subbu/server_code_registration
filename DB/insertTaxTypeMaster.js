db.getCollection("tax_types_master").insert({
   business_type: 1001,
   country_id: 1269750,
   state_id: 1267701,
   tax_id: 11,
   tax_name: "VAT",
   tax_label: "Value Added Tax",
   enabled: true,
});
db.getCollection("tax_types_master").insert({
   business_type: 1001,
   country_id: 1269750,
   state_id: 1267701,
   tax_id: 12,
   tax_name: "ST",
   tax_label: "Service Tax",
   enabled: true,
});
db.getCollection("tax_types_master").insert({
   business_type: 1001,
   country_id: 1269750,
   state_id: 1267701,
   tax_id: 13,
   tax_name: "CST",
   tax_label: "Central Sales Tax",
   enabled: true,
});
db.getCollection("tax_types_master").insert({
   business_type: 1001,
   country_id: 1269750,
   state_id: 1267701,
   tax_id: 14,
   tax_name: "GST",
   tax_label: "Goods and Service Tax",
   enabled: true,
});
