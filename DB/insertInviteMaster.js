use scarabDB
//This is only for testing
//actual population of this table will be done when Invitation API is done
db.getCollection("invite_master").insert({invitation_code: 'JusC0001',
                                          org_id: 'JusC00037',
                                          biz_id: '55a8d9c3076c1f8c721c8888',
										                      bus_type: '1001',
                                          invite_date: new Date(),
                                          });
db.getCollection("invite_master").insert({invitation_code: 'BusC0001',
                                          org_id: 'BusC00028',
                                          biz_id: '559e13c4b1e1046a3c5afd14',
										                      bus_type: '1002',
                                          invite_date: new Date(),
                                          });
db.getCollection("invite_master").insert({invitation_code: 'AruC0001',
                                          org_id: 'AruC00038',
                                          biz_id: '55a1e5cd0a8261b45b1429e7',
										                      bus_type: '1001',
                                          invite_date: new Date(),
                                          });

db.getCollection("invite_master").insert({invitation_code: 'JusC0001',
                                          org_id: 'BusC00028',
                                          biz_id: '559e13c4b1e1046a3c5afd14',
										                      bus_type: '1002',
                                          invite_date: new Date(),
                                          });
db.getCollection("invite_master").insert({invitation_code: 'JusC0001',
                                          org_id: 'AruC00038',
                                          biz_id: '55a1e5cd0a8261b45b1429e7',
										                      bus_type: '1001',
                                          invite_date: new Date(),
                                          });

//TO add following manually after registration
db.getCollection("cust_following").insert({cust_id: 'JusC0001000052',
                                          org_id: 'BusC00028',
                                          biz_id: '559e13c4b1e1046a3c5afd14',
										                      bus_type: '1001',
                                          follow_date: new Date(),
                                          });
db.getCollection("cust_following").insert({cust_id: 'JusC0001000052',
                                          org_id: 'AruC00038',
                                          biz_id: '55c2c444671f543d64991464',
										                      bus_type: '1001',
                                          follow_date: new Date(),
                                          });
                                          
                                          
//STAGING DB
db.getCollection("invite_master").insert({invitation_code: 'JusC0001',
                                          org_id: 'JusC00003',
                                          biz_id: '55e6efeb830bb9c556387143',
										                      bus_type: '1001',
                                          invite_date: new Date(),
                                          });
db.getCollection("invite_master").insert({invitation_code: 'BusC0001',
                                          org_id: 'JusC00003',
                                          biz_id: '55e6efeb830bb9c556387143',
										                      bus_type: '1001',
                                          invite_date: new Date(),
                                          });
db.getCollection("invite_master").insert({invitation_code: 'AruC0001',
                                          org_id: 'JusC00003',
                                          biz_id: '55e6efeb830bb9c556387143',
										                      bus_type: '1001',
                                          invite_date: new Date(),
                                          });
                                          
