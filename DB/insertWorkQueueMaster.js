db.getCollection("work_queue_master").insert({
   workqueue_id: 1,
   workqueue_name: "Take-Away",
   description: "Take Away Work Queue",
   product_id: 170,
   tab_id: 999,
   status_ids: [81,85,90,91],
   additional_criterias: [{delivery_option:11}],
   max_queue_time: 10
});
db.getCollection("work_queue_master").insert({
   workqueue_id: 2,
   workqueue_name: "Eat-In",
   description: "Eat In Work Queue",
   product_id: 170,
   tab_id: 999,
   status_ids: [81,85,90],
   additional_criterias: [{delivery_option:12}],
   max_queue_time: 10
});
db.getCollection("work_queue_master").insert({
   workqueue_id: 3,
   workqueue_name: "Local-Delivery",
   description: "Local Delivery Work Queue",
   product_id: 170,
   tab_id: 999,
   status_ids: [81,85,90,91],
   additional_criterias: [{delivery_option:13}],
   max_queue_time: 120
});
db.getCollection("work_queue_master").insert({
   workqueue_id: 4,
   workqueue_name: "Courier",
   description: "Courier Work Queue",
   product_id: 170,
   tab_id: 999,
   status_ids: [81,83,84,85,90],
   additional_criterias: [{delivery_option:14}],
   max_queue_time: 120
});
