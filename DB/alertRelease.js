use scarabDB

db.createCollection("alert_release");

db.alert_release.update(
  {},
  { 
    $set: {alert_id: String},
    $set: {bus_type: String},
    $set: {org_id: String},
    $set: {content_title: String},
    $set: {content_type_id: Integer},
    $set: {alert_type_id: Integer},
    $set: {image_url: String},
    $set: {video_url: String},
    $set: {image_text: String},
    $set: {descr: String},
    $set: {release_date: Date},
    $set: {update_date: Date}
  },
  { multi: true}

);

db.alert_release.update(
  {},
  { 
   $set: {descr: String},
  },
  { multi: true}

);

db.alert_release.createIndex({ alert_id:1});
