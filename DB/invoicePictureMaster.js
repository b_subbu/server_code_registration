conn = new Mongo();
db = conn.getDB('scarabDB');

db.invoice_picture_master.drop();

db.createCollection("invoice_picture_master");

db.invoice_picture_master.update(
  {},
  { 
    $set: {business_type: Number},
    $set: {picture_clause: Array},
    $set: {create_date: Date}
    },
  { multi: true}
);

db.invoice_picture_master.createIndex({ business_type:1},{unique: true});






