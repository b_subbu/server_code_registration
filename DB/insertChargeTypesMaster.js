db.getCollection("charge_types_master").insert({
   charge_id: 11,
   charge_name: "Tax",
   charge_label: "",
   enabled: true,
   charge_types:[{id:11,name:'VAT'},{id:12,name:'ST'},{id:13,name:'CST'},{id:14,name:'GST'}]
});
db.getCollection("charge_types_master").insert({
   charge_id: 12,
   charge_name: "Packing",
   charge_label: "",
   enabled: true,
   charge_types:[{id:11,name:'Take-Away'},{id:12,name:'Eat-In'},{id:13,name:'Local-Delivery'},{id:14,name:'Courier'}]
});
db.getCollection("charge_types_master").insert({
   charge_id: 13,
   charge_name: "Shipping",
   charge_label: "",
   enabled: true,
   charge_types:[{id:11,name:'Take-Away'},{id:12,name:'Eat-In'},{id:13,name:'Local-Delivery'},{id:14,name:'Courier'}]
});
db.getCollection("charge_types_master").insert({
   charge_id: 14,
   charge_name: "Service",
   charge_label: "",
   enabled: true,
   charge_types:[{id:11,name:'Installation'},{id:12,name:'Maintenance'}]
});

