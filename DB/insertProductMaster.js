db.getCollection("product_master").insert({business_type: 1001,
 products:[
        {
          "id": 11,
          "sequence": 1,
          "name": "Administration",
          "subModules": [
            {
              "id": 106,
              "sequence": 1,
              "name": "System Setup",
              "subModules": [
                {
                  "id": 110,
                  "sequence": 1,
                  "name": "Company Profile"
                },
                {
                  "id": 111,
                  "sequence": 2,
                  "name": "Delivery Options"
                },
                {
                  "id": 112,
                  "sequence": 3,
                  "name": "Taxes"
                }
              ]
            },
            {
              "id": 107,
              "sequence": 2,
              "name": "Invoices",
              "subModules": [
                {
                  "id": 115,
                  "sequence": 1,
                  "name": "Charges"
                },
                {
                  "id": 116,
                  "sequence": 2,
                  "name": "Invoice Config"
                }
              ]
            },
            {
              "id": 120,
              "sequence": 3,
              "name": "Gateways",
              "subModules": [
                {
                  "id": 109,
                  "sequence": 1,
                  "name": "Payment Gateways"
                }
              ]
            },
            {
              "id": 121,
              "sequence": 4,
              "name": "Seller Config",
              "subModules": [
                {
                  "id": 122,
                  "sequence": 1,
                  "name": "Sellers"
                }
              ]
            },
            {
              "id": 101,
              "sequence": 5,
              "name": "User Management",
              "subModules": [
                {
                  "id": 130,
                  "sequence": 1,
                  "name": "Users"
                },
                {
                  "id": 131,
                  "sequence": 2,
                  "name": "User Roles"
                }
              ]
            }
          ]
        },
        {
          "id": 12,
          "sequence": 2,
          "name": "Operation",
          "subModules": [
            {
              "id": 102,
              "sequence": 2,
              "name": "Customer Engagement",
              "subModules": [
                {
                  "id": 140,
                  "sequence": 1,
                  "name": "Alerts"
                },
                {
                  "id": 141,
                  "sequence": 2,
                  "name": "Alert Templates"
                }
              ]
            },
            {
              "id": 103,
              "sequence": 3,
              "name": "Menu Management",
              "subModules": [
                {
                  "id": 160,
                  "sequence": 1,
                  "name": "Menus"
                },
                {
                  "id": 150,
                  "sequence": 2,
                  "name": "Items"
                },
                {
                  "id": 190,
                  "sequence": 3,
                  "name": "Groups"
                }
              ]
            },
            {
              "id": 104,
              "sequence": 4,
              "name": "Work Queues",
              "subModules": [
                {
                  "id": 1,
                  "name": "Take-Away"
                },
               {
                "id": 2,
                "name": "Eat-In" 
               },
               {
                  "id": 3,
                  "name": "Local-Delivery"
               },
               {
                  "id": 4,
                  "name": "Courier"
               },
             
               
             ]
            },
            {
              "id": 105,
              "sequence": 5,
              "name": "Customer Management",
              "subModules": [
                {
                  "id": 180,
                  "sequence": 1,
                  "name": "Invitations"
                }
              ]
            }
          ]
        },
        {
        "id": 13,
        "sequence": 3,
        "name": "Dashboard"
       }
      ]
});

