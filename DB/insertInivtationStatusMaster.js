use scarabDB
db.getCollection("invitation_status_master").insert({status_id:80,
                                              status_name: "Entered"
                                              });
db.getCollection("invitation_status_master").insert({status_id:81,
                                              status_name: "Ready2Auth"
                                             }); 
db.getCollection("invitation_status_master").insert({status_id:82,
                                              status_name: "Authorised"
                                             });
db.getCollection("invitation_status_master").insert({status_id:83,
                                              status_name: "Rejected"
                                             });
db.getCollection("invitation_status_master").insert({status_id:84,
                                              status_name: "Released"
                                             });
db.getCollection("invitation_status_master").insert({status_id:85,
                                              status_name: "Expired"
                                             });
db.getCollection("invitation_status_master").insert({status_id:86,
                                              status_name: "Deleted"
                                             });
