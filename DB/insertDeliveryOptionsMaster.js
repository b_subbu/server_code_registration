db.getCollection("delivery_options_master").insert({
   delivery_id: 11,
   delivery_name: "Take-Away",
   enabled: true,
   pay_options: [{id:1,label:'Cash',enabled:true},{id:2,label:'Online',enabled:true}],
   work_queue: {id:1,name:'Take-Away'}
});
db.getCollection("delivery_options_master").insert({
   delivery_id: 12,
   delivery_name: "Eat-In",
   enabled:true,
   pay_options: [{id:1,label:'Cash',enabled:true},{id:2,label:'Online',enabled:true}],
   work_queue: {id:2,name:'Eat-In'}
});
db.getCollection("delivery_options_master").insert({
   delivery_id: 13,
   delivery_name: "Local-Delivery",
   enabled:true,
   pay_options: [{id:1,label:'Cash',enabled:true},{id:2,label:'Online',enabled:true}],
   work_queue: {id:3,name:'Local-Delivery'}
});
db.getCollection("delivery_options_master").insert({
   delivery_id: 14,
   delivery_name: "Courier",
   enabled:true,
   pay_options: [{id:2,label:'Online',enabled:true}],
   work_queue: {id:4,name:'Courier'}
});
