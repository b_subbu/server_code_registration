db.getCollection("seller_types_master").insert({
   seller_type_id: 11,
   seller_type_name: "Main",
   seller_label: "Main Branch",
   enabled: true,
});
db.getCollection("seller_types_master").insert({
   seller_type_id: 12,
   seller_type_name: "Branch",
   seller_label: "Branch",
   enabled: true,
});
db.getCollection("seller_types_master").insert({
   seller_type_id: 13,
   seller_type_name: "vendor",
   seller_label: "Third Party",
   enabled: true,
});
