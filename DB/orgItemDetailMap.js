use scarabDB;

db.org_itemdetail_map.drop();

db.createCollection("org_itemdetail_map");

db.org_itemdetail_map.update(
  {},
  { 
    $set: {org_id: String},
    $set: {bus_type: String},
    $set: {table_name_detail: String},
    $set: {itemCode: String},
    $set: {field_name: String},
    $set: {field_label: String},
    $set: {field_order: Number},
    $set: {mandatory: Boolean}
   },
  { multi: true}

);


db.org_itemdetail_map.createIndex({ org_id:1,bus_type:1,itemCode:1});

