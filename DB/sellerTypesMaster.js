conn = new Mongo();
db = conn.getDB('scarabDB');



db.createCollection("seller_types_master");

db.seller_types_master.update(
  {},
  { $set: {seller_type_id: Number},
    $set: {seller_type_name: String},
    $set: {seller_type_label: String},
    $set: {enabled: Boolean}
  },
  { multi: true}

);

db.seller_types_master.createIndex({ seller_type_id:1},{unique: true});


