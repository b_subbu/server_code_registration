conn = new Mongo();
db = conn.getDB('scarabDB');



db.invitation_status_master.drop();

db.createCollection("invitation_status_master");

db.invitation_status_master.update(
  {},
  { 
    $set: {status_id: Number},
    $set: {status_name: String},
    },
  { multi: true}

);


db.invitation_status_master.createIndex({ status_id:1});
