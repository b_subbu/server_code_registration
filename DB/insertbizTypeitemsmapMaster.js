use scarabDB
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'itemCode',
                                          field_label: 'Item Code',
                                          header_field: true,
                                          field_order: 1,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'itemName',
                                          field_label: 'Brief Description',
                                          header_field: true,
                                          field_order: 2,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'itemDescription',
                                          field_label: 'Detail Description',
                                          header_field: true,
                                          field_order: 3,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'itemVisual',
                                          field_label: 'Visual',
                                          header_field: true,
                                          field_order:5,
                                          mandatory: false
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'itemPrice',
                                          field_label: 'Regular Price',
                                          header_field: true,
                                          field_order: 4,
                                          mandatory: true
                                          });



db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'itemDetailCode',
                                          field_label: 'code',
                                          header_field: false,
                                          field_order: 1,
                                          mandatory: true,
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'isDeleted',
                                          field_label: 'isDeleted',
                                          header_field: false,
                                          field_order: 1,
                                          mandatory: false
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'units',
                                          field_label: 'Units',
                                          header_field: false,
                                          field_order: 2,
                                          field_type: 1,//businesstype_items_columntypes_master
                                          field_required: true,
                                          field_editable: false,
                                          field_value: 1,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'stock',
                                          field_label: 'Stock',
                                          header_field: false,
                                          field_order: 3,
                                          field_type: 1,//businesstype_items_columntypes_master
                                          field_required: false,
                                          field_editable: true,
                                          mandatory: true
                                          });

db.getCollection("businesstype_itemsmap_master").insert({bus_type:1001,
                                          field_name:'price',
                                          field_label: 'Price',
                                          header_field: false,
                                          field_order: 4,
                                          field_type: 5,
                                          field_required: true,
                                          field_editable: true,
                                          mandatory: true
                                          });
//units and price must be set to end after all fields

db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'itemCode',
                                          field_label: 'Item Code',
                                          header_field: true,
                                          field_order: 1,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'itemName',
                                          field_label: 'Brief Description',
                                          header_field: true,
                                          field_order: 2,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'itemDescription',
                                          field_label: 'Detail Description',
                                          header_field: true,
                                          field_order: 3,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'itemVisual',
                                          field_label: 'Visual',
                                          header_field: true,
                                          field_order:5,
                                          mandatory: false
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'itemPrice',
                                          field_label: 'Regular Price',
                                          header_field: true,
                                          field_order: 4,
                                          mandatory: true
                                          });



db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'itemDetailCode',
                                          field_label: 'code',
                                          header_field: false,
                                          field_order: 1,
                                          mandatory: true,
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'isDeleted',
                                          field_label: 'isDeleted',
                                          header_field: false,
                                          field_order: 1,
                                          mandatory: false
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'units',
                                          field_label: 'Units',
                                          header_field: false,
                                          field_order: 2,
                                          field_type: 1,//businesstype_items_columntypes_master
                                          field_required: true,
                                          field_editable: false,
                                          field_value: 1,
                                          mandatory: true
                                          });
db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'stock',
                                          field_label: 'Stock',
                                          header_field: false,
                                          field_order: 3,
                                          field_type: 1,//businesstype_items_columntypes_master
                                          field_required: false,
                                          field_editable: true,
                                          mandatory: true
                                          });

db.getCollection("businesstype_itemsmap_master").insert({bus_type:1002,
                                          field_name:'price',
                                          field_label: 'Price',
                                          header_field: false,
                                          field_order: 4,
                                          field_type: 5,
                                          field_required: true,
                                          field_editable: true,
                                          mandatory: true
                                          });
//units and price must be set to end after all fields

//JUST for testing no real significance
db.getCollection("businesstype_itemsmap_master").insert({bus_type:'1001',
                                          field_name:'itemColor',
                                          field_label: 'color',
                                          header_field: false,
                                          mandatory: true
                                          });
db.getCollection("org_itemdetail_map").insert({  
    "org_id" : "BusC00030",
    "bus_type" : "1001",
    "table_name_detail" : "BusC00030_menuitem_selection",
    "itemCode": "RW-0002",
    "field_name" : "itemDetailCode",
    "field_label" : "code",
    "field_order": 1,
    "mandatory" : true
});

db.getCollection("org_itemdetail_map").insert({  
    "org_id" : "BusC00030",
    "bus_type" : "1001",
    "table_name_detail" : "BusC00030_menuitem_selection",
    "itemCode": "RW-0002",
    "field_name" : "itemColor",
    "field_label" : "color",
    "field_order": 2,
    "mandatory" : true
});

db.getCollection("org_itemdetail_map").insert({  
    "org_id" : "BusC00030",
    "bus_type" : "1001",
    "table_name_detail" : "BusC00030_menuitem_selection",
    "itemCode": "RW-0002",
    "field_name" : "itemSize",
    "field_label" : "size",
    "field_order": 3,
    "mandatory" : true
});


db.getCollection("BusC00030_menuitem_selection").insert({  
    "org_id" : "BusC00030",
    "bus_type" : "1001",
    "itemCode": "RW-0002",
    "itemSize": 'LL',
    "itemColor" : "Green",
    "qty": 200,
    "itemDetailCode" : "RX-00005-001",
    
});

db.getCollection("BusC00030_menuitem_selection").insert({  
    "org_id" : "BusC00030",
    "bus_type" : "1001",
    "itemCode": "RW-0002",
    "itemSize": 'LL',
    "itemColor" : "RED",
    "qty": 250,
    "itemDetailCode" : "RX-00005-001",
    
});
