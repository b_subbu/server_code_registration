<?php
/**
 * Ajax.php Ajax program to handle Ajax requests
 * For session Management 
 * Should be called like
 * http://ec2-54-213-174-132.us-west-2.compute.amazonaws.com/Ajax.php?functionName=sessionStart&userRole=admin
 * Or it can be post    
 *  
 */
header("Cache-Control: no-cache,no-store,must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header("Pragma: no-cache"); //to avoid caching of previous instructions
session_cache_limiter ('nocache');


//var_dump($_REQUEST);

if(isset($_REQUEST['functionName']))  {
    $funcName = html_entity_decode($_REQUEST['functionName']);
    unset($_REQUEST['func']);
    switch($funcName) {
    case 'sessionStart':
         start_session();
      break;
    case 'sessionCheck':
         session_check();
     break;
    case 'sessionEnd':
         session_end();
         break;
    default:
        echo 'System Error';
        break;
    }
}

//Set cookie with lifetime = 20 minutes (60*20)
//Create a random string as sessionid
//No need to create actual session
//We can just set session data inside cookie
//and check it
//Ie simulate sessions without real session
function start_session() {
       $lifetime=1200;
       $sess_id = substr(md5(rand()), 0, 8);
       setcookie("sessionName",$sess_id,time()+$lifetime);
       echo $sess_id;
       exit;
 
}

// if cookie session does not match the request then error out
//else increase lifetime of cookie by another 20 minutes and return back
function session_check() {

   $y = $_REQUEST['sessionId'];
   //var_dump($_COOKIE);
   if(isset($_COOKIE['sessionName']))
    $existingSession = $_COOKIE['sessionName'];
   else
    $existingSession = '';
   
   if($existingSession == null || empty($existingSession) || $existingSession != $y){
       echo 'Expired Session'; 
       exit;
       }
       
   else {
         $lifetime=1200;
         setcookie("sessionName",$y,time()+$lifetime);
         echo 'success';
         exit;
     }
}


//just set lifetime to zero for this cookie and end
function session_end(){
  setcookie("sessionName",null,-1);
 
  echo 'success';
  exit;
}
?>
