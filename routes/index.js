<<<<<<< HEAD
/**
 * File: index.js
 * Author: Subu
 * Date: 18-May-2015
 * Map to individual functions from app.js 
 * based on url path  
 * Response: Call corresponding controllers
 * Validations, authentications: None 
 * Delete all the commented routes later
 * As these had moved to **webcontroller        
 */
exports.initial = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'landing');
  x.execute(req,res);
}


exports.checkuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/checkUser');
  x.execute(req,res);
}


exports.generateregistration = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/generateRegistration');
  x.execute(req,res);
}

exports.verifyregistration = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/verifyRegistration');
  x.execute(req,res);
}

exports.subscribeuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/subscribeUser');
  x.execute(req,res);
}

exports.checksubscript = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/checkSubscription');
  x.execute(req,res);
}

exports.loginuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/loginUser');
  x.execute(req,res);
}

exports.checksessiontoken = function(req,res,next){

var controller = app.get('controller');
var x = require(controller+'checkSessionToken');
x.execute(req,res,next);

}
exports.logoutuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/logoutUser');
  x.execute(req,res);
}

exports.resetpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/resetPassword');
  x.execute(req,res);

}

exports.forgotpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/forgotPassword');
  x.execute(req,res);

}

exports.clearpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/clearPassword');
  x.execute(req,res);

}

exports.savesubscription = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/saveSubscription');
  x.execute(req,res);

}

exports.getusermodules = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getUserModules');
  x.execute(req,res);

}


exports.getproductdetails = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductDetails');
  x.execute(req,res);

}

exports.getproductheader = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductHeader');
  x.execute(req,res);

}

/**
exports.getproductdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductData');
  x.execute(req,res);

}


exports.searchdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Search/searchData');
  x.execute(req,res);


}


exports.getaudittrail = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Audit/getAuditTrail');
  x.execute(req,res);


}

**/


exports.checkout = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/checkout');
  x.execute(req,res);


}

exports.sucesspayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/successPayUPayment');
  x.execute(req,res);

}

exports.failurepayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/failurePayUPayment');
  x.execute(req,res);

}

exports.cancelpayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/cancelPayUPayment');
  x.execute(req,res);

}

exports.testdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'/createTestData');
  x.testData(req,res);

}

exports.testpaymentgateway = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/testPaymentGateway');
  x.execute(req,res);

}


exports.testsucesspayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/sucessPayUPayment');
  x.execute(req,res);

}

exports.testfailurepayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/failurePayUPayment');
  x.execute(req,res);

}

exports.testcancelpayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/cancelPayUPayment');
  x.execute(req,res);

}

//This is a temporary arrangement probably will be called from cron later
exports.testnotification = function(req,res){
  var model = app.get('model');
  var x = require(model+'Notification/sendNotification');
  x.execute(req,res);

}


exports.testsmsgateway = function(req,res){
  var model = app.get('model');
  var x = require(model+'Notification/testSMSGateway');
  x.execute(req,res);

}

//create all test data through API
//direct creation is resulting in DB issues
exports.releasetestdata = function(req,res){
  var modelpath = app.get('model');
  var model = require(modelpath+'Boarding/createReleaseAlertTestData');
   
    model.getData(req,res);

}

exports.dumpdb = function(req,res){
  var modelpath = app.get('model');
  var model = require(modelpath+'dumpDB');
   
    model.getData(req,res);

}

exports.testPDF = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'/testPDF');
  x.execute(req,res);

}


exports.businesswebcontroller = function(req,res){

  var controller = app.get('controller');
  var x = require(controller+'/businesswebController');
  x.execute(req,res);

}

exports.customerappcontroller = function(req,res){

  var controller = app.get('controller');
  var x = require(controller+'/customerappController');
  x.execute(req,res);

}


exports.none = function(req,res){
//console.log("Inside none");
=======
/**
 * File: index.js
 * Author: Subu
 * Date: 18-May-2015
 * Map to individual functions from app.js 
 * based on url path  
 * Response: Call corresponding controllers
 * Validations, authentications: None       
 */
//var controller = app.get('controller');
 
exports.initial = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'landing');
  x.execute(req,res);
}


exports.checkuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/checkUser');
  x.execute(req,res);
}


exports.generateregistration = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/generateRegistration');
  x.execute(req,res);
}

exports.verifyregistration = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/verifyRegistration');
  x.execute(req,res);
}

exports.subscribeuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/subscribeUser');
  x.execute(req,res);
}

exports.checksubscript = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/checkSubscription');
  x.execute(req,res);
}

exports.loginuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/loginUser');
  x.execute(req,res);
}

exports.resetpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/resetPassword');
  x.execute(req,res);

}

exports.forgotpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/forgotPassword');
  x.execute(req,res);

}

exports.clearpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/clearPassword');
  x.execute(req,res);

}

exports.userprofile = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/userProfile');
  x.execute(req,res);

}

exports.savesubscription = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/saveSubscription');
  x.execute(req,res);

}

exports.createalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/createAlert');
  x.execute(req,res);

}

exports.savealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/saveAlert');
  x.execute(req,res);

}

exports.editalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/editAlert');
  x.execute(req,res);

}

exports.submitalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/submitAlert');
  x.execute(req,res);

}

exports.releasealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/releaseAlert');
  x.execute(req,res);

}
exports.recallalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/recallAlert');
  x.execute(req,res);

}

exports.authorisealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/authoriseAlert');
  x.execute(req,res);

}

exports.rejectalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/rejectAlert');
  x.execute(req,res);

}

exports.deletealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/deleteAlert');
  x.execute(req,res);

}

exports.getusermodules = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getUserModules');
  x.execute(req,res);

}


exports.getproductdetails = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductDetails');
  x.execute(req,res);

}

exports.getproductdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductData');
  x.execute(req,res);

}

exports.deleteuser = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Admin/deleteUser');
  x.execute(req,res);


}

exports.searchdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Search/searchData');
  x.execute(req,res);


}

exports.testdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'/createTestData');
  x.testData(req,res);

}
exports.none = function(req,res){
//console.log("Inside none");
>>>>>>> DEV/master
}