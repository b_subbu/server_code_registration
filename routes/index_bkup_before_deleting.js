/**
 * File: index.js
 * Author: Subu
 * Date: 18-May-2015
 * Map to individual functions from app.js 
 * based on url path  
 * Response: Call corresponding controllers
 * Validations, authentications: None 
 * Delete all the commented routes later
 * As these had moved to **webcontroller        
 */
exports.initial = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'landing');
  x.execute(req,res);
}


exports.checkuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/checkUser');
  x.execute(req,res);
}


exports.generateregistration = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/generateRegistration');
  x.execute(req,res);
}

exports.verifyregistration = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/verifyRegistration');
  x.execute(req,res);
}

exports.subscribeuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/subscribeUser');
  x.execute(req,res);
}

exports.checksubscript = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/checkSubscription');
  x.execute(req,res);
}

exports.loginuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/loginUser');
  x.execute(req,res);
}

exports.checksessiontoken = function(req,res,next){

var controller = app.get('controller');
var x = require(controller+'checkSessionToken');
x.execute(req,res,next);

}
exports.logoutuser = function(req,res) {
  var controller = app.get('controller');
  var x = require(controller+'Registration/logoutUser');
  x.execute(req,res);
}

exports.resetpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/resetPassword');
  x.execute(req,res);

}

exports.forgotpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/forgotPassword');
  x.execute(req,res);

}

exports.clearpassword = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/clearPassword');
  x.execute(req,res);

}
/**
exports.getbusinessprofile = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/getBusinessProfile');
  x.execute(req,res);

}

exports.savebusinessprofile = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/saveBusinessProfile');
  x.execute(req,res);

}

exports.getpaymentgateway = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/getPaymentGateway');
  x.execute(req,res);

}

exports.createpaymentgateway = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/createPaymentGateway');
  x.execute(req,res);

}

exports.editpaymentgateway = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/editPaymentGateway');
  x.execute(req,res);

}

exports.authorisepaymentgateways = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/authorisePaymentGateways');
  x.execute(req,res);

}

exports.rejectpaymentgateways = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/rejectPaymentGateways');
  x.execute(req,res);

}

exports.releasepaymentgateway = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/releasePaymentGateway');
  x.execute(req,res);

}

exports.recallpaymentgateway = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/recallPaymentGateway');
  x.execute(req,res);

}

exports.deletepaymentgateways = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/deletePaymentGateways');
  x.execute(req,res);

}

exports.submitpaymentgateways = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/submitPaymentGateways');
  x.execute(req,res);

}


exports.getdeliveryoption = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/getDeliveryOption');
  x.execute(req,res);

}

exports.createdeliveryoption = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/createDeliveryOption');
  x.execute(req,res);

}

exports.editdeliveryoption = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/editDeliveryOption');
  x.execute(req,res);

}

exports.authorisedeliveryoptions = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/authoriseDeliveryOptions');
  x.execute(req,res);

}

exports.rejectdeliveryoptions = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/rejectDeliveryOptions');
  x.execute(req,res);

}

exports.submitdeliveryoptions = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/submitDeliveryOptions');
  x.execute(req,res);

}

exports.deletedeliveryoptions = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/deleteDeliveryOptions');
  x.execute(req,res);

}


exports.gettaxtype = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/getTaxType');
  x.execute(req,res);

}

exports.createtaxtype = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/createTaxType');
  x.execute(req,res);

}

exports.edittaxtype = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/editTaxType');
  x.execute(req,res);

}

exports.authorisetaxtypes = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/authoriseTaxTypes');
  x.execute(req,res);

}

exports.rejecttaxtypes = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/rejectTaxTypes');
  x.execute(req,res);

}

exports.submittaxtypes = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/submitTaxTypes');
  x.execute(req,res);

}

exports.deletetaxtypes = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Admin/deleteTaxTypes');
  x.execute(req,res);

}


exports.getseller = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Sellers/getSeller');
  x.execute(req,res);

}

exports.createseller = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Sellers/createSeller');
  x.execute(req,res);

}

exports.editseller = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Sellers/editSeller');
  x.execute(req,res);

}

exports.authorisesellers = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Sellers/authoriseSellers');
  x.execute(req,res);

}

exports.rejectsellers = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Sellers/rejectSellers');
  x.execute(req,res);

}

exports.submitsellers = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Sellers/submitSellers');
  x.execute(req,res);

}

exports.deletesellers = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Sellers/deleteSellers');
  x.execute(req,res);

}



exports.getinvoiceconfig = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'InvoiceConfig/getInvoiceConfig');
  x.execute(req,res);

}

exports.createinvoiceconfig = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'InvoiceConfig/createInvoiceConfig');
  x.execute(req,res);

}

exports.editinvoiceconfig = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'InvoiceConfig/editInvoiceConfig');
  x.execute(req,res);

}

exports.authoriseinvoiceconfigs = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'InvoiceConfig/authoriseInvoiceConfigs');
  x.execute(req,res);

}

exports.rejectinvoiceconfigs = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'InvoiceConfig/rejectInvoiceConfigs');
  x.execute(req,res);

}

exports.submitinvoiceconfigs = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'InvoiceConfig/submitInvoiceConfigs');
  x.execute(req,res);

}

exports.deleteinvoiceconfigs = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'InvoiceConfig/deleteInvoiceConfigs');
  x.execute(req,res);

}



exports.getcharge = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/getCharge');
  x.execute(req,res);

}

exports.createcharge = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/createCharge');
  x.execute(req,res);

}

exports.editcharge = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/editCharge');
  x.execute(req,res);

}

exports.authorisecharges = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/authoriseCharges');
  x.execute(req,res);

}

exports.rejectcharges = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/rejectCharges');
  x.execute(req,res);

}

exports.submitcharges = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/submitCharges');
  x.execute(req,res);

}

exports.deletecharges = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/deleteCharges');
  x.execute(req,res);

}

exports.activatecharge = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/activateCharge');
  x.execute(req,res);

}

exports.deactivatecharge = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/deactivateCharge');
  x.execute(req,res);

}

exports.authorisechargeactivations = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/authoriseChargeActivations');
  x.execute(req,res);

}

exports.rejectchargeactivations = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Charges/rejectChargeActivations');
  x.execute(req,res);

}

*/





exports.savesubscription = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Registration/saveSubscription');
  x.execute(req,res);

}

/**
exports.createalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/createAlert');
  x.execute(req,res);

}

exports.savealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/saveAlert');
  x.execute(req,res);

}

exports.editalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/editAlert');
  x.execute(req,res);

}

exports.submitalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/submitAlert');
  x.execute(req,res);

}

exports.releasealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/releaseAlert');
  x.execute(req,res);

}
exports.recallalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/recallAlert');
  x.execute(req,res);

}

exports.authorisealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/authoriseAlert');
  x.execute(req,res);

}

exports.rejectalert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/rejectAlert');
  x.execute(req,res);

}

exports.deletealert = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/deleteAlert');
  x.execute(req,res);

}


exports.savelearnalertimage = function(req,res) {
  var controller = app.get('controller');
  var x  = require(controller+'Boarding/saveLearnAlertImage');
  x.execute(req,res);

}
*/

exports.getusermodules = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getUserModules');
  x.execute(req,res);

}


exports.getproductdetails = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductDetails');
  x.execute(req,res);

}

exports.getproductheader = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductHeader');
  x.execute(req,res);

}

/**
exports.getproductdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Roles/getProductData');
  x.execute(req,res);

}


exports.searchdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Search/searchData');
  x.execute(req,res);


}


exports.getaudittrail = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Audit/getAuditTrail');
  x.execute(req,res);


}

**/

exports.registercustomer = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/registerCustomer');
  x.execute(req,res);


}

exports.logincustomer = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/loginCustomer');
  x.execute(req,res);


}

exports.getbusinesstypes = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/getBusinessTypes');
  x.execute(req,res);


}

exports.getbusinessesfortype = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/getBusinessesForType');
  x.execute(req,res);


}

exports.followbusiness = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/followBusiness');
  x.execute(req,res);


}

exports.unfollowbusiness = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/unfollowBusiness');
  x.execute(req,res);


}

exports.getalerts = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/getAlerts');
  x.execute(req,res);


}

exports.getalertsforbusiness = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/getAlertsForBusiness');
  x.execute(req,res);


}

exports.getalertlinkcontent = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Render/getAlertLinkContent');
  x.execute(req,res);


}
/**
exports.getmenuitemmasterfields = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/getItemMasterFields');
  x.execute(req,res);


}

exports.createmenuitemmasterfields = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/createMenuItemMasterFields');
  x.execute(req,res);


}

exports.deletemenuitemmasterfields = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/deleteMenuItemMasterFields');
  x.execute(req,res);


}

exports.savemenuitemmasterdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/saveMenuItemMasterData');
  x.execute(req,res);


}

exports.createmenuitemdetailfields = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/createMenuItemDetailFields');
  x.execute(req,res);


}

exports.deletemenuitemdetailfields = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/deleteMenuItemDetailFields');
  x.execute(req,res);


}

exports.deletemenuitemmasterdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/deleteMenuItemMasterData');
  x.execute(req,res);


}

exports.getmenuitem = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/getMenuItem');
  x.execute(req,res);


}

exports.getmenuitems = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/getMenuItems');
  x.execute(req,res);


}

exports.getsimilaritem = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/getSimilarItem');
  x.execute(req,res);


}

exports.createmenuitemdetails = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/createMenuItemDetails');
  x.execute(req,res);


}

exports.editmenuitemdetails = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/editMenuItemDetails');
  x.execute(req,res);


}


exports.deletemenuitems = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/deleteMenuItems');
  x.execute(req,res);


}

exports.createmenuitemvisuals = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/createMenuItemVisuals');
  x.execute(req,res);


}

exports.editmenuitemvisuals = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/editMenuItemVisuals');
  x.execute(req,res);


}

exports.savemenuitemdetails = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/saveMenuItemDetails');
  x.execute(req,res);


}

exports.approvemenuitems = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/approveMenuItems');
  x.execute(req,res);


}

exports.rejectmenuitems = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/rejectMenuItems');
  x.execute(req,res);


}

exports.getmenu = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/getMenu');
  x.execute(req,res);


}

exports.createmenu = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/createMenu');
  x.execute(req,res);


}

exports.editmenu = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/editMenu');
  x.execute(req,res);


}

exports.authorisemenus = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/authoriseMenus');
  x.execute(req,res);


}

exports.rejectmenus = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/rejectMenus');
  x.execute(req,res);


}

exports.releasemenus = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/releaseMenus');
  x.execute(req,res);


}

exports.recallmenus = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/recallMenus');
  x.execute(req,res);


}

exports.deletemenus = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/deleteMenus');
  x.execute(req,res);


}

exports.submitmenus = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Menu/submitMenus');
  x.execute(req,res);


}

*/

exports.getappmenus = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'AppMenu/getAppMenus');
  x.execute(req,res);


}

exports.getappmenu = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'AppMenu/getAppMenu');
  x.execute(req,res);


}

exports.getappitem = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'AppMenu/getAppItem');
  x.execute(req,res);


}

exports.getappitemselection = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'AppMenu/getAppItemSelection');
  x.execute(req,res);


}


exports.getappmenunode = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'AppMenu/getAppMenuNode');
  x.execute(req,res);


}

exports.addtocart = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Cart/addToCart');
  x.execute(req,res);


}

exports.removefromcart = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Cart/removeFromCart');
  x.execute(req,res);


}

exports.changeitemqtyincart = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Cart/changeItemQtyInCart');
  x.execute(req,res);


}

exports.getitemsincart = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Cart/getItemsInCart');
  x.execute(req,res);


}

exports.getcartitemscount = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Cart/getCartItemsCount');
  x.execute(req,res);


}

exports.getordersummary = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Cart/getOrderSummary');
  x.execute(req,res);


}

exports.checkout = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/checkout');
  x.execute(req,res);


}

exports.sucesspayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/successPayUPayment');
  x.execute(req,res);

}

exports.failurepayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/failurePayUPayment');
  x.execute(req,res);

}

exports.cancelpayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Checkout/cancelPayUPayment');
  x.execute(req,res);

}

/**
exports.getorder = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Orders/getOrder');
  x.execute(req,res);

}

exports.webcheckout = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Orders/checkOut');
  x.execute(req,res);

}

exports.checkin = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Orders/checkIn');
  x.execute(req,res);

}

exports.ship = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Orders/ship');
  x.execute(req,res);

}

exports.deliver = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Orders/Deliver');
  x.execute(req,res);

}



exports.getinvitation = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/getInvitation');
  x.execute(req,res);


}

exports.createinvitation = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/createInvitation');
  x.execute(req,res);


}

exports.editinvitation = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/editInvitation');
  x.execute(req,res);


}

exports.submitinvitations = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/submitInvitations');
  x.execute(req,res);


}

exports.authoriseinvitations = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/authoriseInvitations');
  x.execute(req,res);


}

exports.rejectinvitations = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/rejectInvitations');
  x.execute(req,res);


}

exports.deleteinvitations = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/deleteInvitations');
  x.execute(req,res);


}

exports.releaseinvitations = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/releaseInvitations');
  x.execute(req,res);


}

*/
exports.downloadapk = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invitation/downloadAPK');
  x.execute(req,res);


}

exports.getinvoices = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Invoices/getInvoices');
  x.execute(req,res);


}

/**
exports.getgroup = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Groups/getGroup');
  x.execute(req,res);


}

exports.creategroup = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Groups/createGroup');
  x.execute(req,res);


}

exports.editgroup = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Groups/editGroup');
  x.execute(req,res);


}

exports.deletegroups = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Groups/deleteGroups');
  x.execute(req,res);


}

exports.approvegroups = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Groups/approveGroups');
  x.execute(req,res);


}

exports.rejectgroups = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Groups/rejectGroups');
  x.execute(req,res);


}

*/

exports.getappgroup = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Groups/getAppGroup');
  x.execute(req,res);


}

/**
 *exports.getworkqueueitems = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Orders/getWorkQueueItems');
  x.execute(req,res);


}



exports.getrole = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Users/getRole');
  x.execute(req,res);


}

exports.createrole = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Users/createRole');
  x.execute(req,res);


}

exports.editrole = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Users/editRole');
  x.execute(req,res);


}

exports.authoriseroles = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Users/authoriseRoles');
  x.execute(req,res);


}

exports.rejectroles = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Users/rejectRoles');
  x.execute(req,res);


}

exports.submitroles = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Users/submitRoles');
  x.execute(req,res);


}

exports.deleteroles = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Users/deleteRoles');
  x.execute(req,res);


}

*/

exports.testdata = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'/createTestData');
  x.testData(req,res);

}

exports.testpaymentgateway = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/testPaymentGateway');
  x.execute(req,res);

}


exports.testsucesspayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/sucessPayUPayment');
  x.execute(req,res);

}

exports.testfailurepayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/failurePayUPayment');
  x.execute(req,res);

}

exports.testcancelpayupayment = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'Payment/cancelPayUPayment');
  x.execute(req,res);

}

//This is a temporary arrangement probably will be called from cron later
exports.testnotification = function(req,res){
  var model = app.get('model');
  var x = require(model+'Notification/sendNotification');
  x.execute(req,res);

}


exports.testsmsgateway = function(req,res){
  var model = app.get('model');
  var x = require(model+'Notification/testSMSGateway');
  x.execute(req,res);

}

//create all test data through API
//direct creation is resulting in DB issues
exports.releasetestdata = function(req,res){
  var modelpath = app.get('model');
  var model = require(modelpath+'Boarding/createReleaseAlertTestData');
   
    model.getData(req,res);

}

exports.dumpdb = function(req,res){
  var modelpath = app.get('model');
  var model = require(modelpath+'dumpDB');
   
    model.getData(req,res);

}

exports.testPDF = function(req,res){
  var controller = app.get('controller');
  var x = require(controller+'/testPDF');
  x.execute(req,res);

}


exports.businesswebcontroller = function(req,res){

  var controller = app.get('controller');
  var x = require(controller+'/businesswebController');
  x.execute(req,res);

}
exports.none = function(req,res){
//console.log("Inside none");
}