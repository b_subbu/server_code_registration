var debug = require('debug'); 
var monk = require('monk');

var mongo = require('mongoskin');

 
debuglog = debug('app:log');
debuglog.log = console.log.bind(console);
 
 //db = mongo.db('mongodb://scarabAdmin:scarab123@localhost:27017/scarabDB',{safe: true});
   //use monk for better results
   //now no driver seems to be available for mongodb 3.0 use with noauth
   //until we get new driver
   
   if(process.env.OS == 'Windows_NT') {   //local machine
           db = monk('scarabAdmin:scarab123@localhost:27017/scarabDB');

            //DEBUG = new debug();        
    }
    else {
        //QA or DEV DB
        db = monk('scarabAdmin:scarab123@ec2-54-149-12-173.us-west-2.compute.amazonaws.com:27017/scarabDB');
   }
//base url is the url of web server and node url is the url of App server
//both are of dev instance
   
baseURL = 'http://ec2-52-24-141-117.us-west-2.compute.amazonaws.com/';
nodeURL = 'http://ec2-54-149-103-9.us-west-2.compute.amazonaws.com:3000/';

module.exports = debuglog;
module.exports= db;
exports.baseURL = baseURL;
exports.nodeURL = nodeURL;
