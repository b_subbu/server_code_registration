/**
 *File: app.js
 *Author: Subu
 *Date: 18-May-2015
 *Desc: Sets project wide dependcies and configurations
 */
     
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var router = express.Router();
var cors = require('cors');
var favicon = require('serve-favicon'); 
var debug = require('debug'); 
var monk = require('monk');

var mongo = require('mongoskin');
db = require('./dbconn');

  var routes = require('./routes/index');

  var cluster = require('cluster');
 
   if(cluster.isMaster){
      // Count the machine's CPUs
      var cpuCount = require('os').cpus().length;
      for (var i = 0; i < cpuCount; i += 1) {
         cluster.fork();
      }
    
    cluster.on('exit', function (worker) {
   
    // Replace the dead worker,
    console.log('Worker ' + worker.id + ' died :(');
    cluster.fork();
    });
   }
 else { 
 
  app = express();

  app.set('port', process.env.PORT || 3000);
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.set('controller',path.join(__dirname,'controller/'));
  app.set('model',path.join(__dirname,'model/'));
  app.set('views', __dirname + '/views');
  app.set('fileuploadfolder',__dirname+'/public/upload_files/');
  app.set('publicfolder',__dirname+'/public/');
 
  app.set('env','development');
  
  app.use(logger('dev'));
  app.use(bodyParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded());

  app.use(cookieParser('scarab_123'));
  //app.use(express.session());
  app.use(express.static(path.join(__dirname, 'public')));
  app.options('*',cors());
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
   });

  app.use(favicon(__dirname + '/public/favicon.ico'));  
  app.get('/', routes.initial);
  app.post('/checkUser',routes.checkuser);
  app.post('/generateRegistration',routes.generateregistration);
  app.post('/verifyRegistration',routes.verifyregistration);
  app.post('/subscribeUser',routes.subscribeuser);
  app.post('/checkSubscription',routes.checksubscript);
  app.post('/loginUser',routes.loginuser);
  app.post('/resetPassword',routes.resetpassword);
  app.post('/forgotPassword',routes.forgotpassword);
  app.post('/clearPassword',routes.clearpassword);
  app.post('/userProfile',routes.userprofile);
  app.post('/saveSubscription',routes.savesubscription);
  app.post('/logoutUser',routes.logoutuser);
  
  app.post('/getUserModules',routes.getusermodules);
  app.post('/getProductDetails',routes.getproductdetails);
  app.post('/getProductData',routes.getproductdata);
  
  app.post('/getAlertMasterData',routes.createalert);
  app.post('/saveAlert',routes.savealert);
  app.post('/editAlert',routes.editalert);
  app.post('/submitAlert',routes.submitalert);
  app.post('/authoriseAlert',routes.authorisealert);
  app.post('/rejectAlert',routes.rejectalert);
  app.post('/deleteAlert',routes.deletealert);
  app.post('/releaseAlert',routes.releasealert);
  app.post('/recallAlert',routes.recallalert);
  
  app.post('/deleteUser',routes.deleteuser);
  
  app.post('/searchData',routes.searchdata);
  
  //Services from here can goto a seperate server later for performance
  //not much changes will be needed except in app and index.js
  
  app.post('/registerCustomer',routes.registercustomer);
  app.post('/loginCustomer',routes.logincustomer);
  app.post('/getBusinessTypes',routes.getbusinesstypes);
  app.post('/getBusinessesForType',routes.getbusinessesfortype);
  app.post('/followBusiness',routes.followbusiness);
  app.post('/unfollowBusiness',routes.unfollowbusiness);
  app.post('/getAlerts',routes.getalerts);
  app.post('/getAlertsForBusiness',routes.getalertsforbusiness);
  
   //The masterFields services may not be required as per re-design
  //just removed from here for faster startup controller+model exists
  //in case if required again
  
  app.post('/getMenuItem',routes.getmenuitem);
  app.post('/createMenuItemDetails',routes.createmenuitemdetails);
  app.post('/editMenuItemDetails',routes.editmenuitemdetails);
  app.post('/deleteMenuItems',routes.deletemenuitems);
  app.post('/createMenuItemVisuals',routes.createmenuitemvisuals);
  app.post('/editMenuItemVisuals',routes.editmenuitemvisuals);
  app.post('/approveMenuItems',routes.approvemenuitems);
  app.post('/rejectMenuItems',routes.rejectmenuitems);
  app.post('/getItemMetaData',routes.getsimilaritem);
  app.post('/getMenuItemsDetails',routes.getmenuitemsdetails);
  
  
  app.post('/getMenu',routes.getmenu); 
  app.post('/createMenu',routes.createmenu); 
  app.post('/editMenu',routes.editmenu); 
  app.post('/authoriseMenus',routes.authorisemenus);
  app.post('/rejectMenus',routes.rejectmenus);
  app.post('/releaseMenus',routes.releasemenus);
  app.post('/recallMenus',routes.recallmenus);
  app.post('/deleteMenus',routes.deletemenus);
  app.post('/submitMenus',routes.submitmenus);
  
  app.post('/getAppMenus',routes.getappmenus);
  app.post('/getAppMenu',routes.getappmenu);
  app.post('/getAppItem',routes.getappitem);
  app.post('/getAppItemSelection',routes.getappitemselection);
  app.post('/getAppMenuNode',routes.getappmenunode);
  
  app.post('/getProductHeader',routes.getproductheader);
  
  app.post('/addToCart',routes.addtocart);
  app.post('/removeFromCart',routes.removefromcart);
  app.post('/changeItemQtyInCart',routes.changeitemqtyincart);
  app.post('/getItemsInCart',routes.getitemsincart);
  app.post('/getCartItemsCount',routes.getcartitemscount);
  
  app.post('/checkout',routes.checkout);
  app.post('/sucessPayUPayment',routes.sucesspayupayment);
  app.post('/failurePayUPayment',routes.failurepayupayment);
  app.post('/cancelPayUPayment',routes.cancelpayupayment);
  
  app.post('/app/getOrder',routes.getorder);
  app.post('/updatePackage',routes.updatepackage);
  app.post('/updateWaybill',routes.updatewaybill);
  app.post('/updateDeliverydate',routes.updatedeliverydate);
  
  app.post('/getInvoices',routes.getinvoices);
  
  
  app.post('/testPaymentGateway',routes.testpaymentgateway);
  app.post('/testsucessPayUPayment',routes.testsucesspayupayment);
  app.post('/testfailurePayUPayment',routes.testfailurepayupayment);
  app.post('/testcancelPayUPayment',routes.testcancelpayupayment);
  
  app.post('/createTestData',routes.testdata);
  app.post('/createReleaseTestData',routes.releasetestdata);
  app.post('/dumpDB',routes.dumpdb);
  


/// catch 404 and forward to error handler
   app.use(function(req, res, next) {
     var err = new Error('Not Found');
     err.status = 404;
     next(err);
    });

/// error handlers

// development error handler
// will print stacktrace
   if (app.get('env') === 'development') {
      app.use(function(err, req, res, next) {
          console.log(err);
          res.status(err.status || 500).jsonp({status:err.status,error:err.message,stack:err.stack});
       });
}

// production error handler
// no stacktraces leaked to user
    app.use(function(err, req, res, next) {
       res.status(err.status || 500).jsonp({error:"System Error Please try later"});
     });



  module.exports = app;

  app.listen(app.get('port'), function(){
      console.log("Express server listening on port " + app.get('port')+" on Worker:"+cluster.worker.id);
  });
 
  /** Implement below for using socket based connections
   *Like Progress bars if required in any page   
  var server = require('http').Server(app);
  io = require('socket.io')(server);
   
  module.exports = io;
  */
  
}